<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;
$aMenuLinksExt = array();

if(CModule::IncludeModule('iblock'))
{
    $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","DETAIL_PAGE_URL");
    $arFilter = Array("IBLOCK_ID"=>const_IBLOCK_ID_brands, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, array('nTopCount' => 9), $arSelect);
    while($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();
        $aMenuLinksExt[]=array(
            "0"=>$arFields["NAME"],
            "1"=>$arFields["DETAIL_PAGE_URL"],
            "2"=>array("0"=>$arFields["DETAIL_PAGE_URL"]),
        "3" => array
    (
        "FROM_IBLOCK" => 1,
                    "IS_PARENT" => 1,
                    "DEPTH_LEVEL" => 1,
                )
        );
    }
}

$aMenuLinks = array_merge( $aMenuLinksExt,$aMenuLinks);
?>