<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$out = array();
if ($arParams['SECTIONS']) {
	
	$arParams['SECTIONS'] = array_slice($arParams['SECTIONS'], 0, 4);
	
	if (count($arParams['SECTIONS'])) {
		$out[] = array(
		    'label' => "Разделы",
		);
	}
	
	foreach ($arParams['SECTIONS'] as $section) {
		$out[] = array(
		    'label' => "  ".htmlspecialchars_decode($section["NAME"]),
		    'value' => $section["SECTION_PAGE_URL"],
		);
	}
}

if (count($arResult['ITEMS'])) {
	$out[] = array(
		    'label' => "Товары",
		);
}
foreach ($arResult['ITEMS'] as $key => $arItem)
{
//	$out[] = $arItem["NAME"];
	$out[] = array(
	    'label' => "  ".htmlspecialchars_decode($arItem["NAME"]),
	    'value' => $arItem["DETAIL_PAGE_URL"],
	);
}

echo json_encode($out);