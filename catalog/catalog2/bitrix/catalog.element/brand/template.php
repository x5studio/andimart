<?
global $currentBrand;
$currentBrand = $arResult;
?>
    <div class="brand-card">
	<h1 class="brand-card__title title-line title-line--accent js-title-line"><?=$arResult['NAME']?></h1>
	<div class="row row row--vcentr">
        <? if (!empty($arResult['DETAIL_PICTURE'])) { ?>
	    <div class="col-12 col-sm-6 push-sm-6 text-center brand-card__img">
			<img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" width="156" height="60" alt="<?=$arResult['DETAIL_PICTURE']['ALT']?>">
	    </div>
        <? } ?>
	    <div class="col-12 col-sm-6 pull-sm-6 brand-card__descr">
            <?=!empty($arResult['PREVIEW_TEXT']) ? $arResult['~PREVIEW_TEXT'] : $arResult['~DETAIL_TEXT']?>
        </div>
	</div>

	<? if (!empty($arResult['PREVIEW_TEXT']) && !empty($arResult['DETAIL_TEXT'])) { ?>
        <div class="brand-card__more">
            <button class="brand-card__more-btn js-spoiler" data-spoiler-target="#brand-card-more" type="button">
            <span data-spoiler-collapse="Больше о бренде" data-spoiler-expand="Свернуть"></span>
            </button>
            <div id="brand-card-more" class="brand-card__more-content">
            <div class="brand-card__more-txt wysiwyg">
                <!-- Тут лучше разрешить только абзацы-->
                <?=$arResult['~DETAIL_TEXT']?>
            </div>
            </div>
	    </div>
	<? } ?>
    </div>
