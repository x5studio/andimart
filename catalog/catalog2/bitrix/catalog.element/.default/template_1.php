<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="content-block content-block--pull-top content-block--sm-bot island">
    <div class="product-card">
	<div class="row b-list">
	    <div class="col-12 col-sm-6 b-list__item">
		<div id="js-product-gallery" class="product-card__gallery">
                    <!-- Все фото должны быть одинакового размера!-->
		    <?
		    if ($arResult["PRICE"]["DISCOUNT_DIFF"])
		    {
			    ?>
	                    <div class="product-card__badge">
				<div class="product-card__badge-t">скидка</div>
				<div class="product-card__badge-sum"><?= $arResult["PRICE"]["PRINT_DISCOUNT_DIFF"] ?></div>
	                    </div>
			    <?
		    }
//deb($arResult["DETAIL_PICTURE"], false);
//deb($arResult["MORE_PHOTO"], false);
		    ?>
                    <div class="product-card__view js-product-slider">

			<? if (empty($arResult["DETAIL_PICTURE"]) && empty($arResult["MORE_PHOTO"])) { ?>
				<div class="product-card__image">
				    <img src="<?=SITE_TEMPLATE_PATH."/static/images/no_img.jpg"?>" width="284" height="284" alt itemprop="image">
				</div>
			<? } ?>
			<? if (!empty($arResult["DETAIL_PICTURE"])) {

				$img = CFile::ResizeImageGet(
						$arResult["DETAIL_PICTURE"]['ID'], Array("width" => 286, "height" => 286), BX_RESIZE_IMAGE_PROPORTIONAL, false, false, false, 90);
				?>
				<div class="product-card__image">
				    <img src="<?= $img["src"] ?>" width="284" height="284" alt itemprop="image">
				</div>
			<? } ?>
			<?
			if (!(count($arResult["MORE_PHOTO"]) == 1 && ($arResult["MORE_PHOTO"][0]['ID'] == $arResult["DETAIL_PICTURE"]['ID']))) {
			foreach ($arResult["MORE_PHOTO"] as $more_photo)
			{

				$img = CFile::ResizeImageGet(
						$more_photo['ID'], Array("width" => 286, "height" => 286), BX_RESIZE_IMAGE_PROPORTIONAL, false, false, false, 90);
				?>
				<? if (!empty($more_photo['DESCRIPTION']) && strpos($more_photo['DESCRIPTION'], 'http') === 0) { ?>
					<div class="product-card__image">
						<a class="js-popup-video" href="<?=$more_photo['DESCRIPTION']?>">
							<img src="<?= $img["src"] ?>" width="284" height="284" alt>
						</a>
					</div>
				<? } else { ?>
					<div class="product-card__image">
					    <img src="<?= $img["src"] ?>" width="284" height="284" alt>
					</div>
				<? } ?>
				<?
			}
			}
			?>
			<!--<div class="product-card__image">
			 <img src="http://placehold.it/284x284" width="284" height="284" alt>
		       </div>
		       <div class="product-card__image">
			 <a class="js-popup-video" href="https://www.youtube.com/watch?v=CrwLEKMj0G8">
			   <img src="https://placeholdit.imgix.net/~text?txtsize=27&amp;txt=video-preview&amp;w=284&amp;h=284" width="284" height="284" alt>
			 </a>
		       </div>-->
                    </div>
		    <? if (!(count($arResult["MORE_PHOTO"]) == 1 && ($arResult["MORE_PHOTO"][0]['ID'] == $arResult["DETAIL_PICTURE"]['ID']))) { ?>
                    <div class="product-card__thumbs js-product-thumbs">

			<? /*if (empty($arResult["DETAIL_PICTURE"]) && empty($arResult["MORE_PHOTO"])) { ?>
				<div class="product-card__thumb">
				    <div class="product-card__thumb-in">
					<img src="<?=SITE_TEMPLATE_PATH."/static/images/no_img.jpg"?>" width="284" height="284" alt>
				    </div>
				</div>
			<? }*/ ?>
			<? if (!empty($arResult["DETAIL_PICTURE"])) { ?>
				<div class="product-card__thumb">
				    <div class="product-card__thumb-in">
					<img src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>" width="284" height="284" alt>
				    </div>
				</div>
			<? } ?>
<?

foreach ($arResult["MORE_PHOTO"] as $more_photo)
{
	$img = CFile::ResizeImageGet(
			$more_photo['ID'], Array("width" => 286, "height" => 286), BX_RESIZE_IMAGE_PROPORTIONAL, false, false, false, 90);
	?>
			<? if (!empty($more_photo['DESCRIPTION']) && strpos($more_photo['DESCRIPTION'], 'http') === 0) { ?>
				<div class="product-card__thumb">
					<div class="product-card__thumb-in product-card__thumb-in--video">
						<img src="<?= $img["src"] ?>" width="284" height="284" alt>
					</div>
				</div>
			<? } else { ?>
				<div class="product-card__thumb">
				    <div class="product-card__thumb-in">
					<img src="<?= $img["src"] ?>" width="284" height="284" alt>
				    </div>
				</div>
			<? } ?>
	<?
}
?>
                    </div>
		    <? } ?>
		</div>
	    </div>
	    <div class="col-12 col-sm-6 b-list__item" itemtype="http://schema.org/Product" itemscope="itemscope">
		<h1 class="product-card__title title-line js-title-line" itemprop="name"><?= $arResult["NAME"] ?></h1>
		<div class="product-card__row row row--vcentr">
                    <div class="col-6 col-sm-5">
			<? //deb($arResult["PROPERTIES"]["CML2_ARTICLE"], false)?>
			<? if (!empty($arResult["PROPERTIES"]["CML2_ARTICLE"]["VALUE"])) { ?>
			    <div class="product-card__prop"><b>Код:</b> <?= $arResult["PROPERTIES"]["CML2_ARTICLE"]["VALUE"] ?></div>
			<? } ?>
			<? if ($arResult["PROPERTIES"]["BREND"]["VALUE"]) { ?>
			    <div class="product-card__prop"><b>Бренд:</b> <?= $arResult["PROPERTIES"]["BREND"]["VALUE"] ?></div>
			<? } ?>
                    </div>
		    <? if ($arResult["PROPERTIES"]["BREND"]["SRC"]) { ?>
			<div class="col-6 col-sm-7">
			    <a class="link-hidden" href="<?= $arResult["PROPERTIES"]["BREND"]["DETAIL_PAGE_URL"] ?>">
				<img class="product-card__brand-logo" src="<?= $arResult["PROPERTIES"]["BREND"]["SRC"] ?>"  alt>
			    </a>
			</div>
		    <? } ?>
		</div>
		<div class="product-card__row">
                    <div class="reviews-widget">
			<div class="reviews-widget__stars">
			    <div class="stars" data-rating="<?= $arResult["REVIEWS"]["SIGN_REAL"] ?>" itemtype="http://schema.org/AggregateRating" itemprop="aggregateRating" itemscope="itemscope">
				<meta itemprop="ratingValue" content="<?= $arResult["REVIEWS"]["SIGN_REAL"] ?>">
				<meta itemprop="bestRating" content="5">
				<meta itemprop="ratingCount" content="<?= $arResult["REVIEWS"]["COUNT"] ?>">
				<div class="stars__list"><i class="stars__star"></i><i class="stars__star"></i><i class="stars__star"></i><i class="stars__star"></i><i class="stars__star"></i></div>
			    </div>
			</div>
			<? if (!empty($arResult["REVIEWS"]["COUNT"])) { ?>
			<button class="reviews-widget__link link link--gray js-scroll-to-reviews">
			    <span>Читать <?= $arResult["REVIEWS"]["COUNT"] ?> <?= number_end($arResult["REVIEWS"]["COUNT"], array("отзыв", "отзыва", "отзывов")) ?></span>
			</button>
			<? } ?>
			<button class="reviews-widget__add link link--gray js-scroll-to-reviews-form">
			    <span>Оставить отзыв</span>
			</button>
                    </div>
		</div>
		<div class="product-card__row">
			<? if ($arResult["CATALOG_QUANTITY"]) {//CATALOG_QUANTITY ?>
				<span class="text-success">●</span> В наличии: <?= $arResult["CATALOG_QUANTITY"] ?> шт.
			<? } else { ?>
				<span class="text-warn">●</span> Нет в наличии
			<? } ?>
		</div>
		<? if (!empty($arResult["PRICE"]) && !empty($arResult["CATALOG_QUANTITY"])) { ?>
			<div class="product-card__row">
			    <div class="product-card__price-b">
				<div class="product-card__count">
				    <input class="input-text text-center" type="text" value="1">
				</div>
				<div class="product-card__prices">
					<? if ($arResult["PRICE"]["DISCOUNT_DIFF"]) { ?>
						<div class="product-card__price-old"><?= $arResult["PRICE"]["PRINT_VALUE"] ?></div>
					<? } ?>
					<div class="product-card__price" itemtype="http://schema.org/Offer" itemprop="offers" itemscope="itemscope">
					    <meta itemprop="price" content="<?= $arResult["PRICE"]["PRINT_DISCOUNT_VALUE"] ?>">
					    <meta itemprop="priceCurrency" content="RUB"><?= $arResult["PRICE"]["PRINT_DISCOUNT_VALUE"] ?></div>
				</div>
			    </div>
			</div>
			<div class="product-card__row">
			    <button class="btn btn--inverse add_to_cart_section" type="button" rel="<?= $arResult["ADD_URL"] ?>">
				<span>В корзину</span>
			    </button>
			</div>
		<? } ?>
		<div class="product-card__row">
                    <div class="product-card__delivery">
			<?
			$APPLICATION->IncludeComponent(
				"bitrix:main.include", "", Array(
			    "AREA_FILE_SHOW" => "file",
			    "AREA_FILE_SUFFIX" => "inc",
			    "EDIT_TEMPLATE" => "",
			    "PATH" => SITE_TEMPLATE_PATH . "/include/header/product_deliv.php"
				)
			);
			?>
		    </div>
		</div>
	    </div>
	</div>
    </div>
</div>
<div class="content-block content-block--sm-top island">
    <div class="content-block js-tabs">
	<div class="island-tabs js-island-tabs">
	    <button class="island-tabs__scroll-l js-island-tabs__l" type="button"></button>
	    <ul class="island-tabs__list js-island-tabs__scroll">
		<li class="island-tabs__item">
                    <a class="island-tabs__link js-tabs__btn" href="#product-descr">
			<span class="island-tabs__title">Описание</span>
                    </a>
		</li>
	<?

	$reviews_cnt = 0;

	$newsFilter["IBLOCK_ID"] = 5;
	$newsFilter["ACTIVE"] = 'Y';
	$newsFilter["PROPERTY_ITEM"] = $arResult["ID"];
	$newsFilter["PROPERTY_US"] = false;
	$elems = CIBlockElement::GetList(array(), $newsFilter, array('IBLOCK_ID'));
	$elems = $elems->Fetch();
	if (!empty($elems))
	{
		$reviews_cnt = $elems['CNT'];
	}
//	CNT
	?>
		<li class="island-tabs__item">
                    <a class="island-tabs__link js-tabs__btn" href="#product-reviews">
			<span class="island-tabs__title">Отзывы</span>
			<? if (!empty($reviews_cnt)) { ?>
			<span class="island-tabs__count"><?= $reviews_cnt ?></span>
			<? } ?>
                    </a>
		</li>
	<?

	$faq_cnt = 0;

	$newsFilter["IBLOCK_ID"] = 6;
	$newsFilter["ACTIVE"] = 'Y';
	$newsFilter["PROPERTY_ITEM"] = $arResult["ID"];
	$elems = CIBlockElement::GetList(array(), $newsFilter, array('IBLOCK_ID'));
	$elems = $elems->Fetch();
	if (!empty($elems))
	{
		$faq_cnt = $elems['CNT'];
	}
//	CNT
	?>

		<li class="island-tabs__item">
                    <a class="island-tabs__link js-tabs__btn" href="#product-faq">
			<span class="island-tabs__title">Вопрос-ответ</span>
			<? if (!empty($faq_cnt)) { ?>
				<span class="island-tabs__count"><?= $faq_cnt ?></span>
			<? } ?>
                    </a>
		</li>
		<? if (!empty($arResult["PROPERTIES"]["URL_TOVARA"]["VALUE"])) { ?>
			<li class="island-tabs__item">
			    <a class="island-tabs__link js-tabs__btn" href="#product-video">
				<span class="island-tabs__title">Видео</span>
				<span class="island-tabs__count"><?= count($arResult["PROPERTIES"]["URL_TOVARA"]["VALUE"]) ?></span>
			    </a>
			</li>
		<? } ?>
		<? if (!empty($arResult["PROPERTIES"]["AKSESSUARY"]["VALUE"])) { ?>
			<li class="island-tabs__item">
	                    <a class="island-tabs__link js-tabs__btn" href="#product-accessories">
				<span class="island-tabs__title">Аксессуары</span>
				<span class="island-tabs__count"><?= count($arResult["PROPERTIES"]["AKSESSUARY"]["VALUE"]) ?></span>
	                    </a>
			</li>
		<? } ?>
		<?

		if (!empty($arResult["ZAPCHASTI_CNT"]))
		{
			?>
			<li class="island-tabs__item">
	                    <a class="island-tabs__link js-tabs__btn" href="#product-parts">
				<span class="island-tabs__title">Запчасти</span>
				<span class="island-tabs__count"><?= $arResult["ZAPCHASTI_CNT"] ?></span>
	                    </a>
			</li>
	<?
}
?>
<?

$master_cnt = 0;
$us = CUser::GetList(($by = "personal_country"), ($order = "desc"),
	array('ACTIVE' => 'Y',
	    'GROUPS_ID' => 8,
	    'UF_SPECIALS' => $arResult['SECTION']['PATH'][0]['ID'],
	    array('LOGIC' => 'OR', 'UF_ADD_CITIES' => $_SESSION["PEK_CURRENT_CITY_ID"], 'UF_CITY' => $_SESSION["PEK_CURRENT_CITY_ID"])),
	array('FIELDS' => array('ID')));
while ($u = $us->Fetch())
{
	$master_cnt++;
}

$services_cnt = 0;
$us = CUser::GetList(($by = "personal_country"), ($order = "desc"),
	array('ACTIVE' => 'Y',
	    'GROUPS_ID' => 9,
	    'UF_BRAND' => $arResult["PROPERTIES"]["BREND"]["REAL_ID"],
	    'UF_SPECIALS' => $arResult['SECTION']['PATH'][0]['ID'],
	    array('LOGIC' => 'OR', 'UF_ADD_CITIES' => $_SESSION["PEK_CURRENT_CITY_ID"], 'UF_CITY' => $_SESSION["PEK_CURRENT_CITY_ID"])),
	array('FIELDS' => array('ID')));
while ($u = $us->Fetch())
{
	$services_cnt++;
}

//$services_cnt = 0;
//$master_cnt = 0;
?>
	<? if (!empty($master_cnt)) { ?>
		<li class="island-tabs__item">
                    <a class="island-tabs__link js-tabs__btn" href="#product-workers">
			<span class="island-tabs__title">Мастера</span>
			<span class="island-tabs__count"><?=$master_cnt?></span>
                    </a>
		</li>
	<? } ?>
	<? if (!empty($services_cnt)) { ?>
		<li class="island-tabs__item">
                    <a class="island-tabs__link js-tabs__btn" href="#product-services">
			<span class="island-tabs__title">Сервис-центры</span>
			<span class="island-tabs__count"><?=$services_cnt?></span>
                    </a>
		</li>
	<? } ?>
	    </ul>
	    <button class="island-tabs__scroll-r js-island-tabs__r" type="button"></button>
	</div>
	<div id="product-descr" class="tab-content content-block js-tabs__content">
	    <div class="b-list">
		<div class="wysiwyg b-list__item text-small-expand">
		    <p><?= $arResult["DETAIL_TEXT"] ?></p>
		</div>
		<div class="b-list__item">
                    <div class="row b-list">
				<? if (is_array($arResult["DISPLAY_PROPERTIES"]) && !empty($arResult["DISPLAY_PROPERTIES"]))
				{ ?>
				<div class="col-12 col-md-7 b-list__item">
				    <h3 class="section-title section-title--sm title-line title-line--accent js-title-line">Технические характеристики</h3>
				    <table class="props">
	<?
	foreach ($arResult["DISPLAY_PROPERTIES"] as $key => $Prop)
	{
		?>
						<tr class="props__item">
						    <td class="props__cell">
							<div class="props__label"><?= $Prop["NAME"] ?></div>
						    </td>
						    <td class="props__cell">
							<div class="props__value"><?= $Prop["DISPLAY_VALUE"] ?></div>
						    </td>
						</tr>
					<?
				}
				?>
				    </table>
				</div>
				<?
			}
			?>
				<?
				if (is_array($arResult["FILES"]) && !empty($arResult["FILES"]))
				{
					?>
				<div class="col-12 col-md-4 offset-md-1 b-list__item">
				    <h3 class="section-title section-title--sm title-line title-line--accent js-title-line">Файлы</h3>
				    <ul class="check-list">
					<?
					foreach ($arResult["FILES"] as $name => $file)
					{
						?>
						<li class="check-list__item">
						    <a target="_blank" href="<?= $file ?>"><?= $name ?></a>
						</li>
					<?
				}
				?>
				    </ul>
				</div>
	<?
}
?>
                    </div>
		</div>
	    </div>
	</div>


	<div id="product-reviews" class="tab-content content-block js-tabs__content">
	    <div class="ajax-preloader js-tab-ajax-content" data-src="/ajax/product_reviews.php?ITEM_ID=<?= $arResult["ID"] ?>&RFT_AJAX=Y"><i class="ajax-preloader__spinner"></i></div>
	</div>
	<div id="product-faq" class="tab-content content-block js-tabs__content">
	    <div class="ajax-preloader js-tab-ajax-content" data-src="/ajax/product_faq.php?ITEM_ID=<?= $arResult["ID"] ?>&RFT_AJAX=Y"><i class="ajax-preloader__spinner"></i></div>
	</div>
	<? if (!empty($arResult["PROPERTIES"]["URL_TOVARA"]["VALUE"])) { ?>
	<div id="product-video" class="tab-content content-block js-tabs__content">
	    <div class="ajax-preloader js-tab-ajax-content" data-src="/ajax/product_video.php?ITEM_ID=<?= $arResult["ID"] ?>&RFT_AJAX=Y"><i class="ajax-preloader__spinner"></i></div>

	</div>
	<? } ?>
	<?
	if (count($arResult["PROPERTIES"]["AKSESSUARY"]["VALUE"]))
	{
		?>
		<div id="product-accessories" class="tab-content content-block js-tabs__content">
		    <div class="ajax-preloader js-tab-ajax-content" data-src="/ajax/product_related.php?ITEM_ID=<?= $arResult["ID"] ?>&RFT_AJAX=Y&type=accessories"><i class="ajax-preloader__spinner"></i></div>
		</div>
		<?
	}
	?>
	<?
	if (!empty($arResult["ZAPCHASTI_CNT"]))
	{
		?>
		<div id="product-parts" class="tab-content content-block js-tabs__content">
		    <div class="ajax-preloader js-tab-ajax-content" data-src="/ajax/product_related.php?ITEM_ID=<?= $arResult["ID"] ?>&RFT_AJAX=Y&type=zapchasti"><i class="ajax-preloader__spinner"></i></div>
		</div>
	<?
}
?>
	<? if (!empty($master_cnt)) { ?>
	<div id="product-workers" class="tab-content content-block js-tabs__content">
	    <div class="ajax-preloader js-tab-ajax-content" data-src="/ajax/product_masters.php?special=<?= $arResult['SECTION']['PATH'][0]['ID'] ?>"><i class="ajax-preloader__spinner"></i></div>
	</div>
	<? } ?>

	<? if (!empty($services_cnt)) { ?>
	<div id="product-services" class="tab-content content-block js-tabs__content">
	    <div class="ajax-preloader js-tab-ajax-content" data-src="/ajax/product_services.php?special=<?= $arResult['SECTION']['PATH'][0]['ID'] ?>&brand=<?=$arResult["PROPERTIES"]["BREND"]["REAL_ID"]?>"><i class="ajax-preloader__spinner"></i></div>
	</div>
	<? } ?>
    </div>
</div>
<?
//p($_SESSION);
if (is_array($arResult["PROPERTIES"]["ANALAGI"]["VALUE"]) && !empty($arResult["PROPERTIES"]["ANALAGI"]["VALUE"]))
{
	global $viewd_filter1;
	$viewd_filter1["XML_ID"] = $arResult["PROPERTIES"]["ANALAGI"]["VALUE"];
	$APPLICATION->IncludeComponent(
		"bitrix:catalog.section", "product_slider", array(
	    "ACTION_VARIABLE" => "action",
	    "ADD_PICT_PROP" => "MORE_PHOTO",
	    "ADD_PROPERTIES_TO_BASKET" => "Y",
	    "ADD_SECTIONS_CHAIN" => "N",
	    "ADD_TO_BASKET_ACTION" => "ADD",
	    "AJAX_MODE" => "N",
	    "AJAX_OPTION_ADDITIONAL" => "",
	    "AJAX_OPTION_HISTORY" => "N",
	    "AJAX_OPTION_JUMP" => "N",
	    "AJAX_OPTION_STYLE" => "N",
	    "BACKGROUND_IMAGE" => "-",
	    "BASKET_URL" => "/lichniy_cabinet/basket.php",
	    "BROWSER_TITLE" => "-",
	    "CACHE_FILTER" => "N",
	    "CACHE_GROUPS" => "Y",
	    "CACHE_TIME" => "36000000",
	    "CACHE_TYPE" => "A",
	    "CONVERT_CURRENCY" => "N",
	    "DETAIL_URL" => "",
	    "DISABLE_INIT_JS_IN_COMPONENT" => "N",
	    "DISPLAY_BOTTOM_PAGER" => "N",
	    "DISPLAY_TOP_PAGER" => "N",
	    "ELEMENT_SORT_FIELD" => "catalog_PRICE_14",
	    "ELEMENT_SORT_FIELD2" => "id",
	    "ELEMENT_SORT_ORDER" => "asc",
	    "ELEMENT_SORT_ORDER2" => "desc",
	    "FILTER_NAME" => "viewd_filter1",
	    "HIDE_NOT_AVAILABLE" => 'Y',
	    "IBLOCK_ID" => "20",
	    "IBLOCK_TYPE" => "1c_catalog",
	    "INCLUDE_SUBSECTIONS" => "Y",
	    "LABEL_PROP" => "CML2_MANUFACTURER",
	    "LINE_ELEMENT_COUNT" => "3",
	    "MESSAGE_404" => "",
	    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
	    "MESS_BTN_BUY" => "Купить",
	    "MESS_BTN_DETAIL" => "Подробнее",
	    "MESS_BTN_SUBSCRIBE" => "Подписаться",
	    "MESS_NOT_AVAILABLE" => "Нет в наличии",
	    "META_DESCRIPTION" => "-",
	    "META_KEYWORDS" => "-",
	    "OFFERS_LIMIT" => "5",
	    "PAGER_BASE_LINK_ENABLE" => "N",
	    "PAGER_DESC_NUMBERING" => "N",
	    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	    "PAGER_SHOW_ALL" => "N",
	    "PAGER_SHOW_ALWAYS" => "N",
	    "PAGER_TEMPLATE" => ".default",
	    "PAGER_TITLE" => "Товары",
	    "PAGE_ELEMENT_COUNT" => "30",
	    "PARTIAL_PRODUCT_PROPERTIES" => "N",
	    "PRICE_CODE" => array(
		0 => "WEB-цена",
	    ),
	    "PRICE_VAT_INCLUDE" => "Y",
	    "PRODUCT_ID_VARIABLE" => "id",
	    "PRODUCT_PROPERTIES" => array(
	    ),
	    "PRODUCT_PROPS_VARIABLE" => "prop",
	    "PRODUCT_QUANTITY_VARIABLE" => "",
	    "PRODUCT_SUBSCRIPTION" => "N",
	    "PROPERTY_CODE" => array(
		0 => "BREND",
		1 => "",
	    ),
	    "SECTION_CODE" => "",
	    "SECTION_ID" => "",
	    "SECTION_ID_VARIABLE" => "SECTION_ID",
	    "SECTION_URL" => "",
	    "SECTION_USER_FIELDS" => array(
		0 => "",
		1 => "",
	    ),
	    "SEF_MODE" => "N",
	    "SET_BROWSER_TITLE" => "Y",
	    "SET_LAST_MODIFIED" => "N",
	    "SET_META_DESCRIPTION" => "Y",
	    "SET_META_KEYWORDS" => "Y",
	    "SET_STATUS_404" => "N",
	    "SET_TITLE" => "Y",
	    "SHOW_404" => "N",
	    "SHOW_ALL_WO_SECTION" => "Y",
	    "SHOW_CLOSE_POPUP" => "N",
	    "SHOW_DISCOUNT_PERCENT" => "N",
	    "SHOW_OLD_PRICE" => "N",
	    "SHOW_PRICE_COUNT" => "1",
	    "TEMPLATE_THEME" => "blue",
	    "USE_MAIN_ELEMENT_SECTION" => "N",
	    "USE_PRICE_COUNT" => "N",
	    "USE_PRODUCT_QUANTITY" => "Y",
	    "COMPONENT_TEMPLATE" => "product_slider",
	    "NAME" => "Похожие товары",
		), false
	);
}
?>
<?
if (count($_SESSION["IBLOCK_COUNTER"]) > 1)
{
	global $viewd_filter;
	$viewd_filter["ID"] = $_SESSION["IBLOCK_COUNTER"];
	$APPLICATION->IncludeComponent(
		"bitrix:catalog.section", "product_slider", array(
	    "ACTION_VARIABLE" => "action",
	    "ADD_PICT_PROP" => "MORE_PHOTO",
	    "ADD_PROPERTIES_TO_BASKET" => "Y",
	    "ADD_SECTIONS_CHAIN" => "N",
	    "ADD_TO_BASKET_ACTION" => "ADD",
	    "AJAX_MODE" => "N",
	    "AJAX_OPTION_ADDITIONAL" => "",
	    "AJAX_OPTION_HISTORY" => "N",
	    "AJAX_OPTION_JUMP" => "N",
	    "AJAX_OPTION_STYLE" => "N",
	    "BACKGROUND_IMAGE" => "-",
	    "BASKET_URL" => "/lichniy_cabinet/basket.php",
	    "BROWSER_TITLE" => "-",
	    "CACHE_FILTER" => "N",
	    "CACHE_GROUPS" => "Y",
	    "CACHE_TIME" => "36000000",
	    "CACHE_TYPE" => "A",
	    "CONVERT_CURRENCY" => "N",
	    "DETAIL_URL" => "",
	    "DISABLE_INIT_JS_IN_COMPONENT" => "N",
	    "DISPLAY_BOTTOM_PAGER" => "N",
	    "DISPLAY_TOP_PAGER" => "N",
	    "ELEMENT_SORT_FIELD" => "sort",
	    "ELEMENT_SORT_FIELD2" => "id",
	    "ELEMENT_SORT_ORDER" => "asc",
	    "ELEMENT_SORT_ORDER2" => "desc",
	    "FILTER_NAME" => "viewd_filter",
	    "HIDE_NOT_AVAILABLE" => 'Y',
	    "IBLOCK_ID" => "20",
	    "IBLOCK_TYPE" => "1c_catalog",
	    "INCLUDE_SUBSECTIONS" => "Y",
	    "LABEL_PROP" => "CML2_MANUFACTURER",
	    "LINE_ELEMENT_COUNT" => "3",
	    "MESSAGE_404" => "",
	    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
	    "MESS_BTN_BUY" => "Купить",
	    "MESS_BTN_DETAIL" => "Подробнее",
	    "MESS_BTN_SUBSCRIBE" => "Подписаться",
	    "MESS_NOT_AVAILABLE" => "Нет в наличии",
	    "META_DESCRIPTION" => "-",
	    "META_KEYWORDS" => "-",
	    "OFFERS_LIMIT" => "5",
	    "PAGER_BASE_LINK_ENABLE" => "N",
	    "PAGER_DESC_NUMBERING" => "N",
	    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	    "PAGER_SHOW_ALL" => "N",
	    "PAGER_SHOW_ALWAYS" => "N",
	    "PAGER_TEMPLATE" => ".default",
	    "PAGER_TITLE" => "Товары",
	    "PAGE_ELEMENT_COUNT" => "30",
	    "PARTIAL_PRODUCT_PROPERTIES" => "N",
	    "PRICE_CODE" => array(
		0 => "WEB-цена",
	    ),
	    "PRICE_VAT_INCLUDE" => "Y",
	    "PRODUCT_ID_VARIABLE" => "id",
	    "PRODUCT_PROPERTIES" => array(
	    ),
	    "PRODUCT_PROPS_VARIABLE" => "prop",
	    "PRODUCT_QUANTITY_VARIABLE" => "",
	    "PRODUCT_SUBSCRIPTION" => "N",
	    "PROPERTY_CODE" => array(
		0 => "BREND",
		1 => "",
	    ),
	    "SECTION_CODE" => "",
	    "SECTION_ID" => "",
	    "SECTION_ID_VARIABLE" => "SECTION_ID",
	    "SECTION_URL" => "",
	    "SECTION_USER_FIELDS" => array(
		0 => "",
		1 => "",
	    ),
	    "SEF_MODE" => "N",
	    "SET_BROWSER_TITLE" => "Y",
	    "SET_LAST_MODIFIED" => "N",
	    "SET_META_DESCRIPTION" => "Y",
	    "SET_META_KEYWORDS" => "Y",
	    "SET_STATUS_404" => "N",
	    "SET_TITLE" => "Y",
	    "SHOW_404" => "N",
	    "SHOW_ALL_WO_SECTION" => "Y",
	    "SHOW_CLOSE_POPUP" => "N",
	    "SHOW_DISCOUNT_PERCENT" => "N",
	    "SHOW_OLD_PRICE" => "N",
	    "SHOW_PRICE_COUNT" => "1",
	    "TEMPLATE_THEME" => "blue",
	    "USE_MAIN_ELEMENT_SECTION" => "N",
	    "USE_PRICE_COUNT" => "N",
	    "USE_PRODUCT_QUANTITY" => "Y",
	    "COMPONENT_TEMPLATE" => "product_slider",
	    "NAME" => "Вы смотрели",
		), false
	);
}
?>
