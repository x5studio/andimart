<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	
	$MESS["FIELD_NOT_FILL"] = "Required field.";
	$MESS["EMAIL_NOT_VALID"] = "Enter your email address.";
	$MESS["CAPTCHA_NOT_VALID"] = "Captcha is incorrect.";
?>