<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arCurrentValues */
/** @global CUserTypeManager $USER_FIELD_MANAGER */
global $USER_FIELD_MANAGER;

if(!\Bitrix\Main\Loader::includeModule("iblock"))
	return;

$boolCatalog = \Bitrix\Main\Loader::includeModule("catalog");

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arIBlock = array();
$rsIBlock = CIBlock::GetList(array("sort" => "asc"), array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE"=>"Y"));
while($arr=$rsIBlock->Fetch())
	$arIBlock[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];

$arProperty = array();
$arPropertySelected = array();
$arPropertySelectedNames = array();
$arProperty_LNS = array();
$arProperty_N = array();
$arProperty_X = array();
if (0 < intval($arCurrentValues['IBLOCK_ID']))
{
	$rsProp = CIBlockProperty::GetList(array("sort"=>"asc", "name"=>"asc"), array("IBLOCK_ID"=>$arCurrentValues["IBLOCK_ID"], "ACTIVE"=>"Y"));
	while ($arr=$rsProp->Fetch())
	{
		if($arr["PROPERTY_TYPE"] != "F")
		{
			$arProperty[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
			
			if (in_array($arr["CODE"], $arCurrentValues["PROPERTY_CODE"]))
			{
				$arPropertySelected[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
				$arPropertySelectedNames[$arr["CODE"]] = $arr["NAME"];
			}
		}
		
		if($arr["PROPERTY_TYPE"]=="N")
			$arProperty_N[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];

		if($arr["PROPERTY_TYPE"]!="F")
		{
			if($arr["MULTIPLE"] == "Y")
				$arProperty_X[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
			elseif($arr["PROPERTY_TYPE"] == "L")
				$arProperty_X[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
			elseif($arr["PROPERTY_TYPE"] == "E" && $arr["LINK_IBLOCK_ID"] > 0)
				$arProperty_X[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
		}
	}
}


$arComponentParameters = array(
	"PARAMETERS" => array(
		"IBLOCK_TYPE" => array(
			"PARENT" => "BASE",
			"NAME" => "Тип инфоблока",
			"TYPE" => "LIST",
			"VALUES" => $arIBlockType,
			"REFRESH" => "Y",
		),
		"IBLOCK_ID" => array(
			"PARENT" => "BASE",
			"NAME" => "Инфоблок",
			"TYPE" => "LIST",
			"ADDITIONAL_VALUES" => "Y",
			"VALUES" => $arIBlock,
			"REFRESH" => "Y",
		),
		"PROPERTY_CODE" => array(
			"PARENT" => "BASE",
			"NAME" => "Выводимые свойства",
			"TYPE" => "LIST",
			"MULTIPLE" => "Y",
			"VALUES" => $arProperty,
			"ADDITIONAL_VALUES" => "Y",
			"REFRESH" => "Y",
		),
		"PROPERTY_CODE_IS_REQUIRED" => array(
			"PARENT" => "BASE",
			"NAME" => "Обязательные свойства",
			"TYPE" => "LIST",
			"MULTIPLE" => "Y",
			"VALUES" => $arPropertySelected,
			"ADDITIONAL_VALUES" => "Y",
		),
		"CODE" => array(
			"PARENT" => "BASE",
			"NAME" => "Код формы"
		),
		"EVENT" => array(
			"PARENT" => "BASE",
			"NAME" => "Почтовое событий"
		)
	)
);

foreach ($arPropertySelectedNames as $code => $prop)
{
	$arComponentParameters['PARAMETERS']['CUSTOM_TITLE_'.$code] =	array(
										"PARENT" => "BASE",
										"NAME" => 'Заголовок поля "'.$prop.'"'
									);
}
foreach ($arPropertySelectedNames as $code => $prop)
{
	$arComponentParameters['PARAMETERS']['PROPERTY_DEFAULT_'.$code] =	array(
										"PARENT" => "BASE",
										"NAME" => 'Значение по умолчанию поля "'.$prop.'"'
									);
}
?>