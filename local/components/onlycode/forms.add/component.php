<?php

include $_SERVER['DOCUMENT_ROOT'].AM_HREF_TO_MAIN."include/lang.php";

global $DB;
CModule::IncludeModule("iblock");
			
$arParams['ADD_ACTIVE'] = (!empty($arParams['ADD_ACTIVE']) && in_array($arParams['ADD_ACTIVE'], array('N', 'Y')))?$arParams['ADD_ACTIVE']:'Y';
$arParams['ADD_ACTIVE_FROM'] = (!empty($arParams['ADD_ACTIVE_FROM']) && in_array($arParams['ADD_ACTIVE_FROM'], array('N', 'Y')))?$arParams['ADD_ACTIVE_FROM']:'N';

if (!isset($arParams['PROPERTY_CODE_IS_REQUIRED']))
{
	$arParams['PROPERTY_CODE_IS_REQUIRED'] = array();
}
$arParams['CODE'] = !empty($arParams['CODE'])?$arParams['CODE']:'formrequest';

if (empty($arParams['IBLOCK_ID']) && empty($arParams['PROPERTY_CODE']))
{
	return false;
}

$arResult = array();

$enums = array();
$enums_t = CIBlockPropertyEnum::GetList(array("sort"=>"asc", "name"=>"asc"), array("IBLOCK_ID"=>$arParams["IBLOCK_ID"]));
while ($enum = $enums_t->Fetch())
{
	$enums[$enum['PROPERTY_CODE']][$enum['ID']] = $enum;
}

$arResult['FIELDS'] = array();
$rsProp = CIBlockProperty::GetList(array("sort"=>"asc", "name"=>"asc"), array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ACTIVE"=>"Y"));
while ($property = $rsProp->Fetch())
{
	if (in_array($property['CODE'], $arParams['PROPERTY_CODE']))
	{
		if ($property['PROPERTY_TYPE'] === 'L')
		{
			$property['VARIANTS'] = $enums[$property['CODE']];
		}
		if ($property['PROPERTY_TYPE'] === 'E')
		{
			$property['VARIANTS'] = array();
			$elems = CIBlockElement::GetList(array('id'=>'asc'), array('IBLOCK_ID' => $property['LINK_IBLOCK_ID'], 'ACTIVE' => 'Y'));
			while ($elem = $elems->Fetch())
			{
				$property['VARIANTS'][$elem['ID']] = $elem;
			}
		}
		if ($property['PROPERTY_TYPE'] === 'G')
		{
			$property['VARIANTS'] = array();
			$sects = CIBlockElement::GetList(array('sort'=>'asc'), array('IBLOCK_ID' => $property['LINK_IBLOCK_ID'], 'ACTIVE' => 'Y'));
			while ($sect = $sects->Fetch())
			{
				$property['VARIANTS'][$sect['ID']] = $sect;
			}
		}
                
		if (in_array($property['CODE'], $arParams['PROPERTY_CODE_IS_REQUIRED']) && ($property['IS_REQUIRED'] !== 'Y'))
		{
			$property['IS_REQUIRED'] = 'Y';
		}
		
		if (!empty($arParams['CUSTOM_TITLE_'.$property['CODE']]))
		{
			$property['NAME'] = $arParams['CUSTOM_TITLE_'.$property['CODE']];
		}
		if (!empty($arParams['PROPERTY_DEFAULT_'.$property['CODE']]))
		{
			$property['VALUE'] = $arParams['PROPERTY_DEFAULT_'.$property['CODE']];
		}
		
		$arResult['FIELDS'][$property["CODE"]] = $property;
	}
	$arResult['FIELDS_FULL'][$property["CODE"]] = $property;
}
//deb($APPLICATION->CaptchaGetCode(), false);
if ($arParams['CAPTCHA'] == 'Y')
{
//	include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/captcha.php");
//	$arResult['CAPTCHA'] = new CCaptcha();
//	$arResult['CAPTCHA']->SetCodeCrypt($captchaPass);
	
	$arResult['CAPTCHA'] = $APPLICATION->CaptchaGetCode();
	
//	deb($arResult['CAPTCHA'], false);
}

//$arResult['SUBMITING'] = false;
if (!empty($_POST) && !empty($_POST[$arParams['CODE']]) && check_bitrix_sessid())
{
//	$arResult['SUBMITING'] = true;
	if (empty($arParams['AJAX_SEND']) || $arParams['AJAX_SEND']!=='N')
	{
		$APPLICATION->RestartBuffer();
	}
	
	$errors = array();
	$errorsTexts = array();
	$vars = $_POST[$arParams['CODE']];
	
	foreach ($arResult['FIELDS'] as $key => $field)
	{
		$value = $vars[$field['CODE']];
		
		if ($field['PROPERTY_TYPE'] == 'L' && empty($field['VARIANTS'][$value]))
		{
			$value = 0;
		}
		
		if (empty($value) && !empty($arParams['PROPERTY_DEFAULT_'.$field['CODE']]))
		{
			$value = $arParams['PROPERTY_DEFAULT_'.$field['CODE']];
		}
				
		if (empty($value) && $field['IS_REQUIRED'] === 'Y' && $field['PROPERTY_TYPE'] != 'F')
		{
			$errors[] = $field['CODE'];
			if (!empty($MESS[$arParams['CODE'].'-FIELD_NOT_FILL']))
			{
				$errorsTexts[$field['CODE']] = str_replace('##FIELD##', $field['NAME'], $MESS[$arParams['CODE'].'-FIELD_NOT_FILL']);	
			}
			else
			{
				$errorsTexts[$field['CODE']] = str_replace('##FIELD##', $field['NAME'], GetMessage("FIELD_NOT_FILL"));				
			}
		}
		
		if (!empty($value) && $field['PROPERTY_TYPE'] == 'S' && $field['USER_TYPE'] == 'onlycode-email' && !check_email($value))
		{
			$value = '';
			
			$errors[] = $field['CODE'];
			if (!empty($MESS[$arParams['CODE'].'-EMAIL_NOT_VALID']))
			{
				$errorsTexts[$field['CODE']] = $MESS[$arParams['CODE'].'-EMAIL_NOT_VALID'];	
			}
			else
			{
				$errorsTexts[$field['CODE']] = GetMessage("EMAIL_NOT_VALID");				
			}
		}
		
		$arResult['FIELDS'][$key]['VALUE'] = $value;
		
		if ($field['PROPERTY_TYPE'] == 'L' && !empty($field['VARIANTS'][$value]))
		{
			$arResult['FIELDS'][$key]['VALUE_ENUM'] = $field['VARIANTS'][$value]['VALUE'];
		}
	}
	
	if ($arParams['CAPTCHA'] == 'Y')
	{
		if (empty($vars["captcha_word"]))
		{
			$errors[] = 'captcha_word';
			if (!empty($MESS[$arParams['CODE'].'-FIELD_NOT_FILL']))
			{
				$errorsTexts['captcha_word'] = str_replace('##FIELD##', "Captcha", $MESS[$arParams['CODE'].'-FIELD_NOT_FILL']);	
			}
			else
			{
				$errorsTexts['captcha_word'] = str_replace('##FIELD##', "Captcha", GetMessage("FIELD_NOT_FILL"));				
			}
		}
		elseif (!$APPLICATION->CaptchaCheckCode($vars["captcha_word"], $vars["captcha_code"]))
		{
			$errors[] = 'captcha_word';
			if (!empty($MESS[$arParams['CODE'].'-CAPTCHA_NOT_VALID']))
			{
				$errorsTexts['captcha_word'] = $MESS[$arParams['CODE'].'-CAPTCHA_NOT_VALID'];	
			}
			else
			{
				$errorsTexts['captcha_word'] = GetMessage("CAPTCHA_NOT_VALID");				
			}
		}
	}
//	deb($arResult['FIELDS']);
	if (!empty($errors))
	{
		$arResult['RETURN'] = array(
			    'RESULT' => 'error',
			    'ERRORS' => $errors,
			    'ERRORSTEXT' => $errorsTexts
		);
		if (in_array('captcha_word', $errors))
		{
			$arResult['RETURN']['CAPTCHA'] = $APPLICATION->CaptchaGetCode();
		}
		if ($arParams['RETURN_TYPE'] == 'html')
		{
			
		}
		else
		{			
			echo json_encode($arResult['RETURN']);
			exit;
		}
	}
	else
	{
		if (!empty($arParams['EVENT']))
		{
			$files_to_mail = array();
			$to_mail = array();
			
			foreach ($arResult['FIELDS'] as $key => $field)
			{
				$to_mail['TITLE_OF_'.$field['CODE']] = $field['NAME'];
				if ($field['PROPERTY_TYPE'] == 'F')
				{
					$file_name = $_FILES[$arParams['CODE']]['name'][$field['CODE']];
					$file_ext = explode('.', $file_name);
					$file_ext = strtolower($file_ext[count($file_ext)-1]);

					$file = $_FILES[$arParams['CODE']]['tmp_name'][$field['CODE']];
					if (!empty($file) && in_array($file_ext, array(	'jpg', 'jpeg', 'gif', 'png', 
											'bmp', 'doc', 'docx', 'rtf', 'txt', 
											'xls', 'xlsx', 'pdf')))
					{
						$files_to_mail[] = CFile::SaveFile(CFile::MakeFileArray($file));
					}
					
				}
				if ($field['PROPERTY_TYPE'] == 'E' && !empty($field['VALUE']))
				{
					$elem = CIBlockElement::GetByID($field['VALUE']);
					if($elem = $elem->GetNextElement())
					{
						$props = $elem->GetProperties();
						$elem = $elem->GetFields();
					
						$to_mail['PAGE_OF_'.$field['CODE']] = 'http://'.$_SERVER['HTTP_HOST'].$elem['DETAIL_PAGE_URL'];
						$to_mail['ELEMENT_NAME_OF_'.$field['CODE']] = $elem['NAME'];
						
						
//						if ($field['CODE'] == 'PRODUCT' && empty($to_mail['GOOD_NAME'])
//						|| $field['CODE'] == 'OFFER')
//						{
//							$to_mail['GOOD_NAME'] = $elem['NAME'];
//							$to_mail['GOOD_PAGE'] = 'http://'.$_SERVER['HTTP_HOST'].$elem['DETAIL_PAGE_URL'];
//							if (!empty($price))
//							{
//								$to_mail['GOOD_PRICE'] = $price['DISCOUNT_PRICE'];
//							}
//							$to_mail['GOOD_PRESENCE_TYPE'] = $to_mail['ELEMENT_PRESENCE_TYPE_OF_'.$field['CODE']];
//						}
					}
				}
				
				
				if ($field['PROPERTY_TYPE'] == 'L')
				{
					if (!empty($field['VARIANTS'][$field['VALUE']]))
					{
						$to_mail[$field['CODE']] = $field['VALUE_ENUM'];
					}
					else
					{
						$to_mail[$field['CODE']] = "";
					}
				}
				else
				{
					$to_mail[$field['CODE']] = $field['VALUE'];
				}
			}
			
			foreach ($arResult['FIELDS_FULL'] as $key => $field)
			{
				if (!isset($to_mail[$field['CODE']]))
				{
					$to_mail[$field['CODE']] = '';
					$to_mail['TITLE_OF_'.$field['CODE']] = $field['NAME'];
				}
			}
			
			
			CEvent::Send($arParams['EVENT'], SITE_ID, $to_mail, "Y", "", $files_to_mail);
		}
		
		$PROPERTY_VALUES = array();
		foreach ($arResult['FIELDS'] as $key => $field)
		{
			if ($field['PROPERTY_TYPE'] == 'F')
			{
				
				$file_name = $_FILES[$arParams['CODE']]['name'][$field['CODE']];
				$file_ext = explode('.', $file_name);
				$file_ext = strtolower($file_ext[count($file_ext)-1]);
				
				$file = $_FILES[$arParams['CODE']]['tmp_name'][$field['CODE']];
				if (!empty($file) && in_array($file_ext, array(	'jpg', 'jpeg', 'gif', 'png', 
										'bmp', 'doc', 'docx', 'rtf', 'txt', 
										'xls', 'xlsx', 'pdf')))
				{
					$PROPERTY_VALUES[$field['ID']] = CFile::MakeFileArray($file);
				}
			}
			elseif ($field['PROPERTY_TYPE'] == 'S' && $field['USER_TYPE'] == 'HTML')
			{
				$PROPERTY_VALUES[$field['ID']] = array(array("TYPE"=>"TEXT", "TEXT"=>$field["VALUE"]));
//				
			}
			else
			{
				$PROPERTY_VALUES[$field['ID']] = $field["VALUE"];
			}
		}
		
		global $emptyName;
		$emptyName = false;
		
		
		global $AM_QUESTIONS_IBLOCK_IDS;
		if (in_array($arParams['IBLOCK_ID'], $AM_QUESTIONS_IBLOCK_IDS))
		{
			$emptyName = true;
		}
		if ($arParams['IBLOCK_ID'] == CONST_IBLOCK_ID_booking_orders)
		{
			$emptyName = true;
		}
		
		$cibe = new CIBlockElement;
		$add = $cibe->Add(array(
			'IBLOCK_ID' => $arParams['IBLOCK_ID'],
			'NAME' => "Заполнение формы",
			'CODE' => randString(7, array("abcdefghijklnmopqrstuvwxyz","ABCDEFGHIJKLNMOPQRSTUVWXYZ","0123456789")),
			"ACTIVE" => $arParams['ADD_ACTIVE'],
			"ACTIVE_FROM" => $arParams['ADD_ACTIVE_FROM']=='Y'? date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), time()):'',
			"PROPERTY_VALUES" => $PROPERTY_VALUES
		));
		
		if (empty($add))
		{
			deb($cibe->LAST_ERROR, false);
		}
		else
		{
		
			$arResult['RETURN'] = array(
					'RESULT' => 'success'
			);
			if ($arParams['RETURN_TYPE'] == 'html')
			{

			}
			else
			{
				echo json_encode($arResult['RETURN']);
				exit;
			}
			
			if (!empty($arParams['AJAX_SEND']) && $arParams['AJAX_SEND']==='N')
			{
				LocalRedirect($APPLICATION->GetCurPage().'?success=Y&c='.$arParams['CODE']);
			}
		}
		
	}
}

if (!empty($_GET['success']) && !empty($_GET['c']) && $_GET['success']=='Y' && $_GET['c']== $arParams['CODE'])
{
	$arResult['RETURN'] = array(
			'RESULT' => 'success'
	);
}

$this->IncludeComponentTemplate();