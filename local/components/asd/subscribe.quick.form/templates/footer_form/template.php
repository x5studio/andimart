<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

if (method_exists($this, 'setFrameMode')) {
	$this->setFrameMode(true);
}

if ($arResult['ACTION']['status']=='error') {
	ShowError($arResult['ACTION']['message']);
} elseif ($arResult['ACTION']['status']=='ok') {
	ShowNote($arResult['ACTION']['message']);
}
?><div id="asd_subscribe_res" style="display: none;"></div>



<div class="subscribe-form">
	<div class="row row--vcentr b-list">
		<div class="col-md-5 b-list__item">
			<div class="subscribe-form__icon">
				<img src="<?=SITE_TEMPLATE_PATH?>/static/images/stuff/mail.svg?v=9fd317b0" width="50" height="38" alt="mail">
			</div>
			<div class="subscribe-form__text">Подпишись, чтобы узнавать о всех новинках, акциях, конкурсах!</div>
		</div>
		<div class="col-md-7 b-list__item">
			<form id="asd_subscribe_form" class="js-subscribe-form" novalidate="novalidate" action="<?= POST_FORM_ACTION_URI?>">
				<!-- js обрабатывающий форму лежит в src/scripts/app/forms/subscribe-form.js там уже прописан ожидаемый ответ от сервера и сделан вывод поп-апов-->
				<input type="hidden" name="asd_subscribe" value="Y" />
				<input type="hidden" name="charset" value="<?= SITE_CHARSET?>" />
				<input type="hidden" name="site_id" value="<?= SITE_ID?>" />
				<input type="hidden" name="asd_rubrics" value="<?= $arParams['RUBRICS_STR']?>" />
				<input type="hidden" name="asd_format" value="<?= $arParams['FORMAT']?>" />
				<input type="hidden" name="asd_show_rubrics" value="<?= $arParams['SHOW_RUBRICS']?>" />
				<input type="hidden" name="asd_not_confirm" value="<?= $arParams['NOT_CONFIRM']?>" />
				<input type="hidden" name="asd_key" value="<?= md5($arParams['JS_KEY'].$arParams['RUBRICS_STR'].$arParams['SHOW_RUBRICS'].$arParams['NOT_CONFIRM'])?>" />

				<div class="input-group">
					<div class="input-group__item input-group__item--grow">
						<input id="dasdsa" class="input-text"  name="asd_email"  type="email" placeholder="E-mail">
					</div>
					<div class="input-group__item">
						<button class="btn" type="submit"  name="asd_submit" id="asd_subscribe_submit" value="<?=GetMessage("ASD_SUBSCRIBEQUICK_PODPISATQSA")?>" >
							<span>Подписаться</span>
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
