<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

CModule::IncludeModule("iblock");

$arr = explode("?",$_SERVER["REQUEST_URI"]);
$url = $arr[0];

//$arSelect = Array("ID", "IBLOCK_ID","IBLOCK_SECTION_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
//$arFilter = Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y","!PROPERTY_NOTSHOW"=>false,"PROPERTY_URL"=>$url);
//$show = true;
$arResult["ITEMS"] = array();
//$res = CIBlockElement::GetList(Array('sort' => 'asc'), $arFilter, false, false, $arSelect);
//while($ob = $res->GetNextElement()){
//    $show = false;
//}
//deb($arResult, false);
//if( $show ){
    $url_array = explode("/",$url);
    unset($url_array[count($url_array)-1]);

    while(count($url_array)>0){

        $new_url = implode("/",$url_array)."/";

        $arSelect = Array("ID", "IBLOCK_ID","IBLOCK_SECTION_ID", "NAME","PREVIEW_TEXT","DETAIL_TEXT","PREVIEW_PICTURE","DETAIL_PICTURE", "DATE_ACTIVE_FROM","PROPERTY_*");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
        $arFilter = Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y","PROPERTY_URL"=> $new_url );
        $arResult["ITEMS"] = array();
        $res = CIBlockElement::GetList(Array('sort' => 'asc'), $arFilter, false, false, $arSelect);
        while($ob = $res->GetNextElement()){

            $arFields = $ob->GetFields();
            $arItem =$arFields;
            $arProps = $ob->GetProperties();
            $arItem["PROPS"] = $arProps;
            $arResult["ITEMS"][]   = $arItem;
        }
        if(count($arResult["ITEMS"])>0)
            break;
        unset($url_array[count($url_array)-1]);

    }
//}
//deb($arResult["ITEMS"], false);
$arResult["SECTION"] = array();
if (!empty($arResult["ITEMS"]))
{
	$section = $arResult["ITEMS"][0]['IBLOCK_SECTION_ID'];
	if (!empty($section))
	{
		$arResult["SECTION"] = CIBlockSection::GetList(array(), array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], 'ID' => $section), false, array('*', 'UF_SHOW_SLIDER'));
		$arResult["SECTION"] = $arResult["SECTION"]->Fetch();
	}
}

$this->IncludeComponentTemplate();