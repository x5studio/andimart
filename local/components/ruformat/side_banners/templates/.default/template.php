<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();
?>
<?
if(count($arResult["ITEMS"])){
	?>
	<? if (empty($arResult['SECTION']) || !empty($arResult['SECTION']['UF_SHOW_SLIDER'])) { ?>
	<div class="aside-promo">
		<div id="js-aside-carousel" class="aside-promo__content">
			<?

			foreach($arResult["ITEMS"] as $key=>$arItem){

				?>
				<div class="aside-promo__slide" style="background-color:<?=$arItem["PROPS"]["BGCOLOR"]["~VALUE"]?>;">
					<a class="aside-promo__link" href="<?=$arItem['PROPS']['LINKS']['VALUE']?>">
						<div class="aside-promo__title"><?=$arItem["NAME"]?></div>
						<div class="aside-promo__text"><?=$arItem["PREVIEW_TEXT"]?></div>
						<div class="aside-promo__img">
                            <?
                                //30698 x5 SP: for pagespeed - сжимаем картинки в боковом слайдере
                                $file = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width'=>600, 'height'=>450), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                                $src = $file["src"];
                                if(!$src){
                                    $src = CFile::GetPath($arItem["PREVIEW_PICTURE"]);
                                }
                                //!30698 x5 SP
                            ?>
                            <img src="<?=$src?>" alt="" />
						</div>
					</a>
				</div>
				<?
			}
			?>

		</div>
		<div id="js-aside-carousel-dots" class="aside-promo__dots"></div>
	</div>
	<? } else { ?><div class="aside-promo">
		     <div class="aside-promo__items">
			<? foreach($arResult["ITEMS"] as $key=>$arItem){ ?>
				<a class="aside-promo__item" href="<?=$arItem['PROPS']['LINKS']['VALUE']?>"><?=$arItem["~PREVIEW_TEXT"]?>
				</a>
			<? } ?>
		     </div>
		   </div>
	<? } ?>
<?

}
?>
