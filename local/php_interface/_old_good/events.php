<?php
define("SITE_SERVER_PROTOCOL", (CMain::IsHTTPS()) ? "https://" : "http://");
define("DEFAULT_DOMAIN", "andimart.ru");
define("CURRENT_SERVER_NAME", SITE_SERVER_PROTOCOL.$_SERVER["SERVER_NAME"]);


AddEventHandler("main", "OnBeforeProlog", "SetPageProps");

function SetPageProps(){
	global $APPLICATION;

	if (strpos($APPLICATION->GetCurPage(), '/bitrix/admin/') === false) {

	CModule::IncludeModule("iblock");
	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
	$arFilter = Array("ID"=>PAGE_SETTIGS_ELEMENT_ID, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	if($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();
		//p($arFields);
		$arProps = $ob->GetProperties();
		$cur_page_array = explode("?",$_SERVER["REQUEST_URI"]);
		$cur_page = $cur_page_array[0];
		//p($arProps);
		if(in_array($cur_page ,$arProps["PAGES"]["VALUE"]))
			define("PAGE_TYPE","reviews");
		if(in_array($cur_page ,$arProps["H1"]["VALUE"]))
			define("NOT_SHOW_H1","Y");
		if(in_array($cur_page ,$arProps["LEFTMENU"]["VALUE"]))
		{
			define("SHOW_LEFT_MENU","Y");
		}
	}

	if(strpos($_SERVER["REQUEST_URI"],"/articles/")
		|| strpos($_SERVER["REQUEST_URI"],"/otzyvy_o_magazine/")
		|| strpos($_SERVER["REQUEST_URI"],"/akcii_statyi/")
		|| strpos($_SERVER["REQUEST_URI"],"/masters/")
		){
		define("NO_WYSIWYG","Y");
	}
	if(strpos($_SERVER["REQUEST_URI"],"catalog/") && !strpos($_SERVER["REQUEST_URI"],"/brands/") && !strpos($_SERVER["REQUEST_URI"],"/calcs/") && !strpos($_SERVER["REQUEST_URI"],"/search/")){
		$arr = explode("/",$_SERVER["REQUEST_URI"]);
		//p($arr);

		$code = $arr[count($arr)-2];
		if($code){
			$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","CODE");
			$arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID, "CODE"=>$code,"ACTIVE_DATE"=>"Y");
			$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount"=>1), $arSelect);
			if($ob = $res->GetNextElement())
			{
				 $arFields = $ob->GetFields();
				define("PRODUCT_CARD","Y");
				define("NOT_SHOW_H1","Y");
			}else{
				define("CATALOG_CATEGORY","Y");
				define("SHOW_LEFT_MENU","Y");
			}
		}else{
			define("CATALOG_CATEGORY","Y");
			define("SHOW_LEFT_MENU","Y");
		}
	}
	if(preg_match('/brands\/$/ui', $APPLICATION->GetCurPage()))
	{
//		define("CATALOG_CATEGORY","Y");
		define("SHOW_LEFT_MENU","N");
	}
	elseif ($APPLICATION->GetCurPage() == '/catalog/calcs/')
	{
		define("SHOW_LEFT_MENU","N");
		define("CATALOG_CALCS","Y");
		define("NOT_SHOW_H1","Y");
	}
	elseif (strpos($APPLICATION->GetCurPage(), '/brands/') !== false)
	{
		define("SHOW_LEFT_MENU","Y");
		define("CATALOG_BRAND","Y");
		define("NOT_SHOW_H1","Y");
	}
	elseif (strpos($APPLICATION->GetCurPage(), '/calcs/') !== false)
	{
				define("CATALOG_CATEGORY","Y");
				define("SHOW_LEFT_MENU","Y");
	}
	elseif (strpos($APPLICATION->GetCurPage(), '/search/') !== false)
	{
		define("SHOW_LEFT_MENU","N");
	}
	//echo PRODUCT_CARD."!";
	}
}

AddEventHandler("main", "OnBeforeProlog", "SetLocation");

function SetLocation(){

	#if(empty($_SERVER['REQUEST_URI'])){
	#	print_r($_SERVER);
	#	die();
	#}

	if(stripos($_SERVER['SCRIPT_NAME'], 'acrit') !== false) {
		return false;
	}


	//var_dump(CHK_EVENT);
	if(CHK_EVENT===true){
			return false;
	}

	//check for YandexBot/3.0
    if(strpos($_SERVER['HTTP_USER_AGENT'], 'http://yandex.com/bots') > 0) {
        return false;
    }

    CModule::IncludeModule("iblock");
    if($_GET["not_show_modal"]=="Y"){
        $_SESSION["SHOW_CITIES_MODAL"] = "N";
        //echo $_SESSION["SHOW_CITIES_MODAL"];
        die();
    }
    if(!$_SESSION["PEK_CURRENT_CITY_ID"]/* || true*/){
		Bitrix\Main\Loader::includeModule("altasib.geoip");
		$ip = ALX_GeoIP::GetAddr('');
        if (!empty($_SESSION["GEOIP"]["city"])) {
            //$_SESSION["PEK_CURRENT_CITY_NAME"] = $_SESSION["GEOIP"]["city"];
            $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
            $arFilter = Array("NAME"=>$_SESSION["GEOIP"]["city"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "IBLOCK_ID"=>CITIES_IBLOCK_ID);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount"=>1), $arSelect);
            while($ob = $res->GetNextElement())
            {
                $arFields = $ob->GetFields();
                $_GET["set_city"]= $arFields["ID"];
                if ($arFields["PROPERTY_DOMAIN_VALUE"]) $url = $arFields["PROPERTY_DOMAIN_VALUE"].".".DEFAULT_DOMAIN;
            }
        }
        $domains = array();
        $cities = array();
        $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_DOMAIN");
        $arFilter = Array( "IBLOCK_ID"=>CITIES_IBLOCK_ID,"ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        while($ob = $res->GetNextElement())
        {
           $arFields = $ob->GetFields();
           $cities [$arFields["NAME"]] = $arFields;
           if ($arFields["PROPERTY_DOMAIN_VALUE"]) $domains[$arFields["PROPERTY_DOMAIN_VALUE"]] = $arFields["NAME"];
        }
//		echo "!!!!";
//		p($cities,true);
//		p($_SESSION["GEOIP"],true);
//		if(is_array($cities[$_SESSION["GEOIP"]["city"]]) && !empty($cities[$_SESSION["GEOIP"]["city"]])){
//			$_SESSION["PEK_CURRENT_CITY_NAME"] = $cities[$_SESSION["GEOIP"]["city"]]["NAME"];
//			$_SESSION["PEK_CURRENT_CITY_ID"] = $cities[$_SESSION["GEOIP"]["city"]]["ID"];
//			$_SESSION["SHOW_CITIES_MODAL"] = "Y";
//		}else{
//			$_SESSION["PEK_CURRENT_CITY_NAME"] = DEFAULT_CITY_NAME;
//			$_SESSION["PEK_CURRENT_CITY_ID"] = DEFAULT_CITY_ID;
//			$_SESSION["SHOW_CITIES_MODAL"] = "Y";
//		}

        $str = explode('.', $_SERVER["SERVER_NAME"]);
        if (count($str)>2 && isset($domains[$str[0]])){
            $_SESSION["PEK_CURRENT_CITY_NAME"] = $cities[$domains[$str[0]]]["NAME"];
            $_SESSION["PEK_CURRENT_CITY_ID"] = $cities[$domains[$str[0]]]["ID"];
            $_SESSION["SHOW_CITIES_MODAL"] = "Y";
        }
        else{
            $_SESSION["PEK_CURRENT_CITY_NAME"] = DEFAULT_CITY_NAME;
            $_SESSION["PEK_CURRENT_CITY_ID"] = DEFAULT_CITY_ID;
            $_SESSION["SHOW_CITIES_MODAL"] = "Y";
            $url = DEFAULT_DOMAIN;
        }

    }
//    if (!empty($_SESSION["GEOIP"]["city"])) {
//        //$_SESSION["PEK_CURRENT_CITY_NAME"] = $_SESSION["GEOIP"]["city"];
//        $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
//        $arFilter = Array("NAME"=>$_SESSION["GEOIP"]["city"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "IBLOCK_ID"=>CITIES_IBLOCK_ID);
//        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount"=>1), $arSelect);
//        while($ob = $res->GetNextElement())
//        {
//            $arFields = $ob->GetFields();
//            $_GET["set_city"]= $arFields["ID"];
//            if ($arFields["PROPERTY_DOMAIN_VALUE"]) $url = $arFields["PROPERTY_DOMAIN_VALUE"].".".DEFAULT_DOMAIN;
//        }
//    }
    if($_GET["set_city_name"]){ //если устаналиваем по названию
        $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
        $arFilter = Array("NAME"=>$_GET["set_city_name"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "IBLOCK_ID"=>CITIES_IBLOCK_ID);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount"=>1), $arSelect);
        while($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();
            $_GET["set_city"]= $arFields["ID"];
            if ($arFields["PROPERTY_DOMAIN_VALUE"]) $url = $arFields["PROPERTY_DOMAIN_VALUE"].".".DEFAULT_DOMAIN;
        }

    }
    if($_GET["set_city"]){
        $res = CIBlockElement::GetByID($_GET["set_city"]);
        if($ar_res = $res->GetNext()){
            $_SESSION["PEK_CURRENT_CITY_NAME"] = $ar_res["NAME"];
            $_SESSION["PEK_CURRENT_CITY_ID"] = $ar_res["ID"];
            $_SESSION["SHOW_CITIES_MODAL"] = "N";

        }else{
            $_SESSION["SHOW_CITIES_MODAL"] = "Y";
        }
        $arSelect = Array("ID", "NAME", "PROPERTY_DOMAIN");
        $arFilter = Array("IBLOCK_ID"=>CITIES_IBLOCK_ID, "ID"=>$_GET["set_city"]);
        $db_res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount"=>1), $arSelect);
        if($obj = $db_res->GetNextElement())
        {
            $arField = $obj->GetFields();
            //var_dump($arField);
            if ($arField["PROPERTY_DOMAIN_VALUE"]) {
                $url = $arField["PROPERTY_DOMAIN_VALUE"].".".DEFAULT_DOMAIN;
            }
            else $url = DEFAULT_DOMAIN;
        }

    }
    if ($_SERVER["SERVER_NAME"] != $url && $url) {
        $url = SITE_SERVER_PROTOCOL.$url.$_SERVER['REQUEST_URI'];
        LocalRedirect($url, true, "307 Temporary Redirect");
    }

}


