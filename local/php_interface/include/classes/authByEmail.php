<?php

use Bitrix\Main\EventManager;
$eventManager = EventManager::getInstance();

class authByEmail
{
	public static function OnBeforeUserRegister(&$arFields)
	{
	    $arFields["LOGIN"] = $arFields["EMAIL"];
	}

	public static function OnBeforeUserLogin(&$arFields)
	{
	    $filter = Array("=EMAIL" => $arFields["LOGIN"]);
	    $rsUsers = CUser::GetList(($by = "LAST_NAME"), ($order = "asc"), $filter);
	    if ($user = $rsUsers->GetNext()) {
		$arFields["LOGIN"] = $user["LOGIN"];
	    }
	}
}

$eventManager->addEventHandler(
    'main',
    'OnBeforeUserLogin',
    array('authByEmail', 'OnBeforeUserLogin')
);
$eventManager->addEventHandler(
    'main',
    'OnBeforeUserRegister',
    array('authByEmail', 'OnBeforeUserRegister')
);