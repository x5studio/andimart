<?php
AddEventHandler('main', 'OnEndBufferContent', 'controller404', 1001);
function controller404(&$content) {
   if(defined('ERROR_404') && ERROR_404 == 'Y') {
   		\Bitrix\Main\Data\StaticHtmlCache::getInstance()->markNonCacheable();
   		header("HTTP/1.1 404 Not Found");
      	$content = file_get_contents($_SERVER["DOCUMENT_ROOT"].'/404.php');
      	return false;
   }
}

AddEventHandler("iblock", "OnAfterIBlockElementAdd", "OnAfterIBlockElementAddHandler");
    // создаем обработчик события "OnAfterIBlockElementAdd"
    function OnAfterIBlockElementAddHandler(&$arFields)
    {
        if($arFields["IBLOCK_ID"]==7) {//акции,статьи
        	//проишем дату создания для ссылок в external
        	$el = new CIBlockElement;
			$el->Update($arFields['ID'], array("XML_ID"=>date("Y-m-d")));
        }
    }
