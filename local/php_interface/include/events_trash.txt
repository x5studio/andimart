
\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    "search",
    "BeforeIndex",
    array(
        "SearchEvents",
        "BeforeIndex"
    )
);

//\Bitrix\Main\EventManager::getInstance()->addEventHandler(
//    "search",
//    "OnReIndex",
//    array(
//        "SearchEvents",
//        "OnReIndex"
//    )
//);

//\Bitrix\Main\EventManager::getInstance()->addEventHandler(
//    "search",
//    "OnBeforeIndexUpdate",
//    array(
//        "SearchEvents",
//        "OnBeforeIndexUpdate"
//    )
//);

\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    "search",
    "OnAfterIndexAdd",
    array(
        "SearchEvents",
        "OnAfterIndexAdd"
    )
);

class SearchEvents {
    static $count_before = 100;
    static $count_after = 100;

    public static function BeforeIndex($arFields)
    {
        if($arFields['PARAM2'] != 20) return;
        if(--self::$count_before <= 0) return;
        \Bitrix\Main\Diag\Debug::writeToFile('BeforeIndex :');
        \Bitrix\Main\Diag\Debug::writeToFile('$arFields = ');
        \Bitrix\Main\Diag\Debug::writeToFile($arFields);
    }

//    public static function OnReIndex($NS, $oCallback, $callback_method)
//    {
//        \Bitrix\Main\Diag\Debug::writeToFile('OnReIndex');
//    }

//    public static function OnBeforeIndexUpdate($ID, $arFields)
//    {
//        \Bitrix\Main\Diag\Debug::writeToFile('OnBeforeIndexUpdate');
//    }

    public static function OnAfterIndexAdd($ID, $arFields)
    {
        if($arFields['PARAM2'] != 20) return;
        if(--self::$count_after <= 0) return;
        \Bitrix\Main\Diag\Debug::writeToFile('OnAfterIndexAdd :');
        \Bitrix\Main\Diag\Debug::writeToFile('$ID = ');
        \Bitrix\Main\Diag\Debug::writeToFile($ID);
        \Bitrix\Main\Diag\Debug::writeToFile('$arFields = ');
        \Bitrix\Main\Diag\Debug::writeToFile($arFields);
    }
}
