<?php


use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\EventManager;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class onlycodePropertyEmail
{
    function GetUserTypeDescription()
    {
        return array(
            'PROPERTY_TYPE'        => 'S',
            'USER_TYPE'            => 'onlycode-email',
            'DESCRIPTION'          => 'Email',
            'GetPropertyFieldHtml' => array(__CLASS__, "GetPropertyFieldHtml"),
        );
    }

    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {
        return '<input type="text" size="30" name="' . $strHTMLControlName["VALUE"] . '" value="' . htmlspecialcharsbx($value["VALUE"]) . '">';
    }
}

AddEventHandler("iblock", "OnIBlockPropertyBuildList", array("onlycodePropertyEmail", "GetUserTypeDescription"));


// AddEventHandler("sale", "OnOrderSave", "OnOrderSaveHandler");
// function OnOrderSaveHandler(&$arFields) {
//     // ... process fields
//     //return false;
// }


//AddEventHandler("main", "OnEndBufferContent", "deleteKernelJs");
//AddEventHandler("main", "OnEndBufferContent", "deleteKernelCss");

function deleteKernelJs(&$content)
{
    global $USER, $APPLICATION;
    if ((is_object($USER) && $USER->IsAuthorized()) || strpos($APPLICATION->GetCurDir(), "/bitrix/") !== false) return;
    if ($APPLICATION->GetProperty("save_kernel") == "Y") return;

    $arPatternsToRemove = Array(
        '/<script.+?src=".+?kernel_main\/kernel_main\.js\?\d+"><\/script\>/',
        '/<script.+?src=".+?bitrix\/js\/main\/core\/core[^"]+"><\/script\>/',
        '/<script.+?>BX\.(setCSSList|setJSList)\(\[.+?\]\).*?<\/script>/',
        '/<script.+?>if\(\!window\.BX\)window\.BX.+?<\/script>/',
        '/<script[^>]+?>\(window\.BX\|\|top\.BX\)\.message[^<]+<\/script>/',
    );

    $content = preg_replace($arPatternsToRemove, "", $content);
    $content = preg_replace("/\n{2,}/", "\n\n", $content);

    if (SITE_TEMPLATE_ID == 'main') {
        $content = str_replace('<script', '<script async', $content);
    }
}

function deleteKernelCss(&$content)
{
    global $USER, $APPLICATION;
    if ((is_object($USER) && $USER->IsAuthorized()) || strpos($APPLICATION->GetCurDir(), "/bitrix/") !== false) return;
    if ($APPLICATION->GetProperty("save_kernel") == "Y") return;

    $arPatternsToRemove = Array(
        '/<link.+?href=".+?main\/popup\.css\?\d+"[^>]+>/',
        '/<link.+?href=".+?main\/popup\.min\.css\?\d+"[^>]+>/',
        '/<link.+?href=".+?kernel_main\/kernel_main\.css\?\d+"[^>]+>/',
        '/<link.+?href=".+?bitrix\/js\/main\/core\/css\/core[^"]+"[^>]+>/',
        '/<link.+?href=".+?bitrix\/templates\/[\w\d_-]+\/styles.css[^"]+"[^>]+>/',
        '/<link.+?href=".+?bitrix\/templates\/[\w\d_-]+\/template_styles.css[^"]+"[^>]+>/',
    );

    $content = preg_replace($arPatternsToRemove, "", $content);
    $content = preg_replace("/\n{2,}/", "\n\n", $content);
}

//поиск только по заголовкам
/*AddEventHandler("search", "BeforeIndex", array("SearchHandlers", "BeforeIndexHandler"));
class SearchHandlers
{
   function BeforeIndexHandler($arFields)
   {
      if($arFields["MODULE_ID"] == "iblock")
      {
         if(array_key_exists("BODY", $arFields) && substr($arFields["ITEM_ID"], 0, 1) != "S") // Только для элементов
         {
            $arFields["BODY"] = "";
         }

         if (substr($arFields["ITEM_ID"], 0, 1) == "S") // Только для разделов
         {
            $arFields["BODY"] = "";
            $arFields['TAGS'] = "";
         }
      }

      return $arFields;
   }
}*/


\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    "catalog",
    "OnProductAdd",
    array(
        "XFive\Product\EventHandlers",
        "onAfterAdd",
    )
);

\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    "catalog",
    "OnProductUpdate",
    array(
        "XFive\Product\EventHandlers",
        "onAfterAdd",
    )
);


\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    "search",
    "BeforeIndex",
    array(
        "XFive\Search\EventHandlers",
        "BeforeIndexHandler_synonyms",
    )
);


\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    "sale",
    "OnSaleOrderSaved",
    array(
        "XFive\Order\EventHandlers",
        "onOrderSaved",
    )
);

\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    "sale",
    "OnSaleComponentOrderCreated",
    array(
        "XFive\Order\EventHandlers",
        "onOrderCreated",
    )
);

\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    "main",
    "OnAfterUserAdd",
    array(
        "XFive\User\EventHandlers",
        "onUserAdd",
    )
);


\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    "sale",
    "onSaleAdminOrderInfoBlockShow",
    array(
        "XFive\AdminOrder\EventHandlers",
        "onSaleAdminOrderInfoBlockShow",
    )
);

//\Bitrix\Main\EventManager::getInstance()->addEventHandler(
//    "main",
//    "OnAdminSaleOrderView",
//    array(
//        "XFive\AdminOrder\EventHandlers",
//        "OnAdminSaleOrderView"
//    )
//);

\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    "main",
    "OnAdminListDisplay",
    array(
        "XFive\AdminOrder\EventHandlers",
        "OnAdminListDisplay",
    )
);

\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    "main",
    "OnBeforeEventAdd",
    array(
        "XFive\EventHandlers",
        "OnBeforeEventAddHandler",
    )
);

\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    "catalog",
    "OnSuccessCatalogImport1C",
    array(
        "XFive\EventHandlers",
        "OnSuccessCatalogImport1C",
    )
);

\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    "iblock",
    "OnAfterIBlockSectionUpdate",
    array(
        "XFive\EventHandlers",
        "OnAfterIBlockSectionUpdateHandler",
    )
);

\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    "sale",
    "OnOrderNewSendEmail",
    array(
        "XFive\Sale\EventHandlers",
        "OnOrderNewSendEmailHandler",
    )
);
\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    "main",
    "OnBeforeEventAdd",
    array(
        "XFive\Order\EventHandlers",
        "OnBeforeEventAddHandler",
    )
);


