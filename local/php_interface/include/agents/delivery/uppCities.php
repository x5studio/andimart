<?php

//define const_IBLOCK_ID_cities

function deliveryPecomUppCities()
{
    CModule::IncludeModule("iblock");
    $cities = array();
    $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","ACTIVE");
    $arFilter = Array("IBLOCK_ID"=>const_IBLOCK_ID_cities);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();
	    $arFields["NAME"] = $arFields["NAME"]=='Орёл'?'Орел':$arFields["NAME"];

        if (mb_stripos($arFields["NAME"], 'москва') !== false) {
            $cities[$arFields["NAME"]]["ACTION"] = "Y";
        } else {
            $cities[$arFields["NAME"]]["ACTION"] = "N";
        }
        $cities[$arFields["NAME"]]["ID"] = $arFields["ID"];
    }
    $json_decode = json_decode (file_get_contents("https://pecom.ru/ru/calc/towns.php"),true);
   // p($json_decode);
    
    $near_cities = array();
    foreach($json_decode as $key=>$value)
    {
        foreach ($value as $k => $v)
        {
            if (abs($k) <= 0) continue;

            if(is_array($cities[$v]) && !empty($cities[$v])){

                $cities[$v]["XML_ID"] = abs($k);
                $cities[$v]["ACTION"] = "Y";
            }else{

                $cities[$v]["XML_ID"] = abs($k);
                $cities[$v]["ACTION"] = "Add";
            }

            if ($key != $v)
            {
                $near_cities[$v] = $cities[$key]["XML_ID"];
            }
        }
    }
//		deb($near_cities);
		
    foreach($cities as  $name=>$action){
	    	    
        if($action["ACTION"]=="Add"){
            $el = new CIBlockElement;
            $arLoadProductArray = Array(
                "NAME"           => $name == 'Орел'?'Орёл':$name,
                "XML_ID"         => $action["XML_ID"],
                "ACTIVE"         => "Y",
                "IBLOCK_ID"      => const_IBLOCK_ID_cities,
            );
            $el_id = $el->Add($arLoadProductArray);
//	    CIBlockElement::SetPropertyValues($el_id, const_IBLOCK_ID_cities, !empty($near_cities[$name])?$near_cities[$name]:$action["XML_ID"], 'SAM_CITY_ID');
        }else{
//	    deb($action);
            $el = new CIBlockElement;
            $arLoadProductArray = Array(
//                "XML_ID"         => $action["XML_ID"],
//                "NAME"           => $name,
                "ACTIVE"         => $action["ACTION"],            // активен
            );
            $el->Update($action["ID"], $arLoadProductArray);
	    
//	    CIBlockElement::SetPropertyValues($action["ID"], const_IBLOCK_ID_cities, !empty($near_cities[$name])?$near_cities[$name]:$action["XML_ID"], 'SAM_CITY_ID');
        }
    }
    
    return 'deliveryPecomUppCities();';
}