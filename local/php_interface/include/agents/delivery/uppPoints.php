<?php

function deliveryPecomUppPoints()
{
	CModule::IncludeModule("iblock");
	
	$cities = array();
	$arSelect = Array("ID", "NAME", "XML_ID");
	$arFilter = Array("IBLOCK_ID"=>const_IBLOCK_ID_cities);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($ob = $res->GetNextElement())
	{
	    $arFields = $ob->GetFields();
	    $cities[$arFields['XML_ID']] = $arFields['ID'];
	}
	
	$cities_points = array();
	$arSelect = Array("ID", "NAME", "XML_ID", "IBLOCK_ID");
	$arFilter = Array("IBLOCK_ID"=>const_IBLOCK_ID_pickpoints, 'PROPERTY_IS_ANDI_POINT' => false);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($arFields = $res->Fetch())
	{
	    $arFields['ACTIVE'] = 'N';
	    $cities_points[$arFields['XML_ID']] = $arFields;
	}
//	deb($cities);
	
	
	// Подключение файла с классом
	require_once(const_DELIVERY_pathto_pek.'pecom_kabinet.php');

	// Создание экземпляра класса
	$sdk = new PecomKabinet(const_DELIVERY_pek_LOGIN, const_DELIVERY_pek_KEY);

	// Вызов метода /branches/all/
	$result = $sdk->call('branches', 'all', array());
	$sdk->close();

	foreach ($result->branches as $branch)
	{
		if (empty($branch->cities)) continue;
		
		foreach ($branch->divisions as $division)
		{
			foreach ($division->warehouses as $warehouse)
			{
				$timeOfWork = $warehouse->timeOfWork;
				if (empty($timeOfWork))
				{
					$timeOfWork = $warehouse->divisionTimeOfWork;
				}
//				if (empty($timeOfWork)) continue;
				
				$worktime = '';
				$worktime_arr = array();
				
				if (!empty($timeOfWork))
				{
					foreach ($timeOfWork as $kw => $w)
					{
						$s = '';
						if (!empty($w->workFrom))
						{
							$s = 'c '.$w->workFrom.' до '.$w->workTo;
						}
						$worktime_arr[FormatDate('D', mktime(0, 0, 0, 0  , $kw+1, 1970))] = $s;
					}

					$lastArr = array();
					$lastTime = '';
					foreach ($worktime_arr as $dt => $time)
					{
						$time = (!empty($time)?$time:'выходной');
						if ($lastTime != $time)
						{
							if (!empty($lastTime))
							{
								$worktime .= (!empty($lastArr)?'<br/>':'').(count($lastArr)>2?$lastArr[0].'-'.$lastArr[count($lastArr)-1]:(count($lastArr)==2?$lastArr[0].', '.$lastArr[1]:$lastArr[0]));
								$worktime .= ' — '.$lastTime;
							}
							$lastTime = $time;
							$lastArr = array($dt);
						}
						else
						{
							$lastArr[] = $dt;
						}					
					}
					$worktime .= (!empty($lastArr)?'<br/>':'').(count($lastArr)>2?$lastArr[0].'-'.$lastArr[count($lastArr)-1]:(count($lastArr)==2?$lastArr[0].', '.$lastArr[1]:$lastArr[0]));
					$worktime .= ' — '.$lastTime;
				}
				
				if (empty($cities[$branch->bitrixId]))
				{
//					deb($branch);
//					$sdk = new PecomKabinet(const_DELIVERY_pek_LOGIN, const_DELIVERY_pek_KEY);
//
//					// Вызов метода /branches/all/
//					$branchresult = $sdk->call('branches', 'FINDBYID', array('id' => 58718));
//					$sdk->close();
//					
//					deb($branchresult);
//				}
//				else
//				{
//					
////					deb($branch->cities);
				}
				
				$cities_points[$warehouse->id] = array_merge(
					!empty($cities_points[$warehouse->id])?$cities_points[$warehouse->id]:array(),
					array(
					'IBLOCK_ID' => const_IBLOCK_ID_pickpoints,
					'ACTIVE' => empty($cities[$branch->bitrixId])?'N':'Y',
					'XML_ID' => $warehouse->id,
					'NAME' => 'ПЭК ' . $warehouse->divisionName,
					'PROPERTY_VALUES' => array(
						'CITY' => $cities[$branch->bitrixId],
						'ADDRESS' => $warehouse->address,
						'EMAIL' => $warehouse->email,
						'PHONE' => $warehouse->telephone,
						'COORDINATES' => $warehouse->coordinates,
						'WORKTIME' => array('TEXT'=>$worktime),
                        'TRANSPORT_COMPANY' => '16215' //ПЭК
					)					
					)
				);
			}
		}
	}
//	deb($cities_points);
	foreach ($cities_points as $cities_point)
	{
		if (!empty($cities_point['ID']))
		{
			$id = $cities_point['ID'];
			unset($cities_point['ID']);
			
			$el = new CIBlockElement;
			$el->Update($id, $cities_point);
		}
		else
		{
			$el = new CIBlockElement;
			$el_id = $el->Add($cities_point);
		}
	}
	
	// Вывод результата
	
	return 'deliveryPecomUppPoints();';
}