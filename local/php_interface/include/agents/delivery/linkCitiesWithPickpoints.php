<?php

function linkCitiesWithPickpoints()
{
	CModule::IncludeModule("iblock");
	CModule::IncludeModule("sale");
	
	
	$cities_points = array();
	$arSelect = Array("ID", "NAME", "XML_ID", "IBLOCK_ID","PROPERTY_CITY");
	$arFilter = Array("IBLOCK_ID"=>const_IBLOCK_ID_pickpoints);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($arFields = $res->Fetch())
	{
	    $cities_points[$arFields['PROPERTY_CITY_VALUE']][] = $arFields['ID'];
	}
	
	$cities = array();
	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "XML_ID", "PROPERTY_SAM_CITY_ID", "PROPERTY_HAS_SAM");
	$arFilter = Array("IBLOCK_ID"=>CITIES_IBLOCK_ID, 'ACTIVE' => 'Y');
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		
		
		if ($arFields['XML_ID'] == $arFields['PROPERTY_SAM_CITY_ID_VALUE'])// || !empty($cities_points[$arFields['ID']]))
		{
			if (empty($arFields['PROPERTY_HAS_SAM_VALUE']))
			{
				CIBlockElement::SetPropertyValues($arFields['ID'], $arFields['IBLOCK_ID'], 14807, 'HAS_SAM');
			}
		}
		else
		{
			if (!empty($arFields['PROPERTY_HAS_SAM_VALUE']))
			{
				CIBlockElement::SetPropertyValues($arFields['ID'], $arFields['IBLOCK_ID'], false, 'HAS_SAM');
			}
		}
	}
	
	return 'linkCitiesWithPickpoints();';
}