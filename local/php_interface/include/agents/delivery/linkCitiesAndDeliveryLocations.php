<?php

//define CITIES_IBLOCK_ID

function deliveryPecomLinkCitiesAndDeliveryLocations()
{
	CModule::IncludeModule("iblock");
	CModule::IncludeModule("sale");
	$cities = array();
	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","ACTIVE");
	$arFilter = Array("IBLOCK_ID"=>CITIES_IBLOCK_ID, 'ACTIVE' => 'Y', 'PROPERTY_LOCATION_ID' => false);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		
		$arFields["NAME"] = explode('(', $arFields["NAME"]);
		$arFields["NAME"] = trim($arFields["NAME"][0]);
	    
		$parameters = array();
		$parameters['filter']['NAME.NAME'] = $arFields["NAME"];
		$parameters['filter']['NAME.LANGUAGE_ID'] = "ru";

		$parameters['limit'] = 1;
		$parameters['select'] = array('*', 'LNAME' => 'NAME.NAME');

		$arVal = Bitrix\Sale\Location\LocationTable::getList( $parameters )->fetch();
		if ( $arVal && strlen( $arVal[ 'LNAME' ] ) > 0 && toLower($arVal[ 'LNAME' ]) == toLower($arFields["NAME"]))
		{
			CIBlockElement::SetPropertyValues($arFields['ID'], $arFields['IBLOCK_ID'], $arVal['CODE'], 'LOCATION_ID');
		}
	}
	
	return 'deliveryPecomLinkCitiesAndDeliveryLocations();';
}