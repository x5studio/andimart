<?php
/**
 * Created by PhpStorm.
 * User: btrx
 * Date: 11.01.2019
 * Time: 10:46
 */

class DpdDeliveryPointsManager
{
    const CITIES_IBLOCK_ID = 3;
    const DELIVERY_POINTS_IBLOCK_ID = 29;

    public static function addPoints($pathToJson)
    {
        /*
            Ключи массива пунктов доставки:

            [A] => Код подразделения
            [B] => Название пункта приёма/выдачи
            [C] => Название страны
            [D] => Город расположения
            [E] => Индекс
            [F] => Адрес
            [G] => Широта
            [H] => Долгота
            [I] => Тип пункта
            [J] => Время работы
            [K] => Прием посылок
            [L] => Вариант оплаты
            [M] => Max габарит
            [N] => Сумма габаритов
            [O] => Максимальный вес посылки
            [P] => Максимальный вес отправления
            [Q] => Наложенный платеж
            [R] => Примерка
            [S] => Описание проезда
        */

        $cities = [];

        $db_res = \CIBlockElement::GetList(
            array(),
            array('IBLOCK_ID' => CITIES_IBLOCK_ID),
            false,
            false,
            array('ID', 'IBLOCK_ID', 'NAME')
        );
        while ($ar_res = $db_res->Fetch()) {
            $cities[$ar_res['ID']] = $ar_res['NAME'];
        }

        $points = json_decode(file_get_contents($pathToJson), true);

        foreach ($points as $point) {
            $el = new CIBlockElement;
            foreach ($cities as $cityId => $cityName) {
                if($point['D'] == $cityName) {
                    $el->Add(
                        array(
                            'IBLOCK_ID' => self::DELIVERY_POINTS_IBLOCK_ID,
                            'ACTIVE' => 'Y',
                            'NAME' => 'DPD ' . $point['B'],
                            'PROPERTY_VALUES' => array(
                                'CITY' => $cityId,
                                'ADDRESS' => $point['F'],
                                'EMAIL' => '',
                                'PHONE' => '',
                                'COORDINATES' => $point['G'] . ', ' . $point['H'],
                                'WORKTIME' =>
                                    array(
                                        "TEXT" => $point['J'],
                                        "TYPE" => "HTML"
                                    ),
                                'TRANSPORT_COMPANY' => '16221' //DPD
                            )
                        )
                    );
                    break;
                }
            }
        }

    }
}