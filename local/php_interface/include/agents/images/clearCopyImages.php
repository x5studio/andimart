<?php

function clearCopyImages() {
	global $DB;
	$strSql = "SELECT
	b_file.ID as fid,
	b_iblock_element.ID as pid,
	b_iblock_element.`NAME`,
	b_file.EXTERNAL_ID
	FROM
	b_iblock_element
	INNER JOIN b_file ON b_iblock_element.DETAIL_PICTURE = b_file.ID
	WHERE
	b_iblock_element.IBLOCK_ID = 20
	ORDER BY
	b_file.EXTERNAL_ID ASC";
	
	$res = $DB->Query($strSql);
	while ($arr=$res->Fetch()) {
		if ($old['EXTERNAL_ID']==$arr['EXTERNAL_ID'] && $arr['EXTERNAL_ID'] && $old) {//уже был такой файл
			CFile::Delete($arr["fid"]);
			$DB->Query("UPDATE b_iblock_element SET `DETAIL_PICTURE`=".$old["fid"]." WHERE ID=".$arr['pid'])->Fetch();
		} else {
			$old = $arr;
		}
	}
	unset($strSql);
	unset($old);
	unset($arr);
	$strSql = "SELECT
	b_file.ID as fid,
	b_iblock_element.ID as pid,
	b_iblock_element.`NAME`,
	b_file.EXTERNAL_ID
	FROM
	b_iblock_element
	INNER JOIN b_file ON b_iblock_element.PREVIEW_PICTURE = b_file.ID
	WHERE
	b_iblock_element.IBLOCK_ID = 20
	ORDER BY
	b_file.EXTERNAL_ID ASC";
	
	$res = $DB->Query($strSql);
	while ($arr=$res->Fetch()) {
		if ($old['EXTERNAL_ID']==$arr['EXTERNAL_ID'] && $arr['EXTERNAL_ID'] && $old) {//уже был такой файл
			CFile::Delete($arr["fid"]);
			$DB->Query("UPDATE b_iblock_element SET `PREVIEW_PICTURE`=".$old["fid"]." WHERE ID=".$arr['pid'])->Fetch();
		} else {
			$old = $arr;
		}
	}
    
    return 'clearCopyImages();';
}