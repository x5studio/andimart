<?php

function deactiveNotCount()
{
	CModule::IncludeModule("iblock");
	
	$elems = CIBlockElement::GetList(
		array(), 
		array('IBLOCK_ID' => const_IBLOCK_ID_catalog, 'ACTIVE' => 'Y', 'CATALOG_QUANTITY' => 0), 
		false, false, array('ID'));
	while ($elem = $elems->Fetch())
	{
		$el = new CIBlockElement;
		$el->Update($elem['ID'], array('ACTIVE' => 'N'));
		\Bitrix\Iblock\PropertyIndex\Manager::updateElementIndex(const_IBLOCK_ID_catalog, $elem['ID']);
	}
	
	return 'deactiveNotCount();';
}