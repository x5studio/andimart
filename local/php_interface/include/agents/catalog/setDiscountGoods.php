<?php

use Bitrix\Main\Context,
	Bitrix\Main\Loader,
	Bitrix\Main\Type\DateTime,
	Bitrix\Currency,
	Bitrix\Catalog,
	Bitrix\Iblock;

function setDiscountGoods()
{
	
	global $APPLICATION;
	global $USER;
	
	CModule::IncludeModule("iblock");
	CModule::IncludeModule("catalog");
	CModule::IncludeModule("sale");
	
	$arParams = array(
		"ACTION_VARIABLE" => "action",
		"ADD_ELEMENT_CHAIN" => "Y",
		"ADD_PICT_PROP" => "MORE_PHOTO",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BASKET_URL" => "/lichniy_cabinet/basket.php",
		"BIG_DATA_RCM_TYPE" => "bestsell",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMMON_ADD_TO_BASKET_ACTION" => "",
		"COMMON_SHOW_CLOSE_POPUP" => "Y",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_ADD_DETAIL_TO_SLIDER" => "N",
		"DETAIL_ADD_TO_BASKET_ACTION" => array(
			0 => "BUY",
		),
		"DETAIL_BACKGROUND_IMAGE" => "-",
		"DETAIL_BRAND_USE" => "N",
		"DETAIL_BROWSER_TITLE" => "-",
		"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
		"DETAIL_DETAIL_PICTURE_MODE" => "IMG",
		"DETAIL_DISPLAY_NAME" => "Y",
		"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
		"DETAIL_META_DESCRIPTION" => "-",
		"DETAIL_META_KEYWORDS" => "-",
		"DETAIL_PROPERTY_CODE" => array(
			0 => "KOLICHESTVO_TOCHEK_VODORAZBORA_SHT",
			1 => "TIP",
			2 => "DIAMETR_SOEDINENIYA_S_METRICHESKOY_REZBOY",
			3 => "DIAMETR_SOEDINENIYA_DYUYM",
			4 => "TIP_1",
			5 => "MATERIAL",
			6 => "VSTROENNYY_VENTILYATOR",
			7 => "VSTROENNYY_TERMOSTAT",
			8 => "DIAMETR_PATRUBKOV_REZBA_DYUYM",
			9 => "DIAMETR_NARUZHNYY_MM",
			10 => "TOLSHCHINA_STENKI_MM",
			11 => "DLINA_M",
			12 => "KHOLODNOE_VODOSNABZHENIE",
			13 => "ARMIRUYUSHCHIY_SLOY",
			14 => "TSVET",
			15 => "MATERIAL_1",
			16 => "SPOSOB_SOEDINENIYA",
			17 => "DIAMETR_PAECHNOGO_SOEDINENIYA_MM",
			18 => "DIAMETR_REZBOVOGO_SOEDINENIYA_DYUYM",
			19 => "DIAMETR_OBZHIMNOGO_SOEDINENIYA_MM",
			20 => "DIAMETR_PRESS_SOEDINENIYA_MM",
			21 => "DIAMETR_NADVIZHNOGO_SOEDINENIYA_MM",
			22 => "TIP_REZBY",
			23 => "TIP_2",
			24 => "PRINTSIP_DEYSTVIYA",
			25 => "POTREBLENIE_TOPLIVA_L_CHAS",
			26 => "MOSHCHNOST_NAGRUZKI_VA",
			27 => "MOSHCHNOST_NAGRUZKI_VT",
			28 => "MINIMALNOE_VKHODNOE_NAPRYAZHENIE_V",
			29 => "MAKSIMALNOE_VKHODNOE_NAPRYAZHENIE_V",
			30 => "VYKHODNOE_NAPRYAZHENIE_V",
			31 => "PODDERZHIVAEMYY_TIP_AKKUMULYATOROV",
			32 => "PODDERZHIVAEMAYA_EMKOST_AKKUMULYATOROV_A_CH",
			33 => "FASOVKA_L",
			34 => "FASOVKA_KG",
			35 => "TIP_NASOSA",
			36 => "PRINTSIP_DEYSTVIYA_1",
			37 => "MOSHCHNOST_KVT",
			38 => "OTAPLIVAEMAYA_PLOSHCHAD_M",
			39 => "TIP_3",
			40 => "SPOSOB_MONTAZHA",
			41 => "KONTUR_GORYACHEGO_VODOSNABZHENIYA",
			42 => "DYMOUDALENIE",
			43 => "KAMERA_SGORANIYA",
			44 => "KOLICHESTVO_TEPLOOBMENNIKOV_SHT",
			45 => "VSTROENNYY_BOYLER",
			46 => "PROIZVODITELNOST_GORYACHEY_VODY_PRI_T_25_S_L_MIN",
			47 => "MAKSIMALNYY_RASKHOD_PRIRODNOGO_GAZA_M_CHAS",
			48 => "DIAMETR_PATRUBKOV_GORYACHEGO_VODOSNABZHENIYA_DYUYM",
			49 => "DIAMETR_GAZOVOGO_PATRUBKA_DYUYM",
			50 => "GARANTIYA_MES",
			51 => "RODINA_BRENDA",
			52 => "STEPEN_AVTONOMNOSTI",
			53 => "NAPRYAZHENIE_ELEKTROPITANIYA_V",
			54 => "VSTROENNYY_TSIRKULYATSIONNYY_NASOS",
			55 => "VSTROENNYY_RASSHIRITELNYY_BAK",
			56 => "ARTIKUL_PROIZVODITELYA",
			57 => "VSTROENNYY_AKKUMULYATOR",
			58 => "VREMYA_SRABATYVANIYA_SEK",
			59 => "TIP_4",
			60 => "TIP_KOLLEKTORNOGO_OBORUDOVANIYA",
			61 => "POTREBLYAEMAYA_MOSHCHNOST_OKHLAZHDENIE_KVT",
			62 => "VNESHNIY_PULT_UPRAVLENIYA",
			63 => "KOMPLEKT_KREPEZHNYKH_ELEMENTOV",
			64 => "TIP_5",
			65 => "TIP_6",
			66 => "OBEM_TOPKI_L",
			67 => "ZAGRUZKA_TOPLIVA",
			68 => "AVTOREGULYATOR_TYAGI",
			69 => "VSTROENNYY_TEN",
			70 => "MAKSIMALNYY_RASKHOD_SZHIZHENNOGO_GAZA_KG_CHAS",
			71 => "ELEKTRICHESKIY_OBOGREV",
			72 => "DIAMETR_PATRUBKOV_GREYUSHCHEGO_KONTURA_DYUYM",
			73 => "OBEM_L",
			74 => "TIP_7",
			75 => "SPOSOB_USTANOVKI",
			76 => "TIP_ROZZHIGA",
			77 => "TIP_BATAREEK",
			78 => "MODULYATSIYA_PLAMENI",
			79 => "MATERIAL_TEPLOOBMENNIKA",
			80 => "MATERIAL_BAKA",
			81 => "VOZMOZHNOST_VSTRAIVANIYA_TENA",
			82 => "KONTUR_RETSIRKULYATSII",
			83 => "DIAMETR_PATRUBKA_RETSIRKULYATSII_DYUYM",
			84 => "VSTROENNYY_TERMOSTAT_1",
			85 => "RASCHETNOE_KOLICHESTVO_POLZOVATELEY",
			86 => "KOLICHESTVO_TENOV_SHT",
			87 => "OSNOVNOY_ISTOCHNIK_PITANIYA",
			88 => "REZERVNYE_ISTOCHNIKI_PITANIYA",
			89 => "BATAREEK_V_KOMPLEKTE_SHT",
			90 => "SOEDINENIE_S_REGULIRUYUSHCHEY_AVTOMATIKOY",
			91 => "SOVMESTIMYY_DIAMETR_TRUBY_MM",
			92 => "TSVET_1",
			93 => "KOMPLEKT_KREPEZHNYKH_ELEMENTOV_1",
			94 => "FASOVKA",
			95 => "DIAMETR_VNUTRENNIY_MM",
			96 => "TOLSHCHINA_STENKI_MM_1",
			97 => "DLINA_M_1",
			98 => "MATERIAL_KONSTRUKTSII",
			99 => "KLASS_ENERGOEFFEKTIVNOSTI",
			100 => "INVERTORNYY",
			101 => "TIP_8",
			102 => "TIP_9",
			103 => "TOPLIVO",
			104 => "KONSTRUKTSIYA",
			105 => "MATERIAL_2",
			106 => "TOLSHCHINA_METALLA_MM",
			107 => "OBEM_L_1",
			108 => "DIAMETR_PATRUBKOV_GREYUSHCHEGO_KONTURA_MM",
			109 => "DIAMETR_PATRUBKOV_NAGREVAEMOGO_KONTURA_DYUYM",
			110 => "DIAMETR_PATRUBKOV_NAGREVAEMOGO_KONTURA_MM",
			111 => "REZHIM_STUPENCHATOY_MOSHCHNOSTI",
			112 => "DYMOKHODNAYA_SISTEMA_V_KOMPLEKTE",
			113 => "TEMPERATURA_SRABATYVANIYA_C",
			114 => "NALICHIE_REZHUSHCHEGO_MEKHANIZMA",
			115 => "MATERIAL_3",
			116 => "FORMA",
			117 => "NALICHIE_POLOCHKI",
			118 => "TIP_PODKLYUCHENIYA",
			119 => "DIAMETR_PODKLYUCHENIYA_DYUYM",
			120 => "TERMOSTAT",
			121 => "MINIMALNAYA_VYSOTA_USTANOVKI_M",
			122 => "RABOCHIY_TOK_A",
			123 => "DLINA_KABELYA_M",
			124 => "PLOSHCHAD_UKLADKI_M",
			125 => "MOSHCHNOST_NA_1M_VT",
			126 => "STABILIZATSIYA_NAPRYAZHENIYA",
			127 => "PREDNAZNACHENIE",
			128 => "TIP_10",
			129 => "SPOSOB_USTANOVKI_1",
			130 => "FORM_FAKTOR",
			131 => "VSTROENNYY_NIPPEL_POD_NASOS",
			132 => "MOSHCHNOST_OKHLAZHDENIYA_KVT",
			133 => "MOSHCHNOST_OBOGREVA_KVT",
			134 => "POTREBLYAEMAYA_MOSHCHNOST_OBOGREV_KVT",
			135 => "UROVEN_SHUMA_DB",
			136 => "MATERIAL_PERVICHNOGO_TEPLOOBMENNIKA",
			137 => "PREDNAZNACHENIE_1",
			138 => "TIP_11",
			139 => "VREMYA_SRABATYVANIYA_SEK_1",
			140 => "KRANOV_V_KOMPLEKTE_SHT",
			141 => "DATCHIKOV_V_KOMPLEKTE_SHT",
			142 => "VOZMOZHNOE_KOLICHESTVO_PODKLYUCHAEMYKH_KRANOV_SHT",
			143 => "VOZMOZHNOE_KOLICHESTVO_PODKLYUCHAEMYKH_DATCHIKOV_S",
			144 => "GORYACHEE_VODOSNABZHENIE",
			145 => "RADIATORNOE_OTOPLENIE",
			146 => "TEPLYY_POL",
			147 => "GLUBINA_POGRUZHENIYA_M",
			148 => "MATERIAL_4",
			149 => "VSTROENNYY_TERMOSTAT_2",
			150 => "VSTROENNYY_TAYMER",
			151 => "DLINA_MATA_M",
			152 => "RUCHNOY_REZHIM",
			153 => "REZHIM_SNA",
			154 => "SAMOOCHISTKA",
			155 => "REZHIM_VLAZHNAYA_UBORKA",
			156 => "TIP_12",
			157 => "UPRAVLENIE",
			158 => "KOEFFITSIENT_POLEZNOGO_DEYSTVIYA_",
			159 => "DIAMETR_DYMOKHODA_MM",
			160 => "DIAMETR_PATRUBKOV_OTOPLENIYA_REZBA_DYUYM",
			161 => "TIP_ZASHCHITNOY_ARMATURY",
			162 => "DIAPAZON_REGULIROVKI_C",
			163 => "TIP_KLAPANA",
			164 => "TIP_13",
			165 => "KOLICHESTVO_KOLLEKTORNYKH_VYKHODOV_SHT",
			166 => "VYSOTA_MM",
			167 => "GLUBINA_MM",
			168 => "SHIRINA_MM",
			169 => "TIP_PODVODKI",
			170 => "DLINA_SM",
			171 => "POVYSHENIE_DAVLENIYA_ATM",
			172 => "ZASHCHITA_OT_SUKHOGO_KHODA",
			173 => "RABOCHAYA_TEMPERATURA_ZHIDKOSTI_C",
			174 => "OSNOVNOE_USTROYSTVO_SLIVA",
			175 => "DOPOLNITELNYE_USTROYSTVA_SLIVA_SHT",
			176 => "KREPEZHNYE_ELEMENTY",
			177 => "DIAMETR_OBZHIMNOGO_SOEDINENIYA_DYUYM",
			178 => "KOLICHESTVO_PATRUBKOV_SHT",
			179 => "MOSHCHNOST_VT",
			180 => "DIAMETR_PATRUBKOV_DYUYM",
			181 => "GLUBINA_MM_1",
			182 => "SHIRINA_MM_1",
			183 => "VYSOTA_MM_1",
			184 => "MEZHOSEVOE_RASSTOYANIE_MM",
			185 => "PODKLYUCHENIE",
			186 => "CHASTOTNOE_REGULIROVANIE",
			187 => "PODEM_M",
			188 => "PROIZVODITELNOST_M_CHAS",
			189 => "MOSHCHNOST_VT_1",
			190 => "MONTAZHNAYA_DLINA_MM",
			191 => "MATERIAL_KORPUSA",
			192 => "KABEL_PITANIYA",
			193 => "DLINA_KABELYA_PITANIYA_M",
			194 => "DIAMETR_PATRUBKOV_REZBA_DYUYM_1",
			195 => "DIAMETR_PATRUBKOV_FLANETS_MM",
			196 => "TIP_14",
			197 => "DIAMETR_PATRUBKOV_OTOPLENIYA_FLANETS_MM",
			198 => "VSTROENNYY_TEPLOOBMENNIK_GORYACHEGO_VODOSNABZHENIY",
			199 => "TIP_SETCHATOGO_FILTRA",
			200 => "OBEM_ML",
			201 => "PROIZVODITELNOST_L_MIN",
			202 => "TIP_ELEMENTA_SISTEMY",
			203 => "TIPORAZMER",
			204 => "KOLICHESTVO_STUPENEY_OCHISTKI_SHT",
			205 => "DIAPAZON_REGULIROVKI_BAR",
			206 => "DAVLENIE_SRABATYVANIYA_BAR",
			207 => "TIP_15",
			208 => "KONSTRUKTSIYA_PECHI",
			209 => "TIP_16",
			210 => "KLEYKAYA_OSNOVA",
			211 => "VID_KABELYA",
			212 => "PRIMENENIE",
			213 => "DIAMETR_SOEDINENIYA_MM",
			214 => "TIP_17",
			215 => "FASOVKA_G",
			216 => "FASOVKA_M",
			217 => "FASOVKA_ML",
			218 => "TIP_18",
			219 => "TIP_OKHLAZHDENIYA",
			220 => "SHUM_RABOTY_NASOSA_DB",
			221 => "VSTROENNYY_GIDROBAK",
			222 => "VSTROENNYY_OBRATNYY_KLAPAN",
			223 => "ZASHCHITA_OT_RABOTY_BEZ_VODY",
			224 => "TIP_NASOSA_1",
			225 => "AVTOMATICHESKIY_ZAPUSK_VYKLYUCHENIE",
			226 => "MATERIAL_TOPKI",
			227 => "MAKSIMALNYY_OBEM_PARNOY_M",
			228 => "PANORAMNAYA_DVERTSA",
			229 => "ZAGRUZKA_TOPLIVA_1",
			230 => "VTORICHNOE_OBRAZOVANIE_PARA",
			231 => "VSTROENNYY_BAK_DLYA_VODY",
			232 => "VOZMOZHNOST_RABOTY_S_GAZOVOY_GORELKOY",
			233 => "VSTROENNAYA_GAZOVAYA_GORELKA",
			234 => "PULT_UPRAVLENIYA",
			235 => "RABOCHAYA_SREDA",
			236 => "REGULIROVKA_POTOKA",
			237 => "OBRATNYY_OSMOS",
			238 => "PREDNAZNACHENIE_2",
			239 => "PROREZINENNYY",
			240 => "S_SHURUPOM_I_DYUBELEM",
			241 => "TSVET_2",
			242 => "MINIMALNAYA_RABOCHAYA_TEMPERATURA_S",
			243 => "MAKSIMALNAYA_RABOCHAYA_TEMPERATURA_S",
			244 => "OSNOVA",
			245 => "PLOSHCHAD_OKHLAZHDENIYA_M",
			246 => "DIAMETR_FLANTSEVOGO_SOEDINENIYA_MM",
			247 => "TIP_19",
			248 => "KOLICHESTVO_TEPLOOBMENNIKOV_SHT_1",
			249 => "TIP_RUKOYATKI",
			250 => "TIP_20",
			251 => "TIP_21",
			252 => "VYNOSNOY_DATCHIK_TEMPERATURY",
			253 => "FUNKTSIYA_PROGRAMMIROVANIYA",
			254 => "PITANIE_TERMOSTATA",
			255 => "NAPRYAZHENIE_RABOCHEE_V",
			256 => "RABOCHAYA_SREDA_1",
			257 => "DIAMETR_KORPUSA_MM",
			258 => "TIP_22",
			259 => "SHKALA_TEMPERATURY_C",
			260 => "SHKALA_DAVLENIYA_BAR",
			261 => "UPRAVLENIE_1",
			262 => "DIAMETR_PATRUBKOV_MM",
			263 => "MONTAZHNAYA_DLINA_MM_1",
			264 => "TIP_SCHETCHIKA",
			265 => "TIP_USTANOVKI",
			266 => "NAPRAVLENIE_DVIZHENIYA_SREDY",
			267 => "MAKSIMALNYY_RASKHOD_M_CHAS",
			268 => "MINIMALNYY_RASKHOD_M_CHAS",
			269 => "RAZEMNYE_SOEDINENIYA",
			270 => "DIAMETR_RAZEMNYKH_SOEDINENIY_REZBA_DYUYM",
			271 => "GLUBINA_VSASYVANIYA_M",
			272 => "PROPUSKNAYA_SPOSOBNOST_M_CHAS",
			273 => "FASOVKA_1",
			274 => "TIP_23",
			275 => "VYNOSNOY_DATCHIK_TEMPERATURY_1",
			276 => "MEZHOSEVOE_RASSTOYANIE_MM_1",
			277 => "KOLICHESTVO_VYKHODOV_SHT",
			278 => "",
		),
		"DETAIL_SET_CANONICAL_URL" => "Y",
		"DETAIL_SET_VIEWED_IN_COMPONENT" => "N",
		"DETAIL_SHOW_BASIS_PRICE" => "Y",
		"DETAIL_SHOW_MAX_QUANTITY" => "Y",
		"DETAIL_USE_COMMENTS" => "N",
		"DETAIL_USE_VOTE_RATING" => "N",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "catalog_PRICE_14",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FILTER_VIEW_MODE" => "VERTICAL",
		"GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
		"GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "3",
		"GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",
		"GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",
		"GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "3",
		"GIFTS_MESS_BTN_BUY" => "Выбрать",
		"GIFTS_SECTION_LIST_BLOCK_TITLE" => "Подарки к товарам этого раздела",
		"GIFTS_SECTION_LIST_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_SECTION_LIST_PAGE_ELEMENT_COUNT" => "3",
		"GIFTS_SECTION_LIST_TEXT_LABEL_GIFT" => "Подарок",
		"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
		"GIFTS_SHOW_IMAGE" => "Y",
		"GIFTS_SHOW_NAME" => "Y",
		"GIFTS_SHOW_OLD_PRICE" => "Y",
		"HIDE_NOT_AVAILABLE" => "Y",
		"IBLOCK_ID" => "20",
		"IBLOCK_TYPE" => "1c_catalog",
		"INCLUDE_SUBSECTIONS" => "Y",
		"INSTANT_RELOAD" => "N",
		"LABEL_PROP" => "-",
		"LINE_ELEMENT_COUNT" => "3",
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"LINK_IBLOCK_ID" => "",
		"LINK_IBLOCK_TYPE" => "",
		"LINK_PROPERTY_SID" => "",
		"LIST_BROWSER_TITLE" => "-",
		"LIST_META_DESCRIPTION" => "-",
		"LIST_META_KEYWORDS" => "-",
		"LIST_PROPERTY_CODE" => array(
			0 => "BREND",
			1 => "",
		),
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_COMPARE" => "Сравнение",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => "forum",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "30",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "WEB-цена",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"SECTIONS_SHOW_PARENT_NAME" => "Y",
		"SECTIONS_VIEW_MODE" => "LIST",
		"SECTION_ADD_TO_BASKET_ACTION" => "ADD",
		"SECTION_BACKGROUND_IMAGE" => "-",
		"SECTION_COUNT_ELEMENTS" => "Y",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_TOP_DEPTH" => "2",
		"SEF_FOLDER" => "/catalog/",
		"SEF_MODE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SHOW_DEACTIVATED" => "N",
		"SHOW_DISCOUNT_PERCENT" => "Y",
		"SHOW_OLD_PRICE" => "Y",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_TOP_ELEMENTS" => "Y",
		"SIDEBAR_DETAIL_SHOW" => "Y",
		"SIDEBAR_PATH" => "",
		"SIDEBAR_SECTION_SHOW" => "Y",
		"TEMPLATE_THEME" => "blue",
		"TOP_ADD_TO_BASKET_ACTION" => "ADD",
		"TOP_ELEMENT_COUNT" => "9",
		"TOP_ELEMENT_SORT_FIELD" => "sort",
		"TOP_ELEMENT_SORT_FIELD2" => "id",
		"TOP_ELEMENT_SORT_ORDER" => "asc",
		"TOP_ELEMENT_SORT_ORDER2" => "desc",
		"TOP_LINE_ELEMENT_COUNT" => "3",
		"TOP_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"TOP_VIEW_MODE" => "SECTION",
		"USE_ALSO_BUY" => "N",
		"USE_BIG_DATA" => "Y",
		"USE_COMMON_SETTINGS_BASKET_POPUP" => "N",
		"USE_COMPARE" => "N",
		"USE_ELEMENT_COUNTER" => "Y",
		"USE_FILTER" => "Y",
		"USE_GIFTS_DETAIL" => "Y",
		"USE_GIFTS_MAIN_PR_SECTION_LIST" => "Y",
		"USE_GIFTS_SECTION" => "Y",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "Y",
		"USE_REVIEW" => "N",
		"USE_SALE_BESTSELLERS" => "Y",
		"USE_STORE" => "N",
		"COMPONENT_TEMPLATE" => "catalog",
		"FILTER_NAME" => "arrFilter",
		"FILTER_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_PRICE_CODE" => array(
		),
		"SEF_URL_TEMPLATES" => array(
			"search" => "search/",
			"sections" => "",
			"section" => "#SECTION_CODE_PATH#/",
			"element" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
			"compare" => "compare.php?action=#ACTION_CODE#",
			"smart_filter" => "#SECTION_CODE_PATH#/filter/#SMART_FILTER_PATH#/apply/",
		),
		"VARIABLE_ALIASES" => array(
			"compare" => array(
				"ACTION_CODE" => "action",
			),
		)
	);
	
/** @global CCacheManager $CACHE_MANAGER */
global $CACHE_MANAGER;
/** @global CIntranetToolbar $INTRANET_TOOLBAR */
global $INTRANET_TOOLBAR;

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

/*************************************************************************
	Processing of received parameters
*************************************************************************/
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = (int)$arParams["IBLOCK_ID"];

$arParams["SECTION_ID"] = (int)$arParams["~SECTION_ID"];
if($arParams["SECTION_ID"] > 0 && $arParams["SECTION_ID"]."" != $arParams["~SECTION_ID"])
{
	if (Loader::includeModule("iblock"))
	{
		Iblock\Component\Tools::process404(
			trim($arParams["MESSAGE_404"]) ?: GetMessage("CATALOG_SECTION_NOT_FOUND")
			,true
			,$arParams["SET_STATUS_404"] === "Y"
			,$arParams["SHOW_404"] === "Y"
			,$arParams["FILE_404"]
		);
	}
	return;
}

if (!isset($arParams["INCLUDE_SUBSECTIONS"]) || !in_array($arParams["INCLUDE_SUBSECTIONS"], array('Y', 'A', 'N')))
	$arParams["INCLUDE_SUBSECTIONS"] = 'Y';
$arParams["SHOW_ALL_WO_SECTION"] = true;
$arParams["SET_LAST_MODIFIED"] = $arParams["SET_LAST_MODIFIED"]==="Y";
$arParams["USE_MAIN_ELEMENT_SECTION"] = $arParams["USE_MAIN_ELEMENT_SECTION"]==="Y";

if (empty($arParams["ELEMENT_SORT_FIELD"]))
	$arParams["ELEMENT_SORT_FIELD"] = "sort";
if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["ELEMENT_SORT_ORDER"]))
	$arParams["ELEMENT_SORT_ORDER"] = "asc";
if (empty($arParams["ELEMENT_SORT_FIELD2"]))
	$arParams["ELEMENT_SORT_FIELD2"] = "id";
if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["ELEMENT_SORT_ORDER2"]))
	$arParams["ELEMENT_SORT_ORDER2"] = "desc";

if(empty($arParams["FILTER_NAME"]) || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
	$arrFilter = array();
}
else
{
	global ${$arParams["FILTER_NAME"]};
	$arrFilter = ${$arParams["FILTER_NAME"]};
	if(!is_array($arrFilter))
		$arrFilter = array();
	elseif (isset($arrFilter['FACET_OPTIONS']) && count($arrFilter) == 1)
		unset($arrFilter['FACET_OPTIONS']);
}

if (empty($arParams["PAGER_PARAMS_NAME"]) || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["PAGER_PARAMS_NAME"]))
{
	$pagerParameters = array();
}
else
{
	$pagerParameters = $GLOBALS[$arParams["PAGER_PARAMS_NAME"]];
	if (!is_array($pagerParameters))
		$pagerParameters = array();
}

$arParams["SECTION_URL"]=trim($arParams["SECTION_URL"]);
$arParams["DETAIL_URL"]=trim($arParams["DETAIL_URL"]);
$arParams["BASKET_URL"]=trim($arParams["BASKET_URL"]);
if($arParams["BASKET_URL"] === '')
	$arParams["BASKET_URL"] = "/lichniy_cabinet/basket.php";

$arParams["ACTION_VARIABLE"]=trim($arParams["ACTION_VARIABLE"]);
if($arParams["ACTION_VARIABLE"] === '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["ACTION_VARIABLE"]))
	$arParams["ACTION_VARIABLE"] = "action";

$arParams["PRODUCT_ID_VARIABLE"]=trim($arParams["PRODUCT_ID_VARIABLE"]);
if($arParams["PRODUCT_ID_VARIABLE"] === '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["PRODUCT_ID_VARIABLE"]))
	$arParams["PRODUCT_ID_VARIABLE"] = "id";

$arParams["PRODUCT_QUANTITY_VARIABLE"]=trim($arParams["PRODUCT_QUANTITY_VARIABLE"]);
if($arParams["PRODUCT_QUANTITY_VARIABLE"] === '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["PRODUCT_QUANTITY_VARIABLE"]))
	$arParams["PRODUCT_QUANTITY_VARIABLE"] = "quantity";

$arParams["PRODUCT_PROPS_VARIABLE"]=trim($arParams["PRODUCT_PROPS_VARIABLE"]);
if($arParams["PRODUCT_PROPS_VARIABLE"] === '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["PRODUCT_PROPS_VARIABLE"]))
	$arParams["PRODUCT_PROPS_VARIABLE"] = "prop";

$arParams["SECTION_ID_VARIABLE"]=trim($arParams["SECTION_ID_VARIABLE"]);
if($arParams["SECTION_ID_VARIABLE"] === '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["SECTION_ID_VARIABLE"]))
	$arParams["SECTION_ID_VARIABLE"] = "SECTION_ID";

$arParams["SET_TITLE"] = $arParams["SET_TITLE"]!="N";
$arParams["SET_BROWSER_TITLE"] = (isset($arParams["SET_BROWSER_TITLE"]) && $arParams["SET_BROWSER_TITLE"] === 'N' ? 'N' : 'Y');
$arParams["SET_META_KEYWORDS"] = (isset($arParams["SET_META_KEYWORDS"]) && $arParams["SET_META_KEYWORDS"] === 'N' ? 'N' : 'Y');
$arParams["SET_META_DESCRIPTION"] = (isset($arParams["SET_META_DESCRIPTION"]) && $arParams["SET_META_DESCRIPTION"] === 'N' ? 'N' : 'Y');
$arParams["ADD_SECTIONS_CHAIN"] = (isset($arParams["ADD_SECTIONS_CHAIN"]) && $arParams["ADD_SECTIONS_CHAIN"]==="Y"); //Turn off by default

$arParams["BACKGROUND_IMAGE"] = (isset($arParams["BACKGROUND_IMAGE"]) ? trim($arParams["BACKGROUND_IMAGE"]) : '');
if ($arParams["BACKGROUND_IMAGE"] == '-')
	$arParams["BACKGROUND_IMAGE"] = '';

$arParams["DISPLAY_COMPARE"] = (isset($arParams['DISPLAY_COMPARE']) && $arParams["DISPLAY_COMPARE"] == "Y");
$arParams['COMPARE_PATH'] = (isset($arParams['COMPARE_PATH']) ? trim($arParams['COMPARE_PATH']) : '');

$arParams["PAGE_ELEMENT_COUNT"] = intval($arParams["PAGE_ELEMENT_COUNT"]);
if($arParams["PAGE_ELEMENT_COUNT"]<=0)
	$arParams["PAGE_ELEMENT_COUNT"]=20;
$arParams["LINE_ELEMENT_COUNT"] = intval($arParams["LINE_ELEMENT_COUNT"]);
if($arParams["LINE_ELEMENT_COUNT"]<=0)
	$arParams["LINE_ELEMENT_COUNT"]=3;

if(!is_array($arParams["PROPERTY_CODE"]))
	$arParams["PROPERTY_CODE"] = array();
foreach($arParams["PROPERTY_CODE"] as $k=>$v)
	if($v==="")
		unset($arParams["PROPERTY_CODE"][$k]);

if(!is_array($arParams["PRICE_CODE"]))
	$arParams["PRICE_CODE"] = array();
$arParams["USE_PRICE_COUNT"] = $arParams["USE_PRICE_COUNT"]=="Y";
$arParams["SHOW_PRICE_COUNT"] = (isset($arParams["SHOW_PRICE_COUNT"]) ? (int)$arParams["SHOW_PRICE_COUNT"] : 1);
if($arParams["SHOW_PRICE_COUNT"]<=0)
	$arParams["SHOW_PRICE_COUNT"]=1;
$arParams["USE_PRODUCT_QUANTITY"] = $arParams["USE_PRODUCT_QUANTITY"]==="Y";

if (!isset($arParams['HIDE_NOT_AVAILABLE']))
	$arParams['HIDE_NOT_AVAILABLE'] = 'N';
if ($arParams['HIDE_NOT_AVAILABLE'] != 'Y' && $arParams['HIDE_NOT_AVAILABLE'] != 'L')
	$arParams['HIDE_NOT_AVAILABLE'] = 'N';

$arParams['ADD_PROPERTIES_TO_BASKET'] = (isset($arParams['ADD_PROPERTIES_TO_BASKET']) && $arParams['ADD_PROPERTIES_TO_BASKET'] == 'N' ? 'N' : 'Y');
if ('N' == $arParams['ADD_PROPERTIES_TO_BASKET'])
{
	$arParams["PRODUCT_PROPERTIES"] = array();
	$arParams["OFFERS_CART_PROPERTIES"] = array();
}
$arParams['PARTIAL_PRODUCT_PROPERTIES'] = (isset($arParams['PARTIAL_PRODUCT_PROPERTIES']) && $arParams['PARTIAL_PRODUCT_PROPERTIES'] === 'Y' ? 'Y' : 'N');
if(!is_array($arParams["PRODUCT_PROPERTIES"]))
	$arParams["PRODUCT_PROPERTIES"] = array();
foreach($arParams["PRODUCT_PROPERTIES"] as $k=>$v)
	if($v==="")
		unset($arParams["PRODUCT_PROPERTIES"][$k]);

if (!is_array($arParams["OFFERS_CART_PROPERTIES"]))
	$arParams["OFFERS_CART_PROPERTIES"] = array();
foreach($arParams["OFFERS_CART_PROPERTIES"] as $i => $pid)
	if ($pid === "")
		unset($arParams["OFFERS_CART_PROPERTIES"][$i]);

if (empty($arParams["OFFERS_SORT_FIELD"]))
	$arParams["OFFERS_SORT_FIELD"] = "sort";
if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["OFFERS_SORT_ORDER"]))
	$arParams["OFFERS_SORT_ORDER"] = "asc";
if (empty($arParams["OFFERS_SORT_FIELD2"]))
	$arParams["OFFERS_SORT_FIELD2"] = "id";
if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["OFFERS_SORT_ORDER2"]))
	$arParams["OFFERS_SORT_ORDER2"] = "desc";

$arParams["DISPLAY_TOP_PAGER"] = $arParams["DISPLAY_TOP_PAGER"]=="Y";
$arParams["DISPLAY_BOTTOM_PAGER"] = $arParams["DISPLAY_BOTTOM_PAGER"]!="N";
$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"]=="Y";
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
$arParams["PAGER_DESC_NUMBERING"] = $arParams["PAGER_DESC_NUMBERING"]=="Y";
$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
$arParams["PAGER_SHOW_ALL"] = $arParams["PAGER_SHOW_ALL"]=="Y";

if ($arParams['DISPLAY_TOP_PAGER'] || $arParams['DISPLAY_BOTTOM_PAGER'])
{
	$arNavParams = array(
		"nPageSize" => $arParams["PAGE_ELEMENT_COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
		"bShowAll" => $arParams["PAGER_SHOW_ALL"],
	);
	$arNavigation = CDBResult::GetNavParams($arNavParams);
	if($arNavigation["PAGEN"]==0 && $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]>0)
		$arParams["CACHE_TIME"] = $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];
}
else
{
	$arNavParams = array(
		"nTopCount" => $arParams["PAGE_ELEMENT_COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
	);
	$arNavigation = false;
}

$arParams['CACHE_GROUPS'] = trim($arParams['CACHE_GROUPS']);
if ('N' != $arParams['CACHE_GROUPS'])
	$arParams['CACHE_GROUPS'] = 'Y';

$arParams["CACHE_FILTER"]=$arParams["CACHE_FILTER"]=="Y";
if(!$arParams["CACHE_FILTER"] && count($arrFilter)>0)
	$arParams["CACHE_TIME"] = 0;

$arParams["PRICE_VAT_INCLUDE"] = $arParams["PRICE_VAT_INCLUDE"] !== "N";

$arParams['CONVERT_CURRENCY'] = (isset($arParams['CONVERT_CURRENCY']) && 'Y' == $arParams['CONVERT_CURRENCY'] ? 'Y' : 'N');
$arParams['CURRENCY_ID'] = trim(strval($arParams['CURRENCY_ID']));
if ('' == $arParams['CURRENCY_ID'])
{
	$arParams['CONVERT_CURRENCY'] = 'N';
}
elseif ('N' == $arParams['CONVERT_CURRENCY'])
{
	$arParams['CURRENCY_ID'] = '';
}

$arParams["OFFERS_LIMIT"] = intval($arParams["OFFERS_LIMIT"]);
if (0 > $arParams["OFFERS_LIMIT"])
	$arParams["OFFERS_LIMIT"] = 0;

$arParams["DISABLE_INIT_JS_IN_COMPONENT"] = (isset($arParams["DISABLE_INIT_JS_IN_COMPONENT"]) && $arParams["DISABLE_INIT_JS_IN_COMPONENT"] == 'Y' ? 'Y' : 'N');
$arParams['CUSTOM_CURRENT_PAGE'] = (isset($arParams['CUSTOM_CURRENT_PAGE']) ? trim($arParams['CUSTOM_CURRENT_PAGE']) : '');

if ($arParams["DISABLE_INIT_JS_IN_COMPONENT"] != 'Y')
	CJSCore::Init(array('popup'));

/*************************************************************************
			Processing of the Buy link
*************************************************************************/
$strError = '';
$successfulAdd = true;

if (isset($_REQUEST[$arParams["ACTION_VARIABLE"]]) && isset($_REQUEST[$arParams["PRODUCT_ID_VARIABLE"]]))
{
	if(isset($_REQUEST[$arParams["ACTION_VARIABLE"]."BUY"]))
		$action = "BUY";
	elseif(isset($_REQUEST[$arParams["ACTION_VARIABLE"]."ADD2BASKET"]))
		$action = "ADD2BASKET";
	else
		$action = strtoupper($_REQUEST[$arParams["ACTION_VARIABLE"]]);

	$productID = (int)$_REQUEST[$arParams["PRODUCT_ID_VARIABLE"]];
	if(($action == "ADD2BASKET" || $action == "BUY" || $action == "SUBSCRIBE_PRODUCT") && $productID > 0)
	{
		if (Loader::includeModule("sale") && Loader::includeModule("catalog"))
		{
			$addByAjax = isset($_REQUEST['ajax_basket']) && 'Y' == $_REQUEST['ajax_basket'];
			if ($addByAjax)
				CUtil::JSPostUnescape();
			$QUANTITY = 0;
			$product_properties = array();
			$intProductIBlockID = (int)CIBlockElement::GetIBlockByID($productID);
			if (0 < $intProductIBlockID)
			{
				if ($arParams['ADD_PROPERTIES_TO_BASKET'] == 'Y')
				{
					if ($intProductIBlockID == $arParams["IBLOCK_ID"])
					{
						if (!empty($arParams["PRODUCT_PROPERTIES"]))
						{
							if (
								isset($_REQUEST[$arParams["PRODUCT_PROPS_VARIABLE"]])
								&& is_array($_REQUEST[$arParams["PRODUCT_PROPS_VARIABLE"]])
							)
							{
								$product_properties = CIBlockPriceTools::CheckProductProperties(
									$arParams["IBLOCK_ID"],
									$productID,
									$arParams["PRODUCT_PROPERTIES"],
									$_REQUEST[$arParams["PRODUCT_PROPS_VARIABLE"]],
									$arParams['PARTIAL_PRODUCT_PROPERTIES'] == 'Y'
								);
								if (!is_array($product_properties))
								{
									$strError = GetMessage("CATALOG_PARTIAL_BASKET_PROPERTIES_ERROR");
									$successfulAdd = false;
								}
							}
							else
							{
								$strError = GetMessage("CATALOG_EMPTY_BASKET_PROPERTIES_ERROR");
								$successfulAdd = false;
							}
						}
					}
					else
					{
						$skuAddProps = (isset($_REQUEST['basket_props']) && !empty($_REQUEST['basket_props']) ? $_REQUEST['basket_props'] : '');
						if (!empty($arParams["OFFERS_CART_PROPERTIES"]) || !empty($skuAddProps))
						{
							$product_properties = CIBlockPriceTools::GetOfferProperties(
								$productID,
								$arParams["IBLOCK_ID"],
								$arParams["OFFERS_CART_PROPERTIES"],
								$skuAddProps
							);
						}
					}
				}
				if ($arParams["USE_PRODUCT_QUANTITY"])
				{
					if (isset($_REQUEST[$arParams["PRODUCT_QUANTITY_VARIABLE"]]))
					{
						$QUANTITY = doubleval($_REQUEST[$arParams["PRODUCT_QUANTITY_VARIABLE"]]);
					}
				}
				if (0 >= $QUANTITY)
				{
					$rsRatios = CCatalogMeasureRatio::getList(
						array(),
						array('PRODUCT_ID' => $productID),
						false,
						false,
						array('PRODUCT_ID', 'RATIO')
					);
					if ($arRatio = $rsRatios->Fetch())
					{
						$intRatio = (int)$arRatio['RATIO'];
						$dblRatio = doubleval($arRatio['RATIO']);
						$QUANTITY = ($dblRatio > $intRatio ? $dblRatio : $intRatio);
					}
				}
				if (0 >= $QUANTITY)
					$QUANTITY = 1;
			}
			else
			{
				$strError = GetMessage('CATALOG_PRODUCT_NOT_FOUND');
				$successfulAdd = false;
			}

			$notifyOption = COption::GetOptionString("sale", "subscribe_prod", "");
			$arNotify = unserialize($notifyOption);
			$arRewriteFields = array();
			if ($action == "SUBSCRIBE_PRODUCT" && $arNotify[SITE_ID]['use'] == 'Y')
			{
				$arRewriteFields["SUBSCRIBE"] = "Y";
				$arRewriteFields["CAN_BUY"] = "N";
			}

			if ($successfulAdd)
			{
				if(!Add2BasketByProductID($productID, $QUANTITY, $arRewriteFields, $product_properties))
				{
					if ($ex = $APPLICATION->GetException())
						$strError = $ex->GetString();
					else
						$strError = GetMessage("CATALOG_ERROR2BASKET");
					$successfulAdd = false;
				}
			}

			if ($addByAjax)
			{
				if ($successfulAdd)
				{
					$addResult = array('STATUS' => 'OK', 'MESSAGE' => GetMessage('CATALOG_SUCCESSFUL_ADD_TO_BASKET'));
				}
				else
				{
					$addResult = array('STATUS' => 'ERROR', 'MESSAGE' => $strError);
				}
				$APPLICATION->RestartBuffer();
				echo CUtil::PhpToJSObject($addResult);
				die();
			}
			else
			{
				if ($successfulAdd)
				{
					$pathRedirect = (
					$action == "BUY"
						? $arParams["BASKET_URL"]
						: $APPLICATION->GetCurPageParam("", array(
							$arParams["PRODUCT_ID_VARIABLE"],
							$arParams["ACTION_VARIABLE"],
							$arParams['PRODUCT_QUANTITY_VARIABLE'],
							$arParams['PRODUCT_PROPS_VARIABLE']
						))
					);
					LocalRedirect($pathRedirect);
				}
			}
		}
	}
}

if (!$successfulAdd)
{
	ShowError($strError);
	return;
}

/*************************************************************************
			Work with cache
*************************************************************************/
//if($this->startResultCache(false, array($arrFilter, ($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $arNavigation, $pagerParameters)))
{
	if (!Loader::includeModule("iblock"))
	{
		$this->abortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$arResultModules = array(
		'iblock' => true,
		'catalog' => false,
		'currency' => false
	);

	$arConvertParams = array();
	if ($arParams['CONVERT_CURRENCY'] == 'Y')
	{
		if (!Loader::includeModule('currency'))
		{
			$arParams['CONVERT_CURRENCY'] = 'N';
			$arParams['CURRENCY_ID'] = '';
		}
		else
		{
			$arResultModules['currency'] = true;
			$currency = Currency\CurrencyTable::getList(array(
				'select' => array('CURRENCY'),
				'filter' => array('=CURRENCY' => $arParams['CURRENCY_ID'])
			))->fetch();
			if (!empty($currency))
			{
				$arParams['CURRENCY_ID'] = $currency['CURRENCY'];
				$arConvertParams['CURRENCY_ID'] = $currency['CURRENCY'];
			}
			else
			{
				$arParams['CONVERT_CURRENCY'] = 'N';
				$arParams['CURRENCY_ID'] = '';
			}
			unset($currency);
		}
	}

	$arSelect = array();
	if(isset($arParams["SECTION_USER_FIELDS"]) && is_array($arParams["SECTION_USER_FIELDS"]))
	{
		foreach($arParams["SECTION_USER_FIELDS"] as $field)
			if(is_string($field) && preg_match("/^UF_/", $field))
				$arSelect[] = $field;
	}
	if(preg_match("/^UF_/", $arParams["META_KEYWORDS"])) $arSelect[] = $arParams["META_KEYWORDS"];
	if(preg_match("/^UF_/", $arParams["META_DESCRIPTION"])) $arSelect[] = $arParams["META_DESCRIPTION"];
	if(preg_match("/^UF_/", $arParams["BROWSER_TITLE"])) $arSelect[] = $arParams["BROWSER_TITLE"];
	if(preg_match("/^UF_/", $arParams["BACKGROUND_IMAGE"])) $arSelect[] = $arParams["BACKGROUND_IMAGE"];

	$arFilter = array(
		"IBLOCK_ID"=>$arParams["IBLOCK_ID"],
		"IBLOCK_ACTIVE"=>"Y",
		"ACTIVE"=>"Y",
		"GLOBAL_ACTIVE"=>"Y",
	);

	$bSectionFound = false;
	//Hidden triky parameter USED to display linked
	//by default it is not set
	if($arParams["BY_LINK"]==="Y")
	{
		$arResult = array(
			"ID" => 0,
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		);
		$bSectionFound = true;
	}
	elseif($arParams["SECTION_ID"] > 0)
	{
		$arFilter["ID"]=$arParams["SECTION_ID"];
		$rsSection = CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
		$rsSection->SetUrlTemplates("", $arParams["SECTION_URL"]);
		$arResult = $rsSection->GetNext();
		if($arResult)
			$bSectionFound = true;
	}
	elseif(strlen($arParams["SECTION_CODE"]) > 0)
	{
		$arFilter["=CODE"]=$arParams["SECTION_CODE"];
		$rsSection = CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
		$rsSection->SetUrlTemplates("", $arParams["SECTION_URL"]);
		$arResult = $rsSection->GetNext();
		if($arResult)
			$bSectionFound = true;
	}
	elseif(strlen($arParams["SECTION_CODE_PATH"]) > 0)
	{
		$sectionId = CIBlockFindTools::GetSectionIDByCodePath($arParams["IBLOCK_ID"], $arParams["SECTION_CODE_PATH"]);
		if ($sectionId)
		{
			$arFilter["ID"]=$sectionId;
			$rsSection = CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
			$rsSection->SetUrlTemplates("", $arParams["SECTION_URL"]);
			$arResult = $rsSection->GetNext();
			if($arResult)
				$bSectionFound = true;
		}
	}
	else
	{
		//Root section (no section filter)
		$arResult = array(
			"ID" => 0,
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		);
		$bSectionFound = true;
	}

	if(!$bSectionFound)
	{
		$this->abortResultCache();
		Iblock\Component\Tools::process404(
			trim($arParams["MESSAGE_404"]) ?: GetMessage("CATALOG_SECTION_NOT_FOUND")
			,true
			,$arParams["SET_STATUS_404"] === "Y"
			,$arParams["SHOW_404"] === "Y"
			,$arParams["FILE_404"]
		);
		return;
	}
	elseif($arResult["ID"] > 0 && $arParams["ADD_SECTIONS_CHAIN"])
	{
		$arResult["PATH"] = array();
		$rsPath = CIBlockSection::GetNavChain($arResult["IBLOCK_ID"], $arResult["ID"]);
		$rsPath->SetUrlTemplates("", $arParams["SECTION_URL"]);
		while($arPath = $rsPath->GetNext())
		{
			$ipropValues = new Iblock\InheritedProperty\SectionValues($arParams["IBLOCK_ID"], $arPath["ID"]);
			$arPath["IPROPERTY_VALUES"] = $ipropValues->getValues();
			$arResult["PATH"][]=$arPath;
		}
	}

	$bIBlockCatalog = false;
	$bOffersIBlockExist = false;
	$arCatalog = false;
	$boolNeedCatalogCache = false;
	$bCatalog = Loader::includeModule('catalog');
	$useCatalogButtons = array();
	if ($bCatalog)
	{
		$arResultModules['catalog'] = true;
		$arResultModules['currency'] = true;
		$arCatalog = CCatalogSKU::GetInfoByIBlock($arParams["IBLOCK_ID"]);
		if (!empty($arCatalog) && is_array($arCatalog))
		{
			$bIBlockCatalog = $arCatalog['CATALOG_TYPE'] != CCatalogSKU::TYPE_PRODUCT;
			$bOffersIBlockExist = (
				$arCatalog['CATALOG_TYPE'] == CCatalogSKU::TYPE_PRODUCT
				|| $arCatalog['CATALOG_TYPE'] == CCatalogSKU::TYPE_FULL
			);
			$boolNeedCatalogCache = true;
			if ($arCatalog['CATALOG_TYPE'] == CCatalogSKU::TYPE_CATALOG || $arCatalog['CATALOG_TYPE'] == CCatalogSKU::TYPE_FULL)
				$useCatalogButtons['add_product'] = true;
			if ($arCatalog['CATALOG_TYPE'] == CCatalogSKU::TYPE_PRODUCT || $arCatalog['CATALOG_TYPE'] == CCatalogSKU::TYPE_FULL)
				$useCatalogButtons['add_sku'] = true;
		}
	}
	$arResult['CATALOG'] = $arCatalog;
	$arResult['USE_CATALOG_BUTTONS'] = $useCatalogButtons;
	unset($useCatalogButtons);
	//This function returns array with prices description and access rights
	//in case catalog module n/a prices get values from element properties
	$arResult["PRICES"] = CIBlockPriceTools::GetCatalogPrices($arParams["IBLOCK_ID"], $arParams["PRICE_CODE"]);
	$arResult['PRICES_ALLOW'] = CIBlockPriceTools::GetAllowCatalogPrices($arResult["PRICES"]);

	if ($bCatalog && $boolNeedCatalogCache && !empty($arResult['PRICES_ALLOW']))
		$boolNeedCatalogCache = CIBlockPriceTools::SetCatalogDiscountCache($arResult['PRICES_ALLOW'], $USER->GetUserGroupArray());

	$arResult['CONVERT_CURRENCY'] = $arConvertParams;

	if ($arResult["ID"] > 0)
	{
		$ipropValues = new Iblock\InheritedProperty\SectionValues($arResult["IBLOCK_ID"], $arResult["ID"]);
		$arResult["IPROPERTY_VALUES"] = $ipropValues->getValues();
	}
	else
	{
		$arResult["IPROPERTY_VALUES"] = array();
	}

	Iblock\Component\Tools::getFieldImageData(
		$arResult,
		array('PICTURE', 'DETAIL_PICTURE'),
		Iblock\Component\Tools::IPROPERTY_ENTITY_SECTION,
		'IPROPERTY_VALUES'
	);

	$arResult['BACKGROUND_IMAGE'] = false;
	if ($arParams['BACKGROUND_IMAGE'] != '' && isset($arResult[$arParams['BACKGROUND_IMAGE']]))
	{
		if (!empty($arResult[$arParams['BACKGROUND_IMAGE']]))
			$arResult['BACKGROUND_IMAGE'] = CFile::GetFileArray($arResult[$arParams['BACKGROUND_IMAGE']]);
	}

	$bGetPropertyCodes = !empty($arParams["PROPERTY_CODE"]);
	$bGetProductProperties = ($arParams['ADD_PROPERTIES_TO_BASKET'] == 'Y'  && !empty($arParams["PRODUCT_PROPERTIES"]));
	$bGetProperties = $bGetPropertyCodes || $bGetProductProperties;

	$propertyList = array();
	if ($bGetProperties)
	{
		$selectProperties = array_fill_keys($arParams['PROPERTY_CODE'], true);
		$propertyIterator = Iblock\PropertyTable::getList(array(
			'select' => array('ID', 'CODE'),
			'filter' => array('=IBLOCK_ID' => $arParams['IBLOCK_ID'], '=ACTIVE' => 'Y'),
			'order' => array('SORT' => 'ASC', 'ID' => 'ASC')
		));
		while ($property = $propertyIterator->fetch())
		{
			$code = (string)$property['CODE'];
			if ($code == '')
				$code = $property['ID'];
			if (!isset($selectProperties[$code]))
				continue;
			$propertyList[] = $code;
			unset($code);
		}
		unset($property, $propertyIterator);
		unset($selectProperties);
	}

	// list of the element fields that will be used in selection
	$arSelect = array(
		"ID",
		"IBLOCK_ID",
		"CODE",
		"XML_ID",
		"NAME",
		"ACTIVE",
		"DATE_ACTIVE_FROM",
		"DATE_ACTIVE_TO",
		"SORT",
		"PREVIEW_TEXT",
		"PREVIEW_TEXT_TYPE",
		"DETAIL_TEXT",
		"DETAIL_TEXT_TYPE",
		"DATE_CREATE",
		"CREATED_BY",
		"TIMESTAMP_X",
		"MODIFIED_BY",
		"TAGS",
		"IBLOCK_SECTION_ID",
		"DETAIL_PAGE_URL",
		"DETAIL_PICTURE",
		"PREVIEW_PICTURE"
	);
	if ($bIBlockCatalog)
		$arSelect[] = "CATALOG_QUANTITY";
	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"IBLOCK_LID" => SITE_ID,
		"IBLOCK_ACTIVE" => "Y",
		"ACTIVE_DATE" => "Y",
		"ACTIVE" => "Y",
		"CHECK_PERMISSIONS" => "Y",
		"MIN_PERMISSION" => "R",
		"INCLUDE_SUBSECTIONS" => 'Y',
	);
	if ($arParams["INCLUDE_SUBSECTIONS"] == 'A')
		$arFilter["SECTION_GLOBAL_ACTIVE"] = "Y";
	if ($bIBlockCatalog && 'Y' == $arParams['HIDE_NOT_AVAILABLE'])
		$arFilter['CATALOG_AVAILABLE'] = 'Y';

	if($arParams["BY_LINK"]!=="Y")
	{
		if($arResult["ID"])
			$arFilter["SECTION_ID"] = $arResult["ID"];
		elseif(!$arParams["SHOW_ALL_WO_SECTION"])
			$arFilter["SECTION_ID"] = 0;
		else
		{
			if (is_set($arFilter, 'INCLUDE_SUBSECTIONS'))
				unset($arFilter["INCLUDE_SUBSECTIONS"]);
			if (is_set($arFilter, 'SECTION_GLOBAL_ACTIVE'))
				unset($arFilter["SECTION_GLOBAL_ACTIVE"]);
		}
	}

	$arSubFilter = array();
	if($bCatalog && $bOffersIBlockExist)
	{
		$bOffersFilterExist = (isset($arrFilter["OFFERS"]) && !empty($arrFilter["OFFERS"]) && is_array($arrFilter["OFFERS"]));
		$arPriceFilter = array();
		foreach($arrFilter as $key => $value)
		{
			if(preg_match('/^(>=|<=|><)CATALOG_PRICE_/', $key))
			{
				$arPriceFilter[$key] = $value;
				unset($arrFilter[$key]);
			}
		}

		if($bOffersFilterExist)
		{
			if (empty($arPriceFilter))
				$arSubFilter = $arrFilter["OFFERS"];
			else
				$arSubFilter = array_merge($arrFilter["OFFERS"], $arPriceFilter);

			$arSubFilter["IBLOCK_ID"] = $arResult['CATALOG']['IBLOCK_ID'];
			$arSubFilter["ACTIVE_DATE"] = "Y";
			$arSubFilter["ACTIVE"] = "Y";
			if ('Y' == $arParams['HIDE_NOT_AVAILABLE'])
				$arSubFilter['CATALOG_AVAILABLE'] = 'Y';
			$arFilter["=ID"] = CIBlockElement::SubQuery("PROPERTY_".$arResult['CATALOG']["SKU_PROPERTY_ID"], $arSubFilter);
		}
		elseif(!empty($arPriceFilter))
		{
			$arSubFilter = $arPriceFilter;

			$arSubFilter["IBLOCK_ID"] = $arResult['CATALOG']['IBLOCK_ID'];
			$arSubFilter["ACTIVE_DATE"] = "Y";
			$arSubFilter["ACTIVE"] = "Y";
			$arFilter[] = array(
				"LOGIC" => "OR",
				array($arPriceFilter),
				"=ID" => CIBlockElement::SubQuery("PROPERTY_".$arResult['CATALOG']["SKU_PROPERTY_ID"], $arSubFilter),
			);
		}
	}

	//PRICES
	$arPriceTypeID = array();
	if (!empty($arResult["PRICES"]))
	{
		if (!$arParams["USE_PRICE_COUNT"])
		{
			foreach ($arResult["PRICES"] as &$value)
			{
				if (!$value['CAN_VIEW'] && !$value['CAN_BUY'])
					continue;
				$arSelect[] = $value["SELECT"];
				$arFilter["CATALOG_SHOP_QUANTITY_".$value["ID"]] = $arParams["SHOW_PRICE_COUNT"];
			}
			unset($value);
		}
		else
		{
			foreach ($arResult["PRICES"] as &$value)
			{
				if (!$value['CAN_VIEW'] && !$value['CAN_BUY'])
					continue;
				$arPriceTypeID[] = $value["ID"];
			}
			unset($value);
		}
	}

	$arSort = array();
	if ($bIBlockCatalog && $arParams['HIDE_NOT_AVAILABLE'] == 'L')
		$arSort['CATALOG_AVAILABLE'] = 'desc,nulls';
	if (!isset($arSort['CATALOG_AVAILABLE']) || $arParams["ELEMENT_SORT_FIELD"] != 'CATALOG_AVAILABLE')
		$arSort[$arParams["ELEMENT_SORT_FIELD"]] = $arParams["ELEMENT_SORT_ORDER"];
	if (!isset($arSort['CATALOG_AVAILABLE']) || $arParams["ELEMENT_SORT_FIELD2"] != 'CATALOG_AVAILABLE')
		$arSort[$arParams["ELEMENT_SORT_FIELD2"]] = $arParams["ELEMENT_SORT_ORDER2"];

	$arDefaultMeasure = array();
	if ($bIBlockCatalog)
		$arDefaultMeasure = CCatalogMeasure::getDefaultMeasure(true, true);
	$currencyList = array();
	$arSections = array();

	//EXECUTE
	$rsElements = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
	$rsElements->SetUrlTemplates($arParams["DETAIL_URL"]);
	if(
		$arParams["BY_LINK"]!=="Y"
		&& !$arParams["SHOW_ALL_WO_SECTION"]
		&& !$arParams["USE_MAIN_ELEMENT_SECTION"]
	)
	{
		$rsElements->SetSectionContext($arResult);
	}

	$arResult["ITEMS"] = array();
	$arMeasureMap = array();
	$arElementLink = array();
	$intKey = 0;
	while($arItem = $rsElements->GetNext())
	{
		$arItem['ID'] = (int)$arItem['ID'];

		$arItem['ACTIVE_FROM'] = $arItem['DATE_ACTIVE_FROM'];
		$arItem['ACTIVE_TO'] = $arItem['DATE_ACTIVE_TO'];

		if($arResult["ID"])
			$arItem["IBLOCK_SECTION_ID"] = $arResult["ID"];

//		$arButtons = CIBlock::GetPanelButtons(
//			$arItem["IBLOCK_ID"],
//			$arItem["ID"],
//			$arResult["ID"],
//			array("SECTION_BUTTONS"=>false, "SESSID"=>false, "CATALOG"=>true)
//		);
//		$arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
//		$arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];
//
//		$ipropValues = new Iblock\InheritedProperty\ElementValues($arItem["IBLOCK_ID"], $arItem["ID"]);
//		$arItem["IPROPERTY_VALUES"] = $ipropValues->getValues();
//
//		Iblock\Component\Tools::getFieldImageData(
//			$arItem,
//			array('PREVIEW_PICTURE', 'DETAIL_PICTURE'),
//			Iblock\Component\Tools::IPROPERTY_ENTITY_ELEMENT,
//			'IPROPERTY_VALUES'
//		);
//
//		$arItem["PROPERTIES"] = array();
//		$arItem["DISPLAY_PROPERTIES"] = array();
//		$arItem["PRODUCT_PROPERTIES"] = array();
//		$arItem['PRODUCT_PROPERTIES_FILL'] = array();
//
//		if ($bIBlockCatalog)
//		{
//			if (!isset($arItem["CATALOG_MEASURE_RATIO"]))
//				$arItem["CATALOG_MEASURE_RATIO"] = 1;
//			if (!isset($arItem['CATALOG_MEASURE']))
//				$arItem['CATALOG_MEASURE'] = 0;
//			$arItem['CATALOG_MEASURE'] = (int)$arItem['CATALOG_MEASURE'];
//			if (0 > $arItem['CATALOG_MEASURE'])
//				$arItem['CATALOG_MEASURE'] = 0;
//			if (!isset($arItem['CATALOG_MEASURE_NAME']))
//				$arItem['CATALOG_MEASURE_NAME'] = '';
//
//			$arItem['CATALOG_MEASURE_NAME'] = $arDefaultMeasure['SYMBOL_RUS'];
//			$arItem['~CATALOG_MEASURE_NAME'] = $arDefaultMeasure['~SYMBOL_RUS'];
//			if (0 < $arItem['CATALOG_MEASURE'])
//			{
//				if (!isset($arMeasureMap[$arItem['CATALOG_MEASURE']]))
//					$arMeasureMap[$arItem['CATALOG_MEASURE']] = array();
//				$arMeasureMap[$arItem['CATALOG_MEASURE']][] = $intKey;
//			}
//		}
//
//		if ($arParams["SET_LAST_MODIFIED"])
//		{
//			$time = DateTime::createFromUserTime($arItem["TIMESTAMP_X"]);
//			if (
//				!isset($arResult["ITEMS_TIMESTAMP_X"])
//				|| $time->getTimestamp() > $arResult["ITEMS_TIMESTAMP_X"]->getTimestamp()
//			)
//				$arResult["ITEMS_TIMESTAMP_X"] = $time;
//		}

		$arResult["ITEMS"][$intKey] = $arItem;
		$arResult["ELEMENTS"][$intKey] = $arItem["ID"];
		$arElementLink[$arItem['ID']] = &$arResult["ITEMS"][$intKey];
		$intKey++;
	}
	$arResult['MODULES'] = $arResultModules;

	$navComponentParameters = array();
	if ($arParams["PAGER_BASE_LINK_ENABLE"] === "Y")
	{
		$pagerBaseLink = trim($arParams["PAGER_BASE_LINK"]);
		if ($pagerBaseLink === "")
			$pagerBaseLink = $arResult["SECTION_PAGE_URL"];

		if ($pagerParameters && isset($pagerParameters["BASE_LINK"]))
		{
			$pagerBaseLink = $pagerParameters["BASE_LINK"];
			unset($pagerParameters["BASE_LINK"]);
		}

		$navComponentParameters["BASE_LINK"] = CHTTP::urlAddParams($pagerBaseLink, $pagerParameters, array("encode"=>true));
	}

	$arResult["NAV_STRING"] = $rsElements->GetPageNavStringEx(
		$navComponentObject,
		$arParams["PAGER_TITLE"],
		$arParams["PAGER_TEMPLATE"],
		$arParams["PAGER_SHOW_ALWAYS"],
		$this,
		$navComponentParameters
	);
	$arResult["NAV_CACHED_DATA"] = null;
	$arResult["NAV_RESULT"] = $rsElements;
	$arResult["NAV_PARAM"] = $navComponentParameters;
	if (isset($arItem))
		unset($arItem);

//	if (!empty($arResult["ELEMENTS"]) && ($bGetProperties || ($bCatalog && $boolNeedCatalogCache)))
//	{
//		$arPropFilter = array(
//			'ID' => $arResult["ELEMENTS"],
//			'IBLOCK_ID' => $arParams['IBLOCK_ID']
//		);
//		CIBlockElement::GetPropertyValuesArray($arElementLink, $arParams["IBLOCK_ID"], $arPropFilter);
//
//		foreach ($arResult["ITEMS"] as &$arItem)
//		{
//			if ($bCatalog && $boolNeedCatalogCache)
//				CCatalogDiscount::SetProductPropertiesCache($arItem['ID'], $arItem["PROPERTIES"]);
//
//			if (!empty($bGetProperties))
//			{
//				if (!empty($propertyList))
//				{
//					foreach ($propertyList as &$pid)
//					{
//						if (!isset($arItem["PROPERTIES"][$pid]))
//							continue;
//						$prop = &$arItem["PROPERTIES"][$pid];
//						$boolArr = is_array($prop["VALUE"]);
//						if (
//								($boolArr && !empty($prop["VALUE"]))
//								|| (!$boolArr && strlen($prop["VALUE"]) > 0)
//						)
//						{
//							$arItem["DISPLAY_PROPERTIES"][$pid] = CIBlockFormatProperties::GetDisplayValue($arItem, $prop, "catalog_out");
//						}
//						unset($prop);
//					}
//					unset($pid);
//				}
//
//				if ($bGetProductProperties)
//				{
//					$arItem["PRODUCT_PROPERTIES"] = CIBlockPriceTools::GetProductProperties(
//						$arParams["IBLOCK_ID"],
//						$arItem["ID"],
//						$arParams["PRODUCT_PROPERTIES"],
//						$arItem["PROPERTIES"]
//					);
//					if (!empty($arItem["PRODUCT_PROPERTIES"]))
//						$arItem['PRODUCT_PROPERTIES_FILL'] = CIBlockPriceTools::getFillProductProperties($arItem['PRODUCT_PROPERTIES']);
//				}
//			}
//		}
//		unset($arItem);
//	}

//	if ($bIBlockCatalog)
//	{
//		if (!empty($arResult["ELEMENTS"]))
//		{
//			$rsRatios = CCatalogMeasureRatio::getList(
//				array(),
//				array('PRODUCT_ID' => $arResult["ELEMENTS"]),
//				false,
//				false,
//				array('PRODUCT_ID', 'RATIO')
//			);
//			while ($arRatio = $rsRatios->Fetch())
//			{
//				$arRatio['PRODUCT_ID'] = (int)$arRatio['PRODUCT_ID'];
//				if (isset($arElementLink[$arRatio['PRODUCT_ID']]))
//				{
//					$intRatio = (int)$arRatio['RATIO'];
//					$dblRatio = doubleval($arRatio['RATIO']);
//					$mxRatio = ($dblRatio > $intRatio ? $dblRatio : $intRatio);
//					if (CATALOG_VALUE_EPSILON > abs($mxRatio))
//						$mxRatio = 1;
//					elseif (0 > $mxRatio)
//						$mxRatio = 1;
//					$arElementLink[$arRatio['PRODUCT_ID']]['CATALOG_MEASURE_RATIO'] = $mxRatio;
//				}
//			}
//		}
//		if (!empty($arMeasureMap))
//		{
//			$rsMeasures = CCatalogMeasure::getList(
//				array(),
//				array('@ID' => array_keys($arMeasureMap)),
//				false,
//				false,
//				array('ID', 'SYMBOL_RUS')
//			);
//			while ($arMeasure = $rsMeasures->GetNext())
//			{
//				$arMeasure['ID'] = (int)$arMeasure['ID'];
//				if (isset($arMeasureMap[$arMeasure['ID']]) && !empty($arMeasureMap[$arMeasure['ID']]))
//				{
//					foreach ($arMeasureMap[$arMeasure['ID']] as &$intOneKey)
//					{
//						$arResult['ITEMS'][$intOneKey]['CATALOG_MEASURE_NAME'] = $arMeasure['SYMBOL_RUS'];
//						$arResult['ITEMS'][$intOneKey]['~CATALOG_MEASURE_NAME'] = $arMeasure['~SYMBOL_RUS'];
//					}
//					unset($intOneKey);
//				}
//			}
//		}
//	}
//	if ($bCatalog && $boolNeedCatalogCache && !empty($arResult["ELEMENTS"]))
//	{
//		CCatalogDiscount::SetProductSectionsCache($arResult["ELEMENTS"]);
//		CCatalogDiscount::SetDiscountProductCache($arResult["ELEMENTS"], array('IBLOCK_ID' => $arParams["IBLOCK_ID"], 'GET_BY_ID' => 'Y'));
//	}

	$currentPath = CHTTP::urlDeleteParams(
		$arParams['CUSTOM_CURRENT_PAGE']?: $APPLICATION->GetCurPageParam(),
		array($arParams['PRODUCT_ID_VARIABLE'], $arParams['ACTION_VARIABLE'], ''),
		array('delete_system_params' => true)
	);
	$currentPath .= (stripos($currentPath, '?') === false ? '?' : '&');
	if ($arParams['COMPARE_PATH'] == '')
	{
		$comparePath = $currentPath;
	}
	else
	{
		$comparePath = CHTTP::urlDeleteParams(
			$arParams['COMPARE_PATH'],
			array($arParams['PRODUCT_ID_VARIABLE'], $arParams['ACTION_VARIABLE'], ''),
			array('delete_system_params' => true)
		);
		$comparePath .= (stripos($comparePath, '?') === false ? '?' : '&');
	}
	$arParams['COMPARE_PATH'] = $comparePath.$arParams['ACTION_VARIABLE'].'=COMPARE';

	$arResult['~BUY_URL_TEMPLATE'] = $currentPath.$arParams["ACTION_VARIABLE"]."=BUY&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
	$arResult['BUY_URL_TEMPLATE'] = htmlspecialcharsbx($arResult['~BUY_URL_TEMPLATE']);
	$arResult['~ADD_URL_TEMPLATE'] = $currentPath.$arParams["ACTION_VARIABLE"]."=ADD2BASKET&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
	$arResult['ADD_URL_TEMPLATE'] = htmlspecialcharsbx($arResult['~ADD_URL_TEMPLATE']);
	$arResult['~SUBSCRIBE_URL_TEMPLATE'] = $currentPath.$arParams["ACTION_VARIABLE"]."=SUBSCRIBE_PRODUCT&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
	$arResult['SUBSCRIBE_URL_TEMPLATE'] = htmlspecialcharsbx($arResult['~SUBSCRIBE_URL_TEMPLATE']);
	$arResult['~COMPARE_URL_TEMPLATE'] = $comparePath.$arParams["ACTION_VARIABLE"]."=ADD_TO_COMPARE_LIST&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
	$arResult['COMPARE_URL_TEMPLATE'] = htmlspecialcharsbx($arResult['~COMPARE_URL_TEMPLATE']);
	unset($comparePath, $currentPath);

	foreach ($arResult["ITEMS"] as &$arItem)
	{
		$arItem["PRICES"] = array();
		$arItem["PRICE_MATRIX"] = false;
		$arItem['MIN_PRICE'] = false;
		if($arParams["USE_PRICE_COUNT"])
		{
			if ($bCatalog)
			{
				$arItem["PRICE_MATRIX"] = CatalogGetPriceTableEx($arItem["ID"], 0, $arPriceTypeID, 'Y', $arConvertParams);
				if (isset($arItem["PRICE_MATRIX"]["COLS"]) && is_array($arItem["PRICE_MATRIX"]["COLS"]))
				{
					foreach($arItem["PRICE_MATRIX"]["COLS"] as $keyColumn=>$arColumn)
						$arItem["PRICE_MATRIX"]["COLS"][$keyColumn]["NAME_LANG"] = htmlspecialcharsEx($arColumn["NAME_LANG"]);
				}
			}
		}
		else
		{
			$arItem["PRICES"] = CIBlockPriceTools::GetItemPrices($arParams["IBLOCK_ID"], $arResult["PRICES"], $arItem, $arParams['PRICE_VAT_INCLUDE'], $arConvertParams);
			if (!empty($arItem['PRICES']))
				$arItem['MIN_PRICE'] = CIBlockPriceTools::getMinPriceFromList($arItem['PRICES']);
		}
		$arItem["CAN_BUY"] = CIBlockPriceTools::CanBuy($arParams["IBLOCK_ID"], $arResult["PRICES"], $arItem);

		$arItem['~BUY_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['~BUY_URL_TEMPLATE']);
		$arItem['BUY_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['BUY_URL_TEMPLATE']);
		$arItem['~ADD_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['~ADD_URL_TEMPLATE']);
		$arItem['ADD_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['ADD_URL_TEMPLATE']);
		$arItem['~SUBSCRIBE_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['~SUBSCRIBE_URL_TEMPLATE']);
		$arItem['SUBSCRIBE_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['SUBSCRIBE_URL_TEMPLATE']);
		if ($arParams['DISPLAY_COMPARE'])
		{
			$arItem['~COMPARE_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['~COMPARE_URL_TEMPLATE']);
			$arItem['COMPARE_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['COMPARE_URL_TEMPLATE']);
		}

		if ($arParams["BY_LINK"] === "Y")
		{
			if (!isset($arSections[$arItem["IBLOCK_SECTION_ID"]]))
			{
				$arSections[$arItem["IBLOCK_SECTION_ID"]] = array();
				$rsPath = CIBlockSection::GetNavChain($arItem["IBLOCK_ID"], $arItem["IBLOCK_SECTION_ID"]);
				$rsPath->SetUrlTemplates("", $arParams["SECTION_URL"]);
				while ($arPath = $rsPath->GetNext())
				{
					$arSections[$arItem["IBLOCK_SECTION_ID"]][] = $arPath;
				}
			}
			$arItem["SECTION"]["PATH"] = $arSections[$arItem["IBLOCK_SECTION_ID"]];
		}
		else
		{
			$arItem["SECTION"]["PATH"] = array();
		}

		if ('Y' == $arParams['CONVERT_CURRENCY'])
		{
			if ($arParams["USE_PRICE_COUNT"])
			{
				if (!empty($arItem["PRICE_MATRIX"]) && is_array($arItem["PRICE_MATRIX"]))
				{
					if (!empty($arItem["PRICE_MATRIX"]['CURRENCY_LIST']) && is_array($arItem["PRICE_MATRIX"]['CURRENCY_LIST']))
						$currencyList = array_merge($arItem['PRICE_MATRIX']['CURRENCY_LIST'], $currencyList);
				}
			}
			else
			{
				if (!empty($arItem["PRICES"]))
				{
					foreach ($arItem["PRICES"] as &$arOnePrices)
					{
						if (isset($arOnePrices['ORIG_CURRENCY']))
							$currencyList[$arOnePrices['ORIG_CURRENCY']] = $arOnePrices['ORIG_CURRENCY'];
					}
					unset($arOnePrices);
				}
			}
		}
	}
	if (isset($arItem))
		unset($arItem);
}
foreach ($arResult['ITEMS'] as $arItem)
{
	if (!empty($arItem['MIN_PRICE']['DISCOUNT_DIFF']))
	{
		CIBlockElement::SetPropertyValues($arItem['ID'], $arItem['IBLOCK_ID'], 14767, 'WITH_DISCOUNT');		
	}
	else
	{
		CIBlockElement::SetPropertyValues($arItem['ID'], $arItem['IBLOCK_ID'], false, 'WITH_DISCOUNT');		
	}
}
	
	return 'setDiscountGoods();';
}