<?php

function setBrandSection()
{	
//	if (getBrandsLastChangeDT() > getCatalogBrandsSectLastChangeDT())
	{
		$brandsSections = array();
		$brandsSections_t = CIBlockSection::GetList(array(), array('IBLOCK_ID' => const_IBLOCK_ID_catalog, 'SECTION_ID' => const_IBLOCK_ID_catalog_SECT_ID_brands));
		while ($brandsSection = $brandsSections_t->Fetch())
		{
			$brandsXML_ID = str_replace('_brands', '', $brandsSection['XML_ID']);

			$goods = CIBlockElement::GetList(array(), array('IBLOCK_ID' => const_IBLOCK_ID_catalog, '!SECTION_ID' => $brandsSection['ID'], 'PROPERTY_BREND' => $brandsXML_ID), false, false, array('ID'));
			while ($good = $goods->Fetch())
			{
				$sects = array();
				$sects_t = CIBlockElement::GetElementGroups($good['ID']);
				while ($sect = $sects_t->Fetch())
				{
					$sects[] = $sect['ID'];
				}
				$sects[] = $brandsSection['ID'];

				CIBlockElement::SetElementSection($good['ID'], $sects);
				\Bitrix\Iblock\PropertyIndex\Manager::updateElementIndex(const_IBLOCK_ID_catalog, $good['ID']);
			}

			$goods = CIBlockElement::GetList(array(), array('IBLOCK_ID' => const_IBLOCK_ID_catalog, 'SECTION_ID' => $brandsSection['ID'], '!PROPERTY_BREND' => $brandsXML_ID), false, false, array('ID'));
			while ($good = $goods->Fetch())
			{
				$sects = array();
				$sects_t = CIBlockElement::GetElementGroups($good['ID']);
				while ($sect = $sects_t->Fetch())
				{
					$sects[] = $sect['ID'];
				}
				unset($sects[array_search($brandsSection['ID'], $sects)]);
				$sects = array_unique($sects);

				CIBlockElement::SetElementSection($good['ID'], $sects);
//				\Bitrix\Iblock\PropertyIndex\Manager::updateElementIndex(const_IBLOCK_ID_catalog, $good['ID']);
			}
		}

		file_put_contents(file_CATALOG_LAST_BRAND_SECT_UPP, time());
	}
	return 'setBrandSection();';
}