<?php

function deactiveNotPrice()
{
	CModule::IncludeModule("iblock");
	
	$elems = CIBlockElement::GetList(
		array(), 
		array('IBLOCK_ID' => const_IBLOCK_ID_catalog, 'ACTIVE' => 'Y', 'CATALOG_PRICE_'.const_PRICE_base => false), 
		false, false, array('ID'));
	while ($elem = $elems->Fetch())
	{
		$el = new CIBlockElement;
		$el->Update($elem['ID'], array('ACTIVE' => 'N'));
//		\Bitrix\Iblock\PropertyIndex\Manager::updateElementIndex(const_IBLOCK_ID_catalog, $elem['ID']);
	}
	
	return 'deactiveNotPrice();';
}