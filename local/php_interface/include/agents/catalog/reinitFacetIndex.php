<?php

use Bitrix\Main\Loader;

function reinitFacetIndex()
{	    
    if (getCatalogLastChangeDT() > getBrandsPropCatLastChangeDT()
	|| getBrandsLastChangeDT() > getCatalogBrandsSectLastChangeDT())
    {
	$iblockDropDown = array();
	$iblockFilter = array('=PROPERTY_INDEX' => 'I');
	if (Loader::includeModule('catalog'))
	{
		$OfferIblocks = array();
		$offersIterator = \Bitrix\Catalog\CatalogIblockTable::getList(array(
			'select' => array('IBLOCK_ID'),
			'filter' => array('!PRODUCT_IBLOCK_ID' => 0)
		));
		while ($offer = $offersIterator->fetch())
		{
			$OfferIblocks[] = (int)$offer['IBLOCK_ID'];
		}
		if (!empty($OfferIblocks))
		{
			unset($offer);
			$iblockFilter['!ID'] = $OfferIblocks;
		}
		unset($offersIterator, $OfferIblocks);
	}
	$iblockList = \Bitrix\Iblock\IblockTable::getList(array(
		'select' => array('ID', 'NAME', 'ACTIVE'),
		'filter' => $iblockFilter,
		'order'  => array('ID' => 'asc', 'NAME' => 'asc'),
	));
	while ($iblockInfo = $iblockList->fetch())
	{
		$iblockDropDown[$iblockInfo['ID']] = '['.$iblockInfo['ID'].'] '.$iblockInfo['NAME'].($iblockInfo['ACTIVE'] == 'N' ? ' ('.GetMessage('IBLOCK_REINDEX_DEACTIVE').')' : '');
	}
	unset($iblockInfo, $iblockList);

	$index = \Bitrix\Iblock\PropertyIndex\Manager::createIndexer(const_IBLOCK_ID_catalog);
	$index->startIndex();
	$res = $index->continueIndex(600);
	$index->endIndex();
	\Bitrix\Iblock\PropertyIndex\Manager::checkAdminNotification();
	CBitrixComponent::clearComponentCache("bitrix:catalog.smart.filter");
	CIBlock::clearIblockTagCache(const_IBLOCK_ID_catalog);
	unset($iblockDropDown[const_IBLOCK_ID_catalog]);
    }
    return 'reinitFacetIndex();';
}