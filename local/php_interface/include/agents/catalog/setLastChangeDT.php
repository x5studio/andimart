<?php

function setLastChangeDT()
{	
	CModule::IncludeModule("iblock");
	
	$elem = CIBlockElement::GetList(array('timestamp_x' => 'desc'), array('IBLOCK_ID' => const_IBLOCK_ID_catalog), false, array('nTopCount' => 1), array('TIMESTAMP_X'));
	$elem = $elem->Fetch();
	
	file_put_contents(file_CATALOG_UPP, MakeTimeStamp($elem['TIMESTAMP_X']));
	
	$sect = CIBlockSection::GetList(array('timestamp_x' => 'desc'), array('IBLOCK_ID' => const_IBLOCK_ID_catalog));
	$sect = $sect->Fetch();
	
	file_put_contents(file_CATALOG_SECT_UPP, MakeTimeStamp($sect['TIMESTAMP_X']));
	
	return 'setLastChangeDT();';
}