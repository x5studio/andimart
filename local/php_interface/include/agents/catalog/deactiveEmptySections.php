<?php

function deactiveEmptySections()
{
    CModule::IncludeModule("iblock");
    
    $sect_cnt = array();
    $elems = CIBlockElement::GetList(array(), array('IBLOCK_ID' => const_IBLOCK_ID_catalog, 'ACTIVE' => 'Y'), false, false, array('ID', 'IBLOCK_SECTION_ID'));
    while ($elem = $elems->Fetch())
    {
	$sects = CIBlockElement::GetElementGroups($elem['ID']);
	while ($sect = $sects->Fetch())
	{
		$sect_cnt[$sect['ID']] = true;
	}
    }
	
    $sects_parents = array();
    $sects = CIBlockSection::GetList(
	    array('left_margin' => 'asc'), 
	    array('IBLOCK_ID' => const_IBLOCK_ID_catalog, 'ACTIVE' => 'Y')
//	    array('CNT_ACTIVE' => 'Y', 'ELEMENT_SUBSECTIONS' => 'Y')
    );
    while ($sect = $sects->Fetch())
    {
	$sects_parents[$sect['ID']] = array(
	    'HAS_ANY' => false,
	    'PARENT' => !empty($sect['IBLOCK_SECTION_ID'])?$sect['IBLOCK_SECTION_ID']:0,
	    'PARENTS' => !empty($sect['IBLOCK_SECTION_ID'])?array_merge($sects_parents[$sect['IBLOCK_SECTION_ID']]['PARENTS'], array($sect['IBLOCK_SECTION_ID'])):array()
	);
    }
    
    foreach ($sects_parents as $sectID => $sectInfo)
    {
	if (!empty($sect_cnt[$sectID]))
	{
	    $sects_parents[$sectID]['HAS_ANY'] = true;
	    
	    foreach ($sectInfo['PARENTS'] as $parentSectionID)
	    {
		$sects_parents[$parentSectionID]['HAS_ANY'] = true;
	    }
	}
    }
    
    foreach ($sects_parents as $sectID => $sectInfo)
    {
	if (!$sectInfo['HAS_ANY'])
	{
	    $bs = new CIBlockSection;
	    $bs->Update($sectID, array('ACTIVE' => 'N'));
	}
    }
    
    return 'deactiveEmptySections();';
}