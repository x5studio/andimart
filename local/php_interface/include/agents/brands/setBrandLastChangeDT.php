<?php

function setBrandLastChangeDT()
{	
	CModule::IncludeModule("iblock");
	
	$elem = CIBlockElement::GetList(array('timestamp_x' => 'desc'), array('IBLOCK_ID' => const_IBLOCK_ID_brands), false, array('nTopCount' => 1), array('TIMESTAMP_X'));
	$elem = $elem->Fetch();
	
	file_put_contents(file_BRANDS_UPP, MakeTimeStamp($elem['TIMESTAMP_X']));
	
	return 'setBrandLastChangeDT();';
}