<?php

//use \Bitrix\Iblock\PropertyIndex;
use Bitrix\Main\Loader;



function setCatalogMainSection(){
    $ids = \XFive\Iblock\SetMainSection::checkDifference();
    \XFive\Iblock\SetMainSection::changeMainSectionsIDs($ids);
    return 'setCatalogMainSection();';
}



function setCatalogSections()
{
	CModule::IncludeModule("iblock");
	
	if (getCatalogLastChangeDT() > getBrandsPropCatLastChangeDT())
	{
		$brands = array();
		$brands_t = CIBlockElement::GetList(array('id' => 'asc'), array('IBLOCK_ID' => const_IBLOCK_ID_brands));
		while($brand_t = $brands_t->GetNextElement())
		{
			$brand = $brand_t->GetFields();
			$brand['PROPERTIES'] = $brand_t->GetProperties();
			
			$brand['CAT_SECTIONS'] = array();
			
			$brands[$brand['XML_ID']] = $brand;
		}
		
		$catalog_sections = array();
		$catalog_sections_t = CIBlockSection::GetList(Array("left_margin"=>"asc"), array('IBLOCK_ID' => const_IBLOCK_ID_catalog), false, array('ID', 'NAME', 'IBLOCK_SECTION_ID', 'IBLOCK_ID'));
		while ($catalog_section = $catalog_sections_t->Fetch())
		{		
			$catalog_section['PARENTS'] = array();			
			if (!empty($catalog_section['IBLOCK_SECTION_ID']))
			{
				$catalog_section['PARENTS'][] = $catalog_section['IBLOCK_SECTION_ID'];				
				$catalog_section['PARENTS'] = array_merge($catalog_sections[$catalog_section['IBLOCK_SECTION_ID']]['PARENTS'], $catalog_section['PARENTS']);
			}
			
			$catalog_sections[$catalog_section['ID']] = $catalog_section;
		}
		
		$catalog_elements = array();
		$catalog_elements_t = CIBlockElement::GetList(Array("id"=>"asc"), array('IBLOCK_ID' => const_IBLOCK_ID_catalog, 'ACTIVE' => 'Y', '!PROPERTY_BREND' => false), false, false, array('ID', 'NAME', 'IBLOCK_SECTION_ID', 'IBLOCK_ID', 'PROPERTY_BREND'));
		while ($catalog_element = $catalog_elements_t->Fetch())
		{
			$catalog_elements[] = $catalog_element['ID'];
			$sects = CIBlockElement::GetElementGroups($catalog_element['ID']);
			while ($sect = $sects->Fetch())
			{
				if (is_array($catalog_sections[$sect['ID']]['PARENTS']))
				{
					$brands[$catalog_element['PROPERTY_BREND_VALUE']]['CAT_SECTIONS'] = 
						array_unique(array_merge($catalog_sections[$sect['ID']]['PARENTS'], 
							array($sect['ID']), $brands[$catalog_element['PROPERTY_BREND_VALUE']]['CAT_SECTIONS']));
				}
			}
		}

		foreach ($brands as $brand)
		{
			CIBlockElement::SetPropertyValues($brand['ID'], $brand['IBLOCK_ID'], $brand['CAT_SECTIONS'], 'CAT_SECTIONS');
		}
//		foreach ($catalog_elements as $catalog_element)
//		{
//			\Bitrix\Iblock\PropertyIndex\Manager::updateElementIndex(const_IBLOCK_ID_catalog, $catalog_element['ID']);
//		}
		
		
		
		
		
		
		file_put_contents(file_BRANDS_UPP_PROP_CAT, time());
	}
	
	return 'setCatalogSections();';
}
