<?php

function uppBrandsSections()
{
	CModule::IncludeModule("iblock");
	
	if (getBrandsLastChangeDT() > getUppBrandsSectionLastChangeDT())
	{
		$brandsSections = array();
		$brandsSections_t = CIBlockSection::GetList(array(), array('IBLOCK_ID' => const_IBLOCK_ID_catalog, 'SECTION_ID' => const_IBLOCK_ID_catalog_SECT_ID_brands));
		while ($brandsSection = $brandsSections_t->Fetch())
		{
			$brandsSections[$brandsSection['CODE']] = $brandsSection;
		}		
		
		$brands = array();
		$brands_t = CIBlockElement::GetList(array('id' => 'asc'), array('IBLOCK_ID' => const_IBLOCK_ID_brands));
		while($brand = $brands_t->Fetch())
		{
			
			$brands[$brand['CODE']] = $brand;
		}
		
		$sects_to_add = array_diff(array_keys($brands), array_keys($brandsSections));
		$sects_to_del = array_diff(array_keys($brandsSections), array_keys($brands));
		
		if (!empty($sects_to_add))
		{
			foreach ($sects_to_add as $sectCode)
			{
				$bs = new CIBlockSection;
				$bs->Add(array(
				    'ACTIVE' => 'N',
				    'IBLOCK_ID' => const_IBLOCK_ID_catalog,
				    'IBLOCK_SECTION_ID' => const_IBLOCK_ID_catalog_SECT_ID_brands,
				    'NAME' => $brands[$sectCode]['NAME'],
				    'XML_ID' => $brands[$sectCode]['XML_ID'].'_brands',
				    'CODE' => $brands[$sectCode]['CODE']
				));
			}
		}
		if (!empty($sects_to_del))
		{
			foreach ($sects_to_del as $sectCode)
			{
				$bs = new CIBlockSection;
				$bs->Delete($brandsSections[$sectCode]['ID']);
			}
		}
		
		file_put_contents(file_BRANDS_UPP_SECTS, time());
	}
	return 'uppBrandsSections();';
}