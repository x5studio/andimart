<?php

//use \Bitrix\Iblock\PropertyIndex;
use Bitrix\Main\Loader;

function setCatalogSectionsToCalc()
{
	CModule::IncludeModule("iblock");

	$calcs = array();
    $calcs_t = CIBlockSection::GetList(Array("left_margin"=>"asc"),
        array('IBLOCK_ID' => const_IBLOCK_ID_catalog, 'SECTION_ID' => 631),
        false, array('ID', 'NAME', 'IBLOCK_SECTION_ID', 'IBLOCK_ID', 'UF_CALC_SECTS'));
    while ($calc = $calcs_t->Fetch())
    {
        $calcs[$calc['ID']] = $calc;
    }
	
	foreach ($calcs as $calc)
	{
		$ids = array();
		$elems = CIBlockElement::GetList(array(), array('IBLOCK_ID' => const_IBLOCK_ID_catalog, 'SECTION_ID' => $calc['UF_CALC_SECTS']));
		while ($elem = $elems->Fetch())
		{
			$ids[] = $elem['ID'];
			$sects = array();
			$sects_t = CIBlockElement::GetElementGroups($elem['ID']);
			while ($sect = $sects_t->Fetch())
			{
				$sects[] = $sect['ID'];
			}
			$sects[] = $calc['ID'];

			CIBlockElement::SetElementSection($elem['ID'], $sects);
			\Bitrix\Iblock\PropertyIndex\Manager::updateElementIndex(const_IBLOCK_ID_catalog, $elem['ID']);
		}
		
		$goods = CIBlockElement::GetList(array(), array(
		    'IBLOCK_ID' => const_IBLOCK_ID_catalog, 
		    'SECTION_ID' => $calc['ID'], 
		    '!ID' => $ids), false, false, array('ID'));
		while ($good = $goods->Fetch())
		{
			$sects = array();
			$sects_t = CIBlockElement::GetElementGroups($good['ID']);
			while ($sect = $sects_t->Fetch())
			{
				$sects[] = $sect['ID'];
			}
			unset($sects[array_search($calc['ID'], $sects)]);
			$sects = array_unique($sects);

			CIBlockElement::SetElementSection($good['ID'], $sects);
		}
	}
	
	return 'setCatalogSectionsToCalc();';
}
