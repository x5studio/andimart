<?php

AddEventHandler("main", "OnBeforeProlog", "sessionStartFunc");

function sessionStartFunc()
{
	global $APPLICATION;
	global $USER;
	
	CModule::IncludeModule("sale");
	
	
	if (strpos($APPLICATION->GetCurPage(), '/lichniy_cabinet/') === 0 && !$USER->IsAuthorized())
	{
		LocalRedirect('/auth/');
	}
}
