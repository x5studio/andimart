<?php
/**
 * Created by PhpStorm.
 * User: btrx
 * Date: 15.12.2018
 * Time: 9:36
 */

namespace XFive\User;


class EventHandlers
{
    public static function onUserAdd(&$arFields)
    {
        //\Bitrix\Main\Diag\Debug::writeToFile('', 'onUserAdd');

        //\Bitrix\Main\Diag\Debug::writeToFile($arFields, '$arFields');

        $arMailFields = $arFields;
        $arMailFields['PASSWORD'] = $arFields['CONFIRM_PASSWORD'];
        $arMailFields['USER_ID'] = $arFields['ID'];
        $arMailFields['STATUS'] = ($arFields['ACTIVE'] == 'Y' ? 'активен' : 'неактивен');
        $arMailFields['URL_LOGIN'] = $arFields['LOGIN'];
        \CEvent::Send('USER_INFO_WITH_PASSWORD', SITE_ID, $arMailFields);
    }
}


