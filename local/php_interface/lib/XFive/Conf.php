<?php
/**
 * Created by PhpStorm.
 * User: btrx
 * Date: 29.10.2018
 * Time: 16:39
 */

namespace XFive;


class Conf
{
    const CODE_IBLOCK_CATALOG = 20;
    const CODE_IBLOCK_BRANDS = 26;
    const CODE_IBLOCK_DISCOUNT_STICKERS = 33;
    const REVIEWS_IBLOCK_ID = 5;
}
