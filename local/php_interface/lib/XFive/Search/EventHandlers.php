<?php
/**
 * Created by PhpStorm.
 * User: Winer
 * Date: 16.09.2018
 * Time: 12:22
 */

namespace XFive\Search;


use Bitrix\Main\Diag\Debug;

class EventHandlers
{

    public static $arSynonyms = [];
    public static $synonyms = [];

    public static function getSynonims()
    {
        $dbHL = \Bitrix\Highloadblock\HighloadBlockTable::getList([
            "select" => ["*"],
            "filter" => [
                "TABLE_NAME" => \XFive\Search\Manager::HL_SYNONYMS_TABLE_NAME,
            ]
        ]);

        if ($hl = $dbHL->fetch()) {
            $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hl["ID"])->fetch();
            $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
            $synonymsClass = $entity->getDataClass();

            $dbMeasures = $synonymsClass::getList([
                "select" => ["*"],
            ]);
            $synonyms = [];
            $i = 0;
            while ($synonym = $dbMeasures->fetch()) {
                $t = [];
                $i2 = 0;
                foreach ($synonym["UF_WORDS"] as $word) {
                    $t[] = $word;
                    self::$synonyms[$word] = $i;
                    $i2++;
                }
                if ($t) {
                    self::$arSynonyms[$i] = $t;
                    $i++;
                }
            }
        }
    }

    public static function BeforeIndexHandler_synonyms($arFields)
    {
        if (!\Bitrix\Main\Loader::IncludeModule("highloadblock")) // подключаем модуль
            return $arFields;
        if (
            $arFields["MODULE_ID"] == "iblock"
            &&
            (
                $arFields['PARAM1']==''
                ||
                in_array(
                    $arFields['PARAM1'],
                    array(
                        \XFive\Conf::CODE_IBLOCK_CATALOG
                        /*,\XFive\Conf::CODE_IBLOCK_OFFERS*/
                    )
                )
            )
        ) {

            if (!self::$synonyms) {
                self::getSynonims();
            }

            $parts = explode(" ", $arFields["TITLE"]);
            $symStr = '';
            foreach ($parts as $index => $part) {
                $part = mb_strtolower(trim($part));
                if (isset(self::$synonyms[$part]) && isset(self::$arSynonyms[self::$synonyms[$part]])) {
                    $syms = self::$arSynonyms[self::$synonyms[$part]];
                    foreach ($syms as $sym) {
                        if ($sym == $part) continue;
                        if($symStr!='')
                            $symStr .= " " . $sym;
                        else
                            $symStr = $sym;
                    }
                }
            }
            if ($symStr != '') {
                $arFields['TITLE'] .= " [[ ".$symStr." ]]"; //x5 20180916 беру в двойные квадратные кавычки, чтобы потом в шаблоне регуляркой убирать синонимы
            }
        }

//        $arFields['TITLE'] = str_replace('-', '', $arFields['TITLE']);
//        $arFields['TITLE'] = str_replace('/', '', $arFields['TITLE']);
//        $arFields['TITLE'] = str_replace('\\', '', $arFields['TITLE']);
//        $arFields['TITLE'] = str_replace('=', '', $arFields['TITLE']);
//        $arFields['TITLE'] = str_replace('.', '', $arFields['TITLE']);

//        \Bitrix\Main\Diag\Debug::writeToFile('***************************************************** INDEX_BEGIN *****************************************************');
//        \Bitrix\Main\Diag\Debug::writeToFile($arFields, '$arFields');
//        \Bitrix\Main\Diag\Debug::writeToFile('***************************************************** INDEX_END *****************************************************');

        return $arFields; // вернём изменения
    }
}
