<?php
/**
 * Created by PhpStorm.
 * User: btrx
 * Date: 29.10.2018
 * Time: 16:23
 */

namespace XFive\Search;


class Manager
{
    /**
     * имя таблцы HL блока Синонимов
     */
    const HL_SYNONYMS_TABLE_NAME = "synonyms";

    /** Возвращает массив синонимов для переданного слова $word
     * Ищет с помощью getList с поиском подстроки по паттерну %word%
     * @param $word - слово
     * @return array - массив синонимов
     */
    public static function getWordSynonyms($word)
    {
        $synonyms = [];

        $dbHL = \Bitrix\Highloadblock\HighloadBlockTable::getList([
            "select" => ["*"],
            "filter" => [
                "TABLE_NAME" => self::HL_SYNONYMS_TABLE_NAME,
            ]
        ]);

        if ($hl = $dbHL->fetch()) {
            $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hl["ID"])->fetch();
            $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
            $synonymsClass = $entity->getDataClass();

            $dbMeasures = $synonymsClass::getList([
                "select" => ["*"],
                "filter" => [
                    "UF_WORDS" => "" . strtolower($word) . ""
                ]
            ]);
            $synonyms = [];
            while ($synonym = $dbMeasures->fetch()) {
                foreach ($synonym["UF_WORDS"] as $word) {
                    $synonyms[] = mb_strtolower($word);

                }
            }

        }
        return $synonyms;
    }

    /** Преобразует переданную строку поиска в новую строку с учетом синонимов.<br>
     * <br> На основе поиска синонимов в HL блоке<br>
     * Например: <br>
     * <ul>
     * <li>исходный запрос "зеленый ковер".</li>
     * <li>В HL блоке <b>Синонимы</b> есть запись о том что ковер, ковровые покрытия, ковролин - синонимы</li>
     * </ul>
     * <br>тогда для исходного запроса мы получим такой результат <i>зеленый (ковер или ковровые покрытия или ковролин)</i>
     * @param $query
     * @return string
     */
    public static function getSearchQuery($query)
    {
        $queryString = \preg_replace("/ {2,}/", " ", \trim($query));
        $queryWords = \explode(" ", $queryString);

        $synonymsMap = [];
        foreach ($queryWords as $word) {
            if (\strlen($word) > 2) {
                $wordSynonyms = self::getWordSynonyms($word);
                if (!empty($wordSynonyms)) {
                    $synonymsMap[$word] = $wordSynonyms;
                }
            }
        }

            foreach ($synonymsMap as $word => $synonyms) {
                $queryString = \str_replace($word, "(" . \implode($synonyms, " или ") . ")", $queryString);
            }

        if (isset($_REQUEST["debug"])) {
            echo "<pre>Исходный запрос:<br>";
            print_r($query);
            echo "</pre>";
            echo "<pre>Массив слов исходного запроса:<br>";
            print_r($queryWords);
            echo "</pre>";
            echo "<pre>Синонимы для слов:<br>";
            print_r($synonymsMap);
            echo "</pre>";
            echo "<pre>То что будет искаться:<br>";
            print_r($queryString);
            echo "</pre>";
        }

        return $queryString;
    }

}