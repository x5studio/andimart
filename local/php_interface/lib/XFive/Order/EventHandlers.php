<?php
/**
 * Created by PhpStorm.
 * User: btrx
 * Date: 15.12.2018
 * Time: 9:27
 */

namespace XFive\Order;


class EventHandlers
{
    public static function onOrderSaved($event)
    {
//        $order = $event->getParameter("ENTITY");
//        $oldValues = $event->getParameter("VALUES");

        //$parameters = $event->getParameters();

        //\Bitrix\Main\Diag\Debug::writeToFile('', 'onOrderSaved');

        //\Bitrix\Main\Diag\Debug::writeToFile($parameters, '$parameters');
    }

    public static function onOrderCreated($order, &$arUserResult, $request, &$arParams, &$arResult, &$arDeliveryServiceAll, &$arPaySystemServiceAll)
    {
//        \Bitrix\Main\Diag\Debug::writeToFile('', 'onOrderCreated');
//
//        \Bitrix\Main\Diag\Debug::writeToFile($order, '$order');
//        \Bitrix\Main\Diag\Debug::writeToFile($arUserResult, '$arUserResult');
//        \Bitrix\Main\Diag\Debug::writeToFile($request, '$request');
//        \Bitrix\Main\Diag\Debug::writeToFile($arParams, '$arParams');
//        \Bitrix\Main\Diag\Debug::writeToFile($arResult, '$arResult');
//        \Bitrix\Main\Diag\Debug::writeToFile($arDeliveryServiceAll, '$arDeliveryServiceAll');
//        \Bitrix\Main\Diag\Debug::writeToFile($arPaySystemServiceAll, '$arPaySystemServiceAll');
    }
    public static function OnBeforeEventAddHandler(&$event, &$lid, &$arFields)
    {
        if($event == "SALE_STATUS_CHANGED_DS"){
            $order = \Bitrix\Sale\Order::load($arFields['ORDER_ID']);
            $propertyCollection = $order->getPropertyCollection();
            $ar = $propertyCollection->getArray();
            foreach ($ar['properties'] as $val){
                if($val['CODE']!="TRACKING_NUMBER") continue;
                else {
                    $arFields["TRACKING_NUMBER"] = $val['VALUE'];
                }
            }
        }
    }
}

