<?php
/**
 * Created by PhpStorm.
 * User: btrx
 * Date: 21.01.2019
 * Time: 10:36
 */

namespace XFive\AdminOrder;


use Bitrix\Main\Diag\Debug;

class EventHandlers
{
    public static function OnAdminListDisplay(&$list)
    {
//        \Bitrix\Main\Diag\Debug::writeToFile('', 'OnAdminListDisplay');
//        \Bitrix\Main\Diag\Debug::writeToFile($list->table_id, '$list->table_id');
//
//        \Bitrix\Main\Diag\Debug::writeToFile(get_class($list), 'get_class($list)');
//        //\Bitrix\Main\Diag\Debug::writeToFile(get_object_vars($list), 'get_object_vars($list)');
//        \Bitrix\Main\Diag\Debug::writeToFile(get_class_methods($list), 'get_class_methods($list)');
//
//        $list->AddRow('1000', array('name' => 'value1'));
    }

//    public static function OnAdminSaleOrderView()
//    {
//        //\Bitrix\Main\Diag\Debug::writeToFile('', 'OnAdminSaleOrderView');
//
//        return array(
//            "TABSET" => "MyTabs",
//            "GetTabs" => array("XFive\AdminOrder\EventHandlers", "getTabs"),
//            "ShowTab" => array("XFive\AdminOrder\EventHandlers", "showTabs"),
//            "Action" => array("XFive\AdminOrder\EventHandlers", "onSave"),
//            "Check" => array("XFive\AdminOrder\EventHandlers", "onBeforeSave"),
//        );
//    }
//
//    function getTabs($args)
//    {
//        return array(
//            array(
//                "DIV" => "myTab1",
//                "TAB" => "текст1",
//                "TITLE" => "текст2",
//                "SORT" => 1
//            )
//        );
//    }
//
//    function showTabs($tabName, $args, $varsFromForm)
//    {
//        if ($tabName == "myTab1") {
//            echo "<span>текст3</span>";
//        }
//    }
//
//    function onBeforeSave($args)
//    {
//        return true;
//    }
//
//    function onSave($args)
//    {
//        return true;
//    }

    public static function onSaleAdminOrderInfoBlockShow($event)
    {
        //\Bitrix\Main\Diag\Debug::writeToFile('', 'onSaleAdminOrderInfoBlockShow');

        $order = $event->getParameter("ORDER");
        //$basket = $event->getParameter("ORDER_BASKET");

        $user_id = $order->getUserId();

        $ar_groups_ids = \CUser::GetUserGroup($user_id);

        //\Bitrix\Main\Diag\Debug::writeToFile($ar_groups_ids);

        $db_res = \CGroup::GetList(
            ($by="c_sort"),
            ($order="desc"),
            array('ID' => implode(' | ', $ar_groups_ids))
        );

        $result_array = [];
        while ($ar_res = $db_res->Fetch()) {
            //\Bitrix\Main\Diag\Debug::writeToFile($ar_res);
            $result_array[] = array(
                //'TITLE' => 'Группа, ID = ' . $ar_res['ID'] . ' :',
                'TITLE' => 'Группа:',
                'VALUE' => $ar_res['NAME'],
                'ID' => 'group_id_' . $ar_res['ID'] . '_name'
            );
        }

        return new \Bitrix\Main\EventResult(
            \Bitrix\Main\EventResult::SUCCESS,
            $result_array
        );
    }
}

