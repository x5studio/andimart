<?php
/**
 * Created by PhpStorm.
 * User: btrx
 * Date: 30.10.2018
 * Time: 11:10
 */

namespace XFive\Product;


class EventHandlers
{
    const ProductCatalogIblockId = 20;

    public static function onAfterAdd($ID, $arFields)
    {
        // Здесь заполняем размеры товара средними значениями, полученными из объема товара

        if ($arFields['IBLOCK_ID'] == self::ProductCatalogIblockId) {
            \XFive\Product\DimensionsFiller::fill(self::ProductCatalogIblockId, $ID);
        }
    }
}
