<?php

namespace XFive\Product;


class DimensionsFiller
{
    const RequisitesPropertyId = 631;
    const VolumeSubPropertyDescription = 'Объем';
    const DimensionsPropertyId = 1125;

    public static function fill($iblockId, $productId)
    {
        $exactDimension = self::getDimensionById($iblockId, $productId);

        if ($exactDimension[0]) {
            \CCatalogProduct::Update($productId, array(
                'WIDTH'  => $exactDimension[1],
                'LENGTH' => $exactDimension[2],
                'HEIGHT' => $exactDimension[0],
            ));
        } else {

            $averageDimension = self::countByProductId($iblockId, $productId);

            \CCatalogProduct::Update($productId, array(
                'WIDTH'  => $averageDimension,
                'LENGTH' => $averageDimension,
                'HEIGHT' => $averageDimension,
            ));
        }
    }

    public static function fillAll($iblockId)
    {
        $res = \CIBlockElement::GetList(array(), array('IBLOCK_ID' => $iblockId), false, false, array('ID'));
        while ($el = $res->Fetch()) {
            \XFive\Product\DimensionsFiller::fill($iblockId, $el['ID']);
        }
    }

    private static function countByProductId($iblockId, $productId)
    {
        // Здесь добываем объем товара и вызываем countByProductVolume()

        $res = \CIBlockElement::GetProperty($iblockId, $productId, array(), array('ID' => self::RequisitesPropertyId));
        $volume = 0;
        while ($ob = $res->GetNext()) {
            if ($ob['DESCRIPTION'] == self::VolumeSubPropertyDescription) {
                $volume = $ob['VALUE'];
                break;
            }
        }

        return self::countByProductVolume($volume);
    }

    private static function countByProductVolume($volume)
    {
        return 10 * pow($volume * 100 * 100 * 100, 1 / 3);
    }

    private static function getDimensionById($iblockId, $productId)
    {
        //Get real dimension

        $res = \CIBlockElement::GetProperty($iblockId, $productId, array(), array('ID' => self::DimensionsPropertyId));
        $ob = $res->GetNext();
        $dimension = explode('x', $ob["VALUE_ENUM"]);

        return $dimension;
    }
}
