<?php
/**
 * Created by PhpStorm.
 * User: btrx
 * Date: 29.10.2018
 * Time: 15:58
 */

namespace XFive\Iblock;


class IblockElementBrandStringFiller
{
    const BRANDS_IBLOCK_ID = 26;
    const CATALOG_IBLOCK_ID = 20;

    public static function fillAll()
    {
        $brandNames = [];

        $rsBrands = \CIBlockElement::GetList(array(), array('IBLOCK_ID' => self::BRANDS_IBLOCK_ID));
        while ($arBrand = $rsBrands->Fetch()) {
            $brandNames[$arBrand['XML_ID']] = $arBrand['NAME'];
        }

        $rsElems = \CIBlockElement::GetList
        (
            array(),
            array('IBLOCK_ID' => self::CATALOG_IBLOCK_ID),
            false,
            false,
            array("ID", "IBLOCK_ID", 'PROPERTY_BREND')
        );
        while ($arElem = $rsElems->Fetch()) {
            \CIBlockElement::SetPropertyValuesEx($arElem['ID'], self::CATALOG_IBLOCK_ID, array('SEARCH_BREND' => $brandNames[$arElem['PROPERTY_BREND_VALUE']]));
        }
        return "XFive\Iblock\IblockElementBrandStringFiller::fillAll();";
    }
}
