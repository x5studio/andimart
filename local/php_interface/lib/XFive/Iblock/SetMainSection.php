<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 24.05.2019
 * Time: 9:28
 */

namespace XFive\Iblock;


class SetMainSection
{

    const CATALOG_ID = 20;
    const USER_GROUP_ID = 13;

    /**
     * @return array|bool
     * @throws \Bitrix\Main\LoaderException
     * Checks for differences between MAIN_TAG and Main Section's ID
     */
    public static function checkDifference() {
        if (\Bitrix\Main\Loader::includeModule('iblock')) {
            $IBLOCK_ID = self::CATALOG_ID;
            $arElement = array();
            $arSections = array();
            $arSelect = Array("ID", "NAME", "IBLOCK_SECTION_ID", "PROPERTY_MAIN_TAG");
            $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y");
            $res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            while ($arFields = $res->Fetch())
            {
                if ($arFields["PROPERTY_MAIN_TAG_VALUE"] == "00000000-0000-0000-0000-000000000000") {
                    //echo $arFields["ID"] ." is not valid"."\r\n";
                    continue;
                }
                if (!empty($arFields["PROPERTY_MAIN_TAG_VALUE"])){
                    $arElement[$arFields["ID"]]["MAIN_TAG"] = $arFields["PROPERTY_MAIN_TAG_VALUE"];
                    $arElement[$arFields["ID"]]["MAIN_SECTION_ID"] = $arFields["IBLOCK_SECTION_ID"];
                }
//                if (empty($arFields["PROPERTY_MAIN_TAG_VALUE"])){
//                    echo $arFields["ID"] ." XML empty"."\r\n";
//                }
            }
            $arSections = array();
            $db_groups = \CIBlockElement::GetElementGroups(array_keys($arElement), true);
            while($ar_group = $db_groups->Fetch())
            {
                $arSections[$ar_group["ID"]] = $ar_group["XML_ID"];
            }
            $arIDs =  array();
            foreach($arElement as $key=>$arItem) {
                if ($arItem["MAIN_TAG"] != $arSections[$arItem["MAIN_SECTION_ID"]]) {
                    $arIDs[] = $key;
                }
            }
            return $arIDs;
        }
        return false;
    }

    /**
     * @param $PRODUCT_ID
     * @return bool
     * @throws \Bitrix\Main\LoaderException
     * Changes Main Section ID by Element ID
     */
    public static function changeElementMainSectionID ($PRODUCT_ID) {
        if (\Bitrix\Main\Loader::includeModule('iblock')) {
            $IBLOCK_ID = self::CATALOG_ID;
            $arElement = array();
            $arSelect = Array("ID", "NAME", "IBLOCK_SECTION_ID", "PROPERTY_MAIN_TAG");
            $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ID" => $PRODUCT_ID);
            $res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            if ($arFields = $res->Fetch())
            {
                if (!empty($arFields["PROPERTY_MAIN_TAG_VALUE"])){
                    $arElement[$arFields["ID"]]["MAIN_TAG"] = $arFields["PROPERTY_MAIN_TAG_VALUE"];
                    $arElement[$arFields["ID"]]["MAIN_SECTION_ID"] = $arFields["IBLOCK_SECTION_ID"];
                }
            }
            $arSections = array();
            $db_old_groups = \CIBlockElement::GetElementGroups($PRODUCT_ID, true);
            while($ar_group = $db_old_groups->Fetch())
            {
                $arSections[$ar_group["ID"]] = $ar_group["XML_ID"];
            }
            $arElement[$PRODUCT_ID]["SECTIONS"] = $arSections;
            if ($arElement[$PRODUCT_ID]["SECTIONS"][$arElement[$PRODUCT_ID]["MAIN_SECTION_ID"]] != $arElement[$PRODUCT_ID]["MAIN_TAG"]) {
                $el = new \CIBlockElement;

                $arLoadProductArray = Array(
                    "IBLOCK_SECTION_ID" => array_keys($arElement[$PRODUCT_ID]["SECTIONS"],$arElement[$PRODUCT_ID]["MAIN_TAG"])[0],
                    "IBLOCK_SECTION" => array_keys($arElement[$PRODUCT_ID]["SECTIONS"])
                );
                $db_res = $el->Update($PRODUCT_ID, $arLoadProductArray);

                if ($db_res ) {
                    print_r("updated");
                }
                else {
                    print_r("notupdated");
                }
            }
            else {
                print_r("equal");
            }
            return true;
        }
        return false;
    }

    /**
     * @param array $IDs
     * Multiple change for Main Section's IDs by Array of Element's ID
     */
    public static function changeMainSectionsIDs($IDs) {
        if (\Bitrix\Main\Loader::includeModule('iblock')) {
            $IBLOCK_ID = self::CATALOG_ID;
            $arElement = array();
            $arSelect = Array("ID", "NAME", "IBLOCK_SECTION_ID", "PROPERTY_MAIN_TAG");
            $arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ID" => $IDs);
            $res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            while ($arFields = $res->Fetch())
            {
                if ($arFields["PROPERTY_MAIN_TAG_VALUE"] == "00000000-0000-0000-0000-000000000000") {
                    //echo $arFields["ID"] ." is not valid"."\r\n";
                    continue;
                }
                if (!empty($arFields["PROPERTY_MAIN_TAG_VALUE"])){
                    $arElement[$arFields["ID"]]["MAIN_TAG"] = $arFields["PROPERTY_MAIN_TAG_VALUE"];
                    $arElement[$arFields["ID"]]["MAIN_SECTION_ID"] = $arFields["IBLOCK_SECTION_ID"];
                }
            }
            $db_groups = \CIBlockElement::GetElementGroups(array_keys($arElement), true);
            while($ar_group = $db_groups->Fetch()) {
                $arElement[$ar_group["IBLOCK_ELEMENT_ID"]]["SECTIONS"][$ar_group["ID"]] = $ar_group["XML_ID"];
            }
            var_dump($arElement);
            foreach ($arElement as $key=>$arItem) {
                if ($arElement[$key]["SECTIONS"][$arElement[$key]["MAIN_SECTION_ID"]] != $arElement[$key]["MAIN_TAG"]) {
                    $el = new \CIBlockElement;
                    $MAIN_SECTION_ID = array_shift(\CIBlockSection::GetList(array(), array(
                        "IBLOCK_ID" => $IBLOCK_ID,
                        "XML_ID" => $arElement[$key]["MAIN_TAG"]
                    ), false, array("ID") )->fetch());
                    $SECTIONS = array_keys($arElement[$key]["SECTIONS"]);
                    $SECTIONS[] = (int)$MAIN_SECTION_ID;
                    $arLoadProductArray = Array(
                        "IBLOCK_SECTION_ID" => $MAIN_SECTION_ID,
                        "IBLOCK_SECTION" => $SECTIONS
                    );

                    var_dump($key);
                    var_dump($arLoadProductArray);
                    $db_res = $el->Update($key, $arLoadProductArray);

                    if ($db_res ) {
                        echo "$key updated"."\r\n";
                    }
                    else {
                        echo "$key notupdated"."\r\n";
                    }
                }
                else {
                    print_r("equal");
                }
            }
            return true;
        }
        return false; 
    }

}
