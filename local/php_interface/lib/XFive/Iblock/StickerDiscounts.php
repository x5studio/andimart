<?php
/**
 * Created by PhpStorm.
 * User: Winer
 * Date: 07.05.2019
 * Time: 14:01
 */

namespace XFive\Iblock;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc as Loc,
    Bitrix\Main\Loader,
    Bitrix\Main\Config\Option,
    Bitrix\Sale\Delivery,
    Bitrix\Sale\PaySystem,
    Bitrix\Sale,
    Bitrix\Sale\Order,
    Bitrix\Sale\DiscountCouponsManager,
    Bitrix\Main\Context;

/** x5 20190508 в данный момент класс не используется, так как было принято решение создать спецгруппу пользователей и вешать скидки на нее,
 * а в result_modifier получать GetOptimalPrice для той специальной группы пользователей
 * Единственный минус в том решении - GetOptimalPrice дает утечку памяти, с другой стороны там все кэшируется, поэтому не должно стать проблемой
 *
 * Данный класс агентом рассчитывал скидки в карточке и в заказе и сохранял в специальное свойство список Id скидок.
 * Class StickerDiscounts
 * @package XFive\Iblock
 */
class StickerDiscounts
{

    const PROP_BASKET_RULES_IDS = "BASKET_RULES_IDS";
    const AGENT_FUNCTION = "XFive\Iblock\StickerDiscounts::setDiscountsForProductsAgent();";

    /** этот агент запускается раз в сутки и запускает другого агента, который выполняется ежеминутно
     * @return string
     */
    public static function addSetDiscountsForProductAgent(){
        $delay_sec = 86400;
        $function = self::AGENT_FUNCTION;

        $arAgent = \CAgent::GetList(array(), array("NAME" => $function))->Fetch();

        if($arAgent){
            \Bitrix\Main\Diag\Debug::dumpToFile(date('Y-m-d H:i:s')." local/php_interface/lib/XFive/Iblock/StickerDiscounts.php:35 сдвигаем агента для расчета скидок");
            \CAgent::Update($arAgent['ID'], array("NEXT_EXEC"=>date('d.m.Y H:i:s',time()+60))); //обновление агента
        } else {

            \Bitrix\Main\Diag\Debug::dumpToFile(date('Y-m-d H:i:s')." local/php_interface/lib/XFive/Iblock/StickerDiscounts.php:41 устанавливаем агента для расчета скидок");
            //вешаем агента
            \CAgent::AddAgent(
                $function, // имя функции
                "main",                          // идентификатор модуля
                "N",                                  // агент не критичен к кол-ву запусков
                180,                                // интервал запуска - 1 минута
                date('d.m.Y H:i:s'),                // дата первой проверки на запуск
                "Y",                                  // агент активен
                date('d.m.Y H:i:s',time()+60)                // дата первого запуска + 60 секунд
            );
        }
        return "XFive\Iblock\StickerDiscounts::addSetDiscountsForProductAgent();";
    }


    public static function debug($var,$row=0,$text=''){
        $toFile = true;
        /*if($toFile) {
            if($text) \Bitrix\Main\Diag\Debug::dumpToFile(date('Y-m-d H:i:s') . " local/php_interface/lib/XFive/Iblock/StickerDiscounts.php:{$row} " . $text);
            if ($var) \Bitrix\Main\Diag\Debug::dumpToFile($var);
        } else {
            d($var);
        }*/
    }

    public static function setDiscountsForProductsAgent()
    {
        self::debug('',117,"=======================START STEP==========================");
        self::debug('',67,'setDiscountsForProductsAgent start');

        $memory_limit = (int) ini_get('memory_limit');
        self::debug($memory_limit,75,'memory_limit');
        Loader::IncludeModule('iblock');
        $iblockIds = array(\XFive\Conf::CODE_IBLOCK_CATALOG);
        sort($iblockIds);

        $lastIblockAndElement = \Bitrix\Main\Config\Option::get('x5', 'sticker_dicounts_last_iblockid_elementid', "0_0");
        $lastIblockAndElementExploded = explode("_", $lastIblockAndElement);
        $lastIblockId = $lastIblockAndElementExploded[0];
        $lastElementId = $lastIblockAndElementExploded[1];
        self::debug($lastIblockAndElementExploded,76,'lastIblockAndElementExploded');

        $lastProcessedId = 0;
        $amountProcessed = 0;
        foreach ($iblockIds as $IBLOCK_ID) {
            self::debug('',76,"foreach (iblockIds as {$IBLOCK_ID})");
            if($IBLOCK_ID<$lastIblockId) continue;
            $arSelect = Array("ID", "IBLOCK_ID", "NAME");
            $arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", /*"ID" => 10588,*/">ID"=>$lastElementId, "CATALOG_TYPE" => array(\Bitrix\Catalog\ProductTable::TYPE_PRODUCT, \Bitrix\Catalog\ProductTable::TYPE_OFFER));
            $res = \CIBlockElement::GetList(Array("ID" => "ASC"), $arFilter, false, array("nTopCount"=>500), $arSelect);
            while ($ob = $res->GetNextElement()) {
                $amountProcessed++;
                $arFields = $ob->GetFields();
                $lastProcessedId = $arFields['ID'];
                self::debug('',76,"while (ob = res->GetNextElement()) ID=".$arFields['ID']);
                //d($arFields);
                $prop = $ob->GetProperties(false, array('CODE' => self::PROP_BASKET_RULES_IDS));
                //d($prop);

                $elementDiscounts = self::getDiscountsByOptimalPrice($arFields['ID']);
                //d($elementDiscounts);
                $orderDiscounts = self::getOrderDiscounts($arFields['ID']);
                //d($orderDiscounts);
                $discountsIds = array_unique(array_merge($elementDiscounts, $orderDiscounts));

                $updateProp = true;
                if (!$discountsIds && !$prop['VALUE']) $updateProp = false;
                if ($prop[self::PROP_BASKET_RULES_IDS]['VALUE']) {
                    $diff = array_diff($prop[self::PROP_BASKET_RULES_IDS]['VALUE'], $discountsIds);
                    if (!$diff) $updateProp = false;
                    //d($diff);
                }
                if ($updateProp) {
                    //d(array(self::PROP_BASKET_RULES_IDS => $discountsIds));
                    \CIBlockElement::SetPropertyValuesEx($arFields['ID'], $arFields['IBLOCK_ID'], array(self::PROP_BASKET_RULES_IDS => $discountsIds));
                    self::debug(array(self::PROP_BASKET_RULES_IDS => $discountsIds),118,"обновление свойства для элемента {$arFields['ID']}");
                } else {
                    self::debug('',121,"Значение свойства не отличается для элемента {$arFields['ID']}");
                }
                $memory_used = round(memory_get_usage() / 1024 / 1024);
                self::debug('',124,"memory_usage:{$memory_used}M");
                if ($memory_used > $memory_limit*0.8) {
                    self::debug('',117,"цикл прерван, так как использование памяти превысило 80% от memory_limit");
                    break 2;
                }
            }
        }
        if($lastProcessedId==0) {
            self::debug('',117,"0_0 и return ''");
            \Bitrix\Main\Config\Option::set('x5', 'sticker_dicounts_last_iblockid_elementid', "0_0");
            self::debug('',117,"=======================FINISH TOTAL, processed={$amountProcessed}==========================");
            return "";
        } else {
            self::debug('',117,$IBLOCK_ID . "_" . $lastProcessedId." и return функция:завершение шага'");
            self::debug('',117,"=======================FINISH STEP, processed={$amountProcessed}==========================");
            \Bitrix\Main\Config\Option::set('x5', 'sticker_dicounts_last_iblockid_elementid', $IBLOCK_ID . "_" . $lastProcessedId);
            return self::AGENT_FUNCTION;
        }


    }


    public static function getOrderDiscounts($productId)
    {
        Loader::IncludeModule('sale');
        //$siteId = \Bitrix\Main\Context::getCurrent()->getSite();
        $siteId = 's1';

        $currencyCode = Option::get('sale', 'default_currency', 'RUB');

        DiscountCouponsManager::init();

        $order = Order::create($siteId, \CSaleUser::GetAnonymousUserID());

        $order->setPersonTypeId(1);
        //$basket = Sale\Basket::loadItemsForFUser(\CSaleBasket::GetBasketUserID(), $siteId)->getOrderableItems();

        // Создаём корзину
        $basket = \Bitrix\Sale\Basket::create($siteId);
        $item = $basket->createItem('catalog', $productId);
        $item->setFields(array(
            'QUANTITY' => 1,
            'CURRENCY' => $currencyCode,
            'LID' => $siteId,
            'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
        ));
        $order->setBasket($basket);

        /*Shipment*/
        $shipmentCollection = $order->getShipmentCollection();
        $shipment = $shipmentCollection->createItem();
        $shipmentItemCollection = $shipment->getShipmentItemCollection();
        $shipment->setField('CURRENCY', $order->getCurrency());
        foreach ($order->getBasket() as $item) {
            $shipmentItem = $shipmentItemCollection->createItem($item);
            $shipmentItem->setQuantity($item->getQuantity());
        }
        $arDeliveryServiceAll = Delivery\Services\Manager::getRestrictedObjectsList($shipment);
        $shipmentCollection = $shipment->getCollection();

        if (!empty($arDeliveryServiceAll)) {
            reset($arDeliveryServiceAll);
            $deliveryObj = current($arDeliveryServiceAll);

            if ($deliveryObj->isProfile()) {
                $name = $deliveryObj->getNameWithParent();
            } else {
                $name = $deliveryObj->getName();
            }

            $shipment->setFields(array(
                'DELIVERY_ID' => $deliveryObj->getId(),
                'DELIVERY_NAME' => $name,
                'CURRENCY' => $order->getCurrency()
            ));

            $shipmentCollection->calculateDelivery();
        }

        $order->doFinalAction(true);

        $testDiscount = $order->getDiscount();
        //d($testDiscount);
        $testDiscount->calculate();
        $calcResults = $testDiscount->getApplyResult(true);
        //d($calcResults);

        $discountsIds = [];
        foreach ($calcResults['DISCOUNT_LIST'] as $DISCOUNT) {
            $discountsIds[] = (int) $DISCOUNT['REAL_DISCOUNT_ID'];
        }
        return $discountsIds;
    }


    public static function getDiscountsByOptimalPrice($productId)
    {
        $discountsIds = [];
        $userGroups = \CUser::GetUserGroup(\CSaleUser::GetAnonymousUserID());
//        $userGroups[] = 1;
//        d($userGroups);
        $r = \CCatalogProduct::GetOptimalPrice($productId, 1, $userGroups);
        if (isset($r['DISCOUNT_LIST']) && $r['DISCOUNT_LIST']) {
            foreach ($r['DISCOUNT_LIST'] as $DISCOUNT) {
                $discountsIds[] = (int) $DISCOUNT['ID'];
            }
        }
        return $discountsIds;
    }

}