<?php
/**
 * Created by DM.
 * Date: 25.03.2019
 * Time: 14:30
 */

namespace XFive;


/**
 * Class GetContacts
 * @package XFive
 */
class GetContacts
{

    const SALE_REGIONS_TABLE_ID = 32;

    const SHOPS_TABLE_ID = 14;


    /**
     * @param $domain
     * @return array|bool
     */
    public static function getContactsByDomain($domain){
        \CModule::IncludeModule('iblock');
        $arIDs = array();
        $arTotalData = array();

//Getting shop's IDs from SALE_REGIONS_TABLE
        $arFilter = Array("IBLOCK_ID"=>self::SALE_REGIONS_TABLE_ID, "CODE"=>$domain, "ACTIVE"=>"Y");
        $arSelect = Array("ID", "NAME", "CODE", "PROPERTY_SHOPS");
        $db_res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        while($obj = $db_res->GetNextElement())
        {
            $artmpFields = $obj->GetFields();
            $arIDs[] = $artmpFields["PROPERTY_SHOPS_VALUE"];
        }

//Getting total info from SHOPS_TABLE
        if ($arIDs[0] !=  NULL) {
            $res = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => self::SHOPS_TABLE_ID, "ID" => $arIDs, "ACTIVE" => "Y"), false, false, Array());
            while ($ob = $res->GetNextElement()) {
                $arFields = $ob->GetFields();
                $arProps = $ob->GetProperties();
                $arTotalData[] = array_merge($arFields, $arProps);
            }
            return $arTotalData;
        }
        else return false;
    }

    /**
     * @param $domain
     * @return array|bool
     */
    public static function getContactsByDomainFromCache($domain){
        $result = [];
        $cache_dir = '/iblock/getcontacts';
        $obCache = new \CPHPCache();
        if ($obCache->InitCache(2592000, serialize(array($domain,'getcontacts')),$cache_dir)) {
            $result = $obCache->GetVars();
//            echo "<br>from cache<br>";
        } elseif ($obCache->StartDataCache()) {
//          echo "<br>not cache<br>";
//          echo "<br>".date('Y-m-d H:i:s')."<br>";
            $result = self::getContactsByDomain($domain);
            global $CACHE_MANAGER;
            if (defined('BX_COMP_MANAGED_CACHE')) {
//                echo "<br>".date('Y-m-d H:i:s')."<br>";
//                echo "<br>defined BX_COMP_MANAGED_CACHE<br>";
                $CACHE_MANAGER->StartTagCache($cache_dir);
                $CACHE_MANAGER->RegisterTag("iblock_id_" . SALE_REGIONS_TABLE_ID);
                $CACHE_MANAGER->RegisterTag("iblock_id_" . SHOPS_TABLE_ID);

                $CACHE_MANAGER->EndTagCache();
            }

            $obCache->EndDataCache($result);
        } else {
//          echo "<br>cache turned off<br>";
//          echo "<br>".date('Y-m-d H:i:s')."<br>";
            $result = self::getContactsByDomain($domain);
        }
        return $result;
    }
}