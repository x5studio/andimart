<?php
/**
 * Created by PhpStorm.
 * User: Winer
 * Date: 12.07.2018
 * Time: 16:54
 */

namespace XFive;


use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Loader;
use Bitrix\Main\ObjectNotFoundException;

class Brands
{
    /**
     * имя таблцы HL блока
     */
    const HL_TABLE_NAME = "b_brend";

    /**
     * @var DataManager
     */
    static $hlClass = NULL;

    /**
     * @param $id
     * @return array|false|mixed
     * @throws ObjectNotFoundException
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getById($id)
    {
        if (is_null(self::$hlClass)) {
            self::setHlClass();
        }

        $hlClass = self::$hlClass;
        $dbResult = $hlClass::getList([
            "filter" => [
                "UF_ID" => $id
            ]
        ]);
        $result = NULL;

        if ($item = $dbResult->fetch()) {
            $result = self::prepare($item);
        }

        return $result;
    }

    /**
     * @param $params
     * @return array
     * @throws ObjectNotFoundException
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getList($params = [])
    {
        if (is_null(self::$hlClass)) {
            self::setHlClass();
        }

        $hlClass = self::$hlClass;
        $dbResult = $hlClass::getList($params);
        $result = [];

        while ($item = $dbResult->fetch()) {
            $result[] = self::prepare($item);
        }

        return $result;
    }

    /**
     * @param array $hlArray
     * @return array
     */
    public static function prepare($hlArray)
    {
        return $hlArray;
    }

    /** Ищет класс для работы с hlblock "Картинки ilcats" и записывает его в статическое поле self::$hlClass
     * @throws ObjectNotFoundException
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    protected static function setHlClass()
    {
        Loader::includeModule("highloadblock");

        $dbHL = \Bitrix\Highloadblock\HighloadBlockTable::getList([
            "select" => ["*"],
            "filter" => [
                "TABLE_NAME" => self::HL_TABLE_NAME,
            ]
        ]);

        if ($hl = $dbHL->fetch()) {
            $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hl["ID"])->fetch();
            $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock); //генерация класса
            $class = $entity->getDataClass();

            self::$hlClass = $class;
        } else {
            throw new ObjectNotFoundException("Hlblock 'Бренд' не создан. Имя таблицы: " . self::$hlClass);
        }
    }
}