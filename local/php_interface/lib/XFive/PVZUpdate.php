<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 04.10.2018
 * Time: 16:47
 */

namespace XFive;

use Bitrix\Main\Loader;

class PVZUpdate
{
    const CITY_TABLE_ID = 3;  // Список городов

    const POINT_TABLE_ID = 29;  // Список точек выдачи


/**
 * @param $filename - JSON filename
 * @return array - result array of cities
 */
    public static function readJSON($filename) {
        $str = file_get_contents($filename);
        $arrTemp = json_decode($str, true);
        $obj = $arrTemp["PVZ"];
        unset($arrTemp);
        foreach ($obj as $city=>$items) {
            foreach ($items as $point_code => $item) {
                $obj[$city][$point_code]['Location'] = $item["cY"] . "," . $item["cX"];
                unset($obj[$city][$point_code]['Note']);
                unset($obj[$city][$point_code]['Dressing']);
                unset($obj[$city][$point_code]['Cash']);
                unset($obj[$city][$point_code]['Station']);
                unset($obj[$city][$point_code]['Metro']);
                unset($obj[$city][$point_code]['WeightLim']);
                unset($obj[$city][$point_code]['Site']);
                unset($obj[$city][$point_code]['Picture']);
                unset($obj[$city][$point_code]['cY']);
                unset($obj[$city][$point_code]['cX']);
            }
        }
        return $obj;
    }

/**
 * @param $arrCities - array of cities
 * @param $nmCity - filter city or ALL for all cities ti show
 */
    public static function printCity($arrCities, $nmCity){
        $city_not_found = true;
        foreach ($arrCities as $city=>$items) {
            if (($city == $nmCity) || ($nmCity == "ALL")){
                $city_not_found = false;
                echo "<b>" . $city . "</b><br>";
                foreach ($items as $item) {
                        echo "Пункт выдачи: ". $item["Name"]."<br>";
                        echo "Адрес: ". $item["Address"]."<br>";
                        echo "Телефон: ". $item["Phone"]."<br>";
                        echo "Время работы: ". $item["WorkTime"]."<br>";
                        echo "Местоположение: ". $item["Location"]."<br>";
                        echo "<br>";
                }
            }
            else continue;
        }
        if ($city_not_found) echo "Город <b>$nmCity</b> не найден! <br>";
    }

/**
 * @param $arrCities - array of cities
 */
    public static function addNewPoints($arrCities){
        $cnt = 0;
        $b_city = true;
        $arFields = array();
        $arFilter = array("IBLOCK_ID" => self::CITY_TABLE_ID);
        $res = \CIBlockElement::GetList(Array(), $arFilter);
        while ($row = $res->Fetch()) {
            foreach ($arrCities as $city=>$items) {
                foreach ($items as $item) {
                    if ($city == $row["NAME"]) {
                        $b_city = false;
                        $arFields = array(
                            "ACTIVE" => "Y",
                            "IBLOCK_ID" => self::POINT_TABLE_ID,
                            "NAME" => 'СДЕК ' . $city . " " . $item["Name"], //Город + Назание точки
                            "PROPERTY_VALUES" => array(
                                "CITY" => $row["ID"], // ID Города
                                "ADDRESS" => $item["Address"], // Адрес
                                "PHONE" => $item["Phone"] , // Телефон
                                "COORDINATES" =>$item["Location"], //Материал - свойство
                                "WORKTIME" => array
                                (
                                    "TEXT" =>  $item["WorkTime"], //Рабочее время
                                    "TYPE" => "HTML"
                                ),
                                'TRANSPORT_COMPANY' => '16216' //СДЕК
                            )
                        );
                        $oElement = new \CIBlockElement();
                        $idElement = $oElement->Add($arFields, false, false, true);
                        $cnt++;
                    }
                }

            }
        }
        if ($b_city) echo "Город <b>$filter_city</b> не найден!!!<br>";
        else echo "Успешно добавлено $cnt записей";
    }


}