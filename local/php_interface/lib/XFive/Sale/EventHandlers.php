<?php

namespace XFive\Sale;

use Bitrix\Sale\Order;
use Bitrix\Sale\Shipment;

class EventHandlers
{
    public static function OnOrderNewSendEmailHandler($ID, &$eventName, &$arFields)
    {
        if ($eventName == 'SALE_NEW_ORDER') {

            /** @var Order $order */
            $order = Order::load($ID);
            $shipment = $order->getShipmentCollection();

            /** @var Shipment $item */
            foreach ($shipment as $item) {
                if (!$item->isSystem()) {
                    $arFields['DELIVERY_NAME'] = $item->getDeliveryName();
                }
            }

            $property = $order->getPropertyCollection();
            $arFields['DELIVERY_ADDRESS'] = $property->getAddress()->getValue();
        }
    }
}