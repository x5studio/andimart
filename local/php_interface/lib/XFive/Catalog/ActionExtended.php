<?php
/**
 * Created by PhpStorm.
 * User: Winer
 * Date: 19.04.2019
 * Time: 15:26
 */

namespace XFive\Catalog;

/**
 * В этом классе мы получаем arResult и дополняем его массивом акций из инфоблока.
 * Class ActionExtended
 * @package XFive\Catalog
 */
class ActionExtended
{

    public static $discountStickersCached = array();
    const PARAM_NAME = "X5_DISCOUNT_STICKER";
    const USER_GROUP_ID = 13;

    public static function getDiscountStickersCached()
    { //можно закэшировать на сутки и тэгированный кэш сделать
        if (!self::$discountStickersCached) {
            self::$discountStickersCached = self::getDiscountStickersSettings();
        }
        return self::$discountStickersCached;
    }

    public static function processSectionResult(&$arResult, $idUser)
    {
        $prodIds = [];
        $skuIds = [];
        foreach ($arResult['ITEMS'] as $key => $ITEM) {
            if ($ITEM['CATALOG_TYPE'] == \Bitrix\Catalog\ProductTable::TYPE_PRODUCT) {
                $prodIds[] = $ITEM['ID'];
            } elseif ($ITEM['CATALOG_TYPE'] == \Bitrix\Catalog\ProductTable::TYPE_SKU) {
                $skuIds[] = $ITEM['ID'];
            }
        }
        if ($prodIds) {
            self::processSectionResultProduct($arResult, $prodIds, $idUser);
        }
        if ($skuIds) {
            self::processSectionResultSku($arResult, $skuIds, $idUser);
        }
    }

    /** дополняет массив arResult переменной с массивом скидок, заданных для специально созданной
     * группы пользователей Для плашек с попапом.
     * @param $arResult
     * @param $prodIds
     * @param $idUser
     */
    public static function processSectionResultProduct(&$arResult, $prodIds, $idUser)
    {
        $userGroups = \CUser::GetUserGroup($idUser);
        $userGroups[] = self::USER_GROUP_ID;
        $discStickers = self::getDiscountStickersCached();
        foreach ($arResult['ITEMS'] as &$ITEM) {
            if (in_array($ITEM['ID'], $prodIds)) {
                //if ($ITEM['MIN_PRICE'] && $ITEM['MIN_PRICE']['DISCOUNT_DIFF'] > 0) {
                    $r = \CCatalogProduct::GetOptimalPrice($ITEM['ID'], 1, $userGroups);
                    if (isset($r['DISCOUNT_LIST']) && $r['DISCOUNT_LIST']) {
                        foreach ($r['DISCOUNT_LIST'] as $DISCOUNT) {
                            if (isset($discStickers[$DISCOUNT['ID']])) {
                                $ITEM[self::PARAM_NAME][$DISCOUNT['ID']] = $discStickers[$DISCOUNT['ID']];
                            }
                        }
                    }
                    unset($r);
                //}
            }
        }
    }

    public static function processSectionResultSku(&$arResult, $skuIds, $idUser)
    {
        //код для торговых предложений не писал, так как на сайте не используются товары с торговыми предложениями
    }


    public static function processElementResult(&$arResult, $idUser)
    {
        if ($arResult['PRODUCT']['TYPE'] == \Bitrix\Catalog\ProductTable::TYPE_PRODUCT) {
            self::processElementResultProduct($arResult, $idUser);
            //TODO написать код на случай товара с торговыми предложениями
        } elseif ($arResult['PRODUCT']['TYPE'] == \Bitrix\Catalog\ProductTable::TYPE_SKU) {
            self::processElementResultSku($arResult, $idUser);
        }
    }

    public static function processElementResultProduct(&$arResult, $idUser)
    {
        $userGroups = \CUser::GetUserGroup($idUser);
        //x5 20190508 в результате обсуждения пришли к варианту, что задается скидки для специальной группы, а плашки отображаем у всех
        $userGroups[] = self::USER_GROUP_ID;
        //if ($arResult['MIN_PRICE']['DISCOUNT_DIFF'] > 0) {
            $discStickers = self::getDiscountStickersCached();
            $r = \CCatalogProduct::GetOptimalPrice($arResult['ID'], 1, $userGroups);
            if (isset($r['DISCOUNT_LIST']) && $r['DISCOUNT_LIST']) {
                foreach ($r['DISCOUNT_LIST'] as $DISCOUNT) {
                    if (isset($discStickers[$DISCOUNT['ID']])) {
                        $arResult[self::PARAM_NAME][$DISCOUNT['ID']] = $discStickers[$DISCOUNT['ID']];
                        //break;
                    }
                }
            }
        //}
        return;
    }

    public static function processElementResultSku(&$arResult, $idUser)
    {
        //код для торговых предложений не писал, так как на сайте не используются товары с торговыми предложениями
    }

    public static function getDiscountStickersSettings()
    {
        $resAr = [];
        $cache_dir = '/iblock/discountstickers';
        $obCache = \Bitrix\Main\Data\Cache::createInstance();
        if ($obCache->initCache(864000, serialize(array(\XFive\Conf::CODE_IBLOCK_DISCOUNT_STICKERS, 'discountstickes')), $cache_dir)) {
            $resAr = $obCache->getVars();
            //echo "<br>from cache<br>";
        } elseif ($obCache->startDataCache()) {
            //echo "<br>not cache<br>";
            $resAr = self::getDiscountStickersSettingsFromDB();
            global $CACHE_MANAGER;
            if (defined('BX_COMP_MANAGED_CACHE')) {
                $CACHE_MANAGER->StartTagCache($cache_dir);
                $CACHE_MANAGER->RegisterTag("iblock_id_" . \XFive\Conf::CODE_IBLOCK_DISCOUNT_STICKERS);
                $CACHE_MANAGER->EndTagCache();
            }
            $obCache->endDataCache($resAr);
        }
        return $resAr;
    }

    public static function getDiscountStickersSettingsFromDB()
    {
        $resAr = [];
        \Bitrix\Main\Loader::includeModule('iblock');
        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_TEXT", "DATE_ACTIVE_FROM", "PROPERTY_DISCOUNT_ID", "PROPERTY_COLOR", "PROPERTY_CAPTION", "PROPERTY_ICON", "PROPERTY_POSITION");
        $arFilter = Array("IBLOCK_ID" => \XFive\Conf::CODE_IBLOCK_DISCOUNT_STICKERS, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
        $res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $resAr[$arFields['PROPERTY_DISCOUNT_ID_VALUE']] = array(
                'discount_id' => $arFields['PROPERTY_DISCOUNT_ID_VALUE'],
                'color' => $arFields['~PROPERTY_COLOR_VALUE'] ? $arFields['~PROPERTY_COLOR_VALUE'] : "",
                'icon' => $arFields['~PROPERTY_ICON_VALUE'] ? $arFields['~PROPERTY_ICON_VALUE'] : "",
                'position' => $arFields['~PROPERTY_POSITION_VALUE'] ? $arFields['~PROPERTY_POSITION_VALUE'] : "",
                'caption' => $arFields['~PROPERTY_CAPTION_VALUE'] ? $arFields['~PROPERTY_CAPTION_VALUE'] : "",
                'popup_text' => $arFields["~PREVIEW_TEXT"]
            );
        }
        return $resAr;
    }

}
