<?php


namespace XFive;


use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Diag\Debug;
use Bitrix\Main\Loader;
use Bitrix\Main\UserTable;

class EventHandlers
{
    /**
     *  Обработчик события на окончание выгрузки из 1С
     */
    public static function onCompleteCatalogImport1CHandler()
    {
        Loader::includeModule('iblock');

        $dbElement = \CIBlockElement::GetList(
            Array('ID' => 'ASC'),
            Array('IBLOCK_ID' => const_IBLOCK_ID_catalog),
            false,
            false,
            Array('ID', 'PREVIEW_PICTURE', 'DETAIL_PICTURE')
        );

        $arPreviewPct = array();
        $arDetailPct = array();

        echo '<pre>';
        while ($arElement = $dbElement->Fetch()) {

            $prvPct = \CFile::GetByID($arElement['PREVIEW_PICTURE'])->Fetch();
            $dtlPct = \CFile::GetByID($arElement['DETAIL_PICTURE'])->Fetch();

            $arPreviewPct[$prvPct['EXTERNAL_ID']][] = array(
                'ELEMENT_ID' => $arElement['ID'],
                'FILE_ID'    => $prvPct['ID'],
                'FILE_NAME'  => $prvPct['FILE_NAME'],
                'WIDTH'      => $prvPct['WIDTH'],
                'HEIGHT'     => $prvPct['HEIGHT'],
                'FILE_SIZE'  => $prvPct['FILE_SIZE'],

            );
            $arDetailPct[$dtlPct['EXTERNAL_ID']][] = array(
                'ELEMENT_ID' => $arElement['ID'],
                'FILE_ID'    => $dtlPct['ID'],
                'FILE_NAME'  => $dtlPct['FILE_NAME'],
                'WIDTH'      => $dtlPct['WIDTH'],
                'HEIGHT'     => $dtlPct['HEIGHT'],
                'FILE_SIZE'  => $dtlPct['FILE_SIZE'],
            );
        }

//        foreach ($arPreviewPct as $k=>$pct) {
//            if(count($pct) > 1) {
//                $el = new \CIBlockElement;
//                $el->Update($pct['ELEMENT_ID'], array());
//
//                break;
//            }
//        }

        foreach ($arDetailPct as $k => $pct) {

            if (count($pct) > 1) {

//                $tempFileID = false;
//                for ($i = 0; $i < count($pct); $i++) {
//                    var_dump(2342);
//                    if ($i == 0) {
//                        $tempFileID = $pct[$i]['FILE_ID'];
//                    } else {
//                        \CFile::Delete($pct[$i]['FILE_ID']);
//
//                        $el = new \CIBlockElement;
//                        $res = $el->Update($pct[$i]['ELEMENT_ID'], array('DETAIL_PICTURE' => $tempFileID), false, false);
//
//                    }
//                }

//var_dump($pct);
                break;
            }
        }

    }

    public static function OnBeforeEventAddHandler(&$event, &$lid, &$arFields)
    {
        if ($event == "SALE_STATUS_CHANGED_DO") {

            $user = UserTable::getList(array(
                'filter' => array('=EMAIL' => $arFields['EMAIL']),
                'select' => array('NAME', 'LAST_NAME'),
                'limit'  => 1,
            ))->fetch();
            $arFields['ORDER_USER'] = $user['NAME'] . ' ' . $user['LAST_NAME'];
        }
    }


    public static function OnSuccessCatalogImport1C($arParams, $arFields)
    {
        \CModule::IncludeModule('iblock');

        $file_name = $arFields;

        if (strpos(basename($file_name), 'sales') === 0) {

            $xml_object = simplexml_load_file($file_name);

            $xml_object_vars = get_object_vars($xml_object);

            $offer_package_vars = get_object_vars($xml_object_vars['ПакетПредложений']);

            $iblock_xml_id = $offer_package_vars['ИдКаталога'];

            $offers_vars = get_object_vars($offer_package_vars['Предложения']);

            $ar_offers = $offers_vars['Предложение'];

            $iblock_id = -1;

            $db_res = \CIBlock::GetList(array(), array('XML_ID' => $iblock_xml_id));
            while ($ar_res = $db_res->Fetch()) {
                $iblock_id = $ar_res['ID'];
                break;
            }

            $ar_purchase_count = [];
            $ar_offer_keys = [];

            foreach ($ar_offers as $offer) {
                $offer_vars = get_object_vars($offer);

                $key = $offer_vars['Ид'];
                $value = $offer_vars['КоличествоПродаж'];

                $ar_purchase_count[$key] = $value;
                $ar_offer_keys[] = $key;
            }

            $ar_new_values = [];

            $db_res = \CIBlockElement::GetList(array(), array('IBLOCK_ID' => $iblock_id, 'XML_ID' => $ar_offer_keys));
            while ($ar_res = $db_res->Fetch()) {
                $ar_new_values[$ar_res['ID']] = $ar_purchase_count[$ar_res['XML_ID']];
            }

            foreach ($ar_new_values as $key => $value) {
                \CIBlockElement::SetPropertyValuesEx($key, $iblock_id, array('PURCHASE_COUNT' => $value));
            }

        }
    }

    /**
     * Значение SALES_NOTES переносим в элементы этого раздела
     */
    public static function OnAfterIBlockSectionUpdateHandler($arFields)
    {
        if ($arFields["RESULT"] && $_SERVER['SCRIPT_URL'] != '/bitrix/admin/1c_exchange.php') {
            $dbElem = \CIBlockElement::GetList(
                Array(),
                Array(
                    'IBLOCK_ID'                           => Conf::CODE_IBLOCK_CATALOG,
                    'SECTION_ID'                          => $arFields['ID'],
                    '!PROPERTY_SALES_NOTES_SECTION_VALUE' => 'Да',
                ),
                false,
                false,
                Array('ID')
            );

            while ($arElem = $dbElem->Fetch()) {
                \CIBlockElement::SetPropertyValuesEx(
                    $arElem['ID'],
                    Conf::CODE_IBLOCK_CATALOG,
                    array('SALES_NOTES' => $arFields['UF_SALES_NOTES'], 'PROMOS_HUCH' => $arFields['UF_PROMOS_HUCH'])
                );
            }
        }
    }
}
