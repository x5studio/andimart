$(function () {
  /**
   * Show-hide search form
   */
  var $body = $('body');
  var $header = $('#header');
  var $searchForm = $('#header-search-form');

  function hideMobileNav() {
    $body.removeClass('is-mobile-nav-show');
  }

  function hideSearch() {
    $searchForm.hide();
  }

  $(document).on('click', '.js-toggle-search', function (e) {
    e.preventDefault();
    hideMobileNav();
    $searchForm.animate({
      opacity: 'toggle'
    }, 300, function () {
      if ($searchForm.is(':visible')) {
        $searchForm.find('input').focus();
      } else {
        $searchForm.find('input').blur();
      }
    });
  });


  /**
   * Toggle mobile nav
   */

  $('.js-toggle-mobile-nav').on('click', function () {
    hideSearch();
    $('body').toggleClass('is-mobile-nav-show');
  });


  /**
   * Sticky header
   */

  var $headerFixed = $('#header-fixed');
  var headerHeight;

  function stickHeader() {
    var scrollTop = $(window).scrollTop();
    var offset = 80;
    $headerFixed.toggleClass('is-show', scrollTop >= headerHeight + offset)
  }

  function bindStickyHeader() {
    if ($.documentWidth() >= 992) {
      headerHeight = $header.outerHeight(true);
      stickHeader();
      $(document).on('scroll', stickHeader);
    } else {
      $(document).off('scroll', stickHeader);
    }
  }

  if ($.documentWidth() >= 992) {
    bindStickyHeader();
  }

  $(window).on('resize', bindStickyHeader);


  /**
   * Toggle city select (sm- screens)
   */

  var $mobileNav = $('#mobile-nav');
  $('#mobile-nav-city').on('click', function (e) {
    e.preventDefault();
    $mobileNav.toggleClass('is-show-city');
  });


  /**
   * Open city select (md screens)
   */

  $('.js-select-city-md').on('click', function (e) {
    e.preventDefault();
    var $this = $(this);
    var position = {
      top: $this.offset().top + $this.outerHeight(),
      left: $this.offset().left + $this.outerWidth() / 2
    };
    Popups.openCityPopup(position);
  })
});
