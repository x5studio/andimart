/**
 * Profile scripts
 */

$(function () {
  'use strict';

  if (undefined != $.magnificPopup) {
    $('.js-portfolio-add').magnificPopup({
      callbacks: {
        elementParse: function(item) {
          var category = $(item.el).data('category');
          // TODO: менять значение скрытых инпутов в форме, для определения в какую категорию добавить материал
          // http://dimsemenov.com/plugins/magnific-popup/documentation.html#api
        }
      }
    });
  }


  /**
   * Orders history
   */

  $(document).on('click', '.js-history-item__toggle', function (e) {
    e.preventDefault();
    var $this = $(this);
    var $item = $this.closest('.js-history-item');
    var $ajax = $item.find('.js-history-item__holder');

    if ($ajax) {
      var url = $this.attr('href');
      $ajax.removeClass('.js-history-item__holder');
      $.ajax({
        url: url,
        success: function(data){
          $ajax.replaceWith($(data));
        }
      });
    }

    $item.toggleClass('is-active');
  });
});
