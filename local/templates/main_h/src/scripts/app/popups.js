$(function () {

  if (undefined != $.magnificPopup) {
    $.extend(true, $.magnificPopup.defaults, {
      tClose: 'Close (Esc)', // Alt text on close button
      tLoading: 'Loading...', // Text that is displayed during loading. Can contain %curr% and %total% keys
      gallery: {
        tPrev: 'Previous (Left arrow key)', // Alt text on left arrow
        tNext: 'Next (Right arrow key)', // Alt text on right arrow
        tCounter: '%curr% / %total%' // Markup for "1 of 7" counter,
      },
      iframe: {
        markup: '<div class="mfp-iframe-scaler">' +
        '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
        '</div>'
      },
      image: {
        markup: '<div class="mfp-figure">' +
        '<div class="mfp-img"></div>' +
        '<div class="mfp-bottom-bar">' +
        '<div class="mfp-title"></div>' +
        '<div class="mfp-counter"></div>' +
        '</div>' +
        '</div>',
        tError: '<a href="%url%">The image</a> could not be loaded.' // Error message when image could not be loaded
      },
      ajax: {
        tError: '<a href="%url%">The content</a> could not be loaded.' // Error message when ajax request failed
      },
      autoFocusLast: false
    });

    $('.js-popup-video').magnificPopup({
      type: 'iframe',
      closeBtnInside: false
    });

    /**
     * Popups in portfolio galleries
     */

    $('.js-popup-gallery').each(function() {
      $(this).magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
          enabled: true
        },
        closeBtnInside: false,
        callbacks: {
          elementParse: function(item) {
            if(item.el.hasClass('js-popup-gallery__video')) {
              item.type = 'iframe';
            }
          }
        }
      });
    });


    /**
     * Default popup
     *
     */

    $('.js-popup').magnificPopup({
      type: 'inline'
    });
  }



  /**
   * Service for working with all popups
   * Include wrapper for http://dimsemenov.com/plugins/magnific-popup/documentation.html
   */

  (function (window) {

    var Popups = function () {
      this.initCityPopup();
    };

    /**
     * TODO: add wrap for magnific calls
     */


    /**
     * Show message in popup
     */

    Popups.prototype.showMessage = function (data) {
      var content = '';

      if (data.title) {
        content += '<div class="popup__title title-line title-line--accent is-animated">' + data.title + '</div>'
      }

      if (data.text) {
        content += '<div class="popup__text">' + data.text + '</div>'
      }

      $.magnificPopup.open({
        items: {
          src: $('<div class="popup">' + content + '</div>'), // Dynamically created element
          type: 'inline'
        }
      }, 0);
    };


    /**
     * City select popup (on md+ screens)
     */

    Popups.prototype.initCityPopup = function () {
      if (!this._initCityPopupNodes()) return false;

      this._setCityPopupPosition();

      var that = this;

      this.city.$anchor.on('click', function (e) {
        e.preventDefault();
        that.openCityPopup();
      });

      $(window).on('resize', function () {
        Utils.waitFinalEvent(function () {
          if ($.windowWidth() >= 992) {
            that._setCityPopupPosition();
          }
        }, 300, 'recalcCityPopupPosition')
      });
    };

    Popups.prototype.openCityPopup = function (ask) {
      var $active = ask ? this.city.$ask : this.city.$select;
      var $inActive = !ask ? this.city.$ask : this.city.$select;

      $inActive.removeClass('is-active');
      $active.addClass('is-active');

      this.city.$wrap.animate({
        opacity: 'show'
      }, 400);

      if (undefined !== Jump) {
        Jump('body');
      }
    };

    Popups.prototype.closeCityPopup = function () {
      this.city.$ask.removeClass('is-active');
      this.city.$select.removeClass('is-active');

      this.city.$wrap.animate({
        opacity: 'hide'
      }, 400);
    };

    Popups.prototype._initCityPopupNodes = function () {
      var $wrap = $('#js-what-city');
      var $anchor = $('#js-what-city-anchor');
      if ($wrap.length === 0 || $anchor.length === 0) return false;

      this.city = {
        $wrap: $wrap,
        $ask: $('#js-what-city-ask'),
        $select: $('#js-what-city-select'),
        $anchor: $anchor
      };

      return true;
    };

    Popups.prototype._setCityPopupPosition = function () {
      var $anchor = this.city.$anchor;
      var $ask = this.city.$ask;
      var $select = this.city.$select;
      var position = {
        top: $anchor.offset().top + $anchor.outerHeight(),
        left: $anchor.offset().left + $anchor.outerWidth() / 2
      };

      $ask.css({
        top: position.top,
        left: position.left
      });

      $select.css({
        top: position.top,
        left: position.left
      });
    };

    window['Popups'] = new Popups();
  })(window);
});
