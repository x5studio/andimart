$(function () {

  /**
   * Gallery
   */

  $('.js-product-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    asNavFor: '.js-product-thumbs',
    focusOnSelect: true
  });

  $('.js-product-thumbs').slick({
    slidesToShow: 3,
    centerPadding: 0,
    centerMode: true,
    slidesToScroll: 1,
    arrows: false,
    asNavFor: '.js-product-slider',
    focusOnSelect: true,
    mobileFirst: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 4
        }
      }
    ]
  });


  /**
   * Anchor links to reviews
   */

  var $tabReviews = $('a[href="#product-reviews"]');
  var $container = $tabReviews.closest('.js-island-tabs__scroll');


  function scrollToTab() {
    var left = $tabReviews.position().left;
    $container.animate({scrollLeft: left});
  }

  $(document).on('click', '.js-scroll-to-reviews', function () {
    Jump($tabReviews[0], {
      offset: -150,
      callback: function () {
        $tabReviews[0].click();
      }
    });
    scrollToTab();
  });

  $(document).on('click', '.js-scroll-to-reviews-form', function () {
    $tabReviews[0].click();
    scrollToTab();

    var reviewsForm = document.getElementById('review-form');
    if (reviewsForm) {
      Jump(reviewsForm, {
        offset: -150
      });
    } else {
      Jump($tabReviews[0], {
        offset: -150
      });
    }
  });
});
