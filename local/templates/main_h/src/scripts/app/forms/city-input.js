$(function () {
  // http://twitter.github.io/typeahead.js/examples/
  var substringMatcher = function(strs) {
    return function findMatches(q, cb) {
      var matches;
      var substringRegex;

      // an array that will be populated with substring matches
      matches = [];

      // regex used to determine if a string contains the substring `q`
      substringRegex = new RegExp(q, 'i');

      // iterate through the pool of strings and for any string that
      // contains the substring `q`, add it to the `matches` array
      $.each(strs, function(i, str) {
        if (substringRegex.test(str)) {
          matches.push(str);
        }
      });

      cb(matches);
    };
  };

  $('.js-city-input').one('focus', function () {
    var $input = $(this);
    var src = $input.data('cities-src');
    var cities;

    if (src) {
      $.get(src, function (data) {
        cities =  JSON.parse(data);
     // console.log(cities);
        $input.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
          },
          {
            name: 'cities',
            source: substringMatcher(cities)
          });

       $input.data('cities', cities);

        $input.focus(); // after init lose focus

        $input.on('typeahead:select', function () {
          $input.change();
        });

        $input.on('typeahead:change', function () {
          $input.change();
        });
      });
    }
  });
});
