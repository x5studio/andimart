$(function () {
  $('.js-city-select').each(function () {
    var $form = $(this);
    var $input = $form.find('.js-city-select__input');
    var $submit = $form.find('.js-city-select__submit');

    $input.on('change input', function () {
      var cities = $input.data('cities'); // src/scripts/app/forms/city-input.js
      if (cities) {
        var found = cities.indexOf($input.val()) !== -1;
        $submit.attr('disabled', !found);
        $input.toggleClass('is-invalid', !found);
      }
    });
alert(1);
    $form.on('submit', function (e) {
      $input.toggleClass('is-invalid', $input.val().length === 0);
      // e.preventDefault();
      // TODO: logic for change city
    });
  });
});
