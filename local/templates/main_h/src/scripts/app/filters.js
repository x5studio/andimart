/**
 * Catalog filters
 */

$(function () {
  'use strict';

  /**
   * Show/hide on sm-
   */

  $(document).on('click', '.js-toggle-filters-sm', function () {
    $('body')
      .toggleClass('is-show-filters')
      .trigger('catalogFiltersOpened');
  });


  /**
   * Input range
   * http://refreshless.com/nouislider/
   */

  if (noUiSlider) {
    $('.js-range').each(function () {
      var $this = $(this);
      var $scale = $this.find('.js-range-scale');
      var $fieldMin = $this.find('.js-range-min');
      var $fieldMinVal = $this.find('.js-range-min-val');
      var $fieldMax = $this.find('.js-range-max');
      var $fieldMaxVal = $this.find('.js-range-max-val');
      var min, max, step;
      var startMin, startMax;

      min = parseInt($this.data('range-min')) || 0;
      max = parseInt($this.data('range-max')) || 100;
      step = parseInt($this.data('range-step')) || 10;

      startMin = parseInt($fieldMin.val()) || min;
      startMax = parseInt($fieldMax.val()) || max;

      if ($fieldMin.length != 0 && $fieldMax.length != 0 && $scale.length != 0) {
        noUiSlider.create($scale[0], {
          start: [startMin, startMax],
          step: step,
          connect: true,
          range: {
            'min': min,
            'max': max
          },
          format: wNumb({
            decimals: 0,
            thousand: ' '
          })
        });

        $scale[0].noUiSlider.on('update', function (values, handle) {
          var value = values[handle];
          if (handle) {
            $fieldMax[0].value = value;
            $fieldMaxVal[0].value = value.replace(' ', '');
          }
          else {
            $fieldMin[0].value = value;
            $fieldMinVal[0].value = value.replace(' ', '');
          }
        });

        $scale[0].noUiSlider.on('change', function (values, handle) {
          $fieldMin.change();
        });

        $fieldMin[0].addEventListener('change', function () {
          $scale[0].noUiSlider.set([this.value, null]);
        });

        $fieldMax[0].addEventListener('change', function () {
          $scale[0].noUiSlider.set([null, this.value]);
        });
      }
    });
  }


  /**
   *
   */

  var $filterForm = $('#filter');
  var $filterBubble = $('#filter-bubble');

  $filterForm.on('change', function (e) {
    var $target = $(e.target);
    var $block = $target.closest('.filter__b');
    $filterBubble.hide().removeClass('is-show');

    // TODO: ajax request....

    $filterBubble.appendTo($block);
    $filterBubble.show().addClass('is-show');
  });
});
