/**
 * Detect old browsers and show notice
 * https://github.com/andrey-hohlov/bobr-js
 */

bobr({
  test: 'ie10',
  callback: function (passed) {
    if (!passed) {
      alert('Вы используете устаревший браузер! Сайт может отображаться некорректно. Рекомендуем вам обновить браузер..');
    }
  }
});

/**
 * Common scripts
 */

$(function () {
  'use strict';

  /**
   * Add empty attribute ontouchstart to body
   * http://stackoverflow.com/questions/6063308/touch-css-pseudo-class-or-something-similar
   */

  if (window.Modernizr && Modernizr.touchevents) {
    document.body.setAttribute('ontouchstart', '');
  }


  /**
   * Fix Windows Phone Viewport
   */

  if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
    var msViewportStyle = document.createElement('style');
    msViewportStyle.appendChild(
      document.createTextNode(
        '@-ms-viewport{width:auto!important}'
      )
    );
    document.getElementsByTagName('head')[0].appendChild(msViewportStyle);
  }

  /**
   * Polyfill for external svg icons support
   * https://github.com/jonathantneal/svg4everybody
   */

  svg4everybody();


  /**
   * Toggle element visible with animation
   */

  $(document).on('click', '.js-toggle-el', function (e) {
    e.preventDefault();
    var $this = $(this);
    var $target = $($this.data('toggle-target'));
    var prop = $(this).data('toggle-prop') || 'height';
    var animate = {};
    animate[prop] = 'toggle';

    if ($target) {
      $target.animate(animate, 300, function () {
        $this.toggleClass('is-target-hidden', !$target.is(':visible'));
      })
    }
  });

  /**
   * Correct css dropdowns work on touch devices
   * http://osvaldas.info/drop-down-navigation-responsive-and-touch-friendly?utm_source=jquer.in&utm_medium=website&utm_campaign=content-curation
   * https://github.com/zenopopovici/DoubleTapToGo
   */

  if (undefined != $.fn.doubleTapToGo) {
    $('.js-has-drop').doubleTapToGo();
  }


  /**
   * Click on element when page loaded
   */

  $('.js-click-onload').click();


  /**
   * Scroll to target
   * https://github.com/callmecavs/jump.js
   */

  if (undefined !== Jump) {
    $(document).on('click', '.js-scroll-to', function (e) {
      e.preventDefault();
      var $this = $(this);
      var target = $this.data('scroll-to') || $this.attr('href');
      var offset = $this.data('scroll-offset') || 0;
      Jump(target, { offset: offset });
    });
  }


  /**
   * Spoiler
   */

  $(document).on('click', '.js-spoiler', function (e) {
    e.preventDefault();
    var $this = $(this);
    var target = $this.data('spoiler-target') || $this.attr('href');
    var $target = $(target);
    if ($target.length != 0) {
      $target.animate({
        height: 'toggle'
      }, 300);
      $this.toggleClass('is-expand');
    }
  });


  /**
   * Titles with line animation
   */

  var $titles = $('.js-title-line');
  $titles.each(function () {
    var $title = $(this);
    var inview = new Waypoint.Inview({
      element: this,
      entered: function(direction) {
        $title.addClass('is-animated');
        inview.destroy();
      }
    })
  });


  /**
   * Custom scroll
   * https://github.com/noraesae/perfect-scrollbar
   */

  if (undefined !== $.fn.perfectScrollbar) {
    var scrollBlockToChecked = function ($block) {
      var $current = $block.find('.is-current').first();
      var $checked = $block.find(':checked').first();
      var $scrollTarget =
        $current.length > 0 ? $current : $checked.length > 0 ? $checked.parent() : null;
      // Parent for checkbox because that hidden by css

      if ($scrollTarget) {
        $block.scrollTop($scrollTarget.position().top);
      }
    };

    $('.js-scroll-block').each(function () {
      var $block = $(this);
      $block.perfectScrollbar({
        suppressScrollX: true,
        theme: 'filters',
        minScrollbarLength: 16,
        maxScrollbarLength: 16
      });
      // Scroll to first checked element
      scrollBlockToChecked($block);
    });

    $('body').on('catalogFiltersOpened', function () {
      $('.js-scroll-block').each(function () {
        var $block = $(this);
        $block.perfectScrollbar('update');
        scrollBlockToChecked($block);
      });
    });
  }


  /**
   * Input masks
   * https://github.com/RobinHerbots/jquery.inputmask
   */

  if (undefined != $.fn.inputmask) {
    $('.js-mask-phone').inputmask('mask', {
      mask: '+7 (999) 9999999',
      placeholder: '*',
      clearIncomplete: true,
      showMaskOnHover: false,
      greedy: false
    });
  }

  /**
   * Init video youtube player on click
   */

  if (VideoPlayer) {
    $(document).on('click', '.js-youtube-init', function (e) {
      e.preventDefault();
      var $this = $(this);
      var url = $this.attr('href');
      var regExp = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
      var match = url.match(regExp);
      var id;

      if (match && match[1].length === 11) {
        id = match[1];
      }

      if (id) {
        VideoPlayer.init(id, this);
      }
    });
  }


  /**
   * "Focus" on element when hover
   */

  var $focusable = $('.js-focus-b');

  $focusable.on('mouseenter click', function () {
    var $this = $(this);
    if (!$this.hasClass('is-active')) {
      $focusable.removeClass('is-active');
      $this.addClass('is-active');
    }
  });


  /**
   * Stickyfill
   */

  $('.js-sticky').Stickyfill();
});
