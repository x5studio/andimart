$(function () {
  'use strict';

  function loadTabContentAjax(newTab) {

    if (!newTab || !newTab.content) return;
    var $ajaxBlock = $(newTab.content).find('.js-tab-ajax-content');
    if ($ajaxBlock.length != 0) {
      $ajaxBlock.removeClass('.js-tab-ajax-content');
      $.ajax({
        url: $ajaxBlock.data('src'),
        success: function(data){
          $ajaxBlock.replaceWith($(data));
        }
      });

        /*x5 этот участок кода уже был в local/templates/main/static/js/app.js, я просто скопировал его сюда begin*/
        if ($(newTab.content).is('[data-url]')) {
            window.history.pushState('', '', $(newTab.content).attr('data-url'));
        }
        /*x5 этот участок кода уже был в local/templates/main/static/js/app.js, я просто скопировал его сюда end*/

        /*x5 20180906 этот участок я написал в local/templates/main/static/js/app.js, сюда скопировал на случай, если кто-нибудь пересоберет исходники begin*/
        if ($(newTab.content).is('[data-x5-breadcrumb-name]')) {

            var html = "<a class=\"breadcrumbs__item-link\" href=\""+$(newTab.content).attr('data-url')+"\" itemprop=\"url\">" +
                "<span class=\"breadcrumbs__item-title\" itemprop=\"title\">"+$(newTab.content).attr('data-x5-breadcrumb-name')+"</span></a>";



            $breadcrumb_el = $('li.breadcrumbs__item.js-tab-inject');
            if ($breadcrumb_el.length <= 0) {
                var $breadcrumb_el = $('<li></li>', {
                    class: 'breadcrumbs__item js-tab-inject',
                    itemtype: 'http://data-vocabulary.org/Breadcrumb',
                    itemscope: 'itemscope',
                    html: html
                });
                $('li.breadcrumbs__item>span').parent().prepend($breadcrumb_el);
            } else {
                $breadcrumb_el.html(html);
            }
        } else {
            $('li.breadcrumbs__item.js-tab-inject').remove();
        }


        if ($(newTab.content).is('[data-x5-breadcrumb-name-after]')) {


            var html = "<span class=\"breadcrumbs__item-title\" itemprop=\"title\">" + $(newTab.content).attr('data-x5-breadcrumb-name-after') + "</span>";

            $breadcrumb_el = $('li.breadcrumbs__item.js-tab-inject-after');
            if ($breadcrumb_el.length <= 0) {

                var calc_breadcrumb = $('li.breadcrumbs__item>span').parent();

                if (calc_breadcrumb.length > 0) {
                    var html_calcs = "<a class=\"breadcrumbs__item-link\" href=\"/catalog/calcs/\" itemprop=\"url\">" +
                        "<span class=\"breadcrumbs__item-title\" itemprop=\"title\">" + calc_breadcrumb.html() + "</span></a>";
                    calc_breadcrumb.html(html_calcs);

                    var $breadcrumb_el = $('<li></li>', {
                        class: 'breadcrumbs__item js-tab-inject-after',
                        itemtype: 'http://data-vocabulary.org/Breadcrumb',
                        itemscope: 'itemscope',
                        html: html
                    });
                    calc_breadcrumb.after($breadcrumb_el);
                }
            } else {
                $breadcrumb_el.html(html);
            }

        } else {
            $('li.breadcrumbs__item.js-tab-inject-after').remove();
        }

        /*x5 20180906 этот участок я написал в local/templates/main/static/js/app.js, сюда скопировал на случай, если кто-нибудь пересоберет исходники end*/

    }
  }


  /**
   * Tabs in island block
   */

  $('.js-island-tabs').each(function () {
    var $wrap = $(this);
    var $scroll = $wrap.find('.js-island-tabs__scroll');
    var $current = $scroll.find('.island-tabs__item--current');
    var $left = $wrap.find('.js-island-tabs__l');
    var $right = $wrap.find('.js-island-tabs__r');
    var contentWidth = 0;

    $scroll.children().each(function() {
      contentWidth += $(this).outerWidth();
    });

    $scroll.perfectScrollbar({
      suppressScrollY: true
    });

    // Force scroll to current item
    if ($current.length != 0) {
      $scroll.scrollLeft($current.position().left - 30);

      $(window).on('resize', function () {
        $scroll.perfectScrollbar('update');
        $scroll.scrollLeft($current.position().left - 30);
      })
    }

    function toggleArrows() {
      var scrollWidth = $scroll.outerWidth();
      var scrollLeft = $scroll.scrollLeft();
      scrollLeft >= 30 ?  $left.show() : $left.hide();
      scrollLeft < contentWidth - scrollWidth - 30 ?  $right.show() : $right.hide();
    }

    toggleArrows();

    $scroll.on('scroll', toggleArrows);

    $(window).on('resize', toggleArrows)
  });


  /**
   * Closable tabs (category page)
   */

  $('.js-cls-tabs').each(function () {
    new Tabit(this, {
      buttonSelector: '.js-cls-tabs__btn',
      contentSelector: '.js-cls-tabs__content',
      buttonActiveClass: 'is-active',
      contentActiveClass: 'is-active',
      closable: true,
      activeIndex: -1,
      onChange: loadTabContentAjax
    });
  });


  /**
   * Radiotabs (registration form)
   */

  $('.js-radiotabs').each(function () {
    new Tabit(this, {
      buttonSelector: '.js-radiotabs__btn',
      contentSelector: '.js-radiotabs__content',
      buttonActiveClass: 'is-active'
    });
  });


  /**
   * Tabs
   */

  $('.js-tabs').each(function () {
    new Tabit(this, {
      buttonSelector: '.js-tabs__btn',
      contentSelector: '.js-tabs__content',
      buttonActiveClass: 'is-active',
      contentActiveClass: 'is-active',
      activeIndex: $(this).data('active') || 0,
      onChange: loadTabContentAjax
    });
  });
});
