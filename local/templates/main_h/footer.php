    <? if(PAGE_TYPE=="reviews" || CATALOG_CATEGORY=="Y"  || CATALOG_BRAND=="Y" || CATALOG_CALCS=='Y' ) { ?>
	<? if(CATALOG_CATEGORY!="Y" && CATALOG_BRAND!="Y"  && NO_WYSIWYG != 'Y' && CATALOG_CALCS != 'Y') { ?>
	</div> <!-- class="wysiwyg" -->
	<? } ?>
	<? if(CATALOG_CATEGORY=="Y") { ?>
				
				<!-- 	</div> .category-card -->
	<? } ?>
	<? if(SHOW_LEFT_MENU=="Y") { ?>
						 </div>
						 <?$APPLICATION->ShowViewContent('catalog_pagination');?>
						  </main>
						  <aside class="layout__aside">
						  <?
						  if(CATALOG_CATEGORY!="Y"){
						  ?>
                <nav class="pages-nav layout__aside--sticky js-sticky">
				<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"left", 
	array(
		"SHOW_ONLY_DEAPTH_LEVEL" => "1",
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "submenu",
		"USE_EXT" => "Y",
		"COMPONENT_TEMPLATE" => "inner"
	),
	false
);?>
                
                </nav>
				<?
						  }else{?>
							  <?$APPLICATION->ShowViewContent('category_menu_additional');?>
							  <?
						  }
				?>
				<?$APPLICATION->ShowViewContent('smart_filter');?>
              </aside>
							</div>
	<? } ?>
        </div><!-- content-block content-block--pull-top island -->
    <? } ?>
    
    <? if (PAGE_TYPE == "order") { ?>
	    </div><!-- content-block -->
	</div><!-- content-block content-block--pull-top island island--small -->
    <? } ?>
<? if (PERSONAL_ORDERS != 'Y') { ?>
</div> <!-- container -->
<? } ?>
</div> <!-- page__content-->
<? if (PAGE_TYPE == "order") { ?>
	    <footer class="page__footer">
        <div class="checkout-footer">
          <div class="container">
            <div class="island">
              <div class="row row--vcentr b-list">
                <div class="col-12 col-sm-6 b-list__item">
                  <div class="h3 title-line js-title-line">
                      <span>Доступные способы оплаты</span>
                  </div>
                  <a class="h5 no-text-decoration" target="_blank" href="/oplata_i_dostavka/"><span>ПОДРОБНЕЕ</span></a>
                </div>
                <div class="col-12 col-sm-6 col-md-5 text-sm-right b-list__item">
                  <img class="flexible-media" src="<?= SITE_TEMPLATE_PATH ?>/static/images/payments.png?v=91f419b8" width="409" height="38" alt>
                </div>
              </div>
            </div>
            <div class="checkout-footer__bot">
              <button class="checkout-footer__up js-scroll-to" data-scroll-to="body" type="button"></button>
              <div class="row">
                <div class="col-12 col-md-6">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => SITE_TEMPLATE_PATH."/include/footer/footer_copy.php"
                            )
                        );?>
                        </div>
                    <div class="col-12 col-md-6 text-md-right">
                        <div class="made-in-x5studio-tip dev-sign">
                            <a href="https://x5studio.ru/" class="dev-sign__link" target="_blank">Техническая поддержка сайта</a><span> X5studio</span>
                        </div>
                    </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
<? } else { ?>
<footer class="page__footer">
    <div class="footer">
        <div class="container">
            <div class="footer__top">
                <div class="content-block island">
                    <?$APPLICATION->IncludeComponent("asd:subscribe.quick.form", "footer_form", Array(
                        "FORMAT" => "text",	// Формат подписки
                        "INC_JQUERY" => "N",	// Подключить jQuery
                        "NOT_CONFIRM" => "N",	// Подписывать без подтверждения
                        "RUBRICS" => array(	// Подписывать на рубрики
                            0 => "1",
                        ),
                        "SHOW_RUBRICS" => "N",	// Показывать рубрики
                        'AJAX_MODE' => 'Y',
                    ),
                        false
                    );?>

                </div>
                <div class="content-block content-block--tight">
                    
			<?$APPLICATION->IncludeComponent(
				"bitrix:menu", 
				"social", 
				array(
					"SHOW_ONLY_DEAPTH_LEVEL" => "1",
					"ALLOW_MULTI_SELECT" => "N",
					"CHILD_MENU_TYPE" => "social",
					"DELAY" => "N",
					"MAX_LEVEL" => "1",
					"MENU_CACHE_GET_VARS" => array(
					),
					"MENU_CACHE_TIME" => "3600",
					"MENU_CACHE_TYPE" => "N",
					"MENU_CACHE_USE_GROUPS" => "Y",
					"ROOT_MENU_TYPE" => "social",
					"USE_EXT" => "Y",
					"COMPONENT_TEMPLATE" => ""
				),
				false
			);?>
                </div>
                <div class="content-block island">
                    <div class="seo-block">
                        <div class="row">
                            <div class="col-12 col-md-10">
                                <!-- Минимизируем негативное влияние на сео - изначально блок развернут, скрываем его после загрузки, вызвав клик по нему.-->
                                <div class="seo-block__title js-toggle-el js-click-onload" data-toggle-target="#seo-text">
                                    <div class="title-line js-title-line">
                                        <?$APPLICATION->IncludeComponent(
                                            "bitrix:main.include",
                                            "",
                                            Array(
                                                "AREA_FILE_SHOW" => "file",
                                                "AREA_FILE_SUFFIX" => "inc",
                                                "EDIT_TEMPLATE" => "",
                                                "PATH" => SITE_TEMPLATE_PATH."/include/footer/footer1.php"
                                            )
                                        );?>
                                        </div>
                                </div>
                                <div id="seo-text" class="seo-block__text">
                                    <div class="seo-block__text-in wysiwyg text-small">
                                        <?$APPLICATION->IncludeComponent(
                                            "bitrix:main.include",
                                            "",
                                            Array(
                                                "AREA_FILE_SHOW" => "file",
                                                "AREA_FILE_SUFFIX" => "inc",
                                                "EDIT_TEMPLATE" => "",
                                                "PATH" => SITE_TEMPLATE_PATH."/include/footer/footer2.php"
                                            )
                                        );?>
                                          </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-block hidden-sm-down">
                    <div class="flex flex--navs">
                        <div class="flex__item">
                            <nav class="nav-list">
                                <div class="nav-list__title">
                                    <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
	"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_TEMPLATE_PATH."/include/footer/footer_menu1.php"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "Y"
	)
);?>
                                    </div>
                                <?$APPLICATION->IncludeComponent("bitrix:menu", "footer", array(
	"SHOW_ONLY_DEAPTH_LEVEL" => "1",
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "catalog",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => "",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "footer1",
		"USE_EXT" => "Y",
		"COMPONENT_TEMPLATE" => "top_catalog_mobile"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "Y"
	)
);?>

                            </nav>
                            <nav class="nav-list">
                                <?$APPLICATION->IncludeComponent("bitrix:menu", "footer", array(
	"SHOW_ONLY_DEAPTH_LEVEL" => "1",
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "catalog",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => "",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "footer2",
		"USE_EXT" => "Y",
		"COMPONENT_TEMPLATE" => "top_catalog_mobile"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>

                            </nav>
                        </div>
                        <div class="flex__item">
                            <nav class="nav-list">
                                <div class="nav-list__title">
                                    <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
	"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_TEMPLATE_PATH."/include/footer/footer_menu2.php"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "Y"
	)
);?>
                                    </div>
                                <?$APPLICATION->IncludeComponent("bitrix:menu", "footer", array(
	"SHOW_ONLY_DEAPTH_LEVEL" => "1",
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "catalog",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => "",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "footer3",
		"USE_EXT" => "Y",
		"COMPONENT_TEMPLATE" => "top_catalog_mobile"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "Y"
	)
);?>

                            </nav>
                            <nav class="nav-list">
                                <?$APPLICATION->IncludeComponent("bitrix:menu", "footer", array(
	"SHOW_ONLY_DEAPTH_LEVEL" => "1",
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "catalog",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => "",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "footer4",
		"USE_EXT" => "Y",
		"COMPONENT_TEMPLATE" => "top_catalog_mobile"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>

                            </nav>
                        </div>
                        <div class="flex__item">
                            <nav class="nav-list">
                                <div class="nav-list__title">
                                    <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
	"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_TEMPLATE_PATH."/include/footer/footer_menu3.php"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "Y"
	)
);?>
                                    </div>
                                <?$APPLICATION->IncludeComponent("bitrix:menu", "footer", array(
	"SHOW_ONLY_DEAPTH_LEVEL" => "1",
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "catalog",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => "",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "footer5",
		"USE_EXT" => "Y",
		"COMPONENT_TEMPLATE" => "top_catalog_mobile"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "Y"
	)
);?>

                            </nav>
                        </div>
                        <div class="flex__item">
                            <nav class="nav-list">
                                <div class="nav-list__title">
                                    <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
	"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_TEMPLATE_PATH."/include/footer/footer_menu4.php"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "Y"
	)
);?>
                                    </div>
                                <?$APPLICATION->IncludeComponent("bitrix:menu", "footer", array(
	"SHOW_ONLY_DEAPTH_LEVEL" => "1",
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "catalog",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => "",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "footer6",
		"USE_EXT" => "Y",
		"COMPONENT_TEMPLATE" => "top_catalog_mobile"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "Y"
	)
);?>

                            </nav>
                        </div>
                        <div class="flex__item">
                            <nav class="nav-list">
                                <div class="nav-list__title">
                                    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_TEMPLATE_PATH."/include/footer/footer_menu5.php",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>
                                    </div>
                                <?$APPLICATION->IncludeComponent("bitrix:menu", "footer", array(
	"SHOW_ONLY_DEAPTH_LEVEL" => "1",
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "catalog",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => "",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "footer7",
		"USE_EXT" => "Y",
		"COMPONENT_TEMPLATE" => "top_catalog_mobile"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>

                            </nav>
                        </div>
                    </div>
                </div>
                <button class="footer__up js-scroll-to" data-scroll-to="body" type="button"></button>
            </div>
            <div class="footer__bottom">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => SITE_TEMPLATE_PATH."/include/footer/footer_copy.php"
                            )
                        );?>
                        </div>
                    <div class="col-12 col-md-6 text-md-right">
                        <div class="made-in-x5studio-tip dev-sign">
                            <a href="https://x5studio.ru/" class="dev-sign__link" target="_blank">Техническая поддержка сайта</a><span> X5studio</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<? } ?>
</div>
    <?

    ?>
    <?$APPLICATION->IncludeComponent(
	"ruformat:side_banners",
	"",
	array(
		"IBLOCK_ID"=>4,
	),
	false
);?>
    <div id="js-what-city" class="what-city">
        <!-- js для управления поп-апом выбора города лежит в src/scripts/app/popups.js-->
        <div class="what-city__mask" onclick="Popups.closeCityPopup();"></div>
        <div id="js-what-city-ask" class="what-city__popup">
            <button class="btn-close what-city__close" type="button" onclick="Popups.closeCityPopup();"></button>
            <div class="what-city__section">
                <div class="what-city__title">Ваш город: <?=$_SESSION["PEK_CURRENT_CITY_NAME"]?></div>
            </div>
            <div class="what-city__section">
                <button class="btn" type="button" onclick="SetCity();Popups.closeCityPopup();">
                    <span>Да, всё верно</span>
                </button>
                <button class="btn btn--link" type="button" onclick="Popups.openCityPopup();">
                    <span>Выбрать другой</span>
                </button>
            </div>
        </div>
        <div id="js-what-city-select" class="what-city__popup">
            <button class="btn-close what-city__close" type="button" onclick="Popups.closeCityPopup();"></button>
            <div class="what-city__section">
                <div class="what-city__title">Ваш город: <?=$_SESSION["PEK_CURRENT_CITY_NAME"]?></div>
                <div class="what-city__section">
	                <div class="row">
	                    <div class="col-8">
	                        <form class="search-form js-city-select" novalidate action="/">
	                            <!-- Логика автокомплита города в src/scripts/city-select-form.js-->
	                            <div class="input-group">
	                                <div class="input-group__item input-group__item--grow">
	                                    <input class="input-text js-city-select__input js-city-input" data-cities-src="/ajax/cities.php" name="set_city_name" type="text" required placeholder="Укажите ваш город">
	                                </div>
	                                <div class="input-group__item">
	                                    <button class="search-form__submit search-form__submit--arrow js-city-select__submit" type="submit" ></button>
	                                </div>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div><br>
                <div class="row city_list"></div>
                    <?/*
		    CModule::IncludeModule("iblock");
		    
                    $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","ACTIVE");
                    $arFilter = Array("IBLOCK_ID"=>CITIES_IBLOCK_ID, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", '!PROPERTY_LOCATION_ID' => false, '!PROPERTY_HAS_SAM' => false);
                    $res = CIBlockElement::GetList(Array('name' => 'asc'), $arFilter, false, false, $arSelect);
                    while($ob = $res->GetNextElement())
                    {
                        $arFields = $ob->GetFields();
                        $cities[] = $arFields;

                    }
                    $cities = array_chunk($cities, ceil(count($cities)/3));
                    foreach($cities as $cities_part){
                        ?>
                    <div class="col-4">
                        <?
                        foreach($cities_part as $city){
                            ?>
                            <div class="what-city__item">
                                <a class="link link--inverse" href="<?=$APPLICATION->GetCurPageParam("set_city=".$city["ID"], array("set_city")); ?>"><?=$city["NAME"]?></a>
                            </div>
                        <?
                        }
                        ?>
                        </div>
                    <?
                    }
                    */?>
            </div>
            
        </div>
    </div>



    <?

    ?>
    <?/*if($_SESSION["SHOW_CITIES_MODAL"]=="Y"){?>
<script>

    // Вызов поп-апа с вопросом о городе
    // Он в макетах был сделан только для больших экранов
    $(function() {
        if ($.documentWidth() >= 992) {
            Popups.openCityPopup(true);
        }
    });

</script>
    <? }
    $_SESSION["SHOW_CITIES_MODAL"]="N";
    */?>
<?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false): ?>
<!-- Yandex.Metrika counter --> <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter45162276 = new Ya.Metrika({ id:45162276, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/45162276" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114596980-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-114596980-1');
</script><!-- Global site tag (gtag.js) - Google Analytics -->

<?endif;?>
</body>
</html><?

if (!empty($_REQUEST['ajax_buy']) && $_REQUEST['ajax_buy'] == 'Y') { 
$APPLICATION->RestartBuffer();
//echo 222222222;
$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket.line",
	"header",
	Array(
		"HIDE_ON_BASKET_PAGES" => "Y",
		"PATH_TO_BASKET" => SITE_DIR."cart/",
		"PATH_TO_ORDER" => SITE_DIR."cart/order/",
		"POSITION_FIXED" => "N",
		"SHOW_AUTHOR" => "N",
		"SHOW_EMPTY_VALUES" => "Y",
		"SHOW_NUM_PRODUCTS" => "Y",
		"SHOW_PERSONAL_LINK" => "N",
		"SHOW_PRODUCTS" => "N",
		"SHOW_TOTAL_PRICE" => "Y"
	)
);
exit;
}