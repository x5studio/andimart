<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


		<div id="portfolio-add-photo" class="radiotabs__content js-radiotabs__content">
		    <form id="<?=$arParams['CODE']?>form-id" class="js-form-validator" novalidate 
			  enctype="multipart/form-data"
			  name="<?=$arParams['CODE']?>" action="<?=POST_FORM_ACTION_URI?>#portfolio_block" method="post">
			<?=bitrix_sessid_post()?>
			<input name="<?=$arParams['CODE']?>[BRAND]" class="portfolio-section_inp" type="hidden" value="">
			<div class="form-row text-small">Фото изображение должно быть в формате .jpg и не превышать по размеру 2МБ</div>
			<div class="form-row">
			    <div class="input-file input-group js-input-file">
				<div class="input-group__item input-group__item--grow">
				    <div class="input-text js-input-file__name"></div>
				</div>
				<label class="input-group__item">
				    <input class="input-file__input" type="file"  data-extlimitlist="jpg" data-maxfilesize="2097152"
					   name="<?=$arParams['CODE']?>[FILE]"
					   onchange="$(this).closest('.js-input-file').find('.js-input-file__name').text(this.files[0] ? this.files[0].name : '');">
				    <span class="btn btn--fluid btn--inverse input-file__btn">
					<span>Обзор</span>
				    </span>
				</label>
			    </div>
			</div>
			<div class="form-row form-row--push-top">
			    <button class="btn btn--fluid" type="submit">
				<span>Добавить</span>
			    </button>
			</div>
		    </form>
		</div>



<script>
//	$(document).ready(function(){
//		$("#applicationform-step3-id").click(function()
//		{
//			$("#applicationform-id").find('input.custom-form-field').val("");
//			$("#applicationform-id").find('select.custom-form-field').val("")
//			$("#applicationform-id").find('select.custom-form-field').change();   
//		});
//		$("#<?= $arParams['CODE'] ?>form-id").submit(function()
//		{
//			var url = $(this).attr('action');
//			var data = $(this).serialize();
//			
//			$.post(url, data, function(output)
//			{
//				if (output.RESULT === 'error')
//				{
//					$('.custom-form-field').removeClass('error');
//					$('.custom-form-select').removeClass('error');
//					
//					for (var i in output.ERRORS)
//					{
//						if ($("select.<?= $arParams['CODE'] ?>-"+output.ERRORS[i]+"-form-field").parents('.custom-form-select').length > 0)
//						{
//							$("select.<?= $arParams['CODE'] ?>-"+output.ERRORS[i]+"-form-field").parents('.custom-form-select').addClass('error');
//						}
//						else
//						{
//							$(".<?= $arParams['CODE'] ?>-"+output.ERRORS[i]+"-form-field").addClass('error');
//						}
//					}
//				}
//				else
//				{
//					$('.custom-form-field').removeClass('error');
//					$('.custom-form-select').removeClass('error');
//					
//					$('.filedapplication-form-step3').click();
//				}
//			}, 'json');
//			
//			return false;
//		});
//	});
</script>
<?
/*
  <form id="<?=$arParams['CODE']?>form-id" name="<?=$arParams['CODE']?>" action="<?=POST_FORM_ACTION_URI?>" method="post">
  <?=bitrix_sessid_post()?>
  <div>
  <? foreach ($arResult['FIELDS'] as $field) { ?>
  <? if (!in_array($field['CODE'], array('AGREE', 'SUBSCRIPTION'))) { ?>
  <? if ($field['PROPERTY_TYPE'] == 'L') { ?>
  <select name="<?=$arParams['CODE']?>[<?=$field['CODE']?>]" class="custom-form custom-form-field <?=$arParams['CODE']?>-<?=$field['CODE']?>-form-field" data-placeholder="<?=$field['NAME']?><? if ($field['IS_REQUIRED'] == 'Y') { ?>*<? } ?>">
  <option value=""></option>
  <? foreach ($field['VARIANTS'] as $variant) { ?>
  <option value="<?=$variant['ID']?>"><?=$variant['VALUE']?></option>
  <? } ?>
  </select>
  <? } else { ?>
  <input name="<?=$arParams['CODE']?>[<?=$field['CODE']?>]" placeholder="<?=$field['NAME']?><? if ($field['IS_REQUIRED'] === 'Y') { ?>*<? } ?>" type="text" name="" class="custom-form custom-form-field <?=$arParams['CODE']?>-<?=$field['CODE']?>-form-field"/>
  <? } ?>
  <? } ?>
  <? } ?>
  </div>
  <div class="action-form-step2">
  <input placeholder="Получить инвестиции" type="submit" name="" class="custom-form standart-speed" value="Получить инвестиции" style="float: right; margin-right: 0px;"/>
  <p>
  <? $variants = array_values($arResult['FIELDS']['AGREE']['VARIANTS']) ?>
  <input checked="checked" type="checkbox" name="<?=$arParams['CODE']?>[AGREE]" value="<?=$variants[0]['ID']?>" class="custom-form" id="iagreetotheproccess"/><label class="<?=$arParams['CODE']?>-AGREE-form-field custom-form-field" for="iagreetotheproccess">Я согласен на обработку персональных данных (<a href="/info.php" target="_blank">подробнее</a>)</label>
  </p>
  <p>
  <? $variants = array_values($arResult['FIELDS']['SUBSCRIPTION']['VARIANTS']) ?>
  <input checked="checked" type="checkbox" name="<?=$arParams['CODE']?>[SUBSCRIPTION]" value="<?=$variants[0]['ID']?>" class="custom-form" id="subscribeme"/><label class="<?=$arParams['CODE']?>-SUBSCRIPTION-form-field custom-form-field" for="subscribeme">подписаться на рассылку</label>
  </p>
  </div>
  </form>

 */?>