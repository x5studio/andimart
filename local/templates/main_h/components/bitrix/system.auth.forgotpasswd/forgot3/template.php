<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}


?>
<?
if(!empty($arParams["~AUTH_RESULT"])):
	$text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
?>
<?endif?>

<div id="popup-password-restore" class="popup">
						<div class="popup__title">Запрос пароля</div>
						<div class="popup__text">
							<!-- После успешной отправки в этом блоке вместо формы выводится текст: "На указанный e-mail выслано письмо с инструкцией о восстановлении пароля."-->
							<!-- Пример обработки и вывода ошибок формы смотри в src/scripts/app/forms/ask-form.js-->
							<form class="js-form-validator" novalidate  name="bform" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
							<?if($arResult["BACKURL"] <> ''):?>
									<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
							<?endif?>
									<input type="hidden" name="AUTH_FORM" value="Y">
									<input type="hidden" name="TYPE" value="SEND_PWD">
								<div class="content-block">Укажите e-mail который вы использовали при регистрации.
									<br>На него будут отправлены контрольная строка для смены пароля, а&nbsp;также&nbsp;ваши регистрационные данные.</div>
								<div class="content-block">
									<div class="form-error content-block" style="display:none;">E-mail не найден в нашей базе клиентов</div>
									<!-- После отправки заменить форму на текст: На указанный e-mail выслано письмо с инструкцией о восстановлении пароля.-->
									<div class="form-row row row--vcentr">
										<div class="col-12 col-sm-3">
											<label class="input-label h5" for="pass_restore_email">Выслать пароль на&nbsp;e-mail:</label>
										</div>
										<div class="col-12 col-sm-8">
											<input id="pass_restore_email" class="input-text" type="email" name="USER_LOGIN" value="<?=$arResult["LAST_LOGIN"]?>"  required>
											<input type="hidden" name="USER_EMAIL" />
										</div>
									</div>
									<div class="form-row row">
										<div class="col-12 col-sm-4">
											<button class="btn btn--fluid" type="submit"  name="send_account_info" value="<?=GetMessage("AUTH_SEND")?>">
												<span>Выслать</span>
											</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					
					
					


<script type="text/javascript">
document.bform.onsubmit = function(){document.bform.USER_EMAIL.value = document.bform.USER_LOGIN.value;};
document.bform.USER_LOGIN.focus();
</script>
