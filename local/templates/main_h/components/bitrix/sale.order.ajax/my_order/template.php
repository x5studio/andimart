<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
if($USER->isAdmin()) {
	/*echo "<pre>";
	print_r($arResult);
	echo "</pre>";*/
}
if (!empty($arResult['ORDER_ID']))
{
	include 'success.php';
}
elseif (empty($arResult['DELIVERY']))
{
	return false;
}
else
{
	$checkedDelivery = 0;
?>
<style>
    .tt-highlight {cursor: pointer}
	.tt-suggestion {cursor: pointer}
</style>
<?/*$APPLICATION->IncludeComponent(
	"ipol:ipol.sdekPickup",
	"",
	Array(
		"CITIES" => array(),
		"CNT_BASKET" => "N",
		"CNT_DELIV" => "N",
		"COUNTRIES" => array(),
		"FORBIDDEN" => array(),
		"NOMAPS" => "N"
	)
);*/?>
	<div class="content-block js-tabs">
	    <div class="box island">
		<div class="island-head-tabs">
		<?
// $DELIVERY_ID - код службы доставки

$ar_city = array("Краснодар", "Пятигорск");

?>
			<? foreach ($arResult['DELIVERY'] as $delivery) { ?>
				<? if ($delivery['CHECKED'] == 'Y') { ?>
					<? $checkedDelivery = $delivery['ID']; ?>
				<? } ?>
				<div class="island-head-tabs__item">
					<a class="island-head-tabs__link<? if ($delivery['CHECKED'] == 'Y') { ?> is-active<? } ?>" 
					   href="#" data-delivery="<?=$delivery['ID']?>">
					     <?//print_r($delivery)?>
						<div class="island-head-tabs__title"><?=clearDeliveryName($delivery['NAME'])?></div>
						<!--<div class="island-head-tabs__title"><?//echo (clearDeliveryName($delivery['NAME']) != "") ? $delivery['NAME'] : "Самовывоз";?></div>-->
					</a>
				</div>
					<?php /*
		echo $checkedDelivery;
		if(in_array($checkedDelivery,array("141","142","143")) ){?>
		<div id="sdek_choose_place">
			
		</div>
		<?php }*/?>
			<? } ?>
		</div>
	
		<div id="checkout-<?=$checkedDelivery?>" class="">
		    <form id='ORDER_FORM' class="form js-form-validator"
			  data-is_order='y'
			  data-is_ajax='y'
			  data-is_json='y'
			  data-add_params='&action=saveOrderAjax'
			  novalidate action="<?= $APPLICATION->GetCurPage(); ?>" 
			  method="POST" name="ORDER_FORM" enctype="multipart/form-data">
			<?
			echo bitrix_sessid_post();

			if (strlen($arResult['PREPAY_ADIT_FIELDS']) > 0)
			{
				echo $arResult['PREPAY_ADIT_FIELDS'];
			}
			?>
			<? foreach ($arResult['PERSON_TYPE'] as $person_type) { ?>
				<input type="hidden" name="PERSON_TYPE" value="<?=$person_type['ID']?>">
				<? break; ?>
			<? } 

			?>
         
			<input type="hidden" name="location_type" value="code">
			<input type="hidden" name="BUYER_STORE" id="BUYER_STORE" value="<?= $arResult['BUYER_STORE'] ?>">
			<input type="hidden" name="DELIVERY_ID" value="<?= $checkedDelivery ?>">
             
			<? if (!empty($arResult['PROPERTIES']['USER_INFO'])) { ?>
				<? foreach ($arResult['PROPERTIES']['USER_INFO'] as $prop) { ?>
					<input type="hidden" name="ORDER_PROP_<?=$prop['ID']?>" value="<?=$prop['PRINT_VALUE']?>">
				<? } ?>
			<? }         //echo $checkedDelivery;
			
						if ($checkedDelivery == 3) $arDeliv = CSaleDelivery::GetByID(16);
else $arDeliv = CSaleDelivery::GetByID(13);
			?>
			<? if ($checkedDelivery == 3) { ?>
			    <? if (!empty($arResult['PROPERTIES']['DELIVERY_INFO'])) { ?>
				    <div class="form__title text-left">По какому адресу вам доставить заказ?</div>

				    <?$prop = $arResult['PROPERTIES']['DELIVERY_INFO']['CITY']; //deb($prop, false);?>
				    <div class="row row--vcentr form-row">
					<div class="col-12 col-sm-1">
					    <label class="input-label" for="checkout_city"><?=$prop['NAME']?><?if (!empty($prop['REQUIED'])) { ?><sup>*</sup><? } ?>:</label>
					</div>
					<div class="col-12 col-sm-8 col-md-8">
					    <? if (!empty($prop['LOCATION_SAM'])) { ?>
					    <input type='hidden' id='change_city' value='<?=$prop['LOCATION_SAM']?>'>
					    <? } ?>
					    
					    <input type='hidden' id='ORDER_<?=$checkedDelivery?>_PROP_<?=$prop['ID']?>' 
						   class="_refresh_after_change" name='ORDER_PROP_<?=$prop['ID']?>' value='<?=$prop['LOCATION_CODE']?>'>
						   <?$prop_city['LOCATION_NAME'] = $prop['LOCATION_NAME'];?>
					    <input id="checkout_city" 
						   class="input-text js-city-input" 
						   data-cities-src="/ajax/cities.php" 
						   data-key='#ORDER_<?=$checkedDelivery?>_PROP_<?=$prop['ID']?>'
						   type="text" required 
						   value='<?=$prop['LOCATION_NAME']?>' 
						   name='ORDER_PROP_<?=$arResult['PROPERTIES']['DELIVERY_INFO']['CITY_TEXT']['ID']?>'>
					</div>
				    </div>
				    <? if (!empty($arResult['PROPERTIES']['DELIVERY_INFO']['STREET'])) { ?>
					<?$prop = $arResult['PROPERTIES']['DELIVERY_INFO']['STREET']; ?>
					<div class="row row--vcentr form-row">
					    <div class="col-12 col-sm-1">
						<label class="input-label" for="checkout_street"><?=$prop['NAME']?><?if (!empty($prop['REQUIED'])) { ?><sup>*</sup><? } ?>:</label>
					    </div>
					    <div class="col-12 col-sm-8">
						<input id="checkout_street" class="input-text" name="ORDER_PROP_<?=$prop['ID']?>" value="<?=$prop['PRINT_VALUE']?>" type="text"<?if (!empty($prop['REQUIED'])) { ?> required<? } ?>>
					    </div>
					</div>
				    <? } ?>
				    <? if (!empty($arResult['PROPERTIES']['DELIVERY_INFO']['HOUSE'])) { ?>
					<div class="row row--vcentr form-row">
					    <?$prop = $arResult['PROPERTIES']['DELIVERY_INFO']['HOUSE']; ?>
					    <div class="col-3 col-sm-1">
						<label class="input-label" for="checkout_house"><?=$prop['NAME']?><?if (!empty($prop['REQUIED'])) { ?><sup>*</sup><? } ?>:</label>
					    </div>
					    <div class="col-9 col-sm-2 form-row">
						<input id="checkout_house" name="ORDER_PROP_<?=$prop['ID']?>" class="input-text" type="text"<?if (!empty($prop['REQUIED'])) { ?> required<? } ?>>
					    </div>
					    <?$prop = $arResult['PROPERTIES']['DELIVERY_INFO']['CORP']; ?>
					    <div class="col-3 col-sm-1">
						<label class="input-label" for="checkout_section"><?=$prop['NAME']?><?if (!empty($prop['REQUIED'])) { ?><sup>*</sup><? } ?>:</label>
					    </div>
					    <div class="col-9 col-sm-2 form-row form-row--force-xs">
						<input id="checkout_section" name="ORDER_PROP_<?=$prop['ID']?>" class="input-text" type="text"<?if (!empty($prop['REQUIED'])) { ?> required<? } ?>>
					    </div>
					    <?$prop = $arResult['PROPERTIES']['DELIVERY_INFO']['APARTMENT']; ?>
					    <div class="col-3 col-sm-1">
						<label class="input-label" for="checkout_flat"><?=$prop['NAME']?><?if (!empty($prop['REQUIED'])) { ?><sup>*</sup><? } ?>:</label>
					    </div>
					    <div class="col-9 col-sm-2 form-row form-row--force-xs">
						<input id="checkout_flat" name="ORDER_PROP_<?=$prop['ID']?>" class="input-text" type="text"<?if (!empty($prop['REQUIED'])) { ?> required<? } ?>>
					    </div>
					    <div class="col-12 col-md-3 form-row--force-sm text-small-expand">Если вы живете в частном доме, то поля «Корпус» и «Квартира» заполнять не обязательно.</div>
					</div>
				    <? } ?>
			    <? } ?>
			    <div class="content-block">
				<div class="form__title text-left">Комментарий</div>
				<div class="row">
				    <div class="col-12 col-sm-9">
					<textarea class="input-text input-text--textarea"
						  name="ORDER_DESCRIPTION"
						  value="<?=$_POST['ORDER_DESCRIPTION']?>"
						  placeholder="Например, запасной номер телефона или &quot;нет грузового лифта&quot; и т.д."></textarea>
				    </div>
				</div>
			    </div>
			<? } else { ?>
				    
				<?
				$arFilter = Array("IBLOCK_ID"=>const_IBLOCK_ID_cities, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", 
				    '!PROPERTY_LOCATION_ID' => false, 
				    '!PROPERTY_HAS_SAM' => false /*,"NAME"=>$_GET["search"]."%"*/);
				$res = CIBlockElement::GetList(Array('name' => 'asc'), $arFilter, false, false);
				$cities = array();
				while($ob = $res->GetNextElement())
				{
				    $arFields = $ob->GetFields();
				    $arFields['PROPERTIES'] = $ob->GetProperties();

				    $cities[$arFields['PROPERTIES']['LOCATION_ID']['VALUE']] = $arFields;
				}
				?>
				
				<? $currentCity = array();?>
				<?$prop = $arResult['PROPERTIES']['DELIVERY_INFO']['CITY']; 
				$prop_city = $arResult['PROPERTIES']['DELIVERY_INFO']['CITY']; 
				?>
				<input type='hidden' id='ORDER_<?=$checkedDelivery?>_PROP_<?=$prop['ID']?>' 
				       name='ORDER_PROP_<?=$arResult['PROPERTIES']['DELIVERY_INFO']['CITY_TEXT']['ID']?>' value='<?=$prop['LOCATION_NAME']?>'>
				<div class="row row--vcentr">
				    <div class="col-12 col-sm-3 col-md-2">
				      <label class="input-label" for="checkout_pickup_city">Ваш&nbsp;населенный&nbsp;пункт:</label>
				    </div>
				    <div class="col-12 col-sm-4">
				      <select id="checkout_pickup_city" class="select" name="ORDER_PROP_<?=$prop['ID']?>">
					    <? foreach ($cities as $city_id => $city) { ?>
						    <? if ($prop['LOCATION_CODE']) { ?>
							<? if ($prop['LOCATION_CODE'] == $city_id) 
							{
							    $currentCity = $city;
							} ?>
							<option value="<?=$city_id?>"<? if ($prop['LOCATION_CODE'] == $city_id) { ?> selected<? } ?>><?=$city["NAME"]?></option>
						    <? } else {//second check ?>
						    <? if ($city['NAME']==$_SESSION["PEK_CURRENT_CITY_NAME"]) 
							{
							    $currentCity = $city;
							} ?>
							<option value="<?=$city_id?>"<? if ($city['NAME']==$_SESSION["PEK_CURRENT_CITY_NAME"]) { ?> selected<? } ?>><?=$city["NAME"]?></option>
						    <? }
					    } ?>
				      </select>
				    </div>
				  </div>
				
				    <?
				    $cities_points = array();
				    $arFilter = Array("IBLOCK_ID"=>const_IBLOCK_ID_pickpoints, 'PROPERTY_CITY' => $currentCity['ID'], 'ACTIVE' => 'Y');
				    $res = CIBlockElement::GetList(Array(), $arFilter);
				    while($ob = $res->GetNextElement())
				    {
					$arFields = $ob->GetFields();
					$arFields['PROPERTIES'] = $ob->GetProperties();
					
					$cities_points[$arFields['ID']] = $arFields;
				    }
//				    deb($cities_points, false);
//				    deb($prop, false);
				    ?>
				
				    <?$prop = $arResult['PROPERTIES']['DELIVERY_INFO']['PICKPOINT']; ?>
				  <div class="content-block">
				    <div class="form__title text-left">Выбрать пункт выдачи</div>
				    <ul class="pickup js-pickup">
					
					<!-- Если не использовать тут radio - потребуется допилить скрипт (src/scripts/app/checkout.js)-->
					<?$k = 0;?>
					<? foreach (array_values($cities_points) as $kcity_point => $city_point) { 
					$prefix = '';
						if (!empty($city_point['PROPERTIES']['IS_ANDI_POINT']['VALUE'])) {
						    $prefix = '«Эндимарт»';
						}
						else
						{
						    $prefix = '«Пэк»';
						}
						
					if(!($prefix == "«Пэк»" && in_array($prop_city['LOCATION_NAME'], $ar_city))){
					?>
					
					    <li class="pickup__item js-pickup__item">
					      <label class="pickup__head">
						
						<input class="pickup__radio js-pickup__radio" 
						       value="<?=$prefix?> (<?=$city_point['NAME']?>)"
						       name="ORDER_PROP_<?=$prop['ID']?>" type="radio" required="required"<? /*if (empty($kcity_point)) { ?> checked<? }*/ ?>>
						<span class="pickup__info d-block">
						  <span class="row row--vcentr">
						    <span class="col-12 col-sm-6">
							<span class="text-small-expand d-block"><?=$prefix?> (<?=$city_point['NAME']?>)</span>
							<span class="text-small-expand d-block"><?=$city_point['PROPERTIES']['ADDRESS']['VALUE']?></span>
						    </span>
						    <span class="col-12 col-sm-4 text-small-expand text-sm-right"><?//echo $prop_city['LOCATION_NAME']?>
						      <span class="text-gray">стоимость доставки:</span> <b><?echo (!in_array($prop_city['LOCATION_NAME'], $ar_city)) ? $arResult['DELIVERY_PRICE_FORMATED'] : $arDeliv["PRICE"];?></b></span>
						  </span>
						</span>
					      </label>
					      <div class="pickup__map-wrap js-pickup__content">
						<div class="pickup__map-in">
						  <div class="row">
						    <div class="col-12 col-sm-6">
						      <div class="pickup__map js-pickup__map" data-center="[<?=$city_point['PROPERTIES']['COORDINATES']['VALUE']?>]" data-marker="{&quot;latlng&quot;:[<?=$city_point['PROPERTIES']['COORDINATES']['VALUE']?>]}" data-marker-image="<?= SITE_TEMPLATE_PATH ?>/static/images/map-marker.svg">
							<div class="ajax-preloader pickup__map-preloader js-map-preloader"><i class="ajax-preloader__spinner"></i></div>
						      </div>
						    </div>
						    <div class="col-12 col-sm-6 text-small-expand">
						      <div class="pickup__text">Телефоны: <?=$city_point['PROPERTIES']['PHONE']['~VALUE']?>
							<br>Режим работы: <?=$city_point['PROPERTIES']['WORKTIME']['~VALUE']['TEXT']?></div>
						    </div>
						  </div>
						</div>
					      </div>
					    </li>
			<? }} ?>
				    </ul>
				  </div>
			<? } ?>
			<? if (!empty($arResult['PAY_SYSTEM'])) { ?>
				<div class="content-block">
					<div class="form__title text-left">Способ оплаты</div>
					<? if (count($arResult['PAY_SYSTEM']) > 1 ) { ?>
						<div class="check-list check-list--expand">
							<? $checked = !empty($_POST['PAY_SYSTEM_ID'])?$_POST['PAY_SYSTEM_ID']:$arResult['PAY_SYSTEM'][0]['ID']; ?>
							<? foreach ($arResult['PAY_SYSTEM'] as $paysystem) { ?>
								<? if (!empty($paysystem['PSA_LOGOTIP']['SRC'])) { ?>
									<div class="check-list__item">
										<div class="row row--vcentr">
											<div class="col-12 col-sm-3 col-md-2">
												<label class="radio">
													<input class="radio__input" value="<?=$paysystem['ID']?>" name="PAY_SYSTEM_ID" type="radio"<? if ($checked == $paysystem['ID']) { ?> checked="checked"<? } ?>>
													<span class="radio__label"><?=$paysystem['NAME']?></span>
												</label>
											</div>
											<div class="col-12 col-sm-5 col-md-4">
												<img	class="flexible-media" 
													src="<?=$paysystem['PSA_LOGOTIP']['SRC']?>" 
													width="<?=$paysystem['PSA_LOGOTIP']['WIDTH']?>" 
													height="<?=$paysystem['PSA_LOGOTIP']['HEIGHT']?>" 
													alt>
											</div>
										</div>
									</div>
								<? } else { ?>
									<div class="check-list__item">
										<label class="radio">
											<input class="radio__input" name="PAY_SYSTEM_ID" value="<?=$paysystem['ID']?>" type="radio"<? if ($checked == $paysystem['ID']) { ?> checked="checked"<? } ?>>
											<span class="radio__label"><?=$paysystem['NAME']?></span>
										</label>
									</div>
								<? } ?>
							<? } ?>

						</div>
					<? } else { ?>
						<? $paysystem = $arResult['PAY_SYSTEM'][0]; ?>
						<input type="hidden" name="PAY_SYSTEM_ID" value="<?=$paysystem['ID']?>">
						<div class="row row--vcentr">
							<div class="col-12 col-sm-2 text-small-expand"><?=$paysystem['NAME']?>:</div>
							<div class="col-12 col-sm-6 col-md-5">
								<img class="flexible-media" 
								     src="<?=$paysystem['PSA_LOGOTIP']['SRC']?>" 
								     width="<?=$paysystem['PSA_LOGOTIP']['WIDTH']?>" 
								     height="<?=$paysystem['PSA_LOGOTIP']['HEIGHT']?>" 
								     alt>
							</div>
						</div>
					<? } ?>
				</div>
			<? } ?>
			<div class="content-block content-block--push-top">
			    <div class="row row--vcentr b-list">
				<div class="col-12 col-sm-4 col-md-6 b-list__item">
				    <div class="sum">
					<div class="sum__row">Итого</div>
					<div class="sum__row">Товаров на
					    <span class="text-nowrap"><?=$arResult['PRICE_WITHOUT_DISCOUNT']?></span>
					</div>
					<? if (!empty($arResult['DISCOUNT_PRICE'])) { ?>
					<div class="sum__row">Скидка
					    <span class="text-nowrap"><?=$arResult['DISCOUNT_PRICE_FORMATED']?></span>
					</div>
					<? } ?>
                    
					<div class="sum__row">Доставка: <?echo (!in_array($prop_city['LOCATION_NAME'], $ar_city)) ? $arResult['DELIVERY_PRICE_FORMATED'] : $arDeliv["PRICE"];?></div>
				    </div>
				</div>
				<div class="col-12 col-sm-4 col-md-3 b-list__item">
				    <?$str = str_replace(' ', '', $arResult['PRICE_WITHOUT_DISCOUNT']);?>
				    <div class="h4 text-nowrap">К оплате: <?echo (!in_array($prop_city['LOCATION_NAME'], $ar_city)) ? $arResult['ORDER_TOTAL_PRICE_FORMATED'] : ($str + $arDeliv["PRICE"])?></div>
				</div>
				<div class="col-12 col-sm-4 col-md-3 b-list__item">
				    <button class="btn btn--fluid btn--xl" type="submit">
					<span><? if ($arResult['ORDER_DATA']['PAY_SYSTEM_ID'] == 2) { ?>Оформить заказ<? } else { ?>Оплатить заказ<? } ?></span>
				    </button>
				</div>
			    </div>
			</div>
		    </form>
		</div>
		<? /*foreach ($arResult['DELIVERY'] as $delivery) { ?>
			<? if ($delivery['CHECKED'] != 'Y') { ?>	
				<div id="checkout-<?=$delivery['ID']?>" class="">
					<div class="ajax-preloader"><i class="ajax-preloader__spinner"></i></div>
				</div>
			<? } ?>
		<? }*/ ?>
	    </div>
	</div>
<? } //deb($arResult, false) ?>