<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!-- У этой и других форм добавлена валидация (класс js-form-validator). Это демо для верстки! Пример интеграции валидатора с формой можно посмотреть у формы подписки (src/scripts/app/forms/subscribe-form.js)-->
<h2 class="form__title text-left">Вход</h2>


<form class="js-form-validator" 
      novalidate name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" 
      action="<?=$arResult["AUTH_URL"]?>" 
      data-is_ajax="y" 
      data-backurl_success="<?=$arResult["PROFILE_URL"]?>" 
      data-add_params="&Login=">
	<? if ($USER->IsAuthorized()) { ?> 
		<script>
			document.location = '<?=$arResult["PROFILE_URL"]?>';
		</script>
	<? } ?>
	<?if($arResult["BACKURL"] <> ''):?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
	<?endif?>
	<?foreach ($arResult["POST"] as $key => $value):?>
	<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
	<?endforeach?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="AUTH" />
	  <? if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']) { ?>
	<div style="color: red">
		<? ShowMessage($arResult['ERROR_MESSAGE']);?>
	</div>
    <? } ?>
	<div class="row row--vcentr form-row">
		<div class="col-12 col-sm-2">
			<label class="input-label" for="login_email">Email:</label>
		</div>
		<div class="col-12 col-sm-10">
			<input id="login_email" class="input-text" type="text" required name="USER_LOGIN" value="<?=$arResult["USER_LOGIN"]?>">
		</div>
	</div>
	<div class="row row--vcentr form-row">
		<div class="col-12 col-sm-2">
			<label class="input-label" for="login_password">Пароль:</label>
		</div>
		<div class="col-12 col-sm-10">
			<input id="login_password" class="input-text"required type="password" name="USER_PASSWORD" autocomplete="off">
		</div>
	</div>
	<?if($arResult["SECURE_AUTH"]):?>
		<span class="bx-auth-secure" id="bx_auth_secure<?=$arResult["RND"]?>" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
					<div class="bx-auth-secure-icon"></div>
				</span>
		<noscript>
				<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
		</noscript>
		<script type="text/javascript">
			document.getElementById('bx_auth_secure<?=$arResult["RND"]?>').style.display = 'inline-block';
		</script>
	<?endif?>

	<div class="row row--vcentr form-row form-row--push-top b-list">
		<div class="col-12 col-sm-6 col-md-5 b-list__item">
			<button class="btn btn--fluid" type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>">
				<span>Войти</span>
			</button>

		</div>
		<div class="col-12 col-sm-6 col-md-5 text-center b-list__item">
			<a class="text-small js-popup" href="#popup-password-restore">Забыли пароль?</a>
		</div>
	</div>
</form>