<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

?>
<h2 class="section-title title-line js-title-line">Наши преимущества</h2>
<div class="content-block">
	<ol class="strongs row">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	$href = $arItem["DETAIL_PICTURE"]["SRC"];
	$class="";
	if($arItem["PROPERTIES"]["VIDEO"]["VALUE"]){
		$href =$arItem["PROPERTIES"]["VIDEO"]["VALUE"];
		$class = "portfolio__link--play js-popup-gallery__video";
	}
	?>
	<li class="strongs__item col-12 col-sm-6">
		<div class="strongs__item-in">
			<div class="strongs__title"><?=$arItem["NAME"]?></div>
			<div class="strongs__text"><?=$arItem["PREVIEW_TEXT"]?></div>
		</div>
	</li>
<?endforeach;?>
		</ol>
	</div>

