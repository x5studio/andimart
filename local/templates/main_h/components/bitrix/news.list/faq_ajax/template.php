<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="content-block row">
	<div class="col-12 col-md-10 offset-md-1">
		<div class="list-header">
			<a class="form-anchor js-scroll-to" data-scroll-offset="-150" href="#ask-form">Задать вопрос</a>
		</div>
		
		<ul class="faq">


<?foreach($arResult["ITEMS"] as $arItem):?>
	<?

	$arr = explode(" ",$arItem["DATE_CREATE"]);
	$arr_answer = explode(" ",$arItem["TIMESTAMP_X"]);
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

	<li class="faq__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<div class="faq__question">
			<div class="faq__question-name"><?=$arr[0]?> <?=$arItem["NAME"]?> <? if($arItem["PROPERTIES"]["CITY"]["VALUE"]!=""){ ?>(<?=$arItem["PROPERTIES"]["CITY"]["VALUE"]?>) <?}  ?></div>
			<div class="faq__text"><?=$arItem["PREVIEW_TEXT"]?></div>
		</div>
		<div class="faq__answer">
			<div class="faq__answer-name"><?=$arItem["PROPERTIES"]["ANSWERDATE"]["VALUE"]?> «Эндимарт»</div>
			<div class="faq__text"><?=$arItem["DETAIL_TEXT"]?></div>
		</div>
	</li>
<?endforeach;?>
		</ul>
		
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
	</div>
</div>
