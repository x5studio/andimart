<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//deb($arResult['PICKPOINTS'], false);
/*
$cnt_c = 0;
foreach($arResult['PICKPOINTS'] as $val){
	
	foreach($val as $pickpoint){
		
		$prefix = '';
		if (!empty($pickpoint['PROPERTIES']['IS_ANDI_POINT']['VALUE'])) {
		    $prefix = '«Эндимарт»';
		}
		else
		{
		    $prefix = '«Пэк»';
		}

		$title = $prefix.' ('.$pickpoint['NAME'].')';
		$text = 'Адрес: '.$pickpoint['PROPERTIES']['ADDRESS']['VALUE'].''
				. (!empty($pickpoint['PROPERTIES']['PHONE']['VALUE'])?'&lt;br&gt; Телефон: '.$pickpoint['PROPERTIES']['PHONE']['VALUE'].'':'')
				. (!empty($pickpoint['PROPERTIES']['WORKTIME']['VALUE']['TEXT'])?'&lt;br&gt; Режим работы: '.$pickpoint['PROPERTIES']['WORKTIME']['VALUE']['TEXT']:'')
				;
		if ($cnt_c > 134)
		{
//			$text = str_replace('"', '', $text);
//			$text = str_replace('"', '', $text);
//			$text = str_replace('/', '', $text);
//			
			deb($cnt_c.': '.$title, false);
			deb($cnt_c.': '.$text, false);
			break;
//			deb($text);
		}
//			
		$cnt_c++;
//		
////		$prefix = '';
////		if (!empty($pickpoint['PROPERTIES']['IS_ANDI_POINT']['VALUE'])) {
////		    $prefix = '«Эндимарт»';
////		}
////		else
////		{
////		    $prefix = '«Пэк»';
////		}
////		
////		$title = $prefix.' ('.$pickpoint['NAME'].')';
////		$text = 'Адрес: '.$pickpoint['PROPERTIES']['ADDRESS']['VALUE'].'&lt;br&gt; Телефон: '.$pickpoint['PROPERTIES']['PHONE']['VALUE'].'&lt;br&gt; Режим работы: '.$pickpoint['PROPERTIES']['WORKTIME']['VALUE'];
////						
//////		deb($title, false);
//////		deb($text, false);
////		deb($pickpoint['PROPERTIES']['WORKTIME']);
//////		deb('end');
////		break;
	}
	if ($cnt_c > 134)
	{
		break;
	}
}
*/
//deb(count($arResult['PICKPOINTS']), false);
?>
<div class="row b-list">
<div class="col-12 col-sm-6 b-list__item">
	<div class="row row--vcentr">
		<div class="col-12 col-sm-4">
			<label class="input-label h3" for="contacts-map-city">Выберите город:</label>
		</div>
		<div class="col-12 col-sm-8">
			<!-- В этом селекте выводим список городов и координаты центра карты для каждого города-->
			<select id="contacts-map-city" class="select">


<?
$cur_key  = 0;
$data="[";
$data_=array();
$cnt_c = 0;
foreach($arResult["ITEMS"] as $key=>$arItem): ?>
	<?
	$center = '';
	if(!empty($arResult['PICKPOINTS'][$arItem['ID']])){
		
		$first = false;
		foreach($arResult['PICKPOINTS'][$arItem['ID']] as $pickpoint){
			
			$prefix = '';
			if (!empty($pickpoint['PROPERTIES']['IS_ANDI_POINT']['VALUE'])) {
			    $prefix = '«Эндимарт»';
			}
			else
			{
			    $prefix = '«Пэк»';
			}

			$title = $prefix.' ('.$pickpoint['NAME'].')';
			$text = 'Адрес: '.trim($pickpoint['PROPERTIES']['ADDRESS']['VALUE']).''
				. (!empty($pickpoint['PROPERTIES']['PHONE']['VALUE'])?'&lt;br&gt; Телефон: '.trim($pickpoint['PROPERTIES']['PHONE']['VALUE']).'':'')
				. (!empty($pickpoint['PROPERTIES']['WORKTIME']['VALUE']['TEXT'])?'&lt;br&gt; Режим работы: '.trim($pickpoint['PROPERTIES']['WORKTIME']['VALUE']['TEXT']):'')
				;
			$text_ = 'Адрес: '.trim($pickpoint['PROPERTIES']['ADDRESS']['VALUE']).''
				. (!empty($pickpoint['PROPERTIES']['PHONE']['VALUE'])?'<br> Телефон: '.trim($pickpoint['PROPERTIES']['PHONE']['VALUE']).'':'')
				. (!empty($pickpoint['PROPERTIES']['WORKTIME']['VALUE']['TEXT'])?'<br> Режим работы: '.trim($pickpoint['PROPERTIES']['WORKTIME']['~VALUE']['TEXT']):'')
				;
			
//			$title = str_replace('&quot;', '&amp;quot;', $title);
			$text = str_replace('&quot;', '&amp;quot;', $text);
			
			if (empty($center))
			{
				$center = $pickpoint['PROPERTIES']['COORDINATES']['VALUE'];
			}
			
			
			
			if($cnt_c>0){
				$data.=",";
			
			}
			$data.="{&quot;latlng&quot;:[".$pickpoint['PROPERTIES']['COORDINATES']['VALUE']."],"
				. "&quot;title&quot;:&quot;".$title."&quot;,"
				.(!empty($pickpoint['PROPERTIES']['IS_ANDI_POINT']['VALUE'])?
				 "&quot;img&quot;:&quot;".SITE_TEMPLATE_PATH.'/static/images/map-marker_andi.png'."&quot;,"
				:
				 "&quot;img&quot;:&quot;".SITE_TEMPLATE_PATH.'/static/images/map-marker.svg'."&quot;,")
				
				. "&quot;text&quot;:&quot;". $text. "&quot;}";
			$cnt_c++;
			
			$pickpoint['PROPERTIES']['COORDINATES']['VALUE'] = explode(',', $pickpoint['PROPERTIES']['COORDINATES']['VALUE']);
			foreach ($pickpoint['PROPERTIES']['COORDINATES']['VALUE'] as $k => $v)
			{
				$pickpoint['PROPERTIES']['COORDINATES']['VALUE'][$k] = trim($v);
			}
			
			$data_[] = array(
			    'latlng' => $pickpoint['PROPERTIES']['COORDINATES']['VALUE'],
			    //Pickpoint checking here!!!
			    'img' => (!empty($pickpoint['PROPERTIES']['IS_ANDI_POINT']['VALUE'])?SITE_TEMPLATE_PATH.'/static/images/map-marker_andi.png':
				 SITE_TEMPLATE_PATH.'/static/images/map-marker.svg'),
			    'title' => htmlspecialchars($pickpoint['NAME']),
			    'text' => htmlspecialchars($text_),
			);
		}
//		if ($cnt_c > 134) break;
	}
	else
	{
		continue;
	}
	?>
	<option data-map-center="[<?=$center?>]" <?if($arParams["PVZ_CITY_ID"] == $arItem["ID"]){ echo "selected"; $cur_key = $key; }?> value><?=$arItem["NAME"]?></option>
<?endforeach;
$data.="]";

//{
//&quot;latlng&quot;:[44.059654,43.034953],
//&quot;title&quot;:&quot;«Эндимарт» (Пятигорск / Бештау)&quot;,
//&quot;text&quot;:&quot;Адрес: г. Пятигорск, Бештаугорское шоссе 9/6&lt;br&gt; Телефоны: +7-928-814-65-29, +7-928-814-65-96&lt;br&gt; Режим работы: ежедневно, с 8:00 до 17:00.&lt;br&gt;  Воскресенье — выходной&quot;},
?>
			</select>
		</div>
	</div>
</div>
<div class="col-12 col-sm-6 b-list__item text-small-expand text-small-expand_center">

	<?$APPLICATION->IncludeComponent(
		"bitrix:main.include",
		"",
		Array(
			"AREA_FILE_SHOW" => "file",
			"AREA_FILE_SUFFIX" => "inc",
			"EDIT_TEMPLATE" => "",
			"PATH" => SITE_TEMPLATE_PATH."/include/contacts/map_text.php"
		),
		false,
		array('HIDE_ICONS' => 'N')
	);?>
	</div>
</div>
<div class="content-block">
	<!-- Все параметры карты - в дата-атрибуте: центр карты, уровень зума (zoom)-->
	<?
	/*echo $_SESSION["PEK_CURRENT_CITY_ID"];
	echo $cur_key."!".$arResult["ITEMS"][$cur_key]["PROPERTIES"]["LOCATION"]["VALUE"][0];
	p($arResult["ITEMS"][$cur_key]);*/
	?>
	<div id="contacts-map" class="map" data-center="[<?=$arResult["ITEMS"][$cur_key]["PROPERTIES"]["LOCATION"]["VALUE"][0]?>]"
	     data-markers='<?= json_encode($data_)?>'
	     <?/*data-markers="<?= $data?>"*/?>
		 data-marker-image="<?=SITE_TEMPLATE_PATH?>/static/images/map-marker.svg">

		<div id="contacts-map-preloader" class="ajax-preloader map__preloader"><i class="ajax-preloader__spinner"></i></div>
	</div>
</div>