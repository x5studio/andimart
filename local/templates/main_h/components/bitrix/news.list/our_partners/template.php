<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<h2 class="section-title title-line js-title-line title-line--accent">Наши партнеры</h2>
<div class="content-block content-block--pull-top content-block--push-bot">
	<div class="portfolio">
		<ul class="portfolio__gallery js-portfolio-carousel js-popup-gallery" data-slides="4">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	
	$img = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]['ID'], Array("width" => 250, "height" => 178), BX_RESIZE_IMAGE_PROPORTIONAL, false, false, false, 80);
	?>

	<li class="portfolio__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <span class="portfolio__link">
                      <img src="<?=$img["src"]?>" width="250" height="178" alt>
                    </span>
	</li>
<?endforeach;?>

		</ul>
	</div>
</div>