<?php
	$arResult['SECTIONS'] = array();
	
	$arFilter = Array("IBLOCK_ID"=>const_IBLOCK_ID_catalog, 'ACTIVE' => 'Y', 'DEPTH_LEVEL' => 1);
	$res = CIBlockSection::GetList(Array(), $arFilter);
	while($arFields = $res->Fetch())
	{
		$arFields['ITEMS'] = array();
		$arResult['SECTIONS'][$arFields['ID']] = $arFields;
	}
	
	if (!empty($arResult['ITEMS']))
	{
		foreach ($arResult['ITEMS'] as $arItem)
		{
				$arResult['SECTIONS'][$arItem['PROPERTIES']['SECTION']['VALUE']]['ITEMS'][$arItem['ID']] = $arItem;
			}
		}
		
?>