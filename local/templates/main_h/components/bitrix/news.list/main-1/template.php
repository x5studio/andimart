<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="hello__slider js-hello-slider">
            <ul class="hello__slides js-hello-slider__items">
                
<?foreach($arResult["ITEMS"] as $arItem){ ?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	 <li class="hello__slide" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
	 <?
	 if($arItem["PROPERTIES"]["LINK"]["VALUE"]){
	 ?>
                    <a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>">
					<?
	 }$img = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]['ID'], Array("width" => 710,"height" => 470), BX_RESIZE_IMAGE_PROPORTIONAL, false, false, false, 80);
					?>
                        <img width="768" src="<?=$img["src"]?>" height="470" alt="">
						 <?
	 if($arItem["PROPERTIES"]["LINK"]["VALUE"]){
	 ?>
                    </a>
						<?
	 }
					?>
                </li>
	
<? } ?>
  </ul>
            <div class="hello__nav">
			<?
			$cnt=0;
			foreach($arResult["ITEMS"] as $arItem){?>
                <button class="hello__nav-item js-hello-slider__nav" data-slide="<?=$cnt?>" type="button">
                    <span><?=$arItem["NAME"]?></span>
                </button>
               
				<?
				$cnt++;
	 }
					?>
            </div>
			 </div>