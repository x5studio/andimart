<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>

<div class="social">
	<div class="social__title">Мы в социальных сетях
	    <span class="social__title-dash">—</span>
	</div>
	<div class="social__list">
	    <!-- У youtube и tw модификаторы на размер иконки-->
		<? foreach ($arResult as $arItem) { ?>
			<a class="social__item" rel="nofollow" href="<?=$arItem["LINK"]?>">
				<svg class="social__item-i social__item-i--big">
				    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/static/images/icons/social_1.svg#<?=$arItem["TEXT"]?>"></use>
				</svg>
			</a>
		<? } ?>
	</div>
</div>
<?/*

<a class="social__item" href="https://www.youtube.com/channel/UCDEuDAlgbLxnaDIFW9E2v5Q/videos?sort=dd&shelf_id=0">
    <svg class="social__item-i social__item-i--big">
        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/static/images/icons/social.svg#youtube"></use>
    </svg>
</a>
<a class="social__item" href="https://www.youtube.com/channel/UCDEuDAlgbLxnaDIFW9E2v5Q/videos?sort=dd&shelf_id=0">
    <svg class="social__item-i">
        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/static/images/icons/social.svg#insta"></use>
    </svg>
</a>
<!--<a class="social__item" href="#">
    <svg class="social__item-i">
        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/static/images/icons/social.svg#fb"></use>
    </svg>
</a>-->
<!--<a class="social__item" href="#">
    <svg class="social__item-i">
        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/static/images/icons/social.svg#ok"></use>
    </svg>
</a>-->
<!--<a class="social__item" href="#">
    <svg class="social__item-i social__item-i--big">
        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/static/images/icons/social.svg#tw"></use>
    </svg>
</a>-->
<!--<a class="social__item" href="#">
    <svg class="social__item-i">
        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/static/images/icons/social.svg#vk"></use>
    </svg>
</a>-->
<? /*if (!empty($arResult)): ?>
   <ul class="pages-nav__list">
        <?
        $previousLevel = 0;
        foreach ($arResult as $arItem):
			if($arItem["SELECTED"]=="Y"){
			?>
			 <li class="pages-nav__item pages-nav__item--current">
                      <span class="pages-nav__title"><?=$arItem["TEXT"]?></span>
                    </li>
			<?
			}else{
				?>
				 <li class="pages-nav__item">
                      <a class="pages-nav__url" href="<?=$arItem["LINK"]?>">
                        <span class="pages-nav__title"><?=$arItem["TEXT"]?></span>
                      </a>
                    </li>
				<?
			}
            ?>

               



        <? endforeach ?>


   </ul>
<? endif ?>