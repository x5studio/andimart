<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>

<? if (!empty($arResult)): ?>
   <ul class="pages-nav__list">
        <?
        $previousLevel = 0;
        foreach ($arResult as $arItem):
			if($arItem["SELECTED"]=="Y"){
			?>
			 <li class="pages-nav__item pages-nav__item--current">
                      <span class="pages-nav__title"><?=$arItem["TEXT"]?></span>
                    </li>
			<?
			}else{
				?>
				 <li class="pages-nav__item">
                      <a class="pages-nav__url" href="<?=$arItem["LINK"]?>">
                        <span class="pages-nav__title"><?=$arItem["TEXT"]?></span>
                      </a>
                    </li>
				<?
			}
            ?>

               



        <? endforeach ?>


   </ul>
<? endif ?>