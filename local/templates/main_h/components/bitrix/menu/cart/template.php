<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>

<? if (!empty($arResult)): ?>
	<ol class="checkout-steps__list js-checkout-steps-scroll">
		<? $found = false; ?>
		<? foreach ($arResult as $karItem => $arItem) { ?>
			<? if ($arItem["LINK"] == $APPLICATION->GetCurPage() && empty($_GET['ORDER_ID'])) { ?>
				<? $found = true; ?>
				<li class="checkout-steps__item checkout-steps__item--current">
					<div class="checkout-steps__title"><?=$arItem["TEXT"]?></div>
				</li>
			<? } else { ?>
				<? if (!empty($_GET['ORDER_ID']) && (count($arResult)-1)==$karItem) { ?>
					<li class="checkout-steps__item checkout-steps__item--current">
						<div class="checkout-steps__title"><?=$arItem["TEXT"]?></div>
					</li>
				<? } elseif (!empty($_GET['ORDER_ID'])) { ?>
					<li class="checkout-steps__item">
						<div class="checkout-steps__title"><?=$arItem["TEXT"]?></div>
					</li>
				<? } elseif ($found) { ?>
					<li class="checkout-steps__item">
						<div class="checkout-steps__title"><?=$arItem["TEXT"]?></div>
					</li>
				<? } else { ?>
					<li class="checkout-steps__item">
						<a class="checkout-steps__link" href="<?=$arItem["LINK"]?>">
							<span class="checkout-steps__title"><?=$arItem["TEXT"]?></span>
						</a>
					</li>
				<? } ?>
			<? } ?>
		<? } ?>
	</ol>
<? endif ?>