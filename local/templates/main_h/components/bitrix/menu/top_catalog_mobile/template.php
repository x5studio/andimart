<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>

<? if (!empty($arResult)): ?>
    <ul class="mobile-nav__list">
        <?
        $previousLevel = 0;
        foreach ($arResult as $arItem):
            if($arParams["SHOW_ONLY_DEAPTH_LEVEL"] && $arParams["SHOW_ONLY_DEAPTH_LEVEL"]<$arItem["DEPTH_LEVEL"])
            ?>
            <li class="mobile-nav__item">
                <a class="mobile-nav__link" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
            </li>


        <? endforeach ?>


    </ul>

<? endif ?>