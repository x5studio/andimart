<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$date = $arResult['EXTERNAL_ID'];
$APPLICATION->AddChainItem(substr($arResult['FIELDS']['DATE_CREATE'],0,10), $arResult['LIST_PAGE_URL'].$date."/");
?>
<div class="row b-list">
	<div class="col-12 col-sm-7 wysiwyg b-list__item " id="news_detail_append">
		<?
		if($arResult["PROPERTIES"]["TEXT1"]["~VALUE"]["TEXT"]){
			echo $arResult["PROPERTIES"]["TEXT1"]["~VALUE"]["TEXT"];
		}
		?>
		<?
		if(is_array($arResult["PROPERTIES"]["FILES1"]["VALUE"]) && !empty($arResult["PROPERTIES"]["FILES1"]["VALUE"])){
		?>
			<div class="image-slider js-carousel js-carousel--slider">
				<div class="image-slider__items js-carousel__items">

					<?
					foreach($arResult["PROPERTIES"]["FILES1"]["VALUE"] as $photo){
						?>
						<img src="<?=CFile::GetPath($photo)?>"  >
					<?
					}
					?>
				</div>
				<?
				if(count($arResult["PROPERTIES"]["FILES1"]["VALUE"])>1){
				?>
				<div class="carousel-nav">
					<button class="carousel-nav__prev js-carousel__prev" type="button"></button>
					<div class="carousel-nav__stat js-carousel__stat"></div>
					<button class="carousel-nav__next js-carousel__next" type="button"></button>
				</div>
				<?
				}
				?>
			</div>
		<?
		}
		?>
		<?
		if($arResult["PROPERTIES"]["TEXT2"]["~VALUE"]["TEXT"]){
			echo $arResult["PROPERTIES"]["TEXT2"]["~VALUE"]["TEXT"];
		}
		?>
		<?
		if(is_array($arResult["PROPERTIES"]["FILES2"]["VALUE"]) && !empty($arResult["PROPERTIES"]["FILES2"]["VALUE"])){
			?>
			<div class="image-slider js-carousel js-carousel--slider">
				<div class="image-slider__items js-carousel__items">

					<?
					foreach($arResult["PROPERTIES"]["FILES2"]["VALUE"] as $photo){
						?>
						<img src="<?=CFile::GetPath($photo)?>"  >
						<?
					}
					?>
				</div>
				<?
				if(count($arResult["PROPERTIES"]["FILES2"]["VALUE"])>1){
				?>
				<div class="carousel-nav">
					<button class="carousel-nav__prev js-carousel__prev" type="button"></button>
					<div class="carousel-nav__stat js-carousel__stat"></div>
					<button class="carousel-nav__next js-carousel__next" type="button"></button>
				</div>
				<?
				}
				?>
			</div>
			<?
		}
		?>

		<?
		if($arResult["PROPERTIES"]["TEXT3"]["~VALUE"]["TEXT"]){
			echo $arResult["PROPERTIES"]["TEXT3"]["~VALUE"]["TEXT"];
		}
		?>
	</div>
	<div class="col-12 col-sm-4 offset-sm-1 b-list__item">
		<h4 class="section-title title-line js-title-line section-title--sm title-line--accent">Еще акции и новости</h4>
		<ul class="news grid grid--xs-1">
			<?
			$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","PREVIEW_PICTURE","DETAIL_PAGE_URL","PROPERTY_ACTIONTEXT");
			$arFilter = Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y","!ID"=>$arResult["ID"]);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>4), $arSelect);
			while($ob = $res->GetNextElement())
			{
				$arFields = $ob->GetFields();
				$src= CFile::GetPath($arFields["PREVIEW_PICTURE"]);
				?>
				<li class="news__item grid__item">
					<a class="news__item-in" href="<?=$arFields["DETAIL_PAGE_URL"]?>">
						<div class="news__image">
							<img src="<?=$src?>" width="220" height="220" alt>
						</div>
						<? if($arFields ["PROPERTY_ACTIONTEXT_VALUE"]) { ?>
							<div class="news__label"><?=$arFields ["PROPERTY_ACTIONTEXT_VALUE"]?></div>
							<?
						}
						?>
						<div class="news__title"><?=$arFields ["NAME"]?></div>
					</a>
				</li>
			<?
			}
			?>


		</ul>
		<div class="content-block">
			<a class="btn btn--sm btn--inverse" href="/dlya_pokupatelya/akcii_statyi/">Все акции и статьи</a>
		</div>
	</div>
</div>
<script>
		$(document).ready(function(){
			var h1 = $("h1");
			$("#news_detail_append").prepend(h1);


		});
</script>