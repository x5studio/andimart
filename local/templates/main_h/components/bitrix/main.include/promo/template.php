<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
global $CUR_SLIDER_SECTION;
if($CUR_SLIDER_SECTION){
    ?>
    <div class="aside-promo">
        <div id="js-aside-carousel" class="aside-promo__content">
            <?
            $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*","PREVIEW_PICTURE","PREVIEW_TEXT");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
            $arFilter = Array("IBLOCK_ID"=>ASIDE_SLIDER_IBLOCK_ID, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y","SECTION_ID"=>$CUR_SLIDER_SECTION);
            $res = CIBlockElement::GetList(Array('sort' => 'asc'), $arFilter, false, false, $arSelect);
            while($ob = $res->GetNextElement()){
                $arFields = $ob->GetFields();

                $arProps = $ob->GetProperties();

        ?>
                <div class="aside-promo__slide" style="background-color:<?=$arProps["BGCOLOR"]["~VALUE"]?>;">
                    <a class="aside-promo__link" href="<?=$arProps['LINKS']['VALUE']?>">
                        <div class="aside-promo__title"><?=$arFields["NAME"]?></div>
                        <div class="aside-promo__text"><?=$arFields["PREVIEW_TEXT"]?></div>
                        <div class="aside-promo__img">
                            <img src="<?=CFile::GetPath($arFields["PREVIEW_PICTURE"])?>"  alt>
                        </div>
                    </a>
                </div>
                <?
            }
            ?>


        </div>
        <div id="js-aside-carousel-dots" class="aside-promo__dots"></div>
    </div>
<?

}else{
if($arResult["FILE"] <> '')
	include($arResult["FILE"]);
}
