<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();


		$cities = array();
		$cities_t = CIBlockElement::GetList(array('name' => 'asc'), array('IBLOCK_ID' => const_IBLOCK_ID_cities, 'ACTIVE' => 'Y'));
		while ($city = $cities_t->Fetch())
		{
			$cities[] = $city;
		}
?>

<?
//if ($arResult['DATA_SAVED'] == 'Y')
//	ShowNote(GetMessage('PROFILE_DATA_SAVED'));
?>
<script type="text/javascript">
<!--
var opened_sections = [<?
$arResult["opened"] = $_COOKIE[$arResult["COOKIE_PREFIX"]."_user_profile_open"];
$arResult["opened"] = preg_replace("/[^a-z0-9_,]/i", "", $arResult["opened"]);
if (strlen($arResult["opened"]) > 0)
{
	echo "'".implode("', '", explode(",", $arResult["opened"]))."'";
}
else
{
	$arResult["opened"] = "reg";
	echo "'reg'";
}
?>];
//-->

var cookie_prefix = '<?=$arResult["COOKIE_PREFIX"]?>';
</script>


<div class="content-block content-block--sm-top content-block--sm-bot island">
<div class="island-head-tabs">
              <div class="island-head-tabs__item">
		  <a class="island-head-tabs__link" href="/lichniy_cabinet/order/">
                  <div class="island-head-tabs__title">История заказов</div>
                </a>
              </div>
              <div class="island-head-tabs__item island-head-tabs__item--current">
                <div class="island-head-tabs__title">Профиль</div>
              </div>
            </div>
            <div class="row">
              <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3">
                <?/*form class="form js-form-validator" novalidate method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">
                  <!-- У этой и других форм добавлена валидация (класс js-form-validator). Это демо для верстки! Пример интеграции валидатора с формой можно посмотреть у формы подписки (src/scripts/app/forms/subscribe-form.js)-->
		  
<?=$arResult["BX_SESSION_CHECK"]?>
<input type="hidden" name="LOGIN" maxlength="50" value="<? echo $arResult["arUser"]["LOGIN"]?>" />
<input type="hidden" name="lang" value="<?=LANG?>" />
<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-2">
                      <label class="input-label" for="profile_name">Имя:</label>
                    </div>
                    <div class="col-12 col-sm-10">
                      <input id="profile_name" class="input-text" type="text" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" required>
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-2">
                      <label class="input-label" for="profile_tel">Телефон:</label>
                    </div>
                    <div class="col-12 col-sm-10">
                      <input id="profile_tel" class="input-text js-mask-phone" type="tel" name="PERSONAL_PHONE" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" required>
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-2">
                      <label class="input-label" for="profile_email">Email:</label>
                    </div>
                    <div class="col-12 col-sm-10">
                      <input id="profile_email" class="input-text" type="email" name="EMAIL" maxlength="50" value="<? echo $arResult["arUser"]["EMAIL"]?>" required>
                    </div>
                  </div>
                  <div class="form-row form-row--push-top text-small text-bold">Сменить пароль</div>
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-2">
                      <label class="input-label" for="profile_password">Пароль:</label>
                    </div>
                    <div class="col-12 col-sm-10">
			<input id="profile_password" class="input-text" type="password" name="NEW_PASSWORD" required autocomplete="off">
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-2">
                      <label class="input-label" for="profile_password_repeat">Пароль ещё&nbsp;раз:</label>
                    </div>
                    <div class="col-12 col-sm-10">
                      <input id="profile_password_repeat" class="input-text" type="password" name="NEW_PASSWORD_CONFIRM" required autocomplete="off">
                    </div>
                  </div>
                  <div class="form-row form-row--push-top">
                    <button class="btn btn--fluid" type="submit" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">
                      <span>Сохранить</span>
                    </button>
                  </div>
                </form*/?>
		<form class="js-form-validator form" novalidate method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">
<?=$arResult["BX_SESSION_CHECK"]?>
<input type="hidden" name="LOGIN" maxlength="50" value="<? echo $arResult["arUser"]["LOGIN"]?>" />
<input type="hidden" name="lang" value="<?=LANG?>" />
<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
		
		<?
		
		
		$img = CFile::ResizeImageGet($arResult["arUser"]["PERSONAL_PHOTO"], Array("width" => 160, "height" => 160), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
		?>
                  <!-- У этой и других форм добавлена валидация (класс js-form-validator). Это демо для верстки! Пример интеграции валидатора с формой можно посмотреть у формы подписки (src/scripts/app/forms/subscribe-form.js)-->
                  <div class="form-row row">
                    <div class="col-12 col-sm-10 offset-sm-2">
                      <div class="profile-photo">
			  <img src="<?=$img['src']?>" width="160" height="160" alt>
                      </div>
                    </div>
                  </div>
<?ShowError($arResult["strProfileError"]);?>
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-2">
                      <div class="input-label">Фото:</div>
                    </div>
                    <div class="col-12 col-sm-10">
                      <div class="input-file input-group js-input-file">
                        <div class="input-group__item input-group__item--grow">
                          <div class="input-text js-input-file__name"></div>
                        </div>
                        <label class="input-group__item">
				<input class="input-file__input" type="file" name="PERSONAL_PHOTO"
				       onchange="$(this).closest('.js-input-file').find('.js-input-file__name').text(this.files[0] ? this.files[0].name : '');">
				<span class="btn btn--fluid btn--inverse input-file__btn">
				<span>Сменить</span>
				</span>
                        </label>
			<input id="PERSONAL_PHOTO_del" name="PERSONAL_PHOTO_del" value="Y" type="hidden" disabled="">
                      </div>
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-2">
                      <label class="input-label" for="profile_name">Имя
                        <br>Фамилия:</label>
                    </div>
                    <div class="col-12 col-sm-10">
                      <input id="profile_name" class="input-text" type="text" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" required>
                    </div>
                  </div>
		  
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-2">
                      <label class="input-label" for="profile_city">Город:</label>
                    </div>
                    <div class="col-12 col-sm-10">
			<input id="profile_city" name="UF_CITY_NEW" class="input-text js-city-select__input js-city-input" data-cities-src="/ajax/cities.php" type="text" maxlength="50" value="<?=$arResult["arUser"]["UF_CITY_NEW"]?>" required>
                      
			<?/*foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):
				if($FIELD_NAME!="UF_CITY")
					continue;

				$arUserField["SETTINGS"]["DEFAULT_VALUE"] = $_SESSION["PEK_CURRENT_CITY_ID"];
				?>

							<?$APPLICATION->IncludeComponent(
								"bitrix:system.field.edit",
								$arUserField["USER_TYPE"]["USER_TYPE_ID"],
								array(//"bVarsFromForm" => $arResult["bVarsFromForm"], 
								    "arUserField" => $arUserField, 
								    "~arUserField" => $arUserField, 
//									    'VALUE' => array($_SESSION["PEK_CURRENT_CITY_ID"]),
								    "form_name" => "regform"), null, array("HIDE_ICONS"=>"Y"));?>
			<?endforeach;*/?>
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-2">
                      <label class="input-label" for="profile_tel">Телефон:</label>
                    </div>
                    <div class="col-12 col-sm-10">
                      <input id="profile_tel" class="input-text js-mask-phone" type="tel" name="PERSONAL_PHONE" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" required>
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-2">
                      <label class="input-label" for="profile_email">Email:</label>
                    </div>
                    <div class="col-12 col-sm-10">
                      <input id="profile_email" class="input-text" type="email" name="EMAIL" maxlength="50" value="<? echo $arResult["arUser"]["EMAIL"]?>" required>
                    </div>
                  </div>
                  <div class="form-row row">
                    <div class="col-12 col-sm-3">
                      <div class="input-label input-label--top">Вид деятельности:</div>
                    </div>
                    <div class="col-12 col-sm-9">
			<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):
				if($FIELD_NAME!="UF_SPECIALS")
					continue;
					//deb($arUserField["USER_TYPE"]["USER_TYPE_ID"], false);
				?>

				<?$APPLICATION->IncludeComponent(
				"bitrix:system.field.edit",
				$arUserField["USER_TYPE"]["USER_TYPE_ID"],
				array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "regform"), null, array("HIDE_ICONS"=>"Y"));?>
			<?endforeach;?>
                    </div>
                  </div>
                  <div class="form-row form-row--push-top text-small text-bold">Сменить пароль</div>
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-2">
                      <label class="input-label" for="profile_password">Пароль:</label>
                    </div>
                    <div class="col-12 col-sm-10">
			<input id="profile_password" class="input-text" type="password" name="NEW_PASSWORD" autocomplete="off">
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-2">
                      <label class="input-label" for="profile_password_repeat">Пароль ещё&nbsp;раз:</label>
                    </div>
                    <div class="col-12 col-sm-10">
                      <input id="profile_password_repeat" class="input-text" type="password" name="NEW_PASSWORD_CONFIRM" autocomplete="off">
                    </div>
                  </div>
                  <div class="form-row form-row--push-top">
                    <button class="btn btn--fluid" type="submit" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">
                      <span>Сохранить</span>
                    </button>
                  </div>
                </form>
              </div>
            </div>
            </div>





<div class="content-block content-block--sm-top content-block--sm-bot island">
            <h2 class="section-title title-line js-title-line">Дополнительная информация</h2>
            <div class="row">
              <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3">
<form class="js-form-validator form" novalidate method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">
<?=$arResult["BX_SESSION_CHECK"]?>
<input type="hidden" name="LOGIN" maxlength="50" value="<? echo $arResult["arUser"]["LOGIN"]?>" />
<input type="hidden" name="EMAIL" maxlength="50" value="<? echo $arResult["arUser"]["EMAIL"]?>" />
<input type="hidden" name="lang" value="<?=LANG?>" />
<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
		
		<!-- У этой и других форм добавлена валидация (класс js-form-validator). Это демо для верстки! Пример интеграции валидатора с формой можно посмотреть у формы подписки (src/scripts/app/forms/subscribe-form.js)-->
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-2">
                      <label class="input-label" for="profile_additional_name">Адрес:</label>
                    </div>
                    <div class="col-12 col-sm-10">
                      <input id="profile_additional_name" class="input-text" name="WORK_STREET" value="<? echo $arResult["arUser"]["WORK_STREET"]?>" type="text">
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-2">
                      <label class="input-label" for="profile_additional_time">Время работы:</label>
                    </div>
                    <div class="col-12 col-sm-10">
                      <input id="profile_additional_time" class="input-text" name="WORK_NOTES" value="<? echo $arResult["arUser"]["WORK_NOTES"]?>" type="text">
                    </div>
                  </div>
                  <?/*div class="form-row form-row--push-top text-small text-bold">Доступность выезда в другие города:</div>
                  <!-- В onclick кнопок удаления просто пример!-->
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-10">
                      <select class="select">
                        <option value selected>Москва</option>
                        <option value="">Новосибирск</option>
                        <option value="">Санкт-Петербург</option>
                        <option value="">Екатеринбург</option>
                      </select>
                    </div>
                    <div class="col-12 col-sm-2">
                      <button class="btn-remove" type="button" onclick="$(this).closest('.form-row').remove()"></button>
                    </div>
                  </div>
                  <div class="form-row">
                    <!-- В onclick просто пример!-->
                    <button class="link text-small" type="button" onclick="$(this).parent().before('&lt;div class=&quot;form-row row row--vcentr&quot;&gt;&lt;div class=&quot;col-xs-12 col-sm-10&quot;&gt;&lt;select class=&quot;select&quot;&gt;&lt;option&gt;Москва&lt;/option&gt;&lt;option selected disabled hidden&gt;Выбрать&lt;/option&gt;&lt;option&gt;Санкт-Петербург&lt;/option&gt;&lt;option&gt;Екатеринбург&lt;/option&gt;&lt;/select&gt;&lt;/div&gt;&lt;div class=&quot;col-xs-12 col-sm-2&quot;&gt;&lt;button class=&quot;btn-remove&quot; type=&quot;button&quot; onclick=&quot;$(this).parent().parent().remove()&quot;&gt;&lt;/button&gt;&lt;/div&gt;&lt;/div&gt;')">
                      <span>Добавить город</span>
                    </button>
                  </div*/?>
		
		
		<?/*div class="form-row row">
                    <div class="col-12 col-sm-2">
                      
                    </div>
                    <div class="col-12 col-sm-10">
			<div class="scroll-block js-scroll-block">
				<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):
					if($FIELD_NAME!="UF_ADD_CITIES")
						continue;

					?>

					<?$APPLICATION->IncludeComponent(
					"bitrix:system.field.edit",
					$arUserField["USER_TYPE"]["USER_TYPE_ID"],
					array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "regform"), null, array("HIDE_ICONS"=>"Y"));?>
				<?endforeach;?>

			</div>
                    </div>
                  </div*/?>
		<? //deb($arResult, false); ?>
		<? 
		/*
		$cities = array();
		$cities_t = CIBlockElement::GetList(array('name' => 'asc'), array('IBLOCK_ID' => const_IBLOCK_ID_cities, 'ACTIVE' => 'Y'));
		while ($city = $cities_t->Fetch())
		{
			$cities[] = $city;
		}
		
		
//		deb($arResult["USER_PROPERTIES"]["DATA"]['UF_ADD_CITIES']['VALUE'], false);
		 */
		?>
		
		
		<div class="form-row form-row--push-top text-small text-bold">Доступность выезда в другие города:</div>
		
		<?/* if (!empty($arResult["USER_PROPERTIES"]["DATA"]['UF_ADD_CITIES']['VALUE'])) { ?>
			<? foreach ($arResult["USER_PROPERTIES"]["DATA"]['UF_ADD_CITIES']['VALUE'] as $c) { ?>
				<div class="form-row row row--vcentr">
				    <div class="col-12 col-sm-10">
				      <select class="select" name="UF_ADD_CITIES[]">
					<? foreach ($cities as $city) { ?>
						<option value="<?=$city['ID']?>"<? if ($city['ID'] == $c) { ?> selected<? } ?>><?=$city['NAME']?></option>
					<? } ?>
				      </select>
				    </div>
				    <div class="col-12 col-sm-2">
				      <button class="btn-remove" type="button" onclick="$(this).closest('.form-row').remove()"></button>
				    </div>
				</div>
			<? } ?>
		<? } */?>
		<? if (!empty($arResult["USER_PROPERTIES"]["DATA"]['UF_ADD_CITIES_NEW']['VALUE'])) { ?>
			<? foreach ($arResult["USER_PROPERTIES"]["DATA"]['UF_ADD_CITIES_NEW']['VALUE'] as $c) { ?>
				<div class="form-row row row--vcentr">
				    <div class="col-12 col-sm-10">
				      <input id="profile_city" name="UF_ADD_CITIES_NEW[]" class="input-text js-city-select__input js-city-input" 
					     data-cities-src="/ajax/cities.php" type="text" maxlength="50"
					     value="<?=$c?>">
				    </div>
				    <div class="col-12 col-sm-2">
				      <button class="btn-remove" type="button" onclick="$(this).closest('.form-row').remove()"></button>
				    </div>
				</div>
			<? } ?>
		<? } ?>
		
		
		
		<div class="form-row">
                    <!-- В onclick просто пример!-->
                    <button class="link text-small" type="button" 
			    onclick="$(this).parent().before(
			'&lt;div class=&quot;form-row row row--vcentr&quot;&gt;'+
				    '&lt;div class=&quot;col-12 col-sm-10&quot;&gt;'+
				      '&lt;input name=&quot;UF_ADD_CITIES_NEW[]&quot; class=&quot;input-text js-city-select__input js-city-input&quot; '+
					     'data-cities-src=&quot;/ajax/cities.php&quot; type=&quot;text&quot; maxlength=&quot;50&quot;'+
					     'value=&quot;&quot;&gt;'+
				    '&lt;/div&gt;'+
				    '&lt;div class=&quot;col-12 col-sm-2&quot;&gt;'+
				      '&lt;button class=&quot;btn-remove&quot; type=&quot;button&quot; onclick=&quot;$(this).closest(\&#039;.form-row\&#039;).remove() &quot;&gt;&lt;/button&gt;'+
				    '&lt;/div&gt;'+
				'&lt;/div&gt;'); jsCityInputFunc();">
                      <span>Добавить город</span>
                    </button>
		</div>
		
		<?/*div class="form-row">
                    <!-- В onclick просто пример!-->
                    <button class="link text-small" type="button" 
			    onclick="$(this).parent().before(
			'&lt;div class=&quot;form-row row row--vcentr&quot;&gt;&lt;div class=&quot;col-xs-12 col-sm-10&quot;&gt;&lt;select name=&quot;UF_ADD_CITIES[]&quot; class=&quot;select&quot;&gt;'
				
			+'&lt;/select&gt;\n\
			&lt;/div&gt;\n\
			&lt;div class=&quot;col-xs-12 col-sm-2&quot;&gt;&lt;button class=&quot;btn-remove&quot; type=&quot;button&quot; \n\
			onclick=&quot;$(this).parent().parent().remove()&quot;&gt;&lt;/button&gt;&lt;/div&gt;&lt;/div&gt;')">
                      <span>Добавить город</span>
                    </button>
		</div*/?>
		
		
                  <div class="form-row form-row--push-top text-small">Присутствие с соцсетях:</div>
                  <!-- У youtube и tw модификатор на размер иконки-->
                  <div class="form-row row row--vcentr">
                    <div class="col-2 col-sm-1">
                      <label class="input-label" for="profile_additional_social_youtube">
                        <svg class="input-label__icon input-label__icon--big">
                          <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#youtube"></use>
                        </svg>
                      </label>
                    </div>
                    <div class="col-10 col-sm-11">
                      <input id="profile_additional_social_youtube" class="input-text" name="UF_IN_YT" value="<? echo $arResult["arUser"]["UF_IN_YT"]?>" type="text">
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-2 col-sm-1">
                      <label class="input-label" for="profile_additional_social_insta">
                        <svg class="input-label__icon">
                          <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#insta"></use>
                        </svg>
                      </label>
                    </div>
                    <div class="col-10 col-sm-11">
                      <input id="profile_additional_social_insta" class="input-text" name="UF_IN_IN" value="<? echo $arResult["arUser"]["UF_IN_IN"]?>" type="text">
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-2 col-sm-1">
                      <label class="input-label" for="profile_additional_social_fb">
                        <svg class="input-label__icon">
                          <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#fb"></use>
                        </svg>
                      </label>
                    </div>
                    <div class="col-10 col-sm-11">
                      <input id="profile_additional_social_fb" class="input-text" name="UF_IN_FB" value="<? echo $arResult["arUser"]["UF_IN_FB"]?>" type="text">
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-2 col-sm-1">
                      <label class="input-label" for="profile_additional_social_ok">
                        <svg class="input-label__icon">
                          <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#ok"></use>
                        </svg>
                      </label>
                    </div>
                    <div class="col-10 col-sm-11">
                      <input id="profile_additional_social_ok" class="input-text" name="UF_IN_OK" value="<? echo $arResult["arUser"]["UF_IN_OK"]?>" type="text">
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-2 col-sm-1">
                      <label class="input-label" for="profile_additional_social_tw">
                        <svg class="input-label__icon input-label__icon--big">
                          <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#tw"></use>
                        </svg>
                      </label>
                    </div>
                    <div class="col-10 col-sm-11">
                      <input id="profile_additional_social_tw" class="input-text" name="UF_IN_TW" value="<? echo $arResult["arUser"]["UF_IN_TW"]?>" type="text">
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-2 col-sm-1">
                      <label class="input-label" for="profile_additional_social_vk">
                        <svg class="input-label__icon">
                          <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#vk"></use>
                        </svg>
                      </label>
                    </div>
                    <div class="col-10 col-sm-11">
                      <input id="profile_additional_social_vk" class="input-text" name="UF_IN_VK" value="<? echo $arResult["arUser"]["UF_IN_VK"]?>" type="text">
                    </div>
                  </div>


                  <div class="form-row form-row--push-top">
                    <button class="btn btn--fluid" type="submit" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">
                      <span>Сохранить</span>
                    </button>
                  </div>
                </form>
              </div>
            </div>
            </div>

<? //deb($arResult, false); ?>