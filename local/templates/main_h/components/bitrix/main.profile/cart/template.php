<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>

<?ShowError($arResult["strProfileError"]);?>
<?
if ($arResult['DATA_SAVED'] == 'Y')
	ShowNote(GetMessage('PROFILE_DATA_SAVED'));
?>
<script type="text/javascript">
<!--
var opened_sections = [<?
$arResult["opened"] = $_COOKIE[$arResult["COOKIE_PREFIX"]."_user_profile_open"];
$arResult["opened"] = preg_replace("/[^a-z0-9_,]/i", "", $arResult["opened"]);
if (strlen($arResult["opened"]) > 0)
{
	echo "'".implode("', '", explode(",", $arResult["opened"]))."'";
}
else
{
	$arResult["opened"] = "reg";
	echo "'reg'";
}
?>];
//-->

var cookie_prefix = '<?=$arResult["COOKIE_PREFIX"]?>';
</script>

<?/*action="/cart/delivery/"*/?>
<form class="js-form-validator" novalidate  method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data" data-is_ajax="y" 
      data-add_params="&save=<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">
    
    <? if ($arResult['DATA_SAVED'] == 'Y') { ?>
	<script>
		document.location = '<?=$arParams["SUCCESS_PAGE"]?>';
	</script>
    <? } ?>
    
<?=$arResult["BX_SESSION_CHECK"]?>
<input type="hidden" name="LOGIN" maxlength="50" value="<? echo $arResult["arUser"]["LOGIN"]?>" />
<input type="hidden" name="lang" value="<?=LANG?>" />
<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
    <div class="row row--vcentr form-row">
      <div class="col-12 col-sm-3 col-md-2">
	<label class="input-label" for="checkout_user_name">Имя:</label>
      </div>
      <div class="col-12 col-sm-9 col-md-10">
	  <input id="checkout_user_name" class="input-text" type="text" required name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>">
      </div>
    </div>
    <div class="row row--vcentr form-row">
      <div class="col-12 col-sm-3 col-md-2">
	<label class="input-label" for="checkout_user_tel">Телефон:</label>
      </div>
      <div class="col-12 col-sm-9 col-md-10">
	<input id="checkout_user_tel" class="input-text js-mask-phone" type="tel" required name="PERSONAL_PHONE" maxlength="255" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>">
      </div>
    </div>
    <div class="row row--vcentr form-row">
      <div class="col-12 col-sm-3 col-md-2">
	<label class="input-label" for="checkout_user_email">Email:</label>
      </div>
      <div class="col-12 col-sm-9 col-md-10">
	<input id="checkout_user_email" class="input-text" type="email" required name="EMAIL" maxlength="50" value="<? echo $arResult["arUser"]["EMAIL"]?>">
      </div>
    </div>
    <div class="row row--vcentr form-row form-row--push-top">
      <div class="col-12 col-sm-6">
	<button class="btn btn--fluid" type="submit" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">
	  <span>Далее</span>
	</button>
      </div>
    </div>
</form>