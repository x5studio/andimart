<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>

<?ShowError($arResult["strProfileError"]);?>
<?
if ($arResult['DATA_SAVED'] == 'Y')
	ShowNote(GetMessage('PROFILE_DATA_SAVED'));
?>
<script type="text/javascript">
<!--
var opened_sections = [<?
$arResult["opened"] = $_COOKIE[$arResult["COOKIE_PREFIX"]."_user_profile_open"];
$arResult["opened"] = preg_replace("/[^a-z0-9_,]/i", "", $arResult["opened"]);
if (strlen($arResult["opened"]) > 0)
{
	echo "'".implode("', '", explode(",", $arResult["opened"]))."'";
}
else
{
	$arResult["opened"] = "reg";
	echo "'reg'";
}
?>];
//-->

var cookie_prefix = '<?=$arResult["COOKIE_PREFIX"]?>';
</script>


<div class="content-block content-block--sm-top content-block--sm-bot island">
<div class="island-head-tabs">
              <div class="island-head-tabs__item">
		  <a class="island-head-tabs__link" href="/lichniy_cabinet/order/">
                  <div class="island-head-tabs__title">История заказов</div>
                </a>
              </div>
              <div class="island-head-tabs__item island-head-tabs__item--current">
                <div class="island-head-tabs__title">Профиль</div>
              </div>
            </div>
            <div class="row">
              <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3">
                <form class="form js-form-validator" novalidate method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">
                  <!-- У этой и других форм добавлена валидация (класс js-form-validator). Это демо для верстки! Пример интеграции валидатора с формой можно посмотреть у формы подписки (src/scripts/app/forms/subscribe-form.js)-->
		  
<?=$arResult["BX_SESSION_CHECK"]?>
<input type="hidden" name="LOGIN" maxlength="50" value="<? echo $arResult["arUser"]["LOGIN"]?>" />
<input type="hidden" name="lang" value="<?=LANG?>" />
<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-2">
                      <label class="input-label" for="profile_name">Имя:</label>
                    </div>
                    <div class="col-12 col-sm-10">
                      <input id="profile_name" class="input-text" type="text" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" required>
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-2">
                      <label class="input-label" for="profile_tel">Телефон:</label>
                    </div>
                    <div class="col-12 col-sm-10">
                      <input id="profile_tel" class="input-text js-mask-phone" type="tel" name="PERSONAL_PHONE" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" required>
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-2">
                      <label class="input-label" for="profile_email">Email:</label>
                    </div>
                    <div class="col-12 col-sm-10">
                      <input id="profile_email" class="input-text" type="email" name="EMAIL" maxlength="50" value="<? echo $arResult["arUser"]["EMAIL"]?>" required>
                    </div>
                  </div>
                  <div class="form-row form-row--push-top text-small text-bold">Сменить пароль</div>
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-2">
                      <label class="input-label" for="profile_password">Пароль:</label>
                    </div>
                    <div class="col-12 col-sm-10">
			<input id="profile_password" class="input-text" type="password" name="NEW_PASSWORD" required autocomplete="off">
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-2">
                      <label class="input-label" for="profile_password_repeat">Пароль ещё&nbsp;раз:</label>
                    </div>
                    <div class="col-12 col-sm-10">
                      <input id="profile_password_repeat" class="input-text" type="password" name="NEW_PASSWORD_CONFIRM" required autocomplete="off">
                    </div>
                  </div>
                  <div class="form-row form-row--push-top">
                    <button class="btn btn--fluid" type="submit" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">
                      <span>Сохранить</span>
                    </button>
                  </div>
                </form>
              </div>
            </div>
            </div>