<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>

<?ShowError($arResult["strProfileError"]);?>
<?
if ($arResult['DATA_SAVED'] == 'Y')
	ShowNote(GetMessage('PROFILE_DATA_SAVED'));
?>
<script type="text/javascript">
<!--
var opened_sections = [<?
$arResult["opened"] = $_COOKIE[$arResult["COOKIE_PREFIX"]."_user_profile_open"];
$arResult["opened"] = preg_replace("/[^a-z0-9_,]/i", "", $arResult["opened"]);
if (strlen($arResult["opened"]) > 0)
{
	echo "'".implode("', '", explode(",", $arResult["opened"]))."'";
}
else
{
	$arResult["opened"] = "reg";
	echo "'reg'";
}
?>];
//-->

var cookie_prefix = '<?=$arResult["COOKIE_PREFIX"]?>';
</script>

<div class="content-block content-block--sm-top content-block--sm-bot island">
            <h2 class="section-title title-line js-title-line">Дополнительная информация</h2>
            <div class="row">
              <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3">
                <?/*form class="js-form-validator form" novalidate action="/">
		    
		    
		    
                  <!-- У этой и других форм добавлена валидация (класс js-form-validator). Это демо для верстки! Пример интеграции валидатора с формой можно посмотреть у формы подписки (src/scripts/app/forms/subscribe-form.js)-->
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-2">
                      <label class="input-label" for="profile_additional_name">Адрес:</label>
                    </div>
                    <div class="col-12 col-sm-10">
                      <input id="profile_additional_name" class="input-text" type="text">
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-2">
                      <label class="input-label" for="profile_additional_time">Время работы:</label>
                    </div>
                    <div class="col-12 col-sm-10">
                      <input id="profile_additional_time" class="input-text" type="text">
                    </div>
                  </div>
                  <div class="form-row form-row--push-top text-small text-bold">Доступность выезда в другие города:</div>
                  <!-- В onclick кнопок удаления просто пример!-->
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-10">
                      <select class="select">
                        <option value selected>Москва</option>
                        <option value="">Новосибирск</option>
                        <option value="">Санкт-Петербург</option>
                        <option value="">Екатеринбург</option>
                      </select>
                    </div>
                    <div class="col-12 col-sm-2">
                      <button class="btn-remove" type="button" onclick="$(this).closest('.form-row').remove()"></button>
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-10">
                      <select class="select">
                        <option value="">Москва</option>
                        <option value selected>Новосибирск</option>
                        <option value="">Санкт-Петербург</option>
                        <option value="">Екатеринбург</option>
                      </select>
                    </div>
                    <div class="col-12 col-sm-2">
                      <button class="btn-remove" type="button" onclick="$(this).parent().parent().remove()"></button>
                    </div>
                  </div>
                  <div class="form-row">
                    <!-- В onclick просто пример!-->
                    <button class="link text-small" type="button" onclick="$(this).parent().before('&lt;div class=&quot;form-row row row--vcentr&quot;&gt;&lt;div class=&quot;col-xs-12 col-sm-10&quot;&gt;&lt;select class=&quot;select&quot;&gt;&lt;option&gt;Москва&lt;/option&gt;&lt;option selected disabled hidden&gt;Выбрать&lt;/option&gt;&lt;option&gt;Санкт-Петербург&lt;/option&gt;&lt;option&gt;Екатеринбург&lt;/option&gt;&lt;/select&gt;&lt;/div&gt;&lt;div class=&quot;col-xs-12 col-sm-2&quot;&gt;&lt;button class=&quot;btn-remove&quot; type=&quot;button&quot; onclick=&quot;$(this).parent().parent().remove()&quot;&gt;&lt;/button&gt;&lt;/div&gt;&lt;/div&gt;')">
                      <span>Добавить город</span>
                    </button>
                  </div>
                  <div class="form-row form-row--push-top text-small">Присутствие с соцсетях:</div>
                  <!-- У youtube и tw модификатор на размер иконки-->
                  <div class="form-row row row--vcentr">
                    <div class="col-2 col-sm-1">
                      <label class="input-label" for="profile_additional_social_youtube">
                        <svg class="input-label__icon input-label__icon--big">
                          <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#youtube"></use>
                        </svg>
                      </label>
                    </div>
                    <div class="col-10 col-sm-11">
                      <input id="profile_additional_social_youtube" class="input-text" type="text">
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-2 col-sm-1">
                      <label class="input-label" for="profile_additional_social_insta">
                        <svg class="input-label__icon">
                          <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#insta"></use>
                        </svg>
                      </label>
                    </div>
                    <div class="col-10 col-sm-11">
                      <input id="profile_additional_social_insta" class="input-text" type="text">
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-2 col-sm-1">
                      <label class="input-label" for="profile_additional_social_fb">
                        <svg class="input-label__icon">
                          <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#fb"></use>
                        </svg>
                      </label>
                    </div>
                    <div class="col-10 col-sm-11">
                      <input id="profile_additional_social_fb" class="input-text" type="text">
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-2 col-sm-1">
                      <label class="input-label" for="profile_additional_social_ok">
                        <svg class="input-label__icon">
                          <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#ok"></use>
                        </svg>
                      </label>
                    </div>
                    <div class="col-10 col-sm-11">
                      <input id="profile_additional_social_ok" class="input-text" type="text">
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-2 col-sm-1">
                      <label class="input-label" for="profile_additional_social_tw">
                        <svg class="input-label__icon input-label__icon--big">
                          <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#tw"></use>
                        </svg>
                      </label>
                    </div>
                    <div class="col-10 col-sm-11">
                      <input id="profile_additional_social_tw" class="input-text" type="text">
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-2 col-sm-1">
                      <label class="input-label" for="profile_additional_social_vk">
                        <svg class="input-label__icon">
                          <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#vk"></use>
                        </svg>
                      </label>
                    </div>
                    <div class="col-10 col-sm-11">
                      <input id="profile_additional_social_vk" class="input-text" type="text">
                    </div>
                  </div>
		 * 
		 * 
                  <div class="form-row form-row--push-top">
                    <button class="btn btn--fluid" type="submit">
                      <span>Сохранить</span>
                    </button>
                  </div>
                </form*/?>
<form class="js-form-validator form" novalidate method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">
<?=$arResult["BX_SESSION_CHECK"]?>
<input type="hidden" name="LOGIN" maxlength="50" value="<? echo $arResult["arUser"]["LOGIN"]?>" />
<input type="hidden" name="EMAIL" maxlength="50" value="<? echo $arResult["arUser"]["EMAIL"]?>" />
<input type="hidden" name="lang" value="<?=LANG?>" />
<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
		
		<!-- У этой и других форм добавлена валидация (класс js-form-validator). Это демо для верстки! Пример интеграции валидатора с формой можно посмотреть у формы подписки (src/scripts/app/forms/subscribe-form.js)-->
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-2">
                      <label class="input-label" for="profile_additional_name">Адрес:</label>
                    </div>
                    <div class="col-12 col-sm-10">
                      <input id="profile_additional_name" class="input-text" name="WORK_STREET" value="<? echo $arResult["arUser"]["WORK_STREET"]?>" type="text">
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-2">
                      <label class="input-label" for="profile_additional_time">Время работы:</label>
                    </div>
                    <div class="col-12 col-sm-10">
                      <input id="profile_additional_time" class="input-text" name="WORK_NOTES" value="<? echo $arResult["arUser"]["WORK_NOTES"]?>" type="text">
                    </div>
                  </div>
                  <?/*div class="form-row form-row--push-top text-small text-bold">Доступность выезда в другие города:</div>
                  <!-- В onclick кнопок удаления просто пример!-->
                  <div class="form-row row row--vcentr">
                    <div class="col-12 col-sm-10">
                      <select class="select">
                        <option value selected>Москва</option>
                        <option value="">Новосибирск</option>
                        <option value="">Санкт-Петербург</option>
                        <option value="">Екатеринбург</option>
                      </select>
                    </div>
                    <div class="col-12 col-sm-2">
                      <button class="btn-remove" type="button" onclick="$(this).closest('.form-row').remove()"></button>
                    </div>
                  </div>
                  <div class="form-row">
                    <!-- В onclick просто пример!-->
                    <button class="link text-small" type="button" onclick="$(this).parent().before('&lt;div class=&quot;form-row row row--vcentr&quot;&gt;&lt;div class=&quot;col-xs-12 col-sm-10&quot;&gt;&lt;select class=&quot;select&quot;&gt;&lt;option&gt;Москва&lt;/option&gt;&lt;option selected disabled hidden&gt;Выбрать&lt;/option&gt;&lt;option&gt;Санкт-Петербург&lt;/option&gt;&lt;option&gt;Екатеринбург&lt;/option&gt;&lt;/select&gt;&lt;/div&gt;&lt;div class=&quot;col-xs-12 col-sm-2&quot;&gt;&lt;button class=&quot;btn-remove&quot; type=&quot;button&quot; onclick=&quot;$(this).parent().parent().remove()&quot;&gt;&lt;/button&gt;&lt;/div&gt;&lt;/div&gt;')">
                      <span>Добавить город</span>
                    </button>
                  </div*/?>
		
		
		<?/*div class="form-row row">
                    <div class="col-12 col-sm-2">
                      
                    </div>
                    <div class="col-12 col-sm-10">
			<div class="scroll-block js-scroll-block">
				<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):
					if($FIELD_NAME!="UF_ADD_CITIES")
						continue;

					?>

					<?$APPLICATION->IncludeComponent(
					"bitrix:system.field.edit",
					$arUserField["USER_TYPE"]["USER_TYPE_ID"],
					array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "regform"), null, array("HIDE_ICONS"=>"Y"));?>
				<?endforeach;?>

			</div>
                    </div>
                  </div*/?>
		<? //deb($arResult, false); ?>
		<? 
		$cities = array();
		$cities_t = CIBlockElement::GetList(array('name' => 'asc'), array('IBLOCK_ID' => const_IBLOCK_ID_cities, 'ACTIVE' => 'Y'));
		while ($city = $cities_t->Fetch())
		{
			$cities[] = $city;
		}
		
		
//		deb($arResult["USER_PROPERTIES"]["DATA"]['UF_ADD_CITIES']['VALUE'], false);
		?>
		
		
		<div class="form-row form-row--push-top text-small text-bold">Доступность выезда в другие города:</div>
		
		<? if (!empty($arResult["USER_PROPERTIES"]["DATA"]['UF_ADD_CITIES']['VALUE'])) { ?>
			<? foreach ($arResult["USER_PROPERTIES"]["DATA"]['UF_ADD_CITIES']['VALUE'] as $c) { ?>
				<div class="form-row row row--vcentr">
				    <div class="col-12 col-sm-10">
				      <select class="select" name="UF_ADD_CITIES[]">
					<? foreach ($cities as $city) { ?>
						<option value="<?=$city['ID']?>"<? if ($city['ID'] == $c) { ?> selected<? } ?>><?=$city['NAME']?></option>
					<? } ?>
				      </select>
				    </div>
				    <div class="col-12 col-sm-2">
				      <button class="btn-remove" type="button" onclick="$(this).closest('.form-row').remove()"></button>
				    </div>
				</div>
			<? } ?>
		<? } ?>
		<div class="form-row">
                    <!-- В onclick просто пример!-->
                    <button class="link text-small" type="button" 
			    onclick="$(this).parent().before(
			'&lt;div class=&quot;form-row row row--vcentr&quot;&gt;&lt;div class=&quot;col-xs-12 col-sm-10&quot;&gt;&lt;select name=&quot;UF_ADD_CITIES[]&quot; class=&quot;select&quot;&gt;'
				<? foreach ($cities as $city) { ?>
					+'&lt;option value=&quot;<?=$city['ID']?>&quot;&gt;<?=$city['NAME']?>&lt;/option&gt;'
				<? } ?>
			+'&lt;/select&gt;\n\
			&lt;/div&gt;\n\
			&lt;div class=&quot;col-xs-12 col-sm-2&quot;&gt;&lt;button class=&quot;btn-remove&quot; type=&quot;button&quot; \n\
			onclick=&quot;$(this).parent().parent().remove()&quot;&gt;&lt;/button&gt;&lt;/div&gt;&lt;/div&gt;')">
                      <span>Добавить город</span>
                    </button>
		</div>
		
		
                  <div class="form-row form-row--push-top text-small">Присутствие с соцсетях:</div>
                  <!-- У youtube и tw модификатор на размер иконки-->
                  <div class="form-row row row--vcentr">
                    <div class="col-2 col-sm-1">
                      <label class="input-label" for="profile_additional_social_youtube">
                        <svg class="input-label__icon input-label__icon--big">
                          <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#youtube"></use>
                        </svg>
                      </label>
                    </div>
                    <div class="col-10 col-sm-11">
                      <input id="profile_additional_social_youtube" class="input-text" name="UF_IN_YT" value="<? echo $arResult["arUser"]["UF_IN_YT"]?>" type="text">
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-2 col-sm-1">
                      <label class="input-label" for="profile_additional_social_insta">
                        <svg class="input-label__icon">
                          <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#insta"></use>
                        </svg>
                      </label>
                    </div>
                    <div class="col-10 col-sm-11">
                      <input id="profile_additional_social_insta" class="input-text" name="UF_IN_IN" value="<? echo $arResult["arUser"]["UF_IN_IN"]?>" type="text">
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-2 col-sm-1">
                      <label class="input-label" for="profile_additional_social_fb">
                        <svg class="input-label__icon">
                          <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#fb"></use>
                        </svg>
                      </label>
                    </div>
                    <div class="col-10 col-sm-11">
                      <input id="profile_additional_social_fb" class="input-text" name="UF_IN_FB" value="<? echo $arResult["arUser"]["UF_IN_FB"]?>" type="text">
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-2 col-sm-1">
                      <label class="input-label" for="profile_additional_social_ok">
                        <svg class="input-label__icon">
                          <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#ok"></use>
                        </svg>
                      </label>
                    </div>
                    <div class="col-10 col-sm-11">
                      <input id="profile_additional_social_ok" class="input-text" name="UF_IN_OK" value="<? echo $arResult["arUser"]["UF_IN_OK"]?>" type="text">
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-2 col-sm-1">
                      <label class="input-label" for="profile_additional_social_tw">
                        <svg class="input-label__icon input-label__icon--big">
                          <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#tw"></use>
                        </svg>
                      </label>
                    </div>
                    <div class="col-10 col-sm-11">
                      <input id="profile_additional_social_tw" class="input-text" name="UF_IN_TW" value="<? echo $arResult["arUser"]["UF_IN_TW"]?>" type="text">
                    </div>
                  </div>
                  <div class="form-row row row--vcentr">
                    <div class="col-2 col-sm-1">
                      <label class="input-label" for="profile_additional_social_vk">
                        <svg class="input-label__icon">
                          <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#vk"></use>
                        </svg>
                      </label>
                    </div>
                    <div class="col-10 col-sm-11">
                      <input id="profile_additional_social_vk" class="input-text" name="UF_IN_VK" value="<? echo $arResult["arUser"]["UF_IN_VK"]?>" type="text">
                    </div>
                  </div>


                  <div class="form-row form-row--push-top">
                    <button class="btn btn--fluid" type="submit" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">
                      <span>Сохранить</span>
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>