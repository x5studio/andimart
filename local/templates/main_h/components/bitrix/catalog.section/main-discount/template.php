<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<? if (!empty($arResult['ITEMS'])) { ?>
<div class="content-block js-carousel js-carousel--catalog" data-rows="2">
        <h3 class="section-title title-line js-title-line">Товары со скидкой</h3>
        <div class="carousel-nav carousel-nav--top">
            <button class="carousel-nav__prev js-carousel__prev" type="button"></button>
            <div class="carousel-nav__stat js-carousel__stat"></div>
            <button class="carousel-nav__next js-carousel__next" type="button"></button>
        </div>
	
        <div class="catalog grid grid--xs-2 grid--sm-3 grid--md-4 js-carousel__items">
	<?
	foreach ($arResult['ITEMS'] as $key => $arItem)
	{
		//p($arItem["PRICES"]);
		$optimal_price_key = "";
		$opt_price = 99999999999;
		foreach ($arItem["PRICES"] as $key => $price)
		{
			if ($opt_price > $price["VALUE"])
			{
				$opt_price = $price["VALUE"];
				$optimal_price_key = $key;
			}
		}
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
		$strMainID = $this->GetEditAreaId($arItem['ID']);

		$arItemIDs = array(
		    'ID' => $strMainID,
		    'PICT' => $strMainID . '_pict',
		    'SECOND_PICT' => $strMainID . '_secondpict',
		    'STICKER_ID' => $strMainID . '_sticker',
		    'SECOND_STICKER_ID' => $strMainID . '_secondsticker',
		    'QUANTITY' => $strMainID . '_quantity',
		    'QUANTITY_DOWN' => $strMainID . '_quant_down',
		    'QUANTITY_UP' => $strMainID . '_quant_up',
		    'QUANTITY_MEASURE' => $strMainID . '_quant_measure',
		    'BUY_LINK' => $strMainID . '_buy_link',
		    'BASKET_ACTIONS' => $strMainID . '_basket_actions',
		    'NOT_AVAILABLE_MESS' => $strMainID . '_not_avail',
		    'SUBSCRIBE_LINK' => $strMainID . '_subscribe',
		    'COMPARE_LINK' => $strMainID . '_compare_link',
		    'PRICE' => $strMainID . '_price',
		    'DSC_PERC' => $strMainID . '_dsc_perc',
		    'SECOND_DSC_PERC' => $strMainID . '_second_dsc_perc',
		    'PROP_DIV' => $strMainID . '_sku_tree',
		    'PROP' => $strMainID . '_prop_',
		    'DISPLAY_PROP_DIV' => $strMainID . '_sku_prop',
		    'BASKET_PROP_DIV' => $strMainID . '_basket_prop',
		);
		?>
		<div class="catalog__item grid__item">
		<?$frame = $this->createFrame()->begin("");?>
		    <div class="catalog__item-in" <? echo $strMainID; ?>>
			<div class="catalog__img">
			    <a class="link-hidden catalog__img_bgr" href="<?= $arItem["DETAIL_PAGE_URL"] ?>" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>')">
				<img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"  alt="<?= $arItem["NAME"] ?>">
			    </a>
			</div>
			<div class="catalog__content">
			    <div class="catalog__content-top">
				<div class="catalog__title">
				    <a class="link-hidden" href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a>
				</div>
				<div class="catalog__brand">
					<?
					if ($arItem["PROPERTIES"]["BREND"]["VALUE"])
					{
						?>
						Бренд: <?= $arItem["PROPERTIES"]["BREND"]["VALUE"] ?>
						<?
					}
					?>
					<? if ($arItem["CATALOG_QUANTITY"]) {//CATALOG_QUANTITY?>
					    <div class="catalog__avail">
						<span id="text-in-stock-product-id-<?=$arItem['ID']?>" class="text-success" data-quantity-in-stock="<?=$arItem["CATALOG_QUANTITY"]?>">●</span> в наличии</div>
					<? } else { ?>
					    <div class="catalog__avail">
						<span id="text-in-stock-product-id-<?=$arItem['ID']?>" class="text-warn" data-quantity-in-stock="0">●</span> нет наличии</div>
					<? } ?>
				</div>
			    </div>

				<? if (!empty($arItem["PRICES"][$optimal_price_key]["PRINT_DISCOUNT_VALUE"]) && !empty($arItem["CATALOG_QUANTITY"])) { ?>
					<div class="catalog__content-bot">
						<div class="product-price">
							<? if ($arItem["PRICES"][$optimal_price_key]["DISCOUNT_DIFF"]) { ?>
								<div class="product-price__old">
									<?= $arItem["PRICES"][$optimal_price_key]["PRINT_VALUE"]; ?>
								</div>
							<? } ?>
							<div class="product-price__current"><?= $arItem["PRICES"][$optimal_price_key]["PRINT_DISCOUNT_VALUE"] ?></div>
						</div>
						<div class="catalog__buy">
						    <div class="catalog__buy-count">
							<div class="input-group">
							    <div class="input-group__item catalog__buy-input">
                                <div class="product-card-wrapper">
                                    <span class="product-card-item-amount-btn-minus catalog_section_minus" data-entity="product-card-item-quantity-minus"><i class="fas fa-minus" style="font-size: 0.8em;"></i></span>
							        <input data-quntity="<?= $arItem["CATALOG_QUANTITY"] ?>" pattern="^[1-9]{1}[0-9]{3}" id="textbox-desired-quantity-product-id-<?=$arItem['ID']?>" class="input-text input-text--sm text-center section_quantity_input" type="text" value="1">
                                    <span class="product-card-item-amount-btn-plus catalog_section_plus" data-entity="product-card-item-quantity-plus"><i class="fas fa-plus" style="font-size: 0.8em;"></i></span>
                                </div>
							    </div>
							    <div class="input-group__item catalog__buy-label">
								    <div class="input-posfix">шт.</div>
							    </div>
							</div>
						    </div>
						    <div class="catalog__buy-btn">
                                <button id="btn-buy-product-id-<?=$arItem['ID']?>" class="btn btn--sm add_to_cart_section js-btn-to-basket" rel="<?= $arItem["ADD_URL"] ?>" type="button">
                                    <span>Купить</span>
                                </button>
                                <a href="/cart/" class="btn btn--inverse btn--sm btn-in-basket" style="display: none">
                                    <span>В корзине</span>
                                </a>
						    </div>
						    <!-- Compare Button -->
						    <?
								$iblockid = $arItem['IBLOCK_ID'];
								$id=$arItem['ID'];
								if(isset($_SESSION["CATALOG_COMPARE_LIST"][$iblockid]["ITEMS"][$id])) { $clascomp='active'; }else{ $clascomp=''; }
							?>
						    <div class="catalog_compare-btn">
						    	<noindex>
									<a href="#" rel="nofollow" data-id="<?=$arItem["ID"];?>" class="sky-compare-btn <?=$clascomp?>" title="Сравнить">
										<svg version="1.0" xmlns="http://www.w3.org/2000/svg" width="479.000000pt" height="479.000000pt" viewBox="0 0 479.000000 479.000000" preserveAspectRatio="xMidYMid meet">
										<metadata>
										Created by potrace 1.15, written by Peter Selinger 2001-2017
										</metadata>
										<g transform="translate(0.000000,479.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
										<path d="M1979 4137 c-18 -12 -44 -38 -56 -56 l-23 -34 2 -1533 c3 -1529 3
										-1533 24 -1561 11 -15 40 -38 64 -51 43 -22 51 -22 405 -22 354 0 362 0 405
										23 33 16 51 34 68 67 l22 45 -2 1521 -3 1521 -28 36 c-52 68 -44 67 -465 67
										-378 0 -379 0 -413 -23z"></path>
										<path d="M3370 3063 c-37 -14 -74 -49 -91 -87 -18 -39 -19 -86 -19 -1002 l0
										-961 23 -43 c16 -30 37 -51 67 -67 l44 -24 377 3 376 3 37 29 c70 53 66 -8 66
										1069 l0 974 -23 34 c-12 18 -38 44 -56 56 -34 23 -35 23 -410 22 -207 0 -383
										-3 -391 -6z"></path>
										<path d="M621 2233 c-18 -9 -45 -34 -60 -56 l-26 -41 -3 -520 c-2 -287 0 -544
										3 -573 7 -57 34 -104 78 -137 27 -20 41 -21 395 -24 241 -2 380 1 404 8 45 13
										94 67 108 120 7 26 10 218 8 582 -3 607 0 582 -75 633 l-38 25 -380 0 c-333 0
										-384 -3 -414 -17z"></path>
										</g>
										</svg>
									</a>
								</noindex>
						    </div>
						</div>
					</div>
				<? } ?>
			</div>
		    </div>
		    <?$frame->end();?>
		</div>

		<?
	}
	?>
        </div>
    </div>
<? } ?>