"use strict";

$(document).ready(function () {
    const $btnBasket = $('.js-btn-to-basket');
    $btnBasket.on('click', function () {
        const lengthOfBtnIdPrefix = 19;
        var productId = this.id.substr(lengthOfBtnIdPrefix);

        var quantityInStock = $('#text-in-stock-product-id-' + productId).data('quantity-in-stock');

        var desiredQuantity = $('#textbox-desired-quantity-product-id-' + productId).val();

        if($.isNumeric(desiredQuantity)) {
            if(desiredQuantity <= quantityInStock) {
                $(this).hide();
                $(this).next().show();
            }
        } else {
            $(this).hide();
            $(this).next().show();
        }
    })
});

function initPlusMinusButtonEvents() {
    $(".catalog_section_plus").on("click",function(){
        var input  = $(this).parent().find(".section_quantity_input");
        if(!!input){
            var max_quantity = input.data("quntity");
            if(!max_quantity){
                max_quantity = 99999;
            }


            var min_quantity = 1;
            var cur_val = parseInt(input.val());
            var new_val = ++cur_val;

            if(new_val>=min_quantity && new_val<=max_quantity){
                input.val(new_val);
            }else{
                input.val(max_quantity);
            }


        }else{
            console.log("no input");
        }
    });
    $(".catalog_section_minus").on("click",function(){
        var input  = $(this).parent().find(".section_quantity_input");
        if(!!input){
            var max_quantity = input.data("quntity");
            if(!max_quantity){
                max_quantity = 99999;
            }


            var min_quantity = 1;
            var cur_val = parseInt(input.val());
            var new_val = --cur_val;

            if(new_val>=min_quantity && new_val<=max_quantity){
                input.val(new_val);
            }else{
                input.val(min_quantity);
            }


        }else{
            console.log("no input");
        }
    });

    $(".section_quantity_input").on("change",function() {
        var input  = $(this);
        var max_quantity = input.data("quntity");
        if(!max_quantity){
            max_quantity = 99999;
        }
        var min_quantity = 1;
        var new_val = input.val();
        if(new_val>=min_quantity && new_val<=max_quantity){
            input.val(new_val);
        }else{
            if(new_val>max_quantity)
                input.val(max_quantity);
            if(new_val<min_quantity)
                input.val(min_quantity)
        }
    });
    $(".section_quantity_input").on("keypress",function(event) {
        event= event || window.event;
        if (event.charCode && (event.charCode < 48 || event.charCode > 57))
            return false;
    });
}


$(document).ready(function () {
    initPlusMinusButtonEvents();
});