<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
if(count($arResult['ITEMS'])>0){
?>
	<div class="image-slider js-carousel js-carousel--slider">
	<div class="image-slider__items js-carousel__items">
	<?
foreach ($arResult['ITEMS'] as $key => $arItem) {
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
	$strMainID = $this->GetEditAreaId($arItem['ID']);
	//p($arItem);
?>

			<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="540" height="332" alt>


<?
}
?>
</div>
<?
if(count($arResult['ITEMS'])>1){
?>
	
		<div class="carousel-nav">
			<button class="carousel-nav__prev js-carousel__prev" type="button"></button>
			<div class="carousel-nav__stat js-carousel__stat"></div>
			<button class="carousel-nav__next js-carousel__next" type="button"></button>
		</div>
	
	<?
}
?>
</div>
<?
}
	?>
