<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
 <a class="cart-widget" href="/cart/">
	<div class="cart-widget__count">
	    <span class="cart-widget__count-val"><?=$arResult['REAL_NUM_PRODUCTS']?></span> <?=number_end($arResult["NUM_PRODUCTS"],array("товар","товара" ,"товаров") )?></div>
	<div class="cart-widget__sum">на <?=$arResult["TOTAL_PRICE"]?>.</div>
    </a>