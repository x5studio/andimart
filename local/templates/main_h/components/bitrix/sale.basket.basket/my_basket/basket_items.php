<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
use Bitrix\Sale\DiscountCouponsManager;




if (!empty($arResult["ERROR_MESSAGE"]))
	ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn  = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn  = false;
$bPriceType    = false;

if ($normalCount > 0):
?>

<div id="basket_items_list">
	<div class="content-block">
	    <div class="bx_ordercart_order_table_container">
		    <div class="cart-items" id="basket_items">
			<div class="cart-items__head hidden-xs-down">
			    <div class="row">
				<div class="col-sm-5" id="col_NAME">Товар</div>
				<div class="col-sm-2">Доступность</div>
				<div class="col-sm-1" id="col_PRICE">Цена</div>
				<div class="col-sm-2">
				    <div class="cart-items__count" id="col_QUANTITY">Кол-во</div>
				</div>
				<div class="col-sm-2" id="col_SUM">Сумма</div>
			    </div>
			</div>
			<ul class="cart-items__list">
			    <?
			    foreach ($arResult["GRID"]["ROWS"] as $k => $arItem) { 

				    if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y") {
			    ?>
			    <li class="cart-items__item" id="<?=$arItem["ID"]?>">
			      <div class="row row--vcentr b-list">
				<div class="col-6 col-sm-2 b-list__item">
				    <?
				    if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0):
					    $url = $arItem["PREVIEW_PICTURE_SRC"];
				    elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0):
					    $url = $arItem["DETAIL_PICTURE_SRC"];
				    else:
					    $url = $templateFolder."/images/no_photo.png";
				    endif;
				    ?>
				    <img class="flexible-media" src="<?=$url?>" width="200" height="200" alt>
				    
				    <div class="bx_scu" style="display: none;">
					    <ul id="prop_<?=$arProp["CODE"]?>_<?=$arItem["ID"]?>"
						    style="width: 200%; margin-left:0;"
						    class="sku_prop_list"
						    >
						    <?
						    foreach ($arProp["VALUES"] as $valueId => $arSkuValue):

							    $selected = "";
							    foreach ($arItem["PROPS"] as $arItemProp):
								    if ($arItemProp["CODE"] == $arItem["SKU_DATA"][$propId]["CODE"])
								    {
									    if ($arItemProp["VALUE"] == $arSkuValue["NAME"] || $arItemProp["VALUE"] == $arSkuValue["XML_ID"])
										    $selected = " bx_active";
								    }
							    endforeach;
						    ?>
							    <li style="width:10%;"
								    class="sku_prop<?=$selected?>"
								    data-value-id="<?=$arSkuValue["XML_ID"]?>"
								    data-element="<?=$arItem["ID"]?>"
								    data-property="<?=$arProp["CODE"]?>"
								    >
								    <a href="javascript:void(0)" class="cnt"><span class="cnt_item" style="background-image:url(<?=$arSkuValue["PICT"]["SRC"];?>)"></span></a>
							    </li>
						    <?
						    endforeach;
						    ?>
					    </ul>
				    </div>
				</div>
				<div class="col-6 col-sm-3 b-list__item">
				  <div class="text-bold">
				    <a class="link-hidden" href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?=$arItem["NAME"]?></a>
				  </div><? if (!empty($arItem["BRAND"])) { ?>Бренд: <?=$arItem["BRAND"]?><? } ?></div>
				<div class="col-6 col-sm-2 text-small-expand b-list__item">
				  <span class="text-success">●</span> В наличии: <?=$arItem["AVAILABLE_QUANTITY"]?>&nbsp;шт.</div>
				<div class="col-6 col-sm-1 text-small-expand text-nowrap b-list__item">
				  <div class="hidden-sm-up text-gray">Цена: </div>
				  <div class="product-price">
				    <?if (floatval($arItem["DISCOUNT_PRICE_PERCENT"]) > 0) { ?>
				    <div class="product-price__old" id="old_price_<?=$arItem["ID"]?>"><?=$arItem["FULL_PRICE_FORMATED"]?></div>
				    <? } else { ?>
				    <div style="display: none;" class="product-price__old" id="old_price_<?=$arItem["ID"]?>"><?=$arItem["FULL_PRICE_FORMATED"]?></div>
				    <? } ?>
				    <div class="product-price__current" id="current_price_<?=$arItem["ID"]?>"><?=$arItem["PRICE_FORMATED"]?></div>
				    
				    
				  </div>
				</div>
				<div class="col-6 col-sm-2 text-small-expand b-list__item b-list__item">
				  <div class="cart-items__count">
				    <div class="input-group">
				      <div class="input-group__item input-group__item--padd-r hidden-sm-up">
					<span class="text-gray">Кол-во:</span>
				      </div>
				      <div class="input-group__item">
					  
					  
					<?
					$ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
					$max = isset($arItem["AVAILABLE_QUANTITY"]) ? "max=\"".$arItem["AVAILABLE_QUANTITY"]."\"" : "";
					$useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
					$useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
					?>
					<input class="input-text input-text--fixwidth-sm text-center" 
					    type="text"
					    size="3"
					    id="QUANTITY_INPUT_<?=$arItem["ID"]?>"
					    name="QUANTITY_INPUT_<?=$arItem["ID"]?>"
					    size="2"
					    maxlength="18"
					    min="0"
					    <?=$max?>
					    step="<?=$ratio?>"
					    style="max-width: 50px"
					    value="<?=$arItem["QUANTITY"]?>"
					    onchange="updateQuantity('QUANTITY_INPUT_<?=$arItem["ID"]?>', '<?=$arItem["ID"]?>', <?=$ratio?>, <?=$useFloatQuantityJS?>)">
				      </div>
					<input type="hidden" id="QUANTITY_<?=$arItem['ID']?>" name="QUANTITY_<?=$arItem['ID']?>" value="<?=$arItem["QUANTITY"]?>" />
				    </div>
				    <button class="cart-items__rm" type="button" onclick="document.location = '<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>';">
					<span>удалить</span>
				    </button>
				  </div>
				</div>
				<div class="col-6 col-sm-2 b-list__item">
				  <span class="hidden-sm-up text-gray">Сумма: </span>
				  <span class="cart-items__sum" id="sum_<?=$arItem["ID"]?>"><?=$arItem['SUM']?></span>
				</div>
			      </div>
			    </li>
			    
			    <? } ?>
			    <? } ?>
			</ul>
	    </div>
	    <input type="hidden" id="column_headers" value="<?=CUtil::JSEscape(implode(array("NAME","DELETE","PRICE","QUANTITY","SUM"), ","))?>" />
	    <input type="hidden" id="offers_props" value="<?=CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ","))?>" />
	    <input type="hidden" id="action_var" value="<?=CUtil::JSEscape($arParams["ACTION_VARIABLE"])?>" />
	    <input type="hidden" id="quantity_float" value="<?=$arParams["QUANTITY_FLOAT"]?>" />
	    <input type="hidden" id="count_discount_4_all_quantity" value="<?=($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N"?>" />
	    <input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
	    <input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
	    <input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />
	    <input type="hidden" id="auto_calculation" value="<?=($arParams["AUTO_CALCULATION"] == "N") ? "N" : "Y"?>" />
	</div>
    
	<div class="content-block">
              <div class="row row--vcentr b-list">
                <div class="col-12 col-sm-4 b-list__item" id="coupons_block">
                  
                    <!-- У этой и других форм добавлена валидация (класс js-form-validator). Это демо для верстки! Пример интеграции валидатора с формой можно посмотреть у формы подписки (src/scripts/app/forms/subscribe-form.js)-->
                    <div class="form__title form__title--pull text-left">Промокод
                      <!-- После применения прмоокода выводится текст "Промокод применён, скидка 12 500 р."-->
                    </div>
                    <div class="input-group">
                      <div class="input-group__item--grow">
                        <input class="input-text" type="text" id="coupon" name="COUPON" value="" onchange="enterCoupon();">
                      </div>
                      <div class="input-group__item">
                        <button class="btn btn--fluid btn--inverse" type="button" onclick="enterCoupon();">
                          <span>Применить</span>
                        </button>
                      </div>
                    </div>
                </div>
                <div class="col-12 col-sm-7 offset-sm-1 b-list__item">
                  <form action="/checkout-contacts.html">
                    <div class="row row--vcentr b-list">
                      <div class="col-12 col-sm-6 b-list__item">
                        <div class="sum">
				<?if (floatval($arResult["DISCOUNT_PRICE_ALL"]) > 0):?>
					<div class="sum__row">Товаров на
					  <span class="text-nowrap" id="PRICE_WITHOUT_DISCOUNT"><?=$arResult["PRICE_WITHOUT_DISCOUNT"]?></span>
					</div>
					<div class="sum__row">Скидка
					  <span class="text-nowrap" id="PRICE_DISCOUNT_ALL"><?=CCurrencyLang::CurrencyFormat($arResult["DISCOUNT_PRICE_ALL"], 'RUB', true);?></span>
					</div>
				<?endif;?>
                        </div>
                        <div class="sum sum--big">Итого:
                          <span class="text-nowrap"id="allSum_FORMATED"><?=str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"])?></span>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 text-sm-right b-list__item">
                        <button class="btn btn--xl" type="submit" onclick="checkOut();">
                          <span>Оформить заказ</span>
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
	    
    
	<?/*div class="bx_ordercart_order_pay">

		<div class="bx_ordercart_order_pay_left" id="coupons_block">
		<?
		if ($arParams["HIDE_COUPON"] != "Y")
		{
		?>
			<div class="bx_ordercart_coupon">
				<span><?=GetMessage("STB_COUPON_PROMT")?></span><input type="text" id="coupon" name="COUPON" value="" onchange="enterCoupon();">&nbsp;
	 * <a class="bx_bt_button bx_big" href="javascript:void(0)" onclick="enterCoupon();" title="<?=GetMessage('SALE_COUPON_APPLY_TITLE'); ?>"><?=GetMessage('SALE_COUPON_APPLY'); ?></a>
			</div><?
				if (!empty($arResult['COUPON_LIST']))
				{
					foreach ($arResult['COUPON_LIST'] as $oneCoupon)
					{
						$couponClass = 'disabled';
						switch ($oneCoupon['STATUS'])
						{
							case DiscountCouponsManager::STATUS_NOT_FOUND:
							case DiscountCouponsManager::STATUS_FREEZE:
								$couponClass = 'bad';
								break;
							case DiscountCouponsManager::STATUS_APPLYED:
								$couponClass = 'good';
								break;
						}
						?><div class="bx_ordercart_coupon"><input disabled readonly type="text" name="OLD_COUPON[]" value="<?=htmlspecialcharsbx($oneCoupon['COUPON']);?>" class="<? echo $couponClass; ?>"><span class="<? echo $couponClass; ?>" data-coupon="<? echo htmlspecialcharsbx($oneCoupon['COUPON']); ?>"></span><div class="bx_ordercart_coupon_notes"><?
						if (isset($oneCoupon['CHECK_CODE_TEXT']))
						{
							echo (is_array($oneCoupon['CHECK_CODE_TEXT']) ? implode('<br>', $oneCoupon['CHECK_CODE_TEXT']) : $oneCoupon['CHECK_CODE_TEXT']);
						}
						?></div></div><?
					}
					unset($couponClass, $oneCoupon);
				}
		}
		else
		{
			?>&nbsp;<?
		}
?>
		</div>
		<div class="bx_ordercart_order_pay_right">
			<table class="bx_ordercart_order_sum">
				<?if ($bWeightColumn && floatval($arResult['allWeight']) > 0):?>
					<tr>
						<td class="custom_t1"><?=GetMessage("SALE_TOTAL_WEIGHT")?></td>
						<td class="custom_t2" id="allWeight_FORMATED"><?=$arResult["allWeight_FORMATED"]?>
						</td>
					</tr>
				<?endif;?>
					<tr>
						<td class="fwb"><?=GetMessage("SALE_TOTAL")?></td>
						<td class="fwb" id="allSum_FORMATED"><?=str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"])?></td>
					</tr>


			</table>
			<div style="clear:both;"></div>
		</div>
		<div style="clear:both;"></div>
		<div class="bx_ordercart_order_pay_center">

			<?if ($arParams["USE_PREPAYMENT"] == "Y" && strlen($arResult["PREPAY_BUTTON"]) > 0):?>
				<?=$arResult["PREPAY_BUTTON"]?>
				<span><?=GetMessage("SALE_OR")?></span>
			<?endif;?>
			<?
			if ($arParams["AUTO_CALCULATION"] != "Y")
			{
				?>
				<a href="javascript:void(0)" onclick="updateBasket();" class="checkout refresh"><?=GetMessage("SALE_REFRESH")?></a>
				<?
			}
			?>
			<a href="javascript:void(0)" onclick="checkOut();" class="checkout"><?=GetMessage("SALE_ORDER")?></a>
		</div>
	</div*/?>
</div>
<?
else:
?>
<div id="basket_items_list">
	<table>
		<tbody>
			<tr>
				<td style="text-align:center">
					<div class=""><?=GetMessage("SALE_NO_ITEMS");?></div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<?
endif;
?>