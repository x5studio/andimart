<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$arSelect = Array("ID", "IBLOCK_ID", "NAME","XML_ID");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
$arFilter = Array("IBLOCK_ID"=>BRANDS_IBLOCK_ID);
$brands = array();
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->GetNextElement()){ 
 $arFields = $ob->GetFields();  
$brands[$arFields["XML_ID"]] = $arFields["NAME"];
}

$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/colors.css',
	'TEMPLATE_CLASS' => 'bx-'.$arParams['TEMPLATE_THEME']
);

if (isset($templateData['TEMPLATE_THEME']))
{
	$this->addExternalCss($templateData['TEMPLATE_THEME']);
}
//$this->addExternalCss("/bitrix/css/main/bootstrap.css");
//$this->addExternalCss("/bitrix/css/main/font-awesome.css");
?>

		<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter filter" novalidate="novalidate" id="filter">
			<?foreach($arResult["HIDDEN"] as $arItem):?>
			<input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
			<?endforeach;?>
			<a id="filter-bubble" class="filter__bubble" href="javascript:void(0)" onclick="$('#set_filter').click()" style="display:none;">Выбрано
                    <div class="filter__bubble-count">24</div>
                    <div class="filter__bubble-call" >показать</div>
                  </a>
                  <div class="filter__heading">Фильтр
                    <button class="filter__close btn-close js-toggle-filters-sm" type="button"></button>
                  </div>
				  <div class="filter__filters">
					<div class="filter__b">
                      <div class="filter__title filter__title--big title-line js-title-line">Автоматика</div>
                      <div class="filter__content scroll-block js-scroll-block">
                        <div class="filter__item filter__item--narrow">
                          <a class="filter-link" href="#">Контроллеры</a>
                        </div>
                        <div class="filter__item filter__item--narrow">
                          <a class="filter-link" href="#">Пеллетные установки</a>
                        </div>
                        <div class="filter__item filter__item--narrow">
                          <a class="filter-link" href="#">Погодозависимые системы</a>
                        </div>
                        <div class="filter__item filter__item--narrow">
                          <a class="filter-link" href="#">Сервоприводы</a>
                        </div>
                        <div class="filter__item filter__item--narrow">
                          <a class="filter-link" href="#">Термоголовки</a>
                        </div>
                        <div class="filter__item filter__item--narrow">
                          <span class="filter-link is-current">Термостаты</span>
                        </div>
                        <div class="filter__item filter__item--narrow">
                          <a class="filter-link" href="#">Турбо-насадки</a>
                        </div>
                      </div>
                    </div>
					
				  </div>
			<?foreach($arResult["ITEMS"] as $key=>$arItem)//prices
				{
					$key = $arItem["ENCODED_ID"];
					if(isset($arItem["PRICE"])){
						if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
							continue;

						$precision = 2;
						if (Bitrix\Main\Loader::includeModule("currency"))
						{
							$res = CCurrencyLang::GetFormatDescription($arItem["VALUES"]["MIN"]["CURRENCY"]);
							$precision = $res['DECIMALS'];
						}
						//p($arItem);
						$min = $arItem["VALUES"]["MIN"]["VALUE"];
						$max = $arItem["VALUES"]["MAX"]["VALUE"];
					
						?>
						  <div class="filter__b">
                      <div class="filter__title"><?=$arItem["NAME"]?></div>
                      <div class="filter__content filter__content--fluid">
                        <div class="range-input js-range" data-range-min="<?=$min?>" data-range-max="<?=$max?>" data-range-step="1">
                          <!-- Пример настроек для целых чисел в слайдере: data-range-min="1" data-range-max="20" data-range-step="1"-->
                          <!-- Неформатированные значения - в hidden полях-->
                          <div class="range-input__fields row">
                            <div class="col-6">
                              <div class="input-group">
                                <div class="input-group__item input-group__item--grow">
                                  <input 
									class="input-text input-text--sm js-range-min"
									type="text"
									onchange="smartFilter.keyup(this)"
									value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
								  >
                                  <input class="js-range-min-val" type="hidden"
								  name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
									id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
									value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
									onchange="smartFilter.keyup(this)"
								  >
                                </div>
                                <div class="input-group__item range-input__label">р.</div>
                              </div>
                            </div>
                            <div class="col-6">
                              <div class="input-group">
                                <div class="input-group__item input-group__item--grow">
                                  <input
								  class="input-text input-text--sm js-range-max" 
								  type="text" 
								  onchange="smartFilter.keyup(this)"
								value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
								  >
                                  <input class="js-range-max-val" type="hidden"
								  
								    name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
											id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
											value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
											onchange="smartFilter.keyup(this)"
								  >
                                </div>
                                <div class="input-group__item range-input__label">р.</div>
                              </div>
                            </div>
                          </div>
                          <div class="range-input__range">
                            <div class="js-range-scale"></div>
                          </div>
                        </div>
                      </div>
                    </div>
						<?
					}
				}
				//not prices
			//echo $arParams["DISPLAY_ELEMENT_COUNT"]."!!!";
			foreach($arResult["ITEMS"] as $key=>$arItem)
			{
				
				if(
					empty($arItem["VALUES"])
					|| isset($arItem["PRICE"])
				)
					continue;

				if (
					$arItem["DISPLAY_TYPE"] == "A"
					&& (
						$arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
					)
				)
					continue;
				
					
					?>
					 <div class="filter__b">
                      <div class="filter__title"><?=$arItem["NAME"]?></div>
                      <div class="filter__content scroll-block js-scroll-block">
                        <?foreach($arItem["VALUES"] as $val => $ar):
						//p($ar);
						if($arItem["CODE"]=="BREND"){
								$ar["VALUE"] = $brands[$ar["VALUE"]];
						}
						?>
                        <div class="filter__item">
                          <label class="checkbox">
                            <input 
							class="checkbox__input" 
							type="checkbox"
							value="<? echo $ar["HTML_VALUE"] ?>"
												name="<? echo $ar["CONTROL_NAME"] ?>"
												id="<? echo $ar["CONTROL_ID"] ?>"
												<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
												onchange="smartFilter.click(this)"
							>
                            <span class="checkbox__label"><?=$ar["VALUE"];?> (<?=$ar["ELEMENT_COUNT"]?>)</span>
                          </label>
                        </div>
						<?endforeach;?>
                      </div>
                    </div>
					
					<?
			}
				?>
				<div class="filter__submit">
                    <div class="grid grid--xs-2">
                      <div class="grid__item">
                        <button class="btn btn--sm btn--fluid btn--inverse" type="submit"  id="set_filter" name="set_filter" value="<?=GetMessage("CT_BCSF_SET_FILTER")?>">
                          <span>Показать</span>
                        </button>
                      </div>
                      <div class="grid__item">
                        <button class="btn btn--sm btn--fluid" type="reset" type="submit" id="del_filter" name="del_filter" value="<?=GetMessage("CT_BCSF_DEL_FILTER")?>">
                          <span>Сбросить</span>
                        </button>
                      </div>
                    </div>
                  </div>
				<!--<div class="clb"></div>
			<div class="bx_filter_button_box active">
				<div class="bx_filter_block">
					<div class="bx_filter_parameters_box_container">
						<input class="bx_filter_search_button" type="submit" id="set_filter" name="set_filter" value="<?=GetMessage("CT_BCSF_SET_FILTER")?>" />
						<input class="bx_filter_search_reset" type="submit" id="del_filter" name="del_filter" value="<?=GetMessage("CT_BCSF_DEL_FILTER")?>" />

						<div class="bx_filter_popup_result <?=$arParams["POPUP_POSITION"]?>" id="modef" <?if(!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"';?> style="display: inline-block;">
							<?echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>'));?>
							<span class="arrow"></span>
							<a href="<?echo $arResult["FILTER_URL"]?>"><?echo GetMessage("CT_BCSF_FILTER_SHOW")?></a>
						</div>
					</div>
				</div>
			</div>-->
			
		</form>
	
<script>
	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', 'vertical');
</script>