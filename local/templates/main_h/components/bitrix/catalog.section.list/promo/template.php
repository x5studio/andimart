<?//deb($arResult)?>
<? if (!empty($arResult['SECTIONS'])) { ?>
	<div class="island js-carousel js-carousel--categories">
        <h3 class="section-title title-line js-title-line">Самые продаваемые категории</h3>
        <div class="featured-cats row js-carousel__items">
		<? foreach ($arResult['SECTIONS'] as $karSection => $arSection) { ?>
			<div class="featured-cats__item col-12 col-sm-3 col-md-2">
			    <div class="featured-cats__item-in">
				<a href="<?=$arSection['SECTION_PAGE_URL']?>">
				    <? if (!empty($arSection["PICTURE"])) { 
					$img = CFile::ResizeImageGet($arSection["PICTURE"]['ID'], Array("width" => 115,"height" => 104), BX_RESIZE_IMAGE_PROPORTIONAL, false, false, false, 80);
					?>
					<img width="116" 
					     src="<?=$img["src"]?>" 
					     height="104" class="featured-cats__img" 
					     alt="<?=$arSection["PICTURE"]["ALT"]?>">
				    <? } else { ?>
					<img width="116" 
					     src="<?=SITE_TEMPLATE_PATH."/static/images/nopic.jpg";?>" 
					     height="104" class="featured-cats__img" >
				    <? } ?>
				</a>
				<div class="featured-cats__title">
				    <a class="link-hidden" href="<?=$arSection['SECTION_PAGE_URL']?>"><?=$arSection['NAME']?></a>
				</div>
				<?/*div class="featured-cats__drop">
				    <div class="featured-cats__drop-item">
					<a class="featured-cats__drop-link" href="catalog-2-lvl.html">Yoneywell Термостаты</a>
				    </div>
				    <div class="featured-cats__drop-item">
					<a class="featured-cats__drop-link" href="catalog-2-lvl.html">Robertshow Термостаты</a>
				    </div>
				    <div class="featured-cats__drop-item">
					<a class="featured-cats__drop-link" href="catalog-2-lvl.html">Roggers термостаты</a>
				    </div>
				    <div class="featured-cats__drop-item">
					<a class="featured-cats__drop-link text-bold" href="catalog-2-lvl.html">Просмотреть все категории</a>
				    </div>
				</div*/?>
			    </div>
			</div>
		<? } //deb($arSection, false)?>
        </div>
        <div class="carousel-nav hidden-sm-up">
            <button class="carousel-nav__prev js-carousel__prev" type="button"></button>
            <div class="carousel-nav__stat js-carousel__stat"></div>
            <button class="carousel-nav__next js-carousel__next" type="button"></button>
        </div>
    </div>

<?/*div class="hello__blocks">
            <div class="hello__block">
                <div class="hello__block-in hello__block-in--cats">
                    <div class="hello__title title-line title-line--white js-title-line">Популярные
                        <br>категории</div>
                    <div class="hello__img">
                        <img width="36" src="/local/templates/main/static/images/stuff/flag.svg?v=0c219981" height="42" alt="">
                    </div>
                    <div class="row">
                        <div class="col-6">
			    <? foreach ($arResult['SECTIONS'] as $karSection => $arSection) { ?>
				<? if (!empty($karSection) && $karSection%6==5) { ?>
					</div>
					<div class="col-6">
				<? } ?>
				<div class="hello__item">
				    <a class="hello__link" href="<?=$arSection['SECTION_PAGE_URL']?>"><?=$arSection['NAME']?></a>
				</div>
			    <? } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hello__block">
                <div class="hello__block-in hello__block-in--help">
                    <div class="hello__title title-line title-line--white js-title-line">В помощь
                        <br>покупателю</div>
                    <div class="hello__img">
                        <img width="14" src="/local/templates/main/static/images/stuff/note.svg?v=5fee942d" height="46" alt="">
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="hello__item">
                                <a class="hello__link" href="https://www.youtube.com/channel/UCDEuDAlgbLxnaDIFW9E2v5Q">Видео</a>
                            </div>
                            <div class="hello__item">
                                <a class="hello__link" href="/calculators/">Калькуляторы</a>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="hello__item">
                                <a class="hello__link" href="/dlya_pokupatelya/articles/">Статьи</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div*/?>
<? } ?>