<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");

if ($arResult["NavPageCount"] > 1) {
?>
<nav class="pagination pagenavigation_ajax">
	<ul class="pagination__list">
<?
$start = 1;
//p($arResult);
if($arResult["NavPageNomer"] > 4){
	$start = $arResult["NavPageNomer"] - 1;
}
$end = $arResult["NavPageNomer"] + 1;
if($start ==1 )
	$end =5;
if($end-$start<5)
	$start = $end-5;
if($start<=0)
	$start=1;
if($end > $arResult["NavPageCount"])
	$end = $arResult["NavPageCount"];

//echo $start." " .$end;
?>

	<?
	if($start>1){
		?>
		<li class="pagination__item">
			<a class="pagination__link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=(1)?>">
				<span class="pagination__title">1</span>
			</a>
		</li>
		<li class="pagination__item pagination__item--dots">
			<span class="pagination__title">...</span>
		</li>
		<!--<li><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($i-1)?>">назад <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
-->
		<?
	}
	for($i=$start;$i<=$end;$i++){
		if($i == $arResult["NavPageNomer"]){
			?>
			<li class="pagination__item pagination__item--current">
				<span class="pagination__title"><?=$i ?></span>
			</li>
			<?
		}else{
			?>

			<li class="pagination__item">
				<a class="pagination__link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($i)?>">
					<span class="pagination__title"><?=$i?></span>
				</a>
			</li>
			<?
		}
	}
	if($arResult["NavPageCount"] > $end){
		?>
		<li class="pagination__item pagination__item--dots">
			<span class="pagination__title">...</span>
		</li>
		<li class="pagination__item">
			<a class="pagination__link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageCount"])?>">
				<span class="pagination__title"><?=$arResult["NavPageCount"]?></span>
			</a>
		</li>
		<!--<li><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($i+1)?>">далее <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
-->
		<?
	}
	?>

</ul>
</nav>
<? } ?>
<?
global $cnt;
$cnt++;
if($cnt==1){
$this->SetViewTarget('pagination_items_count2');?>
<span class="count__val"><?=$arResult["NavRecordCount"]?></span>
<span class="count__label"><?=number_end($arResult["NavRecordCount"],array("товар","товара" ,"товаров") )?></span>
<?$this->EndViewTarget();
}
?>