<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<div class="content-block">
	<?php

	if (!empty($arResult["ERRORS"])):?>
		<?ShowError(implode("<br />", $arResult["ERRORS"]))?>
		<script>
			$(document).ready(function(){
				var destination = $("#review-form").offset().top-130;
				jQuery("html:not(:animated),body:not(:animated)").animate({
					scrollTop: destination
				}, 800);
			});
			</script>
	<?endif;
	if (strlen($arResult["MESSAGE"]) > 0):?>
		<?ShowNote($arResult["MESSAGE"])?>
		<script>
			/*$(document).ready(function(){
				Popups.showMessage({
					title: "Отзыв успешно отправлен.",
					text: "Спасибо. Ваш отзыв отправлен на модерацию."
				});
			});*/

		</script>
	<?endif?>
<!-- js-form-validator -->
	<form class="form " novalidate id="review-form" name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
		<?=bitrix_sessid_post()?>
		<?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>

		<!-- У этой и других форм добавлена валидация (класс js-form-validator). Это демо для верстки! Пример интеграции валидатора с формой можно посмотреть у формы подписки (src/scripts/app/forms/subscribe-form.js)-->
		<div class="form__title">Оставить отзыв</div>
		<div class="row row--vcentr form-row">
			<div class="col-12 col-sm-2 col-md-3 text-sm-right">
				<label class="input-label" for="review_name">Имя:</label>
			</div>
			<div class="col-12 col-sm-8 col-md-6">
				<input id="review_name" class="input-text" type="text" required name="PROPERTY[NAME][0]">
			</div>
		</div>
		<div class="row row--vcentr form-row">
			<div class="col-12 col-sm-2 col-md-3 text-sm-right">
				<label class="input-label" for="review_city">Город:</label>
			</div>
			<div class="col-12 col-sm-8 col-md-6">
				<input id="review_city" class="input-text" type="text" name="PROPERTY[3][0]" required>
			</div>
		</div>
		<div class="row row--vcentr form-row">
			<div class="col-12 col-sm-2 col-md-3 text-sm-right">
				<label class="input-label" for="review_email">Email:</label>
			</div>
			<div class="col-12 col-sm-8 col-md-6">
				<input id="review_email" class="input-text" type="email" name="PROPERTY[4][0]" required>
			</div>
		</div>
		<div class="row row--vcentr form-row">
                  <div class="col-12 col-sm-2 col-md-3 text-sm-right">
                    <label class="input-label" for="review_order">№ заказа:</label>
                  </div>
                  <div class="col-12 col-sm-8 col-md-6">
                    <input id="review_order" class="input-text" name="PROPERTY[22][0]" type="text" required="">
                  </div>
                </div>
		<div class="row form-row">
			<div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3">
				<div class="row row--vcentr">
					<div class="col-12 col-sm-8">
						<div class="input-label">Оцените работу, указав количество звёзд от&nbsp;1&nbsp;до&nbsp;5:</div>
					</div>
					<div class="col-12 col-sm-4">
						<div class="stars stars--form">
							<div class="stars__list">
								<input id="rating_5" name="PROPERTY[2]" class="stars__radio"  value="5" type="radio">
								<label class="stars__star" for="rating_5"></label>
								<input id="rating_4" name="PROPERTY[2]" class="stars__radio"  value="4" type="radio">
								<label class="stars__star" for="rating_4"></label>
								<input id="rating_3" name="PROPERTY[2]" class="stars__radio"  value="3" type="radio" checked="checked">
								<label class="stars__star" for="rating_3"></label>
								<input id="rating_2" name="PROPERTY[2]" class="stars__radio"  value="2" type="radio">
								<label class="stars__star" for="rating_2"></label>
								<input id="rating_1" name="PROPERTY[2]" class="stars__radio"  value="1" type="radio">
								<label class="stars__star" for="rating_1"></label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row form-row">
			<div class="col-12 col-sm-2 col-md-3 text-sm-right">
				<label class="input-label" for="review_question">Отзыв:</label>
			</div>
			<div class="col-12 col-sm-8 col-md-6">
				<textarea id="review_question" class="input-text input-text--textarea" required name="PROPERTY[PREVIEW_TEXT][0]" ></textarea>
			</div>
		</div>
        <?if($arParams["USE_CAPTCHA"] == "Y"):?>
            <div class="row form-row">
                <div class="col-12 col-sm-2 col-md-3 text-sm-right">
                    <label class="input-label" for="review_email">Защита от автоматического заполнения</label>
                </div>
                <div class="col-12 col-sm-8 col-md-6">
                    <input type="hidden" name="captcha_sid" class="input-text"
                           value="<?= $arResult["CAPTCHA_CODE"] ?>"/>
                    <input type="text" name="captcha_word" class="input-text" value="">
                    <img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>" width="180"
                         height="40" alt="CAPTCHA"/>
                </div>
            </div>
        <?endif?>
		<div class="form-row text-center">
			<button class="btn"  type="submit" name="iblock_submit2" value="send">
				<span>Отправить</span>
			</button>
		</div>
		<input type="hidden" value="send" name="iblock_submit">
	</form>
</div>
