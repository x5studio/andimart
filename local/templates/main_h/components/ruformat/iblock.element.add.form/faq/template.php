<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<div class="content-block">
	<?php

	if (!empty($arResult["ERRORS"])):?>
		<?ShowError(implode("<br />", $arResult["ERRORS"]))?>
		<script>
			$(document).ready(function(){
				var destination = $("#ask-form").offset().top-130;
				jQuery("html:not(:animated),body:not(:animated)").animate({
					scrollTop: destination
				}, 800);
			});
		</script>
	<?endif;
	if (strlen($arResult["MESSAGE"]) > 0):?>
		<?ShowNote($arResult["MESSAGE"])?>
		<script>
			$(document).ready(function(){
				Popups.showMessage({
					title: "Вопрос успешно отправлен.",
					text: "Спасибо. Ваш вопрос отправлен на модерацию."
				});
			});

		</script>
	<?endif?>
<!-- js-ask-form-->
	<form class="form " novalidate id="ask-form"  name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
		<?=bitrix_sessid_post()?>
		<?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>

		<!-- Почти готовый скрипт отправки формы src/scripts/app/forms/ask-form.js-->
		<div class="form__title">Задать вопрос</div>
		<div class="form-row form-error text-center js-form-error" style="display:none;"></div>
		<div class="row row--vcentr form-row">
			<div class="col-12 col-sm-2 col-md-3 text-sm-right">
				<label class="input-label" for="ask_name">Имя:</label>
			</div>
			<div class="col-12 col-sm-8 col-md-6">
				<input id="ask_name" class="input-text" data-field-name="Имя" name="PROPERTY[NAME][0]" type="text" required>
			</div>
		</div>
		<div class="row row--vcentr form-row">
			<div class="col-12 col-sm-2 col-md-3 text-sm-right">
				<label class="input-label" for="ask_city">Город:</label>
			</div>
			<div class="col-12 col-sm-8 col-md-6">
				<input id="ask_city" class="input-text" data-field-name="Город" type="text" required name="PROPERTY[5][0]">
			</div>
		</div>
		<div class="row row--vcentr form-row">
			<div class="col-12 col-sm-2 col-md-3 text-sm-right">
				<label class="input-label" for="ask_email" name="PROPERTY[6][0]">Email:</label>
			</div>
			<div class="col-12 col-sm-8 col-md-6">
				<input id="ask_email" class="input-text" data-field-name="Email" type="email" required>
			</div>
		</div>
		<div class="row form-row">
			<div class="col-12 col-sm-2 col-md-3 text-sm-right">
				<label class="input-label" for="ask_question">Вопрос:</label>
			</div>
			<div class="col-12 col-sm-8 col-md-6">
				<textarea id="ask_question" class="input-text input-text--textarea" data-field-name="Вопрос" required name="PROPERTY[PREVIEW_TEXT][0]"></textarea>
			</div>
		</div>
        <?if($arParams["USE_CAPTCHA"] == "Y"):?>
            <div class="row form-row">
                <div class="col-12 col-sm-2 col-md-3 text-sm-right">
                    <label class="input-label" for="review_email">Защита от автоматического заполнения</label>
                </div>
                <div class="col-12 col-sm-8 col-md-6">
                    <input type="hidden" name="captcha_sid" class="input-text"
                           value="<?= $arResult["CAPTCHA_CODE"] ?>"/>
                    <input type="text" name="captcha_word" class="input-text" value="">
                    <img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>" width="180"
                         height="40" alt="CAPTCHA"/>
                </div>
            </div>
        <?endif?>
		<div class="form-row text-center">
			<button class="btn" type="submit" name="iblock_submit2" value="send">
				<span>Отправить</span>
			</button>
		</div>
		<input type="hidden" value="send" name="iblock_submit">
	</form>
</div>
