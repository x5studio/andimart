"use strict";

function initBuyButtonEvents() {
    $('.js-btn-to-basket').on('click', function () {
        const lengthOfBtnIdPrefix = 19;
        var productId = this.id.substr(lengthOfBtnIdPrefix);

        var quantityInStock = $('#text-in-stock-product-id-' + productId).data('quantity-in-stock');

        var desiredQuantity = $('#textbox-desired-quantity-product-id-' + productId).val();

        if($.isNumeric(desiredQuantity)) {
            if(desiredQuantity <= quantityInStock) {
                $(this).hide();
                $(this).next().show();
            }
        } else {
            $(this).hide();
            $(this).next().show();
        }
    })
}

function changeBuyButtonState(products){
    var allButtons = document.getElementsByClassName('js-btn-to-basket');
    var buttonIdsToChange = products;
    const lengthOfIdPrefix = 19;

    for (var i = 0; i < allButtons.length; i++) {
        if (buttonIdsToChange.indexOf(allButtons[i].id.substr(lengthOfIdPrefix)) != -1) {
            $(allButtons[i]).hide();
            $(allButtons[i]).next().show();
        }
    }
}

$(document).ready(function () {
    initBuyButtonEvents();
    changeBuyButtonState(productsWithWrongBuyButtonState);
});
