<?

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

$APPLICATION->AddChainItem('Бренды', '/catalog/brands/');
//$this->addExternalCss("/bitrix/css/main/bootstrap.css");

$arResult["VARIABLES"]['ELEMENT_CODE'] = str_replace('brands/', '', $arResult["VARIABLES"]['SECTION_CODE_PATH']);

//SMART_FILTER_PATH
if (strpos($arResult["VARIABLES"]['ELEMENT_CODE'], '/filter/'))
{
	$arResult["VARIABLES"]['ELEMENT_CODE'] = explode('/filter/', $arResult["VARIABLES"]['ELEMENT_CODE']);
	$arResult["VARIABLES"]['SMART_FILTER_PATH'] = str_replace('/apply', '', $arResult["VARIABLES"]['ELEMENT_CODE'][1]);
	$arResult["VARIABLES"]['ELEMENT_CODE'] = $arResult["VARIABLES"]['ELEMENT_CODE'][0];
}
$arResult["VARIABLES"]['SECTION_CODE_PATH'] = 'brands';
?>


<?
// p($subsction_list);
//include($_SERVER["DOCUMENT_ROOT"] . "/" . $this->GetFolder() . "/section_vertical.php");
?>

<? $this->SetViewTarget('brands_h1_additional'); ?>
<?
		$ElementID = $APPLICATION->IncludeComponent(
			"bitrix:catalog.element", "brand", array(
		    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		    "IBLOCK_ID" => const_IBLOCK_ID_brands,
		    "PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
		    "META_KEYWORDS" => $arParams["DETAIL_META_KEYWORDS"],
		    "META_DESCRIPTION" => $arParams["DETAIL_META_DESCRIPTION"],
		    "BROWSER_TITLE" => $arParams["DETAIL_BROWSER_TITLE"],
		    "SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"],
		    "BASKET_URL" => $arParams["BASKET_URL"],
		    "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
		    "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
		    "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
		    "CHECK_SECTION_ID_VARIABLE" => (isset($arParams["DETAIL_CHECK_SECTION_ID_VARIABLE"]) ? $arParams["DETAIL_CHECK_SECTION_ID_VARIABLE"] : ''),
		    "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
		    "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
		    "CACHE_TYPE" => 'N',
		    "CACHE_TIME" => $arParams["CACHE_TIME"],
		    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		    "SET_TITLE" => $arParams["SET_TITLE"],
		    "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
		    "MESSAGE_404" => $arParams["MESSAGE_404"],
		    "SET_STATUS_404" => $arParams["SET_STATUS_404"],
		    "SHOW_404" => $arParams["SHOW_404"],
		    "FILE_404" => $arParams["FILE_404"],
		    "PRICE_CODE" => $arParams["PRICE_CODE"],
		    "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
		    "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
		    "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
		    "PRICE_VAT_SHOW_VALUE" => $arParams["PRICE_VAT_SHOW_VALUE"],
		    "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
		    "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
		    "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
		    "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
		    "LINK_IBLOCK_TYPE" => $arParams["LINK_IBLOCK_TYPE"],
		    "LINK_IBLOCK_ID" => $arParams["LINK_IBLOCK_ID"],
		    "LINK_PROPERTY_SID" => $arParams["LINK_PROPERTY_SID"],
		    "LINK_ELEMENTS_URL" => $arParams["LINK_ELEMENTS_URL"],
		    "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
		    "OFFERS_FIELD_CODE" => $arParams["DETAIL_OFFERS_FIELD_CODE"],
		    "OFFERS_PROPERTY_CODE" => $arParams["DETAIL_OFFERS_PROPERTY_CODE"],
		    "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
		    "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
		    "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
		    "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
		    "ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
		    "ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
		    "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
		    "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
		    "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
		    "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
		    'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
		    'CURRENCY_ID' => $arParams['CURRENCY_ID'],
		    'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
		    'USE_ELEMENT_COUNTER' => $arParams['USE_ELEMENT_COUNTER'],
		    'SHOW_DEACTIVATED' => $arParams['SHOW_DEACTIVATED'],
		    "USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
		    'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
		    'LABEL_PROP' => $arParams['LABEL_PROP'],
		    'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
		    'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
		    'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
		    'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
		    'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
		    'SHOW_MAX_QUANTITY' => $arParams['DETAIL_SHOW_MAX_QUANTITY'],
		    'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
		    'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
		    'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
		    'MESS_BTN_COMPARE' => $arParams['MESS_BTN_COMPARE'],
		    'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],
		    'USE_VOTE_RATING' => $arParams['DETAIL_USE_VOTE_RATING'],
		    'VOTE_DISPLAY_AS_RATING' => (isset($arParams['DETAIL_VOTE_DISPLAY_AS_RATING']) ? $arParams['DETAIL_VOTE_DISPLAY_AS_RATING'] : ''),
		    'USE_COMMENTS' => $arParams['DETAIL_USE_COMMENTS'],
		    'BLOG_USE' => (isset($arParams['DETAIL_BLOG_USE']) ? $arParams['DETAIL_BLOG_USE'] : ''),
		    'BLOG_URL' => (isset($arParams['DETAIL_BLOG_URL']) ? $arParams['DETAIL_BLOG_URL'] : ''),
		    'BLOG_EMAIL_NOTIFY' => (isset($arParams['DETAIL_BLOG_EMAIL_NOTIFY']) ? $arParams['DETAIL_BLOG_EMAIL_NOTIFY'] : ''),
		    'VK_USE' => (isset($arParams['DETAIL_VK_USE']) ? $arParams['DETAIL_VK_USE'] : ''),
		    'VK_API_ID' => (isset($arParams['DETAIL_VK_API_ID']) ? $arParams['DETAIL_VK_API_ID'] : 'API_ID'),
		    'FB_USE' => (isset($arParams['DETAIL_FB_USE']) ? $arParams['DETAIL_FB_USE'] : ''),
		    'FB_APP_ID' => (isset($arParams['DETAIL_FB_APP_ID']) ? $arParams['DETAIL_FB_APP_ID'] : ''),
		    'BRAND_USE' => (isset($arParams['DETAIL_BRAND_USE']) ? $arParams['DETAIL_BRAND_USE'] : 'N'),
		    'BRAND_PROP_CODE' => (isset($arParams['DETAIL_BRAND_PROP_CODE']) ? $arParams['DETAIL_BRAND_PROP_CODE'] : ''),
		    'DISPLAY_NAME' => (isset($arParams['DETAIL_DISPLAY_NAME']) ? $arParams['DETAIL_DISPLAY_NAME'] : ''),
		    'ADD_DETAIL_TO_SLIDER' => (isset($arParams['DETAIL_ADD_DETAIL_TO_SLIDER']) ? $arParams['DETAIL_ADD_DETAIL_TO_SLIDER'] : ''),
		    'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
		    "ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
		    "ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
		    "DISPLAY_PREVIEW_TEXT_MODE" => (isset($arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE']) ? $arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE'] : ''),
		    "DETAIL_PICTURE_MODE" => (isset($arParams['DETAIL_DETAIL_PICTURE_MODE']) ? $arParams['DETAIL_DETAIL_PICTURE_MODE'] : ''),
		    'ADD_TO_BASKET_ACTION' => $basketAction,
		    'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
		    'DISPLAY_COMPARE' => (isset($arParams['USE_COMPARE']) ? $arParams['USE_COMPARE'] : ''),
		    'COMPARE_PATH' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['compare'],
		    'SHOW_BASIS_PRICE' => (isset($arParams['DETAIL_SHOW_BASIS_PRICE']) ? $arParams['DETAIL_SHOW_BASIS_PRICE'] : 'Y'),
		    'BACKGROUND_IMAGE' => (isset($arParams['DETAIL_BACKGROUND_IMAGE']) ? $arParams['DETAIL_BACKGROUND_IMAGE'] : ''),
		    'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
		    'SET_VIEWED_IN_COMPONENT' => (isset($arParams['DETAIL_SET_VIEWED_IN_COMPONENT']) ? $arParams['DETAIL_SET_VIEWED_IN_COMPONENT'] : ''),
		    "USE_GIFTS_DETAIL" => $arParams['USE_GIFTS_DETAIL']? : 'Y',
		    "USE_GIFTS_MAIN_PR_SECTION_LIST" => $arParams['USE_GIFTS_MAIN_PR_SECTION_LIST']? : 'Y',
		    "GIFTS_SHOW_DISCOUNT_PERCENT" => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
		    "GIFTS_SHOW_OLD_PRICE" => $arParams['GIFTS_SHOW_OLD_PRICE'],
		    "GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT'],
		    "GIFTS_DETAIL_HIDE_BLOCK_TITLE" => $arParams['GIFTS_DETAIL_HIDE_BLOCK_TITLE'],
		    "GIFTS_DETAIL_TEXT_LABEL_GIFT" => $arParams['GIFTS_DETAIL_TEXT_LABEL_GIFT'],
		    "GIFTS_DETAIL_BLOCK_TITLE" => $arParams["GIFTS_DETAIL_BLOCK_TITLE"],
		    "GIFTS_SHOW_NAME" => $arParams['GIFTS_SHOW_NAME'],
		    "GIFTS_SHOW_IMAGE" => $arParams['GIFTS_SHOW_IMAGE'],
		    "GIFTS_MESS_BTN_BUY" => $arParams['GIFTS_MESS_BTN_BUY'],
		    "GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT'],
		    "GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE'],
			), $component
		);
		?>
<? $this->EndViewTarget(); ?> 



<?

global $currentBrand;
$arFilter = array(
	"IBLOCK_ID" => $arParams["IBLOCK_ID"],
//	"ACTIVE" => "Y",
//	"GLOBAL_ACTIVE" => "Y",
);

$arFilter["=CODE"] = $currentBrand['CODE'];

$obCache = new CPHPCache();
if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog/brands/".$ElementID.'/'))
{
    $arCurSection = $obCache->GetVars();
}
elseif ($obCache->StartDataCache())
{
    $arCurSection = array();
    if (Loader::includeModule("iblock"))
    {
	    $dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID", 'IBLOCK_SECTION_ID'));

	    if (defined("BX_COMP_MANAGED_CACHE"))
	    {
		    global $CACHE_MANAGER;
		    $CACHE_MANAGER->StartTagCache("/iblock/catalog/brands");

		    if ($arCurSection = $dbRes->Fetch())
			    $CACHE_MANAGER->RegisterTag("iblock_id_" . $arParams["IBLOCK_ID"]);

		    $CACHE_MANAGER->EndTagCache();
	    }
	    else
	    {
		    if (!$arCurSection = $dbRes->Fetch())
			    $arCurSection = array();
	    }
    }

    $arCurSection['CAT_SECTIONS'] = array();
    {
	    //    IBLOCK_SECTION_ID
	    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "CODE", "DETAIL_PAGE_URL", "SECTION_PAGE_URL"); //IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
	    $arFilter = Array("IBLOCK_ID" => const_IBLOCK_ID_brands, 'ACTIVE' => 'Y');
	    $brands = array();
	    $res = CIBlockElement::GetList(Array('name' => 'asc', 'sort' => 'asc'), $arFilter, false, false, $arSelect);
	    while ($arFields = $res->GetNext())
	    {
		    $arFields['SELECTED'] = false;
		    if ($ElementID === $arFields['ID'])
		    {
			    $arFields['SELECTED'] = true;
		    }
		    $arFields['SECTION_PAGE_URL'] = $arFields['DETAIL_PAGE_URL'];
		    $arCurSection['CAT_SECTIONS'][$arFields["ID"]] = $arFields;
	    }
    }
    $obCache->EndDataCache($arCurSection);
}
if (!isset($arCurSection))
    $arCurSection = array();


$arResult["URL_TEMPLATES"]["smart_filter"] = str_replace('#SECTION_CODE_PATH#/', 'brands/'.$currentBrand['CODE'].'/', $arResult["URL_TEMPLATES"]["smart_filter"]);

if (preg_match('/\/brend-is-/', $arResult["VARIABLES"]["SMART_FILTER_PATH"]))
{
	$arResult["VARIABLES"]["SMART_FILTER_PATH"] = preg_replace('/\/brend-[^\/]+\//', '/', $arResult["VARIABLES"]["SMART_FILTER_PATH"]);
}
else
{
//	$arResult["VARIABLES"]["SMART_FILTER_PATH"] .= (!empty($arResult["VARIABLES"]["SMART_FILTER_PATH"])?'/':'').'brend-is-'.$currentBrand['XML_ID'];
}

$GLOBALS[$arParams["FILTER_NAME"]]['PROPERTY_BREND'] = $currentBrand['XML_ID'];

$this->SetViewTarget('smart_filter');?>
	<?/*$APPLICATION->IncludeComponent(
		"andimarket:catalog.smart.filter",
		//"visual_vertical",
		"",
		array(	
			'CAT_SECTIONS' => $arCurSection['CAT_SECTIONS'],
		    
			"NO_BRANDS" => 'Y',
			"TITLE" => "Бренды",    
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"IBLOCK_SECTION_ID" => "",
			"SECTION_ID" => "",
			"FILTER_NAME" => $arParams["FILTER_NAME"],
			"PRICE_CODE" => $arParams["PRICE_CODE"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"SAVE_IN_SESSION" => "N",
			"FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
			"XML_EXPORT" => "Y",
			"SECTION_TITLE" => "NAME",
			"SECTION_DESCRIPTION" => "DESCRIPTION",
			'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
			"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
			'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
			'CURRENCY_ID' => $arParams['CURRENCY_ID'],
			"SEF_MODE" => $arParams["SEF_MODE"],
			"SEF_RULE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["smart_filter"],
			"SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
			"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
			"INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
			"DISPLAY_ELEMENT_COUNT" =>"Y",
		),
		$component,
		array('HIDE_ICONS' => 'N')
	);*/?>
<?$APPLICATION->IncludeComponent(
			"bitrix:catalog.smart.filter",
			//"visual_vertical",
			"",
			array(
			'CAT_SECTIONS' => $arCurSection['CAT_SECTIONS'],
			    
			    
			"NO_BRANDS" => 'Y',
				"TITLE" => "Бренды", 
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"IBLOCK_SECTION_ID" => $arCurSection['IBLOCK_SECTION_ID'],
				"SECTION_ID" => $arCurSection['ID'],
				"FILTER_NAME" => $arParams["FILTER_NAME"],
				"PRICE_CODE" => $arParams["PRICE_CODE"],
				"CACHE_TYPE" => 'N',
				"CACHE_TIME" => $arParams["CACHE_TIME"],
				"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
				"SAVE_IN_SESSION" => "N",
				"FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
				"XML_EXPORT" => "Y",
				"SECTION_TITLE" => "NAME",
				"SECTION_DESCRIPTION" => "DESCRIPTION",
				'HIDE_NOT_AVAILABLE' => 'Y',
				"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
				'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
				'CURRENCY_ID' => $arParams['CURRENCY_ID'],
				"SEF_MODE" => $arParams["SEF_MODE"],
				"SEF_RULE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["smart_filter"],
				"SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
				"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
				"INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
				"DISPLAY_ELEMENT_COUNT" =>"Y",
			),
			$component,
			array('HIDE_ICONS' => 'N')
		);?>
<?$this->EndViewTarget();?>


<div class="catalog-ctrls">
	<div class="catalog-ctrls__sort island island--small">
	  <div class="catalog-ctrls__in">
	    <div class="row row--vcentr">
	      <div class="col-12 col-sm-4 col-md-6">
		<div class="count catalog-ctrls__count" >
					<?$APPLICATION->ShowViewContent('pagination_items_count2');?>

		</div>
	      </div>
	      <div class="col-12 col-sm-8 col-md-6">
		<div class="row row--vcentr">
		  <div class="col-12 col-sm-5 text-sm-right text-small-expand">Сортировать по</div>
		  <div class="col-12 col-sm-7">
						      <form action="<?=$_SERVER["REQUEST_URI"];?>">
							      <?foreach($_GET as $key=>$valeu){?>
							      <input type="hidden" value="<?=$valeu?>" name="<?=$key?>">
							      <?
							      }
							      ?>
							<select class="select select--xs change_sort_select" name="sort">
							      <option <?if($_GET["sort"]=="price") echo "selected"; ?> value="price" selected="">цене</option>
							      <option <?if($_GET["sort"]=="popular") echo "selected"; ?> value="popular">популярности</option>
							      <option <?if($_GET["sort"]=="raiting") echo "selected"; ?> value="raiting">рейтингу</option>
							</select>
						</form>
		  </div>
		</div>
	      </div>
	    </div>
	  </div>
	</div>
	<div class="catalog-ctrls__filters island island--small">
	  <button class="catalog-ctrls__filter-btn js-toggle-filters-sm" type="button">
	    <img class="catalog-ctrls__filter-btn-i" src="<?= SITE_TEMPLATE_PATH ?>/static/images/filters.svg?v=1431db20" width="48" height="32" alt="">
	    <span>фильтр</span>
	  </button>
	</div>
			</div>
                    <!--<div class="catalog grid grid--xs-2 grid--sm-3 grid--md-3">-->
                    <!-- не закрывается тут-->
                    <!--Перенесен в шаблон-->

      <?
$sort = $arParams["ELEMENT_SORT_FIELD"];
$sort_prder = $arParams["ELEMENT_SORT_ORDER"];

$sort_array = array(
      "price"=>array(
	      "SORT"=>"catalog_PRICE_14",
	      "ORDER"=>"DESC",
      ),
      "popular"=>array(
	      "SORT"=>"shows",
	      "ORDER"=>"ASC",
      ),
      "raiting"=>array(
	      "SORT"=>"PROPERTY_RAITING",
	      "ORDER"=>"DESC",
      ),
);
if($_GET["sort"] && !empty(	$sort_array[$_GET["sort"]])){
      $sort =$sort_array[$_GET["sort"]]["SORT"];
      $sort_prder = $sort_array[$_GET["sort"]]["ORDER"];
}

$intSectionID = $APPLICATION->IncludeComponent(
      "bitrix:catalog.section",
      "",
      array(
		"SHOW_ALL_WO_SECTION" => 'Y',
	    "CUSTOM_SECTION_ID" =>$arResult["VARIABLES"]["SECTION_ID"],
	      "CUSTOM_SECTION_CODE_PATH"  =>$arResult["VARIABLES"]["SECTION_CODE_PATH"],
	      "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
	      "IBLOCK_ID" => $arParams["IBLOCK_ID"],
	      "ELEMENT_SORT_FIELD" =>$sort,
	      "ELEMENT_SORT_ORDER" => $sort_prder,
	      "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
	      "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
	      "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
	      "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
	      "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
	      "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
	      "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
	      "INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
	      "BASKET_URL" => $arParams["BASKET_URL"],
	      "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
	      "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
	      "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
	      "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
	      "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
	      "FILTER_NAME" => $arParams["FILTER_NAME"],
	      "CACHE_TYPE" => 'N',
	      "CACHE_TIME" => $arParams["CACHE_TIME"],
	      "CACHE_FILTER" => $arParams["CACHE_FILTER"],
	      "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
	      "SET_TITLE" => $arParams["SET_TITLE"],
	      "MESSAGE_404" => $arParams["MESSAGE_404"],
	      "SET_STATUS_404" => $arParams["SET_STATUS_404"],
	      "SHOW_404" => $arParams["SHOW_404"],
	      "FILE_404" => $arParams["FILE_404"],
	      "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
	      "PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
	      "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
	      "PRICE_CODE" => $arParams["PRICE_CODE"],
	      "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
	      "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

	      "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
	      "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
	      "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
	      "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
	      "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

	      "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
	      "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
	      "PAGER_TITLE" => $arParams["PAGER_TITLE"],
	      "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
	      "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
	      "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
	      "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
	      "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
	      "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
	      "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
	      "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
          "LAZY_LOAD" => $arParams["LAZY_LOAD"],
          "MESS_BTN_LAZY_LOAD" => $arParams["~MESS_BTN_LAZY_LOAD"],
          "LOAD_ON_SCROLL" => $arParams["LOAD_ON_SCROLL"],

	      "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
	      "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
	      "OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
	      "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
	      "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
	      "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
	      "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
	      "OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

	      "SECTION_ID" => '',
	      "SECTION_CODE" => '',
	      "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
	      "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
	      "USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
	      'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
	      'CURRENCY_ID' => $arParams['CURRENCY_ID'],
	      'HIDE_NOT_AVAILABLE' => 'Y',

	      'LABEL_PROP' => $arParams['LABEL_PROP'],
	      'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
	      'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

	      'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
	      'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
	      'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
	      'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
	      'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
	      'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
	      'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
	      'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
	      'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
	      'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

	      'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
	      "ADD_SECTIONS_CHAIN" => "N",
	      'ADD_TO_BASKET_ACTION' => $basketAction,
	      'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
	      'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
	      'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
	      'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : '')
      ),
      $component
);


?>