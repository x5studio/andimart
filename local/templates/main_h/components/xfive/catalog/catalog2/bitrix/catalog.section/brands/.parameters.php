<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/*x5 20180906 begin*/
$arTemplateParameters = array(
	"X5_SECTION_PAGE_URL" => Array(
		"NAME" => "Ссылка на текущий раздел",
		"TYPE" => "STRING",
		"VALUE" => ""
	),
	"X5_SECTION_NAME" => Array(
		"NAME" => "Название текущего раздела",
		"TYPE" => "STRING",
		"VALUE" => ""
	),
);
/*x5 20180906 end*/