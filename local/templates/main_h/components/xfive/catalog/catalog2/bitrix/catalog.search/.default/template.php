<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$is_ajax = false;
if (!empty($_GET['is_ajax']))
{
	$is_ajax = true;
}

define('BRANDS_IBLOCK_ID', 26);

//\Bitrix\Main\Diag\Debug::writeToFile('$_GET[\'q\'] = ');
//\Bitrix\Main\Diag\Debug::writeToFile($_GET['q']);
//
//\Bitrix\Main\Diag\Debug::writeToFile('$_REQUEST[\'q\'] = ');
//\Bitrix\Main\Diag\Debug::writeToFile($_REQUEST['q']);


//$_GET['q'] = str_replace('-', '', $_GET['q']);
//$_REQUEST['q'] = str_replace('-', '', $_REQUEST['q']);
//$_GET['q'] = str_replace('/', '', $_GET['q']);
//$_REQUEST['q'] = str_replace('/', '', $_REQUEST['q']);
//$_GET['q'] = str_replace('\\', '', $_GET['q']);
//$_REQUEST['q'] = str_replace('\\', '', $_REQUEST['q']);
//$_GET['q'] = str_replace('=', '', $_GET['q']);
//$_REQUEST['q'] = str_replace('=', '', $_REQUEST['q']);
//$_GET['q'] = str_replace('.', '', $_GET['q']);
//$_REQUEST['q'] = str_replace('.', '', $_REQUEST['q']);

//\Bitrix\Main\Diag\Debug::writeToFile('$_GET[\'q\'] = ');
//\Bitrix\Main\Diag\Debug::writeToFile($_GET['q']);
//
//\Bitrix\Main\Diag\Debug::writeToFile('$_REQUEST[\'q\'] = ');
//\Bitrix\Main\Diag\Debug::writeToFile($_REQUEST['q']);

?>

<?
global $USER;

if ($USER->IsAdmin()) {
    //die('test');
}
?>

<div class="content-block content-block--pull-top content-block--sm-bot island">
<?/*
    <h1 class="section-title title-line js-title-line"><? $APPLICATION->ShowTitle(false) ?></h1>
*/?>
<?/*
$arElements = $APPLICATION->IncludeComponent(
	"xfive:search.page",
	".default",
	Array(
		'_component' => $arParams['_component'],
	    
		"RESTART" => $arParams["RESTART"],
		"NO_WORD_LOGIC" => $arParams["NO_WORD_LOGIC"],
		"USE_LANGUAGE_GUESS" => $arParams["USE_LANGUAGE_GUESS"],
		"USE_SUGGEST" => $arParams["USE_SUGGEST"],
		"CHECK_DATES" => $arParams["CHECK_DATES"],
		"arrFILTER" => array("iblock_".$arParams["IBLOCK_TYPE"]),
		"arrFILTER_iblock_".$arParams["IBLOCK_TYPE"] => array($arParams["IBLOCK_ID"]),
		"USE_TITLE_RANK" => $arParams["USE_TITLE_RANK"],
		"DEFAULT_SORT" => "rank",
		"FILTER_NAME" => "",
		"SHOW_WHERE" => "N",
		"arrWHERE" => array(),
		"SHOW_WHEN" => "N",
		"PAGE_RESULT_COUNT" => 500,
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "forum",
	),
	$arParams['_component'],
	array('HIDE_ICONS' => 'Y')
);*/

/*$arElements = $APPLICATION->IncludeComponent(
	"xfive:search.page",
	".default",
	Array(
		'_component' => $arParams['_component'],
	    
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"COLOR_NEW" => "0",
		"COLOR_OLD" => "C8C8C8",
		"COLOR_TYPE" => "Y",
		"DEFAULT_SORT" => "rank",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "Y",
		"FILTER_NAME" => "",
		"FONT_MAX" => "50",
		"FONT_MIN" => "10",
		"NO_WORD_LOGIC" => "Y",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_TITLE" => "Результаты поиска",
		"PAGE_RESULT_COUNT" => "500",
		"PATH_TO_USER_PROFILE" => "#SITE_DIR#people/user/#USER_ID#/",
		"PERIOD_NEW_TAGS" => "",
		"RATING_TYPE" => "",
		"RESTART" => "Y",
		"SHOW_CHAIN" => "Y",
		"SHOW_RATING" => "Y",
		"SHOW_WHEN" => "N",
		"SHOW_WHERE" => "Y",
		"TAGS_INHERIT" => "Y",
		"TAGS_PAGE_ELEMENTS" => "20",
		"TAGS_PERIOD" => "",
		"TAGS_SORT" => "NAME",
		"TAGS_URL_SEARCH" => "",
		"USE_LANGUAGE_GUESS" => "N",
		"USE_SUGGEST" => "N",
		"USE_TITLE_RANK" => "Y",
		"WIDTH" => "100%",
		"arrFILTER" => array("iblock_1c_catalog"),
		"arrFILTER_iblock_1c_catalog" => array("20"),
//		"arrFILTER" => array("iblock_".$arParams["IBLOCK_TYPE"]),
//		"arrFILTER_iblock_".$arParams["IBLOCK_TYPE"] => array($arParams["IBLOCK_ID"]),
		"arrWHERE" => array(),
	),
	$arParams['_component'],
	array('HIDE_ICONS' => 'Y')
);*/
?>
    <form class="search-form" novalidate action="/catalog/">

		<div class="h4">Вы искали:</div>
		<br/>
		<div class="input-group">
                    <div class="input-group__item input-group__item--grow">
			<? if(strlen($arResult["REQUEST"]["~QUERY"]) && is_object($arResult["NAV_RESULT"]))
			{
				//$arResult["FILTER_MD5"] = $arResult["NAV_RESULT"]->GetFilterMD5();
				$obSearchSuggest = new CSearchSuggest($arResult["FILTER_MD5"], $arResult["REQUEST"]["~QUERY"]);
				$obSearchSuggest->SetResultCount($arResult["NAV_RESULT"]->NavRecordCount);
			}
			?>
			<?$APPLICATION->IncludeComponent(
				"bitrix:search.suggest.input",
				"",
				array(
					"NAME" => "q",
					"VALUE" => $arResult["REQUEST"]["~QUERY"],
					"INPUT_SIZE" => 40,
					"DROPDOWN_SIZE" => 10,
					"FILTER_MD5" => $arResult["FILTER_MD5"],
				),
				$arParams['_component'], array("HIDE_ICONS" => "Y")
			);?>

                    </div>
                    <div class="input-group__item">
			<button class="search-form__submit" type="submit" value="<?=GetMessage("SEARCH_GO")?>"></button>
                    </div>
		</div>
		<input type="hidden" name="how" value="<?echo $arResult["REQUEST"]["HOW"]=="d"? "d": "r"?>" />
	    </form>
<?


//$srch = $_GET['q'];
//$arFilter_my = array (
//'IBLOCK_ID' => 20,
//array(
//	"LOGIC" => "OR",
//	'NAME' => '%'.$srch.'%',
//	'PROPERTY_CML2_ARTICLE' => '%'.$srch.'%'
//)
//);


//$items = CIBlockElement::GetList(array("ID" => "DESC"),$arFilter_my);
//while($arItem = $items->Fetch()){
//    $arElements[] = $arItem[ID];
//}


//\Bitrix\Main\Diag\Debug::writeToFile('$arElements = ');
//\Bitrix\Main\Diag\Debug::writeToFile($arElements);


$section_names = [];

$srch = $_GET['q'];

$ar_search_words = explode(' ', $srch);

if(count($ar_search_words) > 0) {
    $srch = "";
    $ar_brand_display_names = [];
    $ar_brand_names = [];
    $ar_brand_codes = [];

    $rsBrands = \CIBlockElement::GetList(array(), array('IBLOCK_ID' => BRANDS_IBLOCK_ID));
    while ($brand = $rsBrands->Fetch()) {
        $ar_brand_display_names[] = $brand['NAME'];
        $ar_brand_names[] = strtolower($brand['NAME']);
        $ar_brand_codes[] = $brand['CODE'];
    }

    foreach ($ar_search_words as $search_word) {
        if(($index = array_search(strtolower($search_word), $ar_brand_names)) !== false) {
            $section['NAME'] = $ar_brand_display_names[$index];
            if(!in_array($section['NAME'], $section_names)) {
                $section_names[] = $section['NAME'];
                $section['SECTION_PAGE_URL'] = '/catalog/brands/' . $ar_brand_codes[$index];
                $arSections[] = $section;
            }
        } else {
            $srch .= ' ' . $search_word;
        }
    }
    $srch = trim($srch);
}


/*
$arBrands = $APPLICATION->IncludeComponent(
    "xfive:search.page",
    ".default_2",
    Array(
        "RESTART" => $arParams["RESTART"],
        "NO_WORD_LOGIC" => $arParams["NO_WORD_LOGIC"],
        "USE_LANGUAGE_GUESS" => $arParams["USE_LANGUAGE_GUESS"],
        "CHECK_DATES" => $arParams["CHECK_DATES"],
        "arrFILTER" => array("iblock_".$arParams["IBLOCK_TYPE"]),
        "arrFILTER_iblock_".$arParams["IBLOCK_TYPE"] => array(BRANDS_IBLOCK_ID),
        "USE_TITLE_RANK" => "N",
        "DEFAULT_SORT" => "rank",
        "FILTER_NAME" => "",
        "SHOW_WHERE" => "N",
        "arrWHERE" => array(),
        "SHOW_WHEN" => "N",
        "PAGE_RESULT_COUNT" => $arParams["PAGE_RESULT_COUNT"],
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "N",
    ),
    $arResult["THEME_COMPONENT"], // $component,
    array('HIDE_ICONS' => 'Y')
);

foreach ($arBrands as $brandId) {
    $rsBrand = \CIBlockElement::GetByID($brandId);
    if($arBrand = $rsBrand->Fetch()) {
        $section['NAME'] = $arBrand['NAME'];
        if(!in_array($section['NAME'], $section_names)) {
            $section_names[] = $section['NAME'];
            $section['SECTION_PAGE_URL'] = '/catalog/brands/' . $arBrand['CODE'];
            $arSections[] = $section;
        }
    }
}
*/


//$srch = $_GET['q'];

$rsSections = new \CSearch();

$rsSections->Search(
    array(
        'QUERY' => $srch,
        'SITE_ID' => SITE_ID,
        'MODULE_ID' => 'iblock',
        'PARAM2' => $arParams["IBLOCK_ID"]
    ),
    array(
        'ITEM_ID' => 'DESC'
    )
);

while(($arSection = $rsSections->Fetch()) && ($arSection['ITEM_ID'][0] == 'S')) {
    $section['NAME'] = $arSection['TITLE'];
    if(!in_array($section['NAME'], $section_names)) {
        $section_names[] = $section['NAME'];
        $section['SECTION_PAGE_URL'] = $arSection['URL'];
        $arSections[] = $section;
    }
}



//while($arSection = $rsSections->Fetch()) {
//    if($arSection['ITEM_ID'][0] == 'S') {
//        $arSections[] = $arSection;
//    } else {
//        $arElements[] = $arSection;
//    }
//}


$arElements = $APPLICATION->IncludeComponent(
    "xfive:search.page",
    ".default_2",
    Array(
        "RESTART" => $arParams["RESTART"],
        "NO_WORD_LOGIC" => $arParams["NO_WORD_LOGIC"],
        "USE_LANGUAGE_GUESS" => $arParams["USE_LANGUAGE_GUESS"],
        "CHECK_DATES" => $arParams["CHECK_DATES"],
        "arrFILTER" => array("iblock_".$arParams["IBLOCK_TYPE"]),
        "arrFILTER_iblock_".$arParams["IBLOCK_TYPE"] => array($arParams["IBLOCK_ID"]),
        "USE_TITLE_RANK" => "N",
        "DEFAULT_SORT" => "rank",
        "FILTER_NAME" => "",
        "SHOW_WHERE" => "N",
        "arrWHERE" => array(),
        "SHOW_WHEN" => "N",
        "PAGE_RESULT_COUNT" => $arParams["PAGE_RESULT_COUNT"],
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "N",
    ),
    $arResult["THEME_COMPONENT"], // $component,
    array('HIDE_ICONS' => 'Y')
);


//p('$arElements = ');
//p($arElements);


//$srch = $_GET['q'];
//
//$rsElements = new \CSearch();
//
//$rsElements->Search(
//    array(
//        'QUERY' => $srch,
//        'SITE_ID' => SITE_ID,
//        'MODULE_ID' => 'iblock',
//        "!ITEM_ID" => "S%",
//        'PARAM2' => $arParams["IBLOCK_ID"]
//    )
//);
//
//while($arElement = $rsElements->Fetch()) {
//    $arElements[] = $arElement;
//}


//\Bitrix\Main\Diag\Debug::writeToFile('$bitrix_search_page_result = ');
//\Bitrix\Main\Diag\Debug::writeToFile($bitrix_search_page_result);


//print_r($arElements);
//поиск по разделам "КОЛХОЗИМ"
//$srch = $_GET['q'];
//$rsSections = CIBlockSection::GetList(array(), array("ACTIVE"=>"Y","IBLOCK_ID"=>"20","NAME"=>"%".$srch."%"),false, array("ID","NAME","SECTION_PAGE_URL"));
//while ($arSection = $rsSections->Fetch())
//{
//    $arSections[]=$arSection;
//}


if (!empty($arElements) && is_array($arElements) || !empty($arSections))
{
    ?>
</div>
          <div class="content-block content-block--pull-top">
          
          <?if (!empty($arSections)):?>
          <div class="catalog-ctrls">
              <div class="catalog-ctrls__sort island island--small">
                <div class="catalog-ctrls__in">
                  <div class="row row--vcentr">
                    <div class="col-5 col-sm-7 col-md-8">
                      <div class="count catalog-ctrls__count">
                        <span class="count__val"><?=count($arSections);?></span><span class="count__label">раздел<?=getNumEnding(count($arSections),array('', 'а', 'ов'))?></span>
                        <? foreach ($arSections as $sect):?>
			            	<br><a href="<?=$sect['SECTION_PAGE_URL']?>"><?=$sect['NAME']?></a>
			            <?endforeach;?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?endif;?>
          
            <div class="catalog-ctrls">
              <div class="catalog-ctrls__sort island island--small">
                <div class="catalog-ctrls__in">
                  <div class="row row--vcentr">
                    <div class="col-5 col-sm-7 col-md-8">
                      <div class="count catalog-ctrls__count">
                        <?$APPLICATION->ShowViewContent('pagination_items_count2');?>
                      </div>
                    </div>
		      
		      
                    <div class="col-7 col-sm-5 col-md-4">
                      <div class="row row--vcentr">
                        <div class="col-12 col-sm-5 text-sm-right text-small-expand">Сортировать по</div>
                            <div class="col-12 col-sm-7">
								<form action="<?=$_SERVER["REQUEST_URI"];?>">
									<?foreach($_GET as $key=>$valeu){?>
									<input type="hidden" value="<?=$valeu?>" name="<?=$key?>">
									<?
									}
									?>
								  <select class="select select--xs change_sort_select" name="sort">
									<option <?if($_GET["sort"]=="price") echo "selected"; ?> value="price" selected="">цене</option>
									<option <?if($_GET["sort"]=="popular") echo "selected"; ?> value="popular">популярности</option>
									<option <?if($_GET["sort"]=="raiting") echo "selected"; ?> value="raiting">рейтингу</option>
								  </select>
							  </form>
                            </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="catalog grid grid--xs-2 grid--sm-3 grid--md-4">
		<?
	$sort_array = array(
		"price"=>array(
			"SORT"=>"catalog_PRICE_14",
			"ORDER"=>"DESC",
		),
		"popular"=>array(
			"SORT"=>"shows",
			"ORDER"=>"ASC",
		),
		"raiting"=>array(
			"SORT"=>"PROPERTY_RAITING",
			"ORDER"=>"DESC",
		),
	);
	if($_GET["sort"] && !empty(	$sort_array[$_GET["sort"]])){
		$sort =$sort_array[$_GET["sort"]]["SORT"];
		$sort_prder = $sort_array[$_GET["sort"]]["ORDER"];
	}
	
		global $searchFilter;
		$searchFilter = array(
			"=ID" => $arElements,
		);


//        \Bitrix\Main\Diag\Debug::writeToFile('$searchFilter = ');
//        \Bitrix\Main\Diag\Debug::writeToFile($searchFilter);


        if ($is_ajax)
		{
			$APPLICATION->RestartBuffer();
		}

		$APPLICATION->IncludeComponent(
		"bitrix:catalog.section",
		$is_ajax?"ajax_search":".default",
		array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ELEMENT_SORT_FIELD" =>$sort,
			"ELEMENT_SORT_ORDER" => $sort_prder,
			"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
			"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
			"PAGE_ELEMENT_COUNT" => $is_ajax?10:12,
			"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
			"PROPERTY_CODE" => $arParams["PROPERTY_CODE"],
			"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
			"OFFERS_FIELD_CODE" => $arParams["OFFERS_FIELD_CODE"],
			"OFFERS_PROPERTY_CODE" => $arParams["OFFERS_PROPERTY_CODE"],
			"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
			"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
			"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
			"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
			"OFFERS_LIMIT" => $arParams["OFFERS_LIMIT"],
			"SECTION_URL" => $arParams["SECTION_URL"],
			"DETAIL_URL" => $arParams["DETAIL_URL"],
			"BASKET_URL" => $arParams["BASKET_URL"],
			"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
			"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
			"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
			"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
			"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"DISPLAY_COMPARE" => $arParams["DISPLAY_COMPARE"],
			"PRICE_CODE" => $arParams["PRICE_CODE"],
			"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
			"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
			"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
			"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
			"USE_PRODUCT_QUANTITY" => $arParams["USE_PRODUCT_QUANTITY"],
			"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
			"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
			"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
			"CURRENCY_ID" => $arParams["CURRENCY_ID"],
			"HIDE_NOT_AVAILABLE" => 'Y',
			"DISPLAY_TOP_PAGER" => 'N',
			"DISPLAY_BOTTOM_PAGER" => 'Y',
			"PAGER_TITLE" => $arParams["PAGER_TITLE"],
			"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
			"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
			"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
			"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
			"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
			"FILTER_NAME" => "searchFilter",
			"SECTION_ID" => "",
			"SECTION_CODE" => "",
			"SECTION_USER_FIELDS" => array(),
			"INCLUDE_SUBSECTIONS" => "Y",
			"SHOW_ALL_WO_SECTION" => "Y",
			"META_KEYWORDS" => "",
			"META_DESCRIPTION" => "",
			"BROWSER_TITLE" => "",
			"ADD_SECTIONS_CHAIN" => "N",
			"SET_TITLE" => "N",
			"SET_STATUS_404" => "N",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "N",

			'LABEL_PROP' => $arParams['LABEL_PROP'],
			'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
			'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

			'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
			'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
			'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
			'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
			'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
			'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
			'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
			'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
			'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
			'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

			'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
			'ADD_TO_BASKET_ACTION' => (isset($arParams['ADD_TO_BASKET_ACTION']) ? $arParams['ADD_TO_BASKET_ACTION'] : ''),
			'SHOW_CLOSE_POPUP' => (isset($arParams['SHOW_CLOSE_POPUP']) ? $arParams['SHOW_CLOSE_POPUP'] : ''),
			'COMPARE_PATH' => $arParams['COMPARE_PATH'],
			'SECTIONS' => $arSections
		),
		$arResult["THEME_COMPONENT"],
		array('HIDE_ICONS' => 'Y')
	);
		if ($is_ajax)
		{
			exit;
		}
		?>
          </div>
		<?//$APPLICATION->ShowViewContent('catalog_pagination');?>
          </div>
		<?
}
elseif (is_array($arElements))
{
    ?>
            <div class="content-block content-block--push-top wysiwyg">
              <div class="h4">По вашему запросу ничего не найдено</div>
              <p>Попробуйте снова или вернитесь на
                <a href="/">главную</a> страницу</p>
            </div>
          </div>
		<?
} ?>
