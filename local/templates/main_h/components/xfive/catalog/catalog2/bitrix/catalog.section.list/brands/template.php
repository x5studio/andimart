<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumbs", Array(
    "PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
    "SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
    "START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
),
    false
);?>
<div class="content-block content-block--pull-top <? if(SHOW_LEFT_MENU!="Y"){ ?>island <? } ?>">
    
	<h1 class="section-title title-line <? if(CATALOG_CATEGORY=="Y"){ ?>title-line--accent<?}?> js-title-line"><? $APPLICATION->ShowTitle(false) ?></h1>
    
	<div class="content-block js-tabs" data-active="<?=$arResult['CHOOSE']?>">
		<div class="island-tabs js-island-tabs brands_tabs">
			<button class="island-tabs__scroll-l js-island-tabs__l" type="button"></button>
			<ul class="island-tabs__list js-island-tabs__scroll">
				<li class="island-tabs__item">
					<a class="island-tabs__link js-tabs__btn" href="#brands-group-0">
						<span class="island-tabs__title">Все бренды</span>
					</a>
				</li>
				<? foreach ($arResult['SECTIONS'] as $karSection => $arSection) { ?>
					<li class="island-tabs__item">
						<a class="island-tabs__link js-tabs__btn" href="#brands-group-<?=$karSection+1?>">
							<span class="island-tabs__title"><?=toLower($arSection['NAME'])?></span>
						</a>
					</li>
				<? } ?>
			</ul>
			<button class="island-tabs__scroll-r js-island-tabs__r" type="button"></button>
		</div>

		<? if (!empty($arResult['SECTION_CHOOSE'])) { ?>
		<div id="brands-group-0" class="tab-content content-block js-tabs__content" data-url="/catalog/brands/">
		    <div class="ajax-preloader js-tab-ajax-content" 
				     data-src="/catalog/brands/?AJAX_MODE=Y"><i class="ajax-preloader__spinner"></i></div>
		</div>
		<? } ?>
		<?
        $chosenSection = [];
        foreach ($arResult['SECTIONS'] as $karSection => $arSection) { ?>
			<? if ($arResult['SECTION_CHOOSE'] != $arSection['ID']) { ?>
				<div id="brands-group-<?=$karSection+1?>" class="tab-content content-block js-tabs__content" data-url="<?=$arSection['SECTION_PAGE_URL']?>" data-x5-breadcrumb-name="<?=$arSection['NAME']?>">
					<div class="ajax-preloader js-tab-ajax-content" 
					     data-src="<?=$arSection['SECTION_PAGE_URL']?>?AJAX_MODE=Y"><i class="ajax-preloader__spinner"></i></div>
				</div>
			<? } else {
				$chosenSection = $arSection;
            } ?>
		<? } ?>

		<? $APPLICATION->IncludeComponent(
			"bitrix:catalog.section",
			"brands",
			array(

			    "X5_SECTION_PAGE_URL" => $chosenSection?$chosenSection['SECTION_PAGE_URL']:"",
				"X5_SECTION_NAME" => $chosenSection?$chosenSection['NAME']:"",

				"KEY" => $arResult['CHOOSE'],

				"IBLOCK_TYPE" => $arParams['_arParams']["IBLOCK_TYPE"],
				"IBLOCK_ID" => const_IBLOCK_ID_brands,
				"ELEMENT_SORT_FIELD" => 'name',
				"ELEMENT_SORT_ORDER" => 'asc',
				"ELEMENT_SORT_FIELD2" => $arParams['_arParams']["ELEMENT_SORT_FIELD2"],
				"ELEMENT_SORT_ORDER2" => $arParams['_arParams']["ELEMENT_SORT_ORDER2"],
				"PROPERTY_CODE" => $arParams['_arParams']["LIST_PROPERTY_CODE"],
				"META_KEYWORDS" => 'N',
				"META_DESCRIPTION" => 'N',
				"BROWSER_TITLE" => 'N',
				"SET_LAST_MODIFIED" => 'N',
				"SHOW_ALL_WO_SECTION" => 'Y',
				"INCLUDE_SUBSECTIONS" => 'Y',
				"BASKET_URL" => $arParams['_arParams']["BASKET_URL"],
				"ACTION_VARIABLE" => $arParams['_arParams']["ACTION_VARIABLE"],
				"PRODUCT_ID_VARIABLE" => $arParams['_arParams']["PRODUCT_ID_VARIABLE"],
				"SECTION_ID_VARIABLE" => $arParams['_arParams']["SECTION_ID_VARIABLE"],
				"PRODUCT_QUANTITY_VARIABLE" => $arParams['_arParams']["PRODUCT_QUANTITY_VARIABLE"],
				"PRODUCT_PROPS_VARIABLE" => $arParams['_arParams']["PRODUCT_PROPS_VARIABLE"],
				"FILTER_NAME" => 'arrFilterBrands',
				"CACHE_TYPE" => 'N',
				"CACHE_TIME" => $arParams['_arParams']["CACHE_TIME"],
				"CACHE_FILTER" => $arParams['_arParams']["CACHE_FILTER"],
				"CACHE_GROUPS" => $arParams['_arParams']["CACHE_GROUPS"],
				"SET_TITLE" => 'N',
				"MESSAGE_404" => '',
				"SET_STATUS_404" => 'N',
				"SHOW_404" => 'N',
				"FILE_404" => '',
				"DISPLAY_COMPARE" => 'N',
				"PAGE_ELEMENT_COUNT" => 100000,
				"LINE_ELEMENT_COUNT" => $arParams['_arParams']["LINE_ELEMENT_COUNT"],
				"PRICE_CODE" => $arParams['_arParams']["PRICE_CODE"],
				"USE_PRICE_COUNT" => $arParams['_arParams']["USE_PRICE_COUNT"],
				"SHOW_PRICE_COUNT" => $arParams['_arParams']["SHOW_PRICE_COUNT"],

				"PRICE_VAT_INCLUDE" => $arParams['_arParams']["PRICE_VAT_INCLUDE"],
				"USE_PRODUCT_QUANTITY" => $arParams['_arParams']['USE_PRODUCT_QUANTITY'],
				"ADD_PROPERTIES_TO_BASKET" => (isset($arParams['_arParams']["ADD_PROPERTIES_TO_BASKET"]) ? $arParams['_arParams']["ADD_PROPERTIES_TO_BASKET"] : ''),
				"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams['_arParams']["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams['_arParams']["PARTIAL_PRODUCT_PROPERTIES"] : ''),
				"PRODUCT_PROPERTIES" => $arParams['_arParams']["PRODUCT_PROPERTIES"],

				"DISPLAY_TOP_PAGER" => $arParams['_arParams']["DISPLAY_TOP_PAGER"],
				"DISPLAY_BOTTOM_PAGER" => $arParams['_arParams']["DISPLAY_BOTTOM_PAGER"],
				"PAGER_TITLE" => $arParams['_arParams']["PAGER_TITLE"],
				"PAGER_SHOW_ALWAYS" => $arParams['_arParams']["PAGER_SHOW_ALWAYS"],
				"PAGER_TEMPLATE" => $arParams['_arParams']["PAGER_TEMPLATE"],
				"PAGER_DESC_NUMBERING" => $arParams['_arParams']["PAGER_DESC_NUMBERING"],
				"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams['_arParams']["PAGER_DESC_NUMBERING_CACHE_TIME"],
				"PAGER_SHOW_ALL" => $arParams['_arParams']["PAGER_SHOW_ALL"],
				"PAGER_BASE_LINK_ENABLE" => $arParams['_arParams']["PAGER_BASE_LINK_ENABLE"],
				"PAGER_BASE_LINK" => $arParams['_arParams']["PAGER_BASE_LINK"],
				"PAGER_PARAMS_NAME" => $arParams['_arParams']["PAGER_PARAMS_NAME"],

				"OFFERS_CART_PROPERTIES" => $arParams['_arParams']["OFFERS_CART_PROPERTIES"],
				"OFFERS_FIELD_CODE" => $arParams['_arParams']["LIST_OFFERS_FIELD_CODE"],
				"OFFERS_PROPERTY_CODE" => $arParams['_arParams']["LIST_OFFERS_PROPERTY_CODE"],
				"OFFERS_SORT_FIELD" => $arParams['_arParams']["OFFERS_SORT_FIELD"],
				"OFFERS_SORT_ORDER" => $arParams['_arParams']["OFFERS_SORT_ORDER"],
				"OFFERS_SORT_FIELD2" => $arParams['_arParams']["OFFERS_SORT_FIELD2"],
				"OFFERS_SORT_ORDER2" => $arParams['_arParams']["OFFERS_SORT_ORDER2"],
				"OFFERS_LIMIT" => $arParams['_arParams']["LIST_OFFERS_LIMIT"],

				"SECTION_ID" => "",
				"SECTION_CODE" => "",
				"SECTION_URL" => "",
				"DETAIL_URL" => '/catalog/brands'.$arParams['_arResult']["URL_TEMPLATES"]["element"],
				"USE_MAIN_ELEMENT_SECTION" => $arParams['_arParams']["USE_MAIN_ELEMENT_SECTION"],
				'CONVERT_CURRENCY' => $arParams['_arParams']['CONVERT_CURRENCY'],
				'CURRENCY_ID' => $arParams['_arParams']['CURRENCY_ID'],
				'HIDE_NOT_AVAILABLE' => 'Y',

				'LABEL_PROP' => $arParams['_arParams']['LABEL_PROP'],
				'ADD_PICT_PROP' => $arParams['_arParams']['ADD_PICT_PROP'],
				'PRODUCT_DISPLAY_MODE' => $arParams['_arParams']['PRODUCT_DISPLAY_MODE'],

				'OFFER_ADD_PICT_PROP' => $arParams['_arParams']['OFFER_ADD_PICT_PROP'],
				'OFFER_TREE_PROPS' => $arParams['_arParams']['OFFER_TREE_PROPS'],
				'PRODUCT_SUBSCRIPTION' => $arParams['_arParams']['PRODUCT_SUBSCRIPTION'],
				'SHOW_DISCOUNT_PERCENT' => $arParams['_arParams']['SHOW_DISCOUNT_PERCENT'],
				'SHOW_OLD_PRICE' => $arParams['_arParams']['SHOW_OLD_PRICE'],
				'MESS_BTN_BUY' => $arParams['_arParams']['MESS_BTN_BUY'],
				'MESS_BTN_ADD_TO_BASKET' => $arParams['_arParams']['MESS_BTN_ADD_TO_BASKET'],
				'MESS_BTN_SUBSCRIBE' => $arParams['_arParams']['MESS_BTN_SUBSCRIBE'],
				'MESS_BTN_DETAIL' => $arParams['_arParams']['MESS_BTN_DETAIL'],
				'MESS_NOT_AVAILABLE' => $arParams['_arParams']['MESS_NOT_AVAILABLE'],

				'TEMPLATE_THEME' => (isset($arParams['_arParams']['TEMPLATE_THEME']) ? $arParams['_arParams']['TEMPLATE_THEME'] : ''),
				"ADD_SECTIONS_CHAIN" => "N",
				'ADD_TO_BASKET_ACTION' => "",
				'SHOW_CLOSE_POPUP' => isset($arParams['_arParams']['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['_arParams']['COMMON_SHOW_CLOSE_POPUP'] : '',
				'COMPARE_PATH' => "",
				'BACKGROUND_IMAGE' => (isset($arParams['_arParams']['SECTION_BACKGROUND_IMAGE']) ? $arParams['_arParams']['SECTION_BACKGROUND_IMAGE'] : ''),
				'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['_arParams']['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['_arParams']['DISABLE_INIT_JS_IN_COMPONENT'] : '')
			),
			$arParams['_component']
		);	
		?>
	</div>
</div>