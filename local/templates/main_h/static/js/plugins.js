! function() {
    "use strict";

    function t(n) {
        if (!n) throw new Error("No options passed to Waypoint constructor");
        if (!n.element) throw new Error("No element option passed to Waypoint constructor");
        if (!n.handler) throw new Error("No handler option passed to Waypoint constructor");
        this.key = "waypoint-" + e, this.options = t.Adapter.extend({}, t.defaults, n), this.element = this.options.element, this.adapter = new t.Adapter(this.element), this.callback = n.handler, this.axis = this.options.horizontal ? "horizontal" : "vertical", this.enabled = this.options.enabled, this.triggerPoint = null, this.group = t.Group.findOrCreate({
            name: this.options.group,
            axis: this.axis
        }), this.context = t.Context.findOrCreateByElement(this.options.context), t.offsetAliases[this.options.offset] && (this.options.offset = t.offsetAliases[this.options.offset]), this.group.add(this), this.context.add(this), i[this.key] = this, e += 1
    }
    var e = 0,
        i = {};
    t.prototype.queueTrigger = function(t) {
        this.group.queueTrigger(this, t)
    }, t.prototype.trigger = function(t) {
        this.enabled && this.callback && this.callback.apply(this, t)
    }, t.prototype.destroy = function() {
        this.context.remove(this), this.group.remove(this), delete i[this.key]
    }, t.prototype.disable = function() {
        return this.enabled = !1, this
    }, t.prototype.enable = function() {
        return this.context.refresh(), this.enabled = !0, this
    }, t.prototype.next = function() {
        return this.group.next(this)
    }, t.prototype.previous = function() {
        return this.group.previous(this)
    }, t.invokeAll = function(t) {
        var e = [];
        for (var n in i) e.push(i[n]);
        for (var o = 0, r = e.length; r > o; o++) e[o][t]()
    }, t.destroyAll = function() {
        t.invokeAll("destroy")
    }, t.disableAll = function() {
        t.invokeAll("disable")
    }, t.enableAll = function() {
        t.Context.refreshAll();
        for (var e in i) i[e].enabled = !0;
        return this
    }, t.refreshAll = function() {
        t.Context.refreshAll()
    }, t.viewportHeight = function() {
        return window.innerHeight || document.documentElement.clientHeight
    }, t.viewportWidth = function() {
        return document.documentElement.clientWidth
    }, t.adapters = [], t.defaults = {
        context: window,
        continuous: !0,
        enabled: !0,
        group: "default",
        horizontal: !1,
        offset: 0
    }, t.offsetAliases = {
        "bottom-in-view": function() {
            return this.context.innerHeight() - this.adapter.outerHeight()
        },
        "right-in-view": function() {
            return this.context.innerWidth() - this.adapter.outerWidth()
        }
    }, window.Waypoint = t
}(),
    function() {
        "use strict";

        function t(t) {
            window.setTimeout(t, 1e3 / 60)
        }

        function e(t) {
            this.element = t, this.Adapter = o.Adapter, this.adapter = new this.Adapter(t), this.key = "waypoint-context-" + i, this.didScroll = !1, this.didResize = !1, this.oldScroll = {
                x: this.adapter.scrollLeft(),
                y: this.adapter.scrollTop()
            }, this.waypoints = {
                vertical: {},
                horizontal: {}
            }, t.waypointContextKey = this.key, n[t.waypointContextKey] = this, i += 1, o.windowContext || (o.windowContext = !0, o.windowContext = new e(window)), this.createThrottledScrollHandler(), this.createThrottledResizeHandler()
        }
        var i = 0,
            n = {},
            o = window.Waypoint,
            r = window.onload;
        e.prototype.add = function(t) {
            var e = t.options.horizontal ? "horizontal" : "vertical";
            this.waypoints[e][t.key] = t, this.refresh()
        }, e.prototype.checkEmpty = function() {
            var t = this.Adapter.isEmptyObject(this.waypoints.horizontal),
                e = this.Adapter.isEmptyObject(this.waypoints.vertical),
                i = this.element == this.element.window;
            t && e && !i && (this.adapter.off(".waypoints"), delete n[this.key])
        }, e.prototype.createThrottledResizeHandler = function() {
            function t() {
                e.handleResize(), e.didResize = !1
            }
            var e = this;
            this.adapter.on("resize.waypoints", function() {
                e.didResize || (e.didResize = !0, o.requestAnimationFrame(t))
            })
        }, e.prototype.createThrottledScrollHandler = function() {
            function t() {
                e.handleScroll(), e.didScroll = !1
            }
            var e = this;
            this.adapter.on("scroll.waypoints", function() {
                (!e.didScroll || o.isTouch) && (e.didScroll = !0, o.requestAnimationFrame(t))
            })
        }, e.prototype.handleResize = function() {
            o.Context.refreshAll()
        }, e.prototype.handleScroll = function() {
            var t = {},
                e = {
                    horizontal: {
                        newScroll: this.adapter.scrollLeft(),
                        oldScroll: this.oldScroll.x,
                        forward: "right",
                        backward: "left"
                    },
                    vertical: {
                        newScroll: this.adapter.scrollTop(),
                        oldScroll: this.oldScroll.y,
                        forward: "down",
                        backward: "up"
                    }
                };
            for (var i in e) {
                var n = e[i],
                    o = n.newScroll > n.oldScroll,
                    r = o ? n.forward : n.backward;
                for (var s in this.waypoints[i]) {
                    var a = this.waypoints[i][s];
                    if (null !== a.triggerPoint) {
                        var l = n.oldScroll < a.triggerPoint,
                            c = n.newScroll >= a.triggerPoint,
                            u = l && c,
                            d = !l && !c;
                        (u || d) && (a.queueTrigger(r), t[a.group.id] = a.group)
                    }
                }
            }
            for (var p in t) t[p].flushTriggers();
            this.oldScroll = {
                x: e.horizontal.newScroll,
                y: e.vertical.newScroll
            }
        }, e.prototype.innerHeight = function() {
            return this.element == this.element.window ? o.viewportHeight() : this.adapter.innerHeight()
        }, e.prototype.remove = function(t) {
            delete this.waypoints[t.axis][t.key], this.checkEmpty()
        }, e.prototype.innerWidth = function() {
            return this.element == this.element.window ? o.viewportWidth() : this.adapter.innerWidth()
        }, e.prototype.destroy = function() {
            var t = [];
            for (var e in this.waypoints)
                for (var i in this.waypoints[e]) t.push(this.waypoints[e][i]);
            for (var n = 0, o = t.length; o > n; n++) t[n].destroy()
        }, e.prototype.refresh = function() {
            var t, e = this.element == this.element.window,
                i = e ? void 0 : this.adapter.offset(),
                n = {};
            this.handleScroll(), t = {
                horizontal: {
                    contextOffset: e ? 0 : i.left,
                    contextScroll: e ? 0 : this.oldScroll.x,
                    contextDimension: this.innerWidth(),
                    oldScroll: this.oldScroll.x,
                    forward: "right",
                    backward: "left",
                    offsetProp: "left"
                },
                vertical: {
                    contextOffset: e ? 0 : i.top,
                    contextScroll: e ? 0 : this.oldScroll.y,
                    contextDimension: this.innerHeight(),
                    oldScroll: this.oldScroll.y,
                    forward: "down",
                    backward: "up",
                    offsetProp: "top"
                }
            };
            for (var r in t) {
                var s = t[r];
                for (var a in this.waypoints[r]) {
                    var l, c, u, d, p, f = this.waypoints[r][a],
                        h = f.options.offset,
                        m = f.triggerPoint,
                        v = 0,
                        g = null == m;
                    f.element !== f.element.window && (v = f.adapter.offset()[s.offsetProp]), "function" == typeof h ? h = h.apply(f) : "string" == typeof h && (h = parseFloat(h), f.options.offset.indexOf("%") > -1 && (h = Math.ceil(s.contextDimension * h / 100))), l = s.contextScroll - s.contextOffset, f.triggerPoint = Math.floor(v + l - h), c = m < s.oldScroll, u = f.triggerPoint >= s.oldScroll, d = c && u, p = !c && !u, !g && d ? (f.queueTrigger(s.backward), n[f.group.id] = f.group) : !g && p ? (f.queueTrigger(s.forward), n[f.group.id] = f.group) : g && s.oldScroll >= f.triggerPoint && (f.queueTrigger(s.forward), n[f.group.id] = f.group)
                }
            }
            return o.requestAnimationFrame(function() {
                for (var t in n) n[t].flushTriggers()
            }), this
        }, e.findOrCreateByElement = function(t) {
            return e.findByElement(t) || new e(t)
        }, e.refreshAll = function() {
            for (var t in n) n[t].refresh()
        }, e.findByElement = function(t) {
            return n[t.waypointContextKey]
        }, window.onload = function() {
            r && r(), e.refreshAll()
        }, o.requestAnimationFrame = function(e) {
            var i = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || t;
            i.call(window, e)
        }, o.Context = e
    }(),
    function() {
        "use strict";

        function t(t, e) {
            return t.triggerPoint - e.triggerPoint
        }

        function e(t, e) {
            return e.triggerPoint - t.triggerPoint
        }

        function i(t) {
            this.name = t.name, this.axis = t.axis, this.id = this.name + "-" + this.axis, this.waypoints = [], this.clearTriggerQueues(), n[this.axis][this.name] = this
        }
        var n = {
                vertical: {},
                horizontal: {}
            },
            o = window.Waypoint;
        i.prototype.add = function(t) {
            this.waypoints.push(t)
        }, i.prototype.clearTriggerQueues = function() {
            this.triggerQueues = {
                up: [],
                down: [],
                left: [],
                right: []
            }
        }, i.prototype.flushTriggers = function() {
            for (var i in this.triggerQueues) {
                var n = this.triggerQueues[i],
                    o = "up" === i || "left" === i;
                n.sort(o ? e : t);
                for (var r = 0, s = n.length; s > r; r += 1) {
                    var a = n[r];
                    (a.options.continuous || r === n.length - 1) && a.trigger([i])
                }
            }
            this.clearTriggerQueues()
        }, i.prototype.next = function(e) {
            this.waypoints.sort(t);
            var i = o.Adapter.inArray(e, this.waypoints),
                n = i === this.waypoints.length - 1;
            return n ? null : this.waypoints[i + 1]
        }, i.prototype.previous = function(e) {
            this.waypoints.sort(t);
            var i = o.Adapter.inArray(e, this.waypoints);
            return i ? this.waypoints[i - 1] : null
        }, i.prototype.queueTrigger = function(t, e) {
            this.triggerQueues[e].push(t)
        }, i.prototype.remove = function(t) {
            var e = o.Adapter.inArray(t, this.waypoints);
            e > -1 && this.waypoints.splice(e, 1)
        }, i.prototype.first = function() {
            return this.waypoints[0]
        }, i.prototype.last = function() {
            return this.waypoints[this.waypoints.length - 1]
        }, i.findOrCreate = function(t) {
            return n[t.axis][t.name] || new i(t)
        }, o.Group = i
    }(),
    function() {
        "use strict";

        function t(t) {
            this.$element = e(t)
        }
        var e = window.jQuery,
            i = window.Waypoint;
        e.each(["innerHeight", "innerWidth", "off", "offset", "on", "outerHeight", "outerWidth", "scrollLeft", "scrollTop"], function(e, i) {
            t.prototype[i] = function() {
                var t = Array.prototype.slice.call(arguments);
                return this.$element[i].apply(this.$element, t)
            }
        }), e.each(["extend", "inArray", "isEmptyObject"], function(i, n) {
            t[n] = e[n]
        }), i.adapters.push({
            name: "jquery",
            Adapter: t
        }), i.Adapter = t
    }(),
    function() {
        "use strict";

        function t(t) {
            return function() {
                var i = [],
                    n = arguments[0];
                return t.isFunction(arguments[0]) && (n = t.extend({}, arguments[1]), n.handler = arguments[0]), this.each(function() {
                    var o = t.extend({}, n, {
                        element: this
                    });
                    "string" == typeof o.context && (o.context = t(this).closest(o.context)[0]), i.push(new e(o))
                }), i
            }
        }
        var e = window.Waypoint;
        window.jQuery && (window.jQuery.fn.waypoint = t(window.jQuery)), window.Zepto && (window.Zepto.fn.waypoint = t(window.Zepto))
    }(), ! function(t, e) {
    "object" == typeof exports && "object" == typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define([], e) : "object" == typeof exports ? exports.Tabit = e() : t.Tabit = e()
}(this, function() {
    return function(t) {
        function e(n) {
            if (i[n]) return i[n].exports;
            var o = i[n] = {
                exports: {},
                id: n,
                loaded: !1
            };
            return t[n].call(o.exports, o, o.exports, e), o.loaded = !0, o.exports
        }
        var i = {};
        return e.m = t, e.c = i, e.p = "", e(0)
    }([function(t, e, i) {
        "use strict";

        function n(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
        }
        var o = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) {
                return typeof t
            } : function(t) {
                return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
            },
            r = function() {
                function t(t, e) {
                    for (var i = 0; i < e.length; i++) {
                        var n = e[i];
                        n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n)
                    }
                }
                return function(e, i, n) {
                    return i && t(e.prototype, i), n && t(e, n), e
                }
            }(),
            s = function() {
                function t(e) {
                    var i = this,
                        o = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                    if (n(this, t), !t._isDom(e)) throw new TypeError("`new Tabit` requires a DOM element as its first argument.");
                    e._tabit && e._tabit.destroy();
                    var r = o.buttonSelector,
                        s = void 0 === r ? "a" : r,
                        a = o.contentSelector,
                        l = void 0 === a ? "div" : a,
                        c = o.buttonAttribute,
                        u = void 0 === c ? "href" : c,
                        d = o.contentAttribute,
                        p = void 0 === d ? "id" : d,
                        f = o.buttonActiveClass,
                        h = void 0 === f ? null : f,
                        m = o.contentActiveClass,
                        v = void 0 === m ? null : m,
                        g = o.event,
                        y = void 0 === g ? "click" : g,
                        b = o.activeIndex,
                        w = void 0 === b ? 0 : b,
                        k = o.toggleDisplay,
                        S = void 0 === k || k,
                        x = o.closable,
                        C = void 0 !== x && x,
                        T = o.beforeInit,
                        _ = void 0 === T ? null : T,
                        A = o.onInit,
                        E = void 0 === A ? null : A,
                        $ = o.beforeChange,
                        P = void 0 === $ ? null : $,
                        O = o.onChange,
                        L = void 0 === O ? null : O,
                        I = o.onDestroy,
                        M = void 0 === I ? null : I;
                    this.settings = {
                        buttonSelector: s,
                        contentSelector: l,
                        buttonAttribute: u,
                        contentAttribute: p,
                        buttonActiveClass: h,
                        contentActiveClass: v,
                        event: y,
                        activeIndex: w,
                        toggleDisplay: S,
                        closable: C,
                        beforeInit: _,
                        onInit: E,
                        beforeChange: P,
                        onChange: L,
                        onDestroy: M
                    }, this._checkSettings(), this.element = e, this.tabs = [], this._activeTab = null;
                    var F = this.element.querySelectorAll(this.settings.buttonSelector);
                    F = Array.prototype.slice.call(F);
                    var D = this.element.querySelectorAll(this.settings.contentSelector);
                    D = Array.prototype.slice.call(D);
                    var j = function(t) {
                        var e = t.getAttribute(u),
                            i = !1;
                        return !!e && ("href" === u && (e = e.replace(/^#/g, "")), D.some(function(t, n, o) {
                                return t.getAttribute(p) === e && (i = o[n], o.splice(n, 1), i)
                            }), i)
                    };
                    return F.forEach(function(t) {
                        var e = j(t);
                        e && i.tabs.push({
                            button: t,
                            content: e
                        })
                    }), this._eventHandlerShim = function(t) {
                        i._eventHandler(t)
                    }, this._bindEvents(), _ && "function" == typeof _ && _.call(this, this), this._initState(), this.element._tabit = this, E && "function" == typeof E && E.call(this, this), this.element._tabit
                }
                return r(t, [{
                    key: "setActiveTab",
                    value: function(t) {
                        var e = this.tabs[t];
                        return !!e && (this._runTabEvent(null, e), !0)
                    }
                }, {
                    key: "getActiveTab",
                    value: function() {
                        return this._activeTab
                    }
                }, {
                    key: "getActiveTabIndex",
                    value: function() {
                        return this.tabs.indexOf(this._activeTab)
                    }
                }, {
                    key: "getTab",
                    value: function(t) {
                        return this.tabs[t]
                    }
                }, {
                    key: "next",
                    value: function() {
                        this._paginate()
                    }
                }, {
                    key: "prev",
                    value: function() {
                        this._paginate(!0)
                    }
                }, {
                    key: "destroy",
                    value: function() {
                        var t = this,
                            e = this.settings.onDestroy;
                        this._unbindEvents();
                        var i = this.settings.buttonActiveClass,
                            n = this.settings.contentActiveClass,
                            o = this.settings.toggleDisplay;
                        this.tabs.forEach(function(t) {
                            i && t.button.classList.remove(i), n && t.content.classList.remove(n), o && (t.content.style.display = "")
                        }), delete this.element._tabit;
                        var r = Object.keys(this);
                        r.forEach(function(e) {
                            delete t[e]
                        }), e && "function" == typeof e && e()
                    }
                }, {
                    key: "_checkSettings",
                    value: function() {
                        var t = this;
                        ["beforeInit", "onInit", "beforeChange", "onChange", "onDestroy"].forEach(function(e) {
                            if (t.settings[e] && "function" != typeof t.settings[e]) throw new TypeError("`" + e + "` parameter must be a function.")
                        })
                    }
                }, {
                    key: "_eventHandler",
                    value: function(e) {
                        for (var i = this.tabs, n = e.target, o = void 0; n !== this.element && !o;) {
                            if (t._domMatches(n, this.settings.buttonSelector))
                                for (var r = 0, s = i.length; r < s; r += 1)
                                    if (n === i[r].button) {
                                        o = i[r];
                                        break
                                    }
                            n = n.parentNode
                        }
                        return o ? this._runTabEvent(e, o) : null
                    }
                }, {
                    key: "_runTabEvent",
                    value: function(t, e) {
                        if (e) {
                            t && t.preventDefault();
                            var i = e,
                                n = this._activeTab,
                                o = this.settings.beforeChange,
                                r = this.settings.onChange,
                                s = this.settings.closable,
                                a = i === n;
                            a && !s || (o && "function" == typeof o && o.call(this, n, i, this), a && s ? (this._hideTab(n), this._activeTab = null) : (this._hideTab(n), this._showTab(i)), r && "function" == typeof r && r.call(this, i, this))
                        }
                    }
                }, {
                    key: "_bindEvents",
                    value: function() {
                        this._setEvents()
                    }
                }, {
                    key: "_unbindEvents",
                    value: function() {
                        this._setEvents(!0)
                    }
                }, {
                    key: "_setEvents",
                    value: function(t) {
                        var e = this,
                            i = this.settings.event,
                            n = t ? "removeEventListener" : "addEventListener";
                        "string" == typeof i ? this.element[n](i, this._eventHandlerShim) : Array.isArray(i) && i.forEach(function(t) {
                            e.element[n](t, e._eventHandlerShim)
                        })
                    }
                }, {
                    key: "_initState",
                    value: function() {
                        var t = this.settings.activeIndex;
                        this.settings.toggleDisplay && this.tabs.forEach(function(e, i) {
                            i !== t && (e.content.style.display = "none")
                        }), t >= 0 && this._runTabEvent(null, this.tabs[t])
                    }
                }, {
                    key: "_hideTab",
                    value: function(t) {
                        this._toggleTab(t, !0)
                    }
                }, {
                    key: "_showTab",
                    value: function(t) {
                        this._toggleTab(t)
                    }
                }, {
                    key: "_toggleTab",
                    value: function(t, e) {
                        if (t) {
                            var i = e ? "remove" : "add",
                                n = this.settings.buttonActiveClass,
                                o = this.settings.contentActiveClass;
                            this.settings.toggleDisplay && (t.content.style.display = e ? "none" : ""), n && t.button.classList[i](n), o && t.content.classList[i](o), e || (this._activeTab = t)
                        }
                    }
                }, {
                    key: "_paginate",
                    value: function(t) {
                        if (this._activeTab) {
                            var e = this.tabs.indexOf(this._activeTab),
                                i = this.tabs.length - 1,
                                n = void 0,
                                o = void 0,
                                r = void 0;
                            t ? (r = -1, o = 0 === e) : (r = 1, o = e === i), n = o ? t ? i : 0 : e + r, this.setActiveTab(n)
                        }
                    }
                }], [{
                    key: "_domMatches",
                    value: function(t, e) {
                        var i = window.Element.prototype,
                            n = i.matches || i.mozMatchesSelector || i.msMatchesSelector || i.oMatchesSelector || i.webkitMatchesSelector;
                        if (!t || 1 !== t.nodeType) return !1;
                        var o = t.parentNode;
                        if (n) return n.call(t, e);
                        for (var r = o.querySelectorAll(e), s = r.length, a = 0; a < s; a++)
                            if (r[a] === t) return !0;
                        return !1
                    }
                }, {
                    key: "_isDom",
                    value: function(t) {
                        return !(!t || "object" !== (void 0 === t ? "undefined" : o(t))) && ("object" === ("undefined" == typeof window ? "undefined" : o(window)) && "object" === o(window.Node) ? t instanceof window.Node : "number" == typeof t.nodeType && "string" == typeof t.nodeName)
                    }
                }]), t
            }();
        t.exports = s
    }])
}),
    function(t, e, i, n) {
        t.fn.doubleTapToGo = function(n) {
            return !!("ontouchstart" in e || navigator.msMaxTouchPoints || navigator.userAgent.toLowerCase().match(/windows phone os 7/i)) && ("unbind" === n ? this.each(function() {
                    t(this).off(), t(i).off("click touchstart MSPointerDown", handleTouch)
                }) : this.each(function() {
                    function e(e) {
                        for (var i = !0, o = t(e.target).parents(), r = 0; r < o.length; r++) o[r] == n[0] && (i = !1);
                        i && (n = !1)
                    }
                    var n = !1;
                    t(this).on("click", function(e) {
                        var i = t(this);
                        i[0] != n[0] && (e.preventDefault(), n = i)
                    }), t(i).on("click touchstart MSPointerDown", e)
                }), this)
        }
    }(jQuery, window, document), ! function(t) {
    "function" == typeof define && define.amd ? define(["inputmask.dependencyLib"], t) : "object" == typeof exports ? module.exports = t(require("./inputmask.dependencyLib.jquery")) : t(window.dependencyLib || jQuery)
}(function(t) {
    function e(i, n) {
        return this instanceof e ? (t.isPlainObject(i) ? n = i : (n = n || {}, n.alias = i), this.el = void 0, this.opts = t.extend(!0, {}, this.defaults, n), this.noMasksCache = n && void 0 !== n.definitions, this.userOptions = n || {}, this.events = {}, void o(this.opts.alias, n, this.opts)) : new e(i, n)
    }

    function i(t) {
        var e = document.createElement("input"),
            i = "on" + t,
            n = i in e;
        return n || (e.setAttribute(i, "return;"), n = "function" == typeof e[i]), e = null, n
    }

    function n(e, i) {
        var n = e.getAttribute("type"),
            o = "INPUT" === e.tagName && -1 !== t.inArray(n, i.supportsInputType) || e.isContentEditable || "TEXTAREA" === e.tagName;
        if (!o) {
            var r = document.createElement("input");
            r.setAttribute("type", n), o = "text" === r.type, r = null
        }
        return o
    }

    function o(e, i, n) {
        var r = n.aliases[e];
        return r ? (r.alias && o(r.alias, void 0, n), t.extend(!0, n, r), t.extend(!0, n, i), !0) : (null === n.mask && (n.mask = e), !1)
    }

    function r(e, i, n) {
        function r(t, i) {
            i = void 0 !== i ? i : e.getAttribute("data-inputmask-" + t), null !== i && ("string" == typeof i && (0 === t.indexOf("on") ? i = window[i] : "false" === i ? i = !1 : "true" === i && (i = !0)), n[t] = i)
        }
        var s, a, l, c, u = e.getAttribute("data-inputmask");
        if (u && "" !== u && (u = u.replace(new RegExp("'", "g"), '"'), a = JSON.parse("{" + u + "}")), a) {
            l = void 0;
            for (c in a)
                if ("alias" === c.toLowerCase()) {
                    l = a[c];
                    break
                }
        }
        r("alias", l), n.alias && o(n.alias, n, i);
        for (s in i) {
            if (a) {
                l = void 0;
                for (c in a)
                    if (c.toLowerCase() === s.toLowerCase()) {
                        l = a[c];
                        break
                    }
            }
            r(s, l)
        }
        return t.extend(!0, i, n), i
    }

    function s(i, n) {
        function o(e) {
            function n(t, e, i, n) {
                this.matches = [], this.isGroup = t || !1, this.isOptional = e || !1, this.isQuantifier = i || !1, this.isAlternator = n || !1, this.quantifier = {
                    min: 1,
                    max: 1
                }
            }

            function o(e, n, o) {
                var r = i.definitions[n];
                o = void 0 !== o ? o : e.matches.length;
                var s = e.matches[o - 1];
                if (r && !g) {
                    r.placeholder = t.isFunction(r.placeholder) ? r.placeholder(i) : r.placeholder;
                    for (var a = r.prevalidator, l = a ? a.length : 0, c = 1; c < r.cardinality; c++) {
                        var u = l >= c ? a[c - 1] : [],
                            d = u.validator,
                            p = u.cardinality;
                        e.matches.splice(o++, 0, {
                            fn: d ? "string" == typeof d ? new RegExp(d) : new function() {
                                this.test = d
                            } : new RegExp("."),
                            cardinality: p ? p : 1,
                            optionality: e.isOptional,
                            newBlockMarker: void 0 === s || s.def !== (r.definitionSymbol || n),
                            casing: r.casing,
                            def: r.definitionSymbol || n,
                            placeholder: r.placeholder,
                            mask: n
                        }), s = e.matches[o - 1]
                    }
                    e.matches.splice(o++, 0, {
                        fn: r.validator ? "string" == typeof r.validator ? new RegExp(r.validator) : new function() {
                            this.test = r.validator
                        } : new RegExp("."),
                        cardinality: r.cardinality,
                        optionality: e.isOptional,
                        newBlockMarker: void 0 === s || s.def !== (r.definitionSymbol || n),
                        casing: r.casing,
                        def: r.definitionSymbol || n,
                        placeholder: r.placeholder,
                        mask: n
                    })
                } else e.matches.splice(o++, 0, {
                    fn: null,
                    cardinality: 0,
                    optionality: e.isOptional,
                    newBlockMarker: void 0 === s || s.def !== n,
                    casing: null,
                    def: i.staticDefinitionSymbol || n,
                    placeholder: void 0 !== i.staticDefinitionSymbol ? n : void 0,
                    mask: n
                }), g = !1
            }

            function r(t, e) {
                t.isGroup && (t.isGroup = !1, o(t, i.groupmarker.start, 0), e !== !0 && o(t, i.groupmarker.end))
            }

            function s(t, e, i, n) {
                e.matches.length > 0 && (void 0 === n || n) && (i = e.matches[e.matches.length - 1], r(i)), o(e, t)
            }

            function a() {
                if (b.length > 0) {
                    if (p = b[b.length - 1], s(u, p, h, !p.isAlternator), p.isAlternator) {
                        f = b.pop();
                        for (var t = 0; t < f.matches.length; t++) f.matches[t].isGroup = !1;
                        b.length > 0 ? (p = b[b.length - 1], p.matches.push(f)) : y.matches.push(f)
                    }
                } else s(u, y, h)
            }

            function l(t) {
                function e(t) {
                    return t === i.optionalmarker.start ? t = i.optionalmarker.end : t === i.optionalmarker.end ? t = i.optionalmarker.start : t === i.groupmarker.start ? t = i.groupmarker.end : t === i.groupmarker.end && (t = i.groupmarker.start), t
                }
                t.matches = t.matches.reverse();
                for (var n in t.matches) {
                    var o = parseInt(n);
                    if (t.matches[n].isQuantifier && t.matches[o + 1] && t.matches[o + 1].isGroup) {
                        var r = t.matches[n];
                        t.matches.splice(n, 1), t.matches.splice(o + 1, 0, r)
                    }
                    void 0 !== t.matches[n].matches ? t.matches[n] = l(t.matches[n]) : t.matches[n] = e(t.matches[n])
                }
                return t
            }
            for (var c, u, d, p, f, h, m, v = /(?:[?*+]|\{[0-9\+\*]+(?:,[0-9\+\*]*)?\})|[^.?*+^${[]()|\\]+|./g, g = !1, y = new n, b = [], w = []; c = v.exec(e);)
                if (u = c[0], g) a();
                else switch (u.charAt(0)) {
                    case i.escapeChar:
                        g = !0;
                        break;
                    case i.optionalmarker.end:
                    case i.groupmarker.end:
                        if (d = b.pop(), void 0 !== d)
                            if (b.length > 0) {
                                if (p = b[b.length - 1], p.matches.push(d), p.isAlternator) {
                                    f = b.pop();
                                    for (var k = 0; k < f.matches.length; k++) f.matches[k].isGroup = !1;
                                    b.length > 0 ? (p = b[b.length - 1], p.matches.push(f)) : y.matches.push(f)
                                }
                            } else y.matches.push(d);
                        else a();
                        break;
                    case i.optionalmarker.start:
                        b.push(new n(!1, !0));
                        break;
                    case i.groupmarker.start:
                        b.push(new n(!0));
                        break;
                    case i.quantifiermarker.start:
                        var S = new n(!1, !1, !0);
                        u = u.replace(/[{}]/g, "");
                        var x = u.split(","),
                            C = isNaN(x[0]) ? x[0] : parseInt(x[0]),
                            T = 1 === x.length ? C : isNaN(x[1]) ? x[1] : parseInt(x[1]);
                        if (("*" === T || "+" === T) && (C = "*" === T ? 0 : 1), S.quantifier = {
                                min: C,
                                max: T
                            }, b.length > 0) {
                            var _ = b[b.length - 1].matches;
                            c = _.pop(), c.isGroup || (m = new n(!0), m.matches.push(c), c = m), _.push(c), _.push(S)
                        } else c = y.matches.pop(), c.isGroup || (m = new n(!0), m.matches.push(c), c = m), y.matches.push(c), y.matches.push(S);
                        break;
                    case i.alternatormarker:
                        b.length > 0 ? (p = b[b.length - 1], h = p.matches.pop()) : h = y.matches.pop(), h.isAlternator ? b.push(h) : (f = new n(!1, !1, !1, !0), f.matches.push(h), b.push(f));
                        break;
                    default:
                        a()
                }
            for (; b.length > 0;) d = b.pop(), r(d, !0), y.matches.push(d);
            return y.matches.length > 0 && (h = y.matches[y.matches.length - 1], r(h), w.push(y)), i.numericInput && l(w[0]), w
        }

        function r(r, s) {
            if (null !== r && "" !== r) {
                if (1 === r.length && i.greedy === !1 && 0 !== i.repeat && (i.placeholder = ""), i.repeat > 0 || "*" === i.repeat || "+" === i.repeat) {
                    var a = "*" === i.repeat ? 0 : "+" === i.repeat ? 1 : i.repeat;
                    r = i.groupmarker.start + r + i.groupmarker.end + i.quantifiermarker.start + a + "," + i.repeat + i.quantifiermarker.end
                }
                var l;
                return void 0 === e.prototype.masksCache[r] || n === !0 ? (l = {
                    mask: r,
                    maskToken: o(r),
                    validPositions: {},
                    _buffer: void 0,
                    buffer: void 0,
                    tests: {},
                    metadata: s
                }, n !== !0 && (e.prototype.masksCache[i.numericInput ? r.split("").reverse().join("") : r] = l, l = t.extend(!0, {}, e.prototype.masksCache[i.numericInput ? r.split("").reverse().join("") : r]))) : l = t.extend(!0, {}, e.prototype.masksCache[i.numericInput ? r.split("").reverse().join("") : r]), l
            }
        }

        function s(t) {
            return t = t.toString()
        }
        var a;
        if (t.isFunction(i.mask) && (i.mask = i.mask(i)), t.isArray(i.mask)) {
            if (i.mask.length > 1) {
                i.keepStatic = null === i.keepStatic || i.keepStatic;
                var l = "(";
                return t.each(i.numericInput ? i.mask.reverse() : i.mask, function(e, i) {
                    l.length > 1 && (l += ")|("), l += s(void 0 === i.mask || t.isFunction(i.mask) ? i : i.mask)
                }), l += ")", r(l, i.mask)
            }
            i.mask = i.mask.pop()
        }
        return i.mask && (a = void 0 === i.mask.mask || t.isFunction(i.mask.mask) ? r(s(i.mask), i.mask) : r(s(i.mask.mask), i.mask)), a
    }

    function a(o, r, s) {
        function l(t, e, i) {
            e = e || 0;
            var n, o, r, a = [],
                l = 0,
                c = h();
            do {
                if (t === !0 && p().validPositions[l]) {
                    var u = p().validPositions[l];
                    o = u.match, n = u.locator.slice(), a.push(i === !0 ? u.input : M(l, o))
                } else r = g(l, n, l - 1), o = r.match, n = r.locator.slice(), (s.jitMasking === !1 || c > l || isFinite(s.jitMasking) && s.jitMasking > l) && a.push(M(l, o));
                l++
            } while ((void 0 === pt || pt > l - 1) && null !== o.fn || null === o.fn && "" !== o.def || e >= l);
            return "" === a[a.length - 1] && a.pop(), a
        }

        function p() {
            return r
        }

        function f(t) {
            var e = p();
            e.buffer = void 0, t !== !0 && (e.tests = {}, e._buffer = void 0, e.validPositions = {}, e.p = 0)
        }

        function h(t, e) {
            var i = -1,
                n = -1,
                o = p().validPositions;
            void 0 === t && (t = -1);
            for (var r in o) {
                var s = parseInt(r);
                o[s] && (e || null !== o[s].match.fn) && (t >= s && (i = s), s >= t && (n = s))
            }
            return -1 !== i && t - i > 1 || t > n ? i : n
        }

        function m(e, i, n) {
            if (s.insertMode && void 0 !== p().validPositions[e] && void 0 === n) {
                var o, r = t.extend(!0, {}, p().validPositions),
                    a = h();
                for (o = e; a >= o; o++) delete p().validPositions[o];
                p().validPositions[e] = i;
                var l, c = !0,
                    u = p().validPositions;
                for (o = l = e; a >= o; o++) {
                    var d = r[o];
                    if (void 0 !== d)
                        for (var m = l, v = -1; m < $() && (null == d.match.fn && u[o] && (u[o].match.optionalQuantifier === !0 || u[o].match.optionality === !0) || null != d.match.fn);) {
                            if (null === d.match.fn || !s.keepStatic && u[o] && (void 0 !== u[o + 1] && k(o + 1, u[o].locator.slice(), o).length > 1 || void 0 !== u[o].alternation) ? m++ : m = P(l), b(m, d.match.def)) {
                                var g = A(m, d.input, !0, !0);
                                c = g !== !1, l = g.caret || g.insert ? h() : m;
                                break
                            }
                            if (c = null == d.match.fn, v === m) break;
                            v = m
                        }
                    if (!c) break
                }
                if (!c) return p().validPositions = t.extend(!0, {}, r), f(!0), !1
            } else p().validPositions[e] = i;
            return f(!0), !0
        }

        function v(t, e, i, n) {
            var o, r = t;
            for (p().p = t, o = r; e > o; o++) void 0 !== p().validPositions[o] && (i === !0 || s.canClearPosition(p(), o, h(), n, s) !== !1) && delete p().validPositions[o];
            for (o = r + 1; o <= h();) {
                for (; void 0 !== p().validPositions[r];) r++;
                var a = p().validPositions[r];
                if (r > o && (o = r + 1), void 0 === p().validPositions[o] && E(o) || void 0 !== a) o++;
                else {
                    var l = g(o);
                    b(r, l.match.def) ? A(r, l.input || M(o), !0) !== !1 && (delete p().validPositions[o], o++) : E(o) || (o++, r--), r++
                }
            }
            var c = h(),
                u = $();
            for (n !== !0 && i !== !0 && void 0 !== p().validPositions[c] && p().validPositions[c].input === s.radixPoint && delete p().validPositions[c], o = c + 1; u >= o; o++) p().validPositions[o] && delete p().validPositions[o];
            f(!0)
        }

        function g(t, e, i) {
            var n = p().validPositions[t];
            if (void 0 === n)
                for (var o = k(t, e, i), r = h(), a = p().validPositions[r] || k(0)[0], l = void 0 !== a.alternation ? a.locator[a.alternation].toString().split(",") : [], c = 0; c < o.length && (n = o[c], !(n.match && (s.greedy && n.match.optionalQuantifier !== !0 || (n.match.optionality === !1 || n.match.newBlockMarker === !1) && n.match.optionalQuantifier !== !0) && (void 0 === a.alternation || a.alternation !== n.alternation || void 0 !== n.locator[a.alternation] && _(n.locator[a.alternation].toString().split(","), l)))); c++);
            return n
        }

        function y(t) {
            return p().validPositions[t] ? p().validPositions[t].match : k(t)[0].match
        }

        function b(t, e) {
            for (var i = !1, n = k(t), o = 0; o < n.length; o++)
                if (n[o].match && n[o].match.def === e) {
                    i = !0;
                    break
                }
            return i
        }

        function w(e, i) {
            var n, o;
            return (p().tests[e] || p().validPositions[e]) && t.each(p().tests[e] || [p().validPositions[e]], function(t, e) {
                var r = e.alternation ? e.locator[e.alternation].toString().indexOf(i) : -1;
                (void 0 === o || o > r) && -1 !== r && (n = e, o = r)
            }), n
        }

        function k(e, i, n) {
            function o(i, n, r, a) {
                function c(r, a, h) {
                    function m(e, i) {
                        var n = 0 === t.inArray(e, i.matches);
                        return n || t.each(i.matches, function(t, o) {
                            return (o.isQuantifier !== !0 || !(n = m(e, i.matches[t - 1]))) && void 0
                        }), n
                    }

                    function v(t, e) {
                        var i = w(t, e);
                        return i ? i.locator.slice(i.alternation + 1) : []
                    }
                    if (l > 1e4) throw "Inputmask: There is probably an error in your mask definition or in the code. Create an issue on github with an example of the mask you are using. " + p().mask;
                    if (l === e && void 0 === r.matches) return u.push({
                        match: r,
                        locator: a.reverse(),
                        cd: f
                    }), !0;
                    if (void 0 !== r.matches) {
                        if (r.isGroup && h !== r) {
                            if (r = c(i.matches[t.inArray(r, i.matches) + 1], a)) return !0
                        } else if (r.isOptional) {
                            var g = r;
                            if (r = o(r, n, a, h)) {
                                if (s = u[u.length - 1].match, !m(s, g)) return !0;
                                d = !0, l = e
                            }
                        } else if (r.isAlternator) {
                            var y, b = r,
                                k = [],
                                S = u.slice(),
                                x = a.length,
                                C = n.length > 0 ? n.shift() : -1;
                            if (-1 === C || "string" == typeof C) {
                                var T, _ = l,
                                    A = n.slice(),
                                    E = [];
                                if ("string" == typeof C) E = C.split(",");
                                else
                                    for (T = 0; T < b.matches.length; T++) E.push(T);
                                for (var $ = 0; $ < E.length; $++) {
                                    if (T = parseInt(E[$]), u = [], n = v(l, T), r = c(b.matches[T] || i.matches[T], [T].concat(a), h) || r, r !== !0 && void 0 !== r && E[E.length - 1] < b.matches.length) {
                                        var P = t.inArray(r, i.matches) + 1;
                                        i.matches.length > P && (r = c(i.matches[P], [P].concat(a.slice(1, a.length)), h), r && (E.push(P.toString()), t.each(u, function(t, e) {
                                            e.alternation = a.length - 1
                                        })))
                                    }
                                    y = u.slice(), l = _, u = [];
                                    for (var O = 0; O < A.length; O++) n[O] = A[O];
                                    for (var L = 0; L < y.length; L++) {
                                        var I = y[L];
                                        I.alternation = I.alternation || x;
                                        for (var M = 0; M < k.length; M++) {
                                            var F = k[M];
                                            if (I.match.def === F.match.def && ("string" != typeof C || -1 !== t.inArray(I.locator[I.alternation].toString(), E))) {
                                                I.match.mask === F.match.mask && (y.splice(L, 1), L--), -1 === F.locator[I.alternation].toString().indexOf(I.locator[I.alternation]) && (F.locator[I.alternation] = F.locator[I.alternation] + "," + I.locator[I.alternation], F.alternation = I.alternation);
                                                break
                                            }
                                        }
                                    }
                                    k = k.concat(y)
                                }
                                "string" == typeof C && (k = t.map(k, function(e, i) {
                                    if (isFinite(i)) {
                                        var n, o = e.alternation,
                                            r = e.locator[o].toString().split(",");
                                        e.locator[o] = void 0, e.alternation = void 0;
                                        for (var s = 0; s < r.length; s++) n = -1 !== t.inArray(r[s], E), n && (void 0 !== e.locator[o] ? (e.locator[o] += ",", e.locator[o] += r[s]) : e.locator[o] = parseInt(r[s]), e.alternation = o);
                                        if (void 0 !== e.locator[o]) return e
                                    }
                                })), u = S.concat(k), l = e, d = u.length > 0
                            } else r = c(b.matches[C] || i.matches[C], [C].concat(a), h);
                            if (r) return !0
                        } else if (r.isQuantifier && h !== i.matches[t.inArray(r, i.matches) - 1])
                            for (var D = r, j = n.length > 0 ? n.shift() : 0; j < (isNaN(D.quantifier.max) ? j + 1 : D.quantifier.max) && e >= l; j++) {
                                var H = i.matches[t.inArray(D, i.matches) - 1];
                                if (r = c(H, [j].concat(a), H)) {
                                    if (s = u[u.length - 1].match, s.optionalQuantifier = j > D.quantifier.min - 1, m(s, H)) {
                                        if (j > D.quantifier.min - 1) {
                                            d = !0, l = e;
                                            break
                                        }
                                        return !0
                                    }
                                    return !0
                                }
                            } else if (r = o(r, n, a, h)) return !0
                    } else l++
                }
                for (var h = n.length > 0 ? n.shift() : 0; h < i.matches.length; h++)
                    if (i.matches[h].isQuantifier !== !0) {
                        var m = c(i.matches[h], [h].concat(r), a);
                        if (m && l === e) return m;
                        if (l > e) break
                    }
            }

            function r(t) {
                var e = t[0] || t;
                return e.locator.slice()
            }
            var s, a = p().maskToken,
                l = i ? n : 0,
                c = i || [0],
                u = [],
                d = !1,
                f = i ? i.join("") : "";
            if (e > -1) {
                if (void 0 === i) {
                    for (var h, m = e - 1; void 0 === (h = p().validPositions[m] || p().tests[m]) && m > -1;) m--;
                    void 0 !== h && m > -1 && (c = r(h), f = c.join(""), h = h[0] || h, l = m)
                }
                if (p().tests[e] && p().tests[e][0].cd === f) return p().tests[e];
                for (var v = c.shift(); v < a.length; v++) {
                    var g = o(a[v], c, [v]);
                    if (g && l === e || l > e) break
                }
            }
            return (0 === u.length || d) && u.push({
                match: {
                    fn: null,
                    cardinality: 0,
                    optionality: !0,
                    casing: null,
                    def: ""
                },
                locator: []
            }), p().tests[e] = t.extend(!0, [], u), p().tests[e]
        }

        function S() {
            return void 0 === p()._buffer && (p()._buffer = l(!1, 1)), p()._buffer
        }

        function x(t) {
            if (void 0 === p().buffer || t === !0) {
                if (t === !0)
                    for (var e in p().tests) void 0 === p().validPositions[e] && delete p().tests[e];
                p().buffer = l(!0, h(), !0)
            }
            return p().buffer
        }

        function C(t, e, i) {
            var n;
            if (i = i, t === !0) f(), t = 0, e = i.length;
            else
                for (n = t; e > n; n++) delete p().validPositions[n], delete p().tests[n];
            for (n = t; e > n; n++) f(!0), i[n] !== s.skipOptionalPartCharacter && A(n, i[n], !0, !0)
        }

        function T(t, e) {
            switch (e.casing) {
                case "upper":
                    t = t.toUpperCase();
                    break;
                case "lower":
                    t = t.toLowerCase()
            }
            return t
        }

        function _(e, i) {
            for (var n = s.greedy ? i : i.slice(0, 1), o = !1, r = 0; r < e.length; r++)
                if (-1 !== t.inArray(e[r], n)) {
                    o = !0;
                    break
                }
            return o
        }

        function A(e, i, n, o) {
            function r(e, i, n, o) {
                var r = !1;
                return t.each(k(e), function(a, l) {
                    for (var c = l.match, u = i ? 1 : 0, d = "", g = c.cardinality; g > u; g--) d += L(e - (g - 1));
                    if (i && (d += i), x(!0), r = null != c.fn ? c.fn.test(d, p(), e, n, s) : (i === c.def || i === s.skipOptionalPartCharacter) && "" !== c.def && {
                            c: c.placeholder || c.def,
                            pos: e
                        }, r !== !1) {
                        var y = void 0 !== r.c ? r.c : i;
                        y = y === s.skipOptionalPartCharacter && null === c.fn ? c.placeholder || c.def : y;
                        var b = e,
                            w = x();
                        if (void 0 !== r.remove && (t.isArray(r.remove) || (r.remove = [r.remove]), t.each(r.remove.sort(function(t, e) {
                                return e - t
                            }), function(t, e) {
                                v(e, e + 1, !0)
                            })), void 0 !== r.insert && (t.isArray(r.insert) || (r.insert = [r.insert]), t.each(r.insert.sort(function(t, e) {
                                return t - e
                            }), function(t, e) {
                                A(e.pos, e.c, !1, o)
                            })), r.refreshFromBuffer) {
                            var k = r.refreshFromBuffer;
                            if (n = !0, C(k === !0 ? k : k.start, k.end, w), void 0 === r.pos && void 0 === r.c) return r.pos = h(), !1;
                            if (b = void 0 !== r.pos ? r.pos : e, b !== e) return r = t.extend(r, A(b, y, !0, o)), !1
                        } else if (r !== !0 && void 0 !== r.pos && r.pos !== e && (b = r.pos, C(e, b, x().slice()), b !== e)) return r = t.extend(r, A(b, y, !0)), !1;
                        return (r === !0 || void 0 !== r.pos || void 0 !== r.c) && (a > 0 && f(!0), m(b, t.extend({}, l, {
                                input: T(y, c)
                            }), o) || (r = !1), !1)
                    }
                }), r
            }

            function a(e, i, n, o) {
                for (var r, a, l, c, u, d, m = t.extend(!0, {}, p().validPositions), v = t.extend(!0, {}, p().tests), y = h(); y >= 0 && (c = p().validPositions[y], !c || void 0 === c.alternation || (r = y, a = p().validPositions[r].alternation, g(r).locator[c.alternation] === c.locator[c.alternation])); y--);
                if (void 0 !== a) {
                    r = parseInt(r);
                    for (var b in p().validPositions)
                        if (b = parseInt(b), c = p().validPositions[b], b >= r && void 0 !== c.alternation) {
                            var k;
                            0 === r ? (k = [], t.each(p().tests[r], function(t, e) {
                                void 0 !== e.locator[a] && (k = k.concat(e.locator[a].toString().split(",")))
                            })) : k = p().validPositions[r].locator[a].toString().split(",");
                            var S = void 0 !== c.locator[a] ? c.locator[a] : k[0];
                            S.length > 0 && (S = S.split(",")[0]);
                            for (var x = 0; x < k.length; x++) {
                                var C = [],
                                    T = 0,
                                    _ = 0;
                                if (S < k[x]) {
                                    for (var E, $, P = b; P >= 0; P--)
                                        if (E = p().validPositions[P], void 0 !== E) {
                                            var O = w(P, k[x]);
                                            p().validPositions[P].match.def !== O.match.def && (C.push(p().validPositions[P].input), p().validPositions[P] = O, p().validPositions[P].input = M(P), null === p().validPositions[P].match.fn && _++, E = O), $ = E.locator[a], E.locator[a] = parseInt(k[x]);
                                            break
                                        }
                                    if (S !== E.locator[a]) {
                                        for (u = b + 1; u < h(void 0, !0) + 1; u++) d = p().validPositions[u], d && null != d.match.fn ? C.push(d.input) : e > u && T++, delete p().validPositions[u], delete p().tests[u];
                                        for (f(!0), s.keepStatic = !s.keepStatic, l = !0; C.length > 0;) {
                                            var L = C.shift();
                                            if (L !== s.skipOptionalPartCharacter && !(l = A(h(void 0, !0) + 1, L, !1, o))) break;
                                        }
                                        if (E.alternation = a, E.locator[a] = $, l) {
                                            var I = h(e) + 1;
                                            for (u = b + 1; u < h() + 1; u++) d = p().validPositions[u], (void 0 === d || null == d.match.fn) && e > u && _++;
                                            e += _ - T, l = A(e > I ? I : e, i, n, o)
                                        }
                                        if (s.keepStatic = !s.keepStatic, l) return l;
                                        f(), p().validPositions = t.extend(!0, {}, m), p().tests = t.extend(!0, {}, v)
                                    }
                                }
                            }
                            break
                        }
                }
                return !1
            }

            function l(e, i) {
                for (var n = p().validPositions[i], o = n.locator, r = o.length, s = e; i > s; s++)
                    if (void 0 === p().validPositions[s] && !E(s, !0)) {
                        var a = k(s),
                            l = a[0],
                            c = -1;
                        t.each(a, function(t, e) {
                            for (var i = 0; r > i && void 0 !== e.locator[i] && _(e.locator[i].toString().split(","), o[i].toString().split(",")); i++) i > c && (c = i, l = e)
                        }), m(s, t.extend({}, l, {
                            input: l.match.placeholder || l.match.def
                        }), !0)
                    }
            }
            n = n === !0;
            for (var c = x(), u = e - 1; u > -1 && !p().validPositions[u]; u--);
            for (u++; e > u; u++) void 0 === p().validPositions[u] && ((!E(u) || c[u] !== M(u)) && k(u).length > 1 || c[u] === s.radixPoint || "0" === c[u] && t.inArray(s.radixPoint, c) < u) && r(u, c[u], !0, o);
            var d = e,
                y = !1,
                b = t.extend(!0, {}, p().validPositions);
            if (d < $() && (y = r(d, i, n, o), (!n || o === !0) && y === !1)) {
                var S = p().validPositions[d];
                if (!S || null !== S.match.fn || S.match.def !== i && i !== s.skipOptionalPartCharacter) {
                    if ((s.insertMode || void 0 === p().validPositions[P(d)]) && !E(d, !0)) {
                        var O = g(d).match,
                            O = O.placeholder || O.def;
                        r(d, O, n, o);
                        for (var I = d + 1, F = P(d); F >= I; I++)
                            if (y = r(I, i, n, o), y !== !1) {
                                l(d, I), d = I;
                                break
                            }
                    }
                } else y = {
                    caret: P(d)
                }
            }
            if (y === !1 && s.keepStatic && (y = a(e, i, n, o)), y === !0 && (y = {
                    pos: d
                }), t.isFunction(s.postValidation) && y !== !1 && !n && o !== !0) {
                var D = s.postValidation(x(!0), y, s);
                if (D) {
                    if (D.refreshFromBuffer) {
                        var j = D.refreshFromBuffer;
                        C(j === !0 ? j : j.start, j.end, D.buffer), f(!0), y = D
                    }
                } else f(!0), p().validPositions = t.extend(!0, {}, b), y = !1
            }
            return y
        }

        function E(t, e) {
            var i;
            if (e ? (i = g(t).match, "" == i.def && (i = y(t))) : i = y(t), null != i.fn) return i.fn;
            if (e !== !0 && t > -1 && !s.keepStatic && void 0 === p().validPositions[t]) {
                var n = k(t);
                return n.length > 2
            }
            return !1
        }

        function $() {
            var t;
            pt = void 0 !== ut ? ut.maxLength : void 0, -1 === pt && (pt = void 0);
            var e, i = h(),
                n = p().validPositions[i],
                o = void 0 !== n ? n.locator.slice() : void 0;
            for (e = i + 1; void 0 === n || null !== n.match.fn || null === n.match.fn && "" !== n.match.def; e++) n = g(e, o, e - 1), o = n.locator.slice();
            var r = y(e - 1);
            return t = "" !== r.def ? e : e - 1, void 0 === pt || pt > t ? t : pt
        }

        function P(t, e) {
            var i = $();
            if (t >= i) return i;
            for (var n = t; ++n < i && (e === !0 && (y(n).newBlockMarker !== !0 || !E(n)) || e !== !0 && !E(n) && (s.nojumps !== !0 || s.nojumpsThreshold > n)););
            return n
        }

        function O(t, e) {
            var i = t;
            if (0 >= i) return 0;
            for (; --i > 0 && (e === !0 && y(i).newBlockMarker !== !0 || e !== !0 && !E(i)););
            return i
        }

        function L(t) {
            return void 0 === p().validPositions[t] ? M(t) : p().validPositions[t].input
        }

        function I(e, i, n, o, r) {
            if (o && t.isFunction(s.onBeforeWrite)) {
                var a = s.onBeforeWrite(o, i, n, s);
                if (a) {
                    if (a.refreshFromBuffer) {
                        var l = a.refreshFromBuffer;
                        C(l === !0 ? l : l.start, l.end, a.buffer || i), i = x(!0)
                    }
                    void 0 !== n && (n = void 0 !== a.caret ? a.caret : n)
                }
            }
            e.inputmask._valueSet(i.join("")), void 0 === n || void 0 !== o && "blur" === o.type || j(e, n), r === !0 && (vt = !0, t(e).trigger("input"))
        }

        function M(t, e) {
            if (e = e || y(t), void 0 !== e.placeholder) return e.placeholder;
            if (null === e.fn) {
                if (t > -1 && !s.keepStatic && void 0 === p().validPositions[t]) {
                    var i, n = k(t),
                        o = 0;
                    if (n.length > 2)
                        for (var r = 0; r < n.length; r++)
                            if (n[r].match.optionality !== !0 && n[r].match.optionalQuantifier !== !0 && (null === n[r].match.fn || void 0 === i || n[r].match.fn.test(i.match.def, p(), t, !0, s) !== !1) && (o++, null === n[r].match.fn && (i = n[r]), o > 1)) return s.placeholder.charAt(t % s.placeholder.length)
                }
                return e.def
            }
            return s.placeholder.charAt(t % s.placeholder.length)
        }

        function F(i, n, o, r) {
            function a() {
                var t = !1,
                    e = S().slice(u, P(u)).join("").indexOf(c);
                if (-1 !== e && !E(u)) {
                    t = !0;
                    for (var i = S().slice(u, u + e), n = 0; n < i.length; n++)
                        if (" " !== i[n]) {
                            t = !1;
                            break
                        }
                }
                return t
            }
            var l = r.slice(),
                c = "",
                u = 0;
            if (f(), p().p = P(-1), !o)
                if (s.autoUnmask !== !0) {
                    var d = S().slice(0, P(-1)).join(""),
                        m = l.join("").match(new RegExp("^" + e.escapeRegex(d), "g"));
                    m && m.length > 0 && (l.splice(0, m.length * d.length), u = P(u))
                } else u = P(u);
            t.each(l, function(e, n) {
                if (void 0 !== n) {
                    var r = new t.Event("keypress");
                    r.which = n.charCodeAt(0), c += n;
                    var l = h(void 0, !0),
                        d = p().validPositions[l],
                        f = g(l + 1, d ? d.locator.slice() : void 0, l);
                    if (!a() || o || s.autoUnmask) {
                        var m = o ? e : null == f.match.fn && f.match.optionality && l + 1 < p().p ? l + 1 : p().p;
                        Y.call(i, r, !0, !1, o, m), u = m + 1, c = ""
                    } else Y.call(i, r, !0, !1, !0, l + 1)
                }
            }), n && I(i, x(), document.activeElement === i ? P(h(0)) : void 0, new t.Event("checkval"))
        }

        function D(e) {
            if (e && void 0 === e.inputmask) return e.value;
            var i = [],
                n = p().validPositions;
            for (var o in n) n[o].match && null != n[o].match.fn && i.push(n[o].input);
            var r = 0 === i.length ? null : (ht ? i.reverse() : i).join("");
            if (null !== r) {
                var a = (ht ? x().slice().reverse() : x()).join("");
                t.isFunction(s.onUnMask) && (r = s.onUnMask(a, r, s) || r)
            }
            return r
        }

        function j(t, e, i, n) {
            function o(t) {
                if (n !== !0 && ht && "number" == typeof t && (!s.greedy || "" !== s.placeholder)) {
                    var e = x().join("").length;
                    t = e - t
                }
                return t
            }
            var r;
            if ("number" != typeof e) return t.setSelectionRange ? (e = t.selectionStart, i = t.selectionEnd) : window.getSelection ? (r = window.getSelection().getRangeAt(0), (r.commonAncestorContainer.parentNode === t || r.commonAncestorContainer === t) && (e = r.startOffset, i = r.endOffset)) : document.selection && document.selection.createRange && (r = document.selection.createRange(), e = 0 - r.duplicate().moveStart("character", -1e5), i = e + r.text.length), {
                begin: o(e),
                end: o(i)
            };
            e = o(e), i = o(i), i = "number" == typeof i ? i : e;
            var a = parseInt(((t.ownerDocument.defaultView || window).getComputedStyle ? (t.ownerDocument.defaultView || window).getComputedStyle(t, null) : t.currentStyle).fontSize) * i;
            if (t.scrollLeft = a > t.scrollWidth ? a : 0, c || s.insertMode !== !1 || e !== i || i++, t.setSelectionRange) t.selectionStart = e, t.selectionEnd = i;
            else if (window.getSelection) {
                if (r = document.createRange(), void 0 === t.firstChild || null === t.firstChild) {
                    var l = document.createTextNode("");
                    t.appendChild(l)
                }
                r.setStart(t.firstChild, e < t.inputmask._valueGet().length ? e : t.inputmask._valueGet().length), r.setEnd(t.firstChild, i < t.inputmask._valueGet().length ? i : t.inputmask._valueGet().length), r.collapse(!0);
                var u = window.getSelection();
                u.removeAllRanges(), u.addRange(r)
            } else t.createTextRange && (r = t.createTextRange(), r.collapse(!0), r.moveEnd("character", i), r.moveStart("character", e), r.select())
        }

        function H(e) {
            var i, n, o = x(),
                r = o.length,
                s = h(),
                a = {},
                l = p().validPositions[s],
                c = void 0 !== l ? l.locator.slice() : void 0;
            for (i = s + 1; i < o.length; i++) n = g(i, c, i - 1), c = n.locator.slice(), a[i] = t.extend(!0, {}, n);
            var u = l && void 0 !== l.alternation ? l.locator[l.alternation] : void 0;
            for (i = r - 1; i > s && (n = a[i], (n.match.optionality || n.match.optionalQuantifier || u && (u !== a[i].locator[l.alternation] && null != n.match.fn || null === n.match.fn && n.locator[l.alternation] && _(n.locator[l.alternation].toString().split(","), u.toString().split(",")) && "" !== k(i)[0].def)) && o[i] === M(i, n.match)); i--) r--;
            return e ? {
                l: r,
                def: a[r] ? a[r].match : void 0
            } : r
        }

        function R(t) {
            for (var e = H(), i = t.length - 1; i > e && !E(i); i--);
            return t.splice(e, i + 1 - e), t
        }

        function W(e) {
            if (t.isFunction(s.isComplete)) return s.isComplete(e, s);
            if ("*" !== s.repeat) {
                var i = !1,
                    n = H(!0),
                    o = O(n.l);
                if (void 0 === n.def || n.def.newBlockMarker || n.def.optionality || n.def.optionalQuantifier) {
                    i = !0;
                    for (var r = 0; o >= r; r++) {
                        var a = g(r).match;
                        if (null !== a.fn && void 0 === p().validPositions[r] && a.optionality !== !0 && a.optionalQuantifier !== !0 || null === a.fn && e[r] !== M(r, a)) {
                            i = !1;
                            break
                        }
                    }
                }
                return i
            }
        }

        function z(t, e) {
            return ht ? t - e > 1 || t - e === 1 && s.insertMode : e - t > 1 || e - t === 1 && s.insertMode
        }

        function B(e) {
            function i(e) {
                if (t.valHooks && (void 0 === t.valHooks[e] || t.valHooks[e].inputmaskpatch !== !0)) {
                    var i = t.valHooks[e] && t.valHooks[e].get ? t.valHooks[e].get : function(t) {
                            return t.value
                        },
                        n = t.valHooks[e] && t.valHooks[e].set ? t.valHooks[e].set : function(t, e) {
                            return t.value = e, t
                        };
                    t.valHooks[e] = {
                        get: function(t) {
                            if (t.inputmask) {
                                if (t.inputmask.opts.autoUnmask) return t.inputmask.unmaskedvalue();
                                var e = i(t),
                                    n = t.inputmask.maskset,
                                    o = n._buffer;
                                return o = o ? o.join("") : "", e !== o ? e : ""
                            }
                            return i(t)
                        },
                        set: function(e, i) {
                            var o, r = t(e);
                            return o = n(e, i), e.inputmask && r.trigger("setvalue"), o
                        },
                        inputmaskpatch: !0
                    }
                }
            }

            function n() {
                return this.inputmask ? this.inputmask.opts.autoUnmask ? this.inputmask.unmaskedvalue() : a.call(this) !== S().join("") ? document.activeElement === this && s.clearMaskOnLostFocus ? (ht ? R(x().slice()).reverse() : R(x().slice())).join("") : a.call(this) : "" : a.call(this)
            }

            function o(e) {
                l.call(this, e), this.inputmask && t(this).trigger("setvalue")
            }

            function r(e) {
                wt.on(e, "mouseenter", function(e) {
                    var i = t(this),
                        n = this,
                        o = n.inputmask._valueGet();
                    o !== x().join("") && h() > 0 && i.trigger("setvalue")
                })
            }
            var a, l;
            e.inputmask.__valueGet || (Object.getOwnPropertyDescriptor && void 0 === e.value ? (a = function() {
                return this.textContent
            }, l = function(t) {
                this.textContent = t
            }, Object.defineProperty(e, "value", {
                get: n,
                set: o
            })) : document.__lookupGetter__ && e.__lookupGetter__("value") ? (a = e.__lookupGetter__("value"), l = e.__lookupSetter__("value"), e.__defineGetter__("value", n), e.__defineSetter__("value", o)) : (a = function() {
                return e.value
            }, l = function(t) {
                e.value = t
            }, i(e.type), r(e)), e.inputmask.__valueGet = a, e.inputmask._valueGet = function(t) {
                return ht && t !== !0 ? a.call(this.el).split("").reverse().join("") : a.call(this.el)
            }, e.inputmask.__valueSet = l, e.inputmask._valueSet = function(t, e) {
                l.call(this.el, null === t || void 0 === t ? "" : e !== !0 && ht ? t.split("").reverse().join("") : t)
            })
        }

        function N(i, n, o, r) {
            function a() {
                if (s.keepStatic) {
                    f(!0);
                    var e, n = [],
                        o = t.extend(!0, {}, p().validPositions);
                    for (e = h(); e >= 0; e--) {
                        var r = p().validPositions[e];
                        if (r && (null != r.match.fn && n.push(r.input), delete p().validPositions[e], void 0 !== r.alternation && r.locator[r.alternation] === g(e).locator[r.alternation])) break
                    }
                    if (e > -1)
                        for (; n.length > 0;) {
                            p().p = P(h());
                            var a = new t.Event("keypress");
                            a.which = n.pop().charCodeAt(0), Y.call(i, a, !0, !1, !1, p().p)
                        } else p().validPositions = t.extend(!0, {}, o)
                }
            }
            if ((s.numericInput || ht) && (n === e.keyCode.BACKSPACE ? n = e.keyCode.DELETE : n === e.keyCode.DELETE && (n = e.keyCode.BACKSPACE), ht)) {
                var l = o.end;
                o.end = o.begin, o.begin = l
            }
            n === e.keyCode.BACKSPACE && (o.end - o.begin < 1 || s.insertMode === !1) ? (o.begin = O(o.begin), void 0 === p().validPositions[o.begin] || p().validPositions[o.begin].input !== s.groupSeparator && p().validPositions[o.begin].input !== s.radixPoint || o.begin--) : n === e.keyCode.DELETE && o.begin === o.end && (o.end = E(o.end) ? o.end + 1 : P(o.end) + 1, void 0 === p().validPositions[o.begin] || p().validPositions[o.begin].input !== s.groupSeparator && p().validPositions[o.begin].input !== s.radixPoint || o.end++), v(o.begin, o.end, !1, r), r !== !0 && a();
            var c = h(o.begin);
            c < o.begin ? (-1 === c && f(), p().p = P(c)) : r !== !0 && (p().p = o.begin)
        }

        function q(n) {
            var o = this,
                r = t(o),
                a = n.keyCode,
                l = j(o);
            if (a === e.keyCode.BACKSPACE || a === e.keyCode.DELETE || d && 127 === a || n.ctrlKey && 88 === a && !i("cut")) n.preventDefault(), 88 === a && (lt = x().join("")), N(o, a, l), I(o, x(), p().p, n, lt !== x().join("")), o.inputmask._valueGet() === S().join("") ? r.trigger("cleared") : W(x()) === !0 && r.trigger("complete"), s.showTooltip && (o.title = s.tooltip || p().mask);
            else if (a === e.keyCode.END || a === e.keyCode.PAGE_DOWN) {
                n.preventDefault();
                var c = P(h());
                s.insertMode || c !== $() || n.shiftKey || c--, j(o, n.shiftKey ? l.begin : c, c, !0)
            } else a === e.keyCode.HOME && !n.shiftKey || a === e.keyCode.PAGE_UP ? (n.preventDefault(), j(o, 0, n.shiftKey ? l.begin : 0, !0)) : (s.undoOnEscape && a === e.keyCode.ESCAPE || 90 === a && n.ctrlKey) && n.altKey !== !0 ? (F(o, !0, !1, lt.split("")), r.trigger("click")) : a !== e.keyCode.INSERT || n.shiftKey || n.ctrlKey ? s.tabThrough === !0 && a === e.keyCode.TAB ? (n.shiftKey === !0 ? (null === y(l.begin).fn && (l.begin = P(l.begin)), l.end = O(l.begin, !0), l.begin = O(l.end, !0)) : (l.begin = P(l.begin, !0), l.end = P(l.begin, !0), l.end < $() && l.end--), l.begin < $() && (n.preventDefault(), j(o, l.begin, l.end))) : s.insertMode !== !1 || n.shiftKey || (a === e.keyCode.RIGHT ? setTimeout(function() {
                var t = j(o);
                j(o, t.begin)
            }, 0) : a === e.keyCode.LEFT && setTimeout(function() {
                var t = j(o);
                j(o, ht ? t.begin + 1 : t.begin - 1)
            }, 0)) : (s.insertMode = !s.insertMode, j(o, s.insertMode || l.begin !== $() ? l.begin : l.begin - 1));
            s.onKeyDown.call(this, n, x(), j(o).begin, s), gt = -1 !== t.inArray(a, s.ignorables)
        }

        function Y(i, n, o, r, a) {
            var l = this,
                c = t(l),
                u = i.which || i.charCode || i.keyCode;
            if (!(n === !0 || i.ctrlKey && i.altKey) && (i.ctrlKey || i.metaKey || gt)) return u === e.keyCode.ENTER && lt !== x().join("") && (lt = x().join(""), setTimeout(function() {
                c.trigger("change")
            }, 0)), !0;
            if (u) {
                46 === u && i.shiftKey === !1 && "," === s.radixPoint && (u = 44);
                var d, h = n ? {
                        begin: a,
                        end: a
                    } : j(l),
                    v = String.fromCharCode(u),
                    g = z(h.begin, h.end);
                g && (p().undoPositions = t.extend(!0, {}, p().validPositions), N(l, e.keyCode.DELETE, h, !0), h.begin = p().p, s.insertMode || (s.insertMode = !s.insertMode, m(h.begin, r), s.insertMode = !s.insertMode), g = !s.multi), p().writeOutBuffer = !0;
                var y = ht && !g ? h.end : h.begin,
                    b = A(y, v, r);
                if (b !== !1) {
                    if (b !== !0 && (y = void 0 !== b.pos ? b.pos : y, v = void 0 !== b.c ? b.c : v), f(!0), void 0 !== b.caret) d = b.caret;
                    else {
                        var w = p().validPositions;
                        d = !s.keepStatic && (void 0 !== w[y + 1] && k(y + 1, w[y].locator.slice(), y).length > 1 || void 0 !== w[y].alternation) ? y + 1 : P(y)
                    }
                    p().p = d
                }
                if (o !== !1) {
                    var S = this;
                    if (setTimeout(function() {
                            s.onKeyValidation.call(S, u, b, s)
                        }, 0), p().writeOutBuffer && b !== !1) {
                        var T = x();
                        I(l, T, s.numericInput && void 0 === b.caret ? O(d) : d, i, n !== !0), n !== !0 && setTimeout(function() {
                            W(T) === !0 && c.trigger("complete")
                        }, 0)
                    } else g && (p().buffer = void 0, p().validPositions = p().undoPositions)
                } else g && (p().buffer = void 0, p().validPositions = p().undoPositions);
                if (s.showTooltip && (l.title = s.tooltip || p().mask), n && t.isFunction(s.onBeforeWrite)) {
                    var _ = s.onBeforeWrite(i, x(), d, s);
                    if (_ && _.refreshFromBuffer) {
                        var E = _.refreshFromBuffer;
                        C(E === !0 ? E : E.start, E.end, _.buffer), f(!0), _.caret && (p().p = _.caret)
                    }
                }
                if (i.preventDefault(), n) return b
            }
        }

        function X(e) {
            var i = this,
                n = e.originalEvent || e,
                o = t(i),
                r = i.inputmask._valueGet(!0),
                a = j(i),
                l = r.substr(0, a.begin),
                c = r.substr(a.end, r.length);
            l === S().slice(0, a.begin).join("") && (l = ""), c === S().slice(a.end).join("") && (c = ""), window.clipboardData && window.clipboardData.getData ? r = l + window.clipboardData.getData("Text") + c : n.clipboardData && n.clipboardData.getData && (r = l + n.clipboardData.getData("text/plain") + c);
            var u = r;
            if (t.isFunction(s.onBeforePaste)) {
                if (u = s.onBeforePaste(r, s), u === !1) return e.preventDefault(), !1;
                u || (u = r)
            }
            return F(i, !1, !1, ht ? u.split("").reverse() : u.toString().split("")), I(i, x(), void 0, e, !0), o.trigger("click"), W(x()) === !0 && o.trigger("complete"), !1
        }

        function U(i) {
            var n = this,
                o = n.inputmask._valueGet();
            if (x().join("") !== o) {
                var r = j(n);
                if (o = o.replace(new RegExp("(" + e.escapeRegex(S().join("")) + ")*"), ""), u) {
                    var s = o.replace(x().join(""), "");
                    if (1 === s.length) {
                        var a = new t.Event("keypress");
                        return a.which = s.charCodeAt(0), Y.call(n, a, !0, !0, !1, p().validPositions[r.begin - 1] ? r.begin : r.begin - 1), !1
                    }
                }
                if (r.begin > o.length && (j(n, o.length), r = j(n)), x().length - o.length !== 1 || o.charAt(r.begin) === x()[r.begin] || o.charAt(r.begin + 1) === x()[r.begin] || E(r.begin)) {
                    for (var l = h() + 1, c = x().slice(l).join(""); null === o.match(e.escapeRegex(c) + "$");) c = c.slice(1);
                    o = o.replace(c, ""), o = o.split(""), F(n, !0, !1, o), W(x()) === !0 && t(n).trigger("complete")
                } else i.keyCode = e.keyCode.BACKSPACE, q.call(n, i);
                i.preventDefault()
            }
        }

        function K(t) {
            var e = t.originalEvent || t;
            lt = x().join(""), "" === ct || 0 !== e.data.indexOf(ct)
        }

        function V(e) {
            var i = this,
                n = e.originalEvent || e,
                o = x().join("");
            0 === n.data.indexOf(ct) && (f(), p().p = P(-1));
            for (var r = n.data, a = 0; a < r.length; a++) {
                var l = new t.Event("keypress");
                l.which = r.charCodeAt(a), mt = !1, gt = !1, Y.call(i, l, !0, !1, !1, p().p)
            }
            o !== x().join("") && setTimeout(function() {
                var t = p().p;
                I(i, x(), s.numericInput ? O(t) : t)
            }, 0), ct = n.data
        }

        function Q(t) {}

        function G(e) {
            var i = this,
                n = i.inputmask._valueGet();
            F(i, !0, !1, (t.isFunction(s.onBeforeMask) ? s.onBeforeMask(n, s) || n : n).split("")), lt = x().join(""), (s.clearMaskOnLostFocus || s.clearIncomplete) && i.inputmask._valueGet() === S().join("") && i.inputmask._valueSet("")
        }

        function Z(t) {
            var e = this,
                i = e.inputmask._valueGet();
            s.showMaskOnFocus && (!s.showMaskOnHover || s.showMaskOnHover && "" === i) ? e.inputmask._valueGet() !== x().join("") && I(e, x(), P(h())) : yt === !1 && j(e, P(h())), s.positionCaretOnTab === !0 && setTimeout(function() {
                j(e, P(h()))
            }, 0), lt = x().join("")
        }

        function J(t) {
            var e = this;
            if (yt = !1, s.clearMaskOnLostFocus && document.activeElement !== e) {
                var i = x().slice(),
                    n = e.inputmask._valueGet();
                n !== e.getAttribute("placeholder") && "" !== n && (-1 === h() && n === S().join("") ? i = [] : R(i), I(e, i))
            }
        }

        function tt(e) {
            function i(e) {
                if (s.radixFocus && "" !== s.radixPoint) {
                    var i = p().validPositions;
                    if (void 0 === i[e] || i[e].input === M(e)) {
                        if (e < P(-1)) return !0;
                        var n = t.inArray(s.radixPoint, x());
                        if (-1 !== n) {
                            for (var o in i)
                                if (o > n && i[o].input !== M(o)) return !1;
                            return !0
                        }
                    }
                }
                return !1
            }
            var n = this;
            if (document.activeElement === n) {
                var o = j(n);
                if (o.begin === o.end)
                    if (i(o.begin)) j(n, s.numericInput ? P(t.inArray(s.radixPoint, x())) : t.inArray(s.radixPoint, x()));
                    else {
                        var r = o.begin,
                            a = h(r),
                            l = P(a);
                        l > r ? j(n, E(r) || E(r - 1) ? r : P(r)) : ((x()[l] !== M(l) || !E(l, !0) && y(l).def === M(l)) && (l = P(l)), j(n, l))
                    }
            }
        }

        function et(t) {
            var e = this;
            setTimeout(function() {
                j(e, 0, P(h()))
            }, 0)
        }

        function it(i) {
            var n = this,
                o = t(n),
                r = j(n),
                a = i.originalEvent || i,
                l = window.clipboardData || a.clipboardData,
                c = ht ? x().slice(r.end, r.begin) : x().slice(r.begin, r.end);
            l.setData("text", ht ? c.reverse().join("") : c.join("")), document.execCommand && document.execCommand("copy"), N(n, e.keyCode.DELETE, r), I(n, x(), p().p, i, lt !== x().join("")), n.inputmask._valueGet() === S().join("") && o.trigger("cleared"), s.showTooltip && (n.title = s.tooltip || p().mask)
        }

        function nt(e) {
            var i = t(this),
                n = this;
            if (n.inputmask) {
                var o = n.inputmask._valueGet(),
                    r = x().slice();
                lt !== r.join("") && setTimeout(function() {
                    i.trigger("change"), lt = r.join("")
                }, 0), "" !== o && (s.clearMaskOnLostFocus && (-1 === h() && o === S().join("") ? r = [] : R(r)), W(r) === !1 && (setTimeout(function() {
                    i.trigger("incomplete")
                }, 0), s.clearIncomplete && (f(), r = s.clearMaskOnLostFocus ? [] : S().slice())), I(n, r, void 0, e))
            }
        }

        function ot(t) {
            var e = this;
            yt = !0, document.activeElement !== e && s.showMaskOnHover && e.inputmask._valueGet() !== x().join("") && I(e, x())
        }

        function rt(t) {
            lt !== x().join("") && dt.trigger("change"), s.clearMaskOnLostFocus && -1 === h() && ut.inputmask._valueGet && ut.inputmask._valueGet() === S().join("") && ut.inputmask._valueSet(""), s.removeMaskOnSubmit && (ut.inputmask._valueSet(ut.inputmask.unmaskedvalue(), !0), setTimeout(function() {
                I(ut, x())
            }, 0))
        }

        function st(t) {
            setTimeout(function() {
                dt.trigger("setvalue")
            }, 0)
        }

        function at(e) {
            if (ut = e, dt = t(ut), s.showTooltip && (ut.title = s.tooltip || p().mask), ("rtl" === ut.dir || s.rightAlign) && (ut.style.textAlign = "right"), ("rtl" === ut.dir || s.numericInput) && (ut.dir = "ltr", ut.removeAttribute("dir"), ut.inputmask.isRTL = !0, ht = !0), wt.off(ut), B(ut), n(ut, s) && (wt.on(ut, "submit", rt), wt.on(ut, "reset", st), wt.on(ut, "mouseenter", ot), wt.on(ut, "blur", nt), wt.on(ut, "focus", Z), wt.on(ut, "mouseleave", J), wt.on(ut, "click", tt), wt.on(ut, "dblclick", et), wt.on(ut, "paste", X), wt.on(ut, "dragdrop", X), wt.on(ut, "drop", X), wt.on(ut, "cut", it), wt.on(ut, "complete", s.oncomplete), wt.on(ut, "incomplete", s.onincomplete), wt.on(ut, "cleared", s.oncleared), wt.on(ut, "keydown", q), wt.on(ut, "keypress", Y), wt.on(ut, "input", U), c || (wt.on(ut, "compositionstart", K), wt.on(ut, "compositionupdate", V), wt.on(ut, "compositionend", Q))), wt.on(ut, "setvalue", G), "" !== ut.inputmask._valueGet() || s.clearMaskOnLostFocus === !1) {
                var i = t.isFunction(s.onBeforeMask) ? s.onBeforeMask(ut.inputmask._valueGet(), s) || ut.inputmask._valueGet() : ut.inputmask._valueGet();
                F(ut, !0, !1, i.split(""));
                var o = x().slice();
                lt = o.join(""), W(o) === !1 && s.clearIncomplete && f(), s.clearMaskOnLostFocus && (o.join("") === S().join("") ? o = [] : R(o)), I(ut, o), document.activeElement === ut && j(ut, P(h()))
            }
        }
        var lt, ct, ut, dt, pt, ft, ht = !1,
            mt = !1,
            vt = !1,
            gt = !1,
            yt = !0,
            bt = !1,
            wt = {
                on: function(i, n, o) {
                    var r = function(i) {
                        if (void 0 === this.inputmask && "FORM" !== this.nodeName) {
                            var n = t.data(this, "_inputmask_opts");
                            n ? new e(n).mask(this) : wt.off(this)
                        } else {
                            if ("setvalue" === i.type || !(this.disabled || this.readOnly && !("keydown" === i.type && i.ctrlKey && 67 === i.keyCode || s.tabThrough === !1 && i.keyCode === e.keyCode.TAB))) {
                                switch (i.type) {
                                    case "input":
                                        if (vt === !0 || bt === !0) return vt = bt, i.preventDefault();
                                        break;
                                    case "keydown":
                                        mt = !1, vt = !1, bt = !1;
                                        break;
                                    case "keypress":
                                        if (mt === !0) return i.preventDefault();
                                        mt = !0;
                                        break;
                                    case "compositionstart":
                                        bt = !0;
                                        break;
                                    case "compositionupdate":
                                        vt = !0;
                                        break;
                                    case "compositionend":
                                        bt = !1;
                                        break;
                                    case "cut":
                                        vt = !0;
                                        break;
                                    case "click":
                                        if (u) {
                                            var r = this;
                                            return setTimeout(function() {
                                                o.apply(r, arguments)
                                            }, 0), !1
                                        }
                                }
                                return o.apply(this, arguments)
                            }
                            i.preventDefault()
                        }
                    };
                    i.inputmask.events[n] = i.inputmask.events[n] || [], i.inputmask.events[n].push(r), -1 !== t.inArray(n, ["submit", "reset"]) ? null != i.form && t(i.form).on(n, r) : t(i).on(n, r)
                },
                off: function(e, i) {
                    if (e.inputmask && e.inputmask.events) {
                        var n;
                        i ? (n = [], n[i] = e.inputmask.events[i]) : n = e.inputmask.events, t.each(n, function(i, n) {
                            for (; n.length > 0;) {
                                var o = n.pop(); - 1 !== t.inArray(i, ["submit", "reset"]) ? null != e.form && t(e.form).off(i, o) : t(e).off(i, o)
                            }
                            delete e.inputmask.events[i]
                        })
                    }
                }
            };
        if (void 0 !== o) switch (o.action) {
            case "isComplete":
                return ut = o.el, W(x());
            case "unmaskedvalue":
                return ut = o.el, void 0 !== ut && void 0 !== ut.inputmask ? (r = ut.inputmask.maskset, s = ut.inputmask.opts, ht = ut.inputmask.isRTL) : (ft = o.value, s.numericInput && (ht = !0), ft = (t.isFunction(s.onBeforeMask) ? s.onBeforeMask(ft, s) || ft : ft).split(""), F(void 0, !1, !1, ht ? ft.reverse() : ft), t.isFunction(s.onBeforeWrite) && s.onBeforeWrite(void 0, x(), 0, s)), D(ut);
            case "mask":
                ut = o.el, r = ut.inputmask.maskset, s = ut.inputmask.opts, ht = ut.inputmask.isRTL, lt = x().join(""), at(ut);
                break;
            case "format":
                return s.numericInput && (ht = !0), ft = (t.isFunction(s.onBeforeMask) ? s.onBeforeMask(o.value, s) || o.value : o.value).split(""), F(void 0, !1, !1, ht ? ft.reverse() : ft), t.isFunction(s.onBeforeWrite) && s.onBeforeWrite(void 0, x(), 0, s), o.metadata ? {
                    value: ht ? x().slice().reverse().join("") : x().join(""),
                    metadata: a({
                        action: "getmetadata"
                    }, r, s)
                } : ht ? x().slice().reverse().join("") : x().join("");
            case "isValid":
                s.numericInput && (ht = !0), o.value ? (ft = o.value.split(""), F(void 0, !1, !0, ht ? ft.reverse() : ft)) : o.value = x().join("");
                for (var kt = x(), St = H(), xt = kt.length - 1; xt > St && !E(xt); xt--);
                return kt.splice(St, xt + 1 - St), W(kt) && o.value === x().join("");
            case "getemptymask":
                return S();
            case "remove":
                ut = o.el, dt = t(ut), r = ut.inputmask.maskset, s = ut.inputmask.opts, ut.inputmask._valueSet(D(ut)), wt.off(ut);
                var Ct;
                Object.getOwnPropertyDescriptor && (Ct = Object.getOwnPropertyDescriptor(ut, "value")), Ct && Ct.get ? ut.inputmask.__valueGet && Object.defineProperty(ut, "value", {
                    get: ut.inputmask.__valueGet,
                    set: ut.inputmask.__valueSet
                }) : document.__lookupGetter__ && ut.__lookupGetter__("value") && ut.inputmask.__valueGet && (ut.__defineGetter__("value", ut.inputmask.__valueGet), ut.__defineSetter__("value", ut.inputmask.__valueSet)), ut.inputmask = void 0;
                break;
            case "getmetadata":
                if (t.isArray(r.metadata)) {
                    for (var Tt, _t = h(), At = _t; At >= 0; At--)
                        if (p().validPositions[At] && void 0 !== p().validPositions[At].alternation) {
                            Tt = p().validPositions[At].alternation;
                            break
                        }
                    return void 0 !== Tt ? r.metadata[p().validPositions[_t].locator[Tt]] : r.metadata[0]
                }
                return r.metadata
        }
    }
    e.prototype = {
        defaults: {
            placeholder: "_",
            optionalmarker: {
                start: "[",
                end: "]"
            },
            quantifiermarker: {
                start: "{",
                end: "}"
            },
            groupmarker: {
                start: "(",
                end: ")"
            },
            alternatormarker: "|",
            escapeChar: "\\",
            mask: null,
            oncomplete: t.noop,
            onincomplete: t.noop,
            oncleared: t.noop,
            repeat: 0,
            greedy: !0,
            autoUnmask: !1,
            removeMaskOnSubmit: !1,
            clearMaskOnLostFocus: !0,
            insertMode: !0,
            clearIncomplete: !1,
            aliases: {},
            alias: null,
            onKeyDown: t.noop,
            onBeforeMask: null,
            onBeforePaste: function(e, i) {
                return t.isFunction(i.onBeforeMask) ? i.onBeforeMask(e, i) : e
            },
            onBeforeWrite: null,
            onUnMask: null,
            showMaskOnFocus: !0,
            showMaskOnHover: !0,
            onKeyValidation: t.noop,
            skipOptionalPartCharacter: " ",
            showTooltip: !1,
            tooltip: void 0,
            numericInput: !1,
            rightAlign: !1,
            undoOnEscape: !0,
            radixPoint: "",
            groupSeparator: "",
            radixFocus: !1,
            nojumps: !1,
            nojumpsThreshold: 0,
            keepStatic: null,
            positionCaretOnTab: !1,
            tabThrough: !1,
            supportsInputType: ["text", "tel", "password"],
            definitions: {
                9: {
                    validator: "[0-9]",
                    cardinality: 1,
                    definitionSymbol: "*"
                },
                a: {
                    validator: "[A-Za-zА-яЁёÀ-ÿµ]",
                    cardinality: 1,
                    definitionSymbol: "*"
                },
                "*": {
                    validator: "[0-9A-Za-zА-яЁёÀ-ÿµ]",
                    cardinality: 1
                }
            },
            ignorables: [8, 9, 13, 19, 27, 33, 34, 35, 36, 37, 38, 39, 40, 45, 46, 93, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123],
            isComplete: null,
            canClearPosition: t.noop,
            postValidation: null,
            staticDefinitionSymbol: void 0,
            jitMasking: !1
        },
        masksCache: {},
        mask: function(i) {
            var n = this;
            return "string" == typeof i && (i = document.getElementById(i) || document.querySelectorAll(i)), i = i.nodeName ? [i] : i, t.each(i, function(i, o) {
                var l = t.extend(!0, {}, n.opts);
                r(o, l, t.extend(!0, {}, n.userOptions));
                var c = s(l, n.noMasksCache);
                void 0 !== c && (void 0 !== o.inputmask && o.inputmask.remove(), o.inputmask = new e, o.inputmask.opts = l, o.inputmask.noMasksCache = n.noMasksCache, o.inputmask.userOptions = t.extend(!0, {}, n.userOptions), o.inputmask.el = o, o.inputmask.maskset = c, o.inputmask.isRTL = !1, t.data(o, "_inputmask_opts", l), a({
                    action: "mask",
                    el: o
                }))
            }), i && i[0] ? i[0].inputmask || this : this
        },
        option: function(e) {
            return "string" == typeof e ? this.opts[e] : "object" == typeof e ? (t.extend(this.opts, e), t.extend(this.userOptions, e), this.el && (void 0 !== e.mask || void 0 !== e.alias ? this.mask(this.el) : (t.data(this.el, "_inputmask_opts", this.opts), a({
                action: "mask",
                el: this.el
            }))), this) : void 0
        },
        unmaskedvalue: function(t) {
            return a({
                action: "unmaskedvalue",
                el: this.el,
                value: t
            }, this.el && this.el.inputmask ? this.el.inputmask.maskset : s(this.opts, this.noMasksCache), this.opts)
        },
        remove: function() {
            return this.el ? (a({
                action: "remove",
                el: this.el
            }), this.el.inputmask = void 0, this.el) : void 0
        },
        getemptymask: function() {
            return a({
                action: "getemptymask"
            }, this.maskset || s(this.opts, this.noMasksCache), this.opts)
        },
        hasMaskedValue: function() {
            return !this.opts.autoUnmask
        },
        isComplete: function() {
            return a({
                action: "isComplete",
                el: this.el
            }, this.maskset || s(this.opts, this.noMasksCache), this.opts)
        },
        getmetadata: function() {
            return a({
                action: "getmetadata"
            }, this.maskset || s(this.opts, this.noMasksCache), this.opts)
        },
        isValid: function(t) {
            return a({
                action: "isValid",
                value: t
            }, this.maskset || s(this.opts, this.noMasksCache), this.opts)
        },
        format: function(t, e) {
            return a({
                action: "format",
                value: t,
                metadata: e
            }, this.maskset || s(this.opts, this.noMasksCache), this.opts)
        }
    }, e.extendDefaults = function(i) {
        t.extend(!0, e.prototype.defaults, i)
    }, e.extendDefinitions = function(i) {
        t.extend(!0, e.prototype.defaults.definitions, i)
    }, e.extendAliases = function(i) {
        t.extend(!0, e.prototype.defaults.aliases, i)
    }, e.format = function(t, i, n) {
        return e(i).format(t, n)
    }, e.unmask = function(t, i) {
        return e(i).unmaskedvalue(t)
    }, e.isValid = function(t, i) {
        return e(i).isValid(t)
    }, e.remove = function(e) {
        t.each(e, function(t, e) {
            e.inputmask && e.inputmask.remove()
        })
    }, e.escapeRegex = function(t) {
        var e = ["/", ".", "*", "+", "?", "|", "(", ")", "[", "]", "{", "}", "\\", "$", "^"];
        return t.replace(new RegExp("(\\" + e.join("|\\") + ")", "gim"), "\\$1")
    }, e.keyCode = {
        ALT: 18,
        BACKSPACE: 8,
        CAPS_LOCK: 20,
        COMMA: 188,
        COMMAND: 91,
        COMMAND_LEFT: 91,
        COMMAND_RIGHT: 93,
        CONTROL: 17,
        DELETE: 46,
        DOWN: 40,
        END: 35,
        ENTER: 13,
        ESCAPE: 27,
        HOME: 36,
        INSERT: 45,
        LEFT: 37,
        MENU: 93,
        NUMPAD_ADD: 107,
        NUMPAD_DECIMAL: 110,
        NUMPAD_DIVIDE: 111,
        NUMPAD_ENTER: 108,
        NUMPAD_MULTIPLY: 106,
        NUMPAD_SUBTRACT: 109,
        PAGE_DOWN: 34,
        PAGE_UP: 33,
        PERIOD: 190,
        RIGHT: 39,
        SHIFT: 16,
        SPACE: 32,
        TAB: 9,
        UP: 38,
        WINDOWS: 91
    };
    var l = navigator.userAgent,
        c = /mobile/i.test(l),
        u = /iemobile/i.test(l),
        d = /iphone/i.test(l) && !u;
    return /android.*safari.*/i.test(l) && !u, window.Inputmask = e, e
}), ! function(t) {
    "function" == typeof define && define.amd ? define(["jquery", "inputmask"], t) : "object" == typeof exports ? module.exports = t(require("jquery"), require("./inputmask")) : t(window.dependencyLib || jQuery, window.Inputmask)
}(function(t, e) {
    return e.extendAliases({
        phone: {
            url: "phone-codes/phone-codes.js",
            countrycode: "",
            phoneCodeCache: {},
            mask: function(e) {
                if (void 0 === e.phoneCodeCache[e.url]) {
                    var i = [];
                    e.definitions["#"] = e.definitions[9], t.ajax({
                        url: e.url,
                        async: !1,
                        type: "get",
                        dataType: "json",
                        success: function(t) {
                            i = t
                        },
                        error: function(t, i, n) {
                            alert(n + " - " + e.url)
                        }
                    }), e.phoneCodeCache[e.url] = i.sort(function(t, e) {
                        return (t.mask || t) < (e.mask || e) ? -1 : 1
                    })
                }
                return e.phoneCodeCache[e.url]
            },
            keepStatic: !1,
            nojumps: !0,
            nojumpsThreshold: 1,
            onBeforeMask: function(t, e) {
                var i = t.replace(/^0{1,2}/, "").replace(/[\s]/g, "");
                return (i.indexOf(e.countrycode) > 1 || -1 === i.indexOf(e.countrycode)) && (i = "+" + e.countrycode + i), i
            }
        },
        phonebe: {
            alias: "phone",
            url: "phone-codes/phone-be.js",
            countrycode: "32",
            nojumpsThreshold: 4
        }
    }), e
}), ! function(t) {
    "use strict";

    function e(t, e) {
        var n, o, r, l, c, u, d = e.window,
            p = 0 !== a(),
            f = e.useLayoutViewport && "Width" === t,
            m = p || !s() || f,
            g = m ? d.document.documentElement["client" + t] : h(t, d);
        return e.useLayoutViewport && !m && (o = g, r = i(d, {
            asRange: !0
        }), g = Math.round(o * r.calculated), v() || (n = d.document.documentElement.clientHeight, c = (o - 1) * r.min, u = (o + 1) * r.max, l = n + 3 >= g && g >= n - 3 || n >= c && u >= n && n + 30 > u, l && (g = n))), g
    }

    function i(t, e) {
        var i, n, o = e && e.asRange,
            r = {
                calculated: 1,
                min: 1,
                max: 1
            },
            l = 0 !== a() || !s();
        return l || (t || (t = window), i = t.document.documentElement.clientWidth, n = f(t), r.calculated = i / n, o && (v() ? r.min = r.max = r.calculated : (r.min = i / (n + 1), r.max = i / (n - 1)))), o ? r : r.calculated
    }

    function n(t) {
        var e, i, n, s = window,
            a = !0;
        return t && t.length && (t = Array.prototype.slice.call(t), e = g(t[0]), e || (t[0] = r(t[0])), i = !e && t[0], i || (t[1] = r(t[1])), n = !i && t[1], e ? (s = t[0], n && t[1].viewport && (a = o(t[1].viewport))) : i ? (t[0].viewport && (a = o(t[0].viewport)), g(t[1]) && (s = t[1])) : !t[0] && t[1] && (n && t[1].viewport ? a = o(t[1].viewport) : g(t[1]) && (s = t[1]))), {
            window: s,
            useVisualViewport: a,
            useLayoutViewport: !a
        }
    }

    function o(t) {
        var e = y(t) && t.toLowerCase();
        if (t && !e) throw new Error("Invalid viewport option: " + t);
        if (e && "visual" !== e && "layout" !== e) throw new Error("Invalid viewport name: " + t);
        return "visual" === e
    }

    function r(t) {
        return y(t) && "" !== t ? {
            viewport: t
        } : t
    }

    function s() {
        return void 0 === k && (k = f() > 10), k
    }

    function a() {
        var t;
        return void 0 === w && (t = document.createElement("div"), t.style.cssText = "width: 100px; height: 100px; overflow: scroll; position: absolute; top: -500px; left: -500px; margin: 0px; padding: 0px; border: none;", document.body.appendChild(t), w = t.offsetWidth - t.clientWidth, document.body.removeChild(t)), w
    }

    function l() {
        var t, e, i, n, o = c(),
            r = o && o.contentDocument || document,
            s = r.body,
            a = r !== document;
        e = r.createElement("div"), e.style.cssText = "width: 1px; height: 1px; position: relative; top: 0px; left: 32000px;", a || (t = u()), i = s.scrollWidth, s.appendChild(e), n = i !== s.scrollWidth, s.removeChild(e), a || d(t), x = n ? "documentElement" : "body", document.body.removeChild(o)
    }

    function c() {
        var t = document.createElement("iframe"),
            e = document.body;
        return t.style.cssText = "position: absolute; top: -600px; left: -600px; width: 500px; height: 500px; margin: 0px; padding: 0px; border: none; display: block;", t.frameborder = "0", e.appendChild(t), t.src = "about:blank", t.contentDocument ? (t.contentDocument.write('<!DOCTYPE html><html><head><meta charset="UTF-8"><title></title><style type="text/css">html, body { overflow: hidden; }</style></head><body></body></html>'), t) : void 0
    }

    function u() {
        var t, e, i = document.documentElement,
            n = document.body,
            o = C ? window.getComputedStyle(i, null) : i.currentStyle,
            r = C ? window.getComputedStyle(n, null) : n.currentStyle,
            s = (o.overflowX || o.overflow || "visible").toLowerCase(),
            a = (r.overflowX || r.overflow || "visible").toLowerCase(),
            l = "hidden" !== a,
            c = "visible" === s,
            u = {
                documentElement: {
                    modified: c
                },
                body: {
                    modified: l
                }
            };
        return c && (t = i.style, u.documentElement.styleOverflowX = t.overflowX, t.overflowX = "auto"), l && (e = n.style, u.body.styleOverflowX = e.overflowX, e.overflowX = "hidden"), u
    }

    function d(t) {
        t.documentElement.modified && (document.documentElement.style.overflowX = t.documentElement.styleOverflowX), t.body.modified && (document.body.style.overflowX = t.body.styleOverflowX)
    }

    function p(t, e) {
        var i = e.documentElement;
        return Math.max(i.body["scroll" + t], e["scroll" + t], i.body["offset" + t], e["offset" + t], e["client" + t])
    }

    function f(t) {
        return h("Width", t)
    }

    function h(t, e) {
        var i = (e || window)["inner" + t];
        return i && m(i), i
    }

    function m(t) {
        !S && b(t) && (S = !0)
    }

    function v() {
        return !!S
    }

    function g(t) {
        return null != t && t.window == t
    }

    function y(t) {
        return "string" == typeof t || t && "object" == typeof t && "[object String]" === Object.prototype.toString.call(t) || !1
    }

    function b(t) {
        return t === +t && t !== (0 | t)
    }
    var w, k, S, x, C = !!window.getComputedStyle;
    if (t.documentWidth = function(t) {
            var e;
            t || (t = document);
            try {
                void 0 === x && l(), e = t[x].scrollWidth
            } catch (i) {
                e = p("Width", t)
            }
            return e
        }, t.documentHeight = function(t) {
            var e;
            t || (t = document);
            try {
                void 0 === x && l(), e = t[x].scrollHeight
            } catch (i) {
                e = p("Height", t)
            }
            return e
        }, t.windowWidth = function(t, i) {
            var o = n(arguments);
            return e("Width", o)
        }, t.windowHeight = function(t, i) {
            var o = n(arguments);
            return e("Height", o)
        }, t.pinchZoomFactor = function(t) {
            return i(t)
        }, t.scrollbarWidth = a, "function" == typeof t) try {
        t(function() {
            void 0 === x && l(), a()
        })
    } catch (t) {}
}("undefined" != typeof jQuery ? jQuery : "undefined" != typeof Zepto ? Zepto : $), ! function(t) {
    "function" == typeof define && define.amd ? define(["jquery", "inputmask"], t) : "object" == typeof exports ? module.exports = t(require("jquery"), require("./inputmask")) : t(jQuery, window.Inputmask)
}(function(t, e) {
    return void 0 === t.fn.inputmask && (t.fn.inputmask = function(i, n) {
        var o, r = this[0];
        if (n = n || {}, "string" == typeof i) switch (i) {
            case "unmaskedvalue":
                return r && r.inputmask ? r.inputmask.unmaskedvalue() : t(r).val();
            case "remove":
                return this.each(function() {
                    this.inputmask && this.inputmask.remove()
                });
            case "getemptymask":
                return r && r.inputmask ? r.inputmask.getemptymask() : "";
            case "hasMaskedValue":
                return !(!r || !r.inputmask) && r.inputmask.hasMaskedValue();
            case "isComplete":
                return !r || !r.inputmask || r.inputmask.isComplete();
            case "getmetadata":
                return r && r.inputmask ? r.inputmask.getmetadata() : void 0;
            case "setvalue":
                t(r).val(n), r && void 0 !== r.inputmask && t(r).triggerHandler("setvalue");
                break;
            case "option":
                if ("string" != typeof n) return this.each(function() {
                    return void 0 !== this.inputmask ? this.inputmask.option(n) : void 0
                });
                if (r && void 0 !== r.inputmask) return r.inputmask.option(n);
                break;
            default:
                return n.alias = i, o = new e(n), this.each(function() {
                    o.mask(this)
                })
        } else {
            if ("object" == typeof i) return o = new e(i), void 0 === i.mask && void 0 === i.alias ? this.each(function() {
                return void 0 !== this.inputmask ? this.inputmask.option(i) : void o.mask(this)
            }) : this.each(function() {
                o.mask(this)
            });
            if (void 0 === i) return this.each(function() {
                o = new e(n), o.mask(this)
            })
        }
    }), t.fn.inputmask
}), ! function(t) {
    "function" == typeof define && define.amd ? define(["jquery"], t) : t("object" == typeof exports ? require("jquery") : window.jQuery || window.Zepto)
}(function(t) {
    var e, i, n, o, r, s, a = "Close",
        l = "BeforeClose",
        c = "AfterClose",
        u = "BeforeAppend",
        d = "MarkupParse",
        p = "Open",
        f = "Change",
        h = "mfp",
        m = "." + h,
        v = "mfp-ready",
        g = "mfp-removing",
        y = "mfp-prevent-close",
        b = function() {},
        w = !!window.jQuery,
        k = t(window),
        S = function(t, i) {
            e.ev.on(h + t + m, i)
        },
        x = function(e, i, n, o) {
            var r = document.createElement("div");
            return r.className = "mfp-" + e, n && (r.innerHTML = n), o ? i && i.appendChild(r) : (r = t(r), i && r.appendTo(i)), r
        },
        C = function(i, n) {
            e.ev.triggerHandler(h + i, n), e.st.callbacks && (i = i.charAt(0).toLowerCase() + i.slice(1), e.st.callbacks[i] && e.st.callbacks[i].apply(e, t.isArray(n) ? n : [n]))
        },
        T = function(i) {
            return i === s && e.currTemplate.closeBtn || (e.currTemplate.closeBtn = t(e.st.closeMarkup.replace("%title%", e.st.tClose)), s = i), e.currTemplate.closeBtn
        },
        _ = function() {
            t.magnificPopup.instance || (e = new b, e.init(), t.magnificPopup.instance = e)
        },
        A = function() {
            var t = document.createElement("p").style,
                e = ["ms", "O", "Moz", "Webkit"];
            if (void 0 !== t.transition) return !0;
            for (; e.length;)
                if (e.pop() + "Transition" in t) return !0;
            return !1
        };
    b.prototype = {
        constructor: b,
        init: function() {
            var i = navigator.appVersion;
            e.isLowIE = e.isIE8 = document.all && !document.addEventListener, e.isAndroid = /android/gi.test(i), e.isIOS = /iphone|ipad|ipod/gi.test(i), e.supportsTransition = A(), e.probablyMobile = e.isAndroid || e.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent), n = t(document), e.popupsCache = {}
        },
        open: function(i) {
            var o;
            if (i.isObj === !1) {
                e.items = i.items.toArray(), e.index = 0;
                var s, a = i.items;
                for (o = 0; o < a.length; o++)
                    if (s = a[o], s.parsed && (s = s.el[0]), s === i.el[0]) {
                        e.index = o;
                        break
                    }
            } else e.items = t.isArray(i.items) ? i.items : [i.items], e.index = i.index || 0;
            if (e.isOpen) return void e.updateItemHTML();
            e.types = [], r = "", i.mainEl && i.mainEl.length ? e.ev = i.mainEl.eq(0) : e.ev = n, i.key ? (e.popupsCache[i.key] || (e.popupsCache[i.key] = {}), e.currTemplate = e.popupsCache[i.key]) : e.currTemplate = {}, e.st = t.extend(!0, {}, t.magnificPopup.defaults, i), e.fixedContentPos = "auto" === e.st.fixedContentPos ? !e.probablyMobile : e.st.fixedContentPos, e.st.modal && (e.st.closeOnContentClick = !1, e.st.closeOnBgClick = !1, e.st.showCloseBtn = !1, e.st.enableEscapeKey = !1), e.bgOverlay || (e.bgOverlay = x("bg").on("click" + m, function() {
                e.close()
            }), e.wrap = x("wrap").attr("tabindex", -1).on("click" + m, function(t) {
                e._checkIfClose(t.target) && e.close()
            }), e.container = x("container", e.wrap)), e.contentContainer = x("content"), e.st.preloader && (e.preloader = x("preloader", e.container, e.st.tLoading));
            var l = t.magnificPopup.modules;
            for (o = 0; o < l.length; o++) {
                var c = l[o];
                c = c.charAt(0).toUpperCase() + c.slice(1), e["init" + c].call(e)
            }
            C("BeforeOpen"), e.st.showCloseBtn && (e.st.closeBtnInside ? (S(d, function(t, e, i, n) {
                i.close_replaceWith = T(n.type)
            }), r += " mfp-close-btn-in") : e.wrap.append(T())), e.st.alignTop && (r += " mfp-align-top"), e.fixedContentPos ? e.wrap.css({
                overflow: e.st.overflowY,
                overflowX: "hidden",
                overflowY: e.st.overflowY
            }) : e.wrap.css({
                top: k.scrollTop(),
                position: "absolute"
            }), (e.st.fixedBgPos === !1 || "auto" === e.st.fixedBgPos && !e.fixedContentPos) && e.bgOverlay.css({
                height: n.height(),
                position: "absolute"
            }), e.st.enableEscapeKey && n.on("keyup" + m, function(t) {
                27 === t.keyCode && e.close()
            }), k.on("resize" + m, function() {
                e.updateSize()
            }), e.st.closeOnContentClick || (r += " mfp-auto-cursor"), r && e.wrap.addClass(r);
            var u = e.wH = k.height(),
                f = {};
            if (e.fixedContentPos && e._hasScrollBar(u)) {
                var h = e._getScrollbarSize();
                h && (f.marginRight = h)
            }
            e.fixedContentPos && (e.isIE7 ? t("body, html").css("overflow", "hidden") : f.overflow = "hidden");
            var g = e.st.mainClass;
            return e.isIE7 && (g += " mfp-ie7"), g && e._addClassToMFP(g), e.updateItemHTML(), C("BuildControls"), t("html").css(f), e.bgOverlay.add(e.wrap).prependTo(e.st.prependTo || t(document.body)), e._lastFocusedEl = document.activeElement, setTimeout(function() {
                e.content ? (e._addClassToMFP(v), e._setFocus()) : e.bgOverlay.addClass(v), n.on("focusin" + m, e._onFocusIn)
            }, 16), e.isOpen = !0, e.updateSize(u), C(p), i
        },
        close: function() {
            e.isOpen && (C(l), e.isOpen = !1, e.st.removalDelay && !e.isLowIE && e.supportsTransition ? (e._addClassToMFP(g), setTimeout(function() {
                e._close()
            }, e.st.removalDelay)) : e._close())
        },
        _close: function() {
            C(a);
            var i = g + " " + v + " ";
            if (e.bgOverlay.detach(), e.wrap.detach(), e.container.empty(), e.st.mainClass && (i += e.st.mainClass + " "), e._removeClassFromMFP(i), e.fixedContentPos) {
                var o = {
                    marginRight: ""
                };
                e.isIE7 ? t("body, html").css("overflow", "") : o.overflow = "", t("html").css(o)
            }
            n.off("keyup" + m + " focusin" + m), e.ev.off(m), e.wrap.attr("class", "mfp-wrap").removeAttr("style"), e.bgOverlay.attr("class", "mfp-bg"), e.container.attr("class", "mfp-container"), !e.st.showCloseBtn || e.st.closeBtnInside && e.currTemplate[e.currItem.type] !== !0 || e.currTemplate.closeBtn && e.currTemplate.closeBtn.detach(), e.st.autoFocusLast && e._lastFocusedEl && t(e._lastFocusedEl).focus(), e.currItem = null, e.content = null, e.currTemplate = null, e.prevHeight = 0, C(c)
        },
        updateSize: function(t) {
            if (e.isIOS) {
                var i = document.documentElement.clientWidth / window.innerWidth,
                    n = window.innerHeight * i;
                e.wrap.css("height", n), e.wH = n
            } else e.wH = t || k.height();
            e.fixedContentPos || e.wrap.css("height", e.wH), C("Resize")
        },
        updateItemHTML: function() {
            var i = e.items[e.index];
            e.contentContainer.detach(), e.content && e.content.detach(), i.parsed || (i = e.parseEl(e.index));
            var n = i.type;
            if (C("BeforeChange", [e.currItem ? e.currItem.type : "", n]), e.currItem = i, !e.currTemplate[n]) {
                var r = !!e.st[n] && e.st[n].markup;
                C("FirstMarkupParse", r), r ? e.currTemplate[n] = t(r) : e.currTemplate[n] = !0
            }
            o && o !== i.type && e.container.removeClass("mfp-" + o + "-holder");
            var s = e["get" + n.charAt(0).toUpperCase() + n.slice(1)](i, e.currTemplate[n]);
            e.appendContent(s, n), i.preloaded = !0, C(f, i), o = i.type, e.container.prepend(e.contentContainer), C("AfterChange")
        },
        appendContent: function(t, i) {
            e.content = t, t ? e.st.showCloseBtn && e.st.closeBtnInside && e.currTemplate[i] === !0 ? e.content.find(".mfp-close").length || e.content.append(T()) : e.content = t : e.content = "", C(u), e.container.addClass("mfp-" + i + "-holder"), e.contentContainer.append(e.content)
        },
        parseEl: function(i) {
            var n, o = e.items[i];
            if (o.tagName ? o = {
                    el: t(o)
                } : (n = o.type, o = {
                    data: o,
                    src: o.src
                }), o.el) {
                for (var r = e.types, s = 0; s < r.length; s++)
                    if (o.el.hasClass("mfp-" + r[s])) {
                        n = r[s];
                        break
                    }
                o.src = o.el.attr("data-mfp-src"), o.src || (o.src = o.el.attr("href"))
            }
            return o.type = n || e.st.type || "inline", o.index = i, o.parsed = !0, e.items[i] = o, C("ElementParse", o), e.items[i]
        },
        addGroup: function(t, i) {
            var n = function(n) {
                n.mfpEl = this, e._openClick(n, t, i)
            };
            i || (i = {});
            var o = "click.magnificPopup";
            i.mainEl = t, i.items ? (i.isObj = !0, t.off(o).on(o, n)) : (i.isObj = !1, i.delegate ? t.off(o).on(o, i.delegate, n) : (i.items = t, t.off(o).on(o, n)))
        },
        _openClick: function(i, n, o) {
            var r = void 0 !== o.midClick ? o.midClick : t.magnificPopup.defaults.midClick;
            if (r || !(2 === i.which || i.ctrlKey || i.metaKey || i.altKey || i.shiftKey)) {
                var s = void 0 !== o.disableOn ? o.disableOn : t.magnificPopup.defaults.disableOn;
                if (s)
                    if (t.isFunction(s)) {
                        if (!s.call(e)) return !0
                    } else if (k.width() < s) return !0;
                i.type && (i.preventDefault(), e.isOpen && i.stopPropagation()), o.el = t(i.mfpEl), o.delegate && (o.items = n.find(o.delegate)), e.open(o)
            }
        },
        updateStatus: function(t, n) {
            if (e.preloader) {
                i !== t && e.container.removeClass("mfp-s-" + i), n || "loading" !== t || (n = e.st.tLoading);
                var o = {
                    status: t,
                    text: n
                };
                C("UpdateStatus", o), t = o.status, n = o.text, e.preloader.html(n), e.preloader.find("a").on("click", function(t) {
                    t.stopImmediatePropagation()
                }), e.container.addClass("mfp-s-" + t), i = t
            }
        },
        _checkIfClose: function(i) {
            if (!t(i).hasClass(y)) {
                var n = e.st.closeOnContentClick,
                    o = e.st.closeOnBgClick;
                if (n && o) return !0;
                if (!e.content || t(i).hasClass("mfp-close") || e.preloader && i === e.preloader[0]) return !0;
                if (i === e.content[0] || t.contains(e.content[0], i)) {
                    if (n) return !0
                } else if (o && t.contains(document, i)) return !0;
                return !1
            }
        },
        _addClassToMFP: function(t) {
            e.bgOverlay.addClass(t), e.wrap.addClass(t)
        },
        _removeClassFromMFP: function(t) {
            this.bgOverlay.removeClass(t), e.wrap.removeClass(t)
        },
        _hasScrollBar: function(t) {
            return (e.isIE7 ? n.height() : document.body.scrollHeight) > (t || k.height())
        },
        _setFocus: function() {
            (e.st.focus ? e.content.find(e.st.focus).eq(0) : e.wrap).focus()
        },
        _onFocusIn: function(i) {
            return i.target === e.wrap[0] || t.contains(e.wrap[0], i.target) ? void 0 : (e._setFocus(), !1)
        },
        _parseMarkup: function(e, i, n) {
            var o;
            n.data && (i = t.extend(n.data, i)), C(d, [e, i, n]), t.each(i, function(i, n) {
                if (void 0 === n || n === !1) return !0;
                if (o = i.split("_"), o.length > 1) {
                    var r = e.find(m + "-" + o[0]);
                    if (r.length > 0) {
                        var s = o[1];
                        "replaceWith" === s ? r[0] !== n[0] && r.replaceWith(n) : "img" === s ? r.is("img") ? r.attr("src", n) : r.replaceWith(t("<img>").attr("src", n).attr("class", r.attr("class"))) : r.attr(o[1], n)
                    }
                } else e.find(m + "-" + i).html(n)
            })
        },
        _getScrollbarSize: function() {
            if (void 0 === e.scrollbarSize) {
                var t = document.createElement("div");
                t.style.cssText = "width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;", document.body.appendChild(t), e.scrollbarSize = t.offsetWidth - t.clientWidth, document.body.removeChild(t)
            }
            return e.scrollbarSize
        }
    }, t.magnificPopup = {
        instance: null,
        proto: b.prototype,
        modules: [],
        open: function(e, i) {
            return _(), e = e ? t.extend(!0, {}, e) : {}, e.isObj = !0, e.index = i || 0, this.instance.open(e)
        },
        close: function() {
            return t.magnificPopup.instance && t.magnificPopup.instance.close()
        },
        registerModule: function(e, i) {
            i.options && (t.magnificPopup.defaults[e] = i.options), t.extend(this.proto, i.proto), this.modules.push(e)
        },
        defaults: {
            disableOn: 0,
            key: null,
            midClick: !1,
            mainClass: "",
            preloader: !0,
            focus: "",
            closeOnContentClick: !1,
            closeOnBgClick: !0,
            closeBtnInside: !0,
            showCloseBtn: !0,
            enableEscapeKey: !0,
            modal: !1,
            alignTop: !1,
            removalDelay: 0,
            prependTo: null,
            fixedContentPos: "auto",
            fixedBgPos: "auto",
            overflowY: "auto",
            closeMarkup: '<button title="%title%" type="button" class="mfp-close">&#215;</button>',
            tClose: "Close (Esc)",
            tLoading: "Loading...",
            autoFocusLast: !0
        }
    }, t.fn.magnificPopup = function(i) {
        _();
        var n = t(this);
        if ("string" == typeof i)
            if ("open" === i) {
                var o, r = w ? n.data("magnificPopup") : n[0].magnificPopup,
                    s = parseInt(arguments[1], 10) || 0;
                r.items ? o = r.items[s] : (o = n, r.delegate && (o = o.find(r.delegate)), o = o.eq(s)), e._openClick({
                    mfpEl: o
                }, n, r)
            } else e.isOpen && e[i].apply(e, Array.prototype.slice.call(arguments, 1));
        else i = t.extend(!0, {}, i), w ? n.data("magnificPopup", i) : n[0].magnificPopup = i, e.addGroup(n, i);
        return n
    };
    var E, $, P, O = "inline",
        L = function() {
            P && ($.after(P.addClass(E)).detach(), P = null)
        };
    t.magnificPopup.registerModule(O, {
        options: {
            hiddenClass: "hide",
            markup: "",
            tNotFound: "Content not found"
        },
        proto: {
            initInline: function() {
                e.types.push(O), S(a + "." + O, function() {
                    L()
                })
            },
            getInline: function(i, n) {
                if (L(), i.src) {
                    var o = e.st.inline,
                        r = t(i.src);
                    if (r.length) {
                        var s = r[0].parentNode;
                        s && s.tagName && ($ || (E = o.hiddenClass, $ = x(E), E = "mfp-" + E), P = r.after($).detach().removeClass(E)), e.updateStatus("ready")
                    } else e.updateStatus("error", o.tNotFound), r = t("<div>");
                    return i.inlineElement = r, r
                }
                return e.updateStatus("ready"), e._parseMarkup(n, {}, i), n
            }
        }
    });
    var I, M = "ajax",
        F = function() {
            I && t(document.body).removeClass(I)
        },
        D = function() {
            F(), e.req && e.req.abort()
        };
    t.magnificPopup.registerModule(M, {
        options: {
            settings: null,
            cursor: "mfp-ajax-cur",
            tError: '<a href="%url%">The content</a> could not be loaded.'
        },
        proto: {
            initAjax: function() {
                e.types.push(M), I = e.st.ajax.cursor, S(a + "." + M, D), S("BeforeChange." + M, D)
            },
            getAjax: function(i) {
                I && t(document.body).addClass(I), e.updateStatus("loading");
                var n = t.extend({
                    url: i.src,
                    success: function(n, o, r) {
                        var s = {
                            data: n,
                            xhr: r
                        };
                        C("ParseAjax", s), e.appendContent(t(s.data), M), i.finished = !0, F(), e._setFocus(), setTimeout(function() {
                            e.wrap.addClass(v)
                        }, 16), e.updateStatus("ready"), C("AjaxContentAdded")
                    },
                    error: function() {
                        F(), i.finished = i.loadError = !0, e.updateStatus("error", e.st.ajax.tError.replace("%url%", i.src))
                    }
                }, e.st.ajax.settings);
                return e.req = t.ajax(n), ""
            }
        }
    });
    var j, H = function(i) {
        if (i.data && void 0 !== i.data.title) return i.data.title;
        var n = e.st.image.titleSrc;
        if (n) {
            if (t.isFunction(n)) return n.call(e, i);
            if (i.el) return i.el.attr(n) || ""
        }
        return ""
    };
    t.magnificPopup.registerModule("image", {
        options: {
            markup: '<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',
            cursor: "mfp-zoom-out-cur",
            titleSrc: "title",
            verticalFit: !0,
            tError: '<a href="%url%">The image</a> could not be loaded.'
        },
        proto: {
            initImage: function() {
                var i = e.st.image,
                    n = ".image";
                e.types.push("image"), S(p + n, function() {
                    "image" === e.currItem.type && i.cursor && t(document.body).addClass(i.cursor)
                }), S(a + n, function() {
                    i.cursor && t(document.body).removeClass(i.cursor), k.off("resize" + m)
                }), S("Resize" + n, e.resizeImage), e.isLowIE && S("AfterChange", e.resizeImage)
            },
            resizeImage: function() {
                var t = e.currItem;
                if (t && t.img && e.st.image.verticalFit) {
                    var i = 0;
                    e.isLowIE && (i = parseInt(t.img.css("padding-top"), 10) + parseInt(t.img.css("padding-bottom"), 10)), t.img.css("max-height", e.wH - i)
                }
            },
            _onImageHasSize: function(t) {
                t.img && (t.hasSize = !0, j && clearInterval(j), t.isCheckingImgSize = !1, C("ImageHasSize", t), t.imgHidden && (e.content && e.content.removeClass("mfp-loading"), t.imgHidden = !1))
            },
            findImageSize: function(t) {
                var i = 0,
                    n = t.img[0],
                    o = function(r) {
                        j && clearInterval(j), j = setInterval(function() {
                            return n.naturalWidth > 0 ? void e._onImageHasSize(t) : (i > 200 && clearInterval(j), i++, void(3 === i ? o(10) : 40 === i ? o(50) : 100 === i && o(500)))
                        }, r)
                    };
                o(1)
            },
            getImage: function(i, n) {
                var o = 0,
                    r = function() {
                        i && (i.img[0].complete ? (i.img.off(".mfploader"), i === e.currItem && (e._onImageHasSize(i), e.updateStatus("ready")), i.hasSize = !0, i.loaded = !0, C("ImageLoadComplete")) : (o++, 200 > o ? setTimeout(r, 100) : s()))
                    },
                    s = function() {
                        i && (i.img.off(".mfploader"), i === e.currItem && (e._onImageHasSize(i), e.updateStatus("error", a.tError.replace("%url%", i.src))), i.hasSize = !0, i.loaded = !0, i.loadError = !0)
                    },
                    a = e.st.image,
                    l = n.find(".mfp-img");
                if (l.length) {
                    var c = document.createElement("img");
                    c.className = "mfp-img", i.el && i.el.find("img").length && (c.alt = i.el.find("img").attr("alt")), i.img = t(c).on("load.mfploader", r).on("error.mfploader", s), c.src = i.src, l.is("img") && (i.img = i.img.clone()), c = i.img[0], c.naturalWidth > 0 ? i.hasSize = !0 : c.width || (i.hasSize = !1)
                }
                return e._parseMarkup(n, {
                    title: H(i),
                    img_replaceWith: i.img
                }, i), e.resizeImage(), i.hasSize ? (j && clearInterval(j), i.loadError ? (n.addClass("mfp-loading"), e.updateStatus("error", a.tError.replace("%url%", i.src))) : (n.removeClass("mfp-loading"), e.updateStatus("ready")), n) : (e.updateStatus("loading"), i.loading = !0, i.hasSize || (i.imgHidden = !0, n.addClass("mfp-loading"), e.findImageSize(i)), n)
            }
        }
    });
    var R, W = function() {
        return void 0 === R && (R = void 0 !== document.createElement("p").style.MozTransform), R
    };
    t.magnificPopup.registerModule("zoom", {
        options: {
            enabled: !1,
            easing: "ease-in-out",
            duration: 300,
            opener: function(t) {
                return t.is("img") ? t : t.find("img")
            }
        },
        proto: {
            initZoom: function() {
                var t, i = e.st.zoom,
                    n = ".zoom";
                if (i.enabled && e.supportsTransition) {
                    var o, r, s = i.duration,
                        c = function(t) {
                            var e = t.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"),
                                n = "all " + i.duration / 1e3 + "s " + i.easing,
                                o = {
                                    position: "fixed",
                                    zIndex: 9999,
                                    left: 0,
                                    top: 0,
                                    "-webkit-backface-visibility": "hidden"
                                },
                                r = "transition";
                            return o["-webkit-" + r] = o["-moz-" + r] = o["-o-" + r] = o[r] = n, e.css(o), e
                        },
                        u = function() {
                            e.content.css("visibility", "visible")
                        };
                    S("BuildControls" + n, function() {
                        if (e._allowZoom()) {
                            if (clearTimeout(o), e.content.css("visibility", "hidden"), t = e._getItemToZoom(), !t) return void u();
                            r = c(t), r.css(e._getOffset()), e.wrap.append(r), o = setTimeout(function() {
                                r.css(e._getOffset(!0)), o = setTimeout(function() {
                                    u(), setTimeout(function() {
                                        r.remove(), t = r = null, C("ZoomAnimationEnded")
                                    }, 16)
                                }, s)
                            }, 16)
                        }
                    }), S(l + n, function() {
                        if (e._allowZoom()) {
                            if (clearTimeout(o), e.st.removalDelay = s, !t) {
                                if (t = e._getItemToZoom(), !t) return;
                                r = c(t)
                            }
                            r.css(e._getOffset(!0)), e.wrap.append(r), e.content.css("visibility", "hidden"), setTimeout(function() {
                                r.css(e._getOffset())
                            }, 16)
                        }
                    }), S(a + n, function() {
                        e._allowZoom() && (u(), r && r.remove(), t = null)
                    })
                }
            },
            _allowZoom: function() {
                return "image" === e.currItem.type
            },
            _getItemToZoom: function() {
                return !!e.currItem.hasSize && e.currItem.img
            },
            _getOffset: function(i) {
                var n;
                n = i ? e.currItem.img : e.st.zoom.opener(e.currItem.el || e.currItem);
                var o = n.offset(),
                    r = parseInt(n.css("padding-top"), 10),
                    s = parseInt(n.css("padding-bottom"), 10);
                o.top -= t(window).scrollTop() - r;
                var a = {
                    width: n.width(),
                    height: (w ? n.innerHeight() : n[0].offsetHeight) - s - r
                };
                return W() ? a["-moz-transform"] = a.transform = "translate(" + o.left + "px," + o.top + "px)" : (a.left = o.left, a.top = o.top), a
            }
        }
    });
    var z = "iframe",
        B = "//about:blank",
        N = function(t) {
            if (e.currTemplate[z]) {
                var i = e.currTemplate[z].find("iframe");
                i.length && (t || (i[0].src = B), e.isIE8 && i.css("display", t ? "block" : "none"))
            }
        };
    t.magnificPopup.registerModule(z, {
        options: {
            markup: '<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe></div>',
            srcAction: "iframe_src",
            patterns: {
                youtube: {
                    index: "youtube.com",
                    id: "v=",
                    src: "//www.youtube.com/embed/%id%?autoplay=1"
                },
                vimeo: {
                    index: "vimeo.com/",
                    id: "/",
                    src: "//player.vimeo.com/video/%id%?autoplay=1"
                },
                gmaps: {
                    index: "//maps.google.",
                    src: "%id%&output=embed"
                }
            }
        },
        proto: {
            initIframe: function() {
                e.types.push(z), S("BeforeChange", function(t, e, i) {
                    e !== i && (e === z ? N() : i === z && N(!0))
                }), S(a + "." + z, function() {
                    N()
                })
            },
            getIframe: function(i, n) {
                var o = i.src,
                    r = e.st.iframe;
                t.each(r.patterns, function() {
                    return o.indexOf(this.index) > -1 ? (this.id && (o = "string" == typeof this.id ? o.substr(o.lastIndexOf(this.id) + this.id.length, o.length) : this.id.call(this, o)), o = this.src.replace("%id%", o), !1) : void 0
                });
                var s = {};
                return r.srcAction && (s[r.srcAction] = o), e._parseMarkup(n, s, i), e.updateStatus("ready"), n
            }
        }
    });
    var q = function(t) {
            var i = e.items.length;
            return t > i - 1 ? t - i : 0 > t ? i + t : t
        },
        Y = function(t, e, i) {
            return t.replace(/%curr%/gi, e + 1).replace(/%total%/gi, i)
        };
    t.magnificPopup.registerModule("gallery", {
        options: {
            enabled: !1,
            arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
            preload: [0, 2],
            navigateByImgClick: !0,
            arrows: !0,
            tPrev: "Previous (Left arrow key)",
            tNext: "Next (Right arrow key)",
            tCounter: "%curr% of %total%"
        },
        proto: {
            initGallery: function() {
                var i = e.st.gallery,
                    o = ".mfp-gallery";
                return e.direction = !0, !(!i || !i.enabled) && (r += " mfp-gallery", S(p + o, function() {
                    i.navigateByImgClick && e.wrap.on("click" + o, ".mfp-img", function() {
                        return e.items.length > 1 ? (e.next(), !1) : void 0
                    }), n.on("keydown" + o, function(t) {
                        37 === t.keyCode ? e.prev() : 39 === t.keyCode && e.next()
                    })
                }), S("UpdateStatus" + o, function(t, i) {
                    i.text && (i.text = Y(i.text, e.currItem.index, e.items.length))
                }), S(d + o, function(t, n, o, r) {
                    var s = e.items.length;
                    o.counter = s > 1 ? Y(i.tCounter, r.index, s) : ""
                }), S("BuildControls" + o, function() {
                    if (e.items.length > 1 && i.arrows && !e.arrowLeft) {
                        var n = i.arrowMarkup,
                            o = e.arrowLeft = t(n.replace(/%title%/gi, i.tPrev).replace(/%dir%/gi, "left")).addClass(y),
                            r = e.arrowRight = t(n.replace(/%title%/gi, i.tNext).replace(/%dir%/gi, "right")).addClass(y);
                        o.click(function() {
                            e.prev()
                        }), r.click(function() {
                            e.next()
                        }), e.container.append(o.add(r))
                    }
                }), S(f + o, function() {
                    e._preloadTimeout && clearTimeout(e._preloadTimeout), e._preloadTimeout = setTimeout(function() {
                        e.preloadNearbyImages(), e._preloadTimeout = null
                    }, 16)
                }), void S(a + o, function() {
                    n.off(o), e.wrap.off("click" + o), e.arrowRight = e.arrowLeft = null
                }))
            },
            next: function() {
                e.direction = !0, e.index = q(e.index + 1), e.updateItemHTML()
            },
            prev: function() {
                e.direction = !1, e.index = q(e.index - 1), e.updateItemHTML()
            },
            goTo: function(t) {
                e.direction = t >= e.index, e.index = t, e.updateItemHTML()
            },
            preloadNearbyImages: function() {
                var t, i = e.st.gallery.preload,
                    n = Math.min(i[0], e.items.length),
                    o = Math.min(i[1], e.items.length);
                for (t = 1; t <= (e.direction ? o : n); t++) e._preloadItem(e.index + t);
                for (t = 1; t <= (e.direction ? n : o); t++) e._preloadItem(e.index - t)
            },
            _preloadItem: function(i) {
                if (i = q(i), !e.items[i].preloaded) {
                    var n = e.items[i];
                    n.parsed || (n = e.parseEl(i)), C("LazyLoad", n), "image" === n.type && (n.img = t('<img class="mfp-img" />').on("load.mfploader", function() {
                        n.hasSize = !0
                    }).on("error.mfploader", function() {
                        n.hasSize = !0, n.loadError = !0, C("LazyLoadError", n)
                    }).attr("src", n.src)), n.preloaded = !0
                }
            }
        }
    });
    var X = "retina";
    t.magnificPopup.registerModule(X, {
        options: {
            replaceSrc: function(t) {
                return t.src.replace(/\.\w+$/, function(t) {
                    return "@2x" + t
                })
            },
            ratio: 1
        },
        proto: {
            initRetina: function() {
                if (window.devicePixelRatio > 1) {
                    var t = e.st.retina,
                        i = t.ratio;
                    i = isNaN(i) ? i() : i, i > 1 && (S("ImageHasSize." + X, function(t, e) {
                        e.img.css({
                            "max-width": e.img[0].naturalWidth / i,
                            width: "100%"
                        })
                    }), S("ElementParse." + X, function(e, n) {
                        n.src = t.replaceSrc(n, i)
                    }))
                }
            }
        }
    }), _()
}), ! function() {
    "use strict";

    function t() {}

    function e(t) {
        this.options = i.Adapter.extend({}, e.defaults, t), this.axis = this.options.horizontal ? "horizontal" : "vertical", this.waypoints = [], this.element = this.options.element, this.createWaypoints()
    }
    var i = window.Waypoint;
    e.prototype.createWaypoints = function() {
        for (var t = {
            vertical: [{
                down: "enter",
                up: "exited",
                offset: "100%"
            }, {
                down: "entered",
                up: "exit",
                offset: "bottom-in-view"
            }, {
                down: "exit",
                up: "entered",
                offset: 0
            }, {
                down: "exited",
                up: "enter",
                offset: function() {
                    return -this.adapter.outerHeight()
                }
            }],
            horizontal: [{
                right: "enter",
                left: "exited",
                offset: "100%"
            }, {
                right: "entered",
                left: "exit",
                offset: "right-in-view"
            }, {
                right: "exit",
                left: "entered",
                offset: 0
            }, {
                right: "exited",
                left: "enter",
                offset: function() {
                    return -this.adapter.outerWidth()
                }
            }]
        }, e = 0, i = t[this.axis].length; i > e; e++) {
            var n = t[this.axis][e];
            this.createWaypoint(n)
        }
    }, e.prototype.createWaypoint = function(t) {
        var e = this;
        this.waypoints.push(new i({
            context: this.options.context,
            element: this.options.element,
            enabled: this.options.enabled,
            handler: function(t) {
                return function(i) {
                    e.options[t[i]].call(e, i)
                }
            }(t),
            offset: t.offset,
            horizontal: this.options.horizontal
        }))
    }, e.prototype.destroy = function() {
        for (var t = 0, e = this.waypoints.length; e > t; t++) this.waypoints[t].destroy();
        this.waypoints = []
    }, e.prototype.disable = function() {
        for (var t = 0, e = this.waypoints.length; e > t; t++) this.waypoints[t].disable()
    }, e.prototype.enable = function() {
        for (var t = 0, e = this.waypoints.length; e > t; t++) this.waypoints[t].enable()
    }, e.defaults = {
        context: window,
        enabled: !0,
        enter: t,
        entered: t,
        exit: t,
        exited: t
    }, i.Inview = e
}(),
    function(t) {
        if ("function" == typeof define && define.amd) define(t);
        else if ("object" == typeof exports) module.exports = t();
        else {
            var e = window.Cookies,
                i = window.Cookies = t();
            i.noConflict = function() {
                return window.Cookies = e, i
            }
        }
    }(function() {
        function t() {
            for (var t = 0, e = {}; t < arguments.length; t++) {
                var i = arguments[t];
                for (var n in i) e[n] = i[n]
            }
            return e
        }

        function e(i) {
            function n(e, o, r) {
                var s;
                if ("undefined" != typeof document) {
                    if (arguments.length > 1) {
                        if (r = t({
                                path: "/"
                            }, n.defaults, r), "number" == typeof r.expires) {
                            var a = new Date;
                            a.setMilliseconds(a.getMilliseconds() + 864e5 * r.expires), r.expires = a
                        }
                        try {
                            s = JSON.stringify(o), /^[\{\[]/.test(s) && (o = s)
                        } catch (t) {}
                        return o = i.write ? i.write(o, e) : encodeURIComponent(String(o)).replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent), e = encodeURIComponent(String(e)), e = e.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent), e = e.replace(/[\(\)]/g, escape), document.cookie = [e, "=", o, r.expires && "; expires=" + r.expires.toUTCString(), r.path && "; path=" + r.path, r.domain && "; domain=" + r.domain, r.secure ? "; secure" : ""].join("")
                    }
                    e || (s = {});
                    for (var l = document.cookie ? document.cookie.split("; ") : [], c = /(%[0-9A-Z]{2})+/g, u = 0; u < l.length; u++) {
                        var d = l[u].split("="),
                            p = d.slice(1).join("=");
                        '"' === p.charAt(0) && (p = p.slice(1, -1));
                        try {
                            var f = d[0].replace(c, decodeURIComponent);
                            if (p = i.read ? i.read(p, f) : i(p, f) || p.replace(c, decodeURIComponent), this.json) try {
                                p = JSON.parse(p)
                            } catch (t) {}
                            if (e === f) {
                                s = p;
                                break
                            }
                            e || (s[f] = p)
                        } catch (t) {}
                    }
                    return s
                }
            }
            return n.set = n, n.get = function(t) {
                return n(t)
            }, n.getJSON = function() {
                return n.apply({
                    json: !0
                }, [].slice.call(arguments))
            }, n.defaults = {}, n.remove = function(e, i) {
                n(e, "", t(i, {
                    expires: -1
                }))
            }, n.withConverter = e, n
        }
        return e(function() {})
    }), ! function(t, e) {
    "object" == typeof exports && "undefined" != typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define(e) : t.Jump = e()
}(this, function() {
    "use strict";
    var t = function(t, e, i, n) {
            return t /= n / 2, t < 1 ? i / 2 * t * t + e : (t--, -i / 2 * (t * (t - 2) - 1) + e)
        },
        e = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) {
            return typeof t
        } : function(t) {
            return t && "function" == typeof Symbol && t.constructor === Symbol ? "symbol" : typeof t
        },
        i = function() {
            function i() {
                return window.scrollY || window.pageYOffset
            }

            function n(t) {
                return t.getBoundingClientRect().top + l
            }

            function o(t) {
                m || (m = t), v = t - m, g = d(v, l, f, h), window.scrollTo(0, g), v < h ? requestAnimationFrame(o) : r()
            }

            function r() {
                window.scrollTo(0, l + f), a && p && (a.setAttribute("tabindex", "-1"), a.focus()), "function" == typeof y && y(), m = !1
            }

            function s(r) {
                var s = arguments.length <= 1 || void 0 === arguments[1] ? {} : arguments[1];
                switch (h = s.duration || 1e3, u = s.offset || 0, y = s.callback, d = s.easing || t, p = s.a11y || !1, l = i(), "undefined" == typeof r ? "undefined" : e(r)) {
                    case "number":
                        a = void 0, p = !1, c = l + r;
                        break;
                    case "object":
                        a = r, c = n(a);
                        break;
                    case "string":
                        a = document.querySelector(r), c = n(a)
                }
                switch (f = c - l + u, e(s.duration)) {
                    case "number":
                        h = s.duration;
                        break;
                    case "function":
                        h = s.duration(f)
                }
                requestAnimationFrame(o)
            }
            var a = void 0,
                l = void 0,
                c = void 0,
                u = void 0,
                d = void 0,
                p = void 0,
                f = void 0,
                h = void 0,
                m = void 0,
                v = void 0,
                g = void 0,
                y = void 0;
            return s
        },
        n = i();
    return n
}),
    function(t) {
        "function" == typeof define && define.amd ? define([], t) : "object" == typeof exports ? module.exports = t() : window.noUiSlider = t()
    }(function() {
        "use strict";

        function t(t, e) {
            var i = document.createElement("div");
            return c(i, e), t.appendChild(i), i
        }

        function e(t) {
            return t.filter(function(t) {
                return !this[t] && (this[t] = !0)
            }, {})
        }

        function i(t, e) {
            return Math.round(t / e) * e
        }

        function n(t, e) {
            var i = t.getBoundingClientRect(),
                n = t.ownerDocument,
                o = n.documentElement,
                r = p();
            return /webkit.*Chrome.*Mobile/i.test(navigator.userAgent) && (r.x = 0), e ? i.top + r.y - o.clientTop : i.left + r.x - o.clientLeft
        }

        function o(t) {
            return "number" == typeof t && !isNaN(t) && isFinite(t)
        }

        function r(t, e, i) {
            i > 0 && (c(t, e), setTimeout(function() {
                u(t, e)
            }, i))
        }

        function s(t) {
            return Math.max(Math.min(t, 100), 0)
        }

        function a(t) {
            return Array.isArray(t) ? t : [t]
        }

        function l(t) {
            t = String(t);
            var e = t.split(".");
            return e.length > 1 ? e[1].length : 0
        }

        function c(t, e) {
            t.classList ? t.classList.add(e) : t.className += " " + e
        }

        function u(t, e) {
            t.classList ? t.classList.remove(e) : t.className = t.className.replace(new RegExp("(^|\\b)" + e.split(" ").join("|") + "(\\b|$)", "gi"), " ")
        }

        function d(t, e) {
            return t.classList ? t.classList.contains(e) : new RegExp("\\b" + e + "\\b").test(t.className)
        }

        function p() {
            var t = void 0 !== window.pageXOffset,
                e = "CSS1Compat" === (document.compatMode || ""),
                i = t ? window.pageXOffset : e ? document.documentElement.scrollLeft : document.body.scrollLeft,
                n = t ? window.pageYOffset : e ? document.documentElement.scrollTop : document.body.scrollTop;
            return {
                x: i,
                y: n
            }
        }

        function f() {
            return window.navigator.pointerEnabled ? {
                start: "pointerdown",
                move: "pointermove",
                end: "pointerup"
            } : window.navigator.msPointerEnabled ? {
                start: "MSPointerDown",
                move: "MSPointerMove",
                end: "MSPointerUp"
            } : {
                start: "mousedown touchstart",
                move: "mousemove touchmove",
                end: "mouseup touchend"
            }
        }

        function h(t, e) {
            return 100 / (e - t)
        }

        function m(t, e) {
            return 100 * e / (t[1] - t[0])
        }

        function v(t, e) {
            return m(t, t[0] < 0 ? e + Math.abs(t[0]) : e - t[0])
        }

        function g(t, e) {
            return e * (t[1] - t[0]) / 100 + t[0]
        }

        function y(t, e) {
            for (var i = 1; t >= e[i];) i += 1;
            return i
        }

        function b(t, e, i) {
            if (i >= t.slice(-1)[0]) return 100;
            var n, o, r, s, a = y(i, t);
            return n = t[a - 1], o = t[a], r = e[a - 1], s = e[a], r + v([n, o], i) / h(r, s)
        }

        function w(t, e, i) {
            if (i >= 100) return t.slice(-1)[0];
            var n, o, r, s, a = y(i, e);
            return n = t[a - 1], o = t[a], r = e[a - 1], s = e[a], g([n, o], (i - r) * h(r, s))
        }

        function k(t, e, n, o) {
            if (100 === o) return o;
            var r, s, a = y(o, t);
            return n ? (r = t[a - 1], s = t[a], o - r > (s - r) / 2 ? s : r) : e[a - 1] ? t[a - 1] + i(o - t[a - 1], e[a - 1]) : o
        }

        function S(t, e, i) {
            var n;
            if ("number" == typeof e && (e = [e]), "[object Array]" !== Object.prototype.toString.call(e)) throw new Error("noUiSlider: 'range' contains invalid value.");
            if (n = "min" === t ? 0 : "max" === t ? 100 : parseFloat(t), !o(n) || !o(e[0])) throw new Error("noUiSlider: 'range' value isn't numeric.");
            i.xPct.push(n), i.xVal.push(e[0]), n ? i.xSteps.push(!isNaN(e[1]) && e[1]) : isNaN(e[1]) || (i.xSteps[0] = e[1]), i.xHighestCompleteStep.push(0)
        }

        function x(t, e, i) {
            if (!e) return !0;
            i.xSteps[t] = m([i.xVal[t], i.xVal[t + 1]], e) / h(i.xPct[t], i.xPct[t + 1]);
            var n = (i.xVal[t + 1] - i.xVal[t]) / i.xNumSteps[t],
                o = Math.ceil(Number(n.toFixed(3)) - 1),
                r = i.xVal[t] + i.xNumSteps[t] * o;
            i.xHighestCompleteStep[t] = r
        }

        function C(t, e, i, n) {
            this.xPct = [], this.xVal = [], this.xSteps = [n || !1], this.xNumSteps = [!1], this.xHighestCompleteStep = [], this.snap = e, this.direction = i;
            var o, r = [];
            for (o in t) t.hasOwnProperty(o) && r.push([t[o], o]);
            for (r.length && "object" == typeof r[0][0] ? r.sort(function(t, e) {
                return t[0][0] - e[0][0]
            }) : r.sort(function(t, e) {
                return t[0] - e[0]
            }), o = 0; o < r.length; o++) S(r[o][1], r[o][0], this);
            for (this.xNumSteps = this.xSteps.slice(0), o = 0; o < this.xNumSteps.length; o++) x(o, this.xNumSteps[o], this)
        }

        function T(t, e) {
            if (!o(e)) throw new Error("noUiSlider: 'step' is not numeric.");
            t.singleStep = e
        }

        function _(t, e) {
            if ("object" != typeof e || Array.isArray(e)) throw new Error("noUiSlider: 'range' is not an object.");
            if (void 0 === e.min || void 0 === e.max) throw new Error("noUiSlider: Missing 'min' or 'max' in 'range'.");
            if (e.min === e.max) throw new Error("noUiSlider: 'range' 'min' and 'max' cannot be equal.");
            t.spectrum = new C(e, t.snap, t.dir, t.singleStep)
        }

        function A(t, e) {
            if (e = a(e), !Array.isArray(e) || !e.length) throw new Error("noUiSlider: 'start' option is incorrect.");
            t.handles = e.length, t.start = e
        }

        function E(t, e) {
            if (t.snap = e, "boolean" != typeof e) throw new Error("noUiSlider: 'snap' option must be a boolean.")
        }

        function $(t, e) {
            if (t.animate = e, "boolean" != typeof e) throw new Error("noUiSlider: 'animate' option must be a boolean.")
        }

        function P(t, e) {
            if (t.animationDuration = e, "number" != typeof e) throw new Error("noUiSlider: 'animationDuration' option must be a number.")
        }

        function O(t, e) {
            var i, n = [!1];
            if ("lower" === e ? e = [!0, !1] : "upper" === e && (e = [!1, !0]), e === !0 || e === !1) {
                for (i = 1; i < t.handles; i++) n.push(e);
                n.push(!1)
            } else {
                if (!Array.isArray(e) || !e.length || e.length !== t.handles + 1) throw new Error("noUiSlider: 'connect' option doesn't match handle count.");
                n = e
            }
            t.connect = n
        }

        function L(t, e) {
            switch (e) {
                case "horizontal":
                    t.ort = 0;
                    break;
                case "vertical":
                    t.ort = 1;
                    break;
                default:
                    throw new Error("noUiSlider: 'orientation' option is invalid.")
            }
        }

        function I(t, e) {
            if (!o(e)) throw new Error("noUiSlider: 'margin' option must be numeric.");
            if (0 !== e && (t.margin = t.spectrum.getMargin(e), !t.margin)) throw new Error("noUiSlider: 'margin' option is only supported on linear sliders.")
        }

        function M(t, e) {
            if (!o(e)) throw new Error("noUiSlider: 'limit' option must be numeric.");
            if (t.limit = t.spectrum.getMargin(e), !t.limit || t.handles < 2) throw new Error("noUiSlider: 'limit' option is only supported on linear sliders with 2 or more handles.")
        }

        function F(t, e) {
            if (!o(e)) throw new Error("noUiSlider: 'padding' option must be numeric.");
            if (0 !== e) {
                if (t.padding = t.spectrum.getMargin(e), !t.padding) throw new Error("noUiSlider: 'padding' option is only supported on linear sliders.");
                if (t.padding < 0) throw new Error("noUiSlider: 'padding' option must be a positive number.");
                if (t.padding >= 50) throw new Error("noUiSlider: 'padding' option must be less than half the range.")
            }
        }

        function D(t, e) {
            switch (e) {
                case "ltr":
                    t.dir = 0;
                    break;
                case "rtl":
                    t.dir = 1;
                    break;
                default:
                    throw new Error("noUiSlider: 'direction' option was not recognized.")
            }
        }

        function j(t, e) {
            if ("string" != typeof e) throw new Error("noUiSlider: 'behaviour' must be a string containing options.");
            var i = e.indexOf("tap") >= 0,
                n = e.indexOf("drag") >= 0,
                o = e.indexOf("fixed") >= 0,
                r = e.indexOf("snap") >= 0,
                s = e.indexOf("hover") >= 0;
            if (o) {
                if (2 !== t.handles) throw new Error("noUiSlider: 'fixed' behaviour must be used with 2 handles");
                I(t, t.start[1] - t.start[0])
            }
            t.events = {
                tap: i || r,
                drag: n,
                fixed: o,
                snap: r,
                hover: s
            }
        }

        function H(t, e) {
            if (e !== !1)
                if (e === !0) {
                    t.tooltips = [];
                    for (var i = 0; i < t.handles; i++) t.tooltips.push(!0)
                } else {
                    if (t.tooltips = a(e), t.tooltips.length !== t.handles) throw new Error("noUiSlider: must pass a formatter for all handles.");
                    t.tooltips.forEach(function(t) {
                        if ("boolean" != typeof t && ("object" != typeof t || "function" != typeof t.to)) throw new Error("noUiSlider: 'tooltips' must be passed a formatter or 'false'.")
                    })
                }
        }

        function R(t, e) {
            if (t.format = e, "function" == typeof e.to && "function" == typeof e.from) return !0;
            throw new Error("noUiSlider: 'format' requires 'to' and 'from' methods.")
        }

        function W(t, e) {
            if (void 0 !== e && "string" != typeof e && e !== !1) throw new Error("noUiSlider: 'cssPrefix' must be a string or `false`.");
            t.cssPrefix = e
        }

        function z(t, e) {
            if (void 0 !== e && "object" != typeof e) throw new Error("noUiSlider: 'cssClasses' must be an object.");
            if ("string" == typeof t.cssPrefix) {
                t.cssClasses = {};
                for (var i in e) e.hasOwnProperty(i) && (t.cssClasses[i] = t.cssPrefix + e[i])
            } else t.cssClasses = e
        }

        function B(t, e) {
            if (e !== !0 && e !== !1) throw new Error("noUiSlider: 'useRequestAnimationFrame' option should be true (default) or false.");
            t.useRequestAnimationFrame = e
        }

        function N(t) {
            var e = {
                    margin: 0,
                    limit: 0,
                    padding: 0,
                    animate: !0,
                    animationDuration: 300,
                    format: X
                },
                i = {
                    step: {
                        r: !1,
                        t: T
                    },
                    start: {
                        r: !0,
                        t: A
                    },
                    connect: {
                        r: !0,
                        t: O
                    },
                    direction: {
                        r: !0,
                        t: D
                    },
                    snap: {
                        r: !1,
                        t: E
                    },
                    animate: {
                        r: !1,
                        t: $
                    },
                    animationDuration: {
                        r: !1,
                        t: P
                    },
                    range: {
                        r: !0,
                        t: _
                    },
                    orientation: {
                        r: !1,
                        t: L
                    },
                    margin: {
                        r: !1,
                        t: I
                    },
                    limit: {
                        r: !1,
                        t: M
                    },
                    padding: {
                        r: !1,
                        t: F
                    },
                    behaviour: {
                        r: !0,
                        t: j
                    },
                    format: {
                        r: !1,
                        t: R
                    },
                    tooltips: {
                        r: !1,
                        t: H
                    },
                    cssPrefix: {
                        r: !1,
                        t: W
                    },
                    cssClasses: {
                        r: !1,
                        t: z
                    },
                    useRequestAnimationFrame: {
                        r: !1,
                        t: B
                    }
                },
                n = {
                    connect: !1,
                    direction: "ltr",
                    behaviour: "tap",
                    orientation: "horizontal",
                    cssPrefix: "noUi-",
                    cssClasses: {
                        target: "target",
                        base: "base",
                        origin: "origin",
                        handle: "handle",
                        handleLower: "handle-lower",
                        handleUpper: "handle-upper",
                        horizontal: "horizontal",
                        vertical: "vertical",
                        background: "background",
                        connect: "connect",
                        ltr: "ltr",
                        rtl: "rtl",
                        draggable: "draggable",
                        drag: "state-drag",
                        tap: "state-tap",
                        active: "active",
                        tooltip: "tooltip",
                        pips: "pips",
                        pipsHorizontal: "pips-horizontal",
                        pipsVertical: "pips-vertical",
                        marker: "marker",
                        markerHorizontal: "marker-horizontal",
                        markerVertical: "marker-vertical",
                        markerNormal: "marker-normal",
                        markerLarge: "marker-large",
                        markerSub: "marker-sub",
                        value: "value",
                        valueHorizontal: "value-horizontal",
                        valueVertical: "value-vertical",
                        valueNormal: "value-normal",
                        valueLarge: "value-large",
                        valueSub: "value-sub"
                    },
                    useRequestAnimationFrame: !0
                };
            Object.keys(i).forEach(function(o) {
                if (void 0 === t[o] && void 0 === n[o]) {
                    if (i[o].r) throw new Error("noUiSlider: '" + o + "' is required.");
                    return !0
                }
                i[o].t(e, void 0 === t[o] ? n[o] : t[o])
            }), e.pips = t.pips;
            var o = [
                ["left", "top"],
                ["right", "bottom"]
            ];
            return e.style = o[e.dir][e.ort], e.styleOposite = o[e.dir ? 0 : 1][e.ort], e
        }

        function q(i, o, l) {
            function h(e, i) {
                var n = t(e, o.cssClasses.origin),
                    r = t(n, o.cssClasses.handle);
                return r.setAttribute("data-handle", i), 0 === i ? c(r, o.cssClasses.handleLower) : i === o.handles - 1 && c(r, o.cssClasses.handleUpper), n
            }

            function m(e, i) {
                return !!i && t(e, o.cssClasses.connect)
            }

            function v(t, e) {
                et = [], it = [], it.push(m(e, t[0]));
                for (var i = 0; i < o.handles; i++) et.push(h(e, i)), at[i] = i, it.push(m(e, t[i + 1]))
            }

            function g(e) {
                c(e, o.cssClasses.target), 0 === o.dir ? c(e, o.cssClasses.ltr) : c(e, o.cssClasses.rtl), 0 === o.ort ? c(e, o.cssClasses.horizontal) : c(e, o.cssClasses.vertical), tt = t(e, o.cssClasses.base)
            }

            function y(e, i) {
                return !!o.tooltips[i] && t(e.firstChild, o.cssClasses.tooltip)
            }

            function b() {
                var t = et.map(y);
                G("update", function(e, i, n) {
                    if (t[i]) {
                        var r = e[i];
                        o.tooltips[i] !== !0 && (r = o.tooltips[i].to(n[i])), t[i].innerHTML = r
                    }
                })
            }

            function w(t, e, i) {
                if ("range" === t || "steps" === t) return ct.xVal;
                if ("count" === t) {
                    var n, o = 100 / (e - 1),
                        r = 0;
                    for (e = [];
                         (n = r++ * o) <= 100;) e.push(n);
                    t = "positions"
                }
                return "positions" === t ? e.map(function(t) {
                    return ct.fromStepping(i ? ct.getStep(t) : t)
                }) : "values" === t ? i ? e.map(function(t) {
                    return ct.fromStepping(ct.getStep(ct.toStepping(t)))
                }) : e : void 0
            }

            function k(t, i, n) {
                function o(t, e) {
                    return (t + e).toFixed(7) / 1
                }
                var r = {},
                    s = ct.xVal[0],
                    a = ct.xVal[ct.xVal.length - 1],
                    l = !1,
                    c = !1,
                    u = 0;
                return n = e(n.slice().sort(function(t, e) {
                    return t - e
                })), n[0] !== s && (n.unshift(s), l = !0), n[n.length - 1] !== a && (n.push(a), c = !0), n.forEach(function(e, s) {
                    var a, d, p, f, h, m, v, g, y, b, w = e,
                        k = n[s + 1];
                    if ("steps" === i && (a = ct.xNumSteps[s]), a || (a = k - w), w !== !1 && void 0 !== k)
                        for (a = Math.max(a, 1e-7), d = w; d <= k; d = o(d, a)) {
                            for (f = ct.toStepping(d), h = f - u, g = h / t, y = Math.round(g), b = h / y, p = 1; p <= y; p += 1) m = u + p * b, r[m.toFixed(5)] = ["x", 0];
                            v = n.indexOf(d) > -1 ? 1 : "steps" === i ? 2 : 0, !s && l && (v = 0), d === k && c || (r[f.toFixed(5)] = [d, v]), u = f
                        }
                }), r
            }

            function S(t, e, i) {
                function n(t, e) {
                    var i = e === o.cssClasses.value,
                        n = i ? p : f,
                        r = i ? u : d;
                    return e + " " + n[o.ort] + " " + r[t]
                }

                function r(t, e, i) {
                    return 'class="' + n(i[1], e) + '" style="' + o.style + ": " + t + '%"'
                }

                function s(t, n) {
                    n[1] = n[1] && e ? e(n[0], n[1]) : n[1], l += "<div " + r(t, o.cssClasses.marker, n) + "></div>", n[1] && (l += "<div " + r(t, o.cssClasses.value, n) + ">" + i.to(n[0]) + "</div>")
                }
                var a = document.createElement("div"),
                    l = "",
                    u = [o.cssClasses.valueNormal, o.cssClasses.valueLarge, o.cssClasses.valueSub],
                    d = [o.cssClasses.markerNormal, o.cssClasses.markerLarge, o.cssClasses.markerSub],
                    p = [o.cssClasses.valueHorizontal, o.cssClasses.valueVertical],
                    f = [o.cssClasses.markerHorizontal, o.cssClasses.markerVertical];
                return c(a, o.cssClasses.pips), c(a, 0 === o.ort ? o.cssClasses.pipsHorizontal : o.cssClasses.pipsVertical), Object.keys(t).forEach(function(e) {
                    s(e, t[e])
                }), a.innerHTML = l, a
            }

            function x(t) {
                var e = t.mode,
                    i = t.density || 1,
                    n = t.filter || !1,
                    o = t.values || !1,
                    r = t.stepped || !1,
                    s = w(e, o, r),
                    a = k(i, e, s),
                    l = t.format || {
                            to: Math.round
                        };
                return rt.appendChild(S(a, n, l))
            }

            function C() {
                var t = tt.getBoundingClientRect(),
                    e = "offset" + ["Width", "Height"][o.ort];
                return 0 === o.ort ? t.width || tt[e] : t.height || tt[e]
            }

            function T(t, e, i, n) {
                var r = function(e) {
                        return !rt.hasAttribute("disabled") && (!d(rt, o.cssClasses.tap) && (!!(e = _(e, n.pageOffset)) && (!(t === ot.start && void 0 !== e.buttons && e.buttons > 1) && ((!n.hover || !e.buttons) && (e.calcPoint = e.points[o.ort], void i(e, n))))))
                    },
                    s = [];
                return t.split(" ").forEach(function(t) {
                    e.addEventListener(t, r, !1), s.push([t, r])
                }), s
            }

            function _(t, e) {
                t.preventDefault();
                var i, n, o = 0 === t.type.indexOf("touch"),
                    r = 0 === t.type.indexOf("mouse"),
                    s = 0 === t.type.indexOf("pointer");
                if (0 === t.type.indexOf("MSPointer") && (s = !0), o) {
                    if (t.touches.length > 1) return !1;
                    i = t.changedTouches[0].pageX, n = t.changedTouches[0].pageY
                }
                return e = e || p(), (r || s) && (i = t.clientX + e.x, n = t.clientY + e.y), t.pageOffset = e, t.points = [i, n], t.cursor = r || s, t
            }

            function A(t) {
                var e = t - n(tt, o.ort),
                    i = 100 * e / C();
                return o.dir ? 100 - i : i
            }

            function E(t) {
                var e = 100,
                    i = !1;
                return et.forEach(function(n, o) {
                    if (!n.hasAttribute("disabled")) {
                        var r = Math.abs(st[o] - t);
                        r < e && (i = o, e = r)
                    }
                }), i
            }

            function $(t, e, i, n) {
                var o = i.slice(),
                    r = [!t, t],
                    s = [t, !t];
                n = n.slice(), t && n.reverse(), n.length > 1 ? n.forEach(function(t, i) {
                    var n = H(o, t, o[t] + e, r[i], s[i]);
                    n === !1 ? e = 0 : (e = n - o[t], o[t] = n)
                }) : r = s = [!0];
                var a = !1;
                n.forEach(function(t, n) {
                    a = B(t, i[t] + e, r[n], s[n]) || a
                }), a && n.forEach(function(t) {
                    P("update", t), P("slide", t)
                })
            }

            function P(t, e, i) {
                Object.keys(dt).forEach(function(n) {
                    var r = n.split(".")[0];
                    t === r && dt[n].forEach(function(t) {
                        t.call(nt, ut.map(o.format.to), e, ut.slice(), i || !1, st.slice())
                    })
                })
            }

            function O(t, e) {
                "mouseout" === t.type && "HTML" === t.target.nodeName && null === t.relatedTarget && I(t, e)
            }

            function L(t, e) {
                if (navigator.appVersion.indexOf("MSIE 9") === -1 && 0 === t.buttons && 0 !== e.buttonsProperty) return I(t, e);
                var i = (o.dir ? -1 : 1) * (t.calcPoint - e.startCalcPoint),
                    n = 100 * i / e.baseSize;
                $(i > 0, n, e.locations, e.handleNumbers)
            }

            function I(t, e) {
                lt && (u(lt, o.cssClasses.active), lt = !1), t.cursor && (document.body.style.cursor = "", document.body.removeEventListener("selectstart", document.body.noUiListener)), document.documentElement.noUiListeners.forEach(function(t) {
                    document.documentElement.removeEventListener(t[0], t[1])
                }), u(rt, o.cssClasses.drag), z(), e.handleNumbers.forEach(function(t) {
                    P("set", t), P("change", t), P("end", t)
                })
            }

            function M(t, e) {
                if (1 === e.handleNumbers.length) {
                    var i = et[e.handleNumbers[0]];
                    if (i.hasAttribute("disabled")) return !1;
                    lt = i.children[0], c(lt, o.cssClasses.active)
                }
                t.preventDefault(), t.stopPropagation();
                var n = T(ot.move, document.documentElement, L, {
                        startCalcPoint: t.calcPoint,
                        baseSize: C(),
                        pageOffset: t.pageOffset,
                        handleNumbers: e.handleNumbers,
                        buttonsProperty: t.buttons,
                        locations: st.slice()
                    }),
                    r = T(ot.end, document.documentElement, I, {
                        handleNumbers: e.handleNumbers
                    }),
                    s = T("mouseout", document.documentElement, O, {
                        handleNumbers: e.handleNumbers
                    });
                if (document.documentElement.noUiListeners = n.concat(r, s), t.cursor) {
                    document.body.style.cursor = getComputedStyle(t.target).cursor, et.length > 1 && c(rt, o.cssClasses.drag);
                    var a = function() {
                        return !1
                    };
                    document.body.noUiListener = a, document.body.addEventListener("selectstart", a, !1)
                }
                e.handleNumbers.forEach(function(t) {
                    P("start", t)
                })
            }

            function F(t) {
                t.stopPropagation();
                var e = A(t.calcPoint),
                    i = E(e);
                return i !== !1 && (o.events.snap || r(rt, o.cssClasses.tap, o.animationDuration), B(i, e, !0, !0), z(), P("slide", i, !0), P("set", i, !0), P("change", i, !0), P("update", i, !0), void(o.events.snap && M(t, {
                        handleNumbers: [i]
                    })))
            }

            function D(t) {
                var e = A(t.calcPoint),
                    i = ct.getStep(e),
                    n = ct.fromStepping(i);
                Object.keys(dt).forEach(function(t) {
                    "hover" === t.split(".")[0] && dt[t].forEach(function(t) {
                        t.call(nt, n)
                    })
                })
            }

            function j(t) {
                t.fixed || et.forEach(function(t, e) {
                    T(ot.start, t.children[0], M, {
                        handleNumbers: [e]
                    })
                }), t.tap && T(ot.start, tt, F, {}), t.hover && T(ot.move, tt, D, {
                    hover: !0
                }), t.drag && it.forEach(function(e, i) {
                    if (e !== !1 && 0 !== i && i !== it.length - 1) {
                        var n = et[i - 1],
                            r = et[i],
                            s = [e];
                        c(e, o.cssClasses.draggable), t.fixed && (s.push(n.children[0]), s.push(r.children[0])), s.forEach(function(t) {
                            T(ot.start, t, M, {
                                handles: [n, r],
                                handleNumbers: [i - 1, i]
                            })
                        })
                    }
                })
            }

            function H(t, e, i, n, r) {
                return et.length > 1 && (n && e > 0 && (i = Math.max(i, t[e - 1] + o.margin)), r && e < et.length - 1 && (i = Math.min(i, t[e + 1] - o.margin))), et.length > 1 && o.limit && (n && e > 0 && (i = Math.min(i, t[e - 1] + o.limit)), r && e < et.length - 1 && (i = Math.max(i, t[e + 1] - o.limit))), o.padding && (0 === e && (i = Math.max(i, o.padding)), e === et.length - 1 && (i = Math.min(i, 100 - o.padding))), i = ct.getStep(i), i = s(i), i !== t[e] && i
            }

            function R(t) {
                return t + "%"
            }

            function W(t, e) {
                st[t] = e, ut[t] = ct.fromStepping(e);
                var i = function() {
                    et[t].style[o.style] = R(e), q(t), q(t + 1)
                };
                window.requestAnimationFrame && o.useRequestAnimationFrame ? window.requestAnimationFrame(i) : i()
            }

            function z() {
                at.forEach(function(t) {
                    var e = st[t] > 50 ? -1 : 1,
                        i = 3 + (et.length + e * t);
                    et[t].childNodes[0].style.zIndex = i
                })
            }

            function B(t, e, i, n) {
                return e = H(st, t, e, i, n), e !== !1 && (W(t, e), !0)
            }

            function q(t) {
                if (it[t]) {
                    var e = 0,
                        i = 100;
                    0 !== t && (e = st[t - 1]), t !== it.length - 1 && (i = st[t]), it[t].style[o.style] = R(e), it[t].style[o.styleOposite] = R(100 - i)
                }
            }

            function Y(t, e) {
                null !== t && t !== !1 && ("number" == typeof t && (t = String(t)), t = o.format.from(t), t === !1 || isNaN(t) || B(e, ct.toStepping(t), !1, !1))
            }

            function X(t, e) {
                var i = a(t),
                    n = void 0 === st[0];
                e = void 0 === e || !!e, i.forEach(Y), o.animate && !n && r(rt, o.cssClasses.tap, o.animationDuration), at.forEach(function(t) {
                    B(t, st[t], !0, !1)
                }), z(), at.forEach(function(t) {
                    P("update", t), null !== i[t] && e && P("set", t)
                })
            }

            function U(t) {
                X(o.start, t)
            }

            function K() {
                var t = ut.map(o.format.to);
                return 1 === t.length ? t[0] : t
            }

            function V() {
                for (var t in o.cssClasses) o.cssClasses.hasOwnProperty(t) && u(rt, o.cssClasses[t]);
                for (; rt.firstChild;) rt.removeChild(rt.firstChild);
                delete rt.noUiSlider
            }

            function Q() {
                return st.map(function(t, e) {
                    var i = ct.getNearbySteps(t),
                        n = ut[e],
                        o = i.thisStep.step,
                        r = null;
                    o !== !1 && n + o > i.stepAfter.startValue && (o = i.stepAfter.startValue - n), r = n > i.thisStep.startValue ? i.thisStep.step : i.stepBefore.step !== !1 && n - i.stepBefore.highestStep, 100 === t ? o = null : 0 === t && (r = null);
                    var s = ct.countStepDecimals();
                    return null !== o && o !== !1 && (o = Number(o.toFixed(s))), null !== r && r !== !1 && (r = Number(r.toFixed(s))), [r, o]
                })
            }

            function G(t, e) {
                dt[t] = dt[t] || [], dt[t].push(e), "update" === t.split(".")[0] && et.forEach(function(t, e) {
                    P("update", e)
                })
            }

            function Z(t) {
                var e = t && t.split(".")[0],
                    i = e && t.substring(e.length);
                Object.keys(dt).forEach(function(t) {
                    var n = t.split(".")[0],
                        o = t.substring(n.length);
                    e && e !== n || i && i !== o || delete dt[t]
                })
            }

            function J(t, e) {
                var i = K(),
                    n = ["margin", "limit", "padding", "range", "animate", "snap", "step", "format"];
                n.forEach(function(e) {
                    void 0 !== t[e] && (l[e] = t[e])
                });
                var r = N(l);
                n.forEach(function(e) {
                    void 0 !== t[e] && (o[e] = r[e])
                }), r.spectrum.direction = ct.direction, ct = r.spectrum, o.margin = r.margin, o.limit = r.limit, o.padding = r.padding, st = [], X(t.start || i, e)
            }
            var tt, et, it, nt, ot = f(),
                rt = i,
                st = [],
                at = [],
                lt = !1,
                ct = o.spectrum,
                ut = [],
                dt = {};
            if (rt.noUiSlider) throw new Error("Slider was already initialized.");
            return g(rt), v(o.connect, tt), nt = {
                destroy: V,
                steps: Q,
                on: G,
                off: Z,
                get: K,
                set: X,
                reset: U,
                __moveHandles: function(t, e, i) {
                    $(t, e, st, i)
                },
                options: l,
                updateOptions: J,
                target: rt,
                pips: x
            }, j(o.events), X(o.start), o.pips && x(o.pips), o.tooltips && b(), nt
        }

        function Y(t, e) {
            if (!t.nodeName) throw new Error("noUiSlider.create requires a single element.");
            var i = N(e, t),
                n = q(t, i, e);
            return t.noUiSlider = n, n
        }
        C.prototype.getMargin = function(t) {
            var e = this.xNumSteps[0];
            if (e && t / e % 1 !== 0) throw new Error("noUiSlider: 'limit', 'margin' and 'padding' must be divisible by step.");
            return 2 === this.xPct.length && m(this.xVal, t)
        }, C.prototype.toStepping = function(t) {
            return t = b(this.xVal, this.xPct, t)
        }, C.prototype.fromStepping = function(t) {
            return w(this.xVal, this.xPct, t)
        }, C.prototype.getStep = function(t) {
            return t = k(this.xPct, this.xSteps, this.snap, t)
        }, C.prototype.getNearbySteps = function(t) {
            var e = y(t, this.xPct);
            return {
                stepBefore: {
                    startValue: this.xVal[e - 2],
                    step: this.xNumSteps[e - 2],
                    highestStep: this.xHighestCompleteStep[e - 2]
                },
                thisStep: {
                    startValue: this.xVal[e - 1],
                    step: this.xNumSteps[e - 1],
                    highestStep: this.xHighestCompleteStep[e - 1]
                },
                stepAfter: {
                    startValue: this.xVal[e - 0],
                    step: this.xNumSteps[e - 0],
                    highestStep: this.xHighestCompleteStep[e - 0]
                }
            }
        }, C.prototype.countStepDecimals = function() {
            var t = this.xNumSteps.map(l);
            return Math.max.apply(null, t)
        }, C.prototype.convert = function(t) {
            return this.getStep(this.toStepping(t))
        };
        var X = {
            to: function(t) {
                return void 0 !== t && t.toFixed(2)
            },
            from: Number
        };
        return {
            create: Y
        }
    }), ! function t(e, i, n) {
    function o(s, a) {
        if (!i[s]) {
            if (!e[s]) {
                var l = "function" == typeof require && require;
                if (!a && l) return l(s, !0);
                if (r) return r(s, !0);
                var c = new Error("Cannot find module '" + s + "'");
                throw c.code = "MODULE_NOT_FOUND", c
            }
            var u = i[s] = {
                exports: {}
            };
            e[s][0].call(u.exports, function(t) {
                var i = e[s][1][t];
                return o(i ? i : t)
            }, u, u.exports, t, e, i, n)
        }
        return i[s].exports
    }
    for (var r = "function" == typeof require && require, s = 0; s < n.length; s++) o(n[s]);
    return o
}({
    1: [function(t, e, i) {
        "use strict";

        function n(t) {
            t.fn.perfectScrollbar = function(t) {
                return this.each(function() {
                    if ("object" == typeof t || "undefined" == typeof t) {
                        var e = t;
                        r.get(this) || o.initialize(this, e)
                    } else {
                        var i = t;
                        "update" === i ? o.update(this) : "destroy" === i && o.destroy(this)
                    }
                })
            }
        }
        var o = t("../main"),
            r = t("../plugin/instances");
        if ("function" == typeof define && define.amd) define(["jquery"], n);
        else {
            var s = window.jQuery ? window.jQuery : window.$;
            "undefined" != typeof s && n(s)
        }
        e.exports = n
    }, {
        "../main": 7,
        "../plugin/instances": 18
    }],
    2: [function(t, e, i) {
        "use strict";

        function n(t, e) {
            var i = t.className.split(" ");
            i.indexOf(e) < 0 && i.push(e), t.className = i.join(" ")
        }

        function o(t, e) {
            var i = t.className.split(" "),
                n = i.indexOf(e);
            n >= 0 && i.splice(n, 1), t.className = i.join(" ")
        }
        i.add = function(t, e) {
            t.classList ? t.classList.add(e) : n(t, e)
        }, i.remove = function(t, e) {
            t.classList ? t.classList.remove(e) : o(t, e)
        }, i.list = function(t) {
            return t.classList ? Array.prototype.slice.apply(t.classList) : t.className.split(" ")
        }
    }, {}],
    3: [function(t, e, i) {
        "use strict";

        function n(t, e) {
            return window.getComputedStyle(t)[e]
        }

        function o(t, e, i) {
            return "number" == typeof i && (i = i.toString() + "px"), t.style[e] = i, t
        }

        function r(t, e) {
            for (var i in e) {
                var n = e[i];
                "number" == typeof n && (n = n.toString() + "px"), t.style[i] = n
            }
            return t
        }
        var s = {};
        s.e = function(t, e) {
            var i = document.createElement(t);
            return i.className = e, i
        }, s.appendTo = function(t, e) {
            return e.appendChild(t), t
        }, s.css = function(t, e, i) {
            return "object" == typeof e ? r(t, e) : "undefined" == typeof i ? n(t, e) : o(t, e, i)
        }, s.matches = function(t, e) {
            return "undefined" != typeof t.matches ? t.matches(e) : "undefined" != typeof t.matchesSelector ? t.matchesSelector(e) : "undefined" != typeof t.webkitMatchesSelector ? t.webkitMatchesSelector(e) : "undefined" != typeof t.mozMatchesSelector ? t.mozMatchesSelector(e) : "undefined" != typeof t.msMatchesSelector ? t.msMatchesSelector(e) : void 0
        }, s.remove = function(t) {
            "undefined" != typeof t.remove ? t.remove() : t.parentNode && t.parentNode.removeChild(t)
        }, s.queryChildren = function(t, e) {
            return Array.prototype.filter.call(t.childNodes, function(t) {
                return s.matches(t, e)
            })
        }, e.exports = s
    }, {}],
    4: [function(t, e, i) {
        "use strict";
        var n = function(t) {
            this.element = t, this.events = {}
        };
        n.prototype.bind = function(t, e) {
            "undefined" == typeof this.events[t] && (this.events[t] = []), this.events[t].push(e), this.element.addEventListener(t, e, !1)
        }, n.prototype.unbind = function(t, e) {
            var i = "undefined" != typeof e;
            this.events[t] = this.events[t].filter(function(n) {
                return !(!i || n === e) || (this.element.removeEventListener(t, n, !1), !1)
            }, this)
        }, n.prototype.unbindAll = function() {
            for (var t in this.events) this.unbind(t)
        };
        var o = function() {
            this.eventElements = []
        };
        o.prototype.eventElement = function(t) {
            var e = this.eventElements.filter(function(e) {
                return e.element === t
            })[0];
            return "undefined" == typeof e && (e = new n(t), this.eventElements.push(e)), e
        }, o.prototype.bind = function(t, e, i) {
            this.eventElement(t).bind(e, i)
        }, o.prototype.unbind = function(t, e, i) {
            this.eventElement(t).unbind(e, i)
        }, o.prototype.unbindAll = function() {
            for (var t = 0; t < this.eventElements.length; t++) this.eventElements[t].unbindAll()
        }, o.prototype.once = function(t, e, i) {
            var n = this.eventElement(t),
                o = function(t) {
                    n.unbind(e, o), i(t)
                };
            n.bind(e, o)
        }, e.exports = o
    }, {}],
    5: [function(t, e, i) {
        "use strict";
        e.exports = function() {
            function t() {
                return Math.floor(65536 * (1 + Math.random())).toString(16).substring(1)
            }
            return function() {
                return t() + t() + "-" + t() + "-" + t() + "-" + t() + "-" + t() + t() + t()
            }
        }()
    }, {}],
    6: [function(t, e, i) {
        "use strict";
        var n = t("./class"),
            o = t("./dom"),
            r = i.toInt = function(t) {
                return parseInt(t, 10) || 0
            },
            s = i.clone = function(t) {
                if (t) {
                    if (t.constructor === Array) return t.map(s);
                    if ("object" == typeof t) {
                        var e = {};
                        for (var i in t) e[i] = s(t[i]);
                        return e
                    }
                    return t
                }
                return null
            };
        i.extend = function(t, e) {
            var i = s(t);
            for (var n in e) i[n] = s(e[n]);
            return i
        }, i.isEditable = function(t) {
            return o.matches(t, "input,[contenteditable]") || o.matches(t, "select,[contenteditable]") || o.matches(t, "textarea,[contenteditable]") || o.matches(t, "button,[contenteditable]")
        }, i.removePsClasses = function(t) {
            for (var e = n.list(t), i = 0; i < e.length; i++) {
                var o = e[i];
                0 === o.indexOf("ps-") && n.remove(t, o)
            }
        }, i.outerWidth = function(t) {
            return r(o.css(t, "width")) + r(o.css(t, "paddingLeft")) + r(o.css(t, "paddingRight")) + r(o.css(t, "borderLeftWidth")) + r(o.css(t, "borderRightWidth"))
        }, i.startScrolling = function(t, e) {
            n.add(t, "ps-in-scrolling"), "undefined" != typeof e ? n.add(t, "ps-" + e) : (n.add(t, "ps-x"), n.add(t, "ps-y"))
        }, i.stopScrolling = function(t, e) {
            n.remove(t, "ps-in-scrolling"), "undefined" != typeof e ? n.remove(t, "ps-" + e) : (n.remove(t, "ps-x"), n.remove(t, "ps-y"))
        }, i.env = {
            isWebKit: "WebkitAppearance" in document.documentElement.style,
            supportsTouch: "ontouchstart" in window || window.DocumentTouch && document instanceof window.DocumentTouch,
            supportsIePointer: null !== window.navigator.msMaxTouchPoints
        }
    }, {
        "./class": 2,
        "./dom": 3
    }],
    7: [function(t, e, i) {
        "use strict";
        var n = t("./plugin/destroy"),
            o = t("./plugin/initialize"),
            r = t("./plugin/update");
        e.exports = {
            initialize: o,
            update: r,
            destroy: n
        }
    }, {
        "./plugin/destroy": 9,
        "./plugin/initialize": 17,
        "./plugin/update": 21
    }],
    8: [function(t, e, i) {
        "use strict";
        e.exports = {
            handlers: ["click-rail", "drag-scrollbar", "keyboard", "wheel", "touch"],
            maxScrollbarLength: null,
            minScrollbarLength: null,
            scrollXMarginOffset: 0,
            scrollYMarginOffset: 0,
            suppressScrollX: !1,
            suppressScrollY: !1,
            swipePropagation: !0,
            useBothWheelAxes: !1,
            wheelPropagation: !1,
            wheelSpeed: 1,
            theme: "default"
        }
    }, {}],
    9: [function(t, e, i) {
        "use strict";
        var n = t("../lib/helper"),
            o = t("../lib/dom"),
            r = t("./instances");
        e.exports = function(t) {
            var e = r.get(t);
            e && (e.event.unbindAll(), o.remove(e.scrollbarX), o.remove(e.scrollbarY), o.remove(e.scrollbarXRail), o.remove(e.scrollbarYRail), n.removePsClasses(t), r.remove(t))
        }
    }, {
        "../lib/dom": 3,
        "../lib/helper": 6,
        "./instances": 18
    }],
    10: [function(t, e, i) {
        "use strict";

        function n(t, e) {
            function i(t) {
                return t.getBoundingClientRect()
            }
            var n = function(t) {
                t.stopPropagation()
            };
            e.event.bind(e.scrollbarY, "click", n), e.event.bind(e.scrollbarYRail, "click", function(n) {
                var o = n.pageY - window.pageYOffset - i(e.scrollbarYRail).top,
                    a = o > e.scrollbarYTop ? 1 : -1;
                s(t, "top", t.scrollTop + a * e.containerHeight), r(t), n.stopPropagation()
            }), e.event.bind(e.scrollbarX, "click", n), e.event.bind(e.scrollbarXRail, "click", function(n) {
                var o = n.pageX - window.pageXOffset - i(e.scrollbarXRail).left,
                    a = o > e.scrollbarXLeft ? 1 : -1;
                s(t, "left", t.scrollLeft + a * e.containerWidth), r(t), n.stopPropagation()
            })
        }
        var o = t("../instances"),
            r = t("../update-geometry"),
            s = t("../update-scroll");
        e.exports = function(t) {
            var e = o.get(t);
            n(t, e)
        }
    }, {
        "../instances": 18,
        "../update-geometry": 19,
        "../update-scroll": 20
    }],
    11: [function(t, e, i) {
        "use strict";

        function n(t, e) {
            function i(i) {
                var o = n + i * e.railXRatio,
                    s = Math.max(0, e.scrollbarXRail.getBoundingClientRect().left) + e.railXRatio * (e.railXWidth - e.scrollbarXWidth);
                o < 0 ? e.scrollbarXLeft = 0 : o > s ? e.scrollbarXLeft = s : e.scrollbarXLeft = o;
                var a = r.toInt(e.scrollbarXLeft * (e.contentWidth - e.containerWidth) / (e.containerWidth - e.railXRatio * e.scrollbarXWidth)) - e.negativeScrollAdjustment;
                c(t, "left", a)
            }
            var n = null,
                o = null,
                a = function(e) {
                    i(e.pageX - o), l(t), e.stopPropagation(), e.preventDefault()
                },
                u = function() {
                    r.stopScrolling(t, "x"), e.event.unbind(e.ownerDocument, "mousemove", a)
                };
            e.event.bind(e.scrollbarX, "mousedown", function(i) {
                o = i.pageX, n = r.toInt(s.css(e.scrollbarX, "left")) * e.railXRatio, r.startScrolling(t, "x"), e.event.bind(e.ownerDocument, "mousemove", a), e.event.once(e.ownerDocument, "mouseup", u), i.stopPropagation(), i.preventDefault()
            })
        }

        function o(t, e) {
            function i(i) {
                var o = n + i * e.railYRatio,
                    s = Math.max(0, e.scrollbarYRail.getBoundingClientRect().top) + e.railYRatio * (e.railYHeight - e.scrollbarYHeight);
                o < 0 ? e.scrollbarYTop = 0 : o > s ? e.scrollbarYTop = s : e.scrollbarYTop = o;
                var a = r.toInt(e.scrollbarYTop * (e.contentHeight - e.containerHeight) / (e.containerHeight - e.railYRatio * e.scrollbarYHeight));
                c(t, "top", a)
            }
            var n = null,
                o = null,
                a = function(e) {
                    i(e.pageY - o), l(t), e.stopPropagation(), e.preventDefault()
                },
                u = function() {
                    r.stopScrolling(t, "y"), e.event.unbind(e.ownerDocument, "mousemove", a)
                };
            e.event.bind(e.scrollbarY, "mousedown", function(i) {
                o = i.pageY, n = r.toInt(s.css(e.scrollbarY, "top")) * e.railYRatio, r.startScrolling(t, "y"), e.event.bind(e.ownerDocument, "mousemove", a), e.event.once(e.ownerDocument, "mouseup", u), i.stopPropagation(), i.preventDefault()
            })
        }
        var r = t("../../lib/helper"),
            s = t("../../lib/dom"),
            a = t("../instances"),
            l = t("../update-geometry"),
            c = t("../update-scroll");
        e.exports = function(t) {
            var e = a.get(t);
            n(t, e), o(t, e)
        }
    }, {
        "../../lib/dom": 3,
        "../../lib/helper": 6,
        "../instances": 18,
        "../update-geometry": 19,
        "../update-scroll": 20
    }],
    12: [function(t, e, i) {
        "use strict";

        function n(t, e) {
            function i(i, n) {
                var o = t.scrollTop;
                if (0 === i) {
                    if (!e.scrollbarYActive) return !1;
                    if (0 === o && n > 0 || o >= e.contentHeight - e.containerHeight && n < 0) return !e.settings.wheelPropagation
                }
                var r = t.scrollLeft;
                if (0 === n) {
                    if (!e.scrollbarXActive) return !1;
                    if (0 === r && i < 0 || r >= e.contentWidth - e.containerWidth && i > 0) return !e.settings.wheelPropagation
                }
                return !0
            }
            var n = !1;
            e.event.bind(t, "mouseenter", function() {
                n = !0
            }), e.event.bind(t, "mouseleave", function() {
                n = !1
            });
            var s = !1;
            e.event.bind(e.ownerDocument, "keydown", function(c) {
                if (!(c.isDefaultPrevented && c.isDefaultPrevented() || c.defaultPrevented)) {
                    var u = r.matches(e.scrollbarX, ":focus") || r.matches(e.scrollbarY, ":focus");
                    if (n || u) {
                        var d = document.activeElement ? document.activeElement : e.ownerDocument.activeElement;
                        if (d) {
                            if ("IFRAME" === d.tagName) d = d.contentDocument.activeElement;
                            else
                                for (; d.shadowRoot;) d = d.shadowRoot.activeElement;
                            if (o.isEditable(d)) return
                        }
                        var p = 0,
                            f = 0;
                        switch (c.which) {
                            case 37:
                                p = c.metaKey ? -e.contentWidth : c.altKey ? -e.containerWidth : -30;
                                break;
                            case 38:
                                f = c.metaKey ? e.contentHeight : c.altKey ? e.containerHeight : 30;
                                break;
                            case 39:
                                p = c.metaKey ? e.contentWidth : c.altKey ? e.containerWidth : 30;
                                break;
                            case 40:
                                f = c.metaKey ? -e.contentHeight : c.altKey ? -e.containerHeight : -30;
                                break;
                            case 33:
                                f = 90;
                                break;
                            case 32:
                                f = c.shiftKey ? 90 : -90;
                                break;
                            case 34:
                                f = -90;
                                break;
                            case 35:
                                f = c.ctrlKey ? -e.contentHeight : -e.containerHeight;
                                break;
                            case 36:
                                f = c.ctrlKey ? t.scrollTop : e.containerHeight;
                                break;
                            default:
                                return
                        }
                        l(t, "top", t.scrollTop - f), l(t, "left", t.scrollLeft + p), a(t), s = i(p, f), s && c.preventDefault()
                    }
                }
            })
        }
        var o = t("../../lib/helper"),
            r = t("../../lib/dom"),
            s = t("../instances"),
            a = t("../update-geometry"),
            l = t("../update-scroll");
        e.exports = function(t) {
            var e = s.get(t);
            n(t, e)
        }
    }, {
        "../../lib/dom": 3,
        "../../lib/helper": 6,
        "../instances": 18,
        "../update-geometry": 19,
        "../update-scroll": 20
    }],
    13: [function(t, e, i) {
        "use strict";

        function n(t, e) {
            function i(i, n) {
                var o = t.scrollTop;
                if (0 === i) {
                    if (!e.scrollbarYActive) return !1;
                    if (0 === o && n > 0 || o >= e.contentHeight - e.containerHeight && n < 0) return !e.settings.wheelPropagation
                }
                var r = t.scrollLeft;
                if (0 === n) {
                    if (!e.scrollbarXActive) return !1;
                    if (0 === r && i < 0 || r >= e.contentWidth - e.containerWidth && i > 0) return !e.settings.wheelPropagation
                }
                return !0
            }

            function n(t) {
                var e = t.deltaX,
                    i = -1 * t.deltaY;
                return "undefined" != typeof e && "undefined" != typeof i || (e = -1 * t.wheelDeltaX / 6, i = t.wheelDeltaY / 6), t.deltaMode && 1 === t.deltaMode && (e *= 10, i *= 10), e !== e && i !== i && (e = 0, i = t.wheelDelta), t.shiftKey ? [-i, -e] : [e, i]
            }

            function o(e, i) {
                var n = t.querySelector("textarea:hover, select[multiple]:hover, .ps-child:hover");
                if (n) {
                    if (!window.getComputedStyle(n).overflow.match(/(scroll|auto)/)) return !1;
                    var o = n.scrollHeight - n.clientHeight;
                    if (o > 0 && !(0 === n.scrollTop && i > 0 || n.scrollTop === o && i < 0)) return !0;
                    var r = n.scrollLeft - n.clientWidth;
                    if (r > 0 && !(0 === n.scrollLeft && e < 0 || n.scrollLeft === r && e > 0)) return !0
                }
                return !1
            }

            function a(a) {
                var c = n(a),
                    u = c[0],
                    d = c[1];
                o(u, d) || (l = !1, e.settings.useBothWheelAxes ? e.scrollbarYActive && !e.scrollbarXActive ? (d ? s(t, "top", t.scrollTop - d * e.settings.wheelSpeed) : s(t, "top", t.scrollTop + u * e.settings.wheelSpeed), l = !0) : e.scrollbarXActive && !e.scrollbarYActive && (u ? s(t, "left", t.scrollLeft + u * e.settings.wheelSpeed) : s(t, "left", t.scrollLeft - d * e.settings.wheelSpeed), l = !0) : (s(t, "top", t.scrollTop - d * e.settings.wheelSpeed), s(t, "left", t.scrollLeft + u * e.settings.wheelSpeed)), r(t), l = l || i(u, d), l && (a.stopPropagation(), a.preventDefault()))
            }
            var l = !1;
            "undefined" != typeof window.onwheel ? e.event.bind(t, "wheel", a) : "undefined" != typeof window.onmousewheel && e.event.bind(t, "mousewheel", a)
        }
        var o = t("../instances"),
            r = t("../update-geometry"),
            s = t("../update-scroll");
        e.exports = function(t) {
            var e = o.get(t);
            n(t, e)
        }
    }, {
        "../instances": 18,
        "../update-geometry": 19,
        "../update-scroll": 20
    }],
    14: [function(t, e, i) {
        "use strict";

        function n(t, e) {
            e.event.bind(t, "scroll", function() {
                r(t)
            })
        }
        var o = t("../instances"),
            r = t("../update-geometry");
        e.exports = function(t) {
            var e = o.get(t);
            n(t, e)
        }
    }, {
        "../instances": 18,
        "../update-geometry": 19
    }],
    15: [function(t, e, i) {
        "use strict";

        function n(t, e) {
            function i() {
                var t = window.getSelection ? window.getSelection() : document.getSelection ? document.getSelection() : "";
                return 0 === t.toString().length ? null : t.getRangeAt(0).commonAncestorContainer
            }

            function n() {
                c || (c = setInterval(function() {
                    return r.get(t) ? (a(t, "top", t.scrollTop + u.top), a(t, "left", t.scrollLeft + u.left), void s(t)) : void clearInterval(c)
                }, 50))
            }

            function l() {
                c && (clearInterval(c), c = null), o.stopScrolling(t)
            }
            var c = null,
                u = {
                    top: 0,
                    left: 0
                },
                d = !1;
            e.event.bind(e.ownerDocument, "selectionchange", function() {
                t.contains(i()) ? d = !0 : (d = !1, l())
            }), e.event.bind(window, "mouseup", function() {
                d && (d = !1, l())
            }), e.event.bind(window, "keyup", function() {
                d && (d = !1, l())
            }), e.event.bind(window, "mousemove", function(e) {
                if (d) {
                    var i = {
                            x: e.pageX,
                            y: e.pageY
                        },
                        r = {
                            left: t.offsetLeft,
                            right: t.offsetLeft + t.offsetWidth,
                            top: t.offsetTop,
                            bottom: t.offsetTop + t.offsetHeight
                        };
                    i.x < r.left + 3 ? (u.left = -5, o.startScrolling(t, "x")) : i.x > r.right - 3 ? (u.left = 5, o.startScrolling(t, "x")) : u.left = 0, i.y < r.top + 3 ? (r.top + 3 - i.y < 5 ? u.top = -5 : u.top = -20, o.startScrolling(t, "y")) : i.y > r.bottom - 3 ? (i.y - r.bottom + 3 < 5 ? u.top = 5 : u.top = 20, o.startScrolling(t, "y")) : u.top = 0, 0 === u.top && 0 === u.left ? l() : n()
                }
            })
        }
        var o = t("../../lib/helper"),
            r = t("../instances"),
            s = t("../update-geometry"),
            a = t("../update-scroll");
        e.exports = function(t) {
            var e = r.get(t);
            n(t, e)
        }
    }, {
        "../../lib/helper": 6,
        "../instances": 18,
        "../update-geometry": 19,
        "../update-scroll": 20
    }],
    16: [function(t, e, i) {
        "use strict";

        function n(t, e, i, n) {
            function o(i, n) {
                var o = t.scrollTop,
                    r = t.scrollLeft,
                    s = Math.abs(i),
                    a = Math.abs(n);
                if (a > s) {
                    if (n < 0 && o === e.contentHeight - e.containerHeight || n > 0 && 0 === o) return !e.settings.swipePropagation
                } else if (s > a && (i < 0 && r === e.contentWidth - e.containerWidth || i > 0 && 0 === r)) return !e.settings.swipePropagation;
                return !0
            }

            function l(e, i) {
                a(t, "top", t.scrollTop - i), a(t, "left", t.scrollLeft - e), s(t)
            }

            function c() {
                w = !0
            }

            function u() {
                w = !1
            }

            function d(t) {
                return t.targetTouches ? t.targetTouches[0] : t
            }

            function p(t) {
                return !(!t.targetTouches || 1 !== t.targetTouches.length) || !(!t.pointerType || "mouse" === t.pointerType || t.pointerType === t.MSPOINTER_TYPE_MOUSE)
            }

            function f(t) {
                if (p(t)) {
                    k = !0;
                    var e = d(t);
                    v.pageX = e.pageX, v.pageY = e.pageY, g = (new Date).getTime(), null !== b && clearInterval(b), t.stopPropagation()
                }
            }

            function h(t) {
                if (!k && e.settings.swipePropagation && f(t), !w && k && p(t)) {
                    var i = d(t),
                        n = {
                            pageX: i.pageX,
                            pageY: i.pageY
                        },
                        r = n.pageX - v.pageX,
                        s = n.pageY - v.pageY;
                    l(r, s), v = n;
                    var a = (new Date).getTime(),
                        c = a - g;
                    c > 0 && (y.x = r / c, y.y = s / c, g = a), o(r, s) && (t.stopPropagation(), t.preventDefault())
                }
            }

            function m() {
                !w && k && (k = !1, clearInterval(b), b = setInterval(function() {
                    return r.get(t) && (y.x || y.y) ? Math.abs(y.x) < .01 && Math.abs(y.y) < .01 ? void clearInterval(b) : (l(30 * y.x, 30 * y.y), y.x *= .8, void(y.y *= .8)) : void clearInterval(b)
                }, 10))
            }
            var v = {},
                g = 0,
                y = {},
                b = null,
                w = !1,
                k = !1;
            i && (e.event.bind(window, "touchstart", c), e.event.bind(window, "touchend", u), e.event.bind(t, "touchstart", f), e.event.bind(t, "touchmove", h), e.event.bind(t, "touchend", m)), n && (window.PointerEvent ? (e.event.bind(window, "pointerdown", c), e.event.bind(window, "pointerup", u), e.event.bind(t, "pointerdown", f), e.event.bind(t, "pointermove", h), e.event.bind(t, "pointerup", m)) : window.MSPointerEvent && (e.event.bind(window, "MSPointerDown", c), e.event.bind(window, "MSPointerUp", u), e.event.bind(t, "MSPointerDown", f), e.event.bind(t, "MSPointerMove", h), e.event.bind(t, "MSPointerUp", m)))
        }
        var o = t("../../lib/helper"),
            r = t("../instances"),
            s = t("../update-geometry"),
            a = t("../update-scroll");
        e.exports = function(t) {
            if (o.env.supportsTouch || o.env.supportsIePointer) {
                var e = r.get(t);
                n(t, e, o.env.supportsTouch, o.env.supportsIePointer)
            }
        }
    }, {
        "../../lib/helper": 6,
        "../instances": 18,
        "../update-geometry": 19,
        "../update-scroll": 20
    }],
    17: [function(t, e, i) {
        "use strict";
        var n = t("../lib/helper"),
            o = t("../lib/class"),
            r = t("./instances"),
            s = t("./update-geometry"),
            a = {
                "click-rail": t("./handler/click-rail"),
                "drag-scrollbar": t("./handler/drag-scrollbar"),
                keyboard: t("./handler/keyboard"),
                wheel: t("./handler/mouse-wheel"),
                touch: t("./handler/touch"),
                selection: t("./handler/selection")
            },
            l = t("./handler/native-scroll");
        e.exports = function(t, e) {
            e = "object" == typeof e ? e : {}, o.add(t, "ps-container");
            var i = r.add(t);
            i.settings = n.extend(i.settings, e), o.add(t, "ps-theme-" + i.settings.theme), i.settings.handlers.forEach(function(e) {
                a[e](t)
            }), l(t), s(t)
        }
    }, {
        "../lib/class": 2,
        "../lib/helper": 6,
        "./handler/click-rail": 10,
        "./handler/drag-scrollbar": 11,
        "./handler/keyboard": 12,
        "./handler/mouse-wheel": 13,
        "./handler/native-scroll": 14,
        "./handler/selection": 15,
        "./handler/touch": 16,
        "./instances": 18,
        "./update-geometry": 19
    }],
    18: [function(t, e, i) {
        "use strict";

        function n(t) {
            function e() {
                l.add(t, "ps-focus")
            }

            function i() {
                l.remove(t, "ps-focus")
            }
            var n = this;
            n.settings = a.clone(c), n.containerWidth = null, n.containerHeight = null, n.contentWidth = null, n.contentHeight = null, n.isRtl = "rtl" === u.css(t, "direction"), n.isNegativeScroll = function() {
                var e = t.scrollLeft,
                    i = null;
                return t.scrollLeft = -1, i = t.scrollLeft < 0, t.scrollLeft = e, i
            }(), n.negativeScrollAdjustment = n.isNegativeScroll ? t.scrollWidth - t.clientWidth : 0, n.event = new d, n.ownerDocument = t.ownerDocument || document, n.scrollbarXRail = u.appendTo(u.e("div", "ps-scrollbar-x-rail"), t), n.scrollbarX = u.appendTo(u.e("div", "ps-scrollbar-x"), n.scrollbarXRail), n.scrollbarX.setAttribute("tabindex", 0), n.event.bind(n.scrollbarX, "focus", e), n.event.bind(n.scrollbarX, "blur", i), n.scrollbarXActive = null, n.scrollbarXWidth = null, n.scrollbarXLeft = null, n.scrollbarXBottom = a.toInt(u.css(n.scrollbarXRail, "bottom")), n.isScrollbarXUsingBottom = n.scrollbarXBottom === n.scrollbarXBottom, n.scrollbarXTop = n.isScrollbarXUsingBottom ? null : a.toInt(u.css(n.scrollbarXRail, "top")), n.railBorderXWidth = a.toInt(u.css(n.scrollbarXRail, "borderLeftWidth")) + a.toInt(u.css(n.scrollbarXRail, "borderRightWidth")), u.css(n.scrollbarXRail, "display", "block"), n.railXMarginWidth = a.toInt(u.css(n.scrollbarXRail, "marginLeft")) + a.toInt(u.css(n.scrollbarXRail, "marginRight")),
                u.css(n.scrollbarXRail, "display", ""), n.railXWidth = null, n.railXRatio = null, n.scrollbarYRail = u.appendTo(u.e("div", "ps-scrollbar-y-rail"), t), n.scrollbarY = u.appendTo(u.e("div", "ps-scrollbar-y"), n.scrollbarYRail), n.scrollbarY.setAttribute("tabindex", 0), n.event.bind(n.scrollbarY, "focus", e), n.event.bind(n.scrollbarY, "blur", i), n.scrollbarYActive = null, n.scrollbarYHeight = null, n.scrollbarYTop = null, n.scrollbarYRight = a.toInt(u.css(n.scrollbarYRail, "right")), n.isScrollbarYUsingRight = n.scrollbarYRight === n.scrollbarYRight, n.scrollbarYLeft = n.isScrollbarYUsingRight ? null : a.toInt(u.css(n.scrollbarYRail, "left")), n.scrollbarYOuterWidth = n.isRtl ? a.outerWidth(n.scrollbarY) : null, n.railBorderYWidth = a.toInt(u.css(n.scrollbarYRail, "borderTopWidth")) + a.toInt(u.css(n.scrollbarYRail, "borderBottomWidth")), u.css(n.scrollbarYRail, "display", "block"), n.railYMarginHeight = a.toInt(u.css(n.scrollbarYRail, "marginTop")) + a.toInt(u.css(n.scrollbarYRail, "marginBottom")), u.css(n.scrollbarYRail, "display", ""), n.railYHeight = null, n.railYRatio = null
        }

        function o(t) {
            return t.getAttribute("data-ps-id")
        }

        function r(t, e) {
            t.setAttribute("data-ps-id", e)
        }

        function s(t) {
            t.removeAttribute("data-ps-id")
        }
        var a = t("../lib/helper"),
            l = t("../lib/class"),
            c = t("./default-setting"),
            u = t("../lib/dom"),
            d = t("../lib/event-manager"),
            p = t("../lib/guid"),
            f = {};
        i.add = function(t) {
            var e = p();
            return r(t, e), f[e] = new n(t), f[e]
        }, i.remove = function(t) {
            delete f[o(t)], s(t)
        }, i.get = function(t) {
            return f[o(t)]
        }
    }, {
        "../lib/class": 2,
        "../lib/dom": 3,
        "../lib/event-manager": 4,
        "../lib/guid": 5,
        "../lib/helper": 6,
        "./default-setting": 8
    }],
    19: [function(t, e, i) {
        "use strict";

        function n(t, e) {
            return t.settings.minScrollbarLength && (e = Math.max(e, t.settings.minScrollbarLength)), t.settings.maxScrollbarLength && (e = Math.min(e, t.settings.maxScrollbarLength)), e
        }

        function o(t, e) {
            var i = {
                width: e.railXWidth
            };
            e.isRtl ? i.left = e.negativeScrollAdjustment + t.scrollLeft + e.containerWidth - e.contentWidth : i.left = t.scrollLeft, e.isScrollbarXUsingBottom ? i.bottom = e.scrollbarXBottom - t.scrollTop : i.top = e.scrollbarXTop + t.scrollTop, a.css(e.scrollbarXRail, i);
            var n = {
                top: t.scrollTop,
                height: e.railYHeight
            };
            e.isScrollbarYUsingRight ? e.isRtl ? n.right = e.contentWidth - (e.negativeScrollAdjustment + t.scrollLeft) - e.scrollbarYRight - e.scrollbarYOuterWidth : n.right = e.scrollbarYRight - t.scrollLeft : e.isRtl ? n.left = e.negativeScrollAdjustment + t.scrollLeft + 2 * e.containerWidth - e.contentWidth - e.scrollbarYLeft - e.scrollbarYOuterWidth : n.left = e.scrollbarYLeft + t.scrollLeft, a.css(e.scrollbarYRail, n), a.css(e.scrollbarX, {
                left: e.scrollbarXLeft,
                width: e.scrollbarXWidth - e.railBorderXWidth
            }), a.css(e.scrollbarY, {
                top: e.scrollbarYTop,
                height: e.scrollbarYHeight - e.railBorderYWidth
            })
        }
        var r = t("../lib/helper"),
            s = t("../lib/class"),
            a = t("../lib/dom"),
            l = t("./instances"),
            c = t("./update-scroll");
        e.exports = function(t) {
            var e = l.get(t);
            e.containerWidth = t.clientWidth, e.containerHeight = t.clientHeight, e.contentWidth = t.scrollWidth, e.contentHeight = t.scrollHeight;
            var i;
            t.contains(e.scrollbarXRail) || (i = a.queryChildren(t, ".ps-scrollbar-x-rail"), i.length > 0 && i.forEach(function(t) {
                a.remove(t)
            }), a.appendTo(e.scrollbarXRail, t)), t.contains(e.scrollbarYRail) || (i = a.queryChildren(t, ".ps-scrollbar-y-rail"), i.length > 0 && i.forEach(function(t) {
                a.remove(t)
            }), a.appendTo(e.scrollbarYRail, t)), !e.settings.suppressScrollX && e.containerWidth + e.settings.scrollXMarginOffset < e.contentWidth ? (e.scrollbarXActive = !0, e.railXWidth = e.containerWidth - e.railXMarginWidth, e.railXRatio = e.containerWidth / e.railXWidth, e.scrollbarXWidth = n(e, r.toInt(e.railXWidth * e.containerWidth / e.contentWidth)), e.scrollbarXLeft = r.toInt((e.negativeScrollAdjustment + t.scrollLeft) * (e.railXWidth - e.scrollbarXWidth) / (e.contentWidth - e.containerWidth))) : e.scrollbarXActive = !1, !e.settings.suppressScrollY && e.containerHeight + e.settings.scrollYMarginOffset < e.contentHeight ? (e.scrollbarYActive = !0, e.railYHeight = e.containerHeight - e.railYMarginHeight, e.railYRatio = e.containerHeight / e.railYHeight, e.scrollbarYHeight = n(e, r.toInt(e.railYHeight * e.containerHeight / e.contentHeight)), e.scrollbarYTop = r.toInt(t.scrollTop * (e.railYHeight - e.scrollbarYHeight) / (e.contentHeight - e.containerHeight))) : e.scrollbarYActive = !1, e.scrollbarXLeft >= e.railXWidth - e.scrollbarXWidth && (e.scrollbarXLeft = e.railXWidth - e.scrollbarXWidth), e.scrollbarYTop >= e.railYHeight - e.scrollbarYHeight && (e.scrollbarYTop = e.railYHeight - e.scrollbarYHeight), o(t, e), e.scrollbarXActive ? s.add(t, "ps-active-x") : (s.remove(t, "ps-active-x"), e.scrollbarXWidth = 0, e.scrollbarXLeft = 0, c(t, "left", 0)), e.scrollbarYActive ? s.add(t, "ps-active-y") : (s.remove(t, "ps-active-y"), e.scrollbarYHeight = 0, e.scrollbarYTop = 0, c(t, "top", 0))
        }
    }, {
        "../lib/class": 2,
        "../lib/dom": 3,
        "../lib/helper": 6,
        "./instances": 18,
        "./update-scroll": 20
    }],
    20: [function(t, e, i) {
        "use strict";
        var n, o, r = t("./instances"),
            s = function(t) {
                var e = document.createEvent("Event");
                return e.initEvent(t, !0, !0), e
            };
        e.exports = function(t, e, i) {
            if ("undefined" == typeof t) throw "You must provide an element to the update-scroll function";
            if ("undefined" == typeof e) throw "You must provide an axis to the update-scroll function";
            if ("undefined" == typeof i) throw "You must provide a value to the update-scroll function";
            "top" === e && i <= 0 && (t.scrollTop = i = 0, t.dispatchEvent(s("ps-y-reach-start"))), "left" === e && i <= 0 && (t.scrollLeft = i = 0, t.dispatchEvent(s("ps-x-reach-start")));
            var a = r.get(t);
            "top" === e && i >= a.contentHeight - a.containerHeight && (i = a.contentHeight - a.containerHeight, i - t.scrollTop <= 1 ? i = t.scrollTop : t.scrollTop = i, t.dispatchEvent(s("ps-y-reach-end"))), "left" === e && i >= a.contentWidth - a.containerWidth && (i = a.contentWidth - a.containerWidth, i - t.scrollLeft <= 1 ? i = t.scrollLeft : t.scrollLeft = i, t.dispatchEvent(s("ps-x-reach-end"))), n || (n = t.scrollTop), o || (o = t.scrollLeft), "top" === e && i < n && t.dispatchEvent(s("ps-scroll-up")), "top" === e && i > n && t.dispatchEvent(s("ps-scroll-down")), "left" === e && i < o && t.dispatchEvent(s("ps-scroll-left")), "left" === e && i > o && t.dispatchEvent(s("ps-scroll-right")), "top" === e && (t.scrollTop = n = i, t.dispatchEvent(s("ps-scroll-y"))), "left" === e && (t.scrollLeft = o = i, t.dispatchEvent(s("ps-scroll-x")))
        }
    }, {
        "./instances": 18
    }],
    21: [function(t, e, i) {
        "use strict";
        var n = t("../lib/helper"),
            o = t("../lib/dom"),
            r = t("./instances"),
            s = t("./update-geometry"),
            a = t("./update-scroll");
        e.exports = function(t) {
            var e = r.get(t);
            e && (e.negativeScrollAdjustment = e.isNegativeScroll ? t.scrollWidth - t.clientWidth : 0, o.css(e.scrollbarXRail, "display", "block"), o.css(e.scrollbarYRail, "display", "block"), e.railXMarginWidth = n.toInt(o.css(e.scrollbarXRail, "marginLeft")) + n.toInt(o.css(e.scrollbarXRail, "marginRight")), e.railYMarginHeight = n.toInt(o.css(e.scrollbarYRail, "marginTop")) + n.toInt(o.css(e.scrollbarYRail, "marginBottom")), o.css(e.scrollbarXRail, "display", "none"), o.css(e.scrollbarYRail, "display", "none"), s(t), a(t, "top", t.scrollTop), a(t, "left", t.scrollLeft), o.css(e.scrollbarXRail, "display", ""), o.css(e.scrollbarYRail, "display", ""))
        }
    }, {
        "../lib/dom": 3,
        "../lib/helper": 6,
        "./instances": 18,
        "./update-geometry": 19,
        "./update-scroll": 20
    }]
}, {}, [1]),
    function(t) {
        "use strict";
        "function" == typeof define && define.amd ? define(["jquery"], t) : "undefined" != typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(function(t) {
        "use strict";
        var e = window.Slick || {};
        e = function() {
            function e(e, n) {
                var o, r = this;
                r.defaults = {
                    accessibility: !0,
                    adaptiveHeight: !1,
                    appendArrows: t(e),
                    appendDots: t(e),
                    arrows: !0,
                    asNavFor: null,
                    prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>',
                    nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next</button>',
                    autoplay: !1,
                    autoplaySpeed: 3e3,
                    centerMode: !1,
                    centerPadding: "50px",
                    cssEase: "ease",
                    customPaging: function(e, i) {
                        return t('<button type="button" data-role="none" role="button" tabindex="0" />').text(i + 1)
                    },
                    dots: !1,
                    dotsClass: "slick-dots",
                    draggable: !0,
                    easing: "linear",
                    edgeFriction: .35,
                    fade: !1,
                    focusOnSelect: !1,
                    infinite: !0,
                    initialSlide: 0,
                    lazyLoad: "ondemand",
                    mobileFirst: !1,
                    pauseOnHover: !0,
                    pauseOnFocus: !0,
                    pauseOnDotsHover: !1,
                    respondTo: "window",
                    responsive: null,
                    rows: 1,
                    rtl: !1,
                    slide: "",
                    slidesPerRow: 1,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    speed: 500,
                    swipe: !0,
                    swipeToSlide: !1,
                    touchMove: !0,
                    touchThreshold: 5,
                    useCSS: !0,
                    useTransform: !0,
                    variableWidth: !1,
                    vertical: !1,
                    verticalSwiping: !1,
                    waitForAnimate: !0,
                    zIndex: 1e3
                }, r.initials = {
                    animating: !1,
                    dragging: !1,
                    autoPlayTimer: null,
                    currentDirection: 0,
                    currentLeft: null,
                    currentSlide: 0,
                    direction: 1,
                    $dots: null,
                    listWidth: null,
                    listHeight: null,
                    loadIndex: 0,
                    $nextArrow: null,
                    $prevArrow: null,
                    slideCount: null,
                    slideWidth: null,
                    $slideTrack: null,
                    $slides: null,
                    sliding: !1,
                    slideOffset: 0,
                    swipeLeft: null,
                    $list: null,
                    touchObject: {},
                    transformsEnabled: !1,
                    unslicked: !1
                }, t.extend(r, r.initials), r.activeBreakpoint = null, r.animType = null, r.animProp = null, r.breakpoints = [], r.breakpointSettings = [], r.cssTransitions = !1, r.focussed = !1, r.interrupted = !1, r.hidden = "hidden", r.paused = !0, r.positionProp = null, r.respondTo = null, r.rowCount = 1, r.shouldClick = !0, r.$slider = t(e), r.$slidesCache = null, r.transformType = null, r.transitionType = null, r.visibilityChange = "visibilitychange", r.windowWidth = 0, r.windowTimer = null, o = t(e).data("slick") || {}, r.options = t.extend({}, r.defaults, n, o), r.currentSlide = r.options.initialSlide, r.originalSettings = r.options, "undefined" != typeof document.mozHidden ? (r.hidden = "mozHidden", r.visibilityChange = "mozvisibilitychange") : "undefined" != typeof document.webkitHidden && (r.hidden = "webkitHidden", r.visibilityChange = "webkitvisibilitychange"), r.autoPlay = t.proxy(r.autoPlay, r), r.autoPlayClear = t.proxy(r.autoPlayClear, r), r.autoPlayIterator = t.proxy(r.autoPlayIterator, r), r.changeSlide = t.proxy(r.changeSlide, r), r.clickHandler = t.proxy(r.clickHandler, r), r.selectHandler = t.proxy(r.selectHandler, r), r.setPosition = t.proxy(r.setPosition, r), r.swipeHandler = t.proxy(r.swipeHandler, r), r.dragHandler = t.proxy(r.dragHandler, r), r.keyHandler = t.proxy(r.keyHandler, r), r.instanceUid = i++, r.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, r.registerBreakpoints(), r.init(!0)
            }
            var i = 0;
            return e
        }(), e.prototype.activateADA = function() {
            var t = this;
            t.$slideTrack.find(".slick-active").attr({
                "aria-hidden": "false"
            }).find("a, input, button, select").attr({
                tabindex: "0"
            })
        }, e.prototype.addSlide = e.prototype.slickAdd = function(e, i, n) {
            var o = this;
            if ("boolean" == typeof i) n = i, i = null;
            else if (i < 0 || i >= o.slideCount) return !1;
            o.unload(), "number" == typeof i ? 0 === i && 0 === o.$slides.length ? t(e).appendTo(o.$slideTrack) : n ? t(e).insertBefore(o.$slides.eq(i)) : t(e).insertAfter(o.$slides.eq(i)) : n === !0 ? t(e).prependTo(o.$slideTrack) : t(e).appendTo(o.$slideTrack), o.$slides = o.$slideTrack.children(this.options.slide), o.$slideTrack.children(this.options.slide).detach(), o.$slideTrack.append(o.$slides), o.$slides.each(function(e, i) {
                t(i).attr("data-slick-index", e)
            }), o.$slidesCache = o.$slides, o.reinit()
        }, e.prototype.animateHeight = function() {
            var t = this;
            if (1 === t.options.slidesToShow && t.options.adaptiveHeight === !0 && t.options.vertical === !1) {
                var e = t.$slides.eq(t.currentSlide).outerHeight(!0);
                t.$list.animate({
                    height: e
                }, t.options.speed)
            }
        }, e.prototype.animateSlide = function(e, i) {
            var n = {},
                o = this;
            o.animateHeight(), o.options.rtl === !0 && o.options.vertical === !1 && (e = -e), o.transformsEnabled === !1 ? o.options.vertical === !1 ? o.$slideTrack.animate({
                left: e
            }, o.options.speed, o.options.easing, i) : o.$slideTrack.animate({
                top: e
            }, o.options.speed, o.options.easing, i) : o.cssTransitions === !1 ? (o.options.rtl === !0 && (o.currentLeft = -o.currentLeft), t({
                animStart: o.currentLeft
            }).animate({
                animStart: e
            }, {
                duration: o.options.speed,
                easing: o.options.easing,
                step: function(t) {
                    t = Math.ceil(t), o.options.vertical === !1 ? (n[o.animType] = "translate(" + t + "px, 0px)", o.$slideTrack.css(n)) : (n[o.animType] = "translate(0px," + t + "px)", o.$slideTrack.css(n))
                },
                complete: function() {
                    i && i.call()
                }
            })) : (o.applyTransition(), e = Math.ceil(e), o.options.vertical === !1 ? n[o.animType] = "translate3d(" + e + "px, 0px, 0px)" : n[o.animType] = "translate3d(0px," + e + "px, 0px)", o.$slideTrack.css(n), i && setTimeout(function() {
                o.disableTransition(), i.call()
            }, o.options.speed))
        }, e.prototype.getNavTarget = function() {
            var e = this,
                i = e.options.asNavFor;
            return i && null !== i && (i = t(i).not(e.$slider)), i
        }, e.prototype.asNavFor = function(e) {
            var i = this,
                n = i.getNavTarget();
            null !== n && "object" == typeof n && n.each(function() {
                var i = t(this).slick("getSlick");
                i.unslicked || i.slideHandler(e, !0)
            })
        }, e.prototype.applyTransition = function(t) {
            var e = this,
                i = {};
            e.options.fade === !1 ? i[e.transitionType] = e.transformType + " " + e.options.speed + "ms " + e.options.cssEase : i[e.transitionType] = "opacity " + e.options.speed + "ms " + e.options.cssEase, e.options.fade === !1 ? e.$slideTrack.css(i) : e.$slides.eq(t).css(i)
        }, e.prototype.autoPlay = function() {
            var t = this;
            t.autoPlayClear(), t.slideCount > t.options.slidesToShow && (t.autoPlayTimer = setInterval(t.autoPlayIterator, t.options.autoplaySpeed))
        }, e.prototype.autoPlayClear = function() {
            var t = this;
            t.autoPlayTimer && clearInterval(t.autoPlayTimer)
        }, e.prototype.autoPlayIterator = function() {
            var t = this,
                e = t.currentSlide + t.options.slidesToScroll;
            t.paused || t.interrupted || t.focussed || (t.options.infinite === !1 && (1 === t.direction && t.currentSlide + 1 === t.slideCount - 1 ? t.direction = 0 : 0 === t.direction && (e = t.currentSlide - t.options.slidesToScroll, t.currentSlide - 1 === 0 && (t.direction = 1))), t.slideHandler(e))
        }, e.prototype.buildArrows = function() {
            var e = this;
            e.options.arrows === !0 && (e.$prevArrow = t(e.options.prevArrow).addClass("slick-arrow"), e.$nextArrow = t(e.options.nextArrow).addClass("slick-arrow"), e.slideCount > e.options.slidesToShow ? (e.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.prependTo(e.options.appendArrows), e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.appendTo(e.options.appendArrows), e.options.infinite !== !0 && e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : e.$prevArrow.add(e.$nextArrow).addClass("slick-hidden").attr({
                "aria-disabled": "true",
                tabindex: "-1"
            }))
        }, e.prototype.buildDots = function() {
            var e, i, n = this;
            if (n.options.dots === !0 && n.slideCount > n.options.slidesToShow) {
                for (n.$slider.addClass("slick-dotted"), i = t("<ul />").addClass(n.options.dotsClass), e = 0; e <= n.getDotCount(); e += 1) i.append(t("<li />").append(n.options.customPaging.call(this, n, e)));
                n.$dots = i.appendTo(n.options.appendDots), n.$dots.find("li").first().addClass("slick-active").attr("aria-hidden", "false")
            }
        }, e.prototype.buildOut = function() {
            var e = this;
            e.$slides = e.$slider.children(e.options.slide + ":not(.slick-cloned)").addClass("slick-slide"), e.slideCount = e.$slides.length, e.$slides.each(function(e, i) {
                t(i).attr("data-slick-index", e).data("originalStyling", t(i).attr("style") || "")
            }), e.$slider.addClass("slick-slider"), e.$slideTrack = 0 === e.slideCount ? t('<div class="slick-track"/>').appendTo(e.$slider) : e.$slides.wrapAll('<div class="slick-track"/>').parent(), e.$list = e.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent(), e.$slideTrack.css("opacity", 0), e.options.centerMode !== !0 && e.options.swipeToSlide !== !0 || (e.options.slidesToScroll = 1), t("img[data-lazy]", e.$slider).not("[src]").addClass("slick-loading"), e.setupInfinite(), e.buildArrows(), e.buildDots(), e.updateDots(), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), e.options.draggable === !0 && e.$list.addClass("draggable")
        }, e.prototype.buildRows = function() {
            var t, e, i, n, o, r, s, a = this;
            if (n = document.createDocumentFragment(), r = a.$slider.children(), a.options.rows >= 1) {
                for (s = a.options.slidesPerRow * a.options.rows, o = Math.ceil(r.length / s), t = 0; t < o; t++) {
                    var l = document.createElement("div");
                    for (e = 0; e < a.options.rows; e++) {
                        var c = document.createElement("div");
                        for (i = 0; i < a.options.slidesPerRow; i++) {
                            var u = t * s + (e * a.options.slidesPerRow + i);
                            r.get(u) && c.appendChild(r.get(u))
                        }
                        l.appendChild(c)
                    }
                    n.appendChild(l)
                }
                a.$slider.empty().append(n), a.$slider.children().children().children().css({
                    width: 100 / a.options.slidesPerRow + "%",
                    display: "inline-block"
                })
            }
        }, e.prototype.checkResponsive = function(e, i) {
            var n, o, r, s = this,
                a = !1,
                l = s.$slider.width(),
                c = window.innerWidth || t(window).width();
            if ("window" === s.respondTo ? r = c : "slider" === s.respondTo ? r = l : "min" === s.respondTo && (r = Math.min(c, l)), s.options.responsive && s.options.responsive.length && null !== s.options.responsive) {
                o = null;
                for (n in s.breakpoints) s.breakpoints.hasOwnProperty(n) && (s.originalSettings.mobileFirst === !1 ? r < s.breakpoints[n] && (o = s.breakpoints[n]) : r > s.breakpoints[n] && (o = s.breakpoints[n]));
                null !== o ? null !== s.activeBreakpoint ? (o !== s.activeBreakpoint || i) && (s.activeBreakpoint = o, "unslick" === s.breakpointSettings[o] ? s.unslick(o) : (s.options = t.extend({}, s.originalSettings, s.breakpointSettings[o]), e === !0 && (s.currentSlide = s.options.initialSlide), s.refresh(e)), a = o) : (s.activeBreakpoint = o, "unslick" === s.breakpointSettings[o] ? s.unslick(o) : (s.options = t.extend({}, s.originalSettings, s.breakpointSettings[o]), e === !0 && (s.currentSlide = s.options.initialSlide), s.refresh(e)), a = o) : null !== s.activeBreakpoint && (s.activeBreakpoint = null, s.options = s.originalSettings, e === !0 && (s.currentSlide = s.options.initialSlide), s.refresh(e), a = o), e || a === !1 || s.$slider.trigger("breakpoint", [s, a])
            }
        }, e.prototype.changeSlide = function(e, i) {
            var n, o, r, s = this,
                a = t(e.currentTarget);
            switch (a.is("a") && e.preventDefault(), a.is("li") || (a = a.closest("li")), r = s.slideCount % s.options.slidesToScroll !== 0, n = r ? 0 : (s.slideCount - s.currentSlide) % s.options.slidesToScroll, e.data.message) {
                case "previous":
                    o = 0 === n ? s.options.slidesToScroll : s.options.slidesToShow - n, s.slideCount > s.options.slidesToShow && s.slideHandler(s.currentSlide - o, !1, i);
                    break;
                case "next":
                    o = 0 === n ? s.options.slidesToScroll : n, s.slideCount > s.options.slidesToShow && s.slideHandler(s.currentSlide + o, !1, i);
                    break;
                case "index":
                    var l = 0 === e.data.index ? 0 : e.data.index || a.index() * s.options.slidesToScroll;
                    s.slideHandler(s.checkNavigable(l), !1, i), a.children().trigger("focus");
                    break;
                default:
                    return
            }
        }, e.prototype.checkNavigable = function(t) {
            var e, i, n = this;
            if (e = n.getNavigableIndexes(), i = 0, t > e[e.length - 1]) t = e[e.length - 1];
            else
                for (var o in e) {
                    if (t < e[o]) {
                        t = i;
                        break
                    }
                    i = e[o]
                }
            return t
        }, e.prototype.cleanUpEvents = function() {
            var e = this;
            e.options.dots && null !== e.$dots && t("li", e.$dots).off("click.slick", e.changeSlide).off("mouseenter.slick", t.proxy(e.interrupt, e, !0)).off("mouseleave.slick", t.proxy(e.interrupt, e, !1)), e.$slider.off("focus.slick blur.slick"), e.options.arrows === !0 && e.slideCount > e.options.slidesToShow && (e.$prevArrow && e.$prevArrow.off("click.slick", e.changeSlide), e.$nextArrow && e.$nextArrow.off("click.slick", e.changeSlide)), e.$list.off("touchstart.slick mousedown.slick", e.swipeHandler), e.$list.off("touchmove.slick mousemove.slick", e.swipeHandler), e.$list.off("touchend.slick mouseup.slick", e.swipeHandler), e.$list.off("touchcancel.slick mouseleave.slick", e.swipeHandler), e.$list.off("click.slick", e.clickHandler), t(document).off(e.visibilityChange, e.visibility), e.cleanUpSlideEvents(), e.options.accessibility === !0 && e.$list.off("keydown.slick", e.keyHandler), e.options.focusOnSelect === !0 && t(e.$slideTrack).children().off("click.slick", e.selectHandler), t(window).off("orientationchange.slick.slick-" + e.instanceUid, e.orientationChange), t(window).off("resize.slick.slick-" + e.instanceUid, e.resize), t("[draggable!=true]", e.$slideTrack).off("dragstart", e.preventDefault), t(window).off("load.slick.slick-" + e.instanceUid, e.setPosition), t(document).off("ready.slick.slick-" + e.instanceUid, e.setPosition)
        }, e.prototype.cleanUpSlideEvents = function() {
            var e = this;
            e.$list.off("mouseenter.slick", t.proxy(e.interrupt, e, !0)), e.$list.off("mouseleave.slick", t.proxy(e.interrupt, e, !1))
        }, e.prototype.cleanUpRows = function() {
            var t, e = this;
            e.options.rows >= 1 && (t = e.$slides.children().children(), t.removeAttr("style"), e.$slider.empty().append(t))
        }, e.prototype.clickHandler = function(t) {
            var e = this;
            e.shouldClick === !1 && (t.stopImmediatePropagation(), t.stopPropagation(), t.preventDefault())
        }, e.prototype.destroy = function(e) {
            var i = this;
            i.autoPlayClear(), i.touchObject = {}, i.cleanUpEvents(), t(".slick-cloned", i.$slider).detach(), i.$dots && i.$dots.remove(), i.$prevArrow && i.$prevArrow.length && (i.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), i.htmlExpr.test(i.options.prevArrow) && i.$prevArrow.remove()), i.$nextArrow && i.$nextArrow.length && (i.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), i.htmlExpr.test(i.options.nextArrow) && i.$nextArrow.remove()), i.$slides && (i.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function() {
                t(this).attr("style", t(this).data("originalStyling"))
            }), i.$slideTrack.children(this.options.slide).detach(), i.$slideTrack.detach(), i.$list.detach(), i.$slider.append(i.$slides)), i.cleanUpRows(), i.$slider.removeClass("slick-slider"), i.$slider.removeClass("slick-initialized"), i.$slider.removeClass("slick-dotted"), i.unslicked = !0, e || i.$slider.trigger("destroy", [i])
        }, e.prototype.disableTransition = function(t) {
            var e = this,
                i = {};
            i[e.transitionType] = "", e.options.fade === !1 ? e.$slideTrack.css(i) : e.$slides.eq(t).css(i)
        }, e.prototype.fadeSlide = function(t, e) {
            var i = this;
            i.cssTransitions === !1 ? (i.$slides.eq(t).css({
                zIndex: i.options.zIndex
            }), i.$slides.eq(t).animate({
                opacity: 1
            }, i.options.speed, i.options.easing, e)) : (i.applyTransition(t), i.$slides.eq(t).css({
                opacity: 1,
                zIndex: i.options.zIndex
            }), e && setTimeout(function() {
                i.disableTransition(t), e.call()
            }, i.options.speed))
        }, e.prototype.fadeSlideOut = function(t) {
            var e = this;
            e.cssTransitions === !1 ? e.$slides.eq(t).animate({
                opacity: 0,
                zIndex: e.options.zIndex - 2
            }, e.options.speed, e.options.easing) : (e.applyTransition(t), e.$slides.eq(t).css({
                opacity: 0,
                zIndex: e.options.zIndex - 2
            }))
        }, e.prototype.filterSlides = e.prototype.slickFilter = function(t) {
            var e = this;
            null !== t && (e.$slidesCache = e.$slides, e.unload(), e.$slideTrack.children(this.options.slide).detach(), e.$slidesCache.filter(t).appendTo(e.$slideTrack), e.reinit())
        }, e.prototype.focusHandler = function() {
            var e = this;
            e.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick", "*:not(.slick-arrow)", function(i) {
                i.stopImmediatePropagation();
                var n = t(this);
                setTimeout(function() {
                    e.options.pauseOnFocus && (e.focussed = n.is(":focus"), e.autoPlay())
                }, 0)
            })
        }, e.prototype.getCurrent = e.prototype.slickCurrentSlide = function() {
            var t = this;
            return t.currentSlide
        }, e.prototype.getDotCount = function() {
            var t = this,
                e = 0,
                i = 0,
                n = 0;
            if (t.options.infinite === !0)
                for (; e < t.slideCount;) ++n, e = i + t.options.slidesToScroll, i += t.options.slidesToScroll <= t.options.slidesToShow ? t.options.slidesToScroll : t.options.slidesToShow;
            else if (t.options.centerMode === !0) n = t.slideCount;
            else if (t.options.asNavFor)
                for (; e < t.slideCount;) ++n, e = i + t.options.slidesToScroll, i += t.options.slidesToScroll <= t.options.slidesToShow ? t.options.slidesToScroll : t.options.slidesToShow;
            else n = 1 + Math.ceil((t.slideCount - t.options.slidesToShow) / t.options.slidesToScroll);
            return n - 1
        }, e.prototype.getLeft = function(t) {
            var e, i, n, o = this,
                r = 0;
            return o.slideOffset = 0, i = o.$slides.first().outerHeight(!0), o.options.infinite === !0 ? (o.slideCount > o.options.slidesToShow && (o.slideOffset = o.slideWidth * o.options.slidesToShow * -1, r = i * o.options.slidesToShow * -1), o.slideCount % o.options.slidesToScroll !== 0 && t + o.options.slidesToScroll > o.slideCount && o.slideCount > o.options.slidesToShow && (t > o.slideCount ? (o.slideOffset = (o.options.slidesToShow - (t - o.slideCount)) * o.slideWidth * -1, r = (o.options.slidesToShow - (t - o.slideCount)) * i * -1) : (o.slideOffset = o.slideCount % o.options.slidesToScroll * o.slideWidth * -1, r = o.slideCount % o.options.slidesToScroll * i * -1))) : t + o.options.slidesToShow > o.slideCount && (o.slideOffset = (t + o.options.slidesToShow - o.slideCount) * o.slideWidth, r = (t + o.options.slidesToShow - o.slideCount) * i), o.slideCount <= o.options.slidesToShow && (o.slideOffset = 0, r = 0), o.options.centerMode === !0 && o.slideCount <= o.options.slidesToShow ? o.slideOffset = o.slideWidth * Math.floor(o.options.slidesToShow) / 2 - o.slideWidth * o.slideCount / 2 : o.options.centerMode === !0 && o.options.infinite === !0 ? o.slideOffset += o.slideWidth * Math.floor(o.options.slidesToShow / 2) - o.slideWidth : o.options.centerMode === !0 && (o.slideOffset = 0, o.slideOffset += o.slideWidth * Math.floor(o.options.slidesToShow / 2)), e = o.options.vertical === !1 ? t * o.slideWidth * -1 + o.slideOffset : t * i * -1 + r, o.options.variableWidth === !0 && (n = o.slideCount <= o.options.slidesToShow || o.options.infinite === !1 ? o.$slideTrack.children(".slick-slide").eq(t) : o.$slideTrack.children(".slick-slide").eq(t + o.options.slidesToShow), e = o.options.rtl === !0 ? n[0] ? (o.$slideTrack.width() - n[0].offsetLeft - n.width()) * -1 : 0 : n[0] ? n[0].offsetLeft * -1 : 0, o.options.centerMode === !0 && (n = o.slideCount <= o.options.slidesToShow || o.options.infinite === !1 ? o.$slideTrack.children(".slick-slide").eq(t) : o.$slideTrack.children(".slick-slide").eq(t + o.options.slidesToShow + 1), e = o.options.rtl === !0 ? n[0] ? (o.$slideTrack.width() - n[0].offsetLeft - n.width()) * -1 : 0 : n[0] ? n[0].offsetLeft * -1 : 0, e += (o.$list.width() - n.outerWidth()) / 2)), e
        }, e.prototype.getOption = e.prototype.slickGetOption = function(t) {
            var e = this;
            return e.options[t]
        }, e.prototype.getNavigableIndexes = function() {
            var t, e = this,
                i = 0,
                n = 0,
                o = [];
            for (e.options.infinite === !1 ? t = e.slideCount : (i = e.options.slidesToScroll * -1, n = e.options.slidesToScroll * -1, t = 2 * e.slideCount); i < t;) o.push(i), i = n + e.options.slidesToScroll, n += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;
            return o
        }, e.prototype.getSlick = function() {
            return this
        }, e.prototype.getSlideCount = function() {
            var e, i, n, o = this;
            return n = o.options.centerMode === !0 ? o.slideWidth * Math.floor(o.options.slidesToShow / 2) : 0, o.options.swipeToSlide === !0 ? (o.$slideTrack.find(".slick-slide").each(function(e, r) {
                if (r.offsetLeft - n + t(r).outerWidth() / 2 > o.swipeLeft * -1) return i = r, !1
            }), e = Math.abs(t(i).attr("data-slick-index") - o.currentSlide) || 1) : o.options.slidesToScroll
        }, e.prototype.goTo = e.prototype.slickGoTo = function(t, e) {
            var i = this;
            i.changeSlide({
                data: {
                    message: "index",
                    index: parseInt(t)
                }
            }, e)
        }, e.prototype.init = function(e) {
            var i = this;
            t(i.$slider).hasClass("slick-initialized") || (t(i.$slider).addClass("slick-initialized"), i.buildRows(), i.buildOut(), i.setProps(), i.startLoad(), i.loadSlider(), i.initializeEvents(), i.updateArrows(), i.updateDots(), i.checkResponsive(!0), i.focusHandler()), e && i.$slider.trigger("init", [i]), i.options.accessibility === !0 && i.initADA(), i.options.autoplay && (i.paused = !1, i.autoPlay())
        }, e.prototype.initADA = function() {
            var e = this;
            e.$slides.add(e.$slideTrack.find(".slick-cloned")).attr({
                "aria-hidden": "true",
                tabindex: "-1"
            }).find("a, input, button, select").attr({
                tabindex: "-1"
            }), e.$slideTrack.attr("role", "listbox"), e.$slides.not(e.$slideTrack.find(".slick-cloned")).each(function(i) {
                t(this).attr("role", "option");
                var n = e.options.centerMode ? i : Math.floor(i / e.options.slidesToShow);
                e.options.dots === !0 && t(this).attr("aria-describedby", "slick-slide" + e.instanceUid + n)
            }), null !== e.$dots && e.$dots.attr("role", "tablist").find("li").each(function(i) {
                t(this).attr({
                    role: "presentation",
                    "aria-selected": "false",
                    "aria-controls": "navigation" + e.instanceUid + i,
                    id: "slick-slide" + e.instanceUid + i
                })
            }).first().attr("aria-selected", "true").end().find("button").attr("role", "button").end().closest("div").attr("role", "toolbar"), e.activateADA()
        }, e.prototype.initArrowEvents = function() {
            var t = this;
            t.options.arrows === !0 && t.slideCount > t.options.slidesToShow && (t.$prevArrow.off("click.slick").on("click.slick", {
                message: "previous"
            }, t.changeSlide), t.$nextArrow.off("click.slick").on("click.slick", {
                message: "next"
            }, t.changeSlide))
        }, e.prototype.initDotEvents = function() {
            var e = this;
            e.options.dots === !0 && e.slideCount > e.options.slidesToShow && t("li", e.$dots).on("click.slick", {
                message: "index"
            }, e.changeSlide), e.options.dots === !0 && e.options.pauseOnDotsHover === !0 && t("li", e.$dots).on("mouseenter.slick", t.proxy(e.interrupt, e, !0)).on("mouseleave.slick", t.proxy(e.interrupt, e, !1))
        }, e.prototype.initSlideEvents = function() {
            var e = this;
            e.options.pauseOnHover && (e.$list.on("mouseenter.slick", t.proxy(e.interrupt, e, !0)), e.$list.on("mouseleave.slick", t.proxy(e.interrupt, e, !1)))
        }, e.prototype.initializeEvents = function() {
            var e = this;
            e.initArrowEvents(), e.initDotEvents(), e.initSlideEvents(), e.$list.on("touchstart.slick mousedown.slick", {
                action: "start"
            }, e.swipeHandler), e.$list.on("touchmove.slick mousemove.slick", {
                action: "move"
            }, e.swipeHandler), e.$list.on("touchend.slick mouseup.slick", {
                action: "end"
            }, e.swipeHandler), e.$list.on("touchcancel.slick mouseleave.slick", {
                action: "end"
            }, e.swipeHandler), e.$list.on("click.slick", e.clickHandler), t(document).on(e.visibilityChange, t.proxy(e.visibility, e)), e.options.accessibility === !0 && e.$list.on("keydown.slick", e.keyHandler), e.options.focusOnSelect === !0 && t(e.$slideTrack).children().on("click.slick", e.selectHandler), t(window).on("orientationchange.slick.slick-" + e.instanceUid, t.proxy(e.orientationChange, e)), t(window).on("resize.slick.slick-" + e.instanceUid, t.proxy(e.resize, e)), t("[draggable!=true]", e.$slideTrack).on("dragstart", e.preventDefault), t(window).on("load.slick.slick-" + e.instanceUid, e.setPosition), t(document).on("ready.slick.slick-" + e.instanceUid, e.setPosition)
        }, e.prototype.initUI = function() {
            var t = this;
            t.options.arrows === !0 && t.slideCount > t.options.slidesToShow && (t.$prevArrow.show(), t.$nextArrow.show()), t.options.dots === !0 && t.slideCount > t.options.slidesToShow && t.$dots.show()
        }, e.prototype.keyHandler = function(t) {
            var e = this;
            t.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === t.keyCode && e.options.accessibility === !0 ? e.changeSlide({
                data: {
                    message: e.options.rtl === !0 ? "next" : "previous"
                }
            }) : 39 === t.keyCode && e.options.accessibility === !0 && e.changeSlide({
                data: {
                    message: e.options.rtl === !0 ? "previous" : "next"
                }
            }))
        }, e.prototype.lazyLoad = function() {
            function e(e) {
                t("img[data-lazy]", e).each(function() {
                    var e = t(this),
                        i = t(this).attr("data-lazy"),
                        n = document.createElement("img");
                    n.onload = function() {
                        e.animate({
                            opacity: 0
                        }, 100, function() {
                            e.attr("src", i).animate({
                                opacity: 1
                            }, 200, function() {
                                e.removeAttr("data-lazy").removeClass("slick-loading")
                            }), s.$slider.trigger("lazyLoaded", [s, e, i])
                        })
                    }, n.onerror = function() {
                        e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), s.$slider.trigger("lazyLoadError", [s, e, i])
                    }, n.src = i
                })
            }
            var i, n, o, r, s = this;
            if (s.options.centerMode === !0 ? s.options.infinite === !0 ? (o = s.currentSlide + (s.options.slidesToShow / 2 + 1), r = o + s.options.slidesToShow + 2) : (o = Math.max(0, s.currentSlide - (s.options.slidesToShow / 2 + 1)), r = 2 + (s.options.slidesToShow / 2 + 1) + s.currentSlide) : (o = s.options.infinite ? s.options.slidesToShow + s.currentSlide : s.currentSlide, r = Math.ceil(o + s.options.slidesToShow), s.options.fade === !0 && (o > 0 && o--, r <= s.slideCount && r++)), i = s.$slider.find(".slick-slide").slice(o, r), "anticipated" === s.options.lazyLoad)
                for (var a = o - 1, l = r, c = s.$slider.find(".slick-slide"), u = 0; u < s.options.slidesToScroll; u++) a < 0 && (a = s.slideCount - 1), i = i.add(c.eq(a)), i = i.add(c.eq(l)), a--, l++;
            e(i), s.slideCount <= s.options.slidesToShow ? (n = s.$slider.find(".slick-slide"), e(n)) : s.currentSlide >= s.slideCount - s.options.slidesToShow ? (n = s.$slider.find(".slick-cloned").slice(0, s.options.slidesToShow), e(n)) : 0 === s.currentSlide && (n = s.$slider.find(".slick-cloned").slice(s.options.slidesToShow * -1), e(n))
        }, e.prototype.loadSlider = function() {
            var t = this;
            t.setPosition(), t.$slideTrack.css({
                opacity: 1
            }), t.$slider.removeClass("slick-loading"), t.initUI(), "progressive" === t.options.lazyLoad && t.progressiveLazyLoad()
        }, e.prototype.next = e.prototype.slickNext = function() {
            var t = this;
            t.changeSlide({
                data: {
                    message: "next"
                }
            })
        }, e.prototype.orientationChange = function() {
            var t = this;
            t.checkResponsive(), t.setPosition()
        }, e.prototype.pause = e.prototype.slickPause = function() {
            var t = this;
            t.autoPlayClear(), t.paused = !0
        }, e.prototype.play = e.prototype.slickPlay = function() {
            var t = this;
            t.autoPlay(), t.options.autoplay = !0, t.paused = !1, t.focussed = !1, t.interrupted = !1
        }, e.prototype.postSlide = function(t) {
            var e = this;
            e.unslicked || (e.$slider.trigger("afterChange", [e, t]), e.animating = !1, e.setPosition(), e.swipeLeft = null, e.options.autoplay && e.autoPlay(), e.options.accessibility === !0 && e.initADA())
        }, e.prototype.prev = e.prototype.slickPrev = function() {
            var t = this;
            t.changeSlide({
                data: {
                    message: "previous"
                }
            })
        }, e.prototype.preventDefault = function(t) {
            t.preventDefault()
        }, e.prototype.progressiveLazyLoad = function(e) {
            e = e || 1;
            var i, n, o, r = this,
                s = t("img[data-lazy]", r.$slider);
            s.length ? (i = s.first(), n = i.attr("data-lazy"), o = document.createElement("img"), o.onload = function() {
                i.attr("src", n).removeAttr("data-lazy").removeClass("slick-loading"), r.options.adaptiveHeight === !0 && r.setPosition(), r.$slider.trigger("lazyLoaded", [r, i, n]),
                    r.progressiveLazyLoad()
            }, o.onerror = function() {
                e < 3 ? setTimeout(function() {
                    r.progressiveLazyLoad(e + 1)
                }, 500) : (i.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), r.$slider.trigger("lazyLoadError", [r, i, n]), r.progressiveLazyLoad())
            }, o.src = n) : r.$slider.trigger("allImagesLoaded", [r])
        }, e.prototype.refresh = function(e) {
            var i, n, o = this;
            n = o.slideCount - o.options.slidesToShow, !o.options.infinite && o.currentSlide > n && (o.currentSlide = n), o.slideCount <= o.options.slidesToShow && (o.currentSlide = 0), i = o.currentSlide, o.destroy(!0), t.extend(o, o.initials, {
                currentSlide: i
            }), o.init(), e || o.changeSlide({
                data: {
                    message: "index",
                    index: i
                }
            }, !1)
        }, e.prototype.registerBreakpoints = function() {
            var e, i, n, o = this,
                r = o.options.responsive || null;
            if ("array" === t.type(r) && r.length) {
                o.respondTo = o.options.respondTo || "window";
                for (e in r)
                    if (n = o.breakpoints.length - 1, i = r[e].breakpoint, r.hasOwnProperty(e)) {
                        for (; n >= 0;) o.breakpoints[n] && o.breakpoints[n] === i && o.breakpoints.splice(n, 1), n--;
                        o.breakpoints.push(i), o.breakpointSettings[i] = r[e].settings
                    }
                o.breakpoints.sort(function(t, e) {
                    return o.options.mobileFirst ? t - e : e - t
                })
            }
        }, e.prototype.reinit = function() {
            var e = this;
            e.$slides = e.$slideTrack.children(e.options.slide).addClass("slick-slide"), e.slideCount = e.$slides.length, e.currentSlide >= e.slideCount && 0 !== e.currentSlide && (e.currentSlide = e.currentSlide - e.options.slidesToScroll), e.slideCount <= e.options.slidesToShow && (e.currentSlide = 0), e.registerBreakpoints(), e.setProps(), e.setupInfinite(), e.buildArrows(), e.updateArrows(), e.initArrowEvents(), e.buildDots(), e.updateDots(), e.initDotEvents(), e.cleanUpSlideEvents(), e.initSlideEvents(), e.checkResponsive(!1, !0), e.options.focusOnSelect === !0 && t(e.$slideTrack).children().on("click.slick", e.selectHandler), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), e.setPosition(), e.focusHandler(), e.paused = !e.options.autoplay, e.autoPlay(), e.$slider.trigger("reInit", [e])
        }, e.prototype.resize = function() {
            var e = this;
            t(window).width() !== e.windowWidth && (clearTimeout(e.windowDelay), e.windowDelay = window.setTimeout(function() {
                e.windowWidth = t(window).width(), e.checkResponsive(), e.unslicked || e.setPosition()
            }, 50))
        }, e.prototype.removeSlide = e.prototype.slickRemove = function(t, e, i) {
            var n = this;
            return "boolean" == typeof t ? (e = t, t = e === !0 ? 0 : n.slideCount - 1) : t = e === !0 ? --t : t, !(n.slideCount < 1 || t < 0 || t > n.slideCount - 1) && (n.unload(), i === !0 ? n.$slideTrack.children().remove() : n.$slideTrack.children(this.options.slide).eq(t).remove(), n.$slides = n.$slideTrack.children(this.options.slide), n.$slideTrack.children(this.options.slide).detach(), n.$slideTrack.append(n.$slides), n.$slidesCache = n.$slides, void n.reinit())
        }, e.prototype.setCSS = function(t) {
            var e, i, n = this,
                o = {};
            n.options.rtl === !0 && (t = -t), e = "left" == n.positionProp ? Math.ceil(t) + "px" : "0px", i = "top" == n.positionProp ? Math.ceil(t) + "px" : "0px", o[n.positionProp] = t, n.transformsEnabled === !1 ? n.$slideTrack.css(o) : (o = {}, n.cssTransitions === !1 ? (o[n.animType] = "translate(" + e + ", " + i + ")", n.$slideTrack.css(o)) : (o[n.animType] = "translate3d(" + e + ", " + i + ", 0px)", n.$slideTrack.css(o)))
        }, e.prototype.setDimensions = function() {
            var t = this;
            t.options.vertical === !1 ? t.options.centerMode === !0 && t.$list.css({
                padding: "0px " + t.options.centerPadding
            }) : (t.$list.height(t.$slides.first().outerHeight(!0) * t.options.slidesToShow), t.options.centerMode === !0 && t.$list.css({
                padding: t.options.centerPadding + " 0px"
            })), t.listWidth = t.$list.width(), t.listHeight = t.$list.height(), t.options.vertical === !1 && t.options.variableWidth === !1 ? (t.slideWidth = Math.ceil(t.listWidth / t.options.slidesToShow), t.$slideTrack.width(Math.ceil(t.slideWidth * t.$slideTrack.children(".slick-slide").length))) : t.options.variableWidth === !0 ? t.$slideTrack.width(5e3 * t.slideCount) : (t.slideWidth = Math.ceil(t.listWidth), t.$slideTrack.height(Math.ceil(t.$slides.first().outerHeight(!0) * t.$slideTrack.children(".slick-slide").length)));
            var e = t.$slides.first().outerWidth(!0) - t.$slides.first().width();
            t.options.variableWidth === !1 && t.$slideTrack.children(".slick-slide").width(t.slideWidth - e)
        }, e.prototype.setFade = function() {
            var e, i = this;
            i.$slides.each(function(n, o) {
                e = i.slideWidth * n * -1, i.options.rtl === !0 ? t(o).css({
                    position: "relative",
                    right: e,
                    top: 0,
                    zIndex: i.options.zIndex - 2,
                    opacity: 0
                }) : t(o).css({
                    position: "relative",
                    left: e,
                    top: 0,
                    zIndex: i.options.zIndex - 2,
                    opacity: 0
                })
            }), i.$slides.eq(i.currentSlide).css({
                zIndex: i.options.zIndex - 1,
                opacity: 1
            })
        }, e.prototype.setHeight = function() {
            var t = this;
            if (1 === t.options.slidesToShow && t.options.adaptiveHeight === !0 && t.options.vertical === !1) {
                var e = t.$slides.eq(t.currentSlide).outerHeight(!0);
                t.$list.css("height", e)
            }
        }, e.prototype.setOption = e.prototype.slickSetOption = function() {
            var e, i, n, o, r, s = this,
                a = !1;
            if ("object" === t.type(arguments[0]) ? (n = arguments[0], a = arguments[1], r = "multiple") : "string" === t.type(arguments[0]) && (n = arguments[0], o = arguments[1], a = arguments[2], "responsive" === arguments[0] && "array" === t.type(arguments[1]) ? r = "responsive" : "undefined" != typeof arguments[1] && (r = "single")), "single" === r) s.options[n] = o;
            else if ("multiple" === r) t.each(n, function(t, e) {
                s.options[t] = e
            });
            else if ("responsive" === r)
                for (i in o)
                    if ("array" !== t.type(s.options.responsive)) s.options.responsive = [o[i]];
                    else {
                        for (e = s.options.responsive.length - 1; e >= 0;) s.options.responsive[e].breakpoint === o[i].breakpoint && s.options.responsive.splice(e, 1), e--;
                        s.options.responsive.push(o[i])
                    }
            a && (s.unload(), s.reinit())
        }, e.prototype.setPosition = function() {
            var t = this;
            t.setDimensions(), t.setHeight(), t.options.fade === !1 ? t.setCSS(t.getLeft(t.currentSlide)) : t.setFade(), t.$slider.trigger("setPosition", [t])
        }, e.prototype.setProps = function() {
            var t = this,
                e = document.body.style;
            t.positionProp = t.options.vertical === !0 ? "top" : "left", "top" === t.positionProp ? t.$slider.addClass("slick-vertical") : t.$slider.removeClass("slick-vertical"), void 0 === e.WebkitTransition && void 0 === e.MozTransition && void 0 === e.msTransition || t.options.useCSS === !0 && (t.cssTransitions = !0), t.options.fade && ("number" == typeof t.options.zIndex ? t.options.zIndex < 3 && (t.options.zIndex = 3) : t.options.zIndex = t.defaults.zIndex), void 0 !== e.OTransform && (t.animType = "OTransform", t.transformType = "-o-transform", t.transitionType = "OTransition", void 0 === e.perspectiveProperty && void 0 === e.webkitPerspective && (t.animType = !1)), void 0 !== e.MozTransform && (t.animType = "MozTransform", t.transformType = "-moz-transform", t.transitionType = "MozTransition", void 0 === e.perspectiveProperty && void 0 === e.MozPerspective && (t.animType = !1)), void 0 !== e.webkitTransform && (t.animType = "webkitTransform", t.transformType = "-webkit-transform", t.transitionType = "webkitTransition", void 0 === e.perspectiveProperty && void 0 === e.webkitPerspective && (t.animType = !1)), void 0 !== e.msTransform && (t.animType = "msTransform", t.transformType = "-ms-transform", t.transitionType = "msTransition", void 0 === e.msTransform && (t.animType = !1)), void 0 !== e.transform && t.animType !== !1 && (t.animType = "transform", t.transformType = "transform", t.transitionType = "transition"), t.transformsEnabled = t.options.useTransform && null !== t.animType && t.animType !== !1
        }, e.prototype.setSlideClasses = function(t) {
            var e, i, n, o, r = this;
            i = r.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"), r.$slides.eq(t).addClass("slick-current"), r.options.centerMode === !0 ? (e = Math.floor(r.options.slidesToShow / 2), r.options.infinite === !0 && (t >= e && t <= r.slideCount - 1 - e ? r.$slides.slice(t - e, t + e + 1).addClass("slick-active").attr("aria-hidden", "false") : (n = r.options.slidesToShow + t, i.slice(n - e + 1, n + e + 2).addClass("slick-active").attr("aria-hidden", "false")), 0 === t ? i.eq(i.length - 1 - r.options.slidesToShow).addClass("slick-center") : t === r.slideCount - 1 && i.eq(r.options.slidesToShow).addClass("slick-center")), r.$slides.eq(t).addClass("slick-center")) : t >= 0 && t <= r.slideCount - r.options.slidesToShow ? r.$slides.slice(t, t + r.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : i.length <= r.options.slidesToShow ? i.addClass("slick-active").attr("aria-hidden", "false") : (o = r.slideCount % r.options.slidesToShow, n = r.options.infinite === !0 ? r.options.slidesToShow + t : t, r.options.slidesToShow == r.options.slidesToScroll && r.slideCount - t < r.options.slidesToShow ? i.slice(n - (r.options.slidesToShow - o), n + o).addClass("slick-active").attr("aria-hidden", "false") : i.slice(n, n + r.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false")), "ondemand" !== r.options.lazyLoad && "anticipated" !== r.options.lazyLoad || r.lazyLoad()
        }, e.prototype.setupInfinite = function() {
            var e, i, n, o = this;
            if (o.options.fade === !0 && (o.options.centerMode = !1), o.options.infinite === !0 && o.options.fade === !1 && (i = null, o.slideCount > o.options.slidesToShow)) {
                for (n = o.options.centerMode === !0 ? o.options.slidesToShow + 1 : o.options.slidesToShow, e = o.slideCount; e > o.slideCount - n; e -= 1) i = e - 1, t(o.$slides[i]).clone(!0).attr("id", "").attr("data-slick-index", i - o.slideCount).prependTo(o.$slideTrack).addClass("slick-cloned");
                for (e = 0; e < n; e += 1) i = e, t(o.$slides[i]).clone(!0).attr("id", "").attr("data-slick-index", i + o.slideCount).appendTo(o.$slideTrack).addClass("slick-cloned");
                o.$slideTrack.find(".slick-cloned").find("[id]").each(function() {
                    t(this).attr("id", "")
                })
            }
        }, e.prototype.interrupt = function(t) {
            var e = this;
            t || e.autoPlay(), e.interrupted = t
        }, e.prototype.selectHandler = function(e) {
            var i = this,
                n = t(e.target).is(".slick-slide") ? t(e.target) : t(e.target).parents(".slick-slide"),
                o = parseInt(n.attr("data-slick-index"));
            return o || (o = 0), i.slideCount <= i.options.slidesToShow ? (i.setSlideClasses(o), void i.asNavFor(o)) : void i.slideHandler(o)
        }, e.prototype.slideHandler = function(t, e, i) {
            var n, o, r, s, a, l = null,
                c = this;
            if (e = e || !1, (c.animating !== !0 || c.options.waitForAnimate !== !0) && !(c.options.fade === !0 && c.currentSlide === t || c.slideCount <= c.options.slidesToShow)) return e === !1 && c.asNavFor(t), n = t, l = c.getLeft(n), s = c.getLeft(c.currentSlide), c.currentLeft = null === c.swipeLeft ? s : c.swipeLeft, c.options.infinite === !1 && c.options.centerMode === !1 && (t < 0 || t > c.getDotCount() * c.options.slidesToScroll) ? void(c.options.fade === !1 && (n = c.currentSlide, i !== !0 ? c.animateSlide(s, function() {
                c.postSlide(n)
            }) : c.postSlide(n))) : c.options.infinite === !1 && c.options.centerMode === !0 && (t < 0 || t > c.slideCount - c.options.slidesToScroll) ? void(c.options.fade === !1 && (n = c.currentSlide, i !== !0 ? c.animateSlide(s, function() {
                c.postSlide(n)
            }) : c.postSlide(n))) : (c.options.autoplay && clearInterval(c.autoPlayTimer), o = n < 0 ? c.slideCount % c.options.slidesToScroll !== 0 ? c.slideCount - c.slideCount % c.options.slidesToScroll : c.slideCount + n : n >= c.slideCount ? c.slideCount % c.options.slidesToScroll !== 0 ? 0 : n - c.slideCount : n, c.animating = !0, c.$slider.trigger("beforeChange", [c, c.currentSlide, o]), r = c.currentSlide, c.currentSlide = o, c.setSlideClasses(c.currentSlide), c.options.asNavFor && (a = c.getNavTarget(), a = a.slick("getSlick"), a.slideCount <= a.options.slidesToShow && a.setSlideClasses(c.currentSlide)), c.updateDots(), c.updateArrows(), c.options.fade === !0 ? (i !== !0 ? (c.fadeSlideOut(r), c.fadeSlide(o, function() {
                c.postSlide(o)
            })) : c.postSlide(o), void c.animateHeight()) : void(i !== !0 ? c.animateSlide(l, function() {
                c.postSlide(o)
            }) : c.postSlide(o)))
        }, e.prototype.startLoad = function() {
            var t = this;
            t.options.arrows === !0 && t.slideCount > t.options.slidesToShow && (t.$prevArrow.hide(), t.$nextArrow.hide()), t.options.dots === !0 && t.slideCount > t.options.slidesToShow && t.$dots.hide(), t.$slider.addClass("slick-loading")
        }, e.prototype.swipeDirection = function() {
            var t, e, i, n, o = this;
            return t = o.touchObject.startX - o.touchObject.curX, e = o.touchObject.startY - o.touchObject.curY, i = Math.atan2(e, t), n = Math.round(180 * i / Math.PI), n < 0 && (n = 360 - Math.abs(n)), n <= 45 && n >= 0 ? o.options.rtl === !1 ? "left" : "right" : n <= 360 && n >= 315 ? o.options.rtl === !1 ? "left" : "right" : n >= 135 && n <= 225 ? o.options.rtl === !1 ? "right" : "left" : o.options.verticalSwiping === !0 ? n >= 35 && n <= 135 ? "down" : "up" : "vertical"
        }, e.prototype.swipeEnd = function(t) {
            var e, i, n = this;
            if (n.dragging = !1, n.interrupted = !1, n.shouldClick = !(n.touchObject.swipeLength > 10), void 0 === n.touchObject.curX) return !1;
            if (n.touchObject.edgeHit === !0 && n.$slider.trigger("edge", [n, n.swipeDirection()]), n.touchObject.swipeLength >= n.touchObject.minSwipe) {
                switch (i = n.swipeDirection()) {
                    case "left":
                    case "down":
                        e = n.options.swipeToSlide ? n.checkNavigable(n.currentSlide + n.getSlideCount()) : n.currentSlide + n.getSlideCount(), n.currentDirection = 0;
                        break;
                    case "right":
                    case "up":
                        e = n.options.swipeToSlide ? n.checkNavigable(n.currentSlide - n.getSlideCount()) : n.currentSlide - n.getSlideCount(), n.currentDirection = 1
                }
                "vertical" != i && (n.slideHandler(e), n.touchObject = {}, n.$slider.trigger("swipe", [n, i]))
            } else n.touchObject.startX !== n.touchObject.curX && (n.slideHandler(n.currentSlide), n.touchObject = {})
        }, e.prototype.swipeHandler = function(t) {
            var e = this;
            if (!(e.options.swipe === !1 || "ontouchend" in document && e.options.swipe === !1 || e.options.draggable === !1 && t.type.indexOf("mouse") !== -1)) switch (e.touchObject.fingerCount = t.originalEvent && void 0 !== t.originalEvent.touches ? t.originalEvent.touches.length : 1, e.touchObject.minSwipe = e.listWidth / e.options.touchThreshold, e.options.verticalSwiping === !0 && (e.touchObject.minSwipe = e.listHeight / e.options.touchThreshold), t.data.action) {
                case "start":
                    e.swipeStart(t);
                    break;
                case "move":
                    e.swipeMove(t);
                    break;
                case "end":
                    e.swipeEnd(t)
            }
        }, e.prototype.swipeMove = function(t) {
            var e, i, n, o, r, s = this;
            return r = void 0 !== t.originalEvent ? t.originalEvent.touches : null, !(!s.dragging || r && 1 !== r.length) && (e = s.getLeft(s.currentSlide), s.touchObject.curX = void 0 !== r ? r[0].pageX : t.clientX, s.touchObject.curY = void 0 !== r ? r[0].pageY : t.clientY, s.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(s.touchObject.curX - s.touchObject.startX, 2))), s.options.verticalSwiping === !0 && (s.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(s.touchObject.curY - s.touchObject.startY, 2)))), i = s.swipeDirection(), "vertical" !== i ? (void 0 !== t.originalEvent && s.touchObject.swipeLength > 4 && t.preventDefault(), o = (s.options.rtl === !1 ? 1 : -1) * (s.touchObject.curX > s.touchObject.startX ? 1 : -1), s.options.verticalSwiping === !0 && (o = s.touchObject.curY > s.touchObject.startY ? 1 : -1), n = s.touchObject.swipeLength, s.touchObject.edgeHit = !1, s.options.infinite === !1 && (0 === s.currentSlide && "right" === i || s.currentSlide >= s.getDotCount() && "left" === i) && (n = s.touchObject.swipeLength * s.options.edgeFriction, s.touchObject.edgeHit = !0), s.options.vertical === !1 ? s.swipeLeft = e + n * o : s.swipeLeft = e + n * (s.$list.height() / s.listWidth) * o, s.options.verticalSwiping === !0 && (s.swipeLeft = e + n * o), s.options.fade !== !0 && s.options.touchMove !== !1 && (s.animating === !0 ? (s.swipeLeft = null, !1) : void s.setCSS(s.swipeLeft))) : void 0)
        }, e.prototype.swipeStart = function(t) {
            var e, i = this;
            return i.interrupted = !0, 1 !== i.touchObject.fingerCount || i.slideCount <= i.options.slidesToShow ? (i.touchObject = {}, !1) : (void 0 !== t.originalEvent && void 0 !== t.originalEvent.touches && (e = t.originalEvent.touches[0]), i.touchObject.startX = i.touchObject.curX = void 0 !== e ? e.pageX : t.clientX, i.touchObject.startY = i.touchObject.curY = void 0 !== e ? e.pageY : t.clientY, void(i.dragging = !0))
        }, e.prototype.unfilterSlides = e.prototype.slickUnfilter = function() {
            var t = this;
            null !== t.$slidesCache && (t.unload(), t.$slideTrack.children(this.options.slide).detach(), t.$slidesCache.appendTo(t.$slideTrack), t.reinit())
        }, e.prototype.unload = function() {
            var e = this;
            t(".slick-cloned", e.$slider).remove(), e.$dots && e.$dots.remove(), e.$prevArrow && e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.remove(), e.$nextArrow && e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.remove(), e.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "")
        }, e.prototype.unslick = function(t) {
            var e = this;
            e.$slider.trigger("unslick", [e, t]), e.destroy()
        }, e.prototype.updateArrows = function() {
            var t, e = this;
            t = Math.floor(e.options.slidesToShow / 2), e.options.arrows === !0 && e.slideCount > e.options.slidesToShow && !e.options.infinite && (e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === e.currentSlide ? (e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : e.currentSlide >= e.slideCount - e.options.slidesToShow && e.options.centerMode === !1 ? (e.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : e.currentSlide >= e.slideCount - 1 && e.options.centerMode === !0 && (e.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")))
        }, e.prototype.updateDots = function() {
            var t = this;
            null !== t.$dots && (t.$dots.find("li").removeClass("slick-active").attr("aria-hidden", "true"), t.$dots.find("li").eq(Math.floor(t.currentSlide / t.options.slidesToScroll)).addClass("slick-active").attr("aria-hidden", "false"))
        }, e.prototype.visibility = function() {
            var t = this;
            t.options.autoplay && (document[t.hidden] ? t.interrupted = !0 : t.interrupted = !1)
        }, t.fn.slick = function() {
            var t, i, n = this,
                o = arguments[0],
                r = Array.prototype.slice.call(arguments, 1),
                s = n.length;
            for (t = 0; t < s; t++)
                if ("object" == typeof o || "undefined" == typeof o ? n[t].slick = new e(n[t], o) : i = n[t].slick[o].apply(n[t].slick, r), "undefined" != typeof i) return i;
            return n
        }
    }),
    function(t, e) {
        function i() {
            C = $ = T = _ = A = E = D
        }

        function n(t, e) {
            for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i])
        }

        function o(t) {
            return parseFloat(t) || 0
        }

        function r() {
            O = {
                top: e.pageYOffset,
                left: e.pageXOffset
            }
        }

        function s() {
            return e.pageXOffset != O.left ? (r(), void T()) : void(e.pageYOffset != O.top && (r(), l()))
        }

        function a(t) {
            setTimeout(function() {
                e.pageYOffset != O.top && (O.top = e.pageYOffset, l())
            }, 0)
        }

        function l() {
            for (var t = I.length - 1; t >= 0; t--) c(I[t])
        }

        function c(t) {
            if (t.inited) {
                var e = O.top <= t.limit.start ? 0 : O.top >= t.limit.end ? 2 : 1;
                t.mode != e && m(t, e)
            }
        }

        function u() {
            for (var t = I.length - 1; t >= 0; t--)
                if (I[t].inited) {
                    var e = Math.abs(b(I[t].clone) - I[t].docOffsetTop),
                        i = Math.abs(I[t].parent.node.offsetHeight - I[t].parent.height);
                    if (e >= 2 || i >= 2) return !1
                }
            return !0
        }

        function d(t) {
            isNaN(parseFloat(t.computed.top)) || t.isCell || "none" == t.computed.display || (t.inited = !0, t.clone || v(t), "absolute" != t.parent.computed.position && "relative" != t.parent.computed.position && (t.parent.node.style.position = "relative"), c(t), t.parent.height = t.parent.node.offsetHeight, t.docOffsetTop = b(t.clone))
        }

        function p(t) {
            var e = !0;
            t.clone && g(t), n(t.node.style, t.css);
            for (var i = I.length - 1; i >= 0; i--)
                if (I[i].node !== t.node && I[i].parent.node === t.parent.node) {
                    e = !1;
                    break
                }
            e && (t.parent.node.style.position = t.parent.css.position), t.mode = -1
        }

        function f() {
            for (var t = I.length - 1; t >= 0; t--) d(I[t])
        }

        function h() {
            for (var t = I.length - 1; t >= 0; t--) p(I[t])
        }

        function m(t, e) {
            var i = t.node.style;
            switch (e) {
                case 0:
                    i.position = "absolute", i.left = t.offset.left + "px", i.right = t.offset.right + "px", i.top = t.offset.top + "px", i.bottom = "auto", i.width = "auto", i.marginLeft = 0, i.marginRight = 0, i.marginTop = 0;
                    break;
                case 1:
                    i.position = "fixed", i.left = t.box.left + "px", i.right = t.box.right + "px", i.top = t.css.top, i.bottom = "auto", i.width = "auto", i.marginLeft = 0, i.marginRight = 0, i.marginTop = 0;
                    break;
                case 2:
                    i.position = "absolute", i.left = t.offset.left + "px", i.right = t.offset.right + "px", i.top = "auto", i.bottom = 0, i.width = "auto", i.marginLeft = 0, i.marginRight = 0
            }
            t.mode = e
        }

        function v(t) {
            t.clone = document.createElement("div");
            var e = t.node.nextSibling || t.node,
                i = t.clone.style;
            i.height = t.height + "px", i.width = t.width + "px", i.marginTop = t.computed.marginTop, i.marginBottom = t.computed.marginBottom, i.marginLeft = t.computed.marginLeft, i.marginRight = t.computed.marginRight, i.padding = i.border = i.borderSpacing = 0, i.fontSize = "1em", i.position = "static", i.cssFloat = t.computed.cssFloat, t.node.parentNode.insertBefore(t.clone, e)
        }

        function g(t) {
            t.clone.parentNode.removeChild(t.clone), t.clone = void 0
        }

        function y(t) {
            var e = getComputedStyle(t),
                i = t.parentNode,
                n = getComputedStyle(i),
                r = t.style.position;
            t.style.position = "relative";
            var s = {
                    top: e.top,
                    marginTop: e.marginTop,
                    marginBottom: e.marginBottom,
                    marginLeft: e.marginLeft,
                    marginRight: e.marginRight,
                    cssFloat: e.cssFloat,
                    display: e.display
                },
                a = {
                    top: o(e.top),
                    marginBottom: o(e.marginBottom),
                    paddingLeft: o(e.paddingLeft),
                    paddingRight: o(e.paddingRight),
                    borderLeftWidth: o(e.borderLeftWidth),
                    borderRightWidth: o(e.borderRightWidth)
                };
            t.style.position = r;
            var l = {
                    position: t.style.position,
                    top: t.style.top,
                    bottom: t.style.bottom,
                    left: t.style.left,
                    right: t.style.right,
                    width: t.style.width,
                    marginTop: t.style.marginTop,
                    marginLeft: t.style.marginLeft,
                    marginRight: t.style.marginRight
                },
                c = w(t),
                u = w(i),
                d = {
                    node: i,
                    css: {
                        position: i.style.position
                    },
                    computed: {
                        position: n.position
                    },
                    numeric: {
                        borderLeftWidth: o(n.borderLeftWidth),
                        borderRightWidth: o(n.borderRightWidth),
                        borderTopWidth: o(n.borderTopWidth),
                        borderBottomWidth: o(n.borderBottomWidth)
                    }
                },
                p = {
                    node: t,
                    box: {
                        left: c.win.left,
                        right: F.clientWidth - c.win.right
                    },
                    offset: {
                        top: c.win.top - u.win.top - d.numeric.borderTopWidth,
                        left: c.win.left - u.win.left - d.numeric.borderLeftWidth,
                        right: -c.win.right + u.win.right - d.numeric.borderRightWidth
                    },
                    css: l,
                    isCell: "table-cell" == e.display,
                    computed: s,
                    numeric: a,
                    width: c.win.right - c.win.left,
                    height: c.win.bottom - c.win.top,
                    mode: -1,
                    inited: !1,
                    parent: d,
                    limit: {
                        start: c.doc.top - a.top,
                        end: u.doc.top + i.offsetHeight - d.numeric.borderBottomWidth - t.offsetHeight - a.top - a.marginBottom
                    }
                };
            return p
        }

        function b(t) {
            for (var e = 0; t;) e += t.offsetTop, t = t.offsetParent;
            return e
        }

        function w(t) {
            var i = t.getBoundingClientRect();
            return {
                doc: {
                    top: i.top + e.pageYOffset,
                    left: i.left + e.pageXOffset
                },
                win: i
            }
        }

        function k() {
            L = setInterval(function() {
                !u() && T()
            }, 500)
        }

        function S() {
            clearInterval(L)
        }

        function x() {
            M && (document[j] ? S() : k())
        }

        function C() {
            M || (r(), f(), e.addEventListener("scroll", s), e.addEventListener("wheel", a), e.addEventListener("resize", T), e.addEventListener("orientationchange", T), t.addEventListener(H, x), k(), M = !0)
        }

        function T() {
            if (M) {
                h();
                for (var t = I.length - 1; t >= 0; t--) I[t] = y(I[t].node);
                f()
            }
        }

        function _() {
            e.removeEventListener("scroll", s), e.removeEventListener("wheel", a), e.removeEventListener("resize", T), e.removeEventListener("orientationchange", T), t.removeEventListener(H, x), S(), M = !1
        }

        function A() {
            _(), h()
        }

        function E() {
            for (A(); I.length;) I.pop()
        }

        function $(t) {
            for (var e = I.length - 1; e >= 0; e--)
                if (I[e].node === t) return;
            var i = y(t);
            I.push(i), M ? d(i) : C()
        }

        function P(t) {
            for (var e = I.length - 1; e >= 0; e--) I[e].node === t && (p(I[e]), I.splice(e, 1))
        }
        var O, L, I = [],
            M = !1,
            F = t.documentElement,
            D = function() {},
            j = "hidden",
            H = "visibilitychange";
        void 0 !== t.webkitHidden && (j = "webkitHidden", H = "webkitvisibilitychange"), e.getComputedStyle || i();
        for (var R = ["", "-webkit-", "-moz-", "-ms-"], W = document.createElement("div"), z = R.length - 1; z >= 0; z--) {
            try {
                W.style.position = R[z] + "sticky"
            } catch (t) {}
            "" != W.style.position && i()
        }
        r(), e.Stickyfill = {
            stickies: I,
            add: $,
            remove: P,
            init: C,
            rebuild: T,
            pause: _,
            stop: A,
            kill: E
        }
    }(document, window), window.jQuery && ! function(t) {
    t.fn.Stickyfill = function(t) {
        return this.each(function() {
            Stickyfill.add(this)
        }), this
    }
}(window.jQuery), ! function(t, e) {
    "function" == typeof define && define.amd ? define([], function() {
        return t.svg4everybody = e()
    }) : "object" == typeof exports ? module.exports = e() : t.svg4everybody = e()
}(this, function() {
    function t(t, e) {
        if (e) {
            var i = document.createDocumentFragment(),
                n = !t.getAttribute("viewBox") && e.getAttribute("viewBox");
            n && t.setAttribute("viewBox", n);
            for (var o = e.cloneNode(!0); o.childNodes.length;) i.appendChild(o.firstChild);
            t.appendChild(i)
        }
    }

    function e(e) {
        e.onreadystatechange = function() {
            if (4 === e.readyState) {
                var i = e._cachedDocument;
                i || (i = e._cachedDocument = document.implementation.createHTMLDocument(""), i.body.innerHTML = e.responseText, e._cachedTarget = {}), e._embeds.splice(0).map(function(n) {
                    var o = e._cachedTarget[n.id];
                    o || (o = e._cachedTarget[n.id] = i.getElementById(n.id)), t(n.svg, o)
                })
            }
        }, e.onreadystatechange()
    }

    function i(i) {
        function n() {
            for (var i = 0; i < d.length;) {
                var s = d[i],
                    a = s.parentNode;
                if (a && /svg/i.test(a.nodeName)) {
                    var l = s.getAttribute("xlink:href");
                    if (o && (!r.validate || r.validate(l, a, s))) {
                        a.removeChild(s);
                        var p = l.split("#"),
                            f = p.shift(),
                            h = p.join("#");
                        if (f.length) {
                            var m = c[f];
                            m || (m = c[f] = new XMLHttpRequest, m.open("GET", f), m.send(), m._embeds = []), m._embeds.push({
                                svg: a,
                                id: h
                            }), e(m)
                        } else t(a, document.getElementById(h))
                    }
                } else ++i
            }
            u(n, 67)
        }
        var o, r = Object(i),
            s = /\bTrident\/[567]\b|\bMSIE (?:9|10)\.0\b/,
            a = /\bAppleWebKit\/(\d+)\b/,
            l = /\bEdge\/12\.(\d+)\b/;
        o = "polyfill" in r ? r.polyfill : s.test(navigator.userAgent) || (navigator.userAgent.match(l) || [])[1] < 10547 || (navigator.userAgent.match(a) || [])[1] < 537;
        var c = {},
            u = window.requestAnimationFrame || setTimeout,
            d = document.getElementsByTagName("use");
        o && n()
    }
    return i
}), ! function(t, e) {
    "function" == typeof define && define.amd ? define("typeahead.js", ["jquery"], function(t) {
        return e(t)
    }) : "object" == typeof exports ? module.exports = e(require("jquery")) : e(jQuery)
}(this, function(t) {
    var e = function() {
            "use strict";
            return {
                isMsie: function() {
                    return !!/(msie|trident)/i.test(navigator.userAgent) && navigator.userAgent.match(/(msie |rv:)(\d+(.\d+)?)/i)[2]
                },
                isBlankString: function(t) {
                    return !t || /^\s*$/.test(t)
                },
                escapeRegExChars: function(t) {
                    return t.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&")
                },
                isString: function(t) {
                    return "string" == typeof t
                },
                isNumber: function(t) {
                    return "number" == typeof t
                },
                isArray: t.isArray,
                isFunction: t.isFunction,
                isObject: t.isPlainObject,
                isUndefined: function(t) {
                    return "undefined" == typeof t
                },
                isElement: function(t) {
                    return !(!t || 1 !== t.nodeType)
                },
                isJQuery: function(e) {
                    return e instanceof t
                },
                toStr: function(t) {
                    return e.isUndefined(t) || null === t ? "" : t + ""
                },
                bind: t.proxy,
                each: function(e, i) {
                    function n(t, e) {
                        return i(e, t)
                    }
                    t.each(e, n)
                },
                map: t.map,
                filter: t.grep,
                every: function(e, i) {
                    var n = !0;
                    return e ? (t.each(e, function(t, o) {
                        return !!(n = i.call(null, o, t, e)) && void 0
                    }), !!n) : n
                },
                some: function(e, i) {
                    var n = !1;
                    return e ? (t.each(e, function(t, o) {
                        return !(n = i.call(null, o, t, e)) && void 0
                    }), !!n) : n
                },
                mixin: t.extend,
                identity: function(t) {
                    return t
                },
                clone: function(e) {
                    return t.extend(!0, {}, e)
                },
                getIdGenerator: function() {
                    var t = 0;
                    return function() {
                        return t++
                    }
                },
                templatify: function(e) {
                    function i() {
                        return String(e)
                    }
                    return t.isFunction(e) ? e : i
                },
                defer: function(t) {
                    setTimeout(t, 0)
                },
                debounce: function(t, e, i) {
                    var n, o;
                    return function() {
                        var r, s, a = this,
                            l = arguments;
                        return r = function() {
                            n = null, i || (o = t.apply(a, l))
                        }, s = i && !n, clearTimeout(n), n = setTimeout(r, e), s && (o = t.apply(a, l)), o
                    }
                },
                throttle: function(t, e) {
                    var i, n, o, r, s, a;
                    return s = 0, a = function() {
                        s = new Date, o = null, r = t.apply(i, n)
                    },
                        function() {
                            var l = new Date,
                                c = e - (l - s);
                            return i = this, n = arguments, 0 >= c ? (clearTimeout(o), o = null, s = l, r = t.apply(i, n)) : o || (o = setTimeout(a, c)), r
                        }
                },
                stringify: function(t) {
                    return e.isString(t) ? t : JSON.stringify(t)
                },
                noop: function() {}
            }
        }(),
        i = function() {
            "use strict";

            function t(t) {
                var s, a;
                return a = e.mixin({}, r, t), s = {
                    css: o(),
                    classes: a,
                    html: i(a),
                    selectors: n(a)
                }, {
                    css: s.css,
                    html: s.html,
                    classes: s.classes,
                    selectors: s.selectors,
                    mixin: function(t) {
                        e.mixin(t, s)
                    }
                }
            }

            function i(t) {
                return {
                    wrapper: '<span class="' + t.wrapper + '"></span>',
                    menu: '<div class="' + t.menu + '"></div>'
                }
            }

            function n(t) {
                var i = {};
                return e.each(t, function(t, e) {
                    i[e] = "." + t
                }), i
            }

            function o() {
                var t = {
                    wrapper: {
                        position: "relative",
                        display: "inline-block"
                    },
                    hint: {
                        position: "absolute",
                        top: "0",
                        left: "0",
                        borderColor: "transparent",
                        boxShadow: "none",
                        opacity: "1"
                    },
                    input: {
                        position: "relative",
                        verticalAlign: "top",
                        backgroundColor: "transparent"
                    },
                    inputWithNoHint: {
                        position: "relative",
                        verticalAlign: "top"
                    },
                    menu: {
                        position: "absolute",
                        top: "100%",
                        left: "0",
                        zIndex: "100",
                        display: "none"
                    },
                    ltr: {
                        left: "0",
                        right: "auto"
                    },
                    rtl: {
                        left: "auto",
                        right: " 0"
                    }
                };
                return e.isMsie() && e.mixin(t.input, {
                    backgroundImage: "url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)"
                }), t
            }
            var r = {
                wrapper: "twitter-typeahead",
                input: "tt-input",
                hint: "tt-hint",
                menu: "tt-menu",
                dataset: "tt-dataset",
                suggestion: "tt-suggestion",
                selectable: "tt-selectable",
                empty: "tt-empty",
                open: "tt-open",
                cursor: "tt-cursor",
                highlight: "tt-highlight"
            };
            return t
        }(),
        n = function() {
            "use strict";

            function i(e) {
                e && e.el || t.error("EventBus initialized without el"), this.$el = t(e.el)
            }
            var n, o;
            return n = "typeahead:", o = {
                render: "rendered",
                cursorchange: "cursorchanged",
                select: "selected",
                autocomplete: "autocompleted"
            }, e.mixin(i.prototype, {
                _trigger: function(e, i) {
                    var o;
                    return o = t.Event(n + e), (i = i || []).unshift(o), this.$el.trigger.apply(this.$el, i), o
                },
                before: function(t) {
                    var e, i;
                    return e = [].slice.call(arguments, 1), i = this._trigger("before" + t, e), i.isDefaultPrevented()
                },
                trigger: function(t) {
                    var e;
                    this._trigger(t, [].slice.call(arguments, 1)), (e = o[t]) && this._trigger(e, [].slice.call(arguments, 1))
                }
            }), i
        }(),
        o = function() {
            "use strict";

            function t(t, e, i, n) {
                var o;
                if (!i) return this;
                for (e = e.split(l), i = n ? a(i, n) : i, this._callbacks = this._callbacks || {}; o = e.shift();) this._callbacks[o] = this._callbacks[o] || {
                        sync: [],
                        async: []
                    }, this._callbacks[o][t].push(i);
                return this
            }

            function e(e, i, n) {
                return t.call(this, "async", e, i, n)
            }

            function i(e, i, n) {
                return t.call(this, "sync", e, i, n)
            }

            function n(t) {
                var e;
                if (!this._callbacks) return this;
                for (t = t.split(l); e = t.shift();) delete this._callbacks[e];
                return this
            }

            function o(t) {
                var e, i, n, o, s;
                if (!this._callbacks) return this;
                for (t = t.split(l), n = [].slice.call(arguments, 1);
                     (e = t.shift()) && (i = this._callbacks[e]);) o = r(i.sync, this, [e].concat(n)), s = r(i.async, this, [e].concat(n)), o() && c(s);
                return this
            }

            function r(t, e, i) {
                function n() {
                    for (var n, o = 0, r = t.length; !n && r > o; o += 1) n = t[o].apply(e, i) === !1;
                    return !n
                }
                return n
            }

            function s() {
                var t;
                return t = window.setImmediate ? function(t) {
                    setImmediate(function() {
                        t()
                    })
                } : function(t) {
                    setTimeout(function() {
                        t()
                    }, 0)
                }
            }

            function a(t, e) {
                return t.bind ? t.bind(e) : function() {
                    t.apply(e, [].slice.call(arguments, 0))
                }
            }
            var l = /\s+/,
                c = s();
            return {
                onSync: i,
                onAsync: e,
                off: n,
                trigger: o
            }
        }(),
        r = function(t) {
            "use strict";

            function i(t, i, n) {
                for (var o, r = [], s = 0, a = t.length; a > s; s++) r.push(e.escapeRegExChars(t[s]));
                return o = n ? "\\b(" + r.join("|") + ")\\b" : "(" + r.join("|") + ")", i ? new RegExp(o) : new RegExp(o, "i")
            }
            var n = {
                node: null,
                pattern: null,
                tagName: "strong",
                className: null,
                wordsOnly: !1,
                caseSensitive: !1
            };
            return function(o) {
                function r(e) {
                    var i, n, r;
                    return (i = a.exec(e.data)) && (r = t.createElement(o.tagName), o.className && (r.className = o.className), n = e.splitText(i.index), n.splitText(i[0].length), r.appendChild(n.cloneNode(!0)), e.parentNode.replaceChild(r, n)), !!i
                }

                function s(t, e) {
                    for (var i, n = 3, o = 0; o < t.childNodes.length; o++) i = t.childNodes[o], i.nodeType === n ? o += e(i) ? 1 : 0 : s(i, e)
                }
                var a;
                o = e.mixin({}, n, o), o.node && o.pattern && (o.pattern = e.isArray(o.pattern) ? o.pattern : [o.pattern], a = i(o.pattern, o.caseSensitive, o.wordsOnly), s(o.node, r))
            }
        }(window.document),
        s = function() {
            "use strict";

            function i(i, o) {
                i = i || {}, i.input || t.error("input is missing"), o.mixin(this), this.$hint = t(i.hint), this.$input = t(i.input), this.query = this.$input.val(), this.queryWhenFocused = this.hasFocus() ? this.query : null, this.$overflowHelper = n(this.$input), this._checkLanguageDirection(), 0 === this.$hint.length && (this.setHint = this.getHint = this.clearHint = this.clearHintIfInvalid = e.noop)
            }

            function n(e) {
                return t('<pre aria-hidden="true"></pre>').css({
                    position: "absolute",
                    visibility: "hidden",
                    whiteSpace: "pre",
                    fontFamily: e.css("font-family"),
                    fontSize: e.css("font-size"),
                    fontStyle: e.css("font-style"),
                    fontVariant: e.css("font-variant"),
                    fontWeight: e.css("font-weight"),
                    wordSpacing: e.css("word-spacing"),
                    letterSpacing: e.css("letter-spacing"),
                    textIndent: e.css("text-indent"),
                    textRendering: e.css("text-rendering"),
                    textTransform: e.css("text-transform")
                }).insertAfter(e)
            }

            function r(t, e) {
                return i.normalizeQuery(t) === i.normalizeQuery(e)
            }

            function s(t) {
                return t.altKey || t.ctrlKey || t.metaKey || t.shiftKey
            }
            var a;
            return a = {
                9: "tab",
                27: "esc",
                37: "left",
                39: "right",
                13: "enter",
                38: "up",
                40: "down"
            }, i.normalizeQuery = function(t) {
                return e.toStr(t).replace(/^\s*/g, "").replace(/\s{2,}/g, " ")
            }, e.mixin(i.prototype, o, {
                _onBlur: function() {
                    this.resetInputValue(), this.trigger("blurred")
                },
                _onFocus: function() {
                    this.queryWhenFocused = this.query, this.trigger("focused")
                },
                _onKeydown: function(t) {
                    var e = a[t.which || t.keyCode];
                    this._managePreventDefault(e, t), e && this._shouldTrigger(e, t) && this.trigger(e + "Keyed", t)
                },
                _onInput: function() {
                    this._setQuery(this.getInputValue()), this.clearHintIfInvalid(), this._checkLanguageDirection()
                },
                _managePreventDefault: function(t, e) {
                    var i;
                    switch (t) {
                        case "up":
                        case "down":
                            i = !s(e);
                            break;
                        default:
                            i = !1
                    }
                    i && e.preventDefault()
                },
                _shouldTrigger: function(t, e) {
                    var i;
                    switch (t) {
                        case "tab":
                            i = !s(e);
                            break;
                        default:
                            i = !0
                    }
                    return i
                },
                _checkLanguageDirection: function() {
                    var t = (this.$input.css("direction") || "ltr").toLowerCase();
                    this.dir !== t && (this.dir = t, this.$hint.attr("dir", t), this.trigger("langDirChanged", t))
                },
                _setQuery: function(t, e) {
                    var i, n;
                    i = r(t, this.query), n = !!i && this.query.length !== t.length, this.query = t, e || i ? !e && n && this.trigger("whitespaceChanged", this.query) : this.trigger("queryChanged", this.query)
                },
                bind: function() {
                    var t, i, n, o, r = this;
                    return t = e.bind(this._onBlur, this), i = e.bind(this._onFocus, this), n = e.bind(this._onKeydown, this),
                        o = e.bind(this._onInput, this), this.$input.on("blur.tt", t).on("focus.tt", i).on("keydown.tt", n), !e.isMsie() || e.isMsie() > 9 ? this.$input.on("input.tt", o) : this.$input.on("keydown.tt keypress.tt cut.tt paste.tt", function(t) {
                        a[t.which || t.keyCode] || e.defer(e.bind(r._onInput, r, t))
                    }), this
                },
                focus: function() {
                    this.$input.focus()
                },
                blur: function() {
                    this.$input.blur()
                },
                getLangDir: function() {
                    return this.dir
                },
                getQuery: function() {
                    return this.query || ""
                },
                setQuery: function(t, e) {
                    this.setInputValue(t), this._setQuery(t, e)
                },
                hasQueryChangedSinceLastFocus: function() {
                    return this.query !== this.queryWhenFocused
                },
                getInputValue: function() {
                    return this.$input.val()
                },
                setInputValue: function(t) {
                    this.$input.val(t), this.clearHintIfInvalid(), this._checkLanguageDirection()
                },
                resetInputValue: function() {
                    this.setInputValue(this.query)
                },
                getHint: function() {
                    return this.$hint.val()
                },
                setHint: function(t) {
                    this.$hint.val(t)
                },
                clearHint: function() {
                    this.setHint("")
                },
                clearHintIfInvalid: function() {
                    var t, e, i, n;
                    t = this.getInputValue(), e = this.getHint(), i = t !== e && 0 === e.indexOf(t), n = "" !== t && i && !this.hasOverflow(), !n && this.clearHint()
                },
                hasFocus: function() {
                    return this.$input.is(":focus")
                },
                hasOverflow: function() {
                    var t = this.$input.width() - 2;
                    return this.$overflowHelper.text(this.getInputValue()), this.$overflowHelper.width() >= t
                },
                isCursorAtEnd: function() {
                    var t, i, n;
                    return t = this.$input.val().length, i = this.$input[0].selectionStart, e.isNumber(i) ? i === t : !document.selection || (n = document.selection.createRange(), n.moveStart("character", -t), t === n.text.length)
                },
                destroy: function() {
                    this.$hint.off(".tt"), this.$input.off(".tt"), this.$overflowHelper.remove(), this.$hint = this.$input = this.$overflowHelper = t("<div>")
                }
            }), i
        }(),
        a = function() {
            "use strict";

            function i(i, o) {
                i = i || {}, i.templates = i.templates || {}, i.templates.notFound = i.templates.notFound || i.templates.empty, i.source || t.error("missing source"), i.node || t.error("missing node"), i.name && !a(i.name) && t.error("invalid dataset name: " + i.name), o.mixin(this), this.highlight = !!i.highlight, this.name = i.name || c(), this.limit = i.limit || 5, this.displayFn = n(i.display || i.displayKey), this.templates = s(i.templates, this.displayFn), this.source = i.source.__ttAdapter ? i.source.__ttAdapter() : i.source, this.async = e.isUndefined(i.async) ? this.source.length > 2 : !!i.async, this._resetLastSuggestion(), this.$el = t(i.node).addClass(this.classes.dataset).addClass(this.classes.dataset + "-" + this.name)
            }

            function n(t) {
                function i(e) {
                    return e[t]
                }
                return t = t || e.stringify, e.isFunction(t) ? t : i
            }

            function s(i, n) {
                function o(e) {
                    return t("<div>").text(n(e))
                }
                return {
                    notFound: i.notFound && e.templatify(i.notFound),
                    pending: i.pending && e.templatify(i.pending),
                    header: i.header && e.templatify(i.header),
                    footer: i.footer && e.templatify(i.footer),
                    suggestion: i.suggestion || o
                }
            }

            function a(t) {
                return /^[_a-zA-Z0-9-]+$/.test(t)
            }
            var l, c;
            return l = {
                val: "tt-selectable-display",
                obj: "tt-selectable-object"
            }, c = e.getIdGenerator(), i.extractData = function(e) {
                var i = t(e);
                return i.data(l.obj) ? {
                    val: i.data(l.val) || "",
                    obj: i.data(l.obj) || null
                } : null
            }, e.mixin(i.prototype, o, {
                _overwrite: function(t, e) {
                    e = e || [], e.length ? this._renderSuggestions(t, e) : this.async && this.templates.pending ? this._renderPending(t) : !this.async && this.templates.notFound ? this._renderNotFound(t) : this._empty(), this.trigger("rendered", this.name, e, !1)
                },
                _append: function(t, e) {
                    e = e || [], e.length && this.$lastSuggestion.length ? this._appendSuggestions(t, e) : e.length ? this._renderSuggestions(t, e) : !this.$lastSuggestion.length && this.templates.notFound && this._renderNotFound(t), this.trigger("rendered", this.name, e, !0)
                },
                _renderSuggestions: function(t, e) {
                    var i;
                    i = this._getSuggestionsFragment(t, e), this.$lastSuggestion = i.children().last(), this.$el.html(i).prepend(this._getHeader(t, e)).append(this._getFooter(t, e))
                },
                _appendSuggestions: function(t, e) {
                    var i, n;
                    i = this._getSuggestionsFragment(t, e), n = i.children().last(), this.$lastSuggestion.after(i), this.$lastSuggestion = n
                },
                _renderPending: function(t) {
                    var e = this.templates.pending;
                    this._resetLastSuggestion(), e && this.$el.html(e({
                        query: t,
                        dataset: this.name
                    }))
                },
                _renderNotFound: function(t) {
                    var e = this.templates.notFound;
                    this._resetLastSuggestion(), e && this.$el.html(e({
                        query: t,
                        dataset: this.name
                    }))
                },
                _empty: function() {
                    this.$el.empty(), this._resetLastSuggestion()
                },
                _getSuggestionsFragment: function(i, n) {
                    var o, s = this;
                    return o = document.createDocumentFragment(), e.each(n, function(e) {
                        var n, r;
                        r = s._injectQuery(i, e), n = t(s.templates.suggestion(r)).data(l.obj, e).data(l.val, s.displayFn(e)).addClass(s.classes.suggestion + " " + s.classes.selectable), o.appendChild(n[0])
                    }), this.highlight && r({
                        className: this.classes.highlight,
                        node: o,
                        pattern: i
                    }), t(o)
                },
                _getFooter: function(t, e) {
                    return this.templates.footer ? this.templates.footer({
                        query: t,
                        suggestions: e,
                        dataset: this.name
                    }) : null
                },
                _getHeader: function(t, e) {
                    return this.templates.header ? this.templates.header({
                        query: t,
                        suggestions: e,
                        dataset: this.name
                    }) : null
                },
                _resetLastSuggestion: function() {
                    this.$lastSuggestion = t()
                },
                _injectQuery: function(t, i) {
                    return e.isObject(i) ? e.mixin({
                        _query: t
                    }, i) : i
                },
                update: function(e) {
                    function i(t) {
                        s || (s = !0, t = (t || []).slice(0, o.limit), a = t.length, o._overwrite(e, t), a < o.limit && o.async && o.trigger("asyncRequested", e))
                    }

                    function n(i) {
                        i = i || [], !r && a < o.limit && (o.cancel = t.noop, a += i.length, o._append(e, i.slice(0, o.limit - a)), o.async && o.trigger("asyncReceived", e))
                    }
                    var o = this,
                        r = !1,
                        s = !1,
                        a = 0;
                    this.cancel(), this.cancel = function() {
                        r = !0, o.cancel = t.noop, o.async && o.trigger("asyncCanceled", e)
                    }, this.source(e, i, n), !s && i([])
                },
                cancel: t.noop,
                clear: function() {
                    this._empty(), this.cancel(), this.trigger("cleared")
                },
                isEmpty: function() {
                    return this.$el.is(":empty")
                },
                destroy: function() {
                    this.$el = t("<div>")
                }
            }), i
        }(),
        l = function() {
            "use strict";

            function i(i, n) {
                function o(e) {
                    var i = r.$node.find(e.node).first();
                    return e.node = i.length ? i : t("<div>").appendTo(r.$node), new a(e, n)
                }
                var r = this;
                i = i || {}, i.node || t.error("node is required"), n.mixin(this), this.$node = t(i.node), this.query = null, this.datasets = e.map(i.datasets, o)
            }
            return e.mixin(i.prototype, o, {
                _onSelectableClick: function(e) {
                    this.trigger("selectableClicked", t(e.currentTarget))
                },
                _onRendered: function(t, e, i, n) {
                    this.$node.toggleClass(this.classes.empty, this._allDatasetsEmpty()), this.trigger("datasetRendered", e, i, n)
                },
                _onCleared: function() {
                    this.$node.toggleClass(this.classes.empty, this._allDatasetsEmpty()), this.trigger("datasetCleared")
                },
                _propagate: function() {
                    this.trigger.apply(this, arguments)
                },
                _allDatasetsEmpty: function() {
                    function t(t) {
                        return t.isEmpty()
                    }
                    return e.every(this.datasets, t)
                },
                _getSelectables: function() {
                    return this.$node.find(this.selectors.selectable)
                },
                _removeCursor: function() {
                    var t = this.getActiveSelectable();
                    t && t.removeClass(this.classes.cursor)
                },
                _ensureVisible: function(t) {
                    var e, i, n, o;
                    e = t.position().top, i = e + t.outerHeight(!0), n = this.$node.scrollTop(), o = this.$node.height() + parseInt(this.$node.css("paddingTop"), 10) + parseInt(this.$node.css("paddingBottom"), 10), 0 > e ? this.$node.scrollTop(n + e) : i > o && this.$node.scrollTop(n + (i - o))
                },
                bind: function() {
                    var t, i = this;
                    return t = e.bind(this._onSelectableClick, this), this.$node.on("click.tt", this.selectors.selectable, t), e.each(this.datasets, function(t) {
                        t.onSync("asyncRequested", i._propagate, i).onSync("asyncCanceled", i._propagate, i).onSync("asyncReceived", i._propagate, i).onSync("rendered", i._onRendered, i).onSync("cleared", i._onCleared, i)
                    }), this
                },
                isOpen: function() {
                    return this.$node.hasClass(this.classes.open)
                },
                open: function() {
                    this.$node.addClass(this.classes.open)
                },
                close: function() {
                    this.$node.removeClass(this.classes.open), this._removeCursor()
                },
                setLanguageDirection: function(t) {
                    this.$node.attr("dir", t)
                },
                selectableRelativeToCursor: function(t) {
                    var e, i, n, o;
                    return i = this.getActiveSelectable(), e = this._getSelectables(), n = i ? e.index(i) : -1, o = n + t, o = (o + 1) % (e.length + 1) - 1, o = -1 > o ? e.length - 1 : o, -1 === o ? null : e.eq(o)
                },
                setCursor: function(t) {
                    this._removeCursor(), (t = t && t.first()) && (t.addClass(this.classes.cursor), this._ensureVisible(t))
                },
                getSelectableData: function(t) {
                    return t && t.length ? a.extractData(t) : null
                },
                getActiveSelectable: function() {
                    var t = this._getSelectables().filter(this.selectors.cursor).first();
                    return t.length ? t : null
                },
                getTopSelectable: function() {
                    var t = this._getSelectables().first();
                    return t.length ? t : null
                },
                update: function(t) {
                    function i(e) {
                        e.update(t)
                    }
                    var n = t !== this.query;
                    return n && (this.query = t, e.each(this.datasets, i)), n
                },
                empty: function() {
                    function t(t) {
                        t.clear()
                    }
                    e.each(this.datasets, t), this.query = null, this.$node.addClass(this.classes.empty)
                },
                destroy: function() {
                    function i(t) {
                        t.destroy()
                    }
                    this.$node.off(".tt"), this.$node = t("<div>"), e.each(this.datasets, i)
                }
            }), i
        }(),
        c = function() {
            "use strict";

            function t() {
                l.apply(this, [].slice.call(arguments, 0))
            }
            var i = l.prototype;
            return e.mixin(t.prototype, l.prototype, {
                open: function() {
                    return !this._allDatasetsEmpty() && this._show(), i.open.apply(this, [].slice.call(arguments, 0))
                },
                close: function() {
                    return this._hide(), i.close.apply(this, [].slice.call(arguments, 0))
                },
                _onRendered: function() {
                    return this._allDatasetsEmpty() ? this._hide() : this.isOpen() && this._show(), i._onRendered.apply(this, [].slice.call(arguments, 0))
                },
                _onCleared: function() {
                    return this._allDatasetsEmpty() ? this._hide() : this.isOpen() && this._show(), i._onCleared.apply(this, [].slice.call(arguments, 0))
                },
                setLanguageDirection: function(t) {
                    return this.$node.css("ltr" === t ? this.css.ltr : this.css.rtl), i.setLanguageDirection.apply(this, [].slice.call(arguments, 0))
                },
                _hide: function() {
                    this.$node.hide()
                },
                _show: function() {
                    this.$node.css("display", "block")
                }
            }), t
        }(),
        u = function() {
            "use strict";

            function i(i, o) {
                var r, s, a, l, c, u, d, p, f, h, m;
                i = i || {}, i.input || t.error("missing input"), i.menu || t.error("missing menu"), i.eventBus || t.error("missing event bus"), o.mixin(this), this.eventBus = i.eventBus, this.minLength = e.isNumber(i.minLength) ? i.minLength : 1, this.input = i.input, this.menu = i.menu, this.enabled = !0, this.active = !1, this.input.hasFocus() && this.activate(), this.dir = this.input.getLangDir(), this._hacks(), this.menu.bind().onSync("selectableClicked", this._onSelectableClicked, this).onSync("asyncRequested", this._onAsyncRequested, this).onSync("asyncCanceled", this._onAsyncCanceled, this).onSync("asyncReceived", this._onAsyncReceived, this).onSync("datasetRendered", this._onDatasetRendered, this).onSync("datasetCleared", this._onDatasetCleared, this), r = n(this, "activate", "open", "_onFocused"), s = n(this, "deactivate", "_onBlurred"), a = n(this, "isActive", "isOpen", "_onEnterKeyed"), l = n(this, "isActive", "isOpen", "_onTabKeyed"), c = n(this, "isActive", "_onEscKeyed"), u = n(this, "isActive", "open", "_onUpKeyed"), d = n(this, "isActive", "open", "_onDownKeyed"), p = n(this, "isActive", "isOpen", "_onLeftKeyed"), f = n(this, "isActive", "isOpen", "_onRightKeyed"), h = n(this, "_openIfActive", "_onQueryChanged"), m = n(this, "_openIfActive", "_onWhitespaceChanged"), this.input.bind().onSync("focused", r, this).onSync("blurred", s, this).onSync("enterKeyed", a, this).onSync("tabKeyed", l, this).onSync("escKeyed", c, this).onSync("upKeyed", u, this).onSync("downKeyed", d, this).onSync("leftKeyed", p, this).onSync("rightKeyed", f, this).onSync("queryChanged", h, this).onSync("whitespaceChanged", m, this).onSync("langDirChanged", this._onLangDirChanged, this)
            }

            function n(t) {
                var i = [].slice.call(arguments, 1);
                return function() {
                    var n = [].slice.call(arguments);
                    e.each(i, function(e) {
                        return t[e].apply(t, n)
                    })
                }
            }
            return e.mixin(i.prototype, {
                _hacks: function() {
                    var i, n;
                    i = this.input.$input || t("<div>"), n = this.menu.$node || t("<div>"), i.on("blur.tt", function(t) {
                        var o, r, s;
                        o = document.activeElement, r = n.is(o), s = n.has(o).length > 0, e.isMsie() && (r || s) && (t.preventDefault(), t.stopImmediatePropagation(), e.defer(function() {
                            i.focus()
                        }))
                    }), n.on("mousedown.tt", function(t) {
                        t.preventDefault()
                    })
                },
                _onSelectableClicked: function(t, e) {
                    this.select(e)
                },
                _onDatasetCleared: function() {
                    this._updateHint()
                },
                _onDatasetRendered: function(t, e, i, n) {
                    this._updateHint(), this.eventBus.trigger("render", i, n, e)
                },
                _onAsyncRequested: function(t, e, i) {
                    this.eventBus.trigger("asyncrequest", i, e)
                },
                _onAsyncCanceled: function(t, e, i) {
                    this.eventBus.trigger("asynccancel", i, e)
                },
                _onAsyncReceived: function(t, e, i) {
                    this.eventBus.trigger("asyncreceive", i, e)
                },
                _onFocused: function() {
                    this._minLengthMet() && this.menu.update(this.input.getQuery())
                },
                _onBlurred: function() {
                    this.input.hasQueryChangedSinceLastFocus() && this.eventBus.trigger("change", this.input.getQuery())
                },
                _onEnterKeyed: function(t, e) {
                    var i;
                    (i = this.menu.getActiveSelectable()) && this.select(i) && e.preventDefault()
                },
                _onTabKeyed: function(t, e) {
                    var i;
                    (i = this.menu.getActiveSelectable()) ? this.select(i) && e.preventDefault(): (i = this.menu.getTopSelectable()) && this.autocomplete(i) && e.preventDefault()
                },
                _onEscKeyed: function() {
                    this.close()
                },
                _onUpKeyed: function() {
                    this.moveCursor(-1)
                },
                _onDownKeyed: function() {
                    this.moveCursor(1)
                },
                _onLeftKeyed: function() {
                    "rtl" === this.dir && this.input.isCursorAtEnd() && this.autocomplete(this.menu.getTopSelectable())
                },
                _onRightKeyed: function() {
                    "ltr" === this.dir && this.input.isCursorAtEnd() && this.autocomplete(this.menu.getTopSelectable())
                },
                _onQueryChanged: function(t, e) {
                    this._minLengthMet(e) ? this.menu.update(e) : this.menu.empty()
                },
                _onWhitespaceChanged: function() {
                    this._updateHint()
                },
                _onLangDirChanged: function(t, e) {
                    this.dir !== e && (this.dir = e, this.menu.setLanguageDirection(e))
                },
                _openIfActive: function() {
                    this.isActive() && this.open()
                },
                _minLengthMet: function(t) {
                    return t = e.isString(t) ? t : this.input.getQuery() || "", t.length >= this.minLength
                },
                _updateHint: function() {
                    var t, i, n, o, r, a, l;
                    t = this.menu.getTopSelectable(), i = this.menu.getSelectableData(t), n = this.input.getInputValue(), !i || e.isBlankString(n) || this.input.hasOverflow() ? this.input.clearHint() : (o = s.normalizeQuery(n), r = e.escapeRegExChars(o), a = new RegExp("^(?:" + r + ")(.+$)", "i"), l = a.exec(i.val), l && this.input.setHint(n + l[1]))
                },
                isEnabled: function() {
                    return this.enabled
                },
                enable: function() {
                    this.enabled = !0
                },
                disable: function() {
                    this.enabled = !1
                },
                isActive: function() {
                    return this.active
                },
                activate: function() {
                    return !!this.isActive() || !(!this.isEnabled() || this.eventBus.before("active")) && (this.active = !0, this.eventBus.trigger("active"), !0)
                },
                deactivate: function() {
                    return !this.isActive() || !this.eventBus.before("idle") && (this.active = !1, this.close(), this.eventBus.trigger("idle"), !0)
                },
                isOpen: function() {
                    return this.menu.isOpen()
                },
                open: function() {
                    return this.isOpen() || this.eventBus.before("open") || (this.menu.open(), this._updateHint(), this.eventBus.trigger("open")), this.isOpen()
                },
                close: function() {
                    return this.isOpen() && !this.eventBus.before("close") && (this.menu.close(), this.input.clearHint(), this.input.resetInputValue(), this.eventBus.trigger("close")), !this.isOpen()
                },
                setVal: function(t) {
                    this.input.setQuery(e.toStr(t))
                },
                getVal: function() {
                    return this.input.getQuery()
                },
                select: function(t) {
                    var e = this.menu.getSelectableData(t);
                    return !(!e || this.eventBus.before("select", e.obj)) && (this.input.setQuery(e.val, !0), this.eventBus.trigger("select", e.obj), this.close(), !0)
                },
                autocomplete: function(t) {
                    var e, i, n;
                    return e = this.input.getQuery(), i = this.menu.getSelectableData(t), n = i && e !== i.val, !(!n || this.eventBus.before("autocomplete", i.obj)) && (this.input.setQuery(i.val), this.eventBus.trigger("autocomplete", i.obj), !0)
                },
                moveCursor: function(t) {
                    var e, i, n, o, r;
                    return e = this.input.getQuery(), i = this.menu.selectableRelativeToCursor(t), n = this.menu.getSelectableData(i), o = n ? n.obj : null, r = this._minLengthMet() && this.menu.update(e), !r && !this.eventBus.before("cursorchange", o) && (this.menu.setCursor(i), n ? this.input.setInputValue(n.val) : (this.input.resetInputValue(), this._updateHint()), this.eventBus.trigger("cursorchange", o), !0)
                },
                destroy: function() {
                    this.input.destroy(), this.menu.destroy()
                }
            }), i
        }();
    ! function() {
        "use strict";

        function o(e, i) {
            e.each(function() {
                var e, n = t(this);
                (e = n.data(m.typeahead)) && i(e, n)
            })
        }

        function r(t, e) {
            return t.clone().addClass(e.classes.hint).removeData().css(e.css.hint).css(d(t)).prop("readonly", !0).removeAttr("id name placeholder required").attr({
                autocomplete: "off",
                spellcheck: "false",
                tabindex: -1
            })
        }

        function a(t, e) {
            t.data(m.attrs, {
                dir: t.attr("dir"),
                autocomplete: t.attr("autocomplete"),
                spellcheck: t.attr("spellcheck"),
                style: t.attr("style")
            }), t.addClass(e.classes.input).attr({
                autocomplete: "off",
                spellcheck: !1
            });
            try {
                !t.attr("dir") && t.attr("dir", "auto")
            } catch (t) {}
            return t
        }

        function d(t) {
            return {
                backgroundAttachment: t.css("background-attachment"),
                backgroundClip: t.css("background-clip"),
                backgroundColor: t.css("background-color"),
                backgroundImage: t.css("background-image"),
                backgroundOrigin: t.css("background-origin"),
                backgroundPosition: t.css("background-position"),
                backgroundRepeat: t.css("background-repeat"),
                backgroundSize: t.css("background-size")
            }
        }

        function p(t) {
            var i, n;
            i = t.data(m.www), n = t.parent().filter(i.selectors.wrapper), e.each(t.data(m.attrs), function(i, n) {
                e.isUndefined(i) ? t.removeAttr(n) : t.attr(n, i)
            }), t.removeData(m.typeahead).removeData(m.www).removeData(m.attr).removeClass(i.classes.input), n.length && (t.detach().insertAfter(n), n.remove())
        }

        function f(i) {
            var n, o;
            return n = e.isJQuery(i) || e.isElement(i), o = n ? t(i).first() : [], o.length ? o : null
        }
        var h, m, v;
        h = t.fn.typeahead, m = {
            www: "tt-www",
            attrs: "tt-attrs",
            typeahead: "tt-typeahead"
        }, v = {
            initialize: function(o, d) {
                function p() {
                    var i, p, v, g, y, b, w, k, S, x, C;
                    e.each(d, function(t) {
                        t.highlight = !!o.highlight
                    }), i = t(this), p = t(h.html.wrapper), v = f(o.hint), g = f(o.menu), y = o.hint !== !1 && !v, b = o.menu !== !1 && !g, y && (v = r(i, h)), b && (g = t(h.html.menu).css(h.css.menu)), v && v.val(""), i = a(i, h), (y || b) && (p.css(h.css.wrapper), i.css(y ? h.css.input : h.css.inputWithNoHint), i.wrap(p).parent().prepend(y ? v : null).append(b ? g : null)), C = b ? c : l, w = new n({
                        el: i
                    }), k = new s({
                        hint: v,
                        input: i
                    }, h), S = new C({
                        node: g,
                        datasets: d
                    }, h), x = new u({
                        input: k,
                        menu: S,
                        eventBus: w,
                        minLength: o.minLength
                    }, h), i.data(m.www, h), i.data(m.typeahead, x)
                }
                var h;
                return d = e.isArray(d) ? d : [].slice.call(arguments, 1), o = o || {}, h = i(o.classNames), this.each(p)
            },
            isEnabled: function() {
                var t;
                return o(this.first(), function(e) {
                    t = e.isEnabled()
                }), t
            },
            enable: function() {
                return o(this, function(t) {
                    t.enable()
                }), this
            },
            disable: function() {
                return o(this, function(t) {
                    t.disable()
                }), this
            },
            isActive: function() {
                var t;
                return o(this.first(), function(e) {
                    t = e.isActive()
                }), t
            },
            activate: function() {
                return o(this, function(t) {
                    t.activate()
                }), this
            },
            deactivate: function() {
                return o(this, function(t) {
                    t.deactivate()
                }), this
            },
            isOpen: function() {
                var t;
                return o(this.first(), function(e) {
                    t = e.isOpen()
                }), t
            },
            open: function() {
                return o(this, function(t) {
                    t.open()
                }), this
            },
            close: function() {
                return o(this, function(t) {
                    t.close()
                }), this
            },
            select: function(e) {
                var i = !1,
                    n = t(e);
                return o(this.first(), function(t) {
                    i = t.select(n)
                }), i
            },
            autocomplete: function(e) {
                var i = !1,
                    n = t(e);
                return o(this.first(), function(t) {
                    i = t.autocomplete(n)
                }), i
            },
            moveCursor: function(t) {
                var e = !1;
                return o(this.first(), function(i) {
                    e = i.moveCursor(t)
                }), e
            },
            val: function(t) {
                var e;
                return arguments.length ? (o(this, function(e) {
                    e.setVal(t)
                }), this) : (o(this.first(), function(t) {
                    e = t.getVal()
                }), e)
            },
            destroy: function() {
                return o(this, function(t, e) {
                    p(e), t.destroy()
                }), this
            }
        }, t.fn.typeahead = function(t) {
            return v[t] ? v[t].apply(this, [].slice.call(arguments, 1)) : v.initialize.apply(this, arguments)
        }, t.fn.typeahead.noConflict = function() {
            return t.fn.typeahead = h, this
        }
    }()
}), ! function(t, e) {
    "object" == typeof exports && "undefined" != typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define(e) : t.validator = e()
}(this, function() {
    "use strict";

    function t(t) {
        if ("string" != typeof t) throw new TypeError("This library (validator.js) validates strings only")
    }

    function e(e) {
        return t(e), e = Date.parse(e), isNaN(e) ? null : new Date(e)
    }

    function i(e) {
        return t(e), parseFloat(e)
    }

    function n(e, i) {
        return t(e), parseInt(e, i || 10)
    }

    function o(e, i) {
        return t(e), i ? "1" === e || "true" === e : "0" !== e && "false" !== e && "" !== e
    }

    function r(e, i) {
        return t(e), e === i
    }

    function s(t) {
        return "object" === ("undefined" == typeof t ? "undefined" : ht(t)) && null !== t ? t = "function" == typeof t.toString ? t.toString() : "[object Object]" : (null === t || "undefined" == typeof t || isNaN(t) && !t.length) && (t = ""), String(t)
    }

    function a(e, i) {
        return t(e), e.indexOf(s(i)) >= 0
    }

    function l(e, i, n) {
        return t(e), "[object RegExp]" !== Object.prototype.toString.call(i) && (i = new RegExp(i, n)), i.test(e)
    }

    function c() {
        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
            e = arguments[1];
        for (var i in e) "undefined" == typeof t[i] && (t[i] = e[i]);
        return t
    }

    function u(e, i) {
        t(e);
        var n = void 0,
            o = void 0;
        "object" === ("undefined" == typeof i ? "undefined" : ht(i)) ? (n = i.min || 0, o = i.max) : (n = arguments[1], o = arguments[2]);
        var r = encodeURI(e).split(/%..|./).length - 1;
        return r >= n && ("undefined" == typeof o || r <= o)
    }

    function d(e, i) {
        t(e), i = c(i, mt), i.allow_trailing_dot && "." === e[e.length - 1] && (e = e.substring(0, e.length - 1));
        var n = e.split(".");
        if (i.require_tld) {
            var o = n.pop();
            if (!n.length || !/^([a-z\u00a1-\uffff]{2,}|xn[a-z0-9-]{2,})$/i.test(o)) return !1
        }
        for (var r, s = 0; s < n.length; s++) {
            if (r = n[s], i.allow_underscores && (r = r.replace(/_/g, "")), !/^[a-z\u00a1-\uffff0-9-]+$/i.test(r)) return !1;
            if (/[\uff01-\uff5e]/.test(r)) return !1;
            if ("-" === r[0] || "-" === r[r.length - 1]) return !1
        }
        return !0
    }

    function p(e, i) {
        if (t(e), i = c(i, vt), i.require_display_name || i.allow_display_name) {
            var n = e.match(gt);
            if (n) e = n[1];
            else if (i.require_display_name) return !1
        }
        var o = e.split("@"),
            r = o.pop(),
            s = o.join("@"),
            a = r.toLowerCase();
        if ("gmail.com" !== a && "googlemail.com" !== a || (s = s.replace(/\./g, "").toLowerCase()), !u(s, {
                max: 64
            }) || !u(r, {
                max: 256
            })) return !1;
        if (!d(r, {
                require_tld: i.require_tld
            })) return !1;
        if ('"' === s[0]) return s = s.slice(1, s.length - 1), i.allow_utf8_local_part ? kt.test(s) : bt.test(s);
        for (var l = i.allow_utf8_local_part ? wt : yt, p = s.split("."), f = 0; f < p.length; f++)
            if (!l.test(p[f])) return !1;
        return !0
    }

    function f(e) {
        var i = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "";
        if (t(e), i = String(i), !i) return f(e, 4) || f(e, 6);
        if ("4" === i) {
            if (!St.test(e)) return !1;
            var n = e.split(".").sort(function(t, e) {
                return t - e
            });
            return n[3] <= 255
        }
        if ("6" === i) {
            var o = e.split(":"),
                r = !1,
                s = f(o[o.length - 1], 4),
                a = s ? 7 : 8;
            if (o.length > a) return !1;
            if ("::" === e) return !0;
            "::" === e.substr(0, 2) ? (o.shift(), o.shift(), r = !0) : "::" === e.substr(e.length - 2) && (o.pop(), o.pop(), r = !0);
            for (var l = 0; l < o.length; ++l)
                if ("" === o[l] && l > 0 && l < o.length - 1) {
                    if (r) return !1;
                    r = !0
                } else if (s && l === o.length - 1);
                else if (!xt.test(o[l])) return !1;
            return r ? o.length >= 1 : o.length === a
        }
        return !1
    }

    function h(t) {
        return "[object RegExp]" === Object.prototype.toString.call(t)
    }

    function m(t, e) {
        for (var i = 0; i < e.length; i++) {
            var n = e[i];
            if (t === n || h(n) && n.test(t)) return !0
        }
        return !1
    }

    function v(e, i) {
        if (t(e), !e || e.length >= 2083 || /\s/.test(e)) return !1;
        if (0 === e.indexOf("mailto:")) return !1;
        i = c(i, Ct);
        var n = void 0,
            o = void 0,
            r = void 0,
            s = void 0,
            a = void 0,
            l = void 0,
            u = void 0,
            p = void 0;
        if (u = e.split("#"), e = u.shift(), u = e.split("?"), e = u.shift(), u = e.split("://"), u.length > 1) {
            if (n = u.shift(), i.require_valid_protocol && i.protocols.indexOf(n) === -1) return !1
        } else {
            if (i.require_protocol) return !1;
            i.allow_protocol_relative_urls && "//" === e.substr(0, 2) && (u[0] = e.substr(2))
        }
        if (e = u.join("://"), u = e.split("/"), e = u.shift(), "" === e && !i.require_host) return !0;
        if (u = e.split("@"), u.length > 1 && (o = u.shift(), o.indexOf(":") >= 0 && o.split(":").length > 2)) return !1;
        s = u.join("@"), l = p = null;
        var h = s.match(Tt);
        return h ? (r = "", p = h[1], l = h[2] || null) : (u = s.split(":"), r = u.shift(), u.length && (l = u.join(":"))), !(null !== l && (a = parseInt(l, 10), !/^[0-9]+$/.test(l) || a <= 0 || a > 65535) || !(f(r) || d(r, i) || p && f(p, 6) || "localhost" === r) || (r = r || p, i.host_whitelist && !m(r, i.host_whitelist) || i.host_blacklist && m(r, i.host_blacklist)))
    }

    function g(e) {
        return t(e), _t.test(e)
    }

    function y(e) {
        return t(e), ["true", "false", "1", "0"].indexOf(e) >= 0
    }

    function b(e) {
        var i = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "en-US";
        if (t(e), i in At) return At[i].test(e);
        throw new Error("Invalid locale '" + i + "'")
    }

    function w(e) {
        var i = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "en-US";
        if (t(e), i in Et) return Et[i].test(e);
        throw new Error("Invalid locale '" + i + "'")
    }

    function k(e) {
        return t(e), Mt.test(e)
    }

    function S(e) {
        return t(e), e === e.toLowerCase()
    }

    function x(e) {
        return t(e), e === e.toUpperCase()
    }

    function C(e) {
        return t(e), Ft.test(e)
    }

    function T(e) {
        return t(e), Dt.test(e)
    }

    function _(e) {
        return t(e), jt.test(e)
    }

    function A(e) {
        return t(e), Dt.test(e) && jt.test(e)
    }

    function E(e) {
        return t(e), Ht.test(e)
    }

    function $(e) {
        return t(e), Rt.test(e)
    }

    function P(e, i) {
        t(e), i = i || {};
        var n = i.hasOwnProperty("allow_leading_zeroes") && !i.allow_leading_zeroes ? Wt : zt,
            o = !i.hasOwnProperty("min") || e >= i.min,
            r = !i.hasOwnProperty("max") || e <= i.max,
            s = !i.hasOwnProperty("lt") || e < i.lt,
            a = !i.hasOwnProperty("gt") || e > i.gt;
        return n.test(e) && o && r && s && a
    }

    function O(e, i) {
        return t(e), i = i || {}, "" !== e && "." !== e && Bt.test(e) && (!i.hasOwnProperty("min") || e >= i.min) && (!i.hasOwnProperty("max") || e <= i.max) && (!i.hasOwnProperty("lt") || e < i.lt) && (!i.hasOwnProperty("gt") || e > i.gt)
    }

    function L(e) {
        return t(e), "" !== e && Nt.test(e)
    }

    function I(e) {
        return t(e), qt.test(e)
    }

    function M(e, n) {
        return t(e), i(e) % parseInt(n, 10) === 0
    }

    function F(e) {
        return t(e), Yt.test(e)
    }

    function D(e) {
        return t(e), Xt.test(e)
    }

    function j(e) {
        t(e);
        try {
            var i = JSON.parse(e);
            return !!i && "object" === ("undefined" == typeof i ? "undefined" : ht(i))
        } catch (t) {}
        return !1
    }

    function H(e) {
        return t(e), 0 === e.length
    }

    function R(e, i) {
        t(e);
        var n = void 0,
            o = void 0;
        "object" === ("undefined" == typeof i ? "undefined" : ht(i)) ? (n = i.min || 0, o = i.max) : (n = arguments[1], o = arguments[2]);
        var r = e.match(/[\uD800-\uDBFF][\uDC00-\uDFFF]/g) || [],
            s = e.length - r.length;
        return s >= n && ("undefined" == typeof o || s <= o)
    }

    function W(e) {
        var i = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "all";
        t(e);
        var n = Ut[i];
        return n && n.test(e)
    }

    function z(e) {
        return t(e), I(e) && 24 === e.length
    }

    function B(e) {
        return t(e), Kt.test(e)
    }

    function N(t) {
        var e = t.match(Kt),
            i = void 0,
            n = void 0,
            o = void 0,
            r = void 0;
        if (e) {
            if (i = e[21], !i) return e[12] ? null : 0;
            if ("z" === i || "Z" === i) return 0;
            n = e[22], i.indexOf(":") !== -1 ? (o = parseInt(e[23], 10), r = parseInt(e[24], 10)) : (o = 0, r = parseInt(e[23], 10))
        } else {
            if (t = t.toLowerCase(), i = t.match(/(?:\s|gmt\s*)(-|\+)(\d{1,4})(\s|$)/), !i) return t.indexOf("gmt") !== -1 ? 0 : null;
            n = i[1];
            var s = i[2];
            3 === s.length && (s = "0" + s), s.length <= 2 ? (o = 0, r = parseInt(s, 10)) : (o = parseInt(s.slice(0, 2), 10), r = parseInt(s.slice(2, 4), 10))
        }
        return (60 * o + r) * ("-" === n ? 1 : -1)
    }

    function q(e) {
        t(e);
        var i = new Date(Date.parse(e));
        if (isNaN(i)) return !1;
        var n = N(e);
        if (null !== n) {
            var o = i.getTimezoneOffset() - n;
            i = new Date(i.getTime() + 6e4 * o)
        }
        var r = String(i.getDate()),
            s = void 0,
            a = void 0,
            l = void 0;
        return !(a = e.match(/(^|[^:\d])[23]\d([^T:\d]|$)/g)) || (s = a.map(function(t) {
                return t.match(/\d+/g)[0]
            }).join("/"), l = String(i.getFullYear()).slice(-2), s === r || s === l || s === "" + r / l || s === "" + l / r)
    }

    function Y(i) {
        var n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : String(new Date);
        t(i);
        var o = e(n),
            r = e(i);
        return !!(r && o && r > o)
    }

    function X(i) {
        var n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : String(new Date);
        t(i);
        var o = e(n),
            r = e(i);
        return !!(r && o && r < o)
    }

    function U(e, i) {
        t(e);
        var n = void 0;
        if ("[object Array]" === Object.prototype.toString.call(i)) {
            var o = [];
            for (n in i)({}).hasOwnProperty.call(i, n) && (o[n] = s(i[n]));
            return o.indexOf(e) >= 0
        }
        return "object" === ("undefined" == typeof i ? "undefined" : ht(i)) ? i.hasOwnProperty(e) : !(!i || "function" != typeof i.indexOf) && i.indexOf(e) >= 0
    }

    function K(e) {
        t(e);
        var i = e.replace(/[^0-9]+/g, "");
        if (!Vt.test(i)) return !1;
        for (var n = 0, o = void 0, r = void 0, s = void 0, a = i.length - 1; a >= 0; a--) o = i.substring(a, a + 1), r = parseInt(o, 10), s ? (r *= 2, n += r >= 10 ? r % 10 + 1 : r) : n += r, s = !s;
        return !(n % 10 !== 0 || !i)
    }

    function V(e) {
        if (t(e), !Qt.test(e)) return !1;
        for (var i = e.replace(/[A-Z]/g, function(t) {
            return parseInt(t, 36)
        }), n = 0, o = void 0, r = void 0, s = !0, a = i.length - 2; a >= 0; a--) o = i.substring(a, a + 1), r = parseInt(o, 10), s ? (r *= 2, n += r >= 10 ? r + 1 : r) : n += r, s = !s;
        return parseInt(e.substr(e.length - 1), 10) === (1e4 - n) % 10
    }

    function Q(e) {
        var i = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "";
        if (t(e), i = String(i), !i) return Q(e, 10) || Q(e, 13);
        var n = e.replace(/[\s-]+/g, ""),
            o = 0,
            r = void 0;
        if ("10" === i) {
            if (!Gt.test(n)) return !1;
            for (r = 0; r < 9; r++) o += (r + 1) * n.charAt(r);
            if (o += "X" === n.charAt(9) ? 100 : 10 * n.charAt(9), o % 11 === 0) return !!n
        } else if ("13" === i) {
            if (!Zt.test(n)) return !1;
            for (r = 0; r < 12; r++) o += Jt[r % 2] * n.charAt(r);
            if (n.charAt(12) - (10 - o % 10) % 10 === 0) return !!n
        }
        return !1
    }

    function G(e) {
        var i = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
        t(e);
        var n = te;
        if (n = i.require_hyphen ? n.replace("?", "") : n, n = i.case_sensitive ? new RegExp(n) : new RegExp(n, "i"), !n.test(e)) return !1;
        var o = e.replace("-", ""),
            r = 8,
            s = 0,
            a = !0,
            l = !1,
            c = void 0;
        try {
            for (var u, d = o[Symbol.iterator](); !(a = (u = d.next()).done); a = !0) {
                var p = u.value,
                    f = "X" === p.toUpperCase() ? 10 : +p;
                s += f * r, --r
            }
        } catch (t) {
            l = !0, c = t
        } finally {
            try {
                !a && d.return && d.return()
            } finally {
                if (l) throw c
            }
        }
        return s % 11 === 0
    }

    function Z(e, i) {
        return t(e), i in ee && ee[i].test(e)
    }

    function J(t) {
        var e = "(\\" + t.symbol.replace(/\./g, "\\.") + ")" + (t.require_symbol ? "" : "?"),
            i = "-?",
            n = "[1-9]\\d*",
            o = "[1-9]\\d{0,2}(\\" + t.thousands_separator + "\\d{3})*",
            r = ["0", n, o],
            s = "(" + r.join("|") + ")?",
            a = "(\\" + t.decimal_separator + "\\d{2})?",
            l = s + a;
        return t.allow_negatives && !t.parens_for_negatives && (t.negative_sign_after_digits ? l += i : t.negative_sign_before_digits && (l = i + l)), t.allow_negative_sign_placeholder ? l = "( (?!\\-))?" + l : t.allow_space_after_symbol ? l = " ?" + l : t.allow_space_after_digits && (l += "( (?!$))?"), t.symbol_after_digits ? l += e : l = e + l, t.allow_negatives && (t.parens_for_negatives ? l = "(\\(" + l + "\\)|" + l + ")" : t.negative_sign_before_digits || t.negative_sign_after_digits || (l = i + l)), new RegExp("^(?!-? )(?=.*\\d)" + l + "$")
    }

    function tt(e, i) {
        return t(e), i = c(i, ie), J(i).test(e)
    }

    function et(e) {
        t(e);
        var i = e.length;
        if (!i || i % 4 !== 0 || ne.test(e)) return !1;
        var n = e.indexOf("=");
        return n === -1 || n === i - 1 || n === i - 2 && "=" === e[i - 1]
    }

    function it(e) {
        return t(e), oe.test(e)
    }

    function nt(e, i) {
        t(e);
        var n = i ? new RegExp("^[" + i + "]+", "g") : /^\s+/g;
        return e.replace(n, "")
    }

    function ot(e, i) {
        t(e);
        for (var n = i ? new RegExp("[" + i + "]") : /\s/, o = e.length - 1; o >= 0 && n.test(e[o]);) o--;
        return o < e.length ? e.substr(0, o + 1) : e
    }

    function rt(t, e) {
        return ot(nt(t, e), e)
    }

    function st(e) {
        return t(e), e.replace(/&/g, "&amp;").replace(/"/g, "&quot;").replace(/'/g, "&#x27;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/\//g, "&#x2F;").replace(/\\/g, "&#x5C;").replace(/`/g, "&#96;")
    }

    function at(e) {
        return t(e), e.replace(/&amp;/g, "&").replace(/&quot;/g, '"').replace(/&#x27;/g, "'").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&#x2F;/g, "/").replace(/&#96;/g, "`")
    }

    function lt(e, i) {
        return t(e), e.replace(new RegExp("[" + i + "]+", "g"), "")
    }

    function ct(e, i) {
        t(e);
        var n = i ? "\\x00-\\x09\\x0B\\x0C\\x0E-\\x1F\\x7F" : "\\x00-\\x1F\\x7F";
        return lt(e, n)
    }

    function ut(e, i) {
        return t(e), e.replace(new RegExp("[^" + i + "]+", "g"), "")
    }

    function dt(e, i) {
        t(e);
        for (var n = e.length - 1; n >= 0; n--)
            if (i.indexOf(e[n]) === -1) return !1;
        return !0
    }

    function pt(t, e) {
        if (e = c(e, re), !p(t)) return !1;
        var i = t.split("@"),
            n = i.pop(),
            o = i.join("@"),
            r = [o, n];
        if (r[1] = r[1].toLowerCase(), "gmail.com" === r[1] || "googlemail.com" === r[1]) {
            if (e.gmail_remove_subaddress && (r[0] = r[0].split("+")[0]), e.gmail_remove_dots && (r[0] = r[0].replace(/\./g, "")), !r[0].length) return !1;
            (e.all_lowercase || e.gmail_lowercase) && (r[0] = r[0].toLowerCase()), r[1] = e.gmail_convert_googlemaildotcom ? "gmail.com" : r[1]
        } else if (~se.indexOf(r[1])) {
            if (e.icloud_remove_subaddress && (r[0] = r[0].split("+")[0]), !r[0].length) return !1;
            (e.all_lowercase || e.icloud_lowercase) && (r[0] = r[0].toLowerCase())
        } else if (~ae.indexOf(r[1])) {
            if (e.outlookdotcom_remove_subaddress && (r[0] = r[0].split("+")[0]), !r[0].length) return !1;
            (e.all_lowercase || e.outlookdotcom_lowercase) && (r[0] = r[0].toLowerCase())
        } else if (~le.indexOf(r[1])) {
            if (e.yahoo_remove_subaddress) {
                var s = r[0].split("-");
                r[0] = s.length > 1 ? s.slice(0, -1).join("-") : s[0]
            }
            if (!r[0].length) return !1;
            (e.all_lowercase || e.yahoo_lowercase) && (r[0] = r[0].toLowerCase())
        } else e.all_lowercase && (r[0] = r[0].toLowerCase());
        return r.join("@")
    }
    for (var ft, ht = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) {
        return typeof t
    } : function(t) {
        return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
    }, mt = (function() {
        function t(t) {
            this.value = t
        }

        function e(e) {
            function i(t, e) {
                return new Promise(function(i, o) {
                    var a = {
                        key: t,
                        arg: e,
                        resolve: i,
                        reject: o,
                        next: null
                    };
                    s ? s = s.next = a : (r = s = a, n(t, e))
                })
            }

            function n(i, r) {
                try {
                    var s = e[i](r),
                        a = s.value;
                    a instanceof t ? Promise.resolve(a.value).then(function(t) {
                        n("next", t)
                    }, function(t) {
                        n("throw", t)
                    }) : o(s.done ? "return" : "normal", s.value)
                } catch (t) {
                    o("throw", t)
                }
            }

            function o(t, e) {
                switch (t) {
                    case "return":
                        r.resolve({
                            value: e,
                            done: !0
                        });
                        break;
                    case "throw":
                        r.reject(e);
                        break;
                    default:
                        r.resolve({
                            value: e,
                            done: !1
                        })
                }
                r = r.next, r ? n(r.key, r.arg) : s = null
            }
            var r, s;
            this._invoke = i, "function" != typeof e.return && (this.return = void 0)
        }
        return "function" == typeof Symbol && Symbol.asyncIterator && (e.prototype[Symbol.asyncIterator] = function() {
            return this
        }), e.prototype.next = function(t) {
            return this._invoke("next", t)
        }, e.prototype.throw = function(t) {
            return this._invoke("throw", t)
        }, e.prototype.return = function(t) {
            return this._invoke("return", t);
        }, {
            wrap: function(t) {
                return function() {
                    return new e(t.apply(this, arguments))
                }
            },
            await: function(e) {
                return new t(e)
            }
        }
    }(), {
        require_tld: !0,
        allow_underscores: !1,
        allow_trailing_dot: !1
    }), vt = {
        allow_display_name: !1,
        require_display_name: !1,
        allow_utf8_local_part: !0,
        require_tld: !0
    }, gt = /^[a-z\d!#\$%&'\*\+\-\/=\?\^_`{\|}~\.\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+[a-z\d!#\$%&'\*\+\-\/=\?\^_`{\|}~\.\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF\s]*<(.+)>$/i, yt = /^[a-z\d!#\$%&'\*\+\-\/=\?\^_`{\|}~]+$/i, bt = /^([\s\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e]|(\\[\x01-\x09\x0b\x0c\x0d-\x7f]))*$/i, wt = /^[a-z\d!#\$%&'\*\+\-\/=\?\^_`{\|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+$/i, kt = /^([\s\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|(\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*$/i, St = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/, xt = /^[0-9A-F]{1,4}$/i, Ct = {
        protocols: ["http", "https", "ftp"],
        require_tld: !0,
        require_protocol: !1,
        require_host: !0,
        require_valid_protocol: !0,
        allow_underscores: !1,
        allow_trailing_dot: !1,
        allow_protocol_relative_urls: !1
    }, Tt = /^\[([^\]]+)\](?::([0-9]+))?$/, _t = /^([0-9a-fA-F][0-9a-fA-F]:){5}([0-9a-fA-F][0-9a-fA-F])$/, At = {
        "en-US": /^[A-Z]+$/i,
        "cs-CZ": /^[A-ZÁČĎÉĚÍŇÓŘŠŤÚŮÝŽ]+$/i,
        "da-DK": /^[A-ZÆØÅ]+$/i,
        "de-DE": /^[A-ZÄÖÜß]+$/i,
        "es-ES": /^[A-ZÁÉÍÑÓÚÜ]+$/i,
        "fr-FR": /^[A-ZÀÂÆÇÉÈÊËÏÎÔŒÙÛÜŸ]+$/i,
        "nl-NL": /^[A-ZÉËÏÓÖÜ]+$/i,
        "hu-HU": /^[A-ZÁÉÍÓÖŐÚÜŰ]+$/i,
        "pl-PL": /^[A-ZĄĆĘŚŁŃÓŻŹ]+$/i,
        "pt-PT": /^[A-ZÃÁÀÂÇÉÊÍÕÓÔÚÜ]+$/i,
        "ru-RU": /^[А-ЯЁ]+$/i,
        "sr-RS@latin": /^[A-ZČĆŽŠĐ]+$/i,
        "sr-RS": /^[А-ЯЂЈЉЊЋЏ]+$/i,
        "tr-TR": /^[A-ZÇĞİıÖŞÜ]+$/i,
        "uk-UA": /^[А-ЯЄIЇҐ]+$/i,
        ar: /^[ءآأؤإئابةتثجحخدذرزسشصضطظعغفقكلمنهوىيًٌٍَُِّْٰ]+$/
    }, Et = {
        "en-US": /^[0-9A-Z]+$/i,
        "cs-CZ": /^[0-9A-ZÁČĎÉĚÍŇÓŘŠŤÚŮÝŽ]+$/i,
        "da-DK": /^[0-9A-ZÆØÅ]$/i,
        "de-DE": /^[0-9A-ZÄÖÜß]+$/i,
        "es-ES": /^[0-9A-ZÁÉÍÑÓÚÜ]+$/i,
        "fr-FR": /^[0-9A-ZÀÂÆÇÉÈÊËÏÎÔŒÙÛÜŸ]+$/i,
        "hu-HU": /^[0-9A-ZÁÉÍÓÖŐÚÜŰ]+$/i,
        "nl-NL": /^[0-9A-ZÉËÏÓÖÜ]+$/i,
        "pl-PL": /^[0-9A-ZĄĆĘŚŁŃÓŻŹ]+$/i,
        "pt-PT": /^[0-9A-ZÃÁÀÂÇÉÊÍÕÓÔÚÜ]+$/i,
        "ru-RU": /^[0-9А-ЯЁ]+$/i,
        "sr-RS@latin": /^[0-9A-ZČĆŽŠĐ]+$/i,
        "sr-RS": /^[0-9А-ЯЂЈЉЊЋЏ]+$/i,
        "tr-TR": /^[0-9A-ZÇĞİıÖŞÜ]+$/i,
        "uk-UA": /^[0-9А-ЯЄIЇҐ]+$/i,
        ar: /^[٠١٢٣٤٥٦٧٨٩0-9ءآأؤإئابةتثجحخدذرزسشصضطظعغفقكلمنهوىيًٌٍَُِّْٰ]+$/
    }, $t = ["AU", "GB", "HK", "IN", "NZ", "ZA", "ZM"], Pt = 0; Pt < $t.length; Pt++) ft = "en-" + $t[Pt], At[ft] = At["en-US"], Et[ft] = Et["en-US"];
    At["pt-BR"] = At["pt-PT"], Et["pt-BR"] = Et["pt-PT"];
    for (var Ot, Lt = ["AE", "BH", "DZ", "EG", "IQ", "JO", "KW", "LB", "LY", "MA", "QM", "QA", "SA", "SD", "SY", "TN", "YE"], It = 0; It < Lt.length; It++) Ot = "ar-" + Lt[It], At[Ot] = At.ar, Et[Ot] = Et.ar;
    var Mt = /^[-+]?[0-9]+$/,
        Ft = /^[\x00-\x7F]+$/,
        Dt = /[^\u0020-\u007E\uFF61-\uFF9F\uFFA0-\uFFDC\uFFE8-\uFFEE0-9a-zA-Z]/,
        jt = /[\u0020-\u007E\uFF61-\uFF9F\uFFA0-\uFFDC\uFFE8-\uFFEE0-9a-zA-Z]/,
        Ht = /[^\x00-\x7F]/,
        Rt = /[\uD800-\uDBFF][\uDC00-\uDFFF]/,
        Wt = /^(?:[-+]?(?:0|[1-9][0-9]*))$/,
        zt = /^[-+]?[0-9]+$/,
        Bt = /^(?:[-+]?(?:[0-9]+))?(?:\.[0-9]*)?(?:[eE][\+\-]?(?:[0-9]+))?$/,
        Nt = /^[-+]?([0-9]+|\.[0-9]+|[0-9]+\.[0-9]+)$/,
        qt = /^[0-9A-F]+$/i,
        Yt = /^#?([0-9A-F]{3}|[0-9A-F]{6})$/i,
        Xt = /^[a-f0-9]{32}$/,
        Ut = {
            3: /^[0-9A-F]{8}-[0-9A-F]{4}-3[0-9A-F]{3}-[0-9A-F]{4}-[0-9A-F]{12}$/i,
            4: /^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i,
            5: /^[0-9A-F]{8}-[0-9A-F]{4}-5[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i,
            all: /^[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}$/i
        },
        Kt = /^([\+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T\s]((([01]\d|2[0-3])((:?)[0-5]\d)?|24:?00)([\.,]\d+(?!:))?)?(\17[0-5]\d([\.,]\d+)?)?([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$/,
        Vt = /^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|(222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})|62[0-9]{14}$/,
        Qt = /^[A-Z]{2}[0-9A-Z]{9}[0-9]$/,
        Gt = /^(?:[0-9]{9}X|[0-9]{10})$/,
        Zt = /^(?:[0-9]{13})$/,
        Jt = [1, 3],
        te = "^\\d{4}-?\\d{3}[\\dX]$",
        ee = {
            "ar-DZ": /^(\+?213|0)(5|6|7)\d{8}$/,
            "ar-SY": /^(!?(\+?963)|0)?9\d{8}$/,
            "ar-SA": /^(!?(\+?966)|0)?5\d{8}$/,
            "en-US": /^(\+?1)?[2-9]\d{2}[2-9](?!11)\d{6}$/,
            "cs-CZ": /^(\+?420)? ?[1-9][0-9]{2} ?[0-9]{3} ?[0-9]{3}$/,
            "de-DE": /^(\+?49[ \.\-])?([\(]{1}[0-9]{1,6}[\)])?([0-9 \.\-\/]{3,20})((x|ext|extension)[ ]?[0-9]{1,4})?$/,
            "da-DK": /^(\+?45)?(\d{8})$/,
            "el-GR": /^(\+?30)?(69\d{8})$/,
            "en-AU": /^(\+?61|0)4\d{8}$/,
            "en-GB": /^(\+?44|0)7\d{9}$/,
            "en-HK": /^(\+?852\-?)?[569]\d{3}\-?\d{4}$/,
            "en-IN": /^(\+?91|0)?[789]\d{9}$/,
            "en-NZ": /^(\+?64|0)2\d{7,9}$/,
            "en-ZA": /^(\+?27|0)\d{9}$/,
            "en-ZM": /^(\+?26)?09[567]\d{7}$/,
            "es-ES": /^(\+?34)?(6\d{1}|7[1234])\d{7}$/,
            "fi-FI": /^(\+?358|0)\s?(4(0|1|2|4|5)?|50)\s?(\d\s?){4,8}\d$/,
            "fr-FR": /^(\+?33|0)[67]\d{8}$/,
            "hu-HU": /^(\+?36)(20|30|70)\d{7}$/,
            "it-IT": /^(\+?39)?\s?3\d{2} ?\d{6,7}$/,
            "ja-JP": /^(\+?81|0)\d{1,4}[ \-]?\d{1,4}[ \-]?\d{4}$/,
            "ms-MY": /^(\+?6?01){1}(([145]{1}(\-|\s)?\d{7,8})|([236789]{1}(\s|\-)?\d{7}))$/,
            "nb-NO": /^(\+?47)?[49]\d{7}$/,
            "nl-BE": /^(\+?32|0)4?\d{8}$/,
            "nn-NO": /^(\+?47)?[49]\d{7}$/,
            "pl-PL": /^(\+?48)? ?[5-8]\d ?\d{3} ?\d{2} ?\d{2}$/,
            "pt-BR": /^(\+?55|0)\-?[1-9]{2}\-?[2-9]{1}\d{3,4}\-?\d{4}$/,
            "pt-PT": /^(\+?351)?9[1236]\d{7}$/,
            "ru-RU": /^(\+?7|8)?9\d{9}$/,
            "sr-RS": /^(\+3816|06)[- \d]{5,9}$/,
            "tr-TR": /^(\+?90|0)?5\d{9}$/,
            "vi-VN": /^(\+?84|0)?((1(2([0-9])|6([2-9])|88|99))|(9((?!5)[0-9])))([0-9]{7})$/,
            "zh-CN": /^(\+?0?86\-?)?1[345789]\d{9}$/,
            "zh-TW": /^(\+?886\-?|0)?9\d{8}$/
        };
    ee["en-CA"] = ee["en-US"], ee["fr-BE"] = ee["nl-BE"];
    var ie = {
            symbol: "$",
            require_symbol: !1,
            allow_space_after_symbol: !1,
            symbol_after_digits: !1,
            allow_negatives: !0,
            parens_for_negatives: !1,
            negative_sign_before_digits: !1,
            negative_sign_after_digits: !1,
            allow_negative_sign_placeholder: !1,
            thousands_separator: ",",
            decimal_separator: ".",
            allow_space_after_digits: !1
        },
        ne = /[^A-Z0-9+\/=]/i,
        oe = /^\s*data:([a-z]+\/[a-z0-9\-\+]+(;[a-z\-]+=[a-z0-9\-]+)?)?(;base64)?,[a-z0-9!\$&',\(\)\*\+,;=\-\._~:@\/\?%\s]*\s*$/i,
        re = {
            all_lowercase: !0,
            gmail_lowercase: !0,
            gmail_remove_dots: !0,
            gmail_remove_subaddress: !0,
            gmail_convert_googlemaildotcom: !0,
            outlookdotcom_lowercase: !0,
            outlookdotcom_remove_subaddress: !0,
            yahoo_lowercase: !0,
            yahoo_remove_subaddress: !0,
            icloud_lowercase: !0,
            icloud_remove_subaddress: !0
        },
        se = ["icloud.com", "me.com"],
        ae = ["hotmail.at", "hotmail.be", "hotmail.ca", "hotmail.cl", "hotmail.co.il", "hotmail.co.nz", "hotmail.co.th", "hotmail.co.uk", "hotmail.com", "hotmail.com.ar", "hotmail.com.au", "hotmail.com.br", "hotmail.com.gr", "hotmail.com.mx", "hotmail.com.pe", "hotmail.com.tr", "hotmail.com.vn", "hotmail.cz", "hotmail.de", "hotmail.dk", "hotmail.es", "hotmail.fr", "hotmail.hu", "hotmail.id", "hotmail.ie", "hotmail.in", "hotmail.it", "hotmail.jp", "hotmail.kr", "hotmail.lv", "hotmail.my", "hotmail.ph", "hotmail.pt", "hotmail.sa", "hotmail.sg", "hotmail.sk", "live.be", "live.co.uk", "live.com", "live.com.ar", "live.com.mx", "live.de", "live.es", "live.eu", "live.fr", "live.it", "live.nl", "msn.com", "outlook.at", "outlook.be", "outlook.cl", "outlook.co.il", "outlook.co.nz", "outlook.co.th", "outlook.com", "outlook.com.ar", "outlook.com.au", "outlook.com.br", "outlook.com.gr", "outlook.com.pe", "outlook.com.tr", "outlook.com.vn", "outlook.cz", "outlook.de", "outlook.dk", "outlook.es", "outlook.fr", "outlook.hu", "outlook.id", "outlook.ie", "outlook.in", "outlook.it", "outlook.jp", "outlook.kr", "outlook.lv", "outlook.my", "outlook.ph", "outlook.pt", "outlook.sa", "outlook.sg", "outlook.sk", "passport.com"],
        le = ["rocketmail.com", "yahoo.ca", "yahoo.co.uk", "yahoo.com", "yahoo.de", "yahoo.fr", "yahoo.in", "yahoo.it", "ymail.com"],
        ce = "6.2.0",
        ue = {
            version: ce,
            toDate: e,
            toFloat: i,
            toInt: n,
            toBoolean: o,
            equals: r,
            contains: a,
            matches: l,
            isEmail: p,
            isURL: v,
            isMACAddress: g,
            isIP: f,
            isFQDN: d,
            isBoolean: y,
            isAlpha: b,
            isAlphanumeric: w,
            isNumeric: k,
            isLowercase: S,
            isUppercase: x,
            isAscii: C,
            isFullWidth: T,
            isHalfWidth: _,
            isVariableWidth: A,
            isMultibyte: E,
            isSurrogatePair: $,
            isInt: P,
            isFloat: O,
            isDecimal: L,
            isHexadecimal: I,
            isDivisibleBy: M,
            isHexColor: F,
            isMD5: D,
            isJSON: j,
            isEmpty: H,
            isLength: R,
            isByteLength: u,
            isUUID: W,
            isMongoId: z,
            isDate: q,
            isAfter: Y,
            isBefore: X,
            isIn: U,
            isCreditCard: K,
            isISIN: V,
            isISBN: Q,
            isISSN: G,
            isMobilePhone: Z,
            isCurrency: tt,
            isISO8601: B,
            isBase64: et,
            isDataURI: it,
            ltrim: nt,
            rtrim: ot,
            trim: rt,
            escape: st,
            unescape: at,
            stripLow: ct,
            whitelist: ut,
            blacklist: lt,
            isWhitelisted: dt,
            normalizeEmail: pt,
            toString: s
        };
    return ue
}),
    function() {
        "use strict";

        function t(t) {
            return t.split("").reverse().join("")
        }

        function e(t, e) {
            return t.substring(0, e.length) === e
        }

        function i(t, e) {
            return t.slice(-1 * e.length) === e
        }

        function n(t, e, i) {
            if ((t[e] || t[i]) && t[e] === t[i]) throw new Error(e)
        }

        function o(t) {
            return "number" == typeof t && isFinite(t)
        }

        function r(t, e) {
            var i = Math.pow(10, e);
            return (Math.round(t * i) / i).toFixed(e)
        }

        function s(e, i, n, s, a, l, c, u, d, p, f, h) {
            var m, v, g, y = h,
                b = "",
                w = "";
            return l && (h = l(h)), !!o(h) && (e !== !1 && 0 === parseFloat(h.toFixed(e)) && (h = 0), h < 0 && (m = !0, h = Math.abs(h)), e !== !1 && (h = r(h, e)), h = h.toString(), h.indexOf(".") !== -1 ? (v = h.split("."), g = v[0], n && (b = n + v[1])) : g = h, i && (g = t(g).match(/.{1,3}/g), g = t(g.join(t(i)))), m && u && (w += u), s && (w += s), m && d && (w += d), w += g, w += b, a && (w += a), p && (w = p(w, y)), w)
        }

        function a(t, n, r, s, a, l, c, u, d, p, f, h) {
            var m, v = "";
            return f && (h = f(h)), !(!h || "string" != typeof h) && (u && e(h, u) && (h = h.replace(u, ""), m = !0), s && e(h, s) && (h = h.replace(s, "")), d && e(h, d) && (h = h.replace(d, ""), m = !0), a && i(h, a) && (h = h.slice(0, -1 * a.length)), n && (h = h.split(n).join("")), r && (h = h.replace(r, ".")), m && (v += "-"), v += h, v = v.replace(/[^0-9\.\-.]/g, ""), "" !== v && (v = Number(v), c && (v = c(v)), !!o(v) && v))
        }

        function l(t) {
            var e, i, o, r = {};
            for (e = 0; e < d.length; e += 1)
                if (i = d[e], o = t[i], void 0 === o) "negative" !== i || r.negativeBefore ? "mark" === i && "." !== r.thousand ? r[i] = "." : r[i] = !1 : r[i] = "-";
                else if ("decimals" === i) {
                    if (!(o >= 0 && o < 8)) throw new Error(i);
                    r[i] = o
                } else if ("encoder" === i || "decoder" === i || "edit" === i || "undo" === i) {
                    if ("function" != typeof o) throw new Error(i);
                    r[i] = o
                } else {
                    if ("string" != typeof o) throw new Error(i);
                    r[i] = o
                }
            return n(r, "mark", "thousand"), n(r, "prefix", "negative"), n(r, "prefix", "negativeBefore"), r
        }

        function c(t, e, i) {
            var n, o = [];
            for (n = 0; n < d.length; n += 1) o.push(t[d[n]]);
            return o.push(i), e.apply("", o)
        }

        function u(t) {
            return this instanceof u ? void("object" == typeof t && (t = l(t), this.to = function(e) {
                return c(t, s, e)
            }, this.from = function(e) {
                return c(t, a, e)
            })) : new u(t)
        }
        var d = ["decimals", "thousand", "mark", "prefix", "postfix", "encoder", "decoder", "negativeBefore", "negative", "edit", "undo"];
        window.wNumb = u
    }(),
    function(t, e) {
        "function" == typeof define && define.amd ? define([], e) : "object" == typeof module && module.exports ? module.exports = e() : t.YamapsService = e()
    }(this, function() {
        "use strict";

        function t() {
            this.queue = [], this.apiLoading = !1, this.apiReady = !1, this.apiParams = null
        }
        return t.prototype.run = function(t) {
            this.apiReady ? t() : (this.queue.push(t), this.apiLoading || this.loadApi())
        }, t.prototype.loadApi = function() {
            if (!this.apiReady) {
                this.apiLoading = !0;
                var t = document.createElement("script"),
                    e = "https://api-maps.yandex.ru/2.1/?onload=YamapsService._apiLoaded";
                if (this.apiParams)
                    for (var i in this.apiParams) "onload" !== i && (e += "&" + i + "=" + this.apiParams[i]);
                t.src = e;
                var n = document.getElementsByTagName("script")[0];
                n.parentNode.insertBefore(t, n)
            }
        }, t.prototype._apiLoaded = function() {
            this.apiReady = !0, this.apiLoading = !1, this.queue && this.queue.length && (this.queue.forEach(function(t) {
                t()
            }), this.queue = null)
        }, t.prototype.fixScroll = function(t, e) {
            function i(t) {
                t.behaviors.disable(["scrollZoom", "multiTouch", "drag"])
            }

            function n(t) {
                t.behaviors.enable(["scrollZoom", "multiTouch", "drag"])
            }
            if (!t) return !1;
            e = e || {};
            var o;
            t.events.add("click", function() {
                n(t)
            }), t.events.add("multitouchstart", function() {
                n(t)
            }), t.events.add("mouseenter", function() {
                o = setTimeout(function() {
                    n(t)
                }, e.hoverDelay || 1e3)
            }), t.events.add("mouseleave", function() {
                clearTimeout(o), i(t)
            }), i(t)
        }, t.prototype.setApiParams = function(t) {
            this.apiParams = t
        }, new t
    });
    
    
    /*!
 * typeahead.js 0.11.1
 * https://github.com/twitter/typeahead.js
 * Copyright 2013-2015 Twitter, Inc. and other contributors; Licensed MIT
 */

!function(a,b){"function"==typeof define&&define.amd?define("bloodhound",["jquery"],function(c){return a.Bloodhound=b(c)}):"object"==typeof exports?module.exports=b(require("jquery")):a.Bloodhound=b(jQuery)}(this,function(a){var b=function(){"use strict";return{isMsie:function(){return/(msie|trident)/i.test(navigator.userAgent)?navigator.userAgent.match(/(msie |rv:)(\d+(.\d+)?)/i)[2]:!1},isBlankString:function(a){return!a||/^\s*$/.test(a)},escapeRegExChars:function(a){return a.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g,"\\$&")},isString:function(a){return"string"==typeof a},isNumber:function(a){return"number"==typeof a},isArray:a.isArray,isFunction:a.isFunction,isObject:a.isPlainObject,isUndefined:function(a){return"undefined"==typeof a},isElement:function(a){return!(!a||1!==a.nodeType)},isJQuery:function(b){return b instanceof a},toStr:function(a){return b.isUndefined(a)||null===a?"":a+""},bind:a.proxy,each:function(b,c){function d(a,b){return c(b,a)}a.each(b,d)},map:a.map,filter:a.grep,every:function(b,c){var d=!0;return b?(a.each(b,function(a,e){return(d=c.call(null,e,a,b))?void 0:!1}),!!d):d},some:function(b,c){var d=!1;return b?(a.each(b,function(a,e){return(d=c.call(null,e,a,b))?!1:void 0}),!!d):d},mixin:a.extend,identity:function(a){return a},clone:function(b){return a.extend(!0,{},b)},getIdGenerator:function(){var a=0;return function(){return a++}},templatify:function(b){function c(){return String(b)}return a.isFunction(b)?b:c},defer:function(a){setTimeout(a,0)},debounce:function(a,b,c){var d,e;return function(){var f,g,h=this,i=arguments;return f=function(){d=null,c||(e=a.apply(h,i))},g=c&&!d,clearTimeout(d),d=setTimeout(f,b),g&&(e=a.apply(h,i)),e}},throttle:function(a,b){var c,d,e,f,g,h;return g=0,h=function(){g=new Date,e=null,f=a.apply(c,d)},function(){var i=new Date,j=b-(i-g);return c=this,d=arguments,0>=j?(clearTimeout(e),e=null,g=i,f=a.apply(c,d)):e||(e=setTimeout(h,j)),f}},stringify:function(a){return b.isString(a)?a:JSON.stringify(a)},noop:function(){}}}(),c="0.11.1",d=function(){"use strict";function a(a){return a=b.toStr(a),a?a.split(/\s+/):[]}function c(a){return a=b.toStr(a),a?a.split(/\W+/):[]}function d(a){return function(c){return c=b.isArray(c)?c:[].slice.call(arguments,0),function(d){var e=[];return b.each(c,function(c){e=e.concat(a(b.toStr(d[c])))}),e}}}return{nonword:c,whitespace:a,obj:{nonword:d(c),whitespace:d(a)}}}(),e=function(){"use strict";function c(c){this.maxSize=b.isNumber(c)?c:100,this.reset(),this.maxSize<=0&&(this.set=this.get=a.noop)}function d(){this.head=this.tail=null}function e(a,b){this.key=a,this.val=b,this.prev=this.next=null}return b.mixin(c.prototype,{set:function(a,b){var c,d=this.list.tail;this.size>=this.maxSize&&(this.list.remove(d),delete this.hash[d.key],this.size--),(c=this.hash[a])?(c.val=b,this.list.moveToFront(c)):(c=new e(a,b),this.list.add(c),this.hash[a]=c,this.size++)},get:function(a){var b=this.hash[a];return b?(this.list.moveToFront(b),b.val):void 0},reset:function(){this.size=0,this.hash={},this.list=new d}}),b.mixin(d.prototype,{add:function(a){this.head&&(a.next=this.head,this.head.prev=a),this.head=a,this.tail=this.tail||a},remove:function(a){a.prev?a.prev.next=a.next:this.head=a.next,a.next?a.next.prev=a.prev:this.tail=a.prev},moveToFront:function(a){this.remove(a),this.add(a)}}),c}(),f=function(){"use strict";function c(a,c){this.prefix=["__",a,"__"].join(""),this.ttlKey="__ttl__",this.keyMatcher=new RegExp("^"+b.escapeRegExChars(this.prefix)),this.ls=c||h,!this.ls&&this._noop()}function d(){return(new Date).getTime()}function e(a){return JSON.stringify(b.isUndefined(a)?null:a)}function f(b){return a.parseJSON(b)}function g(a){var b,c,d=[],e=h.length;for(b=0;e>b;b++)(c=h.key(b)).match(a)&&d.push(c.replace(a,""));return d}var h;try{h=window.localStorage,h.setItem("~~~","!"),h.removeItem("~~~")}catch(i){h=null}return b.mixin(c.prototype,{_prefix:function(a){return this.prefix+a},_ttlKey:function(a){return this._prefix(a)+this.ttlKey},_noop:function(){this.get=this.set=this.remove=this.clear=this.isExpired=b.noop},_safeSet:function(a,b){try{this.ls.setItem(a,b)}catch(c){"QuotaExceededError"===c.name&&(this.clear(),this._noop())}},get:function(a){return this.isExpired(a)&&this.remove(a),f(this.ls.getItem(this._prefix(a)))},set:function(a,c,f){return b.isNumber(f)?this._safeSet(this._ttlKey(a),e(d()+f)):this.ls.removeItem(this._ttlKey(a)),this._safeSet(this._prefix(a),e(c))},remove:function(a){return this.ls.removeItem(this._ttlKey(a)),this.ls.removeItem(this._prefix(a)),this},clear:function(){var a,b=g(this.keyMatcher);for(a=b.length;a--;)this.remove(b[a]);return this},isExpired:function(a){var c=f(this.ls.getItem(this._ttlKey(a)));return b.isNumber(c)&&d()>c?!0:!1}}),c}(),g=function(){"use strict";function c(a){a=a||{},this.cancelled=!1,this.lastReq=null,this._send=a.transport,this._get=a.limiter?a.limiter(this._get):this._get,this._cache=a.cache===!1?new e(0):h}var d=0,f={},g=6,h=new e(10);return c.setMaxPendingRequests=function(a){g=a},c.resetCache=function(){h.reset()},b.mixin(c.prototype,{_fingerprint:function(b){return b=b||{},b.url+b.type+a.param(b.data||{})},_get:function(a,b){function c(a){b(null,a),k._cache.set(i,a)}function e(){b(!0)}function h(){d--,delete f[i],k.onDeckRequestArgs&&(k._get.apply(k,k.onDeckRequestArgs),k.onDeckRequestArgs=null)}var i,j,k=this;i=this._fingerprint(a),this.cancelled||i!==this.lastReq||((j=f[i])?j.done(c).fail(e):g>d?(d++,f[i]=this._send(a).done(c).fail(e).always(h)):this.onDeckRequestArgs=[].slice.call(arguments,0))},get:function(c,d){var e,f;d=d||a.noop,c=b.isString(c)?{url:c}:c||{},f=this._fingerprint(c),this.cancelled=!1,this.lastReq=f,(e=this._cache.get(f))?d(null,e):this._get(c,d)},cancel:function(){this.cancelled=!0}}),c}(),h=window.SearchIndex=function(){"use strict";function c(c){c=c||{},c.datumTokenizer&&c.queryTokenizer||a.error("datumTokenizer and queryTokenizer are both required"),this.identify=c.identify||b.stringify,this.datumTokenizer=c.datumTokenizer,this.queryTokenizer=c.queryTokenizer,this.reset()}function d(a){return a=b.filter(a,function(a){return!!a}),a=b.map(a,function(a){return a.toLowerCase()})}function e(){var a={};return a[i]=[],a[h]={},a}function f(a){for(var b={},c=[],d=0,e=a.length;e>d;d++)b[a[d]]||(b[a[d]]=!0,c.push(a[d]));return c}function g(a,b){var c=0,d=0,e=[];a=a.sort(),b=b.sort();for(var f=a.length,g=b.length;f>c&&g>d;)a[c]<b[d]?c++:a[c]>b[d]?d++:(e.push(a[c]),c++,d++);return e}var h="c",i="i";return b.mixin(c.prototype,{bootstrap:function(a){this.datums=a.datums,this.trie=a.trie},add:function(a){var c=this;a=b.isArray(a)?a:[a],b.each(a,function(a){var f,g;c.datums[f=c.identify(a)]=a,g=d(c.datumTokenizer(a)),b.each(g,function(a){var b,d,g;for(b=c.trie,d=a.split("");g=d.shift();)b=b[h][g]||(b[h][g]=e()),b[i].push(f)})})},get:function(a){var c=this;return b.map(a,function(a){return c.datums[a]})},search:function(a){var c,e,j=this;return c=d(this.queryTokenizer(a)),b.each(c,function(a){var b,c,d,f;if(e&&0===e.length)return!1;for(b=j.trie,c=a.split("");b&&(d=c.shift());)b=b[h][d];return b&&0===c.length?(f=b[i].slice(0),void(e=e?g(e,f):f)):(e=[],!1)}),e?b.map(f(e),function(a){return j.datums[a]}):[]},all:function(){var a=[];for(var b in this.datums)a.push(this.datums[b]);return a},reset:function(){this.datums={},this.trie=e()},serialize:function(){return{datums:this.datums,trie:this.trie}}}),c}(),i=function(){"use strict";function a(a){this.url=a.url,this.ttl=a.ttl,this.cache=a.cache,this.prepare=a.prepare,this.transform=a.transform,this.transport=a.transport,this.thumbprint=a.thumbprint,this.storage=new f(a.cacheKey)}var c;return c={data:"data",protocol:"protocol",thumbprint:"thumbprint"},b.mixin(a.prototype,{_settings:function(){return{url:this.url,type:"GET",dataType:"json"}},store:function(a){this.cache&&(this.storage.set(c.data,a,this.ttl),this.storage.set(c.protocol,location.protocol,this.ttl),this.storage.set(c.thumbprint,this.thumbprint,this.ttl))},fromCache:function(){var a,b={};return this.cache?(b.data=this.storage.get(c.data),b.protocol=this.storage.get(c.protocol),b.thumbprint=this.storage.get(c.thumbprint),a=b.thumbprint!==this.thumbprint||b.protocol!==location.protocol,b.data&&!a?b.data:null):null},fromNetwork:function(a){function b(){a(!0)}function c(b){a(null,e.transform(b))}var d,e=this;a&&(d=this.prepare(this._settings()),this.transport(d).fail(b).done(c))},clear:function(){return this.storage.clear(),this}}),a}(),j=function(){"use strict";function a(a){this.url=a.url,this.prepare=a.prepare,this.transform=a.transform,this.transport=new g({cache:a.cache,limiter:a.limiter,transport:a.transport})}return b.mixin(a.prototype,{_settings:function(){return{url:this.url,type:"GET",dataType:"json"}},get:function(a,b){function c(a,c){b(a?[]:e.transform(c))}var d,e=this;if(b)return a=a||"",d=this.prepare(a,this._settings()),this.transport.get(d,c)},cancelLastRequest:function(){this.transport.cancel()}}),a}(),k=function(){"use strict";function d(d){var e;return d?(e={url:null,ttl:864e5,cache:!0,cacheKey:null,thumbprint:"",prepare:b.identity,transform:b.identity,transport:null},d=b.isString(d)?{url:d}:d,d=b.mixin(e,d),!d.url&&a.error("prefetch requires url to be set"),d.transform=d.filter||d.transform,d.cacheKey=d.cacheKey||d.url,d.thumbprint=c+d.thumbprint,d.transport=d.transport?h(d.transport):a.ajax,d):null}function e(c){var d;if(c)return d={url:null,cache:!0,prepare:null,replace:null,wildcard:null,limiter:null,rateLimitBy:"debounce",rateLimitWait:300,transform:b.identity,transport:null},c=b.isString(c)?{url:c}:c,c=b.mixin(d,c),!c.url&&a.error("remote requires url to be set"),c.transform=c.filter||c.transform,c.prepare=f(c),c.limiter=g(c),c.transport=c.transport?h(c.transport):a.ajax,delete c.replace,delete c.wildcard,delete c.rateLimitBy,delete c.rateLimitWait,c}function f(a){function b(a,b){return b.url=f(b.url,a),b}function c(a,b){return b.url=b.url.replace(g,encodeURIComponent(a)),b}function d(a,b){return b}var e,f,g;return e=a.prepare,f=a.replace,g=a.wildcard,e?e:e=f?b:a.wildcard?c:d}function g(a){function c(a){return function(c){return b.debounce(c,a)}}function d(a){return function(c){return b.throttle(c,a)}}var e,f,g;return e=a.limiter,f=a.rateLimitBy,g=a.rateLimitWait,e||(e=/^throttle$/i.test(f)?d(g):c(g)),e}function h(c){return function(d){function e(a){b.defer(function(){g.resolve(a)})}function f(a){b.defer(function(){g.reject(a)})}var g=a.Deferred();return c(d,e,f),g}}return function(c){var f,g;return f={initialize:!0,identify:b.stringify,datumTokenizer:null,queryTokenizer:null,sufficient:5,sorter:null,local:[],prefetch:null,remote:null},c=b.mixin(f,c||{}),!c.datumTokenizer&&a.error("datumTokenizer is required"),!c.queryTokenizer&&a.error("queryTokenizer is required"),g=c.sorter,c.sorter=g?function(a){return a.sort(g)}:b.identity,c.local=b.isFunction(c.local)?c.local():c.local,c.prefetch=d(c.prefetch),c.remote=e(c.remote),c}}(),l=function(){"use strict";function c(a){a=k(a),this.sorter=a.sorter,this.identify=a.identify,this.sufficient=a.sufficient,this.local=a.local,this.remote=a.remote?new j(a.remote):null,this.prefetch=a.prefetch?new i(a.prefetch):null,this.index=new h({identify:this.identify,datumTokenizer:a.datumTokenizer,queryTokenizer:a.queryTokenizer}),a.initialize!==!1&&this.initialize()}var e;return e=window&&window.Bloodhound,c.noConflict=function(){return window&&(window.Bloodhound=e),c},c.tokenizers=d,b.mixin(c.prototype,{__ttAdapter:function(){function a(a,b,d){return c.search(a,b,d)}function b(a,b){return c.search(a,b)}var c=this;return this.remote?a:b},_loadPrefetch:function(){function b(a,b){return a?c.reject():(e.add(b),e.prefetch.store(e.index.serialize()),void c.resolve())}var c,d,e=this;return c=a.Deferred(),this.prefetch?(d=this.prefetch.fromCache())?(this.index.bootstrap(d),c.resolve()):this.prefetch.fromNetwork(b):c.resolve(),c.promise()},_initialize:function(){function a(){b.add(b.local)}var b=this;return this.clear(),(this.initPromise=this._loadPrefetch()).done(a),this.initPromise},initialize:function(a){return!this.initPromise||a?this._initialize():this.initPromise},add:function(a){return this.index.add(a),this},get:function(a){return a=b.isArray(a)?a:[].slice.call(arguments),this.index.get(a)},search:function(a,c,d){function e(a){var c=[];b.each(a,function(a){!b.some(f,function(b){return g.identify(a)===g.identify(b)})&&c.push(a)}),d&&d(c)}var f,g=this;return f=this.sorter(this.index.search(a)),c(this.remote?f.slice():f),this.remote&&f.length<this.sufficient?this.remote.get(a,e):this.remote&&this.remote.cancelLastRequest(),this},all:function(){return this.index.all()},clear:function(){return this.index.reset(),this},clearPrefetchCache:function(){return this.prefetch&&this.prefetch.clear(),this},clearRemoteCache:function(){return g.resetCache(),this},ttAdapter:function(){return this.__ttAdapter()}}),c}();return l});