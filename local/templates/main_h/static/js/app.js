'use strict';

$(document).on('scroll', '.is-mobile-nav-show', function () {
    if ($(".mobile-nav__city").scrollTop() > 300) {
        $('.mobile-nav__city-close').addClass('is-visible');
    } else {
        $('.mobile-nav__city-close').removeClass('is-visible');
    }
});

$('#soa-property-7').mask('+7 (999) 999-9999');

var jsCityInputFunc;

!function (t) {
    "use strict";
    var e = function () {
        this.domReady = !1
    };
    e.prototype.bindReady = function (e, i) {
        function o() {
            n || (n = !0, s.domReady = !0, e(i))
        }

        function a() {
            if (!n && document.body) try {
                document.documentElement.doScroll("left"), o()
            } catch (t) {
                setTimeout(a, 0)
            }
        }

        var n = !1,
            s = this;
        return this.domReady ? e(i) : (document.addEventListener ? document.addEventListener("DOMContentLoaded", function () {
            o()
        }, !1) : document.attachEvent && (document.documentElement.doScroll && t === t.top && a(), document.attachEvent("onreadystatechange", function () {
            "complete" === document.readyState && o()
        })), void(t.addEventListener ? t.addEventListener("load", o, !1) : t.attachEvent && t.attachEvent("onload", o)))
    }, e.prototype.waitFinalEvent = function () {
        var t = {};
        return function (e, i, o) {
            o || (o = "timer" + (new Date).getTime()), t[o] && clearTimeout(t[o]), t[o] = setTimeout(e, i)
        }
    }(), e.prototype.runAnimation = function (e) {
        function i(t) {
            var n = !1;
            if (null != a) {
                var s = Math.min(t - a, 100);
                n = e(s) === !1
            }
            a = t, n || o(i)
        }

        var o = t.requestAnimationFrame || t.webkitRequestAnimationFrame || t.mozRequestAnimationFrame || t.msRequestAnimationFrame,
            a = null;
        o(i)
    }, t.Utils = new e
}(window), bobr({
    test: "ie10",
    callback: function (t) {
        t || alert("Вы используете устаревший браузер! Сайт может отображаться некорректно. Рекомендуем вам обновить браузер..")
    }
}), $(function () {
    "use strict";
    if (window.Modernizr && Modernizr.touchevents && document.body.setAttribute("ontouchstart", ""), navigator.userAgent.match(/IEMobile\/10\.0/)) {
        var t = document.createElement("style");
        t.appendChild(document.createTextNode("@-ms-viewport{width:auto!important}")), document.getElementsByTagName("head")[0].appendChild(t)
    }
    svg4everybody(), $(document).on("click", ".js-toggle-el", function (t) {
        t.preventDefault();
        var e = $(this),
            i = $(e.data("toggle-target")),
            o = $(this).data("toggle-prop") || "height",
            a = {};
        a[o] = "toggle", i && i.animate(a, 300, function () {
            e.toggleClass("is-target-hidden", !i.is(":visible"))
        })
    }), void 0 != $.fn.doubleTapToGo && $(".js-has-drop").doubleTapToGo(), $(".js-click-onload").click(), void 0 !== Jump && $(document).on("click", ".js-scroll-to", function (t) {
        t.preventDefault();
        var e = $(this),
            i = e.data("scroll-to") || e.attr("href"),
            o = e.data("scroll-offset") || 0;
        Jump(i, {
            offset: o
        })
    }), $(document).on("click", ".js-spoiler", function (t) {
        t.preventDefault();
        var e = $(this),
            i = e.data("spoiler-target") || e.attr("href"),
            o = $(i);
        0 != o.length && (o.animate({
            height: "toggle"
        }, 300), e.toggleClass("is-expand"))
    });
    var e = $(".js-title-line");
    if (e.each(function () {
        var t = $(this),
            e = new Waypoint.Inview({
                element: this,
                entered: function (i) {
                    t.addClass("is-animated"), e.destroy()
                }
            })
    }), void 0 !== $.fn.perfectScrollbar) {
        var i = function (t) {
            var e = t.find(".is-current").first(),
                i = t.find(":checked").first(),
                o = e.length > 0 ? e : i.length > 0 ? i.parent() : null;
            o && t.scrollTop(o.position().top)
        };
        $(".js-scroll-block").each(function () {
            var t = $(this);
            t.perfectScrollbar({
                suppressScrollX: !0,
                theme: "filters",
                minScrollbarLength: 16,
                maxScrollbarLength: 16
            }), i(t)
        }), $("body").on("catalogFiltersOpened", function () {
            $(".js-scroll-block").each(function () {
                var t = $(this);
                t.perfectScrollbar("update"), i(t)
            })
        })
    }
    void 0 != $.fn.inputmask && $(".js-mask-phone").inputmask("mask", {
        mask: "+7 (999) 9999999",
        placeholder: "*",
        clearIncomplete: !0,
        showMaskOnHover: !1,
        greedy: !1
    }), VideoPlayer && $(document).on("click", ".js-youtube-init", function (t) {
        t.preventDefault();
        var e, i = $(this),
            o = i.attr("href"),
            a = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/,
            n = o.match(a);
        n && 11 === n[1].length && (e = n[1]), e && VideoPlayer.init(e, this)
    });
    var o = $(".js-focus-b");
    o.on("mouseenter click", function () {
        var t = $(this);
        t.hasClass("is-active") || (o.removeClass("is-active"), t.addClass("is-active"))
    }), $(".js-sticky").Stickyfill()
}), $(function () {
    "use strict";
    if (void 0 !== $.fn.slick) {
        var t = function (t, e) {
            function i(t, e) {
                var i = t.options.slidesToShow,
                    o = t.slideCount,
                    a = (o - o % i) / i;
                e = (e || 0) + 1;
                var n = Math.ceil(e / i);
                s.text(n + " / " + (a + o % i))
            }

            var o = t.find(".js-carousel__items"),
                a = t.find(".js-carousel__prev"),
                n = t.find(".js-carousel__next"),
                s = t.find(".js-carousel__stat"),
                r = {
                    mobileFirst: !0,
                    prevArrow: !1,
                    nextArrow: !1
                },
                c = $.extend(r, e);
            return o.on("afterChange", function (t, e, o) {
                i(e, o)
            }), o.on("init", function (t, e) {
                i(e)
            }), o.slick(c), a.on("click", function () {
                o.slick("slickPrev")
            }), n.on("click", function () {
                o.slick("slickNext")
            }), {
                $node: o,
                options: c
            }
        };
        $(".js-carousel--categories").each(function () {
            var e = t($(this), {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    responsive: [{
                        breakpoint: 768,
                        settings: "unslick"
                    }]
                }),
                i = e.$node,
                o = e.options;
            $(window).on("resize orientationChange", function (t) {
                Utils.waitFinalEvent(function () {
                    var t = i.slick("getSlick");
                    t.unslicked && $.windowWidth() < 768 && i.slick(o)
                }, 300, "recalcCategoriesCarousel")
            })
        }), $(".js-carousel--catalog").each(function () {
            function e() {
                var t = Math.max.apply(null, $.map(s, function (t) {
                    return $(t).css("height", ""), $(t).innerHeight()
                }));
                s.css({
                    height: t + "px"
                })
            }

            var i = $(this),
                o = parseInt(i.data("rows")) || 1,
                a = t(i, {
                    slidesPerRow: 2,
                    rows: 1,
                    responsive: [{
                        breakpoint: 768,
                        settings: {
                            slidesPerRow: 3,
                            rows: o
                        }
                    }, {
                        breakpoint: 992,
                        settings: {
                            slidesPerRow: 4,
                            rows: o
                        }
                    }]
                }),
                n = a.$node,
                s = n.find(".catalog__item");
            setTimeout(function () {
                e()
            }, 500), n.on("breakpoint", e), $(window).on("resize orientationChange", function () {
                Utils.waitFinalEvent(e, 300, "recalcProductsCarousel")
            })
        }), $(".js-carousel--brands").each(function () {
            t($(this), {
                slidesToShow: 2,
                slidesToScroll: 2,
                responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                }, {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4
                    }
                }]
            })
        }), $("#js-aside-carousel").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: !1,
            dots: !0,
            autoplay: !0,
            autoplaySpeed: 5e3,
            appendDots: $("#js-aside-carousel-dots")
        }), $(".js-hello-slider").each(function () {
            function t(t, e, i, o) {
                var n = o || 0;
                a.filter(".is-active").removeClass("is-active"), a.filter(function () {
                    return $(this).data("slide") == n
                }).addClass("is-active")
            }

            var e = $(this),
                i = e.find(".js-hello-slider__items"),
                o = e.find(".js-hello-slider__dots"),
                a = e.find(".js-hello-slider__nav");
            i.on("init beforeChange", t), a.on("click", function (t) {
                t.preventDefault(), i.slick("slickGoTo", parseInt($(this).data("slide")))
            }), i.slick({
                mobileFirst: !0,
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: !1,
                dots: !0,
                appendDots: o,
                autoplay: !0,
                autoplaySpeed: 4e3,
                responsive: [{
                    breakpoint: 992,
                    settings: {
                        fade: !0
                    }
                }]
            })
        }), $(".js-carousel--slider").each(function () {
            t($(this), {
                slidesToShow: 1,
                slidesToScroll: 1
            })
        }),
            $(".js-portfolio-carousel").each(function () {
            var t = $(this),
                e = parseInt(t.data("slides")) || 3;
            t.slick({
                mobileFirst: !0,
                slidesToShow: 1,
                slidesToScroll: 1,
                adaptiveHeight: true,
                arrows: !0,
                responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: e,
                        slidesToScroll: e,
                        adaptiveHeight: true
                    }
                }]
            })
            let $maxHeight     = 0;
            const $CatalogItem = $(".portfolio__link");


            $CatalogItem.each(function() {
                if ($(this).height() > $maxHeight) {
                    $maxHeight = $(this).height();
                }
            });
            $CatalogItem.height($maxHeight);
        })
    }
}), $(function () {
    "use strict";
    $(".js-checkout-steps-scroll").each(function () {
        var t = $(this),
            e = t.find(".checkout-steps__item--current");
        t.perfectScrollbar({
            suppressScrollY: !0
        }), 0 != e.length && (t.scrollLeft(e.position().left - 30), $(window).on("resize", function () {
            t.perfectScrollbar("update"), t.scrollLeft(e.position().left - 30)
        }))
    }), $(".js-pickup").on("change", function (t) {
        var e = $(this);
        e.find(".js-pickup__content").animate({
            height: "hide"
        });
        var i = e.find(".js-pickup__radio:checked"),
            o = i.closest(".js-pickup__item"),
            a = o.find(".js-pickup__content"),
            n = a.find(".js-pickup__map");
        if (a.stop().animate({
            height: i.is(":checked") ? "show" : "hide"
        }, 300), n.hasClass("is-loaded")) n.data("map").container.fitToViewport();
        else {
            n.addClass("is-loaded");
            var s, r = n.data("center") || [44.059654, 43.034953],
                c = n.data("zoom") || 16,
                l = n.data("marker"),
                u = n.data("marker-image");
            YamapsService.setApiParams({
                lang: "ru_RU"
            }), YamapsService.run(function () {
                if (s = new ymaps.Map(n[0], {
                    center: r,
                    zoom: c,
                    controls: ["zoomControl"],
                    autoFitToViewport: "always"
                }), n.find(".js-map-preloader").remove(), l && l.latlng) {
                    var t = new ymaps.Placemark(l.latlng, {}, {
                        iconLayout: "default#image",
                        iconImageHref: u,
                        iconImageSize: [44, 54],
                        iconImageOffset: [-22, -54]
                    });
                    s.geoObjects.add(t)
                }
                YamapsService.fixScroll(s, {
                    hoverDelay: 1e3
                }), n.data("map", s)
            })
        }
    })
}), $(function () {
    "use strict";
    var t = $("#contacts-map");
    if (0 !== t.length) {


        var e, i = t.data("center") || [44.059654, 43.034953],
            o = t.data("zoom") || 12,
            a = t.data("markers"),
            n_ = t.data("marker-image"),
            s = $("#contacts-map-city");

        YamapsService.setApiParams({
            lang: "ru_RU"
        }), YamapsService.run(function () {
            e = new ymaps.Map(t[0], {
                center: i,
                zoom: o,
                controls: ["zoomControl"]
            }), $("#contacts-map-preloader").remove();
            var r = ymaps.templateLayoutFactory.createClass('<div class="map-baloon"><div class="map-baloon__title">$[properties.title]</div><div class="map-baloon__text">$[properties.text]</div></div>');
            if (a && Object.keys(a).length)

                for (var c = 0; c < a.length; c++) {

                    var l = new ymaps.Placemark(a[c].latlng, {
                        title: a[c].title,
                        text: a[c].text
                    }, {
                        iconLayout: "default#image",
                        iconImageHref: a[c].img,
                        iconImageSize: [44, 54],
                        iconImageOffset: [-22, -54],
                        balloonContentLayout: r
                    });
                    e.geoObjects.add(l)
                }
            0 !== s.length && s.on("change", function () {
                var t = $(this).find(":selected").data("map-center");
                t && e.setCenter(t)
            }), YamapsService.fixScroll(e, {
                hoverDelay: 1e3
            })
        })
    }
}), $(function () {
    "use strict";
    $(document).on("click", ".js-toggle-filters-sm", function () {
        $("body").toggleClass("is-show-filters").trigger("catalogFiltersOpened")
    }), noUiSlider && $(".js-range").each(function () {
        var t, e, i, o, a, n = $(this),
            s = n.find(".js-range-scale"),
            r = n.find(".js-range-min"),
            c = n.find(".js-range-min-val"),
            l = n.find(".js-range-max"),
            u = n.find(".js-range-max-val");
        t = parseInt(n.data("range-min")) || 0, e = parseInt(n.data("range-max")) || 100, i = parseInt(n.data("range-step")) || 10, o = parseInt(r.val()) || t, a = parseInt(l.val()) || e, 0 != r.length && 0 != l.length && 0 != s.length && (noUiSlider.create(s[0], {
            start: [o, a],
            step: i,
            connect: !0,
            range: {
                min: t,
                max: e
            },
            format: wNumb({
                decimals: 0,
                thousand: " "
            })
        }), s[0].noUiSlider.on("update", function (t, e) {
            var i = t[e];
            e ? (l[0].value = i, u[0].value = i.replace(" ", "")) : (r[0].value = i, c[0].value = i.replace(" ", ""))
        }), s[0].noUiSlider.on("change", function (t, e) {
            c.change();
            r.change();
        }), r[0].addEventListener("change", function () {
            s[0].noUiSlider.set([this.value, null]);
            c.change();
            r.change();
        }), l[0].addEventListener("change", function () {
            s[0].noUiSlider.set([null, this.value]);
            c.change();
            r.change();
        }))
    });
    var t = $("#filter"),
        e = $("#filter-bubble");
    t.on("change", function (t) {
        var i = $(t.target),
            o = i.closest(".filter__b");
        e.hide().removeClass("is-show"), e.appendTo(o), e.show();
        //e.addClass("is-show")
    })
}), $(function () {
    function t() {
        a.removeClass("is-mobile-nav-show")
    }

    function e() {
        s.hide()
    }

    function i() {
        var t = $(window).scrollTop(),
            e = 80;
        c.toggleClass("is-show", t >= r + e)
        $('.footer__up').toggleClass('is-show', t >= r + e);
    }

    function o() {
        $.documentWidth() >= 992 ? (r = n.outerHeight(!0), i(), $(document).on("scroll", i)) : $(document).off("scroll", i)
    }

    var a = $("body"),
        n = $("#header"),
        s = $("#header-search-form");
    $(document).on("click", ".js-toggle-search", function (e) {
        e.preventDefault(), t(), s.animate({
            opacity: "toggle"
        }, 300, function () {
            s.is(":visible") ? s.find("input").focus() : s.find("input").blur()
        })
    }),
        $(".js-toggle-mobile-nav").on("click", function () {
            e(),
                $("body").toggleClass("is-mobile-nav-show");
            $(this).toggleClass("js-toggle-mobile-nav--open", 1000);
        });
    var r, c = $("#header-fixed");
    $.documentWidth() >= 992 && o(),
        $(window).on("resize", o);
    var l = $("#mobile-nav");
    $("#mobile-nav-city").on("click", function (t) {
        t.preventDefault(),
            l.toggleClass("is-show-city");


    }),
        $(".js-toggle-mobile-city").on("click", function () {
            $("#mobile-nav").removeClass("is-show-city");
            return false;
        }),
        $(".js-select-city-md").on("click", function (t) {
            t.preventDefault();
            var e = $(this),
                i = {
                    top: e.offset().top + e.outerHeight(),
                    left: e.offset().left + e.outerWidth() / 2
                };
            Popups.openCityPopup(i)
        })
}),


    $(function () {

        void 0 != $.magnificPopup && ($.extend(!0, $.magnificPopup.defaults, {
            tClose: "Close (Esc)",
            tLoading: "Loading...",
            gallery: {
                enabled: true,
                tPrev: "Previous (Left arrow key)",
                tNext: "Next (Right arrow key)",
                tCounter: "%curr% / %total%"
            },
            iframe: {
                markup: '<div class="mfp-iframe-scaler"><iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe></div>'
            },
            image: {
                markup: '<div class="mfp-figure"><div class="mfp-img"></div><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></div>',
                tError: '<a href="%url%">The image</a> could not be loaded.'
            },
            ajax: {
                tError: '<a href="%url%">The content</a> could not be loaded.'
            },
            autoFocusLast: !1,
            callbacks: {
                open: function () {
                    const mc = new Hammer(document.body);
                    mc.on('swipeleft', function () {
                        $.magnificPopup.instance.prev();
                    });

                    mc.on('swiperight', function () {
                        $.magnificPopup.instance.next();
                    });
                    mc.on('swipeup', function () {
                        $.magnificPopup.instance.close();
                    });
                }
            }
        }),

            $(".js-popup-video").magnificPopup({
                type: "iframe",
                closeBtnInside: !1
            }),
            $(".js-popup-gallery").each(function () {
                $(this).magnificPopup({
                    delegate: "a",
                    type: "image",
                    gallery: {
                        enabled: !0
                    },
                    closeBtnInside: !1,
                    callbacks: {
                        elementParse: function (t) {
                            t.el.hasClass("js-popup-gallery__video") && (t.type = "iframe")
                        }
                    }
                })
            }),

            $(".js-popup").magnificPopup({
                type: "inline"
            })),
            function (t) {
                var e = function () {
                    this.initCityPopup()
                };
                e.prototype.showMessage = function (t) {
                    var e = "";
                    t.title && (e += '<div class="popup__title title-line title-line--accent is-animated">' + t.title + "</div>"), t.text && (e += '<div class="popup__text">' + t.text + "</div>"), $.magnificPopup.open({
                        items: {
                            src: $('<div class="popup">' + e + "</div>"),
                            type: "inline"
                        }
                    }, 0)
                }, e.prototype.initCityPopup = function () {
                    if (!this._initCityPopupNodes()) return !1;
                    this._setCityPopupPosition();
                    var e = this;
                    this.city.$anchor.on("click", function (t) {
                        t.preventDefault(), e.openCityPopup()
                    }), $(t).on("resize", function () {
                        Utils.waitFinalEvent(function () {
                            $.windowWidth() >= 992 && e._setCityPopupPosition()
                        }, 300, "recalcCityPopupPosition")
                    })
                }, e.prototype.openCityPopup = function (t) {
                    //loadcitylist from ajax
                    if ($("#js-what-city-select .what-city__section .city_list").text().length == 0) {
                        $.ajax({
                            url: "/ajax/cities_list.php",
                            success: function (data) {
                                var obj = jQuery.parseJSON(data);
                                var items = Math.floor(obj.length / 3);
                                var str = '<div class="col-4">';
                                var mobile_str = '';
                                for (var i = 0; i < obj.length; i++) {
                                    str += '<div class="what-city__item"><a class="link link--inverse" href="?set_city=' + obj[i].id + '">' + obj[i].name + '</a></div>';
                                    mobile_str += '<div class="mobile-nav__city-item"><a class="link-hidden" href="?set_city=' + obj[i].id + '">' + obj[i].name + '</a></div>';
                                    if (i == items) {
                                        str += '</div><div class="col-4">';
                                        items += Math.ceil(obj.length / 3);
                                    }
                                }
                                str += '</div>';
                                $("#js-what-city-select .what-city__section .city_list").html(str);
                                $(".mobile-nav__city-list").html(mobile_str);
                            }
                        });
                    }
                    var e = t ? this.city.$ask : this.city.$select,
                        i = t ? this.city.$select : this.city.$ask;
                    i.removeClass("is-active"), e.addClass("is-active"), this.city.$wrap.animate({
                        opacity: "show"
                    }, 400), void 0 !== Jump && Jump("body")
                }, e.prototype.closeCityPopup = function () {
                    this.city.$ask.removeClass("is-active"), this.city.$select.removeClass("is-active"), this.city.$wrap.animate({
                        opacity: "hide"
                    }, 400)
                }, e.prototype._initCityPopupNodes = function () {
                    var t = $("#js-what-city"),
                        e = $("#js-what-city-anchor");
                    return 0 !== t.length && 0 !== e.length && (this.city = {
                        $wrap: t,
                        $ask: $("#js-what-city-ask"),
                        $select: $("#js-what-city-select"),
                        $anchor: e
                    }, !0)
                }, e.prototype._setCityPopupPosition = function () {
                    var t = this.city.$anchor,
                        e = this.city.$ask,
                        i = this.city.$select,
                        o = {
                            top: t.offset().top + t.outerHeight(),
                            left: t.offset().left + t.outerWidth() / 2
                        };
                    e.css({
                        top: o.top,
                        left: o.left
                    }), i.css({
                        top: o.top,
                        left: o.left
                    })
                }, t.Popups = new e
            }(window)
    }),


    $(function () {
        function t() {
            var t = e.position().left;
            i.animate({
                scrollLeft: t
            })
        }

        $(".js-product-slider").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: !1,
            infinite: false,
            asNavFor: ".js-product-thumbs",
            focusOnSelect: !0
        }), $(".js-product-thumbs").slick({
            slidesToShow: 3,
            centerPadding: 0,
            centerMode: 0,
            slidesToScroll: 1,
            arrows: !1,
            asNavFor: ".js-product-slider",
            focusOnSelect: !0,
            mobileFirst: !0,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 4
                }
            }]
        });
        var e = $('a[href="#product-reviews"]'),
            i = e.closest(".js-island-tabs__scroll");
        $(document).on("click", ".js-scroll-to-reviews", function () {
            Jump(e[0], {
                offset: -150,
                callback: function () {
                    e[0].click()
                }
            }), t()
        }), $(document).on("click", ".js-scroll-to-reviews-form", function () {
            e[0].click(), t();
            var i = document.getElementById("review-form");
            i ? Jump(i, {
                offset: -150
            }) : Jump(e[0], {
                offset: -150
            })
        })
    }),
    $(function () {
        "use strict";
        void 0 != $.magnificPopup && $(".js-portfolio-add").magnificPopup({
            callbacks: {
                elementParse: function (t) {
                    $(t.el).data("category")
                }
            }
        }), $(document).on("click", ".js-history-item__toggle", function (t) {
            t.preventDefault();
            var e = $(this),
                i = e.closest(".js-history-item"),
                o = i.find(".js-history-item__holder");
            if (o) {
                var a = e.attr("href");
                o.removeClass(".js-history-item__holder"), $.ajax({
                    url: a,
                    success: function (t) {
                        o.replaceWith($(t))
                    }
                })
            }
            i.toggleClass("is-active")
        })
    }), $(function () {
    "use strict";

    var toggleVideo = function () {
        // if state == 'hide', hide. Else: show video

        var div = document.getElementById("popupVid");
        if (div) {
            var iframes = div.getElementsByTagName("iframe");
            for (var i = 0; i < iframes.length; i++) {
                iframes[i].contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
            }
        }
    }

    function t(t) {

        if (t && t.content) {
            var e = $(t.content).find(".js-tab-ajax-content");
            0 != e.length && (e.removeClass(".js-tab-ajax-content"), $.ajax({
                url: e.data("src"),
                success: function (t) {
                    e.replaceWith($(t))
                }
            }));

            if ($(t.content).is('[data-url]')) {
                window.history.pushState('', '', $(t.content).attr('data-url'));
            }

            /*x5 20180906 добавление хлебных крошек для брендов begin*/
            if ($(t.content).is('[data-x5-breadcrumb-name]')) {

                var html = "<a class=\"breadcrumbs__item-link\" href=\"" + $(t.content).attr('data-url') + "\" itemprop=\"url\">" +
                    "<span class=\"breadcrumbs__item-title\" itemprop=\"title\">" + $(t.content).attr('data-x5-breadcrumb-name') + "</span></a>";


                $breadcrumb_el = $('li.breadcrumbs__item.js-tab-inject');
                if ($breadcrumb_el.length <= 0) {
                    var $breadcrumb_el = $('<li></li>', {
                        class: 'breadcrumbs__item js-tab-inject',
                        itemtype: 'http://data-vocabulary.org/Breadcrumb',
                        itemscope: 'itemscope',
                        html: html
                    });
                    $('li.breadcrumbs__item>span').parent().prepend($breadcrumb_el);
                } else {
                    $breadcrumb_el.html(html);
                }

            } else {
                $('li.breadcrumbs__item.js-tab-inject').remove();
            }

            /*x5 20180906 добавление хлебных крошек для калькуляторов begin*/
            if ($(t.content).is('[data-x5-breadcrumb-name-after]')) {


                var html = "<span class=\"breadcrumbs__item-title\" itemprop=\"title\">" + $(t.content).attr('data-x5-breadcrumb-name-after') + "</span>";

                $breadcrumb_el = $('li.breadcrumbs__item.js-tab-inject-after');
                if ($breadcrumb_el.length <= 0) {

                    var calc_breadcrumb = $('li.breadcrumbs__item>span').parent();

                    if (calc_breadcrumb.length > 0) {
                        var html_calcs = "<a class=\"breadcrumbs__item-link\" href=\"/catalog/calcs/\" itemprop=\"url\">" +
                            "<span class=\"breadcrumbs__item-title\" itemprop=\"title\">" + calc_breadcrumb.html() + "</span></a>";
                        calc_breadcrumb.html(html_calcs);

                        var $breadcrumb_el = $('<li></li>', {
                            class: 'breadcrumbs__item js-tab-inject-after',
                            itemtype: 'http://data-vocabulary.org/Breadcrumb',
                            itemscope: 'itemscope',
                            html: html
                        });
                        calc_breadcrumb.after($breadcrumb_el);
                    }
                } else {
                    $breadcrumb_el.html(html);
                }

            } else {
                $('li.breadcrumbs__item.js-tab-inject-after').remove();
            }
            /*x5 20180906 добавление хлебных крошек для калькуляторов end*/
        }

        toggleVideo();
    }

    $(".js-island-tabs").each(function () {


        function t() {
            var t = i.outerWidth(),
                e = i.scrollLeft();
            e >= 30 ? a.show() : a.hide(), e < s - t - 30 ? n.show() : n.hide();
        }

        var e = $(this),
            i = e.find(".js-island-tabs__scroll"),
            o = i.find(".island-tabs__item--current"),
            a = e.find(".js-island-tabs__l"),
            n = e.find(".js-island-tabs__r"),
            s = 0;
        i.children().each(function () {
            s += $(this).outerWidth()
        }), i.perfectScrollbar({
            suppressScrollY: !0
        }), 0 != o.length && (i.scrollLeft(o.position().left - 30), $(window).on("resize", function () {
            i.perfectScrollbar("update"), i.scrollLeft(o.position().left - 30)
        })), t(), i.on("scroll", t), $(window).on("resize", t)
    }), $(".js-cls-tabs").each(function () {
        new Tabit(this, {
            buttonSelector: ".js-cls-tabs__btn",
            contentSelector: ".js-cls-tabs__content",
            buttonActiveClass: "is-active",
            contentActiveClass: "is-active",
            closable: !0,
            activeIndex: 0,
            onChange: t
        })
    }), $(".js-radiotabs").each(function () {
        new Tabit(this, {
            buttonSelector: ".js-radiotabs__btn",
            contentSelector: ".js-radiotabs__content",
            buttonActiveClass: "is-active"
        })
    }), $(".js-tabs").each(function () {

        new Tabit(this, {
            buttonSelector: ".js-tabs__btn",
            contentSelector: ".js-tabs__content",
            buttonActiveClass: "is-active",
            contentActiveClass: "is-active",
            activeIndex: $(this).data("active") || 0,
            onChange: t
        })
    })
}),
    function () {
        var t = function () {
            this.youTubeApiReady = !1, this.youTubeApiLoading = !1, this.youTubeApiWaitList = [], this.youTubePlayers = []
        };
        t.prototype.get = function () {
            return this
        }, t.prototype.init = function (t, e, i) {
            i = i || "youtube", "youtube" == i && (this.pauseYouTubePlayers(), this._createYoutubePlayer(t, e))
        }, t.prototype.pauseYouTubePlayers = function (t) {
            for (var e = 0; e < this.youTubePlayers.length; e++) this.youTubePlayers[e] != t && this.youTubePlayers[e].pauseVideo()
        }, t.prototype._createYoutubePlayer = function (t, e, i) {
            var o = this.youTubePlayers,
                a = this.pauseYouTubePlayers.bind(this),
                n = function () {
                    var n = new YT.Player(e, {
                        height: "100%",
                        width: "100%",
                        videoId: t,
                        playerVars: {
                            autohide: 1,
                            showinfo: 0,
                            autoplay: 1
                        },
                        events: {
                            onReady: function (t) {
                                i && i()
                            },
                            onStateChange: function (t) {
                                1 == t.data && a(t.target)
                            }
                        }
                    });
                    return o.push(n), n
                };
            this._loadYoutubePlayer(n)
        }, t.prototype._loadYoutubePlayer = function (t) {
            this._loadYoutubeApi(), this.youTubeApiReady ? t() : this.youTubeApiWaitList.push(t)
        }, t.prototype._loadYoutubeApi = function () {
            if (!this.youTubeApiReady && !this.youTubeApiLoading) {
                this.youTubeApiLoading = !0;
                var t = this;
                window.onYouTubeIframeAPIReady = function () {
                    t._onYouTubeApiReady()
                };
                var e = document.createElement("script");
                e.src = "https://www.youtube.com/iframe_api";
                var i = document.getElementsByTagName("script")[0];
                i.parentNode.insertBefore(e, i)
            }
        }, t.prototype._onYouTubeApiReady = function () {
            this.youTubeApiReady = !0, this.youTubeApiLoading = !1;
            for (var t = 0; t < this.youTubeApiWaitList.length; t++) this.youTubeApiWaitList[t]()
        }, window.VideoPlayer = new t
    }(window), $(function () {
    "use strict";
    $("body").on("submit", ".js-ask-form", function (t) {

        var e = $(this),
            i = e.data("validator") || FormValidator(e);
        i.activate();
        var o = i.validate(),
            a = e.find(".js-form-error");
        if (a.text("").hide(), !o.isValid) {
            t.preventDefault();
            console.log("false");
            if (o.invalidFields) {
                var n = "Не корректно заполнены или не заполнены поля: ";
                o.invalidFields.forEach(function (t, e, i) {
                    n += $(t).data("field-name"), e < i.length - 1 && (n += ", "), a.text(n), a.show()
                })
            }
            return !1
        }
        console.log("true");
        return true;
    })
}), $(jsCityInputFunc = function () {
    var t = function (t) {
        return function (e, i) {
            var o, a;
            o = [], a = new RegExp(e, "i"), $.each(t, function (t, e) {
                a.test(e) && o.push(e)
            }), i(o)
        }
    };
    $(".js-city-input").one("focus", function () {
        var e, i = $(this);

//            o = i.data("cities-src");
//        o && $.get(o, function(e) {
//            e = JSON.parse(o);


        var bestPictures = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: i.data("cities-src") + '?q=%QUERY',
                wildcard: '%QUERY'
            }
        });


        i.typeahead({
            hint: !0,
            highlight: !0,
            minLength: 1
        }, {
            name: "cities",
            source: bestPictures
        }), i.data("cities", e), i.focus(),
            i.on("typeahead:change", function (event, city_name) {
//                if (i.attr('data-key'))
//                {
//                    $(i.attr('data-key')).val('');
//                }
//alert(2);
            }),
            i.on("typeahead:select", function (event, city_name) {

//                console.log($(this));
                console.log(bestPictures);
//                console.log(event);
//                console.log(city_name);
                if (i.attr('data-key')) {
                    for (var z in bestPictures.remote.transport._cache.list.head.val) {
                        if (bestPictures.remote.transport._cache.list.head.val[z] == city_name) {
                            $(i.attr('data-key')).val(z);
                            $(i.attr('data-key')).change();

                            break;
                        }
                    }
                }
                i.change();

                if (i.hasClass('js-city-select__input')) {
                    i.parents('.search-form').find('.js-city-select__submit').removeAttr('disabled');
                }

            }), i.on("typeahead:change", function (eee) {
            i.change()
        })
//        }, 'json')
    })
}), $(function () {
    $(".js-city-select").each(function () {
        var t = $(this),
            e = t.find(".js-city-select__input"),
            i = t.find(".js-city-select__submit");
        e.on("change input", function () {
//            var t = e.data("cities");
//
//            var t_new = $.map(t, function(value, index) {
//                return [value];
//            });
//
//            if (t) {
//                var o = t_new.indexOf(e.val()) !== -1;
//                i.attr("disabled", !o),
//                e.toggleClass("is-invalid", !o)
//            }
        }), t.on("submit", function (t) {
            e.toggleClass("is-invalid", 0 === e.val().length)
        })
    })
}),
    function (t) {
        var cacheDoubleEmail = {};
        var e = function (t) {
            if (!(this instanceof e)) return new e(t);
            var i = this;
            return i.$form = t, i.$fields = t.find(":input"), i.toggleClasses = !1, t.attr("novalidate", !0), t.data("validator", this), t.on("change input", function (t) {
                i.checkField(t.target)
            }), this
        };
        e.prototype.checkField = function (field) {
            var $field = $(field);
            var isRequired = $field.attr('required') !== undefined;
            var minLength = ($field.attr('data-minlength') !== undefined ? parseInt($field.attr('data-minlength')) : 0);
            var extLimit = $field.attr('type') === 'file' && ($field.attr('data-extlimitlist') !== undefined);
            var maxFilesize = $field.attr('type') === 'file' && ($field.attr('data-maxfilesize') !== undefined);
            var maxFilesizeVal = ($field.attr('data-maxfilesize') !== undefined ? parseInt($field.attr('data-maxfilesize')) : 0);
            var extLimitList = ($field.attr('data-extlimitlist') !== undefined ? $field.attr('data-extlimitlist') : '');
            var doubleEmail = ($field.attr('data-doubleemail') !== undefined);
            var isEmail = $field.attr('type') === 'email';
            var validCheckbox = isRequired && $field.attr('type') === 'checkbox';
            var validRadio = isRequired && $field.attr('type') === 'radio';
            var value = $field.val();

            var doubleEmailValid = true;
            if (doubleEmail && isEmail && validator.isEmail(value)) {

                if (cacheDoubleEmail[value] !== undefined) {
                    doubleEmailValid = cacheDoubleEmail[value];
                    console.log('cache');
                }
                else {
                    $.ajax({
                        type: 'POST',
                        url: '/ajax/doubleemail.php',
                        data: {email: value},
                        success: function (out) {
                            if (out.result) {
                                doubleEmailValid = false;
                            }
                            cacheDoubleEmail[value] = doubleEmailValid;
                        },
                        dataType: 'json',
                        async: false
                    });
                }


                $field.parents('form').find('.doubleemail-error').each(function () {
                    if ($(this).attr('data-nowtext') === undefined) {
                        $(this).attr('data-nowtext', $(this).text());
                    }
                    if (!doubleEmailValid) {
                        $(this).addClass('error');
                        $(this).text('Введенный email уже зарегистирован в системе');
                    }
                    else {
                        $(this).removeClass('error');
                        $(this).text($(this).attr('data-nowtext'));
                    }
                });
            }

            var valid = (!isRequired || value)
                && (!extLimit || field.files[0].name.indexOf('.' + extLimitList) >= 0)
                && (minLength <= 0 || minLength <= value.length)
                && (!isEmail || validator.isEmail(value) && doubleEmailValid)
                && (!maxFilesize || field.files[0].size < maxFilesizeVal)
                && (!validCheckbox || $field.parents('form').find('[name="' + $field.attr('name') + '"]:checked').length > 0)
                && (!validRadio || $field.parents('form').find('[name="' + $field.attr('name') + '"]:checked').length > 0);

            $field.data('validator.valid', valid);

            if (this.toggleClasses) {
                $field.toggleClass('is-invalid', !valid);
                $field.toggleClass('is-valid', valid);
            }
            return valid;
        },
            e.prototype.activate = function () {
                this.toggleClasses = !0
            }, e.prototype.validate = function () {
            var t = [],
                e = this;
            return e.$fields.each(function () {
                e.checkField(this) || t.push(this)
            }), {
                isValid: t.length < 1,
                invalidFields: t
            }
        }, t.FormValidator = e
    }(window), $(function () {
    "use strict";


    $("body").on("submit", ".js-form-validator", function (t) {


        var e = $(this),
            n = e.attr('name'),
            i = e.data("validator") || FormValidator(e);
        i.activate();
        var o = i.validate();

        o.isValid || t.preventDefault();

        if (e.attr('data-is_ajax') == 'y' && o.isValid) {
            t.preventDefault();

            var url = e.attr('action');
            var postdata = e.attr('data-add_params');
            if ((typeof postdata) == 'undefined') {
                postdata = '';
            }

            $.post(url, e.serialize() + postdata, function (out) {
                if (e.attr('data-is_onlycode') == 'y' && out.RESULT === 'error') {

                }
                else {
                    if (e.attr('data-is_order') == 'y') {
                        if (out.order.REDIRECT_URL !== undefined) {
                            document.location = out.order.REDIRECT_URL;
                        }
                    }
                    else if (e.attr('data-is_redirect') == 'y' && e.attr('data-is_json') == 'y') {
                        document.location = e.attr('data-redirect_url');
                        window.location.reload();
                    }
                    else {
                        e.html($(out).find('form[name="' + n + '"]').html());
                    }
                }
            }, (e.attr('data-is_json') == 'y') ? 'json' : 'html');
        }
    })
});

/*, $(function() {
    "use strict";
    var t = $(".js-subscribe-form"),
        e = FormValidator(t);
    t.on("submit", function(t) {
        t.preventDefault();
        var i = $(this),
            o = i.find("button[type=submit]"),
            a = i.find("input[name=email]");
        e.activate();
        var n = e.validate();
        if (n.isValid) {
            o.attr("disabled", !0);
            $.post(i.attr("action"), {
                email: a.val()
            }).done(function(t) {
                t.success || t.id ? (Popups.showMessage({
                    title: t.messageTitle || "Подписка оформлена",
                    text: t.message || "Спасибо за проявленный интерес. Теперь вы будет первым узнавать о всех новинках, акциях, конкурсах!"
                }), a.val("")) : Popups.showMessage({
                    title: t.messageTitle || "Ошибка",
                    text: t.message || "Попробуйте повторить попытку позднее.1"
                })
            }).fail(function() {
                Popups.showMessage({
                    title: data.messageTitle || "Ошибка",
                    text: data.message || "Попробуйте повторить попытку позднее.1"
                })
            }).always(function() {
                o.attr("disabled", !1)
            })
        }
    })
});*/
function SetCity() {
    $.ajax({
        method: "GET",
        url: "/?not_show_modal=Y",

    })
}


/*
$(function () {
  'use strict';

$('body').on('submit', '#forgot_bform', function (e) {
   // e.preventDefault();
    var $form = $(this);
	console.log(123);

    var validator = $form.data('validator') || FormValidator($form);
    validator.activate();

    var validation = validator.validate();
    var $msg = $form.find('.form-error');
    $msg.text('').hide();
	console.log(validation.isValid);
    if (!validation.isValid) {
      if (validation.invalidFields) {
        var text = 'Не верный формат адреса электронной почты';
        validation.invalidFields.forEach(function (item, i, arr) {

          $msg.text(text);
          $msg.show();
        });
      }
      return false;
    }

	$.post( "/ajax/check_email.php", { EMAIL:$form.find('#pass_restore_email').val() })
	.done(function( data ) {
		var result = JSON.parse ( data );
		if(result["success"]!="Y"){
			 var text = 'E-mail не найден в нашей базе клиентов';
			 $msg.text(text);
			$msg.show();
			return false;
		}else{
			console.log("submit");
			return true;
		}


	});
	return false;

    // TODO: запрос на сервер (см. пример в subscribe-form.js)
  });
  });*/
$(function () {
    $('#fastreg').submit(function () {
        var response = $('#fastreg').find(".g-recaptcha-response").val();
        if (response.length > 1) {
            $('#capBorder div').css("border", "0");
            return true;
        }
        else {
            $('#capBorder div').css("border", "1px solid red");
            return false;
        }

    });
});


