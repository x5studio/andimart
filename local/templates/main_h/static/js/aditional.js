$(function () {

    var e = $('#ORDER_FORM');
    var n = e.attr('name'); 
    var url = e.attr('action');
//island-head-tabs__link

    uppOrderList = function()
    {
	var data = e.serialize();
	e.html('<div class="ajax-preloader"><i class="ajax-preloader__spinner"></i></div>');
	console.log("data", data);
	$.post(url, data, function(out)
	{
	    e.html($(out).find('form[name="'+n+'"]').html()); 
	    
	    var t = function(t) {return function(e, i) {var o, a;o = [], a = new RegExp(e, "i"), $.each(t, function(t, e) {a.test(e) && o.push(e)}), i(o)}};
	    e.find(".js-city-input").one("focus", function() {
                var e, i = $(this),o = i.data("cities-src");
                
                var bestPictures = new Bloodhound({  datumTokenizer: Bloodhound.tokenizers.whitespace,  queryTokenizer: Bloodhound.tokenizers.whitespace,  remote: {
                        url: o+'?q=%QUERY',    wildcard: '%QUERY'  }});
                
                 i.typeahead({hint: !0,highlight: !0,minLength: 1}, {name: "cities",source: bestPictures}), i.data("cities", e), i.focus(), 
                    i.on("typeahead:change", function(event, city_name) {}),
                    i.on("typeahead:select", function(event, city_name) {console.log(bestPictures);if (i.attr('data-key')){for (var z in bestPictures.remote.transport._cache.list.head.val){if (bestPictures.remote.transport._cache.list.head.val[z] == city_name){$(i.attr('data-key')).val(z);$(i.attr('data-key')).change();break;}}}i.change()}), i.on("typeahead:change", function(eee) {i.change()})});
	    
	    e.find(".js-pickup").on("change", function(t) {var e = $(this);e.find(".js-pickup__content").animate({height: "hide"});var i = e.find(".js-pickup__radio:checked"),o = i.closest(".js-pickup__item"),a = o.find(".js-pickup__content"),n = a.find(".js-pickup__map");if (a.stop().animate({height: i.is(":checked") ? "show" : "hide"}, 300), n.hasClass("is-loaded")) n.data("map").container.fitToViewport();else {n.addClass("is-loaded");var s, r = n.data("center") || [44.059654, 43.034953],c = n.data("zoom") || 16,l = n.data("marker"),u = n.data("marker-image");YamapsService.setApiParams({lang: "ru_RU"}), YamapsService.run(function() {if (s = new ymaps.Map(n[0], {center: r,zoom: c,controls: ["zoomControl"],autoFitToViewport: "always"}), n.find(".js-map-preloader").remove(), l && l.latlng) {var t = new ymaps.Placemark(l.latlng, {}, {iconLayout: "default#image",iconImageHref: u,iconImageSize: [44, 54],iconImageOffset: [-22, -54]});s.geoObjects.add(t)}YamapsService.fixScroll(s, {hoverDelay: 1e3}), n.data("map", s)})}});
	    e.find(".js-pickup__radio:first").click();
//	    js-pickup__radio
	}, 
	'html');
    }
    
    e.on('change', '#checkout_pickup_city, ._refresh_after_change', function()
    {
	uppOrderList();
    });
	    
    $('.island-head-tabs__link').on('click', function()
    {
	var $point = $(this);
	if (!$point.hasClass('is-active'))
	{
	    $('.island-head-tabs__link').removeClass('is-active');
	    $point.addClass('is-active');
	    var deliveryID = $point.attr('data-delivery');	    
	    e.find('input[name="DELIVERY_ID"]').val(deliveryID);
            
            if (e.find('#change_city').length > 0)
            {
                e.find('input[name="ORDER_PROP_1"]').val(e.find('#change_city').val());
            }
            
	    uppOrderList();
	}
    });
    
    
    $(document).on('change', '[name="PAY_SYSTEM_ID"]', function()
    {
        if ($(this).val() == 2)
        {
            $('button.btn.btn--fluid.btn--xl').text('Оформить заказ');
        }
        else
        {
            $('button.btn.btn--fluid.btn--xl').text('Оплатить заказ');
        }
    });
});
$(function () {

        

	$(document).on('click.add_to_cart_section', ".add_to_cart_section", function(){
//		console.log(123);
		var quantity = 1;
		quantity = $(this).parent().parent().find("input.input-text").val();
		var url = '/ajax/add2basket.php' + $(this).attr("rel").substr($(this).attr("rel").indexOf('?'))+"&quantity="+quantity;
		//console.log('нажатие add_to_cart');
//		console.log(url);
		$.ajax({
		  url: url,
		}).done(function(data) {
			$(".header__elem_basket").html(data);
			//console.log('событие add_to_cart.on_done');
//			alert("тут надо вывести модалку. Количество товаров в корзине обновлено");
		});
	});

	$(".change_sort_select").change(function(){
		$(this).parent().submit();
	});
	/*$('body').on('submit', '.js-form-validator1', function (e) {
        var $form = $(this);
	console.log(1);
	return false;
        // Для корректной работы с загруженными по аякс формами
        var validator = $form.data('validator') || FormValidator($form);
        validator.activate();

        var validation = validator.validate();
        if (!validation.isValid) {
            e.preventDefault();
           // console.log(validation.invalidFields);
        }else{
            var msg   = $form.serialize();
            $.ajax({
                type: 'POST',
                url: '',
                data: msg,
                success: function(data) {
                    console.log(data);
                   
                },
                error:  function(xhr, str){
                  conosle.log("error");
                }
            });
            return false;
        }

    });*/

	
	 $('body').on('submit', '#forgot_bform', function (e) {
        var $form = $(this);

        // Для корректной работы с загруженными по аякс формами
        var validator = $form.data('validator') || FormValidator($form);
        validator.activate();

        var validation = validator.validate();
        if (!validation.isValid) {
            e.preventDefault();
           console.log(validation.invalidFields);
        }else{
            var msg   = $form.serialize();
            $.ajax({
                type: 'POST',
                url: '',
                data: msg,
                success: function(data) {
                    console.log(data);
                    if(data=="OK"){
                        Popups.showMessage({
                            title: "Отзыв успешно отправлен.",
                            text: "Спасибо за ваш отзыв!"
                        });
                        setTimeout(location.reload(),5000);
                    }
                    return false;
                },
                error:  function(xhr, str){
                  conosle.log("error");
                }
            });
            return false;
        }

    });

	
	
	
	
	
	
	
	
    $('body').on('submit', '#review-form', function (e) {
        var $form = $(this);

        // Для корректной работы с загруженными по аякс формами
        var validator = $form.data('validator') || FormValidator($form);
        validator.activate();

        var validation = validator.validate();
        if (!validation.isValid) {
            e.preventDefault();
           // console.log(validation.invalidFields);
        }else{
            var msg   = $form.serialize();
            $.ajax({
                type: 'POST',
                url: $form.attr('action'),
                data: msg,
                success: function(data) {
                    console.log(data);
                    if(data=="OK"){
                        Popups.showMessage({
                            title: "Отзыв успешно отправлен.",
                            text: "Спасибо за ваш отзыв!"
                        });
                        $('#review-form').find('textarea, input[type="text"], input[type="email"], input[type="number"]').val('');

                        setTimeout(location.replace(location.href),5000);
                        //console.log(location.href);
                    }
                    return false;
                },
                error:  function(xhr, str){
                  conosle.log("error");
                }
            });
            return false;
        }

    });


    $('body').on('submit', '#ask-form', function (e) {
        var $form = $(this);

        // Для корректной работы с загруженными по аякс формами
        var validator = $form.data('validator') || FormValidator($form);
        validator.activate();

        var validation = validator.validate();
        if (!validation.isValid) {
            e.preventDefault();
            // console.log(validation.invalidFields);
        }else{
            var msg   = $form.serialize();
            $.ajax({
                type: 'POST',
                url: $form.attr('action'),
                data: msg,
                success: function(data) {
                    console.log(data);
                    if(data=="OK"){
                        Popups.showMessage({
                            title: "Вопрос успешно отправлен.",
                            text: "Спасибо. Ваш вопрос отправлен на модерацию."
                        });
                        
                        $('#ask-form').find('textarea, input[type="text"], input[type="email"], input[type="number"]').val('');
                    }
                    return false;
                },
                error:  function(xhr, str){
                    conosle.log("error");
                }
            });
            return false;
        }

    });
});

/*
$(document).ready(function(){
    $(".js-city-select__input").change(function(){
        console.log($(this).val());
        if($(this).val().length>1){
            $.get( "/ajax/cities.php", { search: $(this).val() } )
                .done(function( data ) {

                    var html = "";
                    var cities = JSON.parse ( data );
                    cities.forEach(function(item, i, arr) {
                        html+='<div class="tt-suggestion tt-selectable"><a href="/?set_city='+item["ID"]+'">'+item["NAME"]+'</a></div>';
                    });
                    $(".tt-dataset-cities").html(html);
                });
        }
    });
});*/



$(document).ready(function()
{
    $('.search-form__input').keyup(function()
    {
        $('.search-form__input').not($(this)).val($(this).val());
    });
    
    
    var search_res = {};
    
//$( ".selector" ).autocomplete( "close" );
    
    $('.search-form__input').autocomplete({
		//alert(111);
        minLength: 3,
        source: function (request, response) {
              $.get("/catalog/search/", {
                  q: request.term,
                  is_ajax: 'y'
              }, function (data) {
                  
                var new_data = [];
                for (var i in data)
                {
                    search_res[data[i].label] = data[i].value;                    
                    new_data[new_data.length] = data[i].label;
                }
                  response(new_data);
              }, 'json');
        },
        select: function (event, ui) {
			if (typeof search_res[ui.item.value]!="undefined")
            	document.location = search_res[ui.item.value];
//            console.log(0); 
            return ui.item.label;
        },
        open: function (e, ui) {
		    var acData = $(this).data('ui-autocomplete');
		    acData
		    .menu
		    .element
		    .find('li div:contains("Разделы")')
		    .each(function () {
		    	    var styles = {
				      backgroundColor : "#eee",
				      fontWeight: "bold"
				    };
		        $(this).css(styles);$(this).addClass("nohover");
		     });
		     acData
		    .menu
		    .element
		    .find('li div:contains("Товары")')
		    .each(function () {
		    	    var styles = {
				      backgroundColor : "#eee",
				      fontWeight: "bold"
				    };
		        $(this).css(styles);$(this).addClass("nohover");
		     });
		     
		     
		    acData
		    .menu
		    .element
		    .find('li div')
		    .each(function () {
		        var me = $(this);
		        var keywords = acData.term.split(' ').join('|');
		        me.html(me.text().replace(new RegExp("(" + keywords + ")", "gi"), '<b>$1</b>'));
		     });
		     
		     acData
		    .menu
		    .element
		    .find('li div:not(:has(b))')
		    .each(function () {
		        var me = $(this);
		        var keywords = acData.term.split(' ').join('|');
		        me.html(me.text().replace(new RegExp("(" + keywords.substring(0, keywords.length-1) + ")", "gi"), '<b>$1</b>'));
		     });
		 },
//        focus: function (event, ui) {
//            return ui.item.label;
//        },
//        change: function (event, ui) {
                     
//            console.log(2);
//            return ui.item.label;
//        },
//        response: function (event, ui) {
//                       console.log(event); 
//                       console.log(ui); 
                  
//            console.log(3);
//            return ui.item.label;
//        },
    });
    
    $(window).scroll(function()
    {
        $( ".search-form__input" ).autocomplete( "close" );
    });
    
    $(document).on('click', '.pagenavigation_ajax a', function()
    {
        var $that = $(this);
        var $main_block = $that.parents('.pagenavigation_ajax_mainblock');
        
        if ($main_block.length > 0)
        {
            var url = $that.attr('href');
            $.get(url, {}, function(html)
            {
                $main_block.replaceWith(html);
            });
            
            return false;
        }
    });
    
    $('#master_city, #master_speciality').change(function()
    {
        var data = 'ajax_mode=y&'+$('#filter-block').serialize();
	$('#user_inner_list').html('<div class="ajax-preloader"><i class="ajax-preloader__spinner"></i></div>');
	$.get(document.location.pathname, data, function(html)
	{
            var added = false;
	    var new_url = document.location.pathname;
	    
	    if ($('#master_city').serialize().substr(-1) != '=')
	    {
                new_url += (!added?'?':'&')+$('#master_city').serialize();
		added = true;
	    }
            if ($('#master_speciality').serialize().substr(-1) != '=')
	    {
                new_url += (!added?'?':'&')+$('#master_speciality').serialize();
		added = true;
	    }
	    window.history.pushState(null, null, new_url);
	    
	    $('#user_inner_list').html(html);
	})
    });
    $('#user_inner_list').on('click', '.pagination__link', function(e)
    {
	e.preventDefault;
	var href = $(this).attr('href');
	$('#user_inner_list').html('<div class="ajax-preloader"><i class="ajax-preloader__spinner"></i></div>');
	$.get(href, {
	    ajax_mode:'y',
	    city:$('#master_city').val(),
	    brand:$('#master_speciality').val()
	}, function(html)
	{
	    window.history.pushState(null, null, href);

	    $('#user_inner_list').html(html);
	})
	return false;
    });



});

var removePortfolioWork = function(id)
{
    var $parentBlock = $('.portfolio_i_' + id).parent();
    $('.portfolio_i_' + id).remove();
    
    if ($parentBlock.children().length == 0)
    {
        $parentBlock.parents('div.row').remove();
    }
    
    $.post('/ajax/removePortfolioWork.php', {'id': id}, function(e){});
}


/* compare add ajax */
$(document).on('click', '.sky-compare-btn', function(e) {
    e.preventDefault();

    let id = $(this).attr('data-id');
    if( $(this).hasClass('active')){
        $.get("/",
            { 
                action: "DELETE_FROM_COMPARE_LIST", id: id },
                function(data) {
                     $(".skyCompareCounter").html($(data).find(".skyCompareCounter").html());
                }
        );
        $(this).removeClass('active');
    }else{
        $.get("/",
            { 
                action: "ADD_TO_COMPARE_LIST", id: id },
                function(data) {
                     $(".skyCompareCounter").html($(data).find(".skyCompareCounter").html());
                }
        );
        $(this).addClass('active');
    }
});

