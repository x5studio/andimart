<?php
$page = $APPLICATION->GetCurPage();
$page_vars = explode('/', $page);

if (count($page_vars) == 3)
{

	$arResult['USERS_FIELDS'] = array();
	$arResult['USERS_FIELDS']['UF_CITY_NEW'] = array();
	$arResult['USERS_FIELDS']['UF_ADD_CITIES_NEW'] = array();
	$rsEnum = CIBlockElement::GetList(array('name' => 'asc'), array('IBLOCK_ID' => const_IBLOCK_ID_cities, 'ACTIVE' => 'Y'), false, false, array('ID', 'NAME'));
	while ($arEnum = $rsEnum->Fetch())
	{
		$arResult['USERS_FIELDS']['UF_CITY_NEW'][$arEnum['ID']] = $arEnum;
		$arResult['USERS_FIELDS']['UF_ADD_CITIES_NEW'][$arEnum['ID']] = $arEnum;
	}
	$arResult['USERS_FIELDS']['UF_SPECIALS'] = array();
	$rsEnum = CIBlockSection::GetList(array('name' => 'asc'), array('IBLOCK_ID' => const_IBLOCK_ID_catalog, 'ACTIVE' => 'Y', 'DEPTH_LEVEL' => 1), false, array('ID', 'NAME', 'CODE'));
	while ($arEnum = $rsEnum->GetNext())
	{
		$arResult['USERS_FIELDS']['UF_SPECIALS'][$arEnum['ID']] = $arEnum;
	}
	$arResult['USERS_FIELDS']['UF_BRAND'] = array();
	$rsEnum = CIBlockElement::GetList(array('name' => 'asc'), array('IBLOCK_ID' => const_IBLOCK_ID_brands, 'ACTIVE' => 'Y'), false, false, array('ID', 'NAME', 'CODE'));
	while ($arEnum = $rsEnum->GetNext())
	{
		$arResult['USERS_FIELDS']['UF_BRAND'][$arEnum['ID']] = $arEnum;
	}
	
	$brands = array();
	$arResult['FILTER']['UF_BRAND'] = array();
	$us = CUser::GetList(($by = "personal_country"), ($order = "desc"), array('ACTIVE' => 'Y', 'GROUPS_ID' => 9, '!UF_BRAND' => false), array('SELECT' => array('UF_BRAND')));
	while ($u = $us->Fetch())
	{
	    $brands = array_merge($brands, $u['UF_BRAND']);
	}
	$brands = array_unique($brands);
	if (!empty($brands))
	{
	    foreach ($brands as $brand)
	    {
		if (empty($arResult['USERS_FIELDS']['UF_BRAND'][$brand])) continue;
		$arResult['FILTER']['UF_BRAND'][$arResult['USERS_FIELDS']['UF_BRAND'][$brand]['ID']] = $arResult['USERS_FIELDS']['UF_BRAND'][$brand]['NAME'];
	    }
	}
	
	$cities = array();
	$arResult['FILTER']['CITIES'] = array();
	$us = CUser::GetList(($by = "personal_country"), ($order = "desc"), array('ACTIVE' => 'Y', 'GROUPS_ID' => 9, array('LOGIC' => 'OR', '!UF_ADD_CITIES_NEW' => false, '!UF_CITY_NEW' => false)), array('SELECT' => array('UF_ADD_CITIES_NEW', 'UF_CITY_NEW')));
	while ($u = $us->Fetch())
	{
	    $cities = array_merge($cities, array($u['UF_CITY_NEW']), array($u['UF_ADD_CITIES_NEW']));
	}
	$cities = array_unique($cities);
	sort($cities);
	if (!empty($cities))
	{
	    foreach ($cities as $city)
	    {
		if ($city)
		$arResult['FILTER']['CITIES'][$city] = $city;
	    }
	}
	
	$arResult['FILTER_SELECT'] = array(
	    'UF_BRAND' => false,
	    'CITIES' => false,
	);
	
	if (!empty($_GET))
	{
	    if (!empty($_GET['city']))
	    {
			$city = htmlspecialchars($_GET['city']);
			if (!empty($arResult['FILTER']['CITIES'][$city]))
			{
			    $arResult['FILTER_SELECT']['CITIES'] = $city;
			}
	    } else {
	    	$arResult['FILTER_SELECT']['CITIES']=$_SESSION["PEK_CURRENT_CITY_NAME"];
	    }
	    if (!empty($_GET['brand']))
	    {
		$brand = htmlspecialchars($_GET['brand']);
		if (!empty($arResult['FILTER']['UF_BRAND'][$brand]))
		{
		    $arResult['FILTER_SELECT']['UF_BRAND'] = $brand;
		}
	    }
	}


	$arResult['USERS'] = array();
	$filter = array('ACTIVE' => 'Y', 'GROUPS_ID' => 9);
	foreach ($arResult['FILTER_SELECT'] as $filterCode => $filterVal)
	{
	    if (empty($filterVal)) continue;
	    if ($filterCode == 'CITIES')
	    {
		$filter[] = array('LOGIC' => 'OR', 'UF_ADD_CITIES_NEW' => $filterVal, 'UF_CITY_NEW' => $filterVal);
	    }
	    else
	    {
		$filter[$filterCode] = $filterVal;
	    }
	}
	
	$us = CUser::GetList(($by = "personal_country"), ($order = "desc"), $filter, array('SELECT' => array('UF_*'), 'FIELDS' => array('*'), 'NAV_PARAMS' => array('nPageSize' => 8)));
	while ($u = $us->Fetch())
	{
		$u["REVIEWS"]["COUNT"] = 0;
		$u["REVIEWS"]["SIGN"] = 0;
		$arSelect = Array("ID", "IBLOCK_ID", "NAME","XML_ID","PREVIEW_PICTURE","DETAIL_PAGE_URL","PROPERTY_SIGN");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
		$arFilter = Array("PROPERTY_US"=>$u["ID"],"IBLOCK_ID"=>5);
		$brands = array();
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($ob = $res->GetNextElement()){ 
			$arFields = $ob->GetFields();  
			//p($arFields);
			$u["REVIEWS"]["COUNT"]++;
			$u["REVIEWS"]["SIGN"]+=intval($arFields["PROPERTY_SIGN_VALUE"]);
		}
		if($u["REVIEWS"]["COUNT"]){
			$u["REVIEWS"]["SIGN_REAL"] = ceil($u["REVIEWS"]["SIGN"]/$u["REVIEWS"]["COUNT"]);
		}
		
		
		
		$arResult['USERS'][] = $u;
	}

	$specNum = array(
	    'vodosnabzhenie' => 1,
	    'kanalizatsiya' => 2,
	    'konditsionirovanie' => 3,
	    'santekhnika' => 6,
	    'otoplenie' => 4,
	    'bani' => 5,
	);
	?>
<title>Сервисные центры | интернет-магазин Эндимарт</title>

		<div class="content-block content-block--pull-top island">
            <div class="row b-list">
              <div class="col-12 col-sm-7 b-list__item">
                <h1 class="section-title title-line js-title-line">Сервисные центры</h1>
                <div class="text-small-expand">Осуществляем открытый набор сервисных центров и сервисных специалистов по всей России к регистрации в нашем электронном каталоге!</div>
              </div>
              <div class="col-12 col-sm-4 offset-sm-1 b-list__item">
                <h4 class="section-title title-line js-title-line section-title--sm title-line--accent">Вы - сервисный центр?</h4>
                <div class="text-small-expand">Расскажите клиентам о том, что вы делаете!<br>
                  <a href="/auth/">Вход для центра</a>&emsp;
                  <a href="/auth/">Регистрация</a>
                </div>
              </div>
            </div>
	<form class="content-block" id="filter-block">
	  <div class="row row--vcentr">
	  <div class="col-12 col-sm-5">
	  <div class="row row--vcentr">
	  <div class="col-12 col-sm-4">
	  <label class="input-label" for="master_city">Выберите город:</label>
	  </div>
	  <div class="col-12 col-sm-8">
	  <select id="master_city" class="select" name="city">
	    <option value="all" <? if (empty($arResult['FILTER_SELECT']['CITIES'])) { ?> selected<? } ?>>Все</option>
	    <? foreach ($arResult['FILTER']['CITIES'] as $cityID => $cityVal) { ?>
		<option value="<?=$cityID?>"<? if (!empty($arResult['FILTER_SELECT']['CITIES']) && $cityID == $arResult['FILTER_SELECT']['CITIES']) { ?> selected<? } ?>><?=$cityVal?></option>
	    <? } ?>
	  </select>
	  </div>
	  </div>
	  </div>
	  <div class="col-12 col-sm-6 offset-sm-1">
                  <div class="row row--vcentr">
                    <div class="col-12 col-sm-4">
                      <label class="input-label" for="master_speciality">Обслуживаемые&nbsp;бренды:</label>
                    </div>
                    <div class="col-12 col-sm-8">
                      <select id="master_speciality" class="select" name="brand">
			    <option value<? if (empty($arResult['FILTER_SELECT']['UF_BRAND'])) { ?> selected<? } ?>>Все</option>
			    <? foreach ($arResult['FILTER']['UF_BRAND'] as $brandID => $brandName) { ?>
				<option value="<?=$brandID?>"<? if (!empty($arResult['FILTER_SELECT']['UF_BRAND']) && $brandID == $arResult['FILTER_SELECT']['UF_BRAND']) { ?> selected<? } ?>><?=$brandName?></option>
			    <? } ?>
                      </select>
                    </div>
                  </div>
	  </div>
	  </div>
	  </form>
	    <div class="content-block content-block--push-top" id="user_inner_list">
		    <? if (!empty($_GET['ajax_mode']) && $_GET['ajax_mode'] == 'y') { $APPLICATION->RestartBuffer();}?>
		    
		    <ul class="workers grid grid--xs-2 grid--sm-4">
			
			<? foreach ($arResult['USERS'] as $arUser)
			{
				$brands_link="";
				foreach ($arUser['UF_BRAND'] as $bid) {
					$brands_link .= $arResult['USERS_FIELDS']['UF_BRAND'][$bid]['NAME']."-";
				}
				$link = Cutil::translit(strtolower($arUser['NAME']."-".$arUser['UF_CITY_NEW']."-".$brands_link).$arUser['ID'],"ru",array("replace_space"=>"-","replace_other"=>"-"));
				?>
				<li class="workers__item grid__item">
				    <div class="workers__item-in">
					<div class="workers__content">
					    <? if (!empty($arUser['PERSONAL_PHOTO']))
					    {
						    ?>
						    <div class="workers__photo">
							<a class="link-hidden" href="/service-centers/<?= $link ?>/" style="background-image: url(<?= CFile::GetPath($arUser['PERSONAL_PHOTO']) ?>);">
							    
							</a>
						    </div>
					    <?
					    }
					    else
					    {
						    ?>
						    <div class="workers__photo">
							<a class="link-hidden" href="/service-centers/<?= $link ?>/" style="background-image: url(<?= SITE_TEMPLATE_PATH ?>/static/images/upload/master-photo-sm.jpg);">
							    
							</a>
						    </div>
					    <? } ?>
					    <div class="workers__name">
						<a class="link-hidden" href="/service-centers/<?= $link ?>/"><?= $arUser['NAME'] ?></a>
					    </div>
				    <? if (!empty($arUser['UF_CITY_NEW'])) { ?>
				    <div class="workers__city">г. <?= $arUser['UF_CITY_NEW'] ?></div>
				    <? } ?>

					    <? if (!empty($arUser['UF_BRAND'])) { ?>
						<div class="workers__spec">
						  <div class="workers__spec-h">Обслуживаемые бренды:</div>
						  <div class="text-bold"><? foreach ($arUser['UF_BRAND'] as $kbrandID => $brandID) { ?><?if (!empty($kbrandID)){?>, <? } ?><?=$arResult['USERS_FIELDS']['UF_BRAND'][$brandID]['NAME']?><? } ?></div>
						</div>
					    <? } ?>
					</div>
					<div class="workers__footer">
					    <div class="workers__footer-item">
						<div class="stars stars--sm" data-rating="<?=$arUser["REVIEWS"]["SIGN_REAL"]?>">
						    <div class="stars__list"><i class="stars__star"></i><i class="stars__star"></i><i class="stars__star"></i><i class="stars__star"></i><i class="stars__star"></i></div>
						</div>
					    </div>
					    <div class="workers__footer-item">
						<a class="btn btn--sm btn--inverse" href="/service-centers/<?= $link ?>/">Подробнее</a>
					    </div>
					</div>
				    </div>
				</li>
		    <? } ?>
		    </ul>
	    
		    <?=$us->GetPageNavString(
				'',
				'forum',
				'N',
				false
			);?>
		    <? if (!empty($_GET['ajax_mode']) && $_GET['ajax_mode'] == 'y') { exit;}?>
	    </div>
	</div>
	<?
}
elseif (count($page_vars) == 4)//страница СЦ
{
	$master_id = array_pop(explode("-",$page_vars[2]));

	$arResult['USERS_FIELDS'] = array();
	$arResult['USERS_FIELDS']['UF_CITY_NEW'] = array();
	$arResult['USERS_FIELDS']['UF_ADD_CITIES_NEW'] = array();
	$rsEnum = CIBlockElement::GetList(array('name' => 'asc'), array('IBLOCK_ID' => const_IBLOCK_ID_cities, 'ACTIVE' => 'Y'), false, false, array('ID', 'NAME'));
	while ($arEnum = $rsEnum->Fetch())
	{
		$arResult['USERS_FIELDS']['UF_CITY_NEW'][$arEnum['ID']] = $arEnum;
		$arResult['USERS_FIELDS']['UF_ADD_CITIES_NEW'][$arEnum['ID']] = $arEnum;
	}
	$arResult['USERS_FIELDS']['UF_SPECIALS'] = array();
	$rsEnum = CIBlockSection::GetList(array('name' => 'asc'), array('IBLOCK_ID' => const_IBLOCK_ID_catalog, 'ACTIVE' => 'Y', 'DEPTH_LEVEL' => 1), false, array('ID', 'NAME', 'CODE'));
	while ($arEnum = $rsEnum->GetNext())
	{
		$arResult['USERS_FIELDS']['UF_SPECIALS'][$arEnum['ID']] = $arEnum;
	}
	$arResult['USERS_FIELDS']['UF_BRAND'] = array();
	$rsEnum = CIBlockElement::GetList(array('name' => 'asc'), array('IBLOCK_ID' => const_IBLOCK_ID_brands, 'ACTIVE' => 'Y'), false, false, array('ID', 'NAME', 'CODE'));
	while ($arEnum = $rsEnum->GetNext())
	{
		$arResult['USERS_FIELDS']['UF_BRAND'][$arEnum['ID']] = $arEnum;
	}


	$arResult['USER'] = array();
	$us = CUser::GetList(($by = "personal_country"), ($order = "desc"), array('ACTIVE' => 'Y', 'GROUPS_ID' => 9, 'ID' => $master_id), array('SELECT' => array('UF_*'), 'FIELDS' => array('*')));
	$arResult['USER'] = $us->Fetch();
	if (!$arResult['USER']) {
		//ERROR 404
		define("ERROR_404","Y");
	}
	$specNum = array(
	    'vodosnabzhenie' => 1,
	    'kanalizatsiya' => 2,
	    'konditsionirovanie' => 3,
	    'santekhnika' => 6,
	    'otoplenie' => 4,
	    'bani' => 5,
	);

	if (!empty($arResult['USER']))
	{
		$arResult["REVIEWS"]["COUNT"] = 0;
		$arResult["REVIEWS"]["SIGN"] = 0;
		$arSelect = Array("ID", "IBLOCK_ID", "NAME","XML_ID","PREVIEW_PICTURE","DETAIL_PAGE_URL","PROPERTY_SIGN");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
		$arFilter = Array("PROPERTY_US"=>$arResult['USER']["ID"],"IBLOCK_ID"=>5);
		$brands = array();
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($ob = $res->GetNextElement()){ 
			$arFields = $ob->GetFields();  
			//p($arFields);
			$arResult["REVIEWS"]["COUNT"]++;
			$arResult["REVIEWS"]["SIGN"]+=intval($arFields["PROPERTY_SIGN_VALUE"]);
		}
		if($arResult["REVIEWS"]["COUNT"]){
			$arResult["REVIEWS"]["SIGN_REAL"] = ceil($arResult["REVIEWS"]["SIGN"]/$arResult["REVIEWS"]["COUNT"]);
		}
		
		$APPLICATION->AddChainItem($arResult['USER']['NAME'], '/service-centers/'.$arResult['USER']['ID'].'/');
		?>
		<div class="content-block content-block--pull-top island">
		    <div class="worker-card">
			<div class="row">
			    <div class="col-12 col-sm-6">
				<div class="worker-card__img_in">
				<?if (!empty($arResult['USER']['PERSONAL_PHOTO'])) { ?>
					<div class="worker-card__img" style="background-image: url(<?= CFile::GetPath($arResult['USER']['PERSONAL_PHOTO']) ?>);">
					    
					</div>
				<? } else { ?>
					<div class="worker-card__img" style="background-image: url(<?= SITE_TEMPLATE_PATH ?>/static/images/upload/service-center-photo.jpg);">
					    
					</div>
				<? } ?>
				</div>
			    </div>
			    <div class="col-12 col-sm-6">
				<h1 class="worker-card__title title-line js-title-line"><?= $arResult['USER']['NAME'] ?></h1>
				<div class="worker-card__block">
				    
				    <? if (!empty($arResult['USER']['UF_CITY_NEW'])) { ?>
				    <div class="worker-card__row">Город: <?= $arResult['USER']['UF_CITY_NEW']?></div>
				    <? } ?>
					<div class="worker-card__row">Обслуживаемые бренды: <?
						$showOne = false;
						foreach ($arResult['USER']['UF_BRAND'] as $specID)
						{
							if (!empty($arResult['USERS_FIELDS']['UF_BRAND'][$specID]))
							{
								$spec = $arResult['USERS_FIELDS']['UF_BRAND'][$specID];
								if ($showOne)
								{
									?>,<? } ?>
								<?= toLower($spec['NAME']) ?><?
								$showOne = true;
							}
						}
					?></div>
					
				</div>
				<div class="worker-card__block">
				    <div class="reviews-widget">
					<div class="reviews-widget__stars">
					    <div class="stars" data-rating="<?=$arResult["REVIEWS"]["SIGN_REAL"]?>">
						<div class="stars__list"><i class="stars__star"></i><i class="stars__star"></i><i class="stars__star"></i><i class="stars__star"></i><i class="stars__star"></i></div>
					    </div>
					</div>
					<? if (!empty($arResult["REVIEWS"]["COUNT"])) { ?>
						<a href="#review-block" class="reviews-widget__link link link--gray js-scroll-to">
						  <span>Читать <?=$arResult["REVIEWS"]["COUNT"]?> <?=getNumEnding($arResult["REVIEWS"]["COUNT"], array('отзыв','отзыва','отзывов'))?></span>
						</a>
					<? } ?>
					<a href="#review-block" class="reviews-widget__add link link--gray js-scroll-to">
					    <span>Оставить отзыв</span>
					</a>
				    </div>
				</div>


		<? 
			?>
					<div class="worker-card__block">
					    <div class="worker-card__row">Телефон:
						<a class="link-hidden" href="tel:<?= $arResult['USER']['PERSONAL_PHONE'] ?>"><?= $arResult['USER']['PERSONAL_PHONE'] ?></a>
					    </div>
					    <div class="worker-card__row">E-mail:
						<a class="link link--gray" href="mailto:<?= $arResult['USER']['EMAIL'] ?>"><?= $arResult['USER']['EMAIL'] ?></a>
					    </div>
					    <? if (!empty($arResult['USER']['WORK_NOTES'])) { ?>
					    <div class="worker-card__row">Режим работы: <?=$arResult['USER']['WORK_NOTES']?></div>
					    <? } ?>
					</div>

			<?
			if (!empty($arResult['USER']['UF_IN_YT']) 
				|| !empty($arResult['USER']['UF_IN_IN']) 
				|| !empty($arResult['USER']['UF_IN_FB']) || 
				!empty($arResult['USER']['UF_IN_OK']) || 
				!empty($arResult['USER']['UF_IN_TW']) || 
				!empty($arResult['USER']['UF_IN_VK']))
			{
				?>
						<div class="worker-card__block">
						    <div class="soc-contacts">
							<div class="soc-contacts__label">В соцсетях:</div>
							<div class="soc-contacts__list">
							    <!-- У youtube и tw модификатор на размер иконки-->

							    <? if (!empty($arResult['USER']['UF_IN_YT']))
							    {
								    ?>
								    <a class="soc-contacts__item" href="<?= $arResult['USER']['UF_IN_YT'] ?>">
									<svg class="soc-contacts__item-i soc-contacts__item-i--big">
									<use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#youtube"></use>
									</svg>
								    </a>
							    <? } ?>
							    <? if (!empty($arResult['USER']['UF_IN_IN']))
							    {
								    ?>
								    <a class="soc-contacts__item" href="<?= $arResult['USER']['UF_IN_IN'] ?>">
									<svg class="soc-contacts__item-i">
									<use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#insta"></use>
									</svg>
								    </a>
							    <? } ?>
							    <? if (!empty($arResult['USER']['UF_IN_FB']))
							    {
								    ?>
								    <a class="soc-contacts__item" href="<?= $arResult['USER']['UF_IN_FB'] ?>">
									<svg class="soc-contacts__item-i">
									<use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#fb"></use>
									</svg>
								    </a>
							    <? } ?>
				<? if (!empty($arResult['USER']['UF_IN_OK']))
				{
					?>
								    <a class="soc-contacts__item" href="<?= $arResult['USER']['UF_IN_OK'] ?>">
									<svg class="soc-contacts__item-i">
									<use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#ok"></use>
									</svg>
								    </a>
				<? } ?>
				<? if (!empty($arResult['USER']['UF_IN_TW']))
				{
					?>
								    <a class="soc-contacts__item" href="<?= $arResult['USER']['UF_IN_TW'] ?>">
									<svg class="soc-contacts__item-i soc-contacts__item-i--big">
									<use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#tw"></use>
									</svg>
								    </a>
						<? } ?>
						<? if (!empty($arResult['USER']['UF_IN_VK']))
						{
							?>
								    <a class="soc-contacts__item" href="<?= $arResult['USER']['UF_IN_VK'] ?>">
									<svg class="soc-contacts__item-i">
									<use xlink:href="<?= SITE_TEMPLATE_PATH ?>/static/images/icons/social.svg#vk"></use>
									</svg>
								    </a>
							<? } ?>
							</div>
						    </div>
						</div>
						<? } ?>
						<? if (!empty($arResult['USER']['UF_ADD_CITIES_NEW']))
						{ ?>
						<div class="worker-card__block">
						    <div class="worker-card__row">Выезд: <?
						$showOne = false;
						foreach ($arResult['USER']['UF_ADD_CITIES_NEW'] as $specID)
						{
//							if (!empty($arResult['USERS_FIELDS']['UF_ADD_CITIES_NEW'][$specID]))
							{
//								$spec = $arResult['USERS_FIELDS']['UF_ADD_CITIES_NEW'][$specID];
								if ($showOne) {?>,<? } ?>
						<?= ($specID) ?><?
						$showOne = true;
					}
				}
				?></div>
						</div>
			<? } ?>
		<? ?>
			    </div>
			</div>
		    </div>
		    
		    
		</div>

		<?
		global $arPortfolioFilter;
		$arPortfolioFilter = array(
			'PROPERTY_USER' => $arResult['USER']['ID']
		);
		$APPLICATION->IncludeComponent(
			"bitrix:news.list",
			"portfolio-show2",
			Array(
				'USER_ID' => $arResult['USER']['ID'],
			    
				"ACTIVE_DATE_FORMAT" => "d.m.Y",
				"ADD_SECTIONS_CHAIN" => "N",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "N",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"CACHE_TIME" => "36000000",
				"CACHE_TYPE" => "A",
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"DISPLAY_TOP_PAGER" => "N",
				"FIELD_CODE" => array("", ""),
				"FILTER_NAME" => "arPortfolioFilter",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"IBLOCK_ID" => "30",
				"IBLOCK_TYPE" => "content",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"INCLUDE_SUBSECTIONS" => "N",
				"MESSAGE_404" => "",
				"NEWS_COUNT" => "500",
				"PAGER_BASE_LINK_ENABLE" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_TEMPLATE" => ".default",
				"PAGER_TITLE" => "Новости",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"PREVIEW_TRUNCATE_LEN" => "",
				"PROPERTY_CODE" => array("VIDEO", ""),
				"SET_BROWSER_TITLE" => "N",
				"SET_LAST_MODIFIED" => "N",
				"SET_META_DESCRIPTION" => "N",
				"SET_META_KEYWORDS" => "N",
				"SET_STATUS_404" => "N",
				"SET_TITLE" => "N",
				"SHOW_404" => "N",
				"SORT_BY1" => "ID",
				"SORT_BY2" => "SORT",
				"SORT_ORDER1" => "ASC",
				"SORT_ORDER2" => "ASC",
				"STRICT_SECTION_CHECK" => "N"
			)
		);?>
		

		<div class="content-block content-block--pull-top island" id="review-block">
		    <h2 class="section-title title-line js-title-line is-animated">Отзывы</h2>
		<?

		global $newsFilter;
		$newsFilter = array();
		$newsFilter["PROPERTY_ITEM"] = false; 
		$newsFilter["PROPERTY_US"] = $arResult['USER']['ID']; 

		$APPLICATION->IncludeComponent(
			"bitrix:news.list", 
			"reviews_users", 
			array(
				'USER_ID' => $arResult['USER']['ID'],
			    
				"ACTIVE_DATE_FORMAT" => "d.m.Y",
				"ADD_SECTIONS_CHAIN" => "Y",
				"AJAX_MODE" => "Y",
				"AJAX_OPTION_ADDITIONAL" => "",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"CACHE_TIME" => "36000000",
				"CACHE_TYPE" => "A",
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"DISPLAY_TOP_PAGER" => "N",
				"FIELD_CODE" => array(
					0 => "ID",
					1 => "NAME",
					2 => "SORT",
					3 => "PREVIEW_TEXT",
					4 => "PREVIEW_PICTURE",
					5 => "DATE_ACTIVE_FROM",
					6 => "DATE_CREATE",
					7 => "",
				),
				"FILTER_NAME" => "newsFilter",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"IBLOCK_ID" => "5",
				"IBLOCK_TYPE" => "news",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"INCLUDE_SUBSECTIONS" => "Y",
				"MESSAGE_404" => "",
				"NEWS_COUNT" => "10",
				"PAGER_BASE_LINK_ENABLE" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_TEMPLATE" => "forum",
				"PAGER_TITLE" => "",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"PREVIEW_TRUNCATE_LEN" => "",
				"PROPERTY_CODE" => array(
					0 => "CITY",
					1 => "SIGN",
					2 => "",
				),
				"SET_BROWSER_TITLE" => "N",
				"SET_LAST_MODIFIED" => "N",
				"SET_META_DESCRIPTION" => "N",
				"SET_META_KEYWORDS" => "N",
				"SET_STATUS_404" => "N",
				"SET_TITLE" => "N",
				"SHOW_404" => "N",
				"SORT_BY1" => "ACTIVE_FROM",
				"SORT_BY2" => "SORT",
				"SORT_ORDER1" => "DESC",
				"SORT_ORDER2" => "ASC",
				"COMPONENT_TEMPLATE" => "reviews"
			),
			false
		);?>

		<?$APPLICATION->IncludeComponent(
			"ruformat:iblock.element.add.form", 
			"reviews-product", 
			array(
				"USER_ID" => $arResult['USER']['ID'],
				"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
				"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
				"CUSTOM_TITLE_DETAIL_PICTURE" => "",
				"CUSTOM_TITLE_DETAIL_TEXT" => "",
				"CUSTOM_TITLE_IBLOCK_SECTION" => "",
				"CUSTOM_TITLE_NAME" => "Имя",
				"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
				"CUSTOM_TITLE_PREVIEW_TEXT" => "Отзыв",
				"CUSTOM_TITLE_TAGS" => "",
				"DEFAULT_INPUT_SIZE" => "30",
				"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
				"ELEMENT_ASSOC" => "CREATED_BY",
				"GROUPS" => array(
					0 => "1",
					1 => "2",
					2 => "3",
					3 => "4",
					4 => "5",
					5 => "6",
					6 => "7",
				),
				"IBLOCK_ID" => "5",
				"IBLOCK_TYPE" => "news",
				"LEVEL_LAST" => "Y",
				"LIST_URL" => "",
				"MAX_FILE_SIZE" => "0",
				"MAX_LEVELS" => "100000",
				"MAX_USER_ENTRIES" => "100000",
				"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
				"PROPERTY_CODES" => array(
					0 => "2",
					1 => "3",
					2 => "4",
		//			3 => "22",
					4 => "1026",
					5 => "NAME",
					6 => "PREVIEW_TEXT",
				),
				"PROPERTY_CODES_REQUIRED" => array(
				),
				"RESIZE_IMAGES" => "N",
				"SEF_MODE" => "N",
				"STATUS" => "ANY",
				"STATUS_NEW" => "NEW",
				"USER_MESSAGE_ADD" => "",
				"USER_MESSAGE_EDIT" => "",
				"USE_CAPTCHA" => "Y",
				"COMPONENT_TEMPLATE" => "reviews"
			),
			false
		);
		?>
</div>
	<? } ?>
<? } ?>