<?
global $USER;
//CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
?>
    <!DOCTYPE html>
    <html class="no-js" lang="ru-RU" prefix="og: http://ogp.me/ns#">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
        <meta name="format-detection" content="telephone=no">
        <meta name="format-detection" content="address=no">
        <meta name="msapplication-tap-highlight" content="no">
        <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
        <link href="<?= SITE_TEMPLATE_PATH ?>/static/favicons/apple-touch-icon.png" rel="apple-touch-icon"
              sizes="180x180">
        <link type="image/png" href="<?= SITE_TEMPLATE_PATH ?>/static/favicons/favicon-32x32.png" rel="icon"
              sizes="32x32">
        <link type="image/png" href="<?= SITE_TEMPLATE_PATH ?>/static/favicons/favicon-16x16.png" rel="icon"
              sizes="16x16">
        <link href="<?= SITE_TEMPLATE_PATH ?>/static/favicons/manifest.json" rel="manifest">
        <link href="<?= SITE_TEMPLATE_PATH ?>/static/favicons/favicon.ico" rel="shortcut icon">
        <meta name="msapplication-config" content="<?= SITE_TEMPLATE_PATH ?>/static/favicons/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">


        <? $APPLICATION->ShowHead() ?>
        <title><? $APPLICATION->ShowTitle() ?></title>
        <meta name="copyright" content>
        <meta property="og:type" content="website">
        <meta property="og:site_name" content>
        <meta property="og:url" content>
        <meta property="og:image" content>
        <meta property="og:title" content>
        <meta property="og:description" content>
        <link href="<?= SITE_TEMPLATE_PATH ?>/static/css/styles.css?v=1a06a750" rel="stylesheet">
        <link href="/local/templates/.default/css/custom.css" rel="stylesheet">
        <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/static/js/libs/jquery-3.1.0.min.js"); ?>
        <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/static/js/libs/modernizr.min.js"); ?>
        <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/static/js/libs/bobr.min.js"); ?>
        <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/static/js/libs/jquery.min.js"); ?>
        <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/static/js/plugins.js"); ?>


        <?php /* <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/plugins/doubletaptogo.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/plugins/inputmask.min.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/plugins/inputmask.phone.extensions.min.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/plugins/jquery.documentsize.min.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/plugins/jquery.inputmask.min.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/plugins/jquery.magnific-popup.min.js"></script>
	  <!--<script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/plugins/jquery.waypoints.inview.min.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/plugins/jquery.waypoints.min.js"></script>-->
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/plugins/js.cookie.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/plugins/jump.min.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/plugins/nouislider.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/plugins/perfect-scrollbar.jquery.min.js"></script>
	  <!--<script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/plugins/slick.js"></script>-->
	  <!-- <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/plugins/stickyfill.js"></script>-->
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/plugins/svg4everybody.min.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/plugins/Tabit.min.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/plugins/typeahead.jquery.min.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/plugins/validator.min.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/plugins/wNumb.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/plugins/yamaps.service.js"></script> */ ?>

        <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/static/js/app.js"); ?>
        <? /* <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/app/forms/ask-form.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/app/forms/city-input.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/app/forms/city-select-form.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/app/forms/form-validator.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/app/forms/subscribe-form.js"></script>

	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/app/carousels.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/app/checkout.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/app/carousels.js"></script>
	  <!--  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/app/common.js"></script>-->
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/app/contacts.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/app/filters.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/app/header.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/app/popups.js"></script>
	  <!--<script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/app/product.js"></script>-->
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/app/profile.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/app/tabs.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/app/utils.js"></script>
	  <script src="<?=SITE_TEMPLATE_PATH?>/src/scripts/app/video-player.js"></script>
	 */ ?>
        <link href="<?= SITE_TEMPLATE_PATH ?>/static/css/jquery-ui.min.css" rel="stylesheet">

        <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/static/js/libs/jquery-ui.min.js"); ?>
        <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/static/js/aditional.js"); ?>
    </head>
<body>
<? if ($USER->IsAdmin() || in_array(10, $USER->GetUserGroupArray()) || in_array(11, $USER->GetUserGroupArray())) { ?>
    <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
<? } ?>
<div class="page">
<? if (PAGE_TYPE == "order") { ?>

    <header class="page__header">
        <div class="checkout-header">
            <div class="container">
                <div class="row row--vcentr b-list">
                    <div class="col-12 col-sm-4 b-list__item">
                        <div class="logo" itemtype="http://schema.org/Organization" itemscope="itemscope">
                            <a class="logo__link"<? if ($APPLICATION->GetCurPage() != '/') { ?> href="/" itemprop="url"<? } ?>>
                                <img class="logo__image"
                                     src="<?= SITE_TEMPLATE_PATH ?>/static/images/logo.svg?v=3ea451c9" width="190"
                                     height="38" alt="ЭНДИМАРТ" itemprop="logo">
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-3 b-list__item">
                        <a class="checkout-header__tel" href="tel:<?
                        $APPLICATION->IncludeComponent(
                            "bitrix:main.include", "", Array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => SITE_TEMPLATE_PATH . "/include/header/top_phone.php"
                        ), false, array('HIDE_ICONS' => 'Y')
                        );
                        ?>">
                            <?
                            $APPLICATION->IncludeComponent(
                                "bitrix:main.include", "", Array(
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => SITE_TEMPLATE_PATH . "/include/header/top_phone.php"
                                )
                            );
                            ?></a>
                    </div>
                    <div class="col-12 col-sm-5 text-sm-right b-list__item">
                        <div class="checkout-header__delivery"><?
                            $APPLICATION->IncludeComponent(
                                "bitrix:main.include", "", Array(
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => SITE_TEMPLATE_PATH . "/include/header/under_city_text.php"
                                )
                            );
                            ?></div>
                    </div>
                </div>
            </div>
        </div>
    </header>
<? } else { ?>
    <header class="page__header">
        <div id="header" class="header">
            <div class="container">
                <div class="header__top hidden-sm-down">
                    <nav class="sub-nav">
                        <?
                        $APPLICATION->IncludeComponent("bitrix:menu", "top", Array(
                            "ALLOW_MULTI_SELECT" => "N", // Разрешить несколько активных пунктов одновременно
                            "CHILD_MENU_TYPE" => "submenu", // Тип меню для остальных уровней
                            "DELAY" => "N", // Откладывать выполнение шаблона меню
                            "MAX_LEVEL" => "2", // Уровень вложенности меню
                            "MENU_CACHE_GET_VARS" => "", // Значимые переменные запроса
                            "MENU_CACHE_TIME" => "3600", // Время кеширования (сек.)
                            "MENU_CACHE_TYPE" => "N", // Тип кеширования
                            "MENU_CACHE_USE_GROUPS" => "Y", // Учитывать права доступа
                            "ROOT_MENU_TYPE" => "top", // Тип меню для первого уровня
                            "USE_EXT" => "N", // Подключать файлы с именами вида .тип_меню.menu_ext.php
                            "COMPONENT_TEMPLATE" => "horizontal_multilevel"
                        ), false
                        );
                        ?>

                    </nav>
                    <nav class="sub-nav sub-nav--big">
                        <ul class="sub-nav__list">
                            <?
                            if (!$USER->IsAuthorized()) {
                                ?>
                                <li class="sub-nav__item">
                                    <a class="sub-nav__link" href="/auth/">Вход</a>
                                </li>
                                <li class="sub-nav__item">
                                    <a class="sub-nav__link" href="/auth/">Регистрация</a>
                                </li>
                                <?
                            } else {
                                ?>
                                <li class="sub-nav__item">
                                    <a class="sub-nav__link" href="/lichniy_cabinet/">Личный кабинет</a>
                                </li>
                                <li class="sub-nav__item">
                                    <a class="sub-nav__link" href="/?logout=yes">Выход</a>
                                </li>
                                <?
                            }
                            ?>
                        </ul>
                    </nav>
                </div>
                <!-- Пришлось дублировать центральную часть шапки, чтобы добиться необходимой анимации (появляется на больших экранах при прокрутке). Из отличий тут: логотип без schema разметки, нет блока с регионом, встроена форма поиска, нет .hidden-md-up блоков)-->
                <div id="header-fixed" class="header__fixed">
                    <div class="container">
                        <div class="header__mid header__mid--fixed">
                            <div class="header__logo">
                                <div class="logo">
                                    <a class="logo__link" <? if ($APPLICATION->GetCurPage() != '/') { ?> href="/"<? } ?>>
                                        <img class="logo__image"
                                             src="<?= SITE_TEMPLATE_PATH ?>/static/images/logo.svg?v=3ea451c9"
                                             width="190" height="38" alt="ЭНДИМАРТ">
                                    </a>
                                </div>
                            </div>
                            <div class="header__elems">
                                <div class="header__elem header__elem--tel">
                                    <a class="header__tel" href="tel:<?
                                    $APPLICATION->IncludeComponent(
                                        "bitrix:main.include", "", Array(
                                        "AREA_FILE_SHOW" => "file",
                                        "AREA_FILE_SUFFIX" => "inc",
                                        "EDIT_TEMPLATE" => "",
                                        "PATH" => SITE_TEMPLATE_PATH . "/include/header/top_phone.php"
                                    ), false, array('HIDE_ICONS' => 'Y')
                                    );
                                    ?>">
                                        <?
                                        $APPLICATION->IncludeComponent(
                                            "bitrix:main.include", "", Array(
                                                "AREA_FILE_SHOW" => "file",
                                                "AREA_FILE_SUFFIX" => "inc",
                                                "EDIT_TEMPLATE" => "",
                                                "PATH" => SITE_TEMPLATE_PATH . "/include/header/top_phone.php"
                                            )
                                        );
                                        ?></a>
                                    <div class="text-small hidden-sm-down"><?
                                        $APPLICATION->IncludeComponent(
                                            "bitrix:main.include", "", Array(
                                                "AREA_FILE_SHOW" => "file",
                                                "AREA_FILE_SUFFIX" => "inc",
                                                "EDIT_TEMPLATE" => "",
                                                "PATH" => SITE_TEMPLATE_PATH . "/include/header/top_phone_slogan.php"
                                            )
                                        );
                                        ?></div>
                                </div>
                                <div class="header__elem header__elem--search">
                                    <!-- Пришлось дублировать форму поиска для прилипающей шапки-->
                                    <form class="search-form js-form-validator" novalidate="novalidate"
                                          action="/catalog/search/">
                                        <div class="input-group">
                                            <div class="input-group__item input-group__item--grow">
                                                <input class="input-text search-form__input" type="text" name="q"
                                                       placeholder="Поиск" required="required"
                                                       value="<?= htmlspecialchars($_REQUEST["q"]) ?>">
                                            </div>
                                            <div class="input-group__item">
                                                <button class="search-form__submit" type="submit"></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="header__elem header_card header__elem_basket">
                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:sale.basket.basket.line",
                                        "header",
                                        Array(
                                            "HIDE_ON_BASKET_PAGES" => "Y",
                                            "PATH_TO_BASKET" => SITE_DIR . "cart/",
                                            "PATH_TO_ORDER" => SITE_DIR . "cart/order/",
                                            "POSITION_FIXED" => "N",
                                            "SHOW_AUTHOR" => "N",
                                            "SHOW_EMPTY_VALUES" => "Y",
                                            "SHOW_NUM_PRODUCTS" => "Y",
                                            "SHOW_PERSONAL_LINK" => "N",
                                            "SHOW_PRODUCTS" => "Y",
                                            "SHOW_TOTAL_PRICE" => "Y"
                                        )
                                    ); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header__mid">
                    <div class="header__logo">
                        <div class="logo" itemtype="http://schema.org/Organization" itemscope="itemscope">
                            <a class="logo__link" <? if ($APPLICATION->GetCurPage() != '/') { ?> href="/" itemprop="url"<? } ?>>
                                <img class="logo__image"
                                     src="<?= SITE_TEMPLATE_PATH ?>/static/images/logo.svg?v=3ea451c9" width="190"
                                     height="38" alt="ЭНДИМАРТ" itemprop="logo">
                            </a>
                        </div>
                    </div>
                    <div class="header__elems">
                        <div class="header__elem header__elem--tel">
                            <a class="header__tel" href="tel:<?
                            $APPLICATION->IncludeComponent(
                                "bitrix:main.include", "", Array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => SITE_TEMPLATE_PATH . "/include/header/top_phone.php"
                            ), false, array('HIDE_ICONS' => 'Y')
                            );
                            ?>"><?
                                $APPLICATION->IncludeComponent(
                                    "bitrix:main.include", "", Array(
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => SITE_TEMPLATE_PATH . "/include/header/top_phone.php"
                                ), false, array('HIDE_ICONS' => 'N')
                                );
                                ?></a>
                            <div class="text-small hidden-sm-down"><?
                                $APPLICATION->IncludeComponent(
                                    "bitrix:main.include", "", Array(
                                        "AREA_FILE_SHOW" => "file",
                                        "AREA_FILE_SUFFIX" => "inc",
                                        "EDIT_TEMPLATE" => "",
                                        "PATH" => SITE_TEMPLATE_PATH . "/include/header/top_phone_slogan.php"
                                    )
                                );
                                ?></div>
                        </div>
                        <div class="header__elem header__elem--region hidden-sm-down" style="">
                            <div class="header__text-b">Ваш город
                                <a id="js-what-city-anchor" href="javascript:void(0)"
                                   onclick="Popups.openCityPopup(true);"><?= $_SESSION["PEK_CURRENT_CITY_NAME"] ?></a>
                            </div>
                            <div class="header__text-b">
                                <?
                                $APPLICATION->IncludeComponent(
                                    "bitrix:main.include", "", Array(
                                        "AREA_FILE_SHOW" => "file",
                                        "AREA_FILE_SUFFIX" => "inc",
                                        "EDIT_TEMPLATE" => "",
                                        "PATH" => SITE_TEMPLATE_PATH . "/include/header/under_city_text.php"
                                    )
                                );
                                ?>
                            </div>
                        </div>
                        <div class="header__elem hidden-md-up">
                            <button class="header__show-search js-toggle-search" type="button"></button>
                        </div>
                        <div class="header__elem header__elem_basket" style="">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:sale.basket.basket.line",
                                "header",
                                Array(
                                    "HIDE_ON_BASKET_PAGES" => "Y",
                                    "PATH_TO_BASKET" => SITE_DIR . "cart/",
                                    "PATH_TO_ORDER" => SITE_DIR . "cart/order/",
                                    "POSITION_FIXED" => "N",
                                    "SHOW_AUTHOR" => "N",
                                    "SHOW_EMPTY_VALUES" => "Y",
                                    "SHOW_NUM_PRODUCTS" => "Y",
                                    "SHOW_PERSONAL_LINK" => "N",
                                    "SHOW_PRODUCTS" => "Y",
                                    "SHOW_TOTAL_PRICE" => "Y"
                                )
                            ); ?>
                        </div>
                        <div class="header__elem hidden-md-up">
                            <button class="header__burger js-toggle-mobile-nav" type="button"></button>
                        </div>
                    </div>
                </div>
                <div class="header__bot">
                    <nav class="nav hidden-sm-down">
                        <!-- Для удобства восприятия, шаблон меню можно посмотреть в src/blocks/nav/nav.jade. Используются данные из src/data/catalog-nav.json-->
                        <?
                        $APPLICATION->IncludeComponent(
                            "bitrix:menu", "top_catalog", array(
                            "ALLOW_MULTI_SELECT" => "N",
                            "CHILD_MENU_TYPE" => "catalog",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "3",
                            "MENU_CACHE_GET_VARS" => array(),
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "ROOT_MENU_TYPE" => "catalog",
                            "USE_EXT" => "Y",
                            "COMPONENT_TEMPLATE" => "top_catalog"
                        ), false
                        );
                        ?>

                        <button class="nav__btn js-toggle-search" type="button"></button>
                    </nav>
                    <div id="header-search-form" class="header__search">
                        <form class="search-form" novalidate="novalidate" action="/catalog/">
                            <div class="input-group">
                                <div class="input-group__item input-group__item--grow">
                                    <input class="input-text search-form__input" type="text" placeholder="Поиск"
                                           name="q" required="required" value="<?= htmlspecialchars($_REQUEST["q"]) ?>">
                                </div>
                                <div class="input-group__item">
                                    <button class="search-form__submit" type="submit"></button>
                                </div>
                                <div class="input-group__item">
                                    <button class="search-form__cancel js-toggle-search" type="button"></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div id="mobile-nav" class="mobile-nav">
                <div class="mobile-nav__section" style="">
                    <div class="container">Ваш город&ensp;
                        <a id="mobile-nav-city" href="javascript:void()"
                           onclick="Popups.openCityPopup(true);"><?= $_SESSION["PEK_CURRENT_CITY_NAME"] ?></a>
                    </div>
                </div>
                <div class="mobile-nav__section mobile-nav__section--accent">
                    <div class="container">
                        <?
                        $APPLICATION->IncludeComponent(
                            "bitrix:menu", "top_catalog_mobile", array(
                            "SHOW_ONLY_DEAPTH_LEVEL" => 1,
                            "ALLOW_MULTI_SELECT" => "N",
                            "CHILD_MENU_TYPE" => "catalog",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "3",
                            "MENU_CACHE_GET_VARS" => array(),
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "ROOT_MENU_TYPE" => "catalog",
                            "USE_EXT" => "Y",
                            "COMPONENT_TEMPLATE" => "top_catalog"
                        ), false
                        );
                        ?>

                    </div>
                </div>
                <div class="mobile-nav__section">
                    <div class="container">
                        <ul class="mobile-nav__list mobile-nav__list--inline">
                            <?
                            if (!$USER->IsAuthorized()) {
                                ?>
                                <li class="mobile-nav__item">
                                    <a class="mobile-nav__link" href="/auth/">Вход</a>
                                </li>
                                <li class="mobile-nav__item">
                                    <a class="mobile-nav__link" href="/auth/">Регистрация</a>
                                </li>
                                <?
                            } else {
                                ?>
                                <li class="mobile-nav__item">
                                    <a class="mobile-nav__link" href="/lichniy_cabinet/">Личный кабинет</a>
                                </li>
                                <li class="mobile-nav__item">
                                    <a class="mobile-nav__link" href="/?logout=yes">Выход</a>
                                </li>
                                <?
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <div class="mobile-nav__section">
                    <div class="container">
                        <?
                        $APPLICATION->IncludeComponent(
                            "bitrix:menu", "top_catalog_mobile", array(
                            "SHOW_ONLY_DEAPTH_LEVEL" => 1,
                            "ALLOW_MULTI_SELECT" => "N",
                            "CHILD_MENU_TYPE" => "catalog",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "1",
                            "MENU_CACHE_GET_VARS" => array(),
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "ROOT_MENU_TYPE" => "top",
                            "USE_EXT" => "Y",
                            "COMPONENT_TEMPLATE" => "top_catalog_mobile"
                        ), false
                        );
                        ?>

                    </div>
                </div>
                <div class="mobile-nav__city">
                    <div class="container">
                        <div class="mobile-nav__city-search">
                            <form class="search-form js-city-select" novalidate="novalidate" action="/">
                                <!-- Логика автокомплита города в src/scripts/city-select-form.js-->
                                <div class="input-group">
                                    <div class="input-group__item input-group__item--grow">
                                        <input data-cities-src="/ajax/cities.php" name="set_city_name" type="text"
                                               class="input-text js-city-select__input js-city-input">
                                    </div>
                                    <div class="input-group__item">
                                        <button class="search-form__submit search-form__submit--arrow js-city-select__submit"
                                                type="submit" disabled="disabled"></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="mobile-nav__city-list" style="">
                            <?
                            $arSelect = Array("ID", "NAME");
                            $arFilter = Array("IBLOCK_ID" => CITIES_IBLOCK_ID, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y"/* ,"NAME"=>$_GET["search"]."%" */);
                            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                            $result = array();
                            while ($ob = $res->GetNextElement()) {
                                $arFields = $ob->GetFields();
                                ?>
                                <div class="mobile-nav__city-item">
                                    <a class="link-hidden"
                                       href="<?= $APPLICATION->GetCurPageParam("set_city=" . $arFields ["ID"], array("set_city")); ?>"><?= $arFields ["NAME"] ?></a>
                                </div>
                                <?
                            }
                            ?>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
<? } ?>
    <div class="page__content">
    <div class="container">
<? if (PAGE_TYPE == "order") { ?>
    <div class="content-block content-block--pull-top island island--small">
    <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumbs", Array(
        "PATH" => "", // Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
        "SITE_ID" => "s1", // Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
        "START_FROM" => "0", // Номер пункта, начиная с которого будет построена навигационная цепочка
    ), false
    ); ?>
    <div class="checkout-steps">
        <a class="checkout-steps__back" href="/catalog/">Вернуться в каталог</a>

        <?
        $APPLICATION->IncludeComponent(
            "bitrix:menu", "cart", array(
            "SHOW_ONLY_DEAPTH_LEVEL" => 1,
            "ALLOW_MULTI_SELECT" => "N",
            "CHILD_MENU_TYPE" => "cart",
            "DELAY" => "N",
            "MAX_LEVEL" => "1",
            "MENU_CACHE_GET_VARS" => array(),
            "MENU_CACHE_TIME" => "3600",
            "MENU_CACHE_TYPE" => "A",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "ROOT_MENU_TYPE" => "cart",
            "USE_EXT" => "Y",
            "COMPONENT_TEMPLATE" => ""
        ), false
        );
        ?>
    </div>
    <div class="content-block">
<? } ?>

<? if (PERSONAL_ORDERS == 'Y' || PAGE_TYPE == "masters") { ?>
    <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumbs", Array(
        "PATH" => "", // Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
        "SITE_ID" => "s1", // Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
        "START_FROM" => "0", // Номер пункта, начиная с которого будет построена навигационная цепочка
    ), false
    ); ?>
<? } ?>
<? if (PAGE_TYPE == "reviews" || CATALOG_CATEGORY == "Y" || CATALOG_BRAND == "Y" || CATALOG_CALCS == "Y") { ?>
    <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumbs", Array(
        "PATH" => "", // Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
        "SITE_ID" => "s1", // Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
        "START_FROM" => "0", // Номер пункта, начиная с которого будет построена навигационная цепочка
    ), false
    ); ?>

    <div class="content-block content-block--pull-top <? if (PERSONAL_ORDERS == 'Y') { ?> content-block--sm-bot<? } ?>  <? if (SHOW_LEFT_MENU != "Y" && CATALOG_CALCS != "Y") { ?>island <? } ?>">
    <? if (SHOW_LEFT_MENU == "Y") { ?>
    <div class="layout">
    <main class="layout__main">
    <div class="island <? if (CATALOG_CATEGORY == "Y" || CATALOG_BRAND == "Y") { ?> content-block content-block--sm-bot <? } ?>">
    <? } ?>
    <? if (CATALOG_CATEGORY == "Y") { ?>
    <div class="category-card">
        <? } ?>
        <? if (NOT_SHOW_H1 != "Y") { ?>
            <? if (CATALOG_CATEGORY == "Y") { ?>
                <div class="category-card__head">
            <? } ?>
            <h1 class="section-title title-line <? if (CATALOG_CATEGORY == "Y") { ?>title-line--accent<? } ?> js-title-line"><? $APPLICATION->ShowTitle(false) ?></h1>
            <? if (CATALOG_CATEGORY == "Y") { ?>
                <? $APPLICATION->ShowViewContent('category_h1_additional'); ?>
                </div>
            <? } ?>
        <? } ?>
        <? if (CATALOG_BRAND == "Y") { ?>
        <? $APPLICATION->ShowViewContent('brands_h1_additional'); ?>
    </div>
<? } ?>
    <? if (CATALOG_CATEGORY != "Y" && CATALOG_BRAND != "Y" && NO_WYSIWYG != 'Y' && CATALOG_CALCS != 'Y') { ?>
    <div class="wysiwyg">
<? } ?>
    <?
    global $arrFilter_content_sliders;
    $arr = explode("?", $_SERVER["REQUEST_URI"]);
    $arrFilter_content_sliders = array("PROPERTY_LINK" => $arr[0]);
    // p($arrFilter_content_sliders );
    $APPLICATION->IncludeComponent("bitrix:catalog.section", "sliders", Array(
        "ACTION_VARIABLE" => "action", // Название переменной, в которой передается действие
        "ADD_PICT_PROP" => "-",
        "ADD_PROPERTIES_TO_BASKET" => "Y", // Добавлять в корзину свойства товаров и предложений
        "ADD_SECTIONS_CHAIN" => "N", // Включать раздел в цепочку навигации
        "ADD_TO_BASKET_ACTION" => "ADD",
        "AJAX_MODE" => "N", // Включить режим AJAX
        "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
        "AJAX_OPTION_HISTORY" => "N", // Включить эмуляцию навигации браузера
        "AJAX_OPTION_JUMP" => "N", // Включить прокрутку к началу компонента
        "AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
        "BACKGROUND_IMAGE" => "-", // Установить фоновую картинку для шаблона из свойства
        "BASKET_URL" => "/lichniy_cabinet/basket.php", // URL, ведущий на страницу с корзиной покупателя
        "BROWSER_TITLE" => "-", // Установить заголовок окна браузера из свойства
        "CACHE_FILTER" => "N", // Кешировать при установленном фильтре
        "CACHE_GROUPS" => "Y", // Учитывать права доступа
        "CACHE_TIME" => "36000000", // Время кеширования (сек.)
        "CACHE_TYPE" => "A", // Тип кеширования
        "CONVERT_CURRENCY" => "N", // Показывать цены в одной валюте
        "DETAIL_URL" => "", // URL, ведущий на страницу с содержимым элемента раздела
        "DISABLE_INIT_JS_IN_COMPONENT" => "N", // Не подключать js-библиотеки в компоненте
        "DISPLAY_BOTTOM_PAGER" => "N", // Выводить под списком
        "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
        "ELEMENT_SORT_FIELD" => "sort", // По какому полю сортируем элементы
        "ELEMENT_SORT_FIELD2" => "id", // Поле для второй сортировки элементов
        "ELEMENT_SORT_ORDER" => "asc", // Порядок сортировки элементов
        "ELEMENT_SORT_ORDER2" => "desc", // Порядок второй сортировки элементов
        "FILTER_NAME" => "arrFilter_content_sliders", // Имя массива со значениями фильтра для фильтрации элементов
        "HIDE_NOT_AVAILABLE" => "N", // Товары, которых нет на складах
        "IBLOCK_ID" => "11", // Инфоблок
        "IBLOCK_TYPE" => "sliders", // Тип инфоблока
        "INCLUDE_SUBSECTIONS" => "Y", // Показывать элементы подразделов раздела
        "LABEL_PROP" => "-",
        "LINE_ELEMENT_COUNT" => "3", // Количество элементов выводимых в одной строке таблицы
        "MESSAGE_404" => "", // Сообщение для показа (по умолчанию из компонента)
        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
        "MESS_BTN_BUY" => "Купить",
        "MESS_BTN_DETAIL" => "Подробнее",
        "MESS_BTN_SUBSCRIBE" => "Подписаться",
        "MESS_NOT_AVAILABLE" => "Нет в наличии",
        "META_DESCRIPTION" => "-", // Установить описание страницы из свойства
        "META_KEYWORDS" => "-", // Установить ключевые слова страницы из свойства
        "OFFERS_LIMIT" => "5", // Максимальное количество предложений для показа (0 - все)
        "PAGER_BASE_LINK_ENABLE" => "N", // Включить обработку ссылок
        "PAGER_DESC_NUMBERING" => "N", // Использовать обратную навигацию
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // Время кеширования страниц для обратной навигации
        "PAGER_SHOW_ALL" => "N", // Показывать ссылку "Все"
        "PAGER_SHOW_ALWAYS" => "N", // Выводить всегда
        "PAGER_TEMPLATE" => ".default", // Шаблон постраничной навигации
        "PAGER_TITLE" => "Товары", // Название категорий
        "PAGE_ELEMENT_COUNT" => "30", // Количество элементов на странице
        "PARTIAL_PRODUCT_PROPERTIES" => "N", // Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
        "PRICE_CODE" => "", // Тип цены
        "PRICE_VAT_INCLUDE" => "Y", // Включать НДС в цену
        "PRODUCT_ID_VARIABLE" => "id", // Название переменной, в которой передается код товара для покупки
        "PRODUCT_PROPERTIES" => "", // Характеристики товара
        "PRODUCT_PROPS_VARIABLE" => "prop", // Название переменной, в которой передаются характеристики товара
        "PRODUCT_QUANTITY_VARIABLE" => "", // Название переменной, в которой передается количество товара
        "PRODUCT_SUBSCRIPTION" => "N",
        "PROPERTY_CODE" => array(// Свойства
            0 => "",
            1 => "",
        ),
        "SECTION_CODE" => "", // Код раздела
        "SECTION_ID" => "", // ID раздела
        "SECTION_ID_VARIABLE" => "SECTION_ID", // Название переменной, в которой передается код группы
        "SECTION_URL" => "", // URL, ведущий на страницу с содержимым раздела
        "SECTION_USER_FIELDS" => array(// Свойства раздела
            0 => "",
            1 => "FILES",
            2 => "",
        ),
        "SEF_MODE" => "N", // Включить поддержку ЧПУ
        "SET_BROWSER_TITLE" => "N", // Устанавливать заголовок окна браузера
        "SET_LAST_MODIFIED" => "N", // Устанавливать в заголовках ответа время модификации страницы
        "SET_META_DESCRIPTION" => "N", // Устанавливать описание страницы
        "SET_META_KEYWORDS" => "N", // Устанавливать ключевые слова страницы
        "SET_STATUS_404" => "N", // Устанавливать статус 404
        "SET_TITLE" => "N", // Устанавливать заголовок страницы
        "SHOW_404" => "N", // Показ специальной страницы
        "SHOW_ALL_WO_SECTION" => "N", // Показывать все элементы, если не указан раздел
        "SHOW_CLOSE_POPUP" => "N",
        "SHOW_DISCOUNT_PERCENT" => "N",
        "SHOW_OLD_PRICE" => "N",
        "SHOW_PRICE_COUNT" => "1", // Выводить цены для количества
        "TEMPLATE_THEME" => "blue",
        "USE_MAIN_ELEMENT_SECTION" => "N", // Использовать основной раздел для показа элемента
        "USE_PRICE_COUNT" => "N", // Использовать вывод цен с диапазонами
        "USE_PRODUCT_QUANTITY" => "N", // Разрешить указание количества товара
        "COMPONENT_TEMPLATE" => "content_slider"
    ), false
    );
    ?>
<? } ?>
<? if (PRODUCT_CARD == "Y") { ?>
    <?
    $APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumbs", Array(
        "PATH" => "", // Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
        "SITE_ID" => "s1", // Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
        "START_FROM" => "0", // Номер пункта, начиная с которого будет построена навигационная цепочка
    ), false
    );
    ?>
<? } ?>