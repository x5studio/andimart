<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */

/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

$this->setFrameMode(true);
?>
<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumbs", Array(
    "PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
    "SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
    "START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
),
    false
);?>

<div class="content-block content-block--pull-top">
    <div class="layout">
	<main class="layout__main">
		
		<?
		$ElementID = $APPLICATION->IncludeComponent(
			"bitrix:catalog.element", "", array(
		    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		    "IBLOCK_ID" => const_IBLOCK_ID_brands,
		    "PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
		    "META_KEYWORDS" => $arParams["DETAIL_META_KEYWORDS"],
		    "META_DESCRIPTION" => $arParams["DETAIL_META_DESCRIPTION"],
		    "BROWSER_TITLE" => $arParams["DETAIL_BROWSER_TITLE"],
		    "SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"],
		    "BASKET_URL" => $arParams["BASKET_URL"],
		    "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
		    "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
		    "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
		    "CHECK_SECTION_ID_VARIABLE" => (isset($arParams["DETAIL_CHECK_SECTION_ID_VARIABLE"]) ? $arParams["DETAIL_CHECK_SECTION_ID_VARIABLE"] : ''),
		    "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
		    "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
		    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
		    "CACHE_TIME" => $arParams["CACHE_TIME"],
		    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		    "SET_TITLE" => $arParams["SET_TITLE"],
		    "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
		    "MESSAGE_404" => $arParams["MESSAGE_404"],
		    "SET_STATUS_404" => $arParams["SET_STATUS_404"],
		    "SHOW_404" => $arParams["SHOW_404"],
		    "FILE_404" => $arParams["FILE_404"],
		    "PRICE_CODE" => $arParams["PRICE_CODE"],
		    "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
		    "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
		    "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
		    "PRICE_VAT_SHOW_VALUE" => $arParams["PRICE_VAT_SHOW_VALUE"],
		    "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
		    "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
		    "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
		    "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
		    "LINK_IBLOCK_TYPE" => $arParams["LINK_IBLOCK_TYPE"],
		    "LINK_IBLOCK_ID" => $arParams["LINK_IBLOCK_ID"],
		    "LINK_PROPERTY_SID" => $arParams["LINK_PROPERTY_SID"],
		    "LINK_ELEMENTS_URL" => $arParams["LINK_ELEMENTS_URL"],
		    "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
		    "OFFERS_FIELD_CODE" => $arParams["DETAIL_OFFERS_FIELD_CODE"],
		    "OFFERS_PROPERTY_CODE" => $arParams["DETAIL_OFFERS_PROPERTY_CODE"],
		    "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
		    "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
		    "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
		    "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
		    "ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
		    "ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
		    "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
		    "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
		    "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
		    "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
		    'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
		    'CURRENCY_ID' => $arParams['CURRENCY_ID'],
		    'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
		    'USE_ELEMENT_COUNTER' => $arParams['USE_ELEMENT_COUNTER'],
		    'SHOW_DEACTIVATED' => $arParams['SHOW_DEACTIVATED'],
		    "USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
		    'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
		    'LABEL_PROP' => $arParams['LABEL_PROP'],
		    'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
		    'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
		    'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
		    'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
		    'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
		    'SHOW_MAX_QUANTITY' => $arParams['DETAIL_SHOW_MAX_QUANTITY'],
		    'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
		    'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
		    'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
		    'MESS_BTN_COMPARE' => $arParams['MESS_BTN_COMPARE'],
		    'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],
		    'USE_VOTE_RATING' => $arParams['DETAIL_USE_VOTE_RATING'],
		    'VOTE_DISPLAY_AS_RATING' => (isset($arParams['DETAIL_VOTE_DISPLAY_AS_RATING']) ? $arParams['DETAIL_VOTE_DISPLAY_AS_RATING'] : ''),
		    'USE_COMMENTS' => $arParams['DETAIL_USE_COMMENTS'],
		    'BLOG_USE' => (isset($arParams['DETAIL_BLOG_USE']) ? $arParams['DETAIL_BLOG_USE'] : ''),
		    'BLOG_URL' => (isset($arParams['DETAIL_BLOG_URL']) ? $arParams['DETAIL_BLOG_URL'] : ''),
		    'BLOG_EMAIL_NOTIFY' => (isset($arParams['DETAIL_BLOG_EMAIL_NOTIFY']) ? $arParams['DETAIL_BLOG_EMAIL_NOTIFY'] : ''),
		    'VK_USE' => (isset($arParams['DETAIL_VK_USE']) ? $arParams['DETAIL_VK_USE'] : ''),
		    'VK_API_ID' => (isset($arParams['DETAIL_VK_API_ID']) ? $arParams['DETAIL_VK_API_ID'] : 'API_ID'),
		    'FB_USE' => (isset($arParams['DETAIL_FB_USE']) ? $arParams['DETAIL_FB_USE'] : ''),
		    'FB_APP_ID' => (isset($arParams['DETAIL_FB_APP_ID']) ? $arParams['DETAIL_FB_APP_ID'] : ''),
		    'BRAND_USE' => (isset($arParams['DETAIL_BRAND_USE']) ? $arParams['DETAIL_BRAND_USE'] : 'N'),
		    'BRAND_PROP_CODE' => (isset($arParams['DETAIL_BRAND_PROP_CODE']) ? $arParams['DETAIL_BRAND_PROP_CODE'] : ''),
		    'DISPLAY_NAME' => (isset($arParams['DETAIL_DISPLAY_NAME']) ? $arParams['DETAIL_DISPLAY_NAME'] : ''),
		    'ADD_DETAIL_TO_SLIDER' => (isset($arParams['DETAIL_ADD_DETAIL_TO_SLIDER']) ? $arParams['DETAIL_ADD_DETAIL_TO_SLIDER'] : ''),
		    'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
		    "ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
		    "ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
		    "DISPLAY_PREVIEW_TEXT_MODE" => (isset($arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE']) ? $arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE'] : ''),
		    "DETAIL_PICTURE_MODE" => (isset($arParams['DETAIL_DETAIL_PICTURE_MODE']) ? $arParams['DETAIL_DETAIL_PICTURE_MODE'] : ''),
		    'ADD_TO_BASKET_ACTION' => $basketAction,
		    'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
		    'DISPLAY_COMPARE' => (isset($arParams['USE_COMPARE']) ? $arParams['USE_COMPARE'] : ''),
		    'COMPARE_PATH' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['compare'],
		    'SHOW_BASIS_PRICE' => (isset($arParams['DETAIL_SHOW_BASIS_PRICE']) ? $arParams['DETAIL_SHOW_BASIS_PRICE'] : 'Y'),
		    'BACKGROUND_IMAGE' => (isset($arParams['DETAIL_BACKGROUND_IMAGE']) ? $arParams['DETAIL_BACKGROUND_IMAGE'] : ''),
		    'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
		    'SET_VIEWED_IN_COMPONENT' => (isset($arParams['DETAIL_SET_VIEWED_IN_COMPONENT']) ? $arParams['DETAIL_SET_VIEWED_IN_COMPONENT'] : ''),
		    "USE_GIFTS_DETAIL" => $arParams['USE_GIFTS_DETAIL']? : 'Y',
		    "USE_GIFTS_MAIN_PR_SECTION_LIST" => $arParams['USE_GIFTS_MAIN_PR_SECTION_LIST']? : 'Y',
		    "GIFTS_SHOW_DISCOUNT_PERCENT" => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
		    "GIFTS_SHOW_OLD_PRICE" => $arParams['GIFTS_SHOW_OLD_PRICE'],
		    "GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT'],
		    "GIFTS_DETAIL_HIDE_BLOCK_TITLE" => $arParams['GIFTS_DETAIL_HIDE_BLOCK_TITLE'],
		    "GIFTS_DETAIL_TEXT_LABEL_GIFT" => $arParams['GIFTS_DETAIL_TEXT_LABEL_GIFT'],
		    "GIFTS_DETAIL_BLOCK_TITLE" => $arParams["GIFTS_DETAIL_BLOCK_TITLE"],
		    "GIFTS_SHOW_NAME" => $arParams['GIFTS_SHOW_NAME'],
		    "GIFTS_SHOW_IMAGE" => $arParams['GIFTS_SHOW_IMAGE'],
		    "GIFTS_MESS_BTN_BUY" => $arParams['GIFTS_MESS_BTN_BUY'],
		    "GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT'],
		    "GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE'],
			), $component
		);
		?>
	    
	    <div class="catalog-ctrls">
		<div class="catalog-ctrls__sort island island--small">
                    <div class="catalog-ctrls__in">
			<div class="row row--vcentr">
			    <div class="col-12 col-sm-4 col-md-6">
				<div class="count catalog-ctrls__count">
				    <span class="count__val">127</span>
				    <span class="count__label">товаров</span>
				</div>
			    </div>
			    <div class="col-12 col-sm-8 col-md-6">
				<div class="row row--vcentr">
				    <div class="col-12 col-sm-5 text-sm-right text-small-expand">Сортировать по</div>
				    <div class="col-12 col-sm-7">
					<select class="select select--xs">
					    <option value selected>цене</option>
					    <option value="">популярности</option>
					    <option value="">рейтингу</option>
					</select>
				    </div>
				</div>
			    </div>
			</div>
                    </div>
		</div>
		<div class="catalog-ctrls__filters island island--small">
                    <button class="catalog-ctrls__filter-btn js-toggle-filters-sm" type="button">
			<img class="catalog-ctrls__filter-btn-i" src="static/images/filters.svg?v=1431db20" width="48" height="32" alt>
			<span>фильтр</span>
                    </button>
		</div>
	    </div>
	    <div class="catalog grid grid--xs-2 grid--sm-3 grid--md-3">
		<div class="catalog__item grid__item">
                    <div class="catalog__item-in">
			<div class="catalog__img">
			    <a class="link-hidden" href="product.html">
				<img src="static/images/upload/catalog/0.jpg?v=be80f03e" width="200" height="200" alt="Термостатическая головка с выносным накладным датчиком">
			    </a>
			</div>
			<div class="catalog__content">
			    <div class="catalog__content-top">
				<div class="catalog__title">
				    <a class="link-hidden" href="product.html">Термостатическая головка с выносным накладным датчиком</a>
				</div>
				<div class="catalog__brand">Бренд: JW
				    <div class="catalog__avail">
					<span class="text-success">●</span> в наличии</div>
				</div>
			    </div>
			    <div class="catalog__content-bot">
				<div class="product-price">
				    <div class="product-price__old">126 470 р.</div>
				    <div class="product-price__current">124 000 р.</div>
				</div>
				<div class="catalog__buy">
				    <div class="catalog__buy-count">
					<div class="input-group">
					    <div class="input-group__item catalog__buy-input">
						<input class="input-text input-text--sm text-center" type="text" value="1">
					    </div>
					    <div class="input-group__item catalog__buy-label">
						<div class="input-posfix">шт.</div>
					    </div>
					</div>
				    </div>
				    <div class="catalog__buy-btn">
					<button class="btn btn--sm" type="button">
					    <span>В корзину</span>
					</button>
				    </div>
				</div>
			    </div>
			</div>
                    </div>
		</div>
		<div class="catalog__item grid__item">
                    <div class="catalog__item-in">
			<div class="catalog__img">
			    <a class="link-hidden" href="product.html">
				<img src="static/images/upload/catalog/1.jpg?v=72bc5a49" width="200" height="200" alt="Термостатическая головка с выносным накладным датчиком">
			    </a>
			</div>
			<div class="catalog__content">
			    <div class="catalog__content-top">
				<div class="catalog__title">
				    <a class="link-hidden" href="product.html">Термостатическая головка с выносным накладным датчиком</a>
				</div>
				<div class="catalog__brand">Бренд: JW
				    <div class="catalog__avail">
					<span class="text-success">●</span> в наличии</div>
				</div>
			    </div>
			    <div class="catalog__content-bot">
				<div class="product-price">
				    <div class="product-price__current">124 000 р.</div>
				</div>
				<div class="catalog__buy">
				    <div class="catalog__buy-count">
					<div class="input-group">
					    <div class="input-group__item catalog__buy-input">
						<input class="input-text input-text--sm text-center" type="text" value="1">
					    </div>
					    <div class="input-group__item catalog__buy-label">
						<div class="input-posfix">шт.</div>
					    </div>
					</div>
				    </div>
				    <div class="catalog__buy-btn">
					<button class="btn btn--sm" type="button">
					    <span>В корзину</span>
					</button>
				    </div>
				</div>
			    </div>
			</div>
                    </div>
		</div>
		<div class="catalog__item grid__item">
                    <div class="catalog__item-in">
			<div class="catalog__img">
			    <a class="link-hidden" href="product.html">
				<img src="static/images/upload/catalog/2.jpg?v=46f24cb1" width="200" height="200" alt="Термостатическая головка с выносным накладным датчиком">
			    </a>
			</div>
			<div class="catalog__content">
			    <div class="catalog__content-top">
				<div class="catalog__title">
				    <a class="link-hidden" href="product.html">Термостатическая головка с выносным накладным датчиком</a>
				</div>
				<div class="catalog__brand">Бренд: JW
				    <div class="catalog__avail">
					<span class="text-success">●</span> в наличии</div>
				</div>
			    </div>
			    <div class="catalog__content-bot">
				<div class="product-price">
				    <div class="product-price__old">126 470 р.</div>
				    <div class="product-price__current">124 000 р.</div>
				</div>
				<div class="catalog__buy">
				    <div class="catalog__buy-count">
					<div class="input-group">
					    <div class="input-group__item catalog__buy-input">
						<input class="input-text input-text--sm text-center" type="text" value="1">
					    </div>
					    <div class="input-group__item catalog__buy-label">
						<div class="input-posfix">шт.</div>
					    </div>
					</div>
				    </div>
				    <div class="catalog__buy-btn">
					<button class="btn btn--sm" type="button">
					    <span>В корзину</span>
					</button>
				    </div>
				</div>
			    </div>
			</div>
                    </div>
		</div>
		<div class="catalog__item grid__item">
                    <div class="catalog__item-in">
			<div class="catalog__img">
			    <a class="link-hidden" href="product.html">
				<img src="static/images/upload/catalog/3.jpg?v=80aab8d0" width="200" height="200" alt="Термостатическая головка с выносным накладным датчиком">
			    </a>
			</div>
			<div class="catalog__content">
			    <div class="catalog__content-top">
				<div class="catalog__title">
				    <a class="link-hidden" href="product.html">Термостатическая головка с выносным накладным датчиком</a>
				</div>
				<div class="catalog__brand">Бренд: JW
				    <div class="catalog__avail">
					<span class="text-warn">●</span> нет наличии</div>
				</div>
			    </div>
			    <div class="catalog__content-bot">
				<div class="product-price">
				    <div class="product-price__old">126 470 р.</div>
				    <div class="product-price__current">124 000 р.</div>
				</div>
				<div class="catalog__buy">
				    <div class="catalog__buy-count">
					<div class="input-group">
					    <div class="input-group__item catalog__buy-input">
						<input class="input-text input-text--sm text-center" type="text" value="1">
					    </div>
					    <div class="input-group__item catalog__buy-label">
						<div class="input-posfix">шт.</div>
					    </div>
					</div>
				    </div>
				    <div class="catalog__buy-btn">
					<button class="btn btn--sm" type="button">
					    <span>В корзину</span>
					</button>
				    </div>
				</div>
			    </div>
			</div>
                    </div>
		</div>
		<div class="catalog__item grid__item">
                    <div class="catalog__item-in">
			<div class="catalog__img">
			    <a class="link-hidden" href="product.html">
				<img src="static/images/upload/catalog/4.jpg?v=a7f0ede8" width="200" height="200" alt="Термостатическая головка с выносным накладным датчиком">
			    </a>
			</div>
			<div class="catalog__content">
			    <div class="catalog__content-top">
				<div class="catalog__title">
				    <a class="link-hidden" href="product.html">Термостатическая головка с выносным накладным датчиком</a>
				</div>
				<div class="catalog__brand">Бренд: JW
				    <div class="catalog__avail">
					<span class="text-success">●</span> в наличии</div>
				</div>
			    </div>
			    <div class="catalog__content-bot">
				<div class="product-price">
				    <div class="product-price__old">126 470 р.</div>
				    <div class="product-price__current">124 000 р.</div>
				</div>
				<div class="catalog__buy">
				    <div class="catalog__buy-count">
					<div class="input-group">
					    <div class="input-group__item catalog__buy-input">
						<input class="input-text input-text--sm text-center" type="text" value="1">
					    </div>
					    <div class="input-group__item catalog__buy-label">
						<div class="input-posfix">шт.</div>
					    </div>
					</div>
				    </div>
				    <div class="catalog__buy-btn">
					<button class="btn btn--sm" type="button">
					    <span>В корзину</span>
					</button>
				    </div>
				</div>
			    </div>
			</div>
                    </div>
		</div>
		<div class="catalog__item grid__item">
                    <div class="catalog__item-in">
			<div class="catalog__img">
			    <a class="link-hidden" href="product.html">
				<img src="static/images/upload/catalog/5.jpg?v=02f4017c" width="200" height="200" alt="Термостатическая головка с выносным накладным датчиком">
			    </a>
			</div>
			<div class="catalog__content">
			    <div class="catalog__content-top">
				<div class="catalog__title">
				    <a class="link-hidden" href="product.html">Термостатическая головка с выносным накладным датчиком</a>
				</div>
				<div class="catalog__brand">Бренд: JW
				    <div class="catalog__avail">
					<span class="text-success">●</span> в наличии</div>
				</div>
			    </div>
			    <div class="catalog__content-bot">
				<div class="product-price">
				    <div class="product-price__old">126 470 р.</div>
				    <div class="product-price__current">124 000 р.</div>
				</div>
				<div class="catalog__buy">
				    <div class="catalog__buy-count">
					<div class="input-group">
					    <div class="input-group__item catalog__buy-input">
						<input class="input-text input-text--sm text-center" type="text" value="1">
					    </div>
					    <div class="input-group__item catalog__buy-label">
						<div class="input-posfix">шт.</div>
					    </div>
					</div>
				    </div>
				    <div class="catalog__buy-btn">
					<button class="btn btn--sm" type="button">
					    <span>В корзину</span>
					</button>
				    </div>
				</div>
			    </div>
			</div>
                    </div>
		</div>
		<div class="catalog__item grid__item">
                    <div class="catalog__item-in">
			<div class="catalog__img">
			    <a class="link-hidden" href="product.html">
				<img src="static/images/upload/catalog/6.jpg?v=a309ee1a" width="200" height="200" alt="Термостатическая головка с выносным накладным датчиком">
			    </a>
			</div>
			<div class="catalog__content">
			    <div class="catalog__content-top">
				<div class="catalog__title">
				    <a class="link-hidden" href="product.html">Термостатическая головка с выносным накладным датчиком</a>
				</div>
				<div class="catalog__brand">Бренд: JW
				    <div class="catalog__avail">
					<span class="text-success">●</span> в наличии</div>
				</div>
			    </div>
			    <div class="catalog__content-bot">
				<div class="product-price">
				    <div class="product-price__old">126 470 р.</div>
				    <div class="product-price__current">124 000 р.</div>
				</div>
				<div class="catalog__buy">
				    <div class="catalog__buy-count">
					<div class="input-group">
					    <div class="input-group__item catalog__buy-input">
						<input class="input-text input-text--sm text-center" type="text" value="1">
					    </div>
					    <div class="input-group__item catalog__buy-label">
						<div class="input-posfix">шт.</div>
					    </div>
					</div>
				    </div>
				    <div class="catalog__buy-btn">
					<button class="btn btn--sm" type="button">
					    <span>В корзину</span>
					</button>
				    </div>
				</div>
			    </div>
			</div>
                    </div>
		</div>
		<div class="catalog__item grid__item">
                    <div class="catalog__item-in">
			<div class="catalog__img">
			    <a class="link-hidden" href="product.html">
				<img src="static/images/upload/catalog/7.jpg?v=f5cbb70d" width="200" height="200" alt="Термостатическая головка с выносным накладным датчиком">
			    </a>
			</div>
			<div class="catalog__content">
			    <div class="catalog__content-top">
				<div class="catalog__title">
				    <a class="link-hidden" href="product.html">Термостатическая головка с выносным накладным датчиком</a>
				</div>
				<div class="catalog__brand">Бренд: JW
				    <div class="catalog__avail">
					<span class="text-success">●</span> в наличии</div>
				</div>
			    </div>
			    <div class="catalog__content-bot">
				<div class="product-price">
				    <div class="product-price__current">124 000 р.</div>
				</div>
				<div class="catalog__buy">
				    <div class="catalog__buy-count">
					<div class="input-group">
					    <div class="input-group__item catalog__buy-input">
						<input class="input-text input-text--sm text-center" type="text" value="1">
					    </div>
					    <div class="input-group__item catalog__buy-label">
						<div class="input-posfix">шт.</div>
					    </div>
					</div>
				    </div>
				    <div class="catalog__buy-btn">
					<button class="btn btn--sm" type="button">
					    <span>В корзину</span>
					</button>
				    </div>
				</div>
			    </div>
			</div>
                    </div>
		</div>
		<div class="catalog__item grid__item">
                    <div class="catalog__item-in">
			<div class="catalog__img">
			    <a class="link-hidden" href="product.html">
				<img src="static/images/upload/catalog/0.jpg?v=be80f03e" width="200" height="200" alt="Термостатическая головка с выносным накладным датчиком">
			    </a>
			</div>
			<div class="catalog__content">
			    <div class="catalog__content-top">
				<div class="catalog__title">
				    <a class="link-hidden" href="product.html">Термостатическая головка с выносным накладным датчиком</a>
				</div>
				<div class="catalog__brand">Бренд: JW
				    <div class="catalog__avail">
					<span class="text-success">●</span> в наличии</div>
				</div>
			    </div>
			    <div class="catalog__content-bot">
				<div class="product-price">
				    <div class="product-price__old">126 470 р.</div>
				    <div class="product-price__current">124 000 р.</div>
				</div>
				<div class="catalog__buy">
				    <div class="catalog__buy-count">
					<div class="input-group">
					    <div class="input-group__item catalog__buy-input">
						<input class="input-text input-text--sm text-center" type="text" value="1">
					    </div>
					    <div class="input-group__item catalog__buy-label">
						<div class="input-posfix">шт.</div>
					    </div>
					</div>
				    </div>
				    <div class="catalog__buy-btn">
					<button class="btn btn--sm" type="button">
					    <span>В корзину</span>
					</button>
				    </div>
				</div>
			    </div>
			</div>
                    </div>
		</div>
		<div class="catalog__item grid__item">
                    <div class="catalog__item-in">
			<div class="catalog__img">
			    <a class="link-hidden" href="product.html">
				<img src="static/images/upload/catalog/1.jpg?v=72bc5a49" width="200" height="200" alt="Термостатическая головка с выносным накладным датчиком">
			    </a>
			</div>
			<div class="catalog__content">
			    <div class="catalog__content-top">
				<div class="catalog__title">
				    <a class="link-hidden" href="product.html">Термостатическая головка с выносным накладным датчиком</a>
				</div>
				<div class="catalog__brand">Бренд: JW
				    <div class="catalog__avail">
					<span class="text-success">●</span> в наличии</div>
				</div>
			    </div>
			    <div class="catalog__content-bot">
				<div class="product-price">
				    <div class="product-price__current">124 000 р.</div>
				</div>
				<div class="catalog__buy">
				    <div class="catalog__buy-count">
					<div class="input-group">
					    <div class="input-group__item catalog__buy-input">
						<input class="input-text input-text--sm text-center" type="text" value="1">
					    </div>
					    <div class="input-group__item catalog__buy-label">
						<div class="input-posfix">шт.</div>
					    </div>
					</div>
				    </div>
				    <div class="catalog__buy-btn">
					<button class="btn btn--sm" type="button">
					    <span>В корзину</span>
					</button>
				    </div>
				</div>
			    </div>
			</div>
                    </div>
		</div>
		<div class="catalog__item grid__item">
                    <div class="catalog__item-in">
			<div class="catalog__img">
			    <a class="link-hidden" href="product.html">
				<img src="static/images/upload/catalog/2.jpg?v=46f24cb1" width="200" height="200" alt="Термостатическая головка с выносным накладным датчиком">
			    </a>
			</div>
			<div class="catalog__content">
			    <div class="catalog__content-top">
				<div class="catalog__title">
				    <a class="link-hidden" href="product.html">Термостатическая головка с выносным накладным датчиком</a>
				</div>
				<div class="catalog__brand">Бренд: JW
				    <div class="catalog__avail">
					<span class="text-success">●</span> в наличии</div>
				</div>
			    </div>
			    <div class="catalog__content-bot">
				<div class="product-price">
				    <div class="product-price__old">126 470 р.</div>
				    <div class="product-price__current">124 000 р.</div>
				</div>
				<div class="catalog__buy">
				    <div class="catalog__buy-count">
					<div class="input-group">
					    <div class="input-group__item catalog__buy-input">
						<input class="input-text input-text--sm text-center" type="text" value="1">
					    </div>
					    <div class="input-group__item catalog__buy-label">
						<div class="input-posfix">шт.</div>
					    </div>
					</div>
				    </div>
				    <div class="catalog__buy-btn">
					<button class="btn btn--sm" type="button">
					    <span>В корзину</span>
					</button>
				    </div>
				</div>
			    </div>
			</div>
                    </div>
		</div>
		<div class="catalog__item grid__item">
                    <div class="catalog__item-in">
			<div class="catalog__img">
			    <a class="link-hidden" href="product.html">
				<img src="static/images/upload/catalog/3.jpg?v=80aab8d0" width="200" height="200" alt="Термостатическая головка с выносным накладным датчиком">
			    </a>
			</div>
			<div class="catalog__content">
			    <div class="catalog__content-top">
				<div class="catalog__title">
				    <a class="link-hidden" href="product.html">Термостатическая головка с выносным накладным датчиком</a>
				</div>
				<div class="catalog__brand">Бренд: JW
				    <div class="catalog__avail">
					<span class="text-warn">●</span> нет наличии</div>
				</div>
			    </div>
			    <div class="catalog__content-bot">
				<div class="product-price">
				    <div class="product-price__old">126 470 р.</div>
				    <div class="product-price__current">124 000 р.</div>
				</div>
				<div class="catalog__buy">
				    <div class="catalog__buy-count">
					<div class="input-group">
					    <div class="input-group__item catalog__buy-input">
						<input class="input-text input-text--sm text-center" type="text" value="1">
					    </div>
					    <div class="input-group__item catalog__buy-label">
						<div class="input-posfix">шт.</div>
					    </div>
					</div>
				    </div>
				    <div class="catalog__buy-btn">
					<button class="btn btn--sm" type="button">
					    <span>В корзину</span>
					</button>
				    </div>
				</div>
			    </div>
			</div>
                    </div>
		</div>
	    </div>
	    <nav class="pagination">
		<ul class="pagination__list">
                    <!-- Выводить не более 7 элементов, включая разделитель-->
                    <li class="pagination__item">
			<a class="pagination__link" href="#">
			    <span class="pagination__title">1</span>
			</a>
                    </li>
                    <li class="pagination__item pagination__item--current">
			<span class="pagination__title">2</span>
                    </li>
                    <li class="pagination__item">
			<a class="pagination__link" href="#">
			    <span class="pagination__title">3</span>
			</a>
                    </li>
                    <li class="pagination__item">
			<a class="pagination__link" href="#">
			    <span class="pagination__title">4</span>
			</a>
                    </li>
                    <li class="pagination__item pagination__item--dots">
			<span class="pagination__title">...</span>
                    </li>
                    <li class="pagination__item">
			<a class="pagination__link" href="#">
			    <span class="pagination__title">24</span>
			</a>
                    </li>
                    <li class="pagination__item">
			<a class="pagination__link" href="#">
			    <span class="pagination__title">25</span>
			</a>
                    </li>
		</ul>
	    </nav>
	</main>
	<aside class="layout__aside">
	    <form class="filter" novalidate="novalidate" id="filter" action="/">
		<!-- Появление "бегунка" в src/scripts/app/filter,js-->
		<a id="filter-bubble" class="filter__bubble" href="#" style="display:none;">Выбрано
                    <div class="filter__bubble-count">24</div>
                    <div class="filter__bubble-call">показать</div>
		</a>
		<div class="filter__heading">Фильтр
                    <button class="filter__close btn-close js-toggle-filters-sm" type="button"></button>
		</div>
		<div class="filter__filters">
                    <div class="filter__b">
			<div class="filter__title filter__title--big title-line js-title-line">Автоматика</div>
			<div class="filter__content scroll-block js-scroll-block">
			    <div class="filter__item filter__item--narrow">
				<a class="filter-link" href="#">Контроллеры</a>
			    </div>
			    <div class="filter__item filter__item--narrow">
				<a class="filter-link" href="#">Пеллетные установки</a>
			    </div>
			    <div class="filter__item filter__item--narrow">
				<a class="filter-link" href="#">Погодозависимые системы</a>
			    </div>
			    <div class="filter__item filter__item--narrow">
				<a class="filter-link" href="#">Сервоприводы</a>
			    </div>
			    <div class="filter__item filter__item--narrow">
				<a class="filter-link" href="#">Термоголовки</a>
			    </div>
			    <div class="filter__item filter__item--narrow">
				<span class="filter-link is-current">Термостаты</span>
			    </div>
			    <div class="filter__item filter__item--narrow">
				<a class="filter-link" href="#">Турбо-насадки</a>
			    </div>
			</div>
                    </div>
                    <div class="filter__b">
			<div class="filter__title">Цена</div>
			<div class="filter__content filter__content--fluid">
			    <div class="range-input js-range" data-range-min="0" data-range-max="32000" data-range-step="1000">
				<!-- Пример настроек для целых чисел в слайдере: data-range-min="1" data-range-max="20" data-range-step="1"-->
				<!-- Неформатированные значения - в hidden полях-->
				<div class="range-input__fields row">
				    <div class="col-6">
					<div class="input-group">
					    <div class="input-group__item input-group__item--grow">
						<input class="input-text input-text--sm js-range-min" type="text" value="2000">
						<input class="js-range-min-val" type="hidden">
					    </div>
					    <div class="input-group__item range-input__label">р.</div>
					</div>
				    </div>
				    <div class="col-6">
					<div class="input-group">
					    <div class="input-group__item input-group__item--grow">
						<input class="input-text input-text--sm js-range-max" type="text" value="28000">
						<input class="js-range-max-val" type="hidden">
					    </div>
					    <div class="input-group__item range-input__label">р.</div>
					</div>
				    </div>
				</div>
				<div class="range-input__range">
				    <div class="js-range-scale"></div>
				</div>
			    </div>
			</div>
                    </div>
                    <div class="filter__b">
			<div class="filter__title">Применение</div>
			<div class="filter__content scroll-block js-scroll-block">
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox" checked="checked">
				    <span class="checkbox__label">Охлаждение (18)</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Обогрев, Только (18)</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Один Этап (Нагрев / Охлаждение) (172)</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox" checked="checked">
				    <span class="checkbox__label">Многоступенчатые (Нагрев/Охлаждение) (198)</span>
				</label>
			    </div>
			</div>
                    </div>
                    <div class="filter__b">
			<div class="filter__title">Доступность программировать</div>
			<div class="filter__content scroll-block js-scroll-block">
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Охлаждение (18)</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Обогрев, Только (18)</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Один Этап (Нагрев / Охлаждение) (172)</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Многоступенчатые (Нагрев/Охлаждение) (198)</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Охлаждение (18)</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Обогрев, Только (18)</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox" checked="checked">
				    <span class="checkbox__label">Один Этап (Нагрев / Охлаждение) (172)</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Многоступенчатые (Нагрев/Охлаждение) (198)</span>
				</label>
			    </div>
			</div>
                    </div>
                    <div class="filter__b">
			<div class="filter__title">Бренд</div>
			<div class="filter__content scroll-block scroll-block--big js-scroll-block">
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Lemax</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox" checked="checked">
				    <span class="checkbox__label">Fondital</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox" checked="checked">
				    <span class="checkbox__label">Thermex</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Vaillant</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Valtec</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Baxi</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Atem</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">DAB</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Wilo</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Eko</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Lemax</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Fondital</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Thermex</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Vaillant</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Valtec</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Baxi</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Atem</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">DAB</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Wilo</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Eko</span>
				</label>
			    </div>
			    <div class="filter__item">
				<label class="checkbox">
				    <input class="checkbox__input" type="checkbox">
				    <span class="checkbox__label">Lemax</span>
				</label>
			    </div>
			    <div class="filter__b"></div>
			</div>
                    </div>
		</div>
		<div class="filter__submit">
                    <div class="grid grid--xs-2">
			<div class="grid__item">
			    <button class="btn btn--sm btn--fluid btn--inverse" type="submit">
				<span>Показать</span>
			    </button>
			</div>
			<div class="grid__item">
			    <button class="btn btn--sm btn--fluid" type="reset">
				<span>Сбросить</span>
			    </button>
			</div>
                    </div>
		</div>
	    </form>
	</aside>
    </div>
</div>