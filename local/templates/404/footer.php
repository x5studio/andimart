</div></div>
<footer class="page__footer">
    <div class="footer">
        <div class="container">
            <div class="footer__bottom">
                <div class="row">
                    <div class="col-12 col-md-6"><p style="text-align: justify;">©&nbsp;2016 — <?= date("Y") ?>.
                            Andimart.ru</p></div>
                </div>
            </div>
        </div>
    </div>
</footer></div>
<?/*
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter45162276 = new Ya.Metrika({
                    id:45162276,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/45162276" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
*/?>
</body>
</html>