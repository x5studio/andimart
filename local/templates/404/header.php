<!DOCTYPE html>
<html class="no-js" lang="ru-RU" prefix="og: http://ogp.me/ns#">
<head><? $APPLICATION->ShowHead() ?><title><? $APPLICATION->ShowTitle() ?></title>
    <link href="/local/templates/404/template_styles.css" rel="stylesheet">
</head>
<body>111
<div class="page">
    <header class="page__header">
        <div id="header" class="header">
            <div class="container">
                <div class="header__mid">
                    <div class="header__logo">
                        <div class="logo"><a class="logo__link" href="/"><img class="logo__image"
                                                                              src="/local/templates/404/logo.png"
                                                                              width="325" height="90"
                                                                              alt="ЭНДИМАРТ"></a></div>
                    </div>
                    <div class="header__elems">
                        <div class="header__elem header__elem--tel"><a class="header__tel" href="/o_companii/contacti/"><img
                                        src="/local/templates/404/phone.png" width="390" height="62" alt="ТЕЛЕФОН"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="page__content">
        <div class="container">
            <div class="content-block content-block--pull-top island">