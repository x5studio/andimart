/**
 * Utility functions
 */

;(function (window) {
  'use strict';

  var Utils = function () {
    this.domReady = false;
  };

  /**
   * Crossbrowser document.ready
   * @param handler {function}
   * @param params {object}
   */

  Utils.prototype.bindReady = function (handler, params) {
    var called = false;
    var utils = this;

    if (this.domReady) {
      return handler(params);
    }

    function ready() {
      if (called) return;
      called = true;
      utils.domReady = true;
      handler(params);
    }

    function tryScroll() {
      if (called) return;
      if (!document.body) return;
      try {
        document.documentElement.doScroll('left');
        ready();
      } catch (e) {
        setTimeout(tryScroll, 0);
      }
    }

    if (document.addEventListener) {
      document.addEventListener('DOMContentLoaded', function () {
        ready();
      }, false);
    } else if (document.attachEvent) {
      if (document.documentElement.doScroll && window === window.top) {
        tryScroll();
      }
      document.attachEvent('onreadystatechange', function () {
        if (document.readyState === 'complete') {
          ready();
        }
      });
    }
    if (window.addEventListener) {
      window.addEventListener('load', ready, false);
    } else if (window.attachEvent) {
      window.attachEvent('onload', ready);
    }
  };


  /**
   * Wait for complete repeating event
   * http://stackoverflow.com/questions/2854407/javascript-jquery-window-resize-how-to-fire-after-the-resize-is-completed
   */

  Utils.prototype.waitFinalEvent = (function () {
    var timers = {};
    return function (callback, ms, uniqueId) {
      if (!uniqueId) {
        uniqueId = 'timer' + new Date().getTime();
      }
      if (timers[uniqueId]) {
        clearTimeout(timers[uniqueId]);
      }
      timers[uniqueId] = setTimeout(callback, ms);
    };
  })();


  /**
   * requestAnimationFrame wrapper from Marijn Haverbeke's Eloquent JavaScript
   * @param frameFunc
   */

  Utils.prototype.runAnimation = function (frameFunc) {
    var requestAnimationFrame =
      window.requestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.msRequestAnimationFrame;
    var lastTime = null;
    function frame(time) {
      var stop = false;
      if (lastTime != null) {
        var timerStep = Math.min(time - lastTime, 100);
        stop = frameFunc(timerStep) === false;
      }
      lastTime = time;
      if (!stop) requestAnimationFrame(frame);
    }
    requestAnimationFrame(frame);
  };

  window['Utils'] = new Utils();
})(window);
