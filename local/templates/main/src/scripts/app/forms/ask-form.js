$(function () {
  'use strict';

  $('body').on('submit', '.js-ask-form', function (e) {
    e.preventDefault();
    var $form = $(this);

    // Для корректной работы с загруженными по аякс формами
    var validator = $form.data('validator') || FormValidator($form);
    validator.activate();

    var validation = validator.validate();
    var $msg = $form.find('.js-form-error');
    $msg.text('').hide();

    if (!validation.isValid) {
      if (validation.invalidFields) {
        var text = 'Не корректно заполнены или не заполнены поля: ';
        validation.invalidFields.forEach(function (item, i, arr) {
          console.log(i);
          text += $(item).data('field-name');
          if (i < arr.length - 1) text += ', ';
          $msg.text(text);
          $msg.show();
        });
      }
      return false;
    }

    // TODO: запрос на сервер (см. пример в subscribe-form.js)
  });
});
