/**
 * Form validator
 * Required: https://github.com/chriso/validator.js
 */

(function (window) {

  var FormValidator = function ($form) {
    if (!(this instanceof FormValidator)) {
      return new FormValidator($form);
    }

    var that = this;
    that.$form = $form;
    that.$fields = $form.find(':input');
    that.toggleClasses = false;

    $form.attr('novalidate', true);
    $form.data('validator', this);

    $form.on('change input', function (e) {
      that.checkField(e.target);
    });

    return this;
  };


  /**
   * Field validation
   * Support input and textarea, tests: required, email type
   * TODO: add new validation features: min-length, max-length, equals, pattern, etc
   * @param field
   */

  FormValidator.prototype.checkField = function (field) {
    var $field = $(field);
    var isRequired = $field.attr('required') !== undefined;
    var isEmail = $field.attr('type') === 'email';
    var value = $field.val();
    var valid = (!isRequired || value) && (!isEmail || validator.isEmail(value));

    $field.data('validator.valid', valid);

    if (this.toggleClasses) {
      $field.toggleClass('is-invalid', !valid);
      $field.toggleClass('is-valid', valid);
    }
    return valid;
  };


  /**
   * Activate adding 'is-invalid / is-valid' classes to fields
   */

  FormValidator.prototype.activate = function () {
    this.toggleClasses = true;
  };


  /**
   * Validate form and return all invalid fields
   * @returns {{isValid: boolean, invalidFields: Array}}
   */

  FormValidator.prototype.validate = function () {
    var invalidFields = [];
    var that = this;
    that.$fields.each(function () {
      if (!that.checkField(this)) {
        invalidFields.push(this);
      }
    });
    return {
      isValid: invalidFields.length < 1,
      invalidFields: invalidFields
    };
  };

  window['FormValidator'] = FormValidator;
})(window);


/**
 * Form validation sample
 * Add novalidate attr to forms!
 */


$(function () {
  'use strict';

  $('body').on('submit', '.js-form-validator', function (e) {
    var $form = $(this);

    // Для корректной работы с загруженными по аякс формами
    var validator = $form.data('validator') || FormValidator($form);
    validator.activate();

    var validation = validator.validate();
    if (!validation.isValid) {
      e.preventDefault();
      console.log(validation.invalidFields);
    }
  });
});
