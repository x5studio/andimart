$(function () {
  'use strict';

  var $subscribeForm = $('.js-subscribe-form');
  var validator = FormValidator($subscribeForm);

  $subscribeForm.on('submit', function (e) {
    e.preventDefault();
    var $form = $(this);
    var $submit = $form.find('button[type=submit]');
    var $field = $form.find('input[name=email]');
    validator.activate();
    var validation = validator.validate();

    if (validation.isValid) {
      $submit.attr('disabled', true);

      var request = $.post(
        $form.attr('action'),
        {
          email: $field.val()
        })
        .done(function (data) {
          if (data.success || data.id) { // todo: удалить data.id! это возвращает заглушка для теста
            Popups.showMessage({
              title: data.messageTitle || 'Подписка оформлена',
              text: data.message || 'Спасибо за проявленный интерес. Теперь вы будет первым узнавать о всех новинках, акциях, конкурсах!'
            });
            $field.val('');
          } else {
            // например, если не прошла валидация на сервере
            Popups.showMessage({
              title: data.messageTitle || 'Ошибка',
              text: data.message || 'Попробуйте повторить попытку позднее.'
            });
          }
        })
        .fail(function () {
          Popups.showMessage({
            title: data.messageTitle || 'Ошибка',
            text: data.message || 'Попробуйте повторить попытку позднее.'
          });
        })
        .always(function () {
          $submit.attr('disabled', false);
        });
    }
  });
});
