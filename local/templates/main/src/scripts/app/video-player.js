/**
 * Init video players
 */

(function () {
  var VideoPlayer = function () {
    this.youTubeApiReady = false;
    this.youTubeApiLoading = false;
    this.youTubeApiWaitList = [];
    this.youTubePlayers = [];
  };

  VideoPlayer.prototype.get = function () {
    return this;
  };

  /**
   * Init video playes
   * @param id
   * @param target
   * @param service
   */

  VideoPlayer.prototype.init = function (id, target, service) {
    service = service || 'youtube';

    if (service == 'youtube') {
      this.pauseYouTubePlayers();
      this._createYoutubePlayer(id, target);
    }

  };

  /**
   * Pause all playing youtube players exclude backgrounds
   * @param exclude = player object that must not be stop
   */

  VideoPlayer.prototype.pauseYouTubePlayers = function (exclude) {
    for (var i = 0; i < this.youTubePlayers.length; i++) {
      if (this.youTubePlayers[i] != exclude) {
        this.youTubePlayers[i].pauseVideo();
      }
    }
  };


  /**
   * Create player
   * @param id
   * @param target
   * @param callback
   * @private
   */

  VideoPlayer.prototype._createYoutubePlayer = function (id, target, callback) {
    var players = this.youTubePlayers;
    var pausePlayers = this.pauseYouTubePlayers.bind(this);
    var createFunc = function () {
      var player = new YT.Player(target, {
        height: '100%',
        width: '100%',
        videoId: id,
        playerVars: {
          autohide: 1,
          showinfo: 0,
          autoplay: 1
        },
        events: {
          onReady: function (event) {
            if (callback) {
              callback();
            }
          },
          onStateChange: function (event) {
            if (event.data == 1) {
              pausePlayers(event.target);
            }
          }
        }
      });

      players.push(player);
      return player;
    };
    this._loadYoutubePlayer(createFunc);
  };


  /**
   * Load YouTube player
   * @param createFunc
   * @private
   */

  VideoPlayer.prototype._loadYoutubePlayer = function (createFunc) {
    this._loadYoutubeApi();
    if (this.youTubeApiReady) {
      createFunc()
    } else {
      this.youTubeApiWaitList.push(createFunc);
    }

  };


  /**
   * Load YouTube API asynchronously
   * @private
   */

  VideoPlayer.prototype._loadYoutubeApi = function () {

    if (this.youTubeApiReady) return;
    if (this.youTubeApiLoading) return;

    this.youTubeApiLoading = true;

    var that = this;
    window.onYouTubeIframeAPIReady = function () {
      that._onYouTubeApiReady()
    };

    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  };


  /**
   * Callback when YouTube API loaded
   * create all players
   * @private
   */

  VideoPlayer.prototype._onYouTubeApiReady = function () {
    this.youTubeApiReady = true;
    this.youTubeApiLoading = false;
    for (var i = 0; i < this.youTubeApiWaitList.length; i++) {
      this.youTubeApiWaitList[i]();
    }
  };

  window['VideoPlayer'] = new VideoPlayer;
})(window);
