/**
 * Contacts map
 */

$(function () {
  'use strict';

  var $map = $('#contacts-map');

  if ($map.length === 0) return;

  var center = $map.data('center') || [44.059654, 43.034953];
  var zoom = $map.data('zoom') || 16;
  var markers = $map.data('markers');
  var markerImage = $map.data('marker-image');
  var $citySelect = $('#contacts-map-city');
  var map;

  YamapsService.setApiParams({
    lang: 'ru_RU'
  });

  YamapsService.run(function () {
    map = new ymaps.Map($map[0], {
      center: center,
      zoom: zoom,
      controls: ['zoomControl']
    });

    $('#contacts-map-preloader').remove();

    var BalloonContentLayoutClass = ymaps.templateLayoutFactory.createClass(
      '<div class="map-baloon">' +
      '<div class="map-baloon__title">$[properties.title]</div>' +
      '<div class="map-baloon__text">$[properties.text]</div>' +
      '</div>'
    );

    if (markers && markers.length) {
      for (var i = 0; i < markers.length; i++) {
        var placemark = new ymaps.Placemark(
          markers[i].latlng,
          {
            title: markers[i].title,
            text: markers[i].text
          },
          {
            iconLayout: 'default#image',
            iconImageHref: markerImage,
            iconImageSize: [44, 54],
            iconImageOffset: [-22, -54],
            balloonContentLayout: BalloonContentLayoutClass
          }
        );
        map.geoObjects.add(placemark);
      }
    }

    if ($citySelect.length !== 0) {
      $citySelect.on('change', function () {
        var latlng = $(this).find(':selected').data('map-center');
        if (latlng) {
          map.setCenter(latlng);
        }
      })
    }

    YamapsService.fixScroll(map, { hoverDelay: 1000 });
  });
});
