$(function () {
  'use strict';

  /**
   * Steps
   */

  $('.js-checkout-steps-scroll').each(function () {
    var $this = $(this);
    var $current = $this.find('.checkout-steps__item--current');
    $this.perfectScrollbar({
      suppressScrollY: true
    });

    if ($current.length != 0) {
      $this.scrollLeft($current.position().left - 30);

      $(window).on('resize', function () {
        $this.perfectScrollbar('update');
        $this.scrollLeft($current.position().left - 30);
      })
    }
  });


  /**
   * Pickup select
   */

  $('.js-pickup').on('change', function (e) {
    var $wrap = $(this);
    $wrap.find('.js-pickup__content').animate({
      height: 'hide'
    });

    var $radio = $wrap.find('.js-pickup__radio:checked');
    var $item = $radio.closest('.js-pickup__item');
    var $content = $item.find('.js-pickup__content');
    var $map = $content.find('.js-pickup__map');

    $content.stop().animate({
      height: $radio.is(':checked') ? 'show' : 'hide'
    }, 300);

    if (!$map.hasClass('is-loaded')) {
      $map.addClass('is-loaded');

      var center = $map.data('center') || [44.059654, 43.034953];
      var zoom = $map.data('zoom') || 16;
      var marker = $map.data('marker');
      var markerImage = $map.data('marker-image');
      var map;

      YamapsService.setApiParams({
        lang: 'ru_RU'
      });

      YamapsService.run(function () {
        map = new ymaps.Map($map[0], {
          center: center,
          zoom: zoom,
          controls: ['zoomControl'],
          autoFitToViewport: 'always'
        });

        $map.find('.js-map-preloader').remove();

        if (marker && marker.latlng) {
          var placemark = new ymaps.Placemark(
            marker.latlng,
            {},
            {
              iconLayout: 'default#image',
              iconImageHref: markerImage,
              iconImageSize: [44, 54],
              iconImageOffset: [-22, -54]
            }
          );
          map.geoObjects.add(placemark);
        }

        YamapsService.fixScroll(map, { hoverDelay: 1000 });

        $map.data('map', map);
      });
    } else {
      $map.data('map').container.fitToViewport();
    }
  });
});
