/**
 * Carousels
 * https://github.com/kenwheeler/slick
 */

$(function () {
  'use strict';

  if (undefined !== $.fn.slick) {

    /**
     * Default carousel initialization
     */

    var initItemsCarousel = function ($wrap, slickConfig) {
      var $carousel = $wrap.find('.js-carousel__items');
      var $prev = $wrap.find('.js-carousel__prev');
      var $next = $wrap.find('.js-carousel__next');
      var $stat = $wrap.find('.js-carousel__stat');

      var defaultOptions = {
        mobileFirst: true,
        prevArrow: false,
        nextArrow: false
      };

      var options = $.extend(defaultOptions, slickConfig);

      function fillStat(slick, currentSlide) {
        var slideToShow = slick.options.slidesToShow;
        var slideCount = slick.slideCount;
        var pageCount = (slideCount - slideCount % slideToShow) / slideToShow;
        currentSlide = (currentSlide || 0) + 1;
        var currentPage = Math.ceil(currentSlide / slideToShow);
        $stat.text(currentPage + ' / ' + (pageCount + slideCount % slideToShow))
      }

      $carousel.on('afterChange', function (e, slick, currentSlide) {
        fillStat(slick, currentSlide);
      });

      $carousel.on('init', function (e, slick) {
        fillStat(slick);
      });

      $carousel.slick(options);

      $prev.on('click', function () {
        $carousel.slick('slickPrev');
      });

      $next.on('click', function () {
        $carousel.slick('slickNext');
      });

      return {
        $node: $carousel,
        options: options
      };
    };


    /**
     * Featured categories
     */

    $('.js-carousel--categories').each(function () {
      var instance = initItemsCarousel($(this), {
        slidesToShow: 2,
        slidesToScroll: 2,
        responsive: [
          {
            breakpoint: 768,
            settings: 'unslick'
          }
        ]
      });

      var $carousel = instance.$node;
      var options = instance.options;

      $(window).on('resize orientationChange', function (event) {
        Utils.waitFinalEvent(function () {
          var slick = $carousel.slick('getSlick');
          if (slick.unslicked && $.windowWidth() < 768) {
            $carousel.slick(options);
          }
        }, 300, 'recalcCategoriesCarousel');
      });
    });


    /**
     * Products
     */

    $('.js-carousel--catalog').each(function () {
      var $this = $(this);
      var rows = parseInt($this.data('rows')) || 1;

      var instance = initItemsCarousel($this, {
        slidesPerRow: 2,
        rows: 1,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              slidesPerRow: 3,
              rows: rows
            }
          },
          {
            breakpoint: 992,
            settings: {
              slidesPerRow: 4,
              rows: rows
            }
          }
        ]
      });

      var $carousel = instance.$node;
      var $items = $carousel.find('.catalog__item');

      // Костыль, чтобы сделать плиточки одинаковой высоты
      function recalcHeight() {
        var maxHeight = Math.max.apply(
          null,
          $.map($items, function(e) {
            $(e).css('height', '');
            return $(e).innerHeight();
          })
        );
        $items.css({
          height: maxHeight + 'px'
        })
      }

      setTimeout(function () {
        recalcHeight();
      }, 500);

      $carousel.on('breakpoint', recalcHeight);

      $(window).on('resize orientationChange', function () {
        Utils.waitFinalEvent(recalcHeight, 300, 'recalcProductsCarousel')
      });
    });


    /**
     * Brands
     */

    $('.js-carousel--brands').each(function () {
      var instance = initItemsCarousel($(this), {
        slidesToShow: 2,
        slidesToScroll: 2,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3
            }
          },
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 4
            }
          }
        ]
      });
    });


    /**
     * Aside banners
     */

    $('#js-aside-carousel').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: true,
      autoplay: true,
      autoplaySpeed: 5000,
      appendDots: $('#js-aside-carousel-dots')
    });


    /**
     * Hello slider
     */

    $('.js-hello-slider').each(function () {
      var $wrap = $(this);
      var $carousel = $wrap.find('.js-hello-slider__items');
      var $dots = $wrap.find('.js-hello-slider__dots');
      var $navs = $wrap.find('.js-hello-slider__nav');

      function changeNav(e, slick, currentSlide, nextSlide) {
        var slideIndex = nextSlide || 0; // If will be set start slide option - need rewrite

        $navs.filter('.is-active').removeClass('is-active');
        $navs.filter(function() {
          return ($(this).data('slide') == slideIndex)
        }).addClass('is-active');
      }

      $carousel.on('init beforeChange', changeNav);

      $navs.on('click', function (e) {
        e.preventDefault();
        $carousel.slick('slickGoTo', parseInt($(this).data('slide')));
      });

      $carousel.slick({
        mobileFirst: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        appendDots: $dots,
        autoplay: true,
        autoplaySpeed: 4000,
        responsive: [
          {
            breakpoint: 992,
            settings: {
              fade: true
            }
          }
        ]
      });
    });


    /**
     * Images slider (in articles)
     */

    $('.js-carousel--slider').each(function () {
      var instance = initItemsCarousel($(this), {
        slidesToShow: 1,
        slidesToScroll: 1
      });
    });


    /**
     * Portfolio carousel
     */

    $('.js-portfolio-carousel').each(function () {
      var $this = $(this);
      var slides = parseInt($this.data('slides')) || 3;
      $this.slick({
        mobileFirst: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              slidesToShow: slides,
              slidesToScroll: slides
            }
          }
        ]
      });
    });
  }
});
