/*!
 * yamaps-service
 * http://github.com/andrey-hohlov/yamaps-service
 * Copyright (c) 2016 Andrey Hohlov
 * Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
 * Version: 0.0.1
 */
(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define([], factory);
  } else if (typeof module === 'object' && module.exports) {
    // Node. Does not work with strict CommonJS, but
    // only CommonJS-like environments that support module.exports,
    // like Node.
    module.exports = factory();
  } else {
    // Browser globals (root is window)
    root.YamapsService = factory();
  }
}(this, function () {
  'use strict';

  function YamapsService() {
    this.queue = [];
    this.apiLoading = false;
    this.apiReady = false;
    this.apiParams = null;
  }

  YamapsService.prototype.run = function (func) {
    if (this.apiReady) {
      func();
    } else {
      this.queue.push(func);
      if (!this.apiLoading) {
        this.loadApi();
      }
    }
  };

  YamapsService.prototype.loadApi = function () {
    if (this.apiReady) return;

    this.apiLoading = true;

    var script = document.createElement('script');
    var src = 'https://api-maps.yandex.ru/2.1.50/?onload=YamapsService._apiLoaded';

    if (this.apiParams) {
      for (var param in this.apiParams) {
        if (param === 'onload') continue;
        src += '&' + param + '=' + this.apiParams[param];
      }
    }

    script.src = src;
    var fjs = document.getElementsByTagName('script')[0];
    fjs.parentNode.insertBefore(script, fjs);
  };

  YamapsService.prototype._apiLoaded = function () {
    this.apiReady = true;
    this.apiLoading = false;

    if (this.queue && this.queue.length) {
      this.queue.forEach(function (func) {
        func();
      });
      this.queue = null;
    }
  };

  YamapsService.prototype.fixScroll = function (map, params) {
    if (!map) return false;
    params = params || {};

    var hoverTimer;

    function disableZoom(map) {
      map.behaviors.disable(['scrollZoom', 'multiTouch', 'drag']);
    }

    function enableZoom(map) {
      map.behaviors.enable(['scrollZoom', 'multiTouch', 'drag']);
    }

    map.events.add('click', function () {
      enableZoom(map);
    });

    map.events.add('multitouchstart', function () {
      enableZoom(map);
    });

    map.events.add('mouseenter', function () {
      hoverTimer = setTimeout(function () {
        enableZoom(map);
      }, params.hoverDelay || 1000);
    });

    map.events.add('mouseleave', function () {
      clearTimeout(hoverTimer);
      disableZoom(map);
    });

    disableZoom(map);
  };

  YamapsService.prototype.setApiParams = function (params) {
    this.apiParams = params;
  };

  return new YamapsService();
}));
