<? global $vRegionSeoName, $vRegionSeoBrandName;
$str = explode('.', $_SERVER["SERVER_NAME"]);
$bCalc = !preg_match("/catalog\/calcs/i", $APPLICATION->GetCurPage(false));

if (count($str) > 2 && $bCalc) {
    if (!isset($_GET['q'])) {
        ?>
        <div class="content-block island subdomain">
            <div class="SEO_caption">
                <? if (strpos($APPLICATION->GetCurPage(), '/catalog/brands/') !== false): ?>
                    Продукция <?= $vRegionSeoBrandName . "&nbsp;" . $_SESSION["VREGIONS_REGION"]["WHERE"]; ?>.
                <? elseif ($APPLICATION->GetCurPage() === '/catalog/'): ?>
                    <?= $vRegionSeoName . "&nbsp;" . $_SESSION["VREGIONS_REGION"]["WHERE"]; ?>.
                <? else: ?>
                    <?= $vRegionSeoName . "&nbsp;" . $_SESSION["VREGIONS_REGION"]["WHERE"]; ?> по выгодной цене.
                <? endif; ?>
                <br>Есть вопросы? Звоните нам по телефону бесплатной горячей линии или приходите в один из наших
                партнерских <a href="/o_companii/contacti/">шоу-румов</a>! Опытные консультанты по подбору оборудования,
                инженеры и сервисные специалисты всегда рады помочь. Необходим профессиональный монтаж данного
                оборудования? К вашим услугам <a href="/masters/">каталог</a> мастеров.
            </div>
        </div>
    <? }
}
