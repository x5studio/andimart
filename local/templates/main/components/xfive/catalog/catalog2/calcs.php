<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$APPLICATION->AddChainItem('Калькуляторы', '/catalog/calcs/');
$APPLICATION->SetTitle('Калькуляторы');

$num = 0;
if (!empty($_POST) && !empty($_POST['boiler_output']))
{
	$num = 0;
}
elseif (!empty($_POST) && !empty($_POST['nasos_output']))
{
	$num = 1;
}
elseif (!empty($_POST) && !empty($_POST['nakopit_vod']))
{
	$num = 2;
}
elseif (!empty($_POST) && !empty($_POST['tepl_pol']))
{
	$num = 3;
}
elseif (!empty($_POST) && !empty($_POST['nasos']))
{
	$num = 4;
}
?>
<div class="content-block content-block--pull-top island ">
    <div class="content-block js-tabs" data-active="<?=$num?>">
            <div class="island-tabs js-island-tabs">
              <button class="island-tabs__scroll-l js-island-tabs__l" type="button"></button>
              <ul class="island-tabs__list js-island-tabs__scroll">
                <li class="island-tabs__item">
                  <a class="island-tabs__link js-tabs__btn" href="#calc1">
                    <span class="island-tabs__title">Калькулятор
                    <br>мощности котла</span>
                  </a>
                </li>
                <li class="island-tabs__item">
                  <a class="island-tabs__link js-tabs__btn" href="#calc2">
                    <span class="island-tabs__title">Калькулятор производительности
                    <br>циркуляционного насоса</span>
                  </a>
                </li>
                <li class="island-tabs__item">
                  <a class="island-tabs__link js-tabs__btn" href="#calc3">
                    <span class="island-tabs__title">Калькулятор оптимального объема
                    <br>накопительного водонагревателя</span>
                  </a>
                </li>
                <li class="island-tabs__item">
                  <a class="island-tabs__link js-tabs__btn" href="#calc4">
                    <span class="island-tabs__title">Калькулятор
                    <br>теплых полов</span>
                  </a>
                </li>
                <li class="island-tabs__item">
                  <a class="island-tabs__link js-tabs__btn" href="#calc5">
                    <span class="island-tabs__title">Калькулятор производительности 
                    <br>насосной станции для дома</span>
                  </a>
                </li>
              </ul>
              <button class="island-tabs__scroll-r js-island-tabs__r" type="button"></button>
            </div>
		<div class="content-block content-block--push-top js-tabs__content" id="calc1" data-x5-breadcrumb-name-after="Калькулятор мощности котла">
			<? include 'calcs/calc1.php'?>
		</div>
		<div class="content-block content-block--push-top js-tabs__content" id="calc2" data-x5-breadcrumb-name-after="Калькулятор производительности циркуляционного насоса">
			<? include 'calcs/calc2.php'?>
		</div>
		<div class="content-block content-block--push-top js-tabs__content" id="calc3" data-x5-breadcrumb-name-after="Калькулятор оптимального объема накопительного водонагревателя">
			<? include 'calcs/calc3.php'?>
		</div>
		<div class="content-block content-block--push-top js-tabs__content" id="calc4" data-x5-breadcrumb-name-after="Калькулятор теплых полов">
			<? include 'calcs/calc4.php'?>
		</div>
		<div class="content-block content-block--push-top js-tabs__content" id="calc5" data-x5-breadcrumb-name-after="Калькулятор производительности насосной станции для дома">
			<? include 'calcs/calc5.php'?>
		</div>
          </div>
          </div>
<?global $USER;
if ($USER->IsAdmin()){?>
<div class="content-block content-block--pull-top island ">
    <div class="content-block js-tabs" data-active="<?=$num?>">
        <div class="island-tabs js-island-tabs">
            <button class="island-tabs__scroll-l js-island-tabs__l" type="button"></button>
            <ul class="island-tabs__list js-island-tabs__scroll">
                <li class="island-tabs__item">
                    <a class="island-tabs__link js-tabs__btn" href="#calc1">
                    <span class="island-tabs__title">Калькулятор
                    <br>мощности котла</span>
                    </a>
                </li>
                <li class="island-tabs__item">
                    <a class="island-tabs__link js-tabs__btn" href="#calc2">
                    <span class="island-tabs__title">Калькулятор производительности
                    <br>циркуляционного насоса</span>
                    </a>
                </li>
                <li class="island-tabs__item">
                    <a class="island-tabs__link js-tabs__btn" href="#calc3">
                    <span class="island-tabs__title">Калькулятор оптимального объема
                    <br>накопительного водонагревателя</span>
                    </a>
                </li>
                <li class="island-tabs__item">
                    <a class="island-tabs__link js-tabs__btn" href="#calc4">
                    <span class="island-tabs__title">Калькулятор
                    <br>теплых полов</span>
                    </a>
                </li>
                <li class="island-tabs__item">
                    <a class="island-tabs__link js-tabs__btn" href="#calc5">
                    <span class="island-tabs__title">Калькулятор производительности
                    <br>насосной станции для дома</span>
                    </a>
                </li>
            </ul>
            <button class="island-tabs__scroll-r js-island-tabs__r" type="button"></button>
        </div>
        <div class="content-block content-block--push-top js-tabs__content" id="calc1" data-x5-breadcrumb-name-after="Калькулятор мощности котла">
            <? include 'calcs/calc1.php'?>
        </div>
        <div class="content-block content-block--push-top js-tabs__content" id="calc2" data-x5-breadcrumb-name-after="Калькулятор производительности циркуляционного насоса">
            <? include 'calcs/calc2.php'?>
        </div>
        <div class="content-block content-block--push-top js-tabs__content" id="calc3" data-x5-breadcrumb-name-after="Калькулятор оптимального объема накопительного водонагревателя">
            <? include 'calcs/calc3.php'?>
        </div>
        <div class="content-block content-block--push-top js-tabs__content" id="calc4" data-x5-breadcrumb-name-after="Калькулятор теплых полов">
            <? include 'calcs/calc4.php'?>
        </div>
        <div class="content-block content-block--push-top js-tabs__content" id="calc5" data-x5-breadcrumb-name-after="Калькулятор производительности насосной станции для дома">
            <? include 'calcs/calc5.php'?>
        </div>
    </div>
</div>
<?}?>
