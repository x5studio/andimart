<form novalidate="true" method="post" name="form2" action="/catalog/calcs/" class='js-form-validator'>
			    <? 
				if (!empty($_POST['nakopit_vod'])) 
				{
					$nakopit_vod['dush_cnt'] = (int)$_POST['nakopit_vod']['dush_cnt'];
					$nakopit_vod['bath_cnt'] = (int)$_POST['nakopit_vod']['bath_cnt'];
					
					$nakopit_vod['temp_hot'] = (int)$_POST['nakopit_vod']['temp_hot'];
					$nakopit_vod['temp_cold'] = (int)$_POST['nakopit_vod']['temp_cold'];
					$nakopit_vod['temp'] = (int)$_POST['nakopit_vod']['temp'];
					
					$nakopit_vod['result'] = false;
					$a = $nakopit_vod['dush_cnt']*50+$nakopit_vod['bath_cnt']*200; 
					$t = false;
					$t1 = false;
					$t2 = false;
					switch ($nakopit_vod['temp_hot'])
					{
						case 1: $t = 40; break;
						case 2: $t = 48; break;
						case 3: $t = 56; break;
					}
					switch ($nakopit_vod['temp_cold'])
					{
						case 1: $t1 = 5; break;
						case 2: $t1 = 15; break;
						case 3: $t1 = 25; break;
					}
					switch ($nakopit_vod['temp'])
					{
						case 1: $t2 = 60; break;
						case 2: $t2 = 70; break;
						case 3: $t2 = 80; break;
						case 4: $t2 = 90; break;
					}

					if ($a > 0 && $t !== false && $t1 !== false && $t2 !== false)
					{
						$nakopit_vod['result'] = $a*($t-$t1)/($t2-$t1);
					} 
					
//					[A*(T-T1)/(T2-T1)] 
					?>
				<? } ?>
				<div class="row">
				  <div class="col-sm-8">
				    <div class="content-block">
				      <div class="input-group">
					<div class="input-group__item h5">Количество душевых:</div>
					<div class="input-group__item input-group__item--padd">
					    <input class="input-text input-text--fixwidth" required="" name="nakopit_vod[dush_cnt]" type="text" value='<?=$nakopit_vod['dush_cnt']?>'>
					</div>
					<div class="input-group__item h5">шт</div>
				      </div>
				    </div>
				    <div class="content-block">
				      <div class="input-group">
					<div class="input-group__item h5">Количество ванных:</div>
					<div class="input-group__item input-group__item--padd">
					    <input class="input-text input-text--fixwidth" required="" name="nakopit_vod[bath_cnt]" type="text" value='<?=$nakopit_vod['bath_cnt']?>'>
					</div>
					<div class="input-group__item h5">шт</div>
				      </div>
				    </div>
                    <div class="content-block">
                      <div class="form__title text-left">Выберите требуемую по ощущениям температуру горячей воды на выходе из смесителя:</div>
                      <div class="check-list check-list--expand">
                        <div class="check-list__item">
                          <label class="radio">
                            <input class="radio__input" required="" type="radio" value='1' name="nakopit_vod[temp_hot]"<? if ($nakopit_vod['temp_hot'] == 1) { ?> checked=""<? } ?>>
                            <span class="radio__label">теплая (около 40°C)</span>
                          </label>
                        </div>
                        <div class="check-list__item">
                          <label class="radio">
                            <input class="radio__input" required="" type="radio" value='2' name="nakopit_vod[temp_hot]"<? if ($nakopit_vod['temp_hot'] == 2) { ?> checked=""<? } ?>>
                            <span class="radio__label">горячая (около 48°C)</span>
                          </label>
                        </div>
                        <div class="check-list__item">
                          <label class="radio">
                            <input class="radio__input" required="" type="radio" value='3' name="nakopit_vod[temp_hot]"<? if ($nakopit_vod['temp_hot'] == 3) { ?> checked=""<? } ?>>
                            <span class="radio__label">нестерпимо горячая (около 56°C)</span>
                          </label>
                        </div>
                      </div>
                    </div>
				      
				      
						<div class="content-block">
						  <div class="form__title text-left">Укажите примерную температуру воды с системе холодного водоснабжения:</div>
						  <div class="check-list check-list--expand">
						    <div class="check-list__item">
						      <label class="radio">
							<input class="radio__input" required="" type="radio" value='1' name="nakopit_vod[temp_cold]"<? if ($nakopit_vod['temp_cold'] == 1) { ?> checked=""<? } ?>>
							<span class="radio__label">ледяная (около 5°C)</span>
						      </label>
						    </div>
						    <div class="check-list__item">
						      <label class="radio">
							<input class="radio__input" required="" type="radio" value='2' name="nakopit_vod[temp_cold]"<? if ($nakopit_vod['temp_cold'] == 2) { ?> checked=""<? } ?>>
							<span class="radio__label">холодная (около 15°C)</span>
						      </label>
						    </div>
						    <div class="check-list__item">
						      <label class="radio">
							<input class="radio__input" required="" type="radio" value='3' name="nakopit_vod[temp_cold]"<? if ($nakopit_vod['temp_cold'] == 3) { ?> checked=""<? } ?>>
							<span class="radio__label">прохладная (около 25°C)</span>
						      </label>
						    </div>
						  </div>
						</div>
				      
                    <div class="content-block">
                      <div class="form__title text-left">Выберите температуру воды, поддерживаемую внутри водонагревателя:</div>
                      <div class="check-list check-list--expand">
                        <div class="check-list__item">
                          <label class="radio">
                            <input class="radio__input" required="" type="radio" value='1' name="nakopit_vod[temp]"<? if ($nakopit_vod['temp'] == 1) { ?> checked=""<? } ?>>
                            <span class="radio__label">60°C</span>
                          </label>
                        </div>
                        <div class="check-list__item">
                          <label class="radio">
                            <input class="radio__input" required="" type="radio" value='2' name="nakopit_vod[temp]"<? if ($nakopit_vod['temp'] == 2) { ?> checked=""<? } ?>>
                            <span class="radio__label">70°C</span>
                          </label>
                        </div>
                        <div class="check-list__item">
                          <label class="radio">
                            <input class="radio__input" required="" type="radio" value='3' name="nakopit_vod[temp]"<? if ($nakopit_vod['temp'] == 3) { ?> checked=""<? } ?>>
                            <span class="radio__label">80°C</span>
                          </label>
                        </div>
                        <div class="check-list__item">
                          <label class="radio">
                            <input class="radio__input" required="" type="radio" value='4' name="nakopit_vod[temp]"<? if ($nakopit_vod['temp'] == 4) { ?> checked=""<? } ?>>
                            <span class="radio__label">90°C</span>
                          </label>
                        </div>
                      </div>
                    </div>
	<div class="content-block">
	  <button class="btn btn--inverse" type="submit">
	    <span>Рассчитать</span>
	  </button>
	</div>
					</div>
					<div class="col-sm-4 hidden-xs-down text-center">
					  <img src="<?= SITE_TEMPLATE_PATH ?>/static/images/calc.svg?v=0aca4775" width="85" height="96" alt>
					</div>
				      </div>
				<? 
				if (!empty($nakopit_vod['result'])) 
				{
					?>
					<div class="content-block">
						<div class="calc-result">Оптимальный объем водонагревателя для Вашей системы равен <?=ceil($nakopit_vod['result'])?>л</div>
					</div>
					<div class="content-block">
						<a class="btn btn--fluid" href="/catalog/calcs/optimalnogo_obema_nakopitelnogo_vodonagrevatelya/filter/obem_l-from-<?=ceil($nakopit_vod['result'])?>/apply/">Посмотреть в каталоге
							<span class="hidden-xs-down">подходящие</span>
						</a>
					</div>
				<? } ?>
			</form>