<form novalidate="true" method="post" name="form1" action="/catalog/calcs/" class='js-form-validator'>
<? 
    if (!empty($_POST['boiler_output'])) 
    {
	    $boiler_output['area'] = (float)$_POST['boiler_output']['area'];
	    $boiler_output['degree'] = (int)$_POST['boiler_output']['degree'];
	    $boiler_output['zone'] = (int)$_POST['boiler_output']['zone'];
	    $boiler_output['result'] = false;
	    $b = false;
	    switch ($boiler_output['degree'])
	    {
		    case 1: $b = -20; break;
		    case 2: $b = 0; break;
		    case 3: $b = 20; break;
	    }
	    $c = false;
	    switch ($boiler_output['zone'])
	    {
		    case 1: $c = 40; break;
		    case 2: $c = 20; break;
		    case 3: $c = 0; break;
	    }

	    if ($boiler_output['area'] > 0 && $b !== false && $c !== false)
	    {
		    $a1 = $boiler_output['area']*0.1;
		    $boiler_output['result'] = $a1 + $a1*($b+$c)/100;
	    } ?>
    <? } ?>
    <div class="row">
      <div class="col-sm-8">
	<div class="content-block">
	  <div class="input-group">
	    <div class="input-group__item h5">Введите площадь Вашего дома:</div>
	    <div class="input-group__item input-group__item--padd">
		<input class="input-text input-text--fixwidth" required="" name="boiler_output[area]" type="text" value='<?=$boiler_output['area']?>'>
	    </div>
	    <div class="input-group__item h5">м<sup>2</sup></div>
	  </div>
	</div>
	<div class="content-block">
	  <div class="form__title text-left">Выберите степень теплоизоляции дома:</div>
	  <div class="check-list check-list--expand">
	    <div class="check-list__item">
	      <label class="radio">
		  <input class="radio__input" required="" type="radio" value='1' name="boiler_output[degree]"<? if ($boiler_output['degree'] == 1) { ?> checked=""<? } ?>>
		<span class="radio__label">хорошая</span>
	      </label>
	    </div>
	    <div class="check-list__item">
	      <label class="radio">
		<input class="radio__input" required="" type="radio" value='2' name="boiler_output[degree]"<? if ($boiler_output['degree'] == 2) { ?> checked=""<? } ?>>
		<span class="radio__label">средняя</span>
	      </label>
	    </div>
	    <div class="check-list__item">
	      <label class="radio">
		<input class="radio__input" required="" type="radio" value='3' name="boiler_output[degree]"<? if ($boiler_output['degree'] == 3) { ?> checked=""<? } ?>>
		<span class="radio__label">низкая</span>
	      </label>
	    </div>
	  </div>
	</div>
	<div class="content-block">
	  <div class="form__title text-left">Выберите свою климатическую зону:</div>
	  <div class="check-list check-list--expand">
	    <div class="check-list__item">
	      <label class="radio">
		<input class="radio__input" required="" type="radio" value='1' name="boiler_output[zone]"<? if ($boiler_output['zone'] == 1) { ?> checked=""<? } ?>>
		<span class="radio__label">средняя температура зимой -20°C</span>
	      </label>
	    </div>
	    <div class="check-list__item">
	      <label class="radio">
		<input class="radio__input" required="" type="radio" value='2' name="boiler_output[zone]"<? if ($boiler_output['zone'] == 2) { ?> checked=""<? } ?>>
		<span class="radio__label">средняя температура зимой: -10°C</span>
	      </label>
	    </div>
	    <div class="check-list__item">
	      <label class="radio">
		<input class="radio__input" required="" type="radio" value='3' name="boiler_output[zone]"<? if ($boiler_output['zone'] == 3) { ?> checked=""<? } ?>>
		<span class="radio__label">средняя температура зимой 0°C</span>
	      </label>
	    </div>
	  </div>
	</div>
	<div class="content-block">
	  <button class="btn btn--inverse" type="submit">
	    <span>Рассчитать</span>
	  </button>
	</div>
      </div>
      <div class="col-sm-4 hidden-xs-down text-center">
	<img src="<?= SITE_TEMPLATE_PATH ?>/static/images/calc.svg?v=0aca4775" width="85" height="96" alt>
      </div>
    </div>
    <? 
    if (!empty($boiler_output['result'])) 
    {
	    ?>
	    <div class="content-block">
		    <div class="calc-result">Требуемая ориентировочная мощность котла <?=ceil($boiler_output['result'])?>кВт</div>
	    </div>
	    <div class="content-block">
		    <a class="btn btn--fluid" href="/catalog/calcs/moshchnost_kotla/filter/moshchnost_kvt-from-<?=ceil($boiler_output['result'])?>/apply/">Посмотреть в каталоге
			    <span class="hidden-xs-down">подходящие</span>
		    </a>
	    </div>
    <? } ?>
  </form>