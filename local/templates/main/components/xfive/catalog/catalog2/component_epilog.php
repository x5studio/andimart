<?php

$basketRes = Bitrix\Sale\Internals\BasketTable::getList(array(
    'filter' => array(
        'FUSER_ID' => Bitrix\Sale\Fuser::getId(),
        'ORDER_ID' => null,
        'LID' => SITE_ID,
        'CAN_BUY' => 'Y',
    )
));

$arItems = [];

while ($item = $basketRes->fetch()) {
    $arItems[] = $item['PRODUCT_ID'];
}

?>

<script>

    var productsWithWrongBuyButtonState = JSON.parse('<?=json_encode($arItems)?>');

</script>
