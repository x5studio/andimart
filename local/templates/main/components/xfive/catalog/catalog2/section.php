<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */

/** @var CBitrixComponent $component */

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

global $isSection;
$isSection = true;
$this->setFrameMode(true);

if (false && strpos($arResult["VARIABLES"]['SMART_FILTER_PATH'], "/index.php")) {
    define("ERROR_404", "Y");
} else {
    if (strpos($APPLICATION->GetCurPage(), '/search/') !== false) {
        include 'search.php';
    } elseif ($APPLICATION->GetCurPage() == '/catalog/calcs/') {
        include 'calcs.php';
    } elseif (strpos($APPLICATION->GetCurPage(), '/calcs/') !== false) {
        include 'calcs_list.php';
    } elseif (strpos($APPLICATION->GetCurPage(), '/brands/') === false) {

    if (!isset($arParams['FILTER_VIEW_MODE']) || (string)$arParams['FILTER_VIEW_MODE'] == '')
            $arParams['FILTER_VIEW_MODE'] = 'VERTICAL';
        $arParams['USE_FILTER'] = (isset($arParams['USE_FILTER']) && $arParams['USE_FILTER'] == 'Y' ? 'Y' : 'N');

        $isVerticalFilter = ('Y' == $arParams['USE_FILTER'] && $arParams["FILTER_VIEW_MODE"] == "VERTICAL");
        $isSidebar = ($arParams["SIDEBAR_SECTION_SHOW"] == "Y" && isset($arParams["SIDEBAR_PATH"]) && !empty($arParams["SIDEBAR_PATH"]));
        $isFilter = ($arParams['USE_FILTER'] == 'Y');

        if ($isFilter) {
            $arFilter = array(
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "ACTIVE" => "Y",
                "GLOBAL_ACTIVE" => "Y",
            );
            if (0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
                $arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
            elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"])
                $arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];

            $obCache = new CPHPCache();
            if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog")) {
                $arCurSection = $obCache->GetVars();
            } elseif ($obCache->StartDataCache()) {
                $arCurSection = array();
                if (Loader::includeModule("iblock")) {
                    $dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID", 'IBLOCK_SECTION_ID', 'NAME'));

                    if (defined("BX_COMP_MANAGED_CACHE")) {
                        global $CACHE_MANAGER;
                        $CACHE_MANAGER->StartTagCache("/iblock/catalog");

                        if ($arCurSection = $dbRes->Fetch())
                            $CACHE_MANAGER->RegisterTag("iblock_id_" . $arParams["IBLOCK_ID"]);

                        $CACHE_MANAGER->EndTagCache();
                    } else {
                        if (!$arCurSection = $dbRes->Fetch())
                            $arCurSection = array();
                    }
                }

                $arCurSection['CAT_SECTIONS'] = array();
                if (!empty($arCurSection['IBLOCK_SECTION_ID'])) {
                    //    IBLOCK_SECTION_ID
                    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "SECTION_PAGE_URL"); //IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
                    $arFilter = Array("IBLOCK_ID" => const_IBLOCK_ID_catalog, 'SECTION_ID' => $arCurSection['IBLOCK_SECTION_ID'], 'ACTIVE' => 'Y');
                    $brands = array();
                    $res = CIBlockSection::GetList(Array('sort' => 'asc'), $arFilter, false, $arSelect);
                    while ($arFields = $res->GetNext()) {
                        $arFields['SELECTED'] = false;
                        if ($arCurSection['ID'] === $arFields['ID']) {
                            $arFields['SELECTED'] = true;
                        }
                        $arCurSection['CAT_SECTIONS'][$arFields["ID"]] = $arFields;
                    }
                }
                $obCache->EndDataCache($arCurSection);
            }
            if (!isset($arCurSection))
                $arCurSection = array();
        }
        ?>
        <?
//p($arResult["VARIABLES"]["SECTION_ID"]);
        $section = array();
        $subsction_list = array();
        $arFilter = array("ID" => $arResult["VARIABLES"]["SECTION_ID"], "IBLOCK_ID" => $arParams["IBLOCK_ID"], 'ACTIVE' => 'Y'); // выберет потомков без учета активности
        $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter, array("ELEMENT_SUBSECTIONS" => "Y", 'CNT_ACTIVE' => 'Y'), array("ID", "NAME", "IBLOCK_ID", "UF_SHORT", "UF_SEO_TEXT", "UF_LONG", "UF_DESCR", "DESCRIPTION", "IBLOCK_SECTION_ID", "CODE"));
        while ($arSect = $rsSect->GetNext()) {
            $section = $arSect;
        }
        if ($arResult["VARIABLES"]["SECTION_ID"] == "") {
            $section["NAME"] = "Каталог";
            $section["ID"] = "";
            $section["CODe"] = "";
        }

        $parsection = array(
            'NAME' => 'Каталог'
        );
        if (!empty($section["ID"])) {
            $arFilter = array("ID" => $section["IBLOCK_SECTION_ID"], "IBLOCK_ID" => $arParams["IBLOCK_ID"], 'ACTIVE' => 'Y'); // выберет потомков без учета активности
            $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter, false, array("ID", "NAME", "IBLOCK_ID", "UF_SEO_TEXT", "UF_SHORT", "UF_LONG", "DESCRIPTION", "IBLOCK_SECTION_ID", "CODE"));
            while ($arSect = $rsSect->GetNext()) {
                $parsection = $arSect;
            }
        }
// p($section);
        $arFilter = array("SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"], 'IBLOCK_ID' => $arParams["IBLOCK_ID"], 'ACTIVE' => 'Y',
            'GLOBAL_ACTIVE' => "Y",
            'CNT_ACTIVE' => true); // выберет потомков без учета активности
        $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter, array("ELEMENT_SUBSECTIONS" => "Y", 'CNT_ACTIVE' => 'Y'));
        while ($arSect = $rsSect->GetNext()) {
            $subsction_list[] = $arSect;
        }
//deb($subsction_list, false);

        $has_faq = CIBlockElement::GetList(array(), array('PROPERTY_SECTION' => $section["ID"], 'IBLOCK_ID' => const_IBLOCK_ID_faq, 'ACTIVE' => 'Y'), false, array('nTopCount' => 1));
        $has_faq = $has_faq->Fetch();

        $arCurSection['PARENT_NAME'] = $parsection['NAME'];
        ?>
        <?
        global $vRegionSeoName;

        if ($section["UF_SEO_TEXT"]) {
            $vRegionSeoName = $section["UF_SEO_TEXT"];
        } else $vRegionSeoName = "Каталог инженерных товаров для дома";

        if ($section["UF_DESCR"]) {
            ?>
            <div class="category-card__decsr">
	        <?= $section["UF_DESCR"] ?>
	        </div>
        <?}?>
        <?include($_SERVER["DOCUMENT_ROOT"] . "/" . $this->GetFolder() . "/section_vertical.php");?>
        <? $this->SetViewTarget('category_card_description'); ?>
        <? if ($APPLICATION->GetCurPage() != '/catalog/' && ($section["~UF_SHORT"] || $has_faq || $section["~UF_LONG"])) { ?>
            <div class="island category-card__more js-cls-tabs">
                <?if ($section["~UF_SHORT"]):?>
                    <h4 class="section-title title-line title-line--accent js-title-line is-animated">Краткий обзор</h4>
                    <div class="category-card__decsr">
                    <!-- Тут лучше разрешить только абзацы-->
                    <?= $section["~UF_SHORT"] ?>
                    </div>
                <?endif;?>
                <? if (!empty($has_faq)&&false):?>
                    <h4 class="section-title title-line title-line--accent js-title-line is-animated">Вопрос-ответ</h4>
                    <div class="category-card__decsr">
                        <div class="ajax-preloader js-tab-ajax-content" data-src="/dlya_pokupatelya/vopros-otvet/?RFT_AJAX=Y&SECTION_ID=<?= $section["ID"] ?>">
                        </div>
                    </div>
                <?endif;?>
                <?if ($section["~UF_LONG"]):?>
                    <div class="spacer"></div>
                    <h4 class="section-title title-line title-line--accent js-title-line is-animated">Больше о категории</h4>
                    <div class="category-card__decsr">
                    <?= $section["~UF_LONG"] ?>
                    </div>
                <?endif;?>
            </div>
        <? } ?>
        <? $this->EndViewTarget(); ?>
        <? $this->SetViewTarget('category_h1_additional'); ?>
        <? /*div class="category-card__links">
    <a class="category-card__link" href="/calculator/">
	<span class="category-card__link-i">
	    <img src="<?= SITE_TEMPLATE_PATH ?>/static/images/stuff/calc.svg?v=43f601c5" width="45" height="50" alt>
	</span>Калькулятор</a>
    <a class="category-card__link" href="#">
	<span class="category-card__link-i">
	    <img src="<?= SITE_TEMPLATE_PATH ?>/static/images/stuff/camera.svg?v=3e438440" width="60" height="45" alt>
	</span>Видео по теме</a>
</div*/
        ?>
        <? $this->EndViewTarget(); ?>


        <?
        $this->SetViewTarget('category_menu_additional');
        if (is_array($subsction_list) && !empty($subsction_list)) {
            ?>
            <div class="catalog-nav layout__aside--sticky js-sticky">
                <div class="catalog-nav__title title-line js-title-line"><?= $section["NAME"] ?></div>
                <ul class="catalog-nav__items">
                    <?
                    foreach ($subsction_list as $ar_section) {
                        ?>
                        <li class="catalog-nav__item">
                            <a class="catalog-nav__link"
                               href="<?= $ar_section["SECTION_PAGE_URL"] ?>"><?= $ar_section["NAME"] ?>
                                <span class="catalog-nav__count"><?/*&nbsp;(<?= $ar_section["ELEMENT_CNT"] ?>)*/
                                    ?></span>
                            </a>
                        </li>
                        <?
                    }
                    ?>
                </ul>
            </div>
            <?
        }
        ?>
        <? $this->EndViewTarget(); ?>
    <? } elseif (preg_match('/brands\/$/ui', $APPLICATION->GetCurPage())) { ?>
        <? include 'brands_list.php' ?>
    <? } elseif (strpos($APPLICATION->GetCurPage(), '/brands/') !== false) { ?>
        <? include 'brands_one.php' ?>
    <? } ?>


<? }//404?>

