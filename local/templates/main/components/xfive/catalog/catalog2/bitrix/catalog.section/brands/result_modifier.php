<? 
$arResult['POPULAR'] = array();
$arResult['COLS'] = array();
if (!empty($arResult['ITEMS']))
{
	$sects_t = array();
	$arResult['SECTS'] = array();
	foreach ($arResult['ITEMS'] as $karItem => $arItem) 
	{
		if (preg_match('/^([a-zа-я])/ui', toUpper($arItem['NAME']), $letter))
		{
			$arResult['GROUPS'][$letter[1]][] = $arItem;
		}
		else
		{
			$arResult['GROUPS']['other'][] = $arItem;
		}
		
		if (!empty($arItem['PROPERTIES']['IS_POPULAR']['VALUE']))
		{
			$sects_t = array_merge(!empty($arItem['PROPERTIES']['CAT_SECTIONS']['VALUE'])?$arItem['PROPERTIES']['CAT_SECTIONS']['VALUE']:array(), $sects_t);
			
			$arResult['POPULAR'][] = $arItem;
		}
	}
	
	if (!empty($sects_t))
	{
		$sects_t = CIBlockSection::GetList(array(), array(
		    'IBLOCK_ID' => const_IBLOCK_ID_catalog, 
		    'ID' => $sects_t), array('ELEMENT_SUBSECTIONS' => 'Y', 'CNT_ACTIVE' => 'Y'));
		while ($sect = $sects_t->GetNext())
		{
			if ($sect['ELEMENT_CNT'] > 0)
			{
//				$elem = CIBlockElement::GetList(array(), array('IBLOCK_ID' => const_IBLOCK_ID_catalog, 'SECTION_ID' => $sect, 'INCLUDE_SUBSECTIONS' => 'Y'), array('IBLOCK_ID'));
//				$elem = $elem->Fetch();
				
				$arResult['SECTS'][$sect['ID']] = $sect;
			}
		}
	}
	
	$cnt = max(10, ceil(count($arResult['ITEMS']) / 5));

	$col = array();
	$elem_cnt = 0;
	$arResult['COLS'] = array();
	foreach ($arResult['GROUPS'] as $group)
	{
		$col[] = $group;
		$elem_cnt += count($group);
		
		if ($elem_cnt > $cnt*(1+count($arResult['COLS'])))
		{
			$arResult['COLS'][] = $col;
			$col = array();
		}
	}
	
	$arResult['COLS'][] = $col;

    $dbCalcSect = \CIBlockSection::GetList(array(), array('IBLOCK_ID' => const_IBLOCK_ID_catalog, 'SECTION_ID' => 631),false, array('ID'));
    while ($arCalcSect = $dbCalcSect->Fetch())
    {
        $arResult['CALC_SECT_IDS'][] = $arCalcSect['ID'];
    }
}