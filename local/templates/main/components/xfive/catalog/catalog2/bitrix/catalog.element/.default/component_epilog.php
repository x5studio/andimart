<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (strpos($_SERVER['REQUEST_URI'], '/brands/') !== false)
    LocalRedirect($arResult['DETAIL_PAGE_URL']);
?>

<script>
    $(document).ready(function () {
        let location_hash_array = location.hash.split('#');
        let bool_hash_includes_reviews = location_hash_array.includes('reviews');
        if (bool_hash_includes_reviews) {
            $('.js-scroll-to-reviews').click();
        }
    });
</script>

