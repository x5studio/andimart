<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$is_ajax = false;
if (!empty($_GET['is_ajax'])) {
    $is_ajax = true;
}

define('BRANDS_IBLOCK_ID', 26);

?>

<?
global $USER;

if ($USER->IsAdmin()) {
    //die('test');
}
?>

<div class="content-block content-block--pull-top content-block--sm-bot island">
    <? /*
    <h1 class="section-title title-line js-title-line"><? $APPLICATION->ShowTitle(false) ?></h1>
*/ ?>

    <form class="search-form" novalidate action="/catalog/">

        <div class="h4">Вы искали:</div>
        <br/>
        <div class="input-group">
            <div class="input-group__item input-group__item--grow">
                <? if (strlen($arResult["REQUEST"]["~QUERY"]) && is_object($arResult["NAV_RESULT"])) {
                    //$arResult["FILTER_MD5"] = $arResult["NAV_RESULT"]->GetFilterMD5();
                    $obSearchSuggest = new CSearchSuggest($arResult["FILTER_MD5"], $arResult["REQUEST"]["~QUERY"]);
                    $obSearchSuggest->SetResultCount($arResult["NAV_RESULT"]->NavRecordCount);
                }
                ?>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:search.suggest.input",
                    "",
                    array(
                        "NAME" => "q",
                        "VALUE" => $arResult["REQUEST"]["~QUERY"],
                        "INPUT_SIZE" => 40,
                        "DROPDOWN_SIZE" => 10,
                        "FILTER_MD5" => $arResult["FILTER_MD5"],
                    ),
                    $arParams['_component'], array("HIDE_ICONS" => "Y")
                ); ?>

            </div>
            <div class="input-group__item">
                <button class="search-form__submit" type="submit" value="<?= GetMessage("SEARCH_GO") ?>"></button>
            </div>
        </div>
        <input type="hidden" name="how" value="<? echo $arResult["REQUEST"]["HOW"] == "d" ? "d" : "r" ?>"/>
    </form>
    <?
    $srch = $_GET['q'];

    $ar_search_words = explode(' ', $srch);
    $ar_search_words_trimmed = [];
    if (count($ar_search_words) > 0) {
        $srch = "";
        foreach ($ar_search_words as $search_word) {
            if(trim($search_word)) $ar_search_words_trimmed[] = $search_word;
        }

    }
    $srch = implode(" ",$ar_search_words_trimmed);

    $rsSections = new \CSearch();
    $srch = \XFive\Search\Manager::getSearchQuery($srch);
    $rsSections->Search(
        array(
            'QUERY' => $srch,
            'SITE_ID' => SITE_ID,
            'MODULE_ID' => 'iblock',
        ),
        array(
            'ITEM_ID' => 'DESC'
        ),
        array(
            array(
            'PARAM2' => $arParams["IBLOCK_ID"]
            ),
            array(
            'PARAM2' => BRANDS_IBLOCK_ID
            )
        )
    );
    $arSections = [];
    while ($arSection = $rsSections->Fetch()) {
        if (intval($arSection['PARAM2']) != BRANDS_IBLOCK_ID && $arSection['ITEM_ID'][0] != 'S') {
            continue;
        }
        $section['NAME'] = $arSection['TITLE'];
        $section['ITEM_ID'] = $arSection['ITEM_ID'];
        $section['PARAM2'] = $arSection['PARAM2'];
        $section['SECTION_PAGE_URL'] = $arSection['URL'];
        $arSections[] = $section;
    }


    $arElements = $APPLICATION->IncludeComponent(
	"xfive:search.page",
	".default_2",
	array(
		"RESTART" => "N",
        "NO_WORD_LOGIC" => "N",
        "USE_LANGUAGE_GUESS" => "N",
        "CHECK_DATES" => "N",
        "USE_TITLE_RANK" => "Y",
        "DEFAULT_SORT" => "rank",
        "FILTER_NAME" => "",
        "SHOW_WHERE" => "N",
        "arrWHERE" => array(
			0 => "iblock_1c_catalog",
        ),
        "SHOW_WHEN" => "N",
        "PAGE_RESULT_COUNT" => "500",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "",
        "COMPONENT_TEMPLATE" => ".default_2",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "arrFILTER" => array(
            0 => "iblock_1c_catalog",
        ),
        "arrFILTER_iblock_1c_catalog" => array(
            0 => "20",
        ),
        "USE_SUGGEST" => "N"
	),
	false
);


    if (!empty($arElements) && is_array($arElements) || !empty($arSections))
    {
    ?>
</div>
<div class="content-block content-block--pull-top">

<?
if (!empty($arSections)):?>
    <div class="catalog-ctrls">
        <div class="catalog-ctrls__sort island island--small">
            <div class="catalog-ctrls__in">
                <div class="row row--vcentr">
                    <div class="col-5 col-sm-7 col-md-8">
                        <div class="count catalog-ctrls__count">
                            <span class="count__val"><?= count($arSections); ?></span><span
                                    class="count__label">раздел<?= getNumEnding(count($arSections), array('', 'а', 'ов')) ?></span>
                            <? foreach ($arSections as $sect): ?>
                                <br><a href="<?= $sect['SECTION_PAGE_URL'] ?>"><?= $sect['NAME'] ?></a>
                            <? endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? endif; ?>

<div class="catalog-ctrls">
    <div class="catalog-ctrls__sort island island--small">
        <div class="catalog-ctrls__in">
            <div class="row row--vcentr">
                <div class="col-lg-12 col-sm-12 col-md-6 my-flex">
                    <div class="count catalog-ctrls__count">
                        <?
                        $APPLICATION->ShowViewContent('pagination_items_count2'); ?>
                    </div>
                    <? //if(!strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox')) echo "</div>";
                    ?>
                    <div class="col-7 col-sm-5 col-md-4">
                        <div class="row row--vcentr">
                            <div class="col-12 col-sm-5 text-sm-right text-small-expand">Сортировать по</div>
                            <div class="col-12 col-sm-7">
                                <form action="<?= $_SERVER["REQUEST_URI"]; ?>">
                                    <?
                                    foreach ($_GET as $key => $valeu) {
                                        ?>
                                        <input type="hidden" value="<?= $valeu ?>" name="<?= $key ?>">
                                        <?
                                    }
                                    ?>
                                    <select class="select select--xs change_sort_select" name="sort">
                                        <option <?
                                        if ($_GET["sort"] == "popular") echo "selected"; ?> value="popular" selected="">
                                            популярности
                                        </option>
                                        <option <?
                                        if ($_GET["sort"] == "raiting") echo "selected"; ?> value="raiting">рейтингу
                                        </option>
                                        <option <?
                                        if ($_GET["sort"] == "price_exp") echo "selected"; ?> value="price_exp">сначала
                                            дороже
                                        </option>
                                        <option <?
                                        if ($_GET["sort"] == "price_ch") echo "selected"; ?> value="price_ch">сначала
                                            дешевле
                                        </option>
                                    </select>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="catalog grid grid--xs-2 grid--sm-3 grid--md-4">
        <?
        $sort = $arParams["ELEMENT_SORT_FIELD"];
        $sort_prder = $arParams["ELEMENT_SORT_ORDER"];
        $sort_array = array(
            "popular" => array(
                "SORT" => "shows",
                "ORDER" => "ASC",
            ),
            "raiting" => array(
                "SORT" => "PROPERTY_AVG_SIGN",
                "ORDER" => "DESC",
            ),
            "price_exp" => array(
                "SORT" => "catalog_PRICE_14",
                "ORDER" => "DESC",
            ),
            "price_сh" => array(
                "SORT" => "catalog_PRICE_14",
                "ORDER" => "ASC",
            ),
        );
        if ($_GET["sort"] && !empty($sort_array[$_GET["sort"]])) {
            $sort = $sort_array[$_GET["sort"]]["SORT"];
            $sort_prder = $sort_array[$_GET["sort"]]["ORDER"];
        }

        global $searchFilter;
        $searchFilter = array(
            "=ID" => $arElements,
        );


        //        \Bitrix\Main\Diag\Debug::writeToFile('$searchFilter = ');
        //        \Bitrix\Main\Diag\Debug::writeToFile($searchFilter);


        if ($is_ajax) {
            $APPLICATION->RestartBuffer();
        }

        $APPLICATION->IncludeComponent(
            "bitrix:catalog.section",
            $is_ajax ? "ajax_search" : ".default",
            array(
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "ELEMENT_SORT_FIELD" => $sort,
                "ELEMENT_SORT_ORDER" => $sort_prder,
                "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
                "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
                "PAGE_ELEMENT_COUNT" => $is_ajax ? 10 : 12,
                "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
                "PROPERTY_CODE" => $arParams["PROPERTY_CODE"],
                "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
                "OFFERS_FIELD_CODE" => $arParams["OFFERS_FIELD_CODE"],
                "OFFERS_PROPERTY_CODE" => $arParams["OFFERS_PROPERTY_CODE"],
                "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                "OFFERS_LIMIT" => $arParams["OFFERS_LIMIT"],
                "SECTION_URL" => $arParams["SECTION_URL"],
                "DETAIL_URL" => $arParams["DETAIL_URL"],
                "BASKET_URL" => $arParams["BASKET_URL"],
                "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
                "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "DISPLAY_COMPARE" => $arParams["DISPLAY_COMPARE"],
                "PRICE_CODE" => $arParams["PRICE_CODE"],
                "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
                "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
                "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
                "USE_PRODUCT_QUANTITY" => $arParams["USE_PRODUCT_QUANTITY"],
                "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
                "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
                "CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
                "CURRENCY_ID" => $arParams["CURRENCY_ID"],
                "HIDE_NOT_AVAILABLE" => 'Y',
                "DISPLAY_TOP_PAGER" => 'N',
                "DISPLAY_BOTTOM_PAGER" => 'Y',
                "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                "FILTER_NAME" => "searchFilter",
                "SECTION_ID" => "",
                "SECTION_CODE" => "",
                "SECTION_USER_FIELDS" => array(),
                "INCLUDE_SUBSECTIONS" => "Y",
                "SHOW_ALL_WO_SECTION" => "Y",
                "META_KEYWORDS" => "",
                "META_DESCRIPTION" => "",
                "BROWSER_TITLE" => "",
                "ADD_SECTIONS_CHAIN" => "N",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "N",

                'LABEL_PROP' => $arParams['LABEL_PROP'],
                'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
                'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

                'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
                'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
                'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
                'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
                'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
                'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
                'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
                'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
                'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

                'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
                'ADD_TO_BASKET_ACTION' => (isset($arParams['ADD_TO_BASKET_ACTION']) ? $arParams['ADD_TO_BASKET_ACTION'] : ''),
                'SHOW_CLOSE_POPUP' => (isset($arParams['SHOW_CLOSE_POPUP']) ? $arParams['SHOW_CLOSE_POPUP'] : ''),
                'COMPARE_PATH' => $arParams['COMPARE_PATH'],
                'SECTIONS' => $arSections
            ),
            $arResult["THEME_COMPONENT"],
            array('HIDE_ICONS' => 'Y')
        );
        if ($is_ajax) {
            exit;
        }
        ?>
    </div>
    <?//$APPLICATION->ShowViewContent('catalog_pagination');
    ?>
</div>
<?
}
elseif (is_array($arElements)) {
    ?>
    <div class="content-block content-block--push-top wysiwyg">
        <div class="h4">По вашему запросу ничего не найдено</div>
        <p>Попробуйте снова или вернитесь на
            <a href="/">главную</a> страницу</p>
    </div>
    </div>
    <?
} ?>
