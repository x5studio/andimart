<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \Bitrix\Main\Localization\Loc;

?>


<?

if (!empty($arResult['NAV_RESULT'])) {
    $navParams = array(
        'NavPageCount' => $arResult['NAV_RESULT']->NavPageCount,
        'NavPageNomer' => $arResult['NAV_RESULT']->NavPageNomer,
        'NavNum' => $arResult['NAV_RESULT']->NavNum
    );
} else {
    $navParams = array(
        'NavPageCount' => 1,
        'NavPageNomer' => 1,
        'NavNum' => $this->randString()
    );
}

$showLazyLoad = false;

if ($arParams['PAGE_ELEMENT_COUNT'] > 0 && $navParams['NavPageCount'] > 1) {
    $showLazyLoad = $arParams['LAZY_LOAD'] === 'Y' && $navParams['NavPageNomer'] != $navParams['NavPageCount'];
}

$arParams['MESS_BTN_LAZY_LOAD'] = $arParams['MESS_BTN_LAZY_LOAD'] ?: Loc::getMessage('CT_BCS_CATALOG_MESS_BTN_LAZY_LOAD');

$obName = 'ob' . preg_replace('/[^a-zA-Z0-9_]/', 'x', $this->GetEditAreaId($navParams['NavNum']));
$containerName = 'container-' . $navParams['NavNum'];
//$containerName = 'catalog-section-container';
if (count($arResult['ITEMS']) <= 0 && intval($arParams["CUSTOM_SECTION_ID"]) > 0 && $arParams["CUSTOM_SECTION_CODE_PATH"] == "brands") {
    ?>
    <div class="catalog-ctrls catalog-ctrls_zero_brand">
        <div class=" island island--small">
            <div class="catalog-ctrls__in">
                <div class="row row--vcentr">
                    <div class="col-12 col-sm-12 col-md-12">
                        <div class="count catalog-ctrls__count">
                            Товары данного бренда временно отсутсвуют в нашем каталоге.<br> <a
                                    href='/catalog/'>Обратите внимание на другие выгодные
                                предложения нашего магазина!</a>

                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>
    <style>
        main > .catalog-ctrls .catalog-ctrls__sort{display:none;}
    </style>
    <?php
}

?>

<div class="catalog grid grid--xs-2 grid--sm-3 grid--md-3" data-entity="<?= $containerName ?>">

    <!-- items-container -->
    <?
    foreach ($arResult['ITEMS'] as $key => $arItem) {
        //p($arItem["PRICES"]);
        $optimal_price_key = "";
        $opt_price = 99999999999;
        foreach ($arItem["PRICES"] as $key => $price) {
            if ($opt_price > $price["VALUE"]) {
                $opt_price = $price["VALUE"];
                $optimal_price_key = $key;
            }
        }
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
        $strMainID = $this->GetEditAreaId($arItem['ID']);

        $arItemIDs = array(
            'ID' => $strMainID,
            'PICT' => $strMainID . '_pict',
            'SECOND_PICT' => $strMainID . '_secondpict',
            'STICKER_ID' => $strMainID . '_sticker',
            'SECOND_STICKER_ID' => $strMainID . '_secondsticker',
            'QUANTITY' => $strMainID . '_quantity',
            'QUANTITY_DOWN' => $strMainID . '_quant_down',
            'QUANTITY_UP' => $strMainID . '_quant_up',
            'QUANTITY_MEASURE' => $strMainID . '_quant_measure',
            'BUY_LINK' => $strMainID . '_buy_link',
            'BASKET_ACTIONS' => $strMainID . '_basket_actions',
            'NOT_AVAILABLE_MESS' => $strMainID . '_not_avail',
            'SUBSCRIBE_LINK' => $strMainID . '_subscribe',
            'COMPARE_LINK' => $strMainID . '_compare_link',
            'PRICE' => $strMainID . '_price',
            'DSC_PERC' => $strMainID . '_dsc_perc',
            'SECOND_DSC_PERC' => $strMainID . '_second_dsc_perc',
            'PROP_DIV' => $strMainID . '_sku_tree',
            'PROP' => $strMainID . '_prop_',
            'DISPLAY_PROP_DIV' => $strMainID . '_sku_prop',
            'BASKET_PROP_DIV' => $strMainID . '_basket_prop',
        );
        ?>
        <div class="catalog__item grid__item" <?= $strMainID; ?> data-entity="items-row">

            <?if($arItem["PROPERTIES"]["UTSENKA"]["VALUE"] == "да"):?>
                <div class="product-card__badge catalog-section__badge-markdown">
                    <div class="product-card__badge-t product-card__badge-markdown-t">уценка</div>
                </div>
            <?elseif(!$arItem["PROPERTIES"]["PURCHASE_COUNT"]["VALUE"] && $arItem["CATALOG_QUANTITY"] > 0):?>
                <div class="product-card__badge catalog-section__badge-new">
                    <div class="product-card__badge-t product-card__badge-new-t">новинка</div>
                </div>
            <?endif;?>

            <?
            global $USER;
            if($arItem[\XFive\Catalog\ActionExtended::PARAM_NAME]):?>
                <?php
                $start_top = 0;
                if($arItem["PROPERTIES"]["UTSENKA"]["VALUE"] == "да"||!$arItem["PROPERTIES"]["PURCHASE_COUNT"]["VALUE"]) $start_top = 30;
                foreach($arItem[\XFive\Catalog\ActionExtended::PARAM_NAME] as $sticker):
                $color = $sticker['color']?$sticker['color']:"";
                $icon = $sticker['icon']?\CFile::GetPath($sticker['icon']):"";
                if($sticker['position']=='слева'){$position = 'left';}else{$position = 'right';};
                $caption = $sticker['caption']?$sticker['caption']:"";
                $popup_text = $sticker['popup_text']?$sticker['popup_text']:"";
                //if($arItem["PROPERTIES"]["UTSENKA"]["VALUE"] == "да"&&!$arItem["PROPERTIES"]["PURCHASE_COUNT"]["VALUE"]) $other_top = "30px";
                if($position=='left') {
                $b=3;
                }
                else {
                ?>
                <div class="product-card__badge catalog-section__badge-action" <?if($start_top>0):?>style="top:<?=$start_top?>px;"<?endif;?>>
                    <div class="sticker-popup-caption"><?=$caption?></div>
                    <div class="sticker-popup-text"><?=$popup_text?></div>
                    <div class="product-card__badge-t product-card__badge-action-t action-popup" <?if($color):?>style="background-color: <?=$color?>;"<?endif;?>><?=$caption?$caption:"АКЦИЯ";?></div>
                </div>
                <? $start_top+=30;
                }
                endforeach;?>
            <?endif;?>
            <div class="catalog__item-in">
                <div class="catalog__img">
                    <a class="link-hidden" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                        <? $img = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]['ID'], Array("width" => 180, "height" => 180), BX_RESIZE_IMAGE_PROPORTIONAL, false, false, false, 80); ?>
                        <img src="<?= $img["src"] ?>" alt="<?= $arItem["NAME"] ?>">
                    </a>
                </div>
                <div class="catalog__content">
                    <div class="catalog__content-top">
                        <div class="catalog__title">
                            <a class="link-hidden" href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a>
                        </div>
                        <div class="catalog__social-rating">
                            <?include 'social_rating.php'?>
                        </div>
                        <div class="catalog__brand">
                            <?
                            if ($arItem["PROPERTIES"]["BREND"]["VALUE"]) {
                                ?>
                                Бренд: <?= $arItem["PROPERTIES"]["BREND"]["VALUE"] ?>
                                <?
                            }
                            ?>
                            <? if ($arItem["CATALOG_QUANTITY"]) {//CATALOG_QUANTITY?>
                                <div class="catalog__avail">
                                <span id="text-in-stock-product-id-<?= $arItem['ID'] ?>" class="text-success"
                                      data-quantity-in-stock="<?= $arItem["CATALOG_QUANTITY"] ?>">●</span> в наличии
                                </div>
                            <? } else { ?>
                                <div class="catalog__avail">
                                <span id="text-in-stock-product-id-<?= $arItem['ID'] ?>" class="text-warn"
                                      data-quantity-in-stock="0">●</span> нет наличии
                                </div>
                            <? } ?>

                        </div>
                        <?
                            if (($arItem["CATALOG_QUANTITY"] <= 0) || ($arItem["PRICES"][$optimal_price_key]["VALUE"] == 0)): ?>
                                <div class="product-card__row no_balance">
                                    <a class="btn report_admission btn--sm" href="/ajax/product_report.php?ITEM_ID=<?= $arItem["ID"] ?>&RFT_AJAX=Y">
                                        <span>Заказать</span>
                                    </a>
                                </div>
                            <?endif;?>
                    </div>

                    <? if (($arItem["PRICES"][$optimal_price_key]["VALUE"] > 0) && !empty($arItem["CATALOG_QUANTITY"])) { ?>
                        <div class="catalog__content-bot">
                            <div class="product-price">
                                <? if ($arItem["PRICES"][$optimal_price_key]["DISCOUNT_DIFF"]) { ?>
                                    <div class="product-price__old">
                                        <?= $arItem["PRICES"][$optimal_price_key]["PRINT_VALUE"]; ?>
                                    </div>
                                <? } ?>
                                <div class="product-price__current"><?= $arItem["PRICES"][$optimal_price_key]["PRINT_DISCOUNT_VALUE"] ?></div>
                            </div>
                            <div class="catalog__buy">
                                <div class="catalog__buy-count">
                                    <div class="input-group">
                                        <div class="input-group__item catalog__buy-input">

                                            <div class="product-card-wrapper">
                                            <span class="product-card-item-amount-btn-minus catalog_section_minus"
                                                  data-entity="product-card-item-quantity-minus"><i class="fas fa-minus"
                                                                                                    style="font-size: 0.8em;"></i></span>
                                                <input data-quntity="<?= $arItem["CATALOG_QUANTITY"] ?>"
                                                       pattern="^[1-9]{1}[0-9]{3}"
                                                       id="textbox-desired-quantity-product-id-<?= $arItem['ID'] ?>"
                                                       class="input-text input-text--sm text-center section_quantity_input"
                                                       type="text" value="1">
                                                <span class="product-card-item-amount-btn-plus catalog_section_plus"
                                                      data-entity="product-card-item-quantity-plus"><i
                                                            class="fas fa-plus"
                                                            style="font-size: 0.8em;"></i></span>
                                            </div>

                                        </div>
                                        <div class="input-group__item catalog__buy-label">
                                            <div class="input-posfix">шт.</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="catalog__buy-btn">
                                    <button id="btn-buy-product-id-<?= $arItem['ID'] ?>"
                                            class="btn btn--sm add_to_cart_section js-btn-to-basket"
                                            rel="<?= $arItem["ADD_URL"] ?>" type="button">
                                        <span>Купить</span>
                                    </button>
                                    <a href="/cart/" class="btn btn--inverse btn--sm btn-in-basket"
                                       style="display: none">
                                        <span>В корзине</span>
                                    </a>
                                </div>
								<?
								$iblockid = $arItem['IBLOCK_ID'];
								$id=$arItem['ID'];
								if(isset($_SESSION["CATALOG_COMPARE_LIST"][$iblockid]["ITEMS"][$id])) { $clascomp='active'; }else{ $clascomp=''; }
							?>
						    <div class="catalog_compare-btn">
						    	<noindex>
									<a href="#" rel="nofollow" data-id="<?=$arItem["ID"];?>" class="sky-compare-btn <?=$clascomp?>" title="Сравнить">
										<svg version="1.0" xmlns="http://www.w3.org/2000/svg" width="479.000000pt" height="479.000000pt" viewBox="0 0 479.000000 479.000000" preserveAspectRatio="xMidYMid meet">
										<metadata>
										Created by potrace 1.15, written by Peter Selinger 2001-2017
										</metadata>
										<g transform="translate(0.000000,479.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
										<path d="M1979 4137 c-18 -12 -44 -38 -56 -56 l-23 -34 2 -1533 c3 -1529 3
										-1533 24 -1561 11 -15 40 -38 64 -51 43 -22 51 -22 405 -22 354 0 362 0 405
										23 33 16 51 34 68 67 l22 45 -2 1521 -3 1521 -28 36 c-52 68 -44 67 -465 67
										-378 0 -379 0 -413 -23z"></path>
										<path d="M3370 3063 c-37 -14 -74 -49 -91 -87 -18 -39 -19 -86 -19 -1002 l0
										-961 23 -43 c16 -30 37 -51 67 -67 l44 -24 377 3 376 3 37 29 c70 53 66 -8 66
										1069 l0 974 -23 34 c-12 18 -38 44 -56 56 -34 23 -35 23 -410 22 -207 0 -383
										-3 -391 -6z"></path>
										<path d="M621 2233 c-18 -9 -45 -34 -60 -56 l-26 -41 -3 -520 c-2 -287 0 -544
										3 -573 7 -57 34 -104 78 -137 27 -20 41 -21 395 -24 241 -2 380 1 404 8 45 13
										94 67 108 120 7 26 10 218 8 582 -3 607 0 582 -75 633 l-38 25 -380 0 c-333 0
										-384 -3 -414 -17z"></path>
										</g>
										</svg>
									</a>
								</noindex>
						    </div>
                            </div>
                        </div>
                    <? } ?>

                </div>
            </div>
        </div>
        <?
    }

    ?>
    <script type="text/javascript">
        $('.report_admission').magnificPopup({
            type: 'ajax',
            mainClass: 'report_style',
            closeBtnInside: true
        });
    </script>
    <!-- items-container -->

    <!--<div class="catalog__item grid__item"></div>-->
    <? $this->SetViewTarget('catalog_pagination'); ?>
    <div class="col-lg-12" data-pagination-num="<?= $navParams['NavNum'] ?>">
        <!-- pagination-container -->
        <?= $arResult["NAV_STRING"]; ?>
        <!-- pagination-container -->
    </div>
    <? $this->EndViewTarget(); ?>

    <?
    if ($showLazyLoad) {
        ?>
        <!--    <div class="row bx---><? //= $arParams['TEMPLATE_THEME']
        ?><!--">-->
        <div class="btn btn-default btn-lg center-block col-xs-12 col-sm-12 col-md-12 col col-lg-12"
             style="margin: 15px 0px;"
             data-use="show-more-<?= $navParams['NavNum'] ?>">
            <?= $arParams['MESS_BTN_LAZY_LOAD'] ?>
        </div>
        <!--    </div>-->
        <?
    } else {
        ?>
        <!--    <div class="catalog__item grid__item"></div>-->
        <?
    }
    ?>
    <!--<div class="catalog__item grid__item"></div>-->
    <?


    $signer = new \Bitrix\Main\Security\Sign\Signer;
    $signedTemplate = $signer->sign($templateName, 'catalog.section');
    $signedParams = $signer->sign(base64_encode(serialize($arResult['ORIGINAL_PARAMETERS'])), 'catalog.section');

    ?>

    <script>
        BX.message({
            BTN_MESSAGE_BASKET_REDIRECT: '<?=GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT')?>',
            BASKET_URL: '<?=$arParams['BASKET_URL']?>',
            ADD_TO_BASKET_OK: '<?=GetMessageJS('ADD_TO_BASKET_OK')?>',
            TITLE_ERROR: '<?=GetMessageJS('CT_BCS_CATALOG_TITLE_ERROR')?>',
            TITLE_BASKET_PROPS: '<?=GetMessageJS('CT_BCS_CATALOG_TITLE_BASKET_PROPS')?>',
            TITLE_SUCCESSFUL: '<?=GetMessageJS('ADD_TO_BASKET_OK')?>',
            BASKET_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCS_CATALOG_BASKET_UNKNOWN_ERROR')?>',
            BTN_MESSAGE_SEND_PROPS: '<?=GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_SEND_PROPS')?>',
            BTN_MESSAGE_CLOSE: '<?=GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE')?>',
            BTN_MESSAGE_CLOSE_POPUP: '<?=GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE_POPUP')?>',
            COMPARE_MESSAGE_OK: '<?=GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_OK')?>',
            COMPARE_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_UNKNOWN_ERROR')?>',
            COMPARE_TITLE: '<?=GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_TITLE')?>',
            PRICE_TOTAL_PREFIX: '<?=GetMessageJS('CT_BCS_CATALOG_PRICE_TOTAL_PREFIX')?>',
            RELATIVE_QUANTITY_MANY: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_MANY'])?>',
            RELATIVE_QUANTITY_FEW: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_FEW'])?>',
            BTN_MESSAGE_COMPARE_REDIRECT: '<?=GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT')?>',
            BTN_MESSAGE_LAZY_LOAD: '<?=CUtil::JSEscape($arParams['MESS_BTN_LAZY_LOAD'])?>',
            BTN_MESSAGE_LAZY_LOAD_WAITER: '<?=GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_LAZY_LOAD_WAITER')?>',
            SITE_ID: '<?=CUtil::JSEscape($component->getSiteId())?>'
        });
        var <?=$obName?> =
        new JCCatalogSectionComponent({
            siteId: '<?=$component->getSiteId()?>',
            componentPath: '<?=CUtil::JSEscape($componentPath)?>',
            navParams: <?=CUtil::PhpToJSObject($navParams)?>,
            deferredLoad: false, // enable it for deferred load
            initiallyShowHeader: '<?=!empty($arResult['ITEM_ROWS'])?>',
            bigData: <?=CUtil::PhpToJSObject($arResult['BIG_DATA'])?>,
            lazyLoad: !!'<?=$showLazyLoad?>',
            loadOnScroll: !!'<?=($arParams['LOAD_ON_SCROLL'] === 'Y')?>',
            template: '<?=CUtil::JSEscape($signedTemplate)?>',
            ajaxId: '<?=CUtil::JSEscape($arParams['AJAX_ID'])?>',
            parameters: '<?=CUtil::JSEscape($signedParams)?>',
            container: '<?=$containerName?>'
        });
    </script>
