<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div id="brands-group-<?=$arParams['KEY']?>" class="tab-content content-block js-tabs__content" <?=$arParams['X5_SECTION_PAGE_URL']?"data-url='{$arParams['X5_SECTION_PAGE_URL']}'":""?> <?=$arParams['X5_SECTION_NAME']?"data-x5-breadcrumb-name='{$arParams['X5_SECTION_NAME']}'":""?>>
	<? if (!empty($_GET['AJAX_MODE']) && $_GET['AJAX_MODE'] == 'Y') { $APPLICATION->RestartBuffer(); } ?>

	<? if (!empty($arResult["ITEMS"])) { ?>
	<div class="content-block" data-url="<?=!empty($arParams['KEY'])?$APPLICATION->GetCurPage():'/brands/';?>">
	    
		<? if (!empty($arResult['POPULAR'])) { ?>
			<div class="content-block">Популярные бренды:
				<div class="row b-list">
					<? foreach ($arResult['POPULAR'] as $arItem) { ?>
						<div class="col-6 col-sm-4 col-md-3 b-list__item">
							<div class="featured-brand">
								<div class="featured-brand__img">
								  <a class="link-hidden" href="<?=$arItem['DETAIL_PAGE_URL']?>">
								      <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" width="156" height="60" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>">
								  </a>
								</div>
								<div class="featured-brand__title">
								  <a class="link-hidden" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a>
								</div>
                                <?php

                                ?>
								<div class="featured-brand__links">
									<? foreach ($arItem['PROPERTIES']['CAT_SECTIONS']['VALUE'] as $ksect => $sect) {

									    // Убираем отсюда разделы калькулятора
                                        if(in_array($sect, $arResult['CALC_SECT_IDS']))
                                            continue;

										 $section = $arResult['SECTS'][$sect];

											$elem = CIBlockElement::GetList(array(),
												array('IBLOCK_ID' => const_IBLOCK_ID_catalog,
												    'SECTION_ID' => $sect,
												    'ACTIVE' => 'Y',
												    'PROPERTY_BREND' => $arItem['XML_ID'],
												    'INCLUDE_SUBSECTIONS' => 'N'), array('IBLOCK_ID'));

										?>
										<? if ($elem = $elem->Fetch()) { ?>
                                            <?
                                            /*x5 20180906 по просьбе клиента скрыли отображение ссылки на бренд в этом списке*/
                                            if(mb_strtolower($arItem['NAME'])==mb_strtolower($section['NAME'])) continue;?>

											<div class="featured-brand__link">
												<a href="<?=$section['SECTION_PAGE_URL']?>filter/brend-is-<?=$arItem['XML_ID']?>/apply/"><?=$section['NAME']?>&nbsp;(<?=$elem['CNT']?>)</a>
											</div>
										<? } ?>
									<? } ?>
								</div>
							</div>
						</div>
					<? } ?>
				</div>
			</div>
		<? } ?>
		<div class="content-block content-block--push-top">
			<div class="brands-list">
				<? foreach ($arResult['COLS'] as $col) { ?>
					<div class="brands-list__col">
						<? foreach ($col as $group) { ?>
							<div class="brands-list__group">
							<? foreach($group as $arItem) { ?>
								<?
								$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
								$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
								?>
								<div class="brands-list__item">
									<a class="link link--gray" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a>
								</div>
							<? } ?>
							</div>
						<? } ?>
					</div>
				<? } ?>
			</div>
		</div>
	</div>
	<? } ?>
	<? if (!empty($_GET['AJAX_MODE']) && $_GET['AJAX_MODE'] == 'Y') { exit; } ?>
</div>