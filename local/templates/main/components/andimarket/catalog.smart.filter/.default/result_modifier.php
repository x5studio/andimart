<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arResult['BRANDS'] = array();
if (empty($arParams['NO_BRANDS']) && !empty($arResult["ITEMS"][const_IBLOCK_ID_catalog_PROP_ID_brands]['VALUES']))
{
    $brands_xml_ids = array();
    foreach ($arResult["ITEMS"][const_IBLOCK_ID_catalog_PROP_ID_brands]['VALUES'] as $br_xml_id => $value)
    {
	$brands_xml_ids[] = $br_xml_id;
    }
    
    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "XML_ID"); //IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
    $arFilter = Array("IBLOCK_ID" => const_IBLOCK_ID_brands, 'XML_ID' => $brands_xml_ids);
    $brands = array();
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while ($arFields = $res->Fetch())
    {
	$arResult['BRANDS'][$arFields["XML_ID"]] = $arFields["NAME"];
    }
}

