<? if ($arResult['ORDER']['PAYED'] != 'Y' && strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0 && $arResult["PAY_SYSTEM"]['IS_CASH'] != 'Y') { ?>
<div class="row b-list">
	<div class="col-12 col-sm-6 b-list__item">
		<div class="content-block">
			<div class="h5">
				 Ваш заказ № <?=$arResult['ORDER_ID']?>
			</div>
			<div class="text-small-expand">
				 от <?=toLower(FormatDate('j F Y г. H:i', MakeTimeStamp($arResult['ORDER']['DATE_INSERT'])))?>
			</div>
			<div class="text-small-expand">
				 Данные покупателя: <?=$arResult['ORDER_INFO']['NAME']?>, тел.: <?=$arResult['ORDER_INFO']['PHONE']?>, е-mail: <?=$arResult['ORDER_INFO']['EMAIL']?>
			</div>
			 <? if ($arResult['ORDER']['DELIVERY_ID'] == 3) { ?>
			<div class="text-small-expand">
				 Адрес доставки <?=formAddress($arResult['ORDER_INFO'])?>.
			</div>
			 <? } elseif ($arResult['ORDER']['DELIVERY_ID'] == 8) { ?>
			<div class="text-small-expand">
				 Самовывоз: <?=$arResult['ORDER_INFO']['PICKPOINT']?>.
			</div>
			 <? } ?>
		</div>
		<div class="content-block">
 <a href="#" style="max-width: 240px;" onclick="$('._pay_order form').submit(); return false;" class="btn btn--fluid btn--inverse">Оплатить заказ</a>
		</div>
	</div>
</div>
<div style="display: none;" class="_pay_order">
	 <?

if ($arResult['ORDER']['PAYED'] != 'Y')
{
	if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0)
	{
		if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y")
		{
			?> <script language="JavaScript">
				window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))?>');
			</script> <?= GetMessage("SOA_TEMPL_PAY_LINK", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))))?> <?
			if (CSalePdf::isPdfAvailable() && CSalePaySystemsHelper::isPSActionAffordPdf($arResult['PAY_SYSTEM']['ACTION_FILE']))
			{
				?><br>
	 <?= GetMessage("SOA_TEMPL_PAY_PDF", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))."&pdf=1&DOWNLOAD=Y")) ?> <?
			}
		}
		else
		{
			if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"])>0)
			{
				try
				{
					include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
				}
				catch(\Bitrix\Main\SystemException $e)
				{
					if($e->getCode() == CSalePaySystemAction::GET_PARAM_VALUE)
						$message = GetMessage("SOA_TEMPL_ORDER_PS_ERROR");
					else
						$message = $e->getMessage();

					echo '<span style="color:red;">'.$message.'</span>';
				}
			}
		}
	}
}
?>
</div>
 <? } else { ?>
<div class="row b-list">
	<div class="col-12 col-sm-6 b-list__item">
		<div class="content-block">
			<p>
				 Ваш заказ успешно принят!
			</p>
			 <?=$arResult['ORDER_INFO']['NAME']?>, мы очень рады, что вы выбрали наш магазин. <br>
			 В ближайшее время с вами свяжется менеджер для подтверждения данных заказа.
		</div>
		<div class="content-block">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_TEMPLATE_PATH."/include/order/dop_service.php"
	)
);?>
		</div>
		<div class="content-block">
			<div class="h5">
				 Ваш заказ № <?=$arResult['ORDER_ID']?>
			</div>
			<div class="text-small-expand">
				 от <?=toLower(FormatDate('j F Y г. H:i', MakeTimeStamp($arResult['ORDER']['DATE_INSERT'])))?>
			</div>
			<div class="text-small-expand">
				 Данные покупателя: <?=$arResult['ORDER_INFO']['NAME']?>, тел.: <?=$arResult['ORDER_INFO']['PHONE']?>, е-mail: <?=$arResult['ORDER_INFO']['EMAIL']?>
			</div>
			 <? if ($arResult['ORDER']['DELIVERY_ID'] == 3) { ?>
			<div class="text-small-expand">
				 Адрес доставки <?=formAddress($arResult['ORDER_INFO'])?>.
			</div>
			 <? } elseif ($arResult['ORDER']['DELIVERY_ID'] == 8) { ?>
			<div class="text-small-expand">
				 Самовывоз: <?=$arResult['ORDER_INFO']['PICKPOINT']?>.
			</div>
			 <? } ?>
		</div>
		 <? if ($arResult['ORDER']['PAYED'] != 'Y' && strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0 && $arResult["PAY_SYSTEM"]['IS_CASH'] != 'Y') { ?>
		<div class="content-block">
 <a href="#" style="max-width: 240px;" onclick="$('._pay_order form').submit(); return false;" class="btn btn--fluid btn--inverse">Оплатить заказ</a>
		</div>
		 <? } ?>
	</div>
	<div class="col-12 col-sm-5 offset-sm-1 col-md-4 b-list__item">
		<div class="island box">
			<div class="content-block h5 text-center">
				 Нравится у нас покупать? <br>
				 Оставьте отзыв на
			</div>
			<div class="content-block text-center">
 <img width="144" src="/local/templates/main/static/images/ya-market.svg?v=f281af3a" height="28" alt="">
			</div>
 <a class="btn btn--fluid btn--inverse" href="#">Оставить отзыв</a>
		</div>
	</div>
</div>
 <?/*
<div class="content-block">
    <div class="spoiler">
	<a class="spoiler__btn js-spoiler" href="#order-products">
	    <span class="spoiler__btn-collapse">Cостав заказа</span>
	    <span class="spoiler__btn-expand">Свернуть состав заказа</span>
	</a>
	<div id="order-products" class="spoiler__content">
	    <div class="history-products">
		<div class="history-products__head row hidden-xs-down">
		    <div class="col-sm-6">Товар</div>
		    <div class="col-sm-2">Цена</div>
		    <div class="col-sm-2">Кол-во</div>
		    <div class="col-sm-2">Сумма</div>
		</div>
		<ul class="history-products__list">
		    <li class="history-products__item">
			<div class="row row--vcentr">
                            <div class="col-4 col-sm-2">
				<img class="flexible-media" src="<?= SITE_TEMPLATE_PATH ?>/static/images/upload/catalog/1.jpg?v=72bc5a49" width="200" height="200" alt>
                            </div>
                            <div class="col-8 col-sm-4">
				<div class="text-bold">
				    <a class="link-hidden" href="product.html">Термостатическая головка с выносным накладным датчиком</a>
				</div>Бренд: JW</div>
                            <div class="col-8 offset-4 col-sm-2 offset-sm-0 text-small-expand">
				<div class="hidden-sm-up text-gray">Цена: </div>
				<div class="product-price">
				    <div class="product-price__old">126 470 р.</div>
				    <div class="product-price__current">124 000 р.</div>
				</div>
                            </div>
                            <div class="col-8 offset-4 col-sm-2 offset-sm-0 text-small-expand">
				<span class="hidden-sm-up text-gray">Кол-во: </span>2</div>
                            <div class="col-8 offset-4 col-sm-2 offset-sm-0 text-small-expand">
				<span class="hidden-sm-up text-gray">Сумма: </span>
				<span class="history-products__sum">46 000 р.</span>
                            </div>
			</div>
		    </li>
		    <li class="history-products__item">
			<div class="row row--vcentr">
                            <div class="col-4 col-sm-2">
				<img class="flexible-media" src="<?= SITE_TEMPLATE_PATH ?>/static/images/upload/catalog/2.jpg?v=46f24cb1" width="200" height="200" alt>
                            </div>
                            <div class="col-8 col-sm-4">
				<div class="text-bold">
				    <a class="link-hidden" href="product.html">Термостатическая головка с выносным накладным датчиком</a>
				</div>Бренд: JW</div>
                            <div class="col-8 offset-4 col-sm-2 offset-sm-0 text-small-expand">
				<div class="hidden-sm-up text-gray">Цена:</div>
				<div class="product-price">
				    <div class="product-price__old">126 470 р.</div>
				    <div class="product-price__current">124 000 р.</div>
				</div>
                            </div>
                            <div class="col-8 offset-4 col-sm-2 offset-sm-0 text-small-expand">
				<span class="hidden-sm-up text-gray">Кол-во:</span> 2</div>
                            <div class="col-8 offset-4 col-sm-2 offset-sm-0 text-small-expand">
				<span class="hidden-sm-up text-gray">Сумма:</span>
				<span class="history-products__sum"> 46 000 р.</span>
                            </div>
			</div>
		    </li>
		    <li class="history-products__item">
			<div class="row row--vcentr">
                            <div class="col-4 col-sm-2">
				<img class="flexible-media" src="<?= SITE_TEMPLATE_PATH ?>/static/images/upload/catalog/4.jpg?v=a7f0ede8" width="200" height="200" alt>
                            </div>
                            <div class="col-8 col-sm-4">
				<div class="text-bold">
				    <a class="link-hidden" href="product.html">Термостатическая головка с выносным накладным датчиком</a>
				</div>Бренд: JW</div>
                            <div class="col-8 offset-4 col-sm-2 offset-sm-0 text-small-expand">
				<div class="hidden-sm-up text-gray">Цена:</div>
				<div class="product-price">
				    <div class="product-price__old">126 470 р.</div>
				    <div class="product-price__current">124 000 р.</div>
				</div>
                            </div>
                            <div class="col-8 offset-4 col-sm-2 offset-sm-0 text-small-expand">
				<span class="hidden-sm-up text-gray">Кол-во:</span> 2</div>
                            <div class="col-8 offset-4 col-sm-2 offset-sm-0 text-small-expand">
				<span class="hidden-sm-up text-gray">Сумма:</span>
				<span class="history-products__sum"> 46 000 р.</span>
                            </div>
			</div>
		    </li>
		</ul>
	    </div>
	</div>
    </div>
</div>
 * 
 */?>
<div class="content-block">
	<div class="sum">
		<div class="sum__row">
			 Итого
		</div>
		<div class="sum__row">
			 Товаров на <span class="text-nowrap"><?=CurrencyFormat($arResult['ORDER']['PRICE']-$arResult['ORDER']['DISCOUNT_VALUE']-$arResult['ORDER']['PRICE_DELIVERY'], $arResult['ORDER']['CURRENCY'])?></span>
		</div>
		 <? if (!empty($arResult['DISCOUNT_VALUE'])) { ?>
		<div class="sum__row">
			 Скидка <span class="text-nowrap"><?=CurrencyFormat($arResult['ORDER']['DISCOUNT_VALUE'], $arResult['ORDER']['CURRENCY'])?></span>
		</div>
		 <? } ?>
		<div class="sum__row">
			 Доставка: <?=CurrencyFormat($arResult['ORDER']['PRICE_DELIVERY'], $arResult['ORDER']['CURRENCY'])?>
		</div>
	</div>
</div>
<div class="content-block">
	<div class="row">
		<div class="col-12 col-sm-4">
			<div class="h4">
				 К оплате: <?=CurrencyFormat($arResult['ORDER']['PRICE'], $arResult['ORDER']['CURRENCY'])?>
			</div>
		</div>
		<div class="col-12 col-sm-4">
			 Оплата: <span class="h5"><?=$arResult['PAY_SYSTEM']['NAME']?></span>
		</div>
	</div>
</div>
<div class="content-block">
 <a class="print-link" href="/lichniy_cabinet/order/<?=$arResult['ORDER']['ID']?>/?print">распечатать информацию о заказе</a>
</div>
<? } ?>