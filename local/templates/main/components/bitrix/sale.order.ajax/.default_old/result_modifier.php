<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */
global $USER;

$arResult['DELIVERY_CHECKED'] = false;
foreach ($arResult['DELIVERY'] as $delivery) 
{
	if ($delivery['CHECKED'] == 'Y') 
	{
		$arResult['DELIVERY_CHECKED'] = $delivery['ID'];
	}
}

$userInfoArr = array();
if ($USER->IsAuthorized())
{
	$us = CUser::GetByID($USER->GetID());
	$us = $us->Fetch();
	
	$userInfoArr['EMAIL'] = $us['EMAIL'];
	$userInfoArr['PHONE'] = $us['PERSONAL_PHONE'];
	$userInfoArr['NAME'] = $us['NAME'];
}
else
{
	$userInfoArr['EMAIL'] = $_REQUEST['email'];
	$userInfoArr['PHONE'] = $_REQUEST['phone'];
	$userInfoArr['NAME'] = $_REQUEST['name'];
}

$arResult['PROPERTIES']['DELIVERY_INFO'] = array();
$arResult['PROPERTIES']['USER_INFO'] = array();

$propVals = array();
foreach ($arResult['ORDER_PROP']['PRINT'] as $prop)
{
	$propVals[$prop['ID']] = $prop['VALUE'];
}

foreach (array_merge($arResult['ORDER_PROP']['USER_PROPS_Y'], $arResult['ORDER_PROP']['USER_PROPS_N'], $arResult['ORDER_PROP']['RELATED']) as $kProp => $prop)
{
	if ($prop['PROPS_GROUP_ID'] == 1)
	{		
		$prop['PRINT_VALUE'] = $propVals[$prop['ID']];
		if ('CITY' == $prop['CODE'])
		{
			foreach ($prop['VARIANTS'] as $variant)
			{
				if ($variant['ID'] == $prop['VALUE'])
				{
					$prop['LOCATION_CODE'] = $variant['CODE'];
					$prop['LOCATION_NAME'] = $variant['CITY_NAME_LANG'];
					$prop['LOCATION_SAM'] = false;
					
					if ($arResult['DELIVERY_CHECKED'] == 3)
					{
						$c_info = CIBlockElement::GetList(array(), array('IBLOCK_ID' => const_IBLOCK_ID_cities, 'PROPERTY_LOCATION_ID' => $prop['LOCATION_CODE']), false, array('nTopCount' => 1), array('ID', 'PROPERTY_HAS_SAM', 'PROPERTY_SAM_CITY_ID'));
						$c_info = $c_info->Fetch();

						if ($c_info['PROPERTY_HAS_SAM_VALUE'] != 'Y')
						{
							$c_info = CIBlockElement::GetList(array(), array('IBLOCK_ID' => const_IBLOCK_ID_cities, 'XML_ID' => $c_info['PROPERTY_SAM_CITY_ID_VALUE']), false, array('nTopCount' => 1), array('PROPERTY_LOCATION_ID'));
							$c_info = $c_info->Fetch();
							
							$prop['LOCATION_SAM'] = $c_info['PROPERTY_LOCATION_ID_VALUE'];
						}
						unset($c_info);
					}
					
					break;
				}
			}
			
			unset($prop['VARIANTS']);
			unset($prop['~VARIANTS']);
		}
		
		$arResult['PROPERTIES']['DELIVERY_INFO'][$prop['CODE']] = $prop;
	}
	elseif ($prop['PROPS_GROUP_ID'] == 2)
	{	
		$prop['PRINT_VALUE'] = !empty($userInfoArr[$prop['CODE']])?$userInfoArr[$prop['CODE']]:$propVals[$prop['ID']];
		
		$arResult['PROPERTIES']['USER_INFO'][$prop['CODE']] = $prop;
	}
}

$arResult['ORDER_INFO'] = array();
if (!empty($arResult['ORDER_ID']))
{
	$db_props = CSaleOrderPropsValue::GetOrderProps($arResult['ORDER_ID']);
	
	while ($arProps = $db_props->Fetch())
	{
		$arResult['ORDER_INFO'][$arProps['CODE']] = $arProps['VALUE'];
	}
}