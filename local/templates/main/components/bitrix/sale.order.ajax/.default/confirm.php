<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 * @var array $arResult
 * @var       $APPLICATION CMain
 */

if ($arParams["SET_TITLE"] == "Y") {
    $APPLICATION->SetTitle(Loc::getMessage("SOA_ORDER_COMPLETE"));
}

if (!empty($arResult["ORDER"])) {

    $order = \Bitrix\Sale\Order::load($arResult['ORDER']['ID']);
    global $USER;
    $arOrder = [];
    $arOrder['CUSTOMER']['NAME'] = $order->getPropertyCollection()->getPayerName()->getFieldValues()['VALUE'];
    $arOrder['CUSTOMER']['PHONE'] = $order->getPropertyCollection()->getPhone()->getFieldValues()['VALUE'];
    $arOrder['CUSTOMER']['EMAIL'] = $order->getPropertyCollection()->getUserEmail()->getFieldValues()['VALUE'];
    $arOrder['CUSTOMER']['ADDRESS'] = $order->getPropertyCollection()->getDeliveryLocation()->getValue();
    $address = $order->getPropertyCollection()->getItemByOrderPropertyCode('CITY_TEXT');
    if(!empty($address)) {
        $arOrder['CUSTOMER']['CITY_TEXT'] = $address->getValue();
    }

    $fields = $order->getDeliverySystemId();
    if (is_array($fields)) {
        $deliverySystemId = array_shift($fields); /* Если приходит несколько служб доставки, то берем только первую */
    } else {
        $deliverySystemId = $fields;
    }

    $params = array(
        'filter' => array('ID' => $deliverySystemId),
    );

    $dbRes = \Bitrix\Sale\Delivery\Services\Table::getList($params);
    $service = $dbRes->fetch();
    if (isset($service['PARENT_ID'])) { /* Если есть PARENT_ID, значит это не служба, а профиль. Достаём также имя службы */
        $params = array(
            'filter' => array('ID' => $service['PARENT_ID']),
        );
        $dbRes = \Bitrix\Sale\Delivery\Services\Table::getList($params);
        $service_2 = $dbRes->fetch();

        $arOrder['DELIVERY_SYSTEM_NAME'] = $service_2['NAME'] . '. ' . $service['NAME'];
    } else {
        $arOrder['DELIVERY_SYSTEM_NAME'] = $service['NAME'];
    }

    $arOrder['DELIVERY_PRICE'] = $order->getDeliveryPrice();
    $arOrder['PRICE'] = $order->getPrice();
    $arOrder['ADDRESS'] = Bitrix\Sale\Location\Admin\LocationHelper::getLocationStringByCode($arOrder['CUSTOMER']['ADDRESS']);

    $arOrder['FULL_ADDRESS'] = $arOrder['CUSTOMER']['CITY_TEXT'] ? $arOrder['CUSTOMER']['CITY_TEXT'] : $arOrder['ADDRESS'];
    ?>
    <div class="order-complete">
        <div class="order-complete__form">
            <div class="order-complete__box">
                <div class="order-complete__title order-complete__title--center">Ваш заказ успешно принят</div>
                <div class="order-complete__desc">
                    <?= $arOrder['CUSTOMER']['NAME'] ?><?= Loc::getMessage("SOA_ORDER_SUC1", ["#LINK#" => $arParams["PATH_TO_PERSONAL"]]) ?>
                </div>
                <div class="order-complete__info">
                    <div>
                        <?= Loc::getMessage("SOA_ORDER_SUC", [
                            "#ORDER_DATE#" => $arResult["ORDER"]["DATE_INSERT"],
                            "#ORDER_ID#"   => $arResult["ORDER"]["ACCOUNT_NUMBER"],
                        ]) ?>
                    </div>
                    <? if (!empty($arResult['ORDER']["PAYMENT_ID"])) { ?>
                        <div>
                            <?= Loc::getMessage("SOA_PAYMENT_SUC", [
                                "#PAYMENT_ID#" => $arResult['PAYMENT'][$arResult['ORDER']["PAYMENT_ID"]]['ACCOUNT_NUMBER'],
                            ]) ?>
                        </div>
                    <? } ?>
                    <div><b>Данные покупателя:</b> <?= $arOrder['CUSTOMER']['NAME'] ?>
                        , <?= $arOrder['CUSTOMER']['PHONE'] ?>
                        , <?= $arOrder['CUSTOMER']['EMAIL'] ?></div>
                    <div><b>Доставка:</b> <?= $arOrder['DELIVERY_SYSTEM_NAME'] ?> <b>по адресу:</b></div>
                    <div><?=$arOrder['FULL_ADDRESS']?></div>
                </div>
                <br/>
               <?
               $parameters = array(
                   'apikey' => '7cd000f0-6c2f-46d6-a3c4-a26922cfb3ab',
                   'geocode' => $arOrder['FULL_ADDRESS'],
               );

               // Отправляем запрос к геокодеру
               if (!$geocode = @file_get_contents('https://geocode-maps.yandex.ru/1.x/?'. http_build_query($parameters))) {
                   $error = error_get_last();
                   throw new Exception('HTTP request failed. Error: ' . $error['message']);
               }

               $xml = new SimpleXMLElement($geocode);

               $xml->registerXPathNamespace('ymaps', 'http://maps.yandex.ru/ymaps/1.x');
               $xml->registerXPathNamespace('gml', 'http://www.opengis.net/gml');

               $result = $xml->xpath('/ymaps:ymaps/ymaps:GeoObjectCollection/gml:featureMember/ymaps:GeoObject/gml:Point/gml:pos');

               if (isset($result[0])) {

                   list($longitude, $latitude) = explode(' ', $result[0]);

               }

               ?>
                <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
                <div id="map" style="width: 600px; height: 400px"></div>
                <script type="text/javascript">
                    ymaps.ready(init);
                    function init() {
                        var myMap = new ymaps.Map("map", {
                            center: [<?=$latitude?>, <?=$longitude?>],
                            zoom: 12,
                            controls: [
                                'zoomControl', // Ползунок масштаба
                                'rulerControl', // Линейка
                                'routeButtonControl', // Панель маршрутизации
                                'trafficControl', // Пробки
                                'typeSelector', // Переключатель слоев карты
                                'fullscreenControl', // Полноэкранный режим
                            ]
                        });

                        var placemark = new ymaps.Placemark([<?=$latitude?>, <?=$longitude?>], {
                            hintContent: '<?=$arOrder['FULL_ADDRESS']?>',
                        }, {
                            'preset': 'islands#nightDotIcon'
                        });

                        myMap.geoObjects.add(placemark);
                    }
                </script>

                <div class="order-complete__result">
                    <div class="order-complete__head">Итого</div>
                    <div>Товаров на: <?= ($arOrder['PRICE'] - $arOrder['DELIVERY_PRICE']) ?> рублей</div>
                    <div>Доставка: <?= $arOrder['DELIVERY_PRICE'] ?> рублей</div>
                </div>
                <div class="order-complete__payment">
                    <span class="order-complete__head">К оплате:</span> <?= $arOrder['PRICE'] ?> рублей
                </div>
            </div>
            <? if ($arResult["ORDER"]["IS_ALLOW_PAY"] === 'Y') {
                if (!empty($arResult["PAYMENT"])) {
                    foreach ($arResult["PAYMENT"] as $payment) {
                        if ($payment["PAID"] != 'Y') {
                            if (!empty($arResult['PAY_SYSTEM_LIST'])
                                && array_key_exists($payment["PAY_SYSTEM_ID"], $arResult['PAY_SYSTEM_LIST'])
                            ) {
                                $arPaySystem = $arResult['PAY_SYSTEM_LIST'][$payment["PAY_SYSTEM_ID"]];

                                if (empty($arPaySystem["ERROR"])) {
                                    ?>
                                    <div class="order-complete__box">
                                        <div class="ps_logo">
                                            <div class="order-complete__head"><?= Loc::getMessage("SOA_PAY") ?></div>
                                            <?= CFile::ShowImage($arPaySystem["LOGOTIP"], 100, 100, "border=0\" style=\"width:100px\"", "", false) ?>
                                            <span class="paysystem_name"><?= $arPaySystem["NAME"] ?></span>
                                        </div>
                                        <div>
                                            <? if (strlen($arPaySystem["ACTION_FILE"]) > 0 && $arPaySystem["NEW_WINDOW"] == "Y" && $arPaySystem["IS_CASH"] != "Y") { ?>
                                                <?
                                                $orderAccountNumber = urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]));
                                                $paymentAccountNumber = $payment["ACCOUNT_NUMBER"];
                                                ?>
                                                <script>
                                                    window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=$orderAccountNumber?>&PAYMENT_ID=<?=$paymentAccountNumber?>');
                                                </script>
                                            <?= Loc::getMessage("SOA_PAY_LINK", ["#LINK#" => $arParams["PATH_TO_PAYMENT"] . "?ORDER_ID=" . $orderAccountNumber . "&PAYMENT_ID=" . $paymentAccountNumber]) ?>
                                            <? if (CSalePdf::isPdfAvailable() && $arPaySystem['IS_AFFORD_PDF']) { ?>
                                            <br/>
                                                <?= Loc::getMessage("SOA_PAY_PDF", ["#LINK#" => $arParams["PATH_TO_PAYMENT"] . "?ORDER_ID=" . $orderAccountNumber . "&pdf=1&DOWNLOAD=Y"]) ?>
                                            <? } ?>
                                            <? } else { ?>
                                                <?= $arPaySystem["BUFFERED_OUTPUT"] ?>
                                            <? } ?>
                                        </div>
                                    </div>
                                <? } else { ?>
                                    <span style="color:red;"><?= Loc::getMessage("SOA_ORDER_PS_ERROR") ?></span>
                                <? }
                            } else { ?>
                                <span style="color:red;"><?= Loc::getMessage("SOA_ORDER_PS_ERROR") ?></span>
                            <? }
                        }
                    }
                }
            } else { ?>
                <br/><strong><?= $arParams['MESS_PAY_SYSTEM_PAYABLE_ERROR'] ?></strong>
                <?
            }
            ?>
            <div class="order-complete__box">
                <a class="order-complete__link order-complete__link--master" href="/masters/">
                    Каталог мастеров в помощь по установке оборудования</a>
                <a class="order-complete__link order-complete__link--service" href="/dlya_pokupatelya/akcii_statyi/2020-09-08/proektirovanie-so-skidkoy/">
                    Проектирование системы отопления со скидкой
                </a>
                <a class="order-complete__link order-complete__link--print" href="/lichniy_cabinet/order/<?= $arResult['ORDER']['ID'] ?>/?print">
                    Распечатать информацию о заказе
                </a>
            </div>
        </div>
        <div class="order-complete__review">
            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                [
                    "AREA_FILE_SHOW"   => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE"    => "",
                    "PATH"             => SITE_TEMPLATE_PATH . "/include/review/ya_market_review.php",
                ]
            ); ?>
        </div>
    </div>
<? } else {
    ?>
<!--    <b>--><?//= Loc::getMessage("SOA_ERROR_ORDER") ?><!--</b>-->
    <div class="order-complete__box">
        <?= Loc::getMessage("SOA_ERROR_ORDER_LOST", ["#ORDER_ID#" => $arResult["ACCOUNT_NUMBER"]]) ?>
        <?= Loc::getMessage("SOA_ERROR_ORDER_LOST1") ?>
    </div>
<? } ?>
