<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */

$component = $this->__component;
$component::scaleImages($arResult['JS_DATA'], $arParams['SERVICES_IMAGES_SCALING']);

/*x5 20180912 изменение местоположения ссответственно выбранному в шапке begin*/
$location_code = '';
$city_id = 0;
if (isset($_GET['set_city'])) {
    $city_id = intval($_GET['set_city']);
} else
    //x5 20190517 обратился Антон с тем, что перестала работать подстановка города
//} elseif(
//    !$arResult['USER_VALS']['DELIVERY_LOCATION_BCODE'] &&
//!$arResult['USER_VALS']['DELIVERY_LOCATION'] &&
//!$arResult['ORDER_DATA']['DELIVERY_LOCATION_BCODE']
//)
{

    if (isset($_SESSION['PEK_CURRENT_CITY_ID']) && intval($_SESSION['PEK_CURRENT_CITY_ID']) > 0 && $_SESSION['PEK_CURRENT_CITY_ID']!=$arResult['USER_VALS']['DELIVERY_LOCATION'] && $_SESSION['PEK_CURRENT_CITY_ID']!=$arResult['ORDER_DATA']['DELIVERY_LOCATION']) {
        $city_id = intval($_SESSION['PEK_CURRENT_CITY_ID']);
    }
}

if ($city_id > 0) {
    $arFilter = Array(
        "IBLOCK_ID" => CITIES_IBLOCK_ID, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y",
        '!PROPERTY_LOCATION_ID' => false, '!PROPERTY_HAS_SAM' => false,
        "ID" => $city_id
    );
    $res = \CIBlockElement::GetList(Array('name' => 'asc'), $arFilter, false, false, array("ID", "NAME", "PROPERTY_LOCATION_ID"));
    $result = array();
    if ($arFields = $res->Fetch()) {
        $location_code = $arFields['PROPERTY_LOCATION_ID_VALUE'];
    }
    $location = \CSaleLocation::GetByID($location_code);
    $arResult['SET_CITY'] = $location ? $location['ID'] : '0';
}
$arResult["DELIVERY_TEXT"] = COption::GetOptionString("grain.customsettings", "SALE_ORDER_DELIVERY_TEXT");
//p($arResult,true);
/*x5 20180912 изменение местоположения ссответственно выбранному в шапке end*/