<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arResult['BRANDS'] = array();
if (empty($arParams['NO_BRANDS']) && !empty($arResult["ITEMS"][const_IBLOCK_ID_catalog_PROP_ID_brands]['VALUES']))
{
    $brands_xml_ids = array();
    foreach ($arResult["ITEMS"][const_IBLOCK_ID_catalog_PROP_ID_brands]['VALUES'] as $br_xml_id => $value)
    {
	$brands_xml_ids[] = $br_xml_id;
    }
    
    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "XML_ID"); //IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
    $arFilter = Array("IBLOCK_ID" => const_IBLOCK_ID_brands, 'XML_ID' => $brands_xml_ids);
    $brands = array();
    $res = CIBlockElement::GetList(Array('name' => 'asc'), $arFilter, false, false, $arSelect);
    while ($arFields = $res->Fetch())
    {
	$arResult['BRANDS'][$arFields["XML_ID"]] = $arFields["NAME"];
    }
    
    foreach ($arResult["ITEMS"] as $karItem => $arItem)
    {
//	if (const_IBLOCK_ID_catalog_PROP_ID_brands == $arItem["ID"])
	{
		$is_int = false;
//		if ((int)$arItem["VALUES"][0]["VALUE"] > 0)
//		{
//			$is_int = true;
//		}
		if ($arItem["DISPLAY_TYPE"] != "A")
		{
			$newBr = array();
			foreach ($arItem["VALUES"] as $karValue => $arValue)
			{
				if (const_IBLOCK_ID_catalog_PROP_ID_brands == $arItem["ID"])
				{
					$arResult["ITEMS"][$karItem]["VALUES"][$karValue]["VALUE"] = $arResult['BRANDS'][$arValue["VALUE"]];
					$newBr[$arResult['BRANDS'][$arValue["VALUE"]]] = $arResult["ITEMS"][$karItem]["VALUES"][$karValue];
				}
				else
				{		
					$newBr[$is_int?(float)$arValue["VALUE"]:$arValue["VALUE"]] = $arResult["ITEMS"][$karItem]["VALUES"][$karValue];
	//				deb($newBr, false);
				}
			}

			ksort($newBr);
			$arResult["ITEMS"][$karItem]["VALUES"] = array_values($newBr);
		}
	}
    }
}

