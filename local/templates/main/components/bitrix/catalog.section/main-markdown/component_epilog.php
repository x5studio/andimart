<?php

$basketRes = Bitrix\Sale\Internals\BasketTable::getList(array(
    'filter' => array(
        'FUSER_ID' => Bitrix\Sale\Fuser::getId(),
        'ORDER_ID' => null,
        'LID' => SITE_ID,
        'CAN_BUY' => 'Y',
    )
));

$arItems = [];

while ($item = $basketRes->fetch()) {
    $arItems[] = $item['PRODUCT_ID'];
}

?>

<script>
    $(document).ready(function () {
        var allButtons = document.getElementsByClassName('js-btn-to-basket');
        var buttonIdsToChange = JSON.parse('<?=json_encode($arItems)?>');
        const lengthOfIdPrefix = 19;

        for (var i = 0; i < allButtons.length; i++) {
            if (buttonIdsToChange.indexOf(allButtons[i].id.substr(lengthOfIdPrefix)) != -1) {
                $(allButtons[i]).hide();
                $(allButtons[i]).next().show();
            }
        }
    });
</script>
