<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="content-block pagenavigation_ajax_mainblock">
    <div class="catalog grid grid--xs-2 grid--sm-3 grid--md-4 catalog--borders">
	<?
	foreach ($arResult['ITEMS'] as $key => $arItem)
	{
		//p($arItem["PRICES"]);
		$optimal_price_key = "";
		$opt_price = 99999999999;
		foreach ($arItem["PRICES"] as $key => $price)
		{
			if ($opt_price > $price["VALUE"])
			{
				$opt_price = $price["VALUE"];
				$optimal_price_key = $key;
			}
		}
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
		$strMainID = $this->GetEditAreaId($arItem['ID']);

		$arItemIDs = array(
		    'ID' => $strMainID,
		    'PICT' => $strMainID . '_pict',
		    'SECOND_PICT' => $strMainID . '_secondpict',
		    'STICKER_ID' => $strMainID . '_sticker',
		    'SECOND_STICKER_ID' => $strMainID . '_secondsticker',
		    'QUANTITY' => $strMainID . '_quantity',
		    'QUANTITY_DOWN' => $strMainID . '_quant_down',
		    'QUANTITY_UP' => $strMainID . '_quant_up',
		    'QUANTITY_MEASURE' => $strMainID . '_quant_measure',
		    'BUY_LINK' => $strMainID . '_buy_link',
		    'BASKET_ACTIONS' => $strMainID . '_basket_actions',
		    'NOT_AVAILABLE_MESS' => $strMainID . '_not_avail',
		    'SUBSCRIBE_LINK' => $strMainID . '_subscribe',
		    'COMPARE_LINK' => $strMainID . '_compare_link',
		    'PRICE' => $strMainID . '_price',
		    'DSC_PERC' => $strMainID . '_dsc_perc',
		    'SECOND_DSC_PERC' => $strMainID . '_second_dsc_perc',
		    'PROP_DIV' => $strMainID . '_sku_tree',
		    'PROP' => $strMainID . '_prop_',
		    'DISPLAY_PROP_DIV' => $strMainID . '_sku_prop',
		    'BASKET_PROP_DIV' => $strMainID . '_basket_prop',
		);
		?>
		<div class="catalog__item grid__item" <? echo $strMainID; ?>>
		    <div class="catalog__item-in">
			<div class="catalog__img catalog__img_bgr" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>')">
			    <a class="link-hidden" href="<?= $arItem["DETAIL_PAGE_URL"] ?>" >
				<img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"  alt="<?= $arItem["NAME"] ?>">
			    </a>
			</div>
			<div class="catalog__content">
			    <div class="catalog__content-top">
				<div class="catalog__title">
	                            <a class="link-hidden" href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a>
				</div>
				<div class="catalog__brand">
				    <?
				    if ($arItem["PROPERTIES"]["BREND"]["VALUE"])
				    {
					    ?>
					    Бренд: <?= $arItem["PROPERTIES"]["BREND"]["VALUE"] ?>
					    <?
				    }
				    ?>
				    <? if ($arItem["CATALOG_QUANTITY"])
				    {//CATALOG_QUANTITY ?>
					    <div class="catalog__avail">
						<span class="text-success">●</span> в наличии</div>
	<? }
	else
	{ ?>
					    <div class="catalog__avail">
						<span class="text-warn">●</span> нет наличии</div>
				    <? } ?>
				</div>
			    </div>
				    <? if (!empty($arItem["PRICES"][$optimal_price_key]["PRINT_DISCOUNT_VALUE"]) && !empty($arItem["CATALOG_QUANTITY"]))
				    { ?>
				    <div class="catalog__content-bot">
					<div class="product-price">
		<? if ($arItem["PRICES"][$optimal_price_key]["DISCOUNT_DIFF"])
		{ ?>
						    <div class="product-price__old">
			<?= $arItem["PRICES"][$optimal_price_key]["PRINT_VALUE"]; ?>
						    </div>
		<? } ?>
					    <div class="product-price__current"><?= $arItem["PRICES"][$optimal_price_key]["PRINT_DISCOUNT_VALUE"] ?></div>
					</div>
					<div class="catalog__buy">
					    <div class="catalog__buy-count">
						<div class="input-group">
						    <div class="input-group__item catalog__buy-input">
							<input class="input-text input-text--sm text-center" type="text" value="1">
						    </div>
						    <div class="input-group__item catalog__buy-label">
							<div class="input-posfix">шт.</div>
						    </div>
						</div>
					    </div>
					    <div class="catalog__buy-btn">
						<button class="btn btn--sm add_to_cart_section" rel="<?= $arItem["DETAIL_PAGE_URL"] ?>?action=ADD2BASKET&id=<?= $arItem["ID"] ?>" type="button">
						    <span>В корзину</span>
						</button>
					    </div>
					</div>
				    </div>
		<? } ?>
			</div>
		    </div>
		</div>

	<? } ?>

</div>
<? echo $arResult["NAV_STRING"]; ?>
</div>