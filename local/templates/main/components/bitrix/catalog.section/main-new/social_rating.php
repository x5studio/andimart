<?
//$this->addExternalCss("/local/styles.css");
?>
<div class="stars"
     data-rating="<?=($arItem['AVERAGE_RATING'] ? $arItem['AVERAGE_RATING'] : 0)?>"
     itemtype="http://schema.org/AggregateRating" itemprop="aggregateRating"
     itemscope="itemscope">
    <meta itemprop="ratingValue" content="<?=($arItem['AVERAGE_RATING'] ? $arItem['AVERAGE_RATING'] : 0)?>">
    <meta itemprop="bestRating" content="5">
    <meta itemprop="ratingCount" content="<?=($arItem['REVIEW_COUNT'] ? $arItem['REVIEW_COUNT'] : 0)?>">
    <div class="stars__list">
        <i class="stars__star"></i>
        <i class="stars__star"></i>
        <i class="stars__star"></i>
        <i class="stars__star"></i>
        <i class="stars__star"></i>
    </div>
</div>
<div class="social-rating">
    <!--
    <div class="rating" data-rating="<?=($arItem['AVERAGE_RATING'] ? $arItem['AVERAGE_RATING'] : 0)?>">
        <div class="fill-rating"></div>
    </div>
    -->
    <a href="<?=$arItem["DETAIL_PAGE_URL"].'#reviews'?>"
       target="_blank"
       class="comments-ref" data-review-count="<?=($arItem['REVIEW_COUNT'] ? $arItem['REVIEW_COUNT'] : 0)?>"
    >
        <span class="comments-count"><?=($arItem['REVIEW_COUNT'] ? $arItem['REVIEW_COUNT'] : '')?></span>
    </a>
</div>