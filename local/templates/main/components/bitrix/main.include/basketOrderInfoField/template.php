<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if($arResult['BASKET_IS_EMPTY'] == 'N') {?>
<div class="info-field-<?=isset($arResult['PAGE']) ? $arResult['PAGE'] : 'cart'?>">
    <div class="exclaim"></div>
    <?if ($arResult["FILE"] <> '')
        include($arResult["FILE"]);?>
</div>
<?}?>