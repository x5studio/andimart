<?
$cntBasketItems = CSaleBasket::GetList(
    array(),
    array(
        "FUSER_ID" => CSaleBasket::GetBasketUserID(),
        "LID" => SITE_ID,
        "ORDER_ID" => "NULL"
    ),
    array()
);
$arResult['BASKET_IS_EMPTY'] = $cntBasketItems === 0 ? 'Y' : 'N';
$path = $APPLICATION->GetCurPage();
if(strripos($path, 'order')) {
    $arResult['PAGE'] = 'order';
} elseif (strripos($path, 'cart')) {
    $arResult['PAGE'] = 'cart';
}
?>