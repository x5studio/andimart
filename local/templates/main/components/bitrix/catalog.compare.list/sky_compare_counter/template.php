<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

?>

<a href="/compare/" class=" nav__link skyCompareCounter">
<svg version="1.0" xmlns="http://www.w3.org/2000/svg"
 width="479.000000pt" height="479.000000pt" viewBox="0 0 479.000000 479.000000"
 preserveAspectRatio="xMidYMid meet">
<metadata>
Created by potrace 1.15, written by Peter Selinger 2001-2017
</metadata>
<g transform="translate(0.000000,479.000000) scale(0.100000,-0.100000)"
fill="#000000" stroke="none">
<path d="M1979 4137 c-18 -12 -44 -38 -56 -56 l-23 -34 2 -1533 c3 -1529 3
-1533 24 -1561 11 -15 40 -38 64 -51 43 -22 51 -22 405 -22 354 0 362 0 405
23 33 16 51 34 68 67 l22 45 -2 1521 -3 1521 -28 36 c-52 68 -44 67 -465 67
-378 0 -379 0 -413 -23z"/>
<path d="M3370 3063 c-37 -14 -74 -49 -91 -87 -18 -39 -19 -86 -19 -1002 l0
-961 23 -43 c16 -30 37 -51 67 -67 l44 -24 377 3 376 3 37 29 c70 53 66 -8 66
1069 l0 974 -23 34 c-12 18 -38 44 -56 56 -34 23 -35 23 -410 22 -207 0 -383
-3 -391 -6z"/>
<path d="M621 2233 c-18 -9 -45 -34 -60 -56 l-26 -41 -3 -520 c-2 -287 0 -544
3 -573 7 -57 34 -104 78 -137 27 -20 41 -21 395 -24 241 -2 380 1 404 8 45 13
94 67 108 120 7 26 10 218 8 582 -3 607 0 582 -75 633 l-38 25 -380 0 c-333 0
-384 -3 -414 -17z"/>
</g>
</svg>
<span class="nav__title"><?=count($arResult); ?></span>
</a>
