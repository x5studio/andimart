<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>

<? if (!empty($arResult)): ?>
    <div class="content-block content-block--push-top">
    <nav class="box-nav">
        <?
        $previousLevel = 0;
        foreach ($arResult as $arItem):
            if($arParams["SHOW_ONLY_DEAPTH_LEVEL"] && $arParams["SHOW_ONLY_DEAPTH_LEVEL"]<$arItem["DEPTH_LEVEL"])
            ?>

                <a class="box-nav__item" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>



        <? endforeach ?>


    </nav>
    </div>
<? endif ?>