<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<? if (!empty($arResult)): ?>
    <ul class="nav__list">
        <?$previousLevel = 0;
        foreach ($arResult as $parent_key =>$arItem):?>
            <? if ($previousLevel > 1 && $arItem["DEPTH_LEVEL"] == 1) {
                ?>
                <? echo("</div></div></li>"); ?>
            <? } ?>
            <? if ($arItem["IS_PARENT"]): ?>
                <? if ($arItem["DEPTH_LEVEL"] == 1): ?>
                    <li class="nav__item nav__item--has-child js-has-drop <? if ($arItem["SELECTD"]) echo "nav__item--current"; ?>">
                        <a class="nav__link" href="<?= $arItem["LINK"] ?>"> <span
                                class="nav__title"><?= $arItem["TEXT"] ?></span></a>
                        <div class="nav__child">
                            <div class="flex flex--start">
								<div class="flex__item">
                                <?$cnt = 0;
                                if($parent_key == 0){
                                    $cnt = 1;
                                }
								$triple = ceil(count($arItem["SUBMENU"])/3);
                                foreach ($arItem["SUBMENU"] as $key1 => $arItem2) {
                                    $cnt++;
									if($cnt == ($triple+1) || $cnt == (($triple*2)+1)){
										echo '</div><div class="flex__item">';
									}?>
                                    <ul class="nav__child-group">
                                        <li class="nav__child-item nav__child-item--heading">
                                            <a href="<?= $arItem2["LINK"] ?>"
                                               class="sub-nav__child-link"><?= $arItem2["TEXT"] ?>
                                            </a>
                                        </li>
                                        <? foreach ($arItem2["SUBMENU"] as $key2 => $arSubItem3) {?>
                                            <li class="nav__child-item ">
                                                <a href="<?= $arSubItem3["LINK"] ?>"
                                                   class="sub-nav__child-link"><?= $arSubItem3["TEXT"] ?>
                                                </a>
                                            </li>
                                         <?}?>
                                    </ul>
                                <?}?>
								</div>
                            </div>
                        </div>
                    </li>
                <? endif ?>
            <? else: ?>
                <? if ($arItem["DEPTH_LEVEL"] == 1) {?>
                    <li class="nav__item nav__item--has-child js-has-drop <? if ($arItem["SELECTD"]) echo "nav__item--current"; ?>">
                        <a class="nav__link" href="<?= $arItem["LINK"] ?>"> <span
                                class="nav__title"><?= $arItem["TEXT"] ?></span></a>
                    </li>
                 <?}
            endif ?>
            <? $previousLevel = $arItem["DEPTH_LEVEL"]; ?>
        <? endforeach ?>
        <? if ($previousLevel > 1)://close last item tags?>
            <? echo("</div></div></li>"); ?>
        <? endif ?>
    </ul>
<? endif ?>