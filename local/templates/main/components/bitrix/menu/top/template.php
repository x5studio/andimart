<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
	<ul class="sub-nav__list">
<?
$previousLevel = 0;
foreach($arResult as $arItem):
	$style = '';
	if($arItem["PARAMS"]["NOCLICK"]=="Y"){
		$arItem["LINK"]="javascript:void(0)";
		$style = "style='cursor:default'";
	}
	?>
	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>
	<?if ($arItem["IS_PARENT"]):?>
		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
	<li class="sub-nav__item sub-nav__item--has-child js-has-drop">
	<a <?=$style?> class="sub-nav__link"  href="<?=$arItem["LINK"]?>" ><?=$arItem["TEXT"]?></a>
	<ul class="sub-nav__child">
		<?endif?>
	<?else:?>
		<?if ($arItem["PERMISSION"] > "D"):?>
			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
                <li class="sub-nav__item">
                    <a class="sub-nav__link <?=($arItem['PARAMS']['CLASS'] == 'btn-gradient')?'btn-gradient':''?>" <?=($arItem['PARAMS']['CLASS'] == 'btn-gradient')?' target="_blank" ':''?> href="<?=$arItem["LINK"]?>" <?=($arItem["LINK"] == 'https://andimart.otoplenie.city/')?"onclick=\"ym(45162276,'reachGoal','6');return true;\"":''?>>
                        <?=($arItem['PARAMS']['CLASS'] == 'btn-gradient')?'<span class="btn-gradient__text">':''?>
                        <?=$arItem["TEXT"]?>
                        <?=($arItem['PARAMS']['CLASS'] == 'btn-gradient')?'</span>':''?>
                    </a>
                </li>
            <?else:?>
				<li class="sub-nav__child-item"><a href="<?=$arItem["LINK"]?>" class="sub-nav__child-link"><?=$arItem["TEXT"]?></a></li>
			<?endif?>
		<?endif?>
	<?endif?>
	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>
<?endforeach?>
<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>
</ul>
<?endif?>