<? //deb($arResult, false) ?>
<div class="content-block content-block--pull-top content-block--sm-bot island">
    <div class="island-head-tabs">
	<div class="island-head-tabs__item island-head-tabs__item--current">
	    <div class="island-head-tabs__title">История заказов</div>
	</div>
	<div class="island-head-tabs__item">
	    <a class="island-head-tabs__link" href="/lichniy_cabinet/">
		<div class="island-head-tabs__title">Профиль</div>
	    </a>
	</div>
    </div>
    <div class="history-head row hidden-xs-down">
	<div class="col-sm-2">№ заказа</div>
	<div class="col-sm-3">Доставка</div>
	<!--<div class="col-sm-3">Оплата</div>-->
	<div class="col-sm-2">Состояние заказа</div>

	<div class="col-sm-2">Трекинговый номер</div>
    </div>
</div>

<ul class="history">
    <?
    foreach ($arResult['ORDERS'] as $arOrder)
    {
        $delivery_service_name = '';

//        if($arOrder['ORDER']['ACCOUNT_NUMBER'] == '264' || $arOrder['ORDER']['ACCOUNT_NUMBER'] == '261'){
//            p('$arOrder = ');
//            p($arOrder);
//        }
		
	    $orderProps = array();
	    $db_props = CSaleOrderPropsValue::GetOrderProps($arOrder['ORDER']['ID']);
	    while ($arProps = $db_props->Fetch())
	    {
		    $orderProps[$arProps['CODE']] = $arProps;
	    }

//        if($arOrder['ORDER']['ACCOUNT_NUMBER'] == '264' || $arOrder['ORDER']['ACCOUNT_NUMBER'] == '261'){
//            p('$orderProps = ');
//            p($orderProps);
//        }

        $ar_deliv = \Bitrix\Sale\Delivery\Services\Manager::getById($arOrder['ORDER']['DELIVERY_ID']);
//        if($arOrder['ORDER']['ACCOUNT_NUMBER'] == '463' || $arOrder['ORDER']['ACCOUNT_NUMBER'] == '462'){
//            p('$ar_deliv = ');
//            p($ar_deliv);
//        }

        $db_res = \CSaleDeliveryHandler::GetList(array(), array());
        while ($ar_res = $db_res->Fetch()) {
            if(strpos($ar_deliv['CODE'], $ar_res['SID']) === 0) {
                $delivery_service_name = $ar_res['NAME'] . ', ' . $ar_deliv['NAME'];
                break;
            }
        }

        //p($delivery_service_name);

		//p($orderProps["TRACKING_NUMBER"],true);
//			deb($orderProps);
	    ?>
	    <li class="history__item js-history-item">
		<a class="history__head island js-history-item__toggle" href="ajax-history-body.html">
		    <div class="row">
                <div class="col-sm-2">
                    <span class="hidden-sm-up text-gray">№ заказа: </span><b><?= $arOrder['ORDER']['ACCOUNT_NUMBER'] ?></b>
                </div>

                <div class="col-sm-3">
                    <? if ($arOrder['ORDER']['DELIVERY_ID'] == 3) { ?>
                        <!--
                        <span class="hidden-sm-up text-gray">Доставка по адресу: </span>
                        <?=formAddress2($orderProps)?>
                        -->
                        <?=reset($arOrder['SHIPMENT'])['DELIVERY_NAME']?>
                    <? } elseif ($arOrder['ORDER']['DELIVERY_ID'] == 8) { ?>
                        <!--
                        <span class="hidden-sm-up text-gray">Пункт выдачи: </span>
                        <?=$orderProps['PICKPOINT']['VALUE']?>
                        -->
                        <?=reset($arOrder['SHIPMENT'])['DELIVERY_NAME']?>
                    <? } else { ?>
                        <?=$delivery_service_name?>
                    <? } ?>
                </div>
                <div class="col-sm-2">
                    <span class="hidden-sm-up text-gray">Состояние заказа: </span><?= $arResult['INFO']['STATUS'][$arOrder['ORDER']['STATUS_ID']]['NAME'] ?>
                </div>
                <?/*div class="col-sm-3">
                    <span class="hidden-sm-up text-gray">Оплата заказа: </span><?= $arResult['INFO']['PAY_SYSTEM'][$arOrder['ORDER']['PAY_SYSTEM_ID']]['NAME'] ?>

                </div*/?>
                <?
                if($orderProps["TRACKING_NUMBER"]["VALUE"]){
				?>
                    <div class="col-sm-2">
                    <span class="hidden-sm-up text-gray">Трекинговый номер: </span><?=$orderProps["TRACKING_NUMBER"]["VALUE"]?>  </div>
                <?}?>
		    </div>

		</a>
		<div class="history__body js-history-item__body island">
		    <button class="history__close js-history-item__toggle" type="button"></button>
		    <div class="history__num">Заказ № <?= $arOrder['ORDER']['ACCOUNT_NUMBER'] ?></div>

			<? if ($arOrder['ORDER']['DELIVERY_ID'] == 3)
			{ ?>
			    <div class="history__info">Доставка по адресу: <?= formAddress2($orderProps) ?><?/* <a href="">посмотреть на карте</a>*/?></div>
			<? } elseif ($arOrder['ORDER']['DELIVERY_ID'] == 8){ ?>

			    <div class="history__info">Пункт выдачи: <?=$orderProps['PICKPOINT']['VALUE']?><?/* <a href="">посмотреть на карте</a>*/?>
			    </div>
            <? } else { ?>
                <?//=$delivery_service_name?>
            <? } ?>
		    <?/*div class="history__info">Оплата заказа: <?= $arResult['INFO']['PAY_SYSTEM'][$arOrder['ORDER']['PAY_SYSTEM_ID']]['NAME'] ?></div*/?>
		    <div class="history__info">Состояние заказа: <?= $arResult['INFO']['STATUS'][$arOrder['ORDER']['STATUS_ID']]['NAME'] ?></div>
			<?
			if($orderProps["TRACKING_NUMBER"]["VALUE"]){
				?>
		    <div class="history__info">Трекинговый номер: <?=$orderProps["TRACKING_NUMBER"]["VALUE"]?> </div>
			<?
			}
			?>
		    <div class="history__products">
			<div class="history-products">
			    <div class="history-products__head row hidden-xs-down">
				<div class="col-sm-6">Товар</div>
				<div class="col-sm-2">Цена</div>
				<div class="col-sm-2">Кол-во</div>
				<div class="col-sm-2">Сумма</div>
			    </div>
			    <ul class="history-products__list">
				<? foreach ($arOrder['BASKET_ITEMS'] as $arItem) { 
					
				$item = CIBlockElement::GetByID($arItem['PRODUCT_ID'])	;
				$item = $item->Fetch();
				?>
				<li class="history-products__item">
				    <div class="row row--vcentr">
					<div class="col-4 col-sm-2" style="padding-bottom: 100px; background: url('<?=CFile::GetPath($item['PREVIEW_PICTURE'])?>') no-repeat center center/contain;">
					    
					</div>
					<div class="col-8 col-sm-4">
					    <div class="text-bold">
						<a class="link-hidden" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a>
					    </div></div>
					<div class="col-8 offset-4 col-sm-2 offset-sm-0 text-small-expand">
					    <div class="hidden-sm-up text-gray">Цена: </div>
					    <div class="product-price">
						<? if (!empty($arItem['DISCOUNT_PRICE']) && ((int)$arItem['DISCOUNT_PRICE'] > 0)) { ?>
						<div class="product-price__old"><?=CurrencyFormat($arItem['BASE_PRICE'], $arItem['CURRENCY'])?></div>
						<? } ?>
						<div class="product-price__current"><?=CurrencyFormat($arItem['PRICE'], $arItem['CURRENCY'])?></div>
					    </div>
					</div>
					<div class="col-8 offset-4 col-sm-2 offset-sm-0 text-small-expand">
					    <span class="hidden-sm-up text-gray">Кол-во: </span><?=$arItem['QUANTITY']?></div>
					<div class="col-8 offset-4 col-sm-2 offset-sm-0 text-small-expand">
					    <span class="hidden-sm-up text-gray">Сумма: </span>
					    <span class="history-products__sum"><?=CurrencyFormat($arItem['PRICE']*$arItem['QUANTITY'], $arItem['CURRENCY'])?></span>
					</div>
				    </div>
				</li>
				<? } ?>
			    </ul>
			</div>
		    </div>
		    <div class="history__sum">
			<div class="sum">
				<!--<div class="sum__row">Итого</div>-->
<!--				<div class="sum__row">Товаров на
				    <span class="text-nowrap">13 880 руб.</span>
				</div>
									<div class="sum__row">Скидка
				    <span class="text-nowrap">1 388 руб.</span>
				</div>-->
				<? 
				$arOrder['ORDER']['PRICE_DELIVERY'] = (int)$arOrder['ORDER']['PRICE_DELIVERY'];
				if (!empty($arOrder['ORDER']['PRICE_DELIVERY'])) { ?>
					<div class="sum__row">Доставка: <?=CurrencyFormat($arOrder['ORDER']['PRICE_DELIVERY'], $arOrder['ORDER']['CURRENCY'])?></div>

				<div class="sum__row">Итого: <?=$arOrder['ORDER']['FORMATED_PRICE']?></div>
			</div>
		    </div>
		<?//deb($arOrder, false) PRICE_DELIVERY?>
		</div>
	    </li>
<? } ?>
    <? } ?>
</ul>
<?= $arResult["NAV_STRING"]; ?>
<?
/* nav class="pagination">
  <ul class="pagination__list">
  <!-- Выводить не более 7 элементов, включая разделитель-->
  <li class="pagination__item">
  <a class="pagination__link" href="#">
  <span class="pagination__title">1</span>
  </a>
  </li>
  <li class="pagination__item pagination__item--current">
  <span class="pagination__title">2</span>
  </li>
  <li class="pagination__item">
  <a class="pagination__link" href="#">
  <span class="pagination__title">3</span>
  </a>
  </li>
  <li class="pagination__item">
  <a class="pagination__link" href="#">
  <span class="pagination__title">4</span>
  </a>
  </li>
  <li class="pagination__item pagination__item--dots">
  <span class="pagination__title">...</span>
  </li>
  <li class="pagination__item">
  <a class="pagination__link" href="#">
  <span class="pagination__title">24</span>
  </a>
  </li>
  <li class="pagination__item">
  <a class="pagination__link" href="#">
  <span class="pagination__title">25</span>
  </a>
  </li> </ul>
  </nav */?>
