<? 

//error_reporting(2047);
global $APPLICATION;
$APPLICATION->RestartBuffer(); 
$orderProps = array();
$db_props = CSaleOrderPropsValue::GetOrderProps($arResult['ID']);
while ($arProps = $db_props->Fetch())
{
	$orderProps[$arProps['CODE']] = $arProps;
}

?>
<!DOCTYPE html>
<html class="no-js" lang="ru-RU" prefix="og: http://ogp.me/ns#">
    <head>
	<link href="<?= SITE_TEMPLATE_PATH ?>/static/css/styles.css?v=1a06a750" rel="stylesheet">
	<link href="/local/templates/.default/css/custom.css" rel="stylesheet">
    </head>
    <body style="background-color: #fff;">

	<ul>
	    <li class="history__item js-history-item is-active">		
		<div class="history__body js-history-item__body island">
		    <div class="history__num">Заказ № <?= $arResult['ACCOUNT_NUMBER'] ?></div>

			<? if ($arResult['DELIVERY_ID'] == 3)
			{ ?>
			    <div class="history__info">Доставка по адресу: <?= formAddress2($orderProps) ?><?/* <a href="">посмотреть на карте</a>*/?></div>
			<? } elseif ($arResult['DELIVERY_ID'] == 8){ ?> 
			    
			    <div class="history__info">Пункт выдачи: <?=$orderProps['PICKPOINT']['VALUE']?><?/* <a href="">посмотреть на карте</a>*/?>
			    </div>
			<? } ?>
		    <?/*div class="history__info">Оплата заказа: <?= $arResult['PAYMENT'][$arResult['ID']]['PAY_SYSTEM']['NAME'] ?></div*/?>
		    <?/*div class="history__info">Состояние заказа: <?= $arResult['STATUS']['NAME'] ?></div>
		    <div class="history__info">Трекинговый номер: —</div*/?>
		    <div class="history__products">
			<div class="history-products">
			    <div class="history-products__head row hidden-xs-down">
				<div class="col-sm-6" style="color: initial;">Товары</div>
			    </div>
			    <ul class="history-products__list">
				<? foreach ($arResult['BASKET'] as $arItem) { 
				?>
				<li class="history-products__item print">
				    <div class="row row--vcentr">
					<div class="col-4" style="max-width: 100px;padding-bottom: 100px; background: url('<?=CFile::GetPath($arItem['PREVIEW_PICTURE'])?>') no-repeat center center/contain;">
					    
					</div>
					<div class="col-8">
					    <div class="col-12">
						<div class="text-bold">
						    <a class="link-hidden" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a>
						</div></div>
					    <div class="col-12 text-small-expand">
						<div class="">Цена: </div>
						<div class="product-price">
						    <? if (!empty($arItem['DISCOUNT_PRICE']) && ((int)$arItem['DISCOUNT_PRICE'] > 0)) { ?>
							<div class="product-price__current" style="color: red"><s><?=CurrencyFormat($arItem['BASE_PRICE'], $arItem['CURRENCY'])?></s></div>
						    <? } ?>
						    <div class="product-price__current"><?=CurrencyFormat($arItem['PRICE'], $arItem['CURRENCY'])?></div>
						</div>
					    </div>
					    <div class="col-12 text-small-expand">
						<span class="">Кол-во: </span><?=$arItem['QUANTITY']?></div>
					    <div class="col-12 text-small-expand">
						<span class="">Сумма: </span>
						<span class="history-products__sum"><?=CurrencyFormat($arItem['PRICE']*$arItem['QUANTITY'], $arItem['CURRENCY'])?></span>
					    </div>
					</div>
				    </div>
				</li>
				<? } ?>
			    </ul>
			</div>
		    </div>
		    <div class="history__sum">
			<div class="sum">
				<!--<div class="sum__row">Итого</div>-->
<!--				<div class="sum__row">Товаров на
				    <span class="text-nowrap">13 880 руб.</span>
				</div>
									<div class="sum__row">Скидка
				    <span class="text-nowrap">1 388 руб.</span>
				</div>-->
				<? 
				$arResult['PRICE_DELIVERY'] = (int)$arResult['PRICE_DELIVERY'];
				if (!empty($arResult['PRICE_DELIVERY'])) { ?>
					<div class="sum__row">Доставка: <?=$arResult['PRICE_DELIVERY_FORMATED']?></div>
				<? } ?>
				<div class="sum__row">Итого: <?=$arResult['PRICE_FORMATED']?></div>
			</div>
		    </div>
		</div>
	</li>
</ul>
	<script>
		window.print();
	</script>
	</body>
	</html>
<?
die();
?>