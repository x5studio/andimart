<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<ul class="news grid grid--sm-3">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

	<li class="news__item grid__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<a class="news__item-in" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
			<div class="news__image">
				<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="220" height="220" alt>
			</div>
			<?
			if($arItem["PROPERTIES"]["ACTIONTEXT"]["VALUE"]){
			?>
			<div class="news__label"><?=$arItem["PROPERTIES"]["ACTIONTEXT"]["VALUE"]?>1</div>
			<?
			}
			?>
			<div class="news__title"><?=$arItem["NAME"]?></div>
		</a>
	</li>
<?endforeach;?>
</ul>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>

