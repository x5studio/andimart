<form novalidate="true" method="post" name="form2" action="/catalog/calcs/" class='js-form-validator'>
			    <? 
				if (!empty($_POST['nasos_output'])) 
				{
					$nasos_output['area'] = (float)$_POST['nasos_output']['area'];
					$nasos_output['type'] = (int)$_POST['nasos_output']['type'];
					$nasos_output['result'] = false;
					$b = false;
					switch ($nasos_output['type'])
					{
						case 1: $b = 20; break;
						case 2: $b = 5; break;
					}

					if ($nasos_output['area'] > 0 && $b !== false)
					{
						$a1 = $nasos_output['area']*0.1;
						$nasos_output['result'] = $a1/$b;
					} ?>
				<? } ?>
				<div class="row">
				  <div class="col-sm-8">
				    <div class="content-block">
				      <div class="input-group">
					<div class="input-group__item h5">Введите площадь отапливаемого помещения:</div>
					<div class="input-group__item input-group__item--padd">
					    <input class="input-text input-text--fixwidth" required="" name="nasos_output[area]" type="text" value='<?=$nasos_output['area']?>'>
					</div>
					<div class="input-group__item h5">м<sup>2</sup></div>
				      </div>
				    </div>
                    <div class="content-block">
                      <div class="form__title text-left">Выберите тип системы отопления:</div>
                      <div class="check-list check-list--expand">
                        <div class="check-list__item">
                          <label class="radio">
                            <input class="radio__input" required="" type="radio" value='1' name="nasos_output[type]"<? if ($nasos_output['type'] == 1) { ?> checked=""<? } ?>>
                            <span class="radio__label">радиаторное отопление</span>
                          </label>
                        </div>
                        <div class="check-list__item">
                          <label class="radio">
                            <input class="radio__input" required="" type="radio" value='2' name="nasos_output[type]"<? if ($nasos_output['type'] == 2) { ?> checked=""<? } ?>>
                            <span class="radio__label">система "теплый пол"</span>
                          </label>
                        </div>
                      </div>
                    </div>
	<div class="content-block">
	  <button class="btn btn--inverse" type="submit">
	    <span>Рассчитать</span>
	  </button>
	</div>
					</div>
					<div class="col-sm-4 hidden-xs-down text-center">
					  <img src="<?= SITE_TEMPLATE_PATH ?>/static/images/calc.svg?v=0aca4775" width="85" height="96" alt>
					</div>
				      </div>
				<? 
				if (!empty($nasos_output['result'])) 
				{
					?>
					<div class="content-block">
						<div class="calc-result">Требуемая ориентировочная производительность циркуляционного насоса равна <?=ceil($nasos_output['result']*10)/10?>м<span style='font-family: Arial'>³</span>/час</div>
					</div>
					<div class="content-block">
						<a class="btn btn--fluid" href="/catalog/calcs/proizvoditelnosti_tsirkulyatsionnogo_nasosa/filter/proizvoditelnost_m_chas-from-<?=ceil($nasos_output['result']*10)/10?>/apply/">Посмотреть в каталоге
							<span class="hidden-xs-down">подходящие</span>
						</a>
					</div>
				<? } ?>
			</form>