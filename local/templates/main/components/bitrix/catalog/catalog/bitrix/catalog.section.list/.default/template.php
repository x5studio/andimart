<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
//p($arResult["SECTIONS"]);
foreach($arResult["SECTIONS"] as $sec){
	if(!$sec["PICTURE"]["SRC"])
		$sec["PICTURE"]["SRC"] = SITE_TEMPLATE_PATH."/static/images/nopic.jpg";
	
	$subsction_list = array();
	$arFilter = array('IBLOCK_ID' => const_IBLOCK_ID_catalog, "SECTION_ID"=>$sec["ID"], 'ACTIVE' => 'Y'); // выберет потомков без учета активности
	$rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter, false);//array("ELEMENT_SUBSECTIONS"=>"Y",'CNT_ACTIVE' => 'Y'));
	while ($arSect = $rsSect->GetNext())
	{
		$subsction_list[] =$arSect;
	} 
	?>
	<div class="col-12 col-sm-6 b-list__item">
                      <div class="category-thumb">
                        <div class="category-thumb__image">
                          <a class="link-hidden" href="<?=$sec["SECTION_PAGE_URL"]?>">
                            <img src="<?=$sec["PICTURE"]["SRC"]?>" width="116" height="104" alt="<?=$sec["NAME"]?>">
                          </a>
                        </div>
                        <div class="category-thumb__title title-line title-line--accent js-title-line">
                          <a class="link-hidden" href="<?=$sec["SECTION_PAGE_URL"]?>"><?=$sec["NAME"]?><?/*&nbsp;(<?=$sec["ELEMENT_CNT"]?>)*/?></a>
                        </div>
						<?
						if(is_array($subsction_list) && !empty($subsction_list)){
						?>
							<ul class="category-thumb__items">
							<? foreach($subsction_list as $sub_section){
								$cnt = CIBLockElement::GetList(array(), array(
								    'SECTION_ID' => $sub_section['ID'],
								    'IBLOCK_ID' => const_IBLOCK_ID_catalog, 
								    'ACTIVE' => 'Y'), array('IBLOCK_ID'));
								$cnt = $cnt->Fetch();
								
								?>
								<li class="category-thumb__item">
									<a class="category-thumb__link" href="<?=$sub_section["SECTION_PAGE_URL"]?>"><?=$sub_section["NAME"]?>
									  <span class="category-thumb__count"><? if (!empty($cnt['CNT'])) { ?>&nbsp;(<?=$cnt['CNT']?>)<? } ?></span>
									</a>
								</li>
								<?
							}
								?>
							</ul>
						<?
						}else{
							?>
							<div class="category-thumb__text"><?=$sec["DESCRIPTION"]?></div>
							<?
							
						}
						?>

                      </div>
                    </div>
	<?
}
?>
<?

?>