<?php

if (empty($arResult['PREVIEW_TEXT']) 
	&& !empty($arResult['DETAIL_TEXT'])) 
{
	
	$arResult['PREVIEW_TEXT'] = mb_substr($arResult['DETAIL_TEXT'], 0, (mb_strpos($arResult['DETAIL_TEXT'], '.', 200, 'utf-8')+1), 'utf-8');
	$arResult['~PREVIEW_TEXT'] = htmlspecialchars_decode($arResult['PREVIEW_TEXT']);
	
	$arResult['DETAIL_TEXT'] = trim(mb_substr($arResult['DETAIL_TEXT'], (mb_strpos($arResult['DETAIL_TEXT'], '.', 200, 'utf-8')+1), NULL, 'utf-8'));
	$arResult['~DETAIL_TEXT'] = htmlspecialchars_decode($arResult['DETAIL_TEXT']);
}
