<form novalidate="true" method="post" name="form2" action="/catalog/calcs/" class='js-form-validator'>
			    <? 
				if (!empty($_POST['nasos'])) 
				{
					$nasos['cnt'] = (int)$_POST['nasos']['cnt'];
					$nasos['result'] = false;
					$a = $nasos['cnt'];
					if ($a > 0)
					{
						$nasos['result'] = $a*10*0.8*60/1000;
					} ?>
				<? } ?>
				<div class="row">
				  <div class="col-sm-8">
				    <div class="content-block">
				      <div class="input-group">
					<div class="input-group__item h5">Укажите количество точек разбора воды в Вашем доме (смесители, краны, санузлы и т.д.):</div>
					<div class="input-group__item input-group__item--padd">
					    <input class="input-text input-text--fixwidth" required="" name="nasos[cnt]" type="text" value='<?=$nasos['cnt']?>'>
					</div>
					<div class="input-group__item h5">шт.</div>
				      </div>
				    </div>
	<div class="content-block">
	  <button class="btn btn--inverse" type="submit">
	    <span>Рассчитать</span>
	  </button>
	</div>
					</div>
					<div class="col-sm-4 hidden-xs-down text-center">
					  <img src="<?= SITE_TEMPLATE_PATH ?>/static/images/calc.svg?v=0aca4775" width="85" height="96" alt>
					</div>
				      </div>
				<? 
				if (!empty($nasos['result'])) 
				{
					?>
					<div class="content-block">
						<div class="calc-result">Для Вашей системы подойдет насос со следующей производительностью: <?=ceil($nasos['result'])?> м<span style='font-family: Arial'>³</span>/час</div>
					</div>
					<div class="content-block">
						<a class="btn btn--fluid" href="/catalog/calcs/proizvoditelnosti_nasosnoy_stantsii_dlya_doma/filter/proizvoditelnost_m_chas-from-<?=ceil($nasos['result'])?>/apply/">
							Показать все <span class="hidden-xs-down">насосные станции с производительностью <?=ceil($nasos['result'])?> м<span style='font-family: Arial'>³</span>/час в каталоге товаров</span>
						</a>
					</div>
				<? } ?>
			</form>