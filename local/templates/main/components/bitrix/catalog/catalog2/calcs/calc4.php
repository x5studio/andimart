<form novalidate="true" method="post" name="form2" action="/catalog/calcs/" class='js-form-validator'>
			    <? 
				if (!empty($_POST['tepl_pol'])) 
				{
					$tepl_pol['area'] = (float)$_POST['tepl_pol']['area'];
					$tepl_pol['mosh'] = (int)$_POST['tepl_pol']['mosh'];
					$tepl_pol['result'] = false;
					$a = $tepl_pol['area'];
					$b = false;
					switch ($tepl_pol['mosh'])
					{
						case 1: $b = 0.15; break;
						case 2: $b = 0.2; break;
						case 3: $b = 0.25; break;
					}

					if ($a > 0 && $b !== false)
					{
						$tepl_pol['result'] = $a/$b;
						$tepl_pol['result2'] = $a/$b/100;
						$tepl_pol['result3'] = $a/$b/70;
					} ?>
				<? } ?>
				<div class="row">
				  <div class="col-sm-8">
				    <div class="content-block">
				      <div class="input-group">
					<div class="input-group__item h5">Введите площадь полов, подлежащих обогреву:</div>
					<div class="input-group__item input-group__item--padd">
					    <input class="input-text input-text--fixwidth" required="" name="tepl_pol[area]" type="text" value='<?=$tepl_pol['area']?>'>
					</div>
					<div class="input-group__item h5">м<sup>2</sup></div>
				      </div>
				    </div>
                    <div class="content-block">
                      <div class="form__title text-left">Выберите требуемую мощность теплого пола:</div>
                      <div class="check-list check-list--expand">
                        <div class="check-list__item">
                          <label class="radio">
                            <input class="radio__input" required="" type="radio" value='1' name="tepl_pol[mosh]"<? if ($tepl_pol['mosh'] == 1) { ?> checked=""<? } ?>>
                            <span class="radio__label">высокая мощность</span>
                          </label>
                        </div>
                        <div class="check-list__item">
                          <label class="radio">
                            <input class="radio__input" required="" type="radio" value='2' name="tepl_pol[mosh]"<? if ($tepl_pol['mosh'] == 2) { ?> checked=""<? } ?>>
                            <span class="radio__label">стандартная мощность</span>
                          </label>
                        </div>
                        <div class="check-list__item">
                          <label class="radio">
                            <input class="radio__input" required="" type="radio" value='3' name="tepl_pol[mosh]"<? if ($tepl_pol['mosh'] == 3) { ?> checked=""<? } ?>>
                            <span class="radio__label">малая мощность</span>
                          </label>
                        </div>
                      </div>
                    </div>
	<div class="content-block">
	  <button class="btn btn--inverse" type="submit">
	    <span>Рассчитать</span>
	  </button>
	</div>
					</div>
					<div class="col-sm-4 hidden-xs-down text-center">
					  <img src="<?= SITE_TEMPLATE_PATH ?>/static/images/calc.svg?v=0aca4775" width="85" height="96" alt>
					</div>
				      </div>
				<? 
				if (!empty($tepl_pol['result']) && !empty($tepl_pol['result2']) && !empty($tepl_pol['result3'])) 
				{
					?>
					<div class="content-block">
						<div class="calc-result">Для данной системы необходимо: <?=ceil($tepl_pol['result'])?> метров трубы (диаметром 16 мм)</div>
					</div>
					<div class="content-block">
						<div class="calc-result">Минимальное количество петель теплого пола составляет: <?=ceil($tepl_pol['result2'])?> шт.</div>
					</div>
					<div class="content-block">
						<div class="calc-result">Оптимальное количество петель теплого пола составляет: <?=ceil($tepl_pol['result3'])?> шт.</div>
					</div>
					<div class="content-block">
						<a class="btn btn--fluid" href="/catalog/calcs/teplykh_polov/filter/teplyy_pol-is-00266a59-e280-11e6-a625-c46e1f000a92/diametr_naruzhnyy_mm-to-16/apply/">
							Показать все трубы <span class="hidden-xs-down">для теплых полов с диаметром 16 мм в каталоге товаров</span>
						</a>
					</div>
					<div class="content-block">
						<a class="btn btn--fluid" href="/catalog/calcs/teplykh_polov2/filter/kolichestvo_vykhodov_sht-from-<?=ceil($tepl_pol['result2'])?>/apply/">
							Показать все <span class="hidden-xs-down">распределительные коллекторы с количеством выходов, равным <?=ceil($tepl_pol['result2'])?> шт.</span>
						</a>
					</div>
					<div class="content-block">
						<a class="btn btn--fluid" href="/catalog/calcs/teplykh_polov2/filter/kolichestvo_vykhodov_sht-from-<?=ceil($tepl_pol['result3'])?>/apply/">
							Показать все <span class="hidden-xs-down">распределительные коллекторы с количеством выходов, равным <?=ceil($tepl_pol['result3'])?> шт.</span>
						</a>
					</div>
				<? } ?>
			</form>