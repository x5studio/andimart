<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="content-block content-block--sm-top island">
    <h2 class="section-title title-line js-title-line" id="portfolio_block">Фото и видео работ</h2>
    
<? //deb($arResult['SECTIONS'], false); 
foreach ($arResult['SECTIONS'] as $arSection) { ?>
	<div class="profile-portfoilo">
		<div class="row row--vcentr b-list">
			<div class="col-12 col-sm-10 b-list__item">
			    <div class="profile-portfoilo__title"><?=$arSection['NAME']?></div>
			</div>
			<div class="col-12 col-sm-2 b-list__item">
			    <button class="btn btn--fluid btn--sm js-portfolio-add" 
				    onclick="$('.portfolio-section_inp').val(<?=$arSection['ID']?>)"
				    data-category="category" data-mfp-src="#popup-portfolio-add" type="button">
				<span>Добавить</span>
			    </button>
			</div>
		</div>
		
		<? if (!empty($arSection['ITEMS'])) { ?>
			<div class="row">
			    <div class="col-12 col-sm-10">
				<ul class="profile-portfoilo__list js-popup-gallery">
					<? foreach ($arSection['ITEMS'] as $arItem) { ?>
						<? if (!empty($arItem['PROPERTIES']['VIDEO']['VALUE'])) { 
							
							$videoCode = '';
							if (preg_match('/v=([^\&]+)/', $arItem['PROPERTIES']['VIDEO']['VALUE'], $out))
							{
								$videoCode = $out[1];
							}
							elseif (preg_match('/youtu\.be\/([^\&]+)/', $arItem['PROPERTIES']['VIDEO']['VALUE'], $out))
							{
								$videoCode = $out[1];
							}
							if (!empty($videoCode)) {
							?>
								<li class="profile-portfoilo__item portfolio_i_<?=$arItem['ID']?>">
								    <a class="profile-portfoilo__link profile-portfoilo__link--play js-popup-gallery__video" href="<?=$arItem['PROPERTIES']['VIDEO']['VALUE']?>">
										<img src="//img.youtube.com/vi/<?=$videoCode?>/sddefault.jpg" alt>
								    </a>
								    <button class="profile-portfoilo__rm" type="button" onclick="removePortfolioWork(<?=$arItem['ID']?>);"></button>
								</li>
							<? } else { ?>							
								<li class="profile-portfoilo__item portfolio_i_<?=$arItem['ID']?>">
								    <a class="profile-portfoilo__link profile-portfoilo__link--play" href="javascript:;">
										<img src="" width="101" height="72"  alt>
								    </a>
								    <button class="profile-portfoilo__rm" type="button" onclick="removePortfolioWork(<?=$arItem['ID']?>);"></button>
								</li>
							<? } ?>
						<? } elseif (!empty($arItem['PROPERTIES']['FILE']['VALUE'])) { 
							
							$file = CFile::GetFileArray($arItem['PROPERTIES']['FILE']['VALUE']);
//							deb($file, false);
							
							$ext = explode('.', $file);
							$ext = toLower($ext[count($ext)-1]);
							
							if (in_array($ext, array('jpg', 'jpeg')) || 'image/jpg' == $file['CONTENT_TYPE'] || 'image/jpeg' == $file['CONTENT_TYPE']) {								
							?>
							<li class="profile-portfoilo__item portfolio_i_<?=$arItem['ID']?>">
							    <a class="profile-portfoilo__link" href="<?=$file['SRC'] ?>">
									<img src="<?=$file['SRC']?>"  alt>
							    </a>
							    <button class="profile-portfoilo__rm" type="button" onclick="removePortfolioWork(<?=$arItem['ID']?>);"></button>
							</li>
							<? } else { ?>					
								<li class="profile-portfoilo__item portfolio_i_<?=$arItem['ID']?>">
								    <a class="profile-portfoilo__link" href="javascript:;">
										<img src="" width="101" height="72"  alt>
								    </a>
								    <button class="profile-portfoilo__rm" type="button" onclick="removePortfolioWork(<?=$arItem['ID']?>);"></button>
								</li>
							<? } ?>
						<? } ?>
					<? } ?>
				</ul>
			    </div>
			</div>
		<? } ?>
	</div>
<? } ?>
	
    <!-- На все категории - две формы добавления (отдельно фото, отдельно видео). У кнопки открытия поп-апа в data-атрибуте можно указать категорию, и менять значение скрытого инпута в формах при открытии поп-апа. Заготовка в src/scripts/profile.js-->
    <div class="popup-holder">
	<div id="popup-portfolio-add" class="popup popup--sm">
	    <div class="popup__title title-line title-line--accent is-animated">Добавить</div>
	    <div class="popup__text">
		<div class="radiotabs js-radiotabs">
                    <div class="radiotabs__btns">
			<div class="radiotabs__row">
			    <a class="radiotabs__btn js-radiotabs__btn" href="#portfolio-add-photo">Фото</a>
			</div>
			<div class="radiotabs__row">
			    <a class="radiotabs__btn js-radiotabs__btn" href="#portfolio-add-video">Видео</a>
			</div>
                    </div>
		    <?$APPLICATION->IncludeComponent(
			"onlycode:forms.add", 
			"portfolio-photo", 
			array(
				"IBLOCK_TYPE" => "content",
				"IBLOCK_ID" => "30",
				"PROPERTY_CODE" => array(
					"USER",			    
					"SECTION",
					"FILE",
				),
				"PROPERTY_CODE_IS_REQUIRED" => array(
					"USER",			    
					"SECTION",
					"FILE",
				),
				"CODE" => "portfolio-photo",
				"RETURN_TYPE" => "html",
				"AJAX_SEND" => "N",
				"PROPERTY_DEFAULT_USER" => $USER->GetID()
			),
			false,
			array('HIDE_ICONS' => 'Y')
		);?>
		<?$APPLICATION->IncludeComponent(
			"onlycode:forms.add", 
			"portfolio-video", 
			array(
				"IBLOCK_TYPE" => "content",
				"IBLOCK_ID" => "30",
				"PROPERTY_CODE" => array(
					"USER",			    
					"SECTION",
					"VIDEO",
				),
				"PROPERTY_CODE_IS_REQUIRED" => array(
					"USER",			    
					"SECTION",
					"VIDEO",
				),
				"CODE" => "portfolio-video",
				"RETURN_TYPE" => "json",
				"AJAX_SEND" => "Y",
				"PROPERTY_DEFAULT_USER" => $USER->GetID()
			),
			false,
			array('HIDE_ICONS' => 'Y')
		);?>
		</div>
	    </div>
	</div>
    </div>
</div>



<script>
//	$(document).ready(function(){
//		$("#applicationform-step3-id").click(function()
//		{
//			$("#applicationform-id").find('input.custom-form-field').val("");
//			$("#applicationform-id").find('select.custom-form-field').val("")
//			$("#applicationform-id").find('select.custom-form-field').change();   
//		});
//		$("#<?= $arParams['CODE'] ?>form-id").submit(function()
//		{
//			var url = $(this).attr('action');
//			var data = $(this).serialize();
//			
//			$.post(url, data, function(output)
//			{
//				if (output.RESULT === 'error')
//				{
//					$('.custom-form-field').removeClass('error');
//					$('.custom-form-select').removeClass('error');
//					
//					for (var i in output.ERRORS)
//					{
//						if ($("select.<?= $arParams['CODE'] ?>-"+output.ERRORS[i]+"-form-field").parents('.custom-form-select').length > 0)
//						{
//							$("select.<?= $arParams['CODE'] ?>-"+output.ERRORS[i]+"-form-field").parents('.custom-form-select').addClass('error');
//						}
//						else
//						{
//							$(".<?= $arParams['CODE'] ?>-"+output.ERRORS[i]+"-form-field").addClass('error');
//						}
//					}
//				}
//				else
//				{
//					$('.custom-form-field').removeClass('error');
//					$('.custom-form-select').removeClass('error');
//					
//					$('.filedapplication-form-step3').click();
//				}
//			}, 'json');
//			
//			return false;
//		});
//	});
</script>
<?
/*
  <form id="<?=$arParams['CODE']?>form-id" name="<?=$arParams['CODE']?>" action="<?=POST_FORM_ACTION_URI?>" method="post">
  <?=bitrix_sessid_post()?>
  <div>
  <? foreach ($arResult['FIELDS'] as $field) { ?>
  <? if (!in_array($field['CODE'], array('AGREE', 'SUBSCRIPTION'))) { ?>
  <? if ($field['PROPERTY_TYPE'] == 'L') { ?>
  <select name="<?=$arParams['CODE']?>[<?=$field['CODE']?>]" class="custom-form custom-form-field <?=$arParams['CODE']?>-<?=$field['CODE']?>-form-field" data-placeholder="<?=$field['NAME']?><? if ($field['IS_REQUIRED'] == 'Y') { ?>*<? } ?>">
  <option value=""></option>
  <? foreach ($field['VARIANTS'] as $variant) { ?>
  <option value="<?=$variant['ID']?>"><?=$variant['VALUE']?></option>
  <? } ?>
  </select>
  <? } else { ?>
  <input name="<?=$arParams['CODE']?>[<?=$field['CODE']?>]" placeholder="<?=$field['NAME']?><? if ($field['IS_REQUIRED'] === 'Y') { ?>*<? } ?>" type="text" name="" class="custom-form custom-form-field <?=$arParams['CODE']?>-<?=$field['CODE']?>-form-field"/>
  <? } ?>
  <? } ?>
  <? } ?>
  </div>
  <div class="action-form-step2">
  <input placeholder="Получить инвестиции" type="submit" name="" class="custom-form standart-speed" value="Получить инвестиции" style="float: right; margin-right: 0px;"/>
  <p>
  <? $variants = array_values($arResult['FIELDS']['AGREE']['VARIANTS']) ?>
  <input checked="checked" type="checkbox" name="<?=$arParams['CODE']?>[AGREE]" value="<?=$variants[0]['ID']?>" class="custom-form" id="iagreetotheproccess"/><label class="<?=$arParams['CODE']?>-AGREE-form-field custom-form-field" for="iagreetotheproccess">Я согласен на обработку персональных данных (<a href="/info.php" target="_blank">подробнее</a>)</label>
  </p>
  <p>
  <? $variants = array_values($arResult['FIELDS']['SUBSCRIPTION']['VARIANTS']) ?>
  <input checked="checked" type="checkbox" name="<?=$arParams['CODE']?>[SUBSCRIPTION]" value="<?=$variants[0]['ID']?>" class="custom-form" id="subscribeme"/><label class="<?=$arParams['CODE']?>-SUBSCRIPTION-form-field custom-form-field" for="subscribeme">подписаться на рассылку</label>
  </p>
  </div>
  </form>

 */?>