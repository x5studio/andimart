<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>

		
		<?
	
$arResult["REVIEWS"]["COUNT"] = 0;
$arResult["REVIEWS"]["SIGN"] = 0;
$arSelect = Array("ID", "IBLOCK_ID", "NAME","XML_ID","PREVIEW_PICTURE","DETAIL_PAGE_URL","PROPERTY_SIGN");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
$arFilter = Array("PROPERTY_US"=>$arParams["USER_ID"],"IBLOCK_ID"=>5);
$brands = array();
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->GetNextElement()){ 
	$arFields = $ob->GetFields();  
	//p($arFields);
	$arResult["REVIEWS"]["COUNT"]++;
	$arResult["REVIEWS"]["SIGN"]+=intval($arFields["PROPERTY_SIGN_VALUE"]);
}
if($arResult["REVIEWS"]["COUNT"]){
	$arResult["REVIEWS"]["SIGN_REAL"] = ceil($arResult["REVIEWS"]["SIGN"]/$arResult["REVIEWS"]["COUNT"]);
}
?>
<div class="content-block row">
  <div class="col-12 col-md-10 offset-md-1">
<div class="list-header">
      <div class="row row--vcentr b-list">
        <div class="col-12 col-sm-6 b-list__item">
          <div class="rating-widget">
            <div class="rating-widget__stars">
              <div class="stars stars--xl" data-rating="<?=$arResult["REVIEWS"]["SIGN_REAL"]?>">
                <div class="stars__list"><i class="stars__star"></i><i class="stars__star"></i><i class="stars__star"></i><i class="stars__star"></i><i class="stars__star"></i></div>
              </div>
            </div>
            <div class="rating-widget__label">
              <div class="rating-widget__val"><?=$arResult["REVIEWS"]["SIGN_REAL"]?></div>общая оценка</div>
          </div>
        </div>
        <div class="col-12 col-sm-6 text-sm-right b-list__item">
          <a class="form-anchor js-scroll-to" data-scroll-offset="-150" href="#review-form">Оставить отзыв</a>
        </div>
      </div>
    </div>
<?
	
		?>
		<ul class="reviews">



<?foreach($arResult["ITEMS"] as $arItem):
?>
	<?
	$arr = explode(" ",$arItem["DATE_ACTIVE_FROM"]);
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

	<li class="reviews__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<div class="reviews__head">
			<div class="reviews__rating">
				<div class="stars stars--sm" data-rating="<?=$arItem["PROPERTIES"]["SIGN"]["VALUE"]?>">
					<div class="stars__list">
						<i class="stars__star"></i>
						<i class="stars__star"></i>
						<i class="stars__star"></i>
						<i class="stars__star"></i>
						<i class="stars__star"></i>
					</div>
				</div>
			</div>
			<div class="reviews__name"><?=$arr[0]?> <?=$arItem["NAME"]?> <? if($arItem["PROPERTIES"]["CITY"]["VALUE"]!=""){ ?>(<?=$arItem["PROPERTIES"]["CITY"]["VALUE"]?>) <?}  ?></div>
		</div>
		<div class="reviews__text"><?=$arItem["PREVIEW_TEXT"]?></div>
	</li>
<?endforeach;?>
		</ul>
		

		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
			<?=$arResult["NAV_STRING"]?>
		<?endif;?>

	</div>
</div>

