<?php

$arComponentParameters = array(
    "GROUPS" => array(
    ),
    "PARAMETERS" => array(
        "PVZ_CITY_ID" => array(
            "PARENT" => "BASE",
            "NAME" => "CITY ID",
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ),
    ),
);