<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

?>
<h2 class="section-title title-line js-title-line title-line--accent">Узнайте о нас больше. Видео.</h2>
<div class="content-block content-block--pull-top content-block--push-bot">
	<div class="portfolio">
		<ul class="portfolio__gallery js-portfolio-carousel js-popup-gallery">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	$href = $arItem["DETAIL_PICTURE"]["SRC"];
	$class="";
	if($arItem["PROPERTIES"]["VIDEO"]["VALUE"]){
		$href =$arItem["PROPERTIES"]["VIDEO"]["VALUE"];
		$class = "portfolio__link--play js-popup-gallery__video";
	}
	?>
	<li class=" portfolio__item" >
		<a class="<?=$class?> portfolio__link" href="<?=$href?>">
			<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="249" height="178" alt>
		</a>
	</li>
<?endforeach;?>
		</ul>
	</div>
</div>
