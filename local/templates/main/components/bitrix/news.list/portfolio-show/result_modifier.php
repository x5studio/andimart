<?php
	$arResult['SECTIONS'] = array();
	
	if (!empty($arResult['ITEMS']))
	{
		foreach ($arResult['ITEMS'] as $karItem => $arItem)
		{
			if (!empty($arItem['PROPERTIES']['VIDEO']['VALUE'])) 
			{
				$videoCode = '';
				if (preg_match('/v=([^\&]+)/', $arItem['PROPERTIES']['VIDEO']['VALUE'], $out))
				{
					$videoCode = $out[1];
				}
				elseif (preg_match('/youtu\.be\/([^\&]+)/', $arItem['PROPERTIES']['VIDEO']['VALUE'], $out))
				{
					$videoCode = $out[1];
				}
				if (empty($videoCode)) 
				{
					unset($arResult['ITEMS'][$karItem]);
				}
			} 
			elseif (!empty($arItem['PROPERTIES']['FILE']['VALUE'])) 
			{
				$file = CFile::GetFileArray($arItem['PROPERTIES']['FILE']['VALUE']);

				$ext = explode('.', $file);
				$ext = toLower($ext[count($ext)-1]);

				if (!(in_array($ext, array('jpg', 'jpeg')) || 'image/jpg' == $file['CONTENT_TYPE'] || 'image/jpeg' == $file['CONTENT_TYPE'])) 
				{
					unset($arResult['ITEMS'][$karItem]);
				}
			}
			else
			{
				unset($arResult['ITEMS'][$karItem]);
			}
		}
	}
	if (!empty($arResult['ITEMS']))
	{
		$arFilter = Array("IBLOCK_ID"=>const_IBLOCK_ID_catalog, 'ACTIVE' => 'Y', 'DEPTH_LEVEL' => 1);
		$res = CIBlockSection::GetList(Array(), $arFilter);
		while($arFields = $res->Fetch())
		{
			$arFields['ITEMS'] = array();
			$arResult['SECTIONS'][$arFields['ID']] = $arFields;
		}

		foreach ($arResult['ITEMS'] as $arItem)
		{
			$arResult['SECTIONS'][$arItem['PROPERTIES']['SECTION']['VALUE']]['ITEMS'][$arItem['ID']] = $arItem;
		}
	}
	
	
		
?>