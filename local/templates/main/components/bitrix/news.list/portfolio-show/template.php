<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult['ITEMS'])) { ?>
<div class="content-block island">
    <h2 class="section-title title-line js-title-line">Фото и видео работ</h2>
    <? foreach ($arResult['SECTIONS'] as $key=>$arSection) { ?>

	<? if (!empty($arSection['ITEMS'])) { ?>

		<div class="portfolio">
		    <div class="portfolio__title"><?=$arSection['NAME']?></div>
		    <div class="portfolio__gallery js-portfolio-carousel js-popup-gallery" data-slides="2">
			<? foreach ($arSection['ITEMS'] as $arItem) { ?>
				<? if (!empty($arItem['PROPERTIES']['VIDEO']['VALUE'])) {

					$videoCode = '';
					if (preg_match('/v=([^\&]+)/', $arItem['PROPERTIES']['VIDEO']['VALUE'], $out))
					{
						$videoCode = $out[1];
					}
					elseif (preg_match('/youtu\.be\/([^\&]+)/', $arItem['PROPERTIES']['VIDEO']['VALUE'], $out))
					{
						$videoCode = $out[1];
					}
					if (!empty($videoCode)) {
					?>
						<div class="portfolio__item">
							<a class="portfolio__link--play js-popup-gallery__video portfolio__link" href="<?=$arItem['PROPERTIES']['VIDEO']['VALUE']?>">
							    <img src="//img.youtube.com/vi/<?=$videoCode?>/sddefault.jpg">
							</a>
						</div>
					<? } else { ?>
					<? } ?>
				<? } elseif (!empty($arItem['PROPERTIES']['FILE']['VALUE'])) {

					$file = CFile::GetFileArray($arItem['PROPERTIES']['FILE']['VALUE']);
//							deb($file, false);

					$ext = explode('.', $file);
					$ext = toLower($ext[count($ext)-1]);

					if (in_array($ext, array('jpg', 'jpeg')) || 'image/jpg' == $file['CONTENT_TYPE'] || 'image/jpeg' == $file['CONTENT_TYPE']) {
					?>
					<div class="portfolio__item">
					    <a class="portfolio__link" href="<?=$file['SRC']?>" >
						<img src="<?=$file['SRC']?>">
					    </a>
					</div>
					<? } ?>
				<? } ?>
			<? } ?>
		    </div>
		</div>
	<? } ?>
    <? } ?>
</div>
<? } ?>

<?/*
<div class="content-block content-block--sm-top island">
    <h2 class="section-title title-line js-title-line" id="portfolio_block">Фото и видео работ</h2>

<? //deb($arResult['SECTIONS'], false);
foreach ($arResult['SECTIONS'] as $arSection) { ?>
	<div class="profile-portfoilo">
		<div class="row row--vcentr b-list">
			<div class="col-12 col-sm-10 b-list__item">
			    <div class="profile-portfoilo__title"><?=$arSection['NAME']?></div>
			</div>
<!--			<div class="col-12 col-sm-2 b-list__item">
			    <button class="btn btn--fluid btn--sm js-portfolio-add"
				    onclick="$('.portfolio-section_inp').val(<?=$arSection['ID']?>)"
				    data-category="category" data-mfp-src="#popup-portfolio-add" type="button">
				<span>Добавить</span>
			    </button>
			</div>-->
		</div>

		<? if (!empty($arSection['ITEMS'])) { ?>
			<div class="row">
			    <div class="col-12 col-sm-10">
				<ul class="profile-portfoilo__list js-popup-gallery">
					<? foreach ($arSection['ITEMS'] as $arItem) { ?>
						<? if (!empty($arItem['PROPERTIES']['VIDEO']['VALUE'])) {

							$videoCode = '';
							if (preg_match('/v=([^\&]+)/', $arItem['PROPERTIES']['VIDEO']['VALUE'], $out))
							{
								$videoCode = $out[1];
							}
							elseif (preg_match('/youtu\.be\/([^\&]+)/', $arItem['PROPERTIES']['VIDEO']['VALUE'], $out))
							{
								$videoCode = $out[1];
							}
							if (!empty($videoCode)) {
							?>
								<li class="profile-portfoilo__item portfolio_i_<?=$arItem['ID']?>">
								    <a class="profile-portfoilo__link profile-portfoilo__link--play js-popup-gallery__video" href="<?=$arItem['PROPERTIES']['VIDEO']['VALUE']?>">
										<img src="//img.youtube.com/vi/<?=$videoCode?>/sddefault.jpg" alt>
								    </a>
								    <button class="profile-portfoilo__rm" type="button" onclick="removePortfolioWork(<?=$arItem['ID']?>);"></button>
								</li>
							<? } else { ?>
								<li class="profile-portfoilo__item portfolio_i_<?=$arItem['ID']?>">
								    <a class="profile-portfoilo__link profile-portfoilo__link--play" href="javascript:;">
										<img src="" width="101" height="72"  alt>
								    </a>
								    <button class="profile-portfoilo__rm" type="button" onclick="removePortfolioWork(<?=$arItem['ID']?>);"></button>
								</li>
							<? } ?>
						<? } elseif (!empty($arItem['PROPERTIES']['FILE']['VALUE'])) {

							$file = CFile::GetFileArray($arItem['PROPERTIES']['FILE']['VALUE']);
//							deb($file, false);

							$ext = explode('.', $file);
							$ext = toLower($ext[count($ext)-1]);

							if (in_array($ext, array('jpg', 'jpeg')) || 'image/jpg' == $file['CONTENT_TYPE'] || 'image/jpeg' == $file['CONTENT_TYPE']) {
							?>
							<li class="profile-portfoilo__item portfolio_i_<?=$arItem['ID']?>">
							    <a class="profile-portfoilo__link" href="<?=$file['SRC'] ?>">
									<img src="<?=$file['SRC']?>"  alt>
							    </a>
							    <button class="profile-portfoilo__rm" type="button" onclick="removePortfolioWork(<?=$arItem['ID']?>);"></button>
							</li>
							<? } else { ?>
								<li class="profile-portfoilo__item portfolio_i_<?=$arItem['ID']?>">
								    <a class="profile-portfoilo__link" href="javascript:;">
										<img src="" width="101" height="72"  alt>
								    </a>
								    <button class="profile-portfoilo__rm" type="button" onclick="removePortfolioWork(<?=$arItem['ID']?>);"></button>
								</li>
							<? } ?>
						<? } ?>
					<? } ?>
				</ul>
			    </div>
			</div>
		<? } ?>
	</div>
<? } ?>

    <!-- На все категории - две формы добавления (отдельно фото, отдельно видео). У кнопки открытия поп-апа в data-атрибуте можно указать категорию, и менять значение скрытого инпута в формах при открытии поп-апа. Заготовка в src/scripts/profile.js-->

</div>
