<? if (!empty($arResult['ITEMS'])) { 
	
	$cnt = CIBlockElement::GetList(
    array(),
    array('IBLOCK_ID' => 26),
    array(),
    false,
    array('ID', 'NAME')
); 


	
	?>
	<div class="content-block island">
		<div class="brands-carousel js-carousel js-carousel--brands">
		    <div class="row row--vcentr">
			<div class="col-12 col-sm-3">
			    <div class="row row--vcentr">
				<div class="col-6 col-sm-12">
				    <h3 class="brands-carousel__title title-line title-line--accent js-title-line">Бренды</h3>
				</div>
				<div class="col-6 col-sm-12">
				    <div class="brands-carousel__more">
					<a class="brands-carousel__more-link" href="/catalog/brands/">все бренды (<?=$cnt?>)</a>
				    </div>
				</div>
			    </div>
			</div>
			<div class="col-12 col-sm-9">
			    <div class="brands-carousel__carousel">
				<button class="brands-carousel__prev js-carousel__prev" type="button"></button>
				<div class="brands-carousel__items js-carousel__items">
				    <? foreach($arResult['ITEMS'] as $arItem) { 
					
					$img = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]['ID'], Array("height" => 75, "width" => 150), BX_RESIZE_IMAGE_PROPORTIONAL, false, false, false, 80);
					?>
					<div class="brands-carousel__item">
					    <a class="link-hidden" href="<?=$arItem['DETAIL_PAGE_URL']?>">
						<img width="80" src="<?=$img['src']?>" height="76" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>">
					    </a>
					</div>
				    <? } ?>
				</div>
				<button class="brands-carousel__next js-carousel__next" type="button"></button>
			    </div>
			</div>
		    </div>
		    <div class="carousel-nav hidden-sm-up">
			<button class="carousel-nav__prev js-carousel__prev" type="button"></button>
			<div class="carousel-nav__stat js-carousel__stat"></div>
			<button class="carousel-nav__next js-carousel__next" type="button"></button>
		    </div>
		</div>
	    </div>
<? }// deb($arResult, false)?>
