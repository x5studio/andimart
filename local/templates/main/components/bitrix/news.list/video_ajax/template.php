<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if (!empty($arResult["ITEMS"])) { ?>
	<div id="popupVid">
	<? foreach($arResult["ITEMS"] as $arItem) { 
		$video = '';
		$videoCode = '';
		if (!empty($arItem['PROPERTIES']['CML2_TRAITS']['VALUE']))
		{
			foreach ($arItem['PROPERTIES']['CML2_TRAITS']['VALUE'] as $trait_key => $trait_value)
			{
				if ($arItem['PROPERTIES']['CML2_TRAITS']['DESCRIPTION'][$trait_key] == 'url')
				{
					$video = $trait_value;
					break;
				}
			}
		}
		
		if (preg_match('/v=([^\&]+)/', $video, $out))
		{
			$videoCode = $out[1];
		}
		elseif (preg_match('/youtu\.be\/([^\&]+)/', $video, $out))
		{
			$videoCode = $out[1];
		}
		
		if (empty($video) || empty($videoCode)) continue;
		?>
		<div class="product-video">
			<?/*div class="product-video__title"><?=$arItem['NAME']?></div>
			<? if (!empty($arItem["DETAIL_TEXT"])) { ?>
				<div class="product-video__descr"><?=$arItem['~DETAIL_TEXT']?></div>
			<? } */?>
			
				
			<div class="product-video__container">
			    <a class="product-video__link js-youtube-init" href="<?=$video?>">
				<img src="//img.youtube.com/vi/<?=$videoCode?>/sddefault.jpg" alt>
			    </a>
			</div>
		</div>
	<? } ?>
	</div>
<? }?>