<?php
	$arResult['SECTIONS'] = array();
	
	$arFilter = Array("IBLOCK_ID"=>const_IBLOCK_ID_brands, 'ACTIVE' => 'Y');
	$res = CIBlockElement::GetList(Array('name' => 'asc'), $arFilter);
	while($arFields = $res->Fetch())
	{
		$arFields['ITEMS'] = array();
		$arResult['SECTIONS'][$arFields['ID']] = $arFields;
	}
	
	if (!empty($arResult['ITEMS']))
	{
		foreach ($arResult['ITEMS'] as $arItem)
		{
			if (!empty($arResult['SECTIONS'][$arItem['PROPERTIES']['BRAND']['VALUE']]))
			{
				$arResult['SECTIONS'][$arItem['PROPERTIES']['BRAND']['VALUE']]['ITEMS'][$arItem['ID']] = $arItem;
			}
		}
	}
		
?>