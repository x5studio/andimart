<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use Bitrix\Main\Page\Asset;

$asset = Asset::getInstance();

$context = \Bitrix\Main\Application::getInstance()->getContext();
$server = $context->getServer();
$curPage = $server->getRequestUri();
$arResult['PROTOCOL'] = CMain::IsHTTPS() ? "https://" : "http://";
$arResult['SERVER_NAME'] = $_SERVER['SERVER_NAME'];
$arResult['NEXT_NUM'] = $arResult['NavPageNomer'] + 1;
$arResult['PREV_NUM'] = $arResult['NavPageNomer'] - 1;
$arResult['NEXT_PAGE'] = $arResult['PROTOCOL'] . $arResult['SERVER_NAME'] . $arResult['sUrlPath'] .
    '?' . $strNavQueryString . 'PAGEN_' . $arResult["NavNum"] . '=' . $arResult['NEXT_NUM'];
if ($arResult['PREV_NUM'] > 1)
    $arResult['PREV_PAGE'] = $arResult['PROTOCOL'] . $arResult['SERVER_NAME'] . $arResult['sUrlPath'] .
        '?' . $strNavQueryString . 'PAGEN_' . $arResult["NavNum"] . '=' . $arResult['PREV_NUM'];
else
    $arResult['PREV_PAGE'] = $arResult['PROTOCOL'] . $arResult['SERVER_NAME'] . $arResult['sUrlPath'];

if (intval($arResult['NavPageCount']) > 1 && $arResult["NavNum"] == 1) {
    global $setNext;
    if ($setNext != true) {
        $setNext = true;
        if ($arResult['PREV_NUM'] >= 1) {
            $asset->addString('<link rel="prev" href="' . $arResult['PREV_PAGE'] . '" />');
        }

        if ($arResult['NEXT_NUM'] <= $arResult['NavPageCount']) {
            $asset->addString('<link rel="next" href="' . $arResult['NEXT_PAGE'] . '" />');
        }
    }
}


if (!$arResult["NavShowAlways"]) {
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}

$arResult["NavQueryString"] = str_replace('ajax_mode=y&amp;', '', $arResult["NavQueryString"]);
$arResult["NavQueryString"] = str_replace('ajax_mode=y', '', $arResult["NavQueryString"]);

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");

if ($arResult["NavPageCount"] > 1) {
    ?>
    <nav class="pagination">
        <ul class="pagination__list">
            <?
            $start = 1;
            //p($arResult);
            if ($arResult["NavPageNomer"] > 4) {
                $start = $arResult["NavPageNomer"] - 1;
            }
            $end = $arResult["NavPageNomer"] + 1;
            if ($start == 1)
                $end = 5;
            if ($end - $start < 5)
                $start = $end - 5;
            if ($start <= 0)
                $start = 1;
            if ($end > $arResult["NavPageCount"])
                $end = $arResult["NavPageCount"];
            //echo $start." " .$end;
            ?>

            <?
            if ($start > 1) {
                ?>
                <li class="pagination__item">
                    <a class="pagination__link" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1">
                        <span class="pagination__title">1</span>
                    </a>
                </li>
                <li class="pagination__item pagination__item--dots">
                    <span class="pagination__title">...</span>
                </li>
                <?
            }
            for ($i = $start; $i <= $end; $i++) {
                if ($i == $arResult["NavPageNomer"]) {
                    ?>
                    <li class="pagination__item pagination__item--current">
                        <span class="pagination__title"><?= $i ?></span>
                    </li>
                    <?
                } elseif ($i == 1) {
                    ?>

                    <li class="pagination__item">
                        <a class="pagination__link" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($i) ?>">
                            <span class="pagination__title"><?= $i ?></span>
                        </a>
                    </li>
                    <?
                } else {
                    ?>

                    <li class="pagination__item">
                        <a class="pagination__link"
                           href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($i) ?>">
                            <span class="pagination__title"><?= $i ?></span>
                        </a>
                    </li>
                    <?
                }
            }
            if ($arResult["NavPageCount"] > $end) {
                ?>
                <li class="pagination__item pagination__item--dots">
                    <span class="pagination__title">...</span>
                </li>
                <li class="pagination__item">
                    <a class="pagination__link"
                       href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageCount"]) ?>">
                        <span class="pagination__title"><?= $arResult["NavPageCount"] ?></span>
                    </a>
                </li>
                <!--<li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($i + 1) ?>">далее <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
-->
                <?
            }
            ?>

        </ul>
    </nav>
<? } ?>
<?
global $cnt;
$cnt++;
if ($cnt == 1) {
    $this->SetViewTarget('pagination_items_count2'); ?>
    <span class="count__val"><?= $arResult["NavRecordCount"] ?></span>
    <span class="count__label"><?= number_end($arResult["NavRecordCount"], array("товар", "товара", "товаров")) ?></span>
    <?
    $this->EndViewTarget();
}
?>