<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<?
unset($arParams["arUserField"]["USER_TYPE"]["FIELDS"]['']);
if(
	$arParams["arUserField"]["ENTITY_VALUE_ID"] <= 0
	&& $arParams["arUserField"]["SETTINGS"]["DEFAULT_VALUE"] > 0
)
{
	$arResult['VALUE'] = array($arParams["arUserField"]["SETTINGS"]["DEFAULT_VALUE"]);
}
else
{
	$arResult['VALUE'] = array_filter($arResult["VALUE"]);
}

if($arParams['arUserField']["SETTINGS"]["DISPLAY"] != "CHECKBOX")
{
	if($arParams["arUserField"]["MULTIPLE"] == "Y")
	{
			
			if (!empty($arParams['new_name']))
			{
				$arParams["arUserField"]["FIELD_NAME"] = $arParams['new_name'];
			}
		?>
		<select name="<?echo $arParams["arUserField"]["FIELD_NAME"]?>"  id="reg_worker_city" class="select" require >
		<?
		foreach ($arParams["arUserField"]["USER_TYPE"]["FIELDS"] as $key => $val)
		{
			$bSelected = in_array($key, $arResult["VALUE"]);
			?>
			<option value="<?echo $key?>" <?echo ($bSelected? "selected" : "")?> title="<?echo trim($val, " .")?>"><?echo $val?></option>
			<?
		}
		?>
		</select>
		<?
	}
	else
	{
			
			if (!empty($arParams['new_name']))
			{
				$arParams["arUserField"]["FIELD_NAME"] = $arParams['new_name'];
			}
		?>
		<select name="<?echo $arParams["arUserField"]["FIELD_NAME"]?>"  id="reg_worker_city" class="select" require  >
		<?
		$bWasSelect = false;
		foreach ($arParams["arUserField"]["USER_TYPE"]["FIELDS"] as $key => $val)
		{
			if($bWasSelect)
				$bSelected = false;
			else
				$bSelected = in_array($key, $arResult["VALUE"]);

			if($bSelected)
				$bWasSelect = true;
			?>
			<option value="<?echo $val?>" <?echo ($bSelected? "selected" : "")?> title="<?echo trim($val, " .")?>"><?echo $val?></option>
			<?
		}
		?>
		</select>
		<?
	}
}
else
{
	if($arParams["arUserField"]["MULTIPLE"] == "Y")
	{
		?><div class="check-list">
		<input type="hidden" value="" name="<?echo $arParams["arUserField"]["FIELD_NAME"]?>">
		<?
		foreach ($arParams["arUserField"]["USER_TYPE"]["FIELDS"] as $key => $val)
		{
			$id = $arParams["arUserField"]["FIELD_NAME"]."_".$key;

			$bSelected = in_array($key, $arResult["VALUE"]);
			?>
			<div class="check-list__item">
				<label class="checkbox">
					<input class="checkbox__input" type="checkbox" type="checkbox" required
						   value="<?echo $key?>"
						   name="<?echo $arParams["arUserField"]["FIELD_NAME"]?>" <?echo ($bSelected? "checked" : "")?>
						   
					>
					<span class="checkbox__label"><?echo $val?></span>
				</label>
			</div>

			<?
		}
		?>
		</div>
		<?
	}
	else
	{
		$bWasSelect = false;
		foreach ($arParams["arUserField"]["USER_TYPE"]["FIELDS"] as $key => $val)
		{
			$id = $arParams["arUserField"]["FIELD_NAME"]."_".$key;

			if($bWasSelect)
				$bSelected = false;
			else
				$bSelected = in_array($key, $arResult["VALUE"]);

			if($bSelected)
				$bWasSelect = true;
			?>2
			<input type="radio" value="<?echo $key?>" name="<?echo $arParams["arUserField"]["FIELD_NAME"]?>" <?echo ($bSelected? "checked" : "")?> id="<?echo $id?>"><label for="<?echo $id?>"><?echo $val?></label><br />
			<?
		}
	}
}
?>