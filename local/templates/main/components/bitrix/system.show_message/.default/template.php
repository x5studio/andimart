<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>

<?if(strpos($arParams['MESSAGE'], Loc::getMessage('CATALOG_COMPARE_LIST_EMPTY')) !== false):?>
    <div class="content-block island compare-result compare-list-empty">
        <div class="text-compare-list-is-empty">
            <?=Loc::getMessage('COMPARE_LIST_IS_EMPTY');?>
        </div>
        <div class="text-add-products-to-compare-list">
            <?=Loc::getMessage('ADD_PRODUCTS_TO_COMPARE_LIST_1');?>
            <a href="/catalog/">
                <?=Loc::getMessage('ADD_PRODUCTS_TO_COMPARE_LIST_2');?>
            </a>
        </div>
        <img class="img-product-snippet-dummy" src="<?=SITE_TEMPLATE_PATH?>/images/product_snippet_dummy.jpg">
    </div>
<?else:?>
    <p><font class="<?=$arParams["STYLE"]?>"><?=$arParams["MESSAGE"]?></font></p>
<?endif;?>

