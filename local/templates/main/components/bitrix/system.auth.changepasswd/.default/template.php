<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<? 
$error = $APPLICATION->arAuthResult; 
?>
<h2 class="form__title text-left">Восстановление пароля</h2>

<form class="js-form-validator" 
      novalidate method="post" action="<?=$arResult["AUTH_FORM"]?>" name="bform"
      data-is_ajax="y" 
      data-backurl_success="/auth/" 
      data-add_params="&change_pwd=<?=GetMessage("AUTH_CHANGE")?>">
	
	<? if (!empty($error) && $error['TYPE'] != 'ERROR') { ?>
		<script>
			document.location = '/auth/';
		</script>
	<? } ?>
	<?if (strlen($arResult["BACKURL"]) > 0): ?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
	<? endif ?>
	<input type="hidden" name="AUTH_FORM" value="Y">
	<input type="hidden" name="TYPE" value="CHANGE_PWD">
	
	<input type="hidden" name="USER_LOGIN" value="<?=$arResult["LAST_LOGIN"]?>">
	<input type="hidden" name="USER_CHECKWORD" value="<?=$arResult["USER_CHECKWORD"]?>">
	<? if (!empty($error) && $error['TYPE'] == 'ERROR') { ?>
		<div class="form-error content-block"><?=$error['MESSAGE']?></div>
	<? } ?>
	<div class="row row--vcentr form-row">
		<div class="col-12 col-sm-3">
			<label class="input-label" for="login_password">Пароль:</label>
		</div>
		<div class="col-12 col-sm-9">
			<input id="login_password" class="input-text" required type="password" name="USER_PASSWORD" maxlength="50" value="<?=$arResult["USER_PASSWORD"]?>" autocomplete="off">
		</div>
	</div>
	<div class="row row--vcentr form-row">
		<div class="col-12 col-sm-3">
			<label class="input-label" for="login_password_confirm">Подтверждение пароля:</label>
		</div>
		<div class="col-12 col-sm-9">
			<input id="login_password_confirm" class="input-text" required type="password" name="USER_CONFIRM_PASSWORD" maxlength="50" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" autocomplete="off">
		</div>
	</div>
	
	<div class="row row--vcentr form-row form-row--push-top b-list">
		<div class="col-12 col-sm-6 col-md-5 b-list__item">
			<button class="btn btn--fluid" type="submit" name="change_pwd" value="<?=GetMessage("AUTH_CHANGE")?>">
				<span>Изменить</span>
			</button>

		</div>
		<div class="col-12 col-sm-6 col-md-5 text-center b-list__item">
			<a class="text-small" href="/auth/">Авторизация</a>
		</div>
	</div>
</form>

<?/*div class="bx-auth">

<?
ShowMessage($arParams["~AUTH_RESULT"]);
?>
<form method="post" action="<?=$arResult["AUTH_FORM"]?>" name="bform">
	<?if (strlen($arResult["BACKURL"]) > 0): ?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
	<? endif ?>
	<input type="hidden" name="AUTH_FORM" value="Y">
	<input type="hidden" name="TYPE" value="CHANGE_PWD">
	<table class="data-table bx-changepass-table">
		<thead>
			<tr>
				<td colspan="2"><b><?=GetMessage("AUTH_CHANGE_PASSWORD")?></b></td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><span class="starrequired">*</span><?=GetMessage("AUTH_LOGIN")?></td>
				<td><input type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["LAST_LOGIN"]?>" class="bx-auth-input" /></td>
			</tr>
			<tr>
				<td><span class="starrequired">*</span><?=GetMessage("AUTH_CHECKWORD")?></td>
				<td><input type="text" name="USER_CHECKWORD" maxlength="50" value="<?=$arResult["USER_CHECKWORD"]?>" class="bx-auth-input" /></td>
			</tr>
			<tr>
				<td><span class="starrequired">*</span><?=GetMessage("AUTH_NEW_PASSWORD_REQ")?></td>
				<td><input type="password" name="USER_PASSWORD" maxlength="50" value="<?=$arResult["USER_PASSWORD"]?>" class="bx-auth-input" autocomplete="off" />
<?if($arResult["SECURE_AUTH"]):?>
				<span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
					<div class="bx-auth-secure-icon"></div>
				</span>
				<noscript>
				<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
				</noscript>
<script type="text/javascript">
document.getElementById('bx_auth_secure').style.display = 'inline-block';
</script>
<?endif?>
				</td>
			</tr>
			<tr>
				<td><span class="starrequired">*</span><?=GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?></td>
				<td><input type="password" name="USER_CONFIRM_PASSWORD" maxlength="50" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" class="bx-auth-input" autocomplete="off" /></td>
			</tr>
		<?if($arResult["USE_CAPTCHA"]):?>
			<tr>
				<td></td>
				<td>
					<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
					<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
				</td>
			</tr>
			<tr>
				<td><span class="starrequired">*</span><?echo GetMessage("system_auth_captcha")?></td>
				<td><input type="text" name="captcha_word" maxlength="50" value="" /></td>
			</tr>
		<?endif?>
		</tbody>
		<tfoot>
			<tr>
				<td></td>
				<td><input type="submit" name="change_pwd" value="<?=GetMessage("AUTH_CHANGE")?>" /></td>
			</tr>
		</tfoot>
	</table>

<p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>
<p><span class="starrequired">*</span><?=GetMessage("AUTH_REQ")?></p>
<p>
<a href="<?=$arResult["AUTH_AUTH_URL"]?>"><b><?=GetMessage("AUTH_AUTH")?></b></a>
</p>

</form>

<script type="text/javascript">
document.bform.USER_LOGIN.focus();
</script>
</div>