<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$isAjax = false;
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$isAjax = (
		(isset($_POST['ajax_action']) && $_POST['ajax_action'] == 'Y')
		|| (isset($_POST['compare_result_reload']) && $_POST['compare_result_reload'] == 'Y')
	);
}

$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME']
);

?><div class="bx_compare <? echo $templateData['TEMPLATE_CLASS']; ?>" id="bx_catalog_compare_block"><?
if ($isAjax)
{
	$APPLICATION->RestartBuffer();
}
?>
<!--<div class="bx_sort_container">
	<div class="sorttext"><?=GetMessage("CATALOG_SHOWN_CHARACTERISTICS")?>:</div>
	<a class="sortbutton<? echo (!$arResult["DIFFERENT"] ? ' current' : ''); ?>" href="<? echo $arResult['COMPARE_URL_TEMPLATE'].'DIFFERENT=N'; ?>" rel="nofollow"><?=GetMessage("CATALOG_ALL_CHARACTERISTICS")?></a>
	<a class="sortbutton<? echo ($arResult["DIFFERENT"] ? ' current' : ''); ?>" href="<? echo $arResult['COMPARE_URL_TEMPLATE'].'DIFFERENT=Y'; ?>" rel="nofollow"><?=GetMessage("CATALOG_ONLY_DIFFERENT")?></a>
</div>-->
<?
$cat_array = array();
if (!empty($arResult["SHOW_FIELDS"])){
	foreach($arResult["ITEMS"] as $arElement){
		$cat = $arElement["IBLOCK_SECTION_ID"];
		if(!empty($cat_array[$cat])){
			$cat_array[$cat] = $cat_array[$cat] + 1;
		}else{
			$cat_array[$cat] = 1;
		}
	}
	unset($arElement);
	if( isset($_GET['catid']) && !empty($_GET['catid']) && !empty($cat_array[$_GET['catid']]) ){
		$cat_compare = $_GET['catid'];
	}else{
		$cat_compare = $arResult["ITEMS"][0]["IBLOCK_SECTION_ID"];
	}
	echo "<div class='cat_container'>";
	foreach ($cat_array as $catid => $count) {
		if($count > 0){
			$res = CIBlockSection::GetByID($catid);
			if($arRes = $res->GetNext())
			{
				?><a href="/compare/?catid=<?=$catid?>" class="<? if( $catid == $cat_compare ) echo 'active'?>" ><?=$arRes['NAME']?> (<?=$count?>)</a> <?
			}
			if( $catid == $cat_compare ){
				$cat_compare_url = $arRes['SECTION_PAGE_URL'];
			}
		}
	}
	echo "</div>";

}

?>


<div class="table_compare">
	<div class="prod_cont_x">
	<div id="comp_prod_cont">
		<div class="container">
		<div class="owl-carousel owl-theme draggable owl_carus_p">
		<?
		if (!empty($arResult["SHOW_FIELDS"]))
		{

			foreach($arResult["ITEMS"] as $arElement)
			{
				if( $cat_compare == $arElement["IBLOCK_SECTION_ID"] ){?>
			<div class="item">
				<div class="catalog__item-in">
				<a onclick="CatalogCompareObj.delete('<?=CUtil::JSEscape($arElement['~DELETE_URL'])?>');" href="javascript:void(0)" class="del" data-id="<?=$arElement['ID']?>">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 125" enable-background="new 0 0 100 100" xml:space="preserve">
						<path d="M46,73.6V34c0-1.657-1.343-3-3-3s-3,1.343-3,3v39.6c0,1.657,1.343,3,3,3S46,75.256,46,73.6z M61.105,73.6V34  c0-1.657-1.343-3-3-3s-3,1.343-3,3v39.6c0,1.657,1.343,3,3,3S61.105,75.256,61.105,73.6z M73,31c-1.657,0-3,1.343-3,3v47  c0,1.234-1.767,3-3,3H33.209C32.197,84,31,82.374,31,81V34c0-1.657-1.343-3-3-3s-3,1.343-3,3v47c0,4.257,3.371,9,8.209,9H67  c4.542,0,9-4.458,9-9V34C76,32.343,74.657,31,73,31z M81,22H65v-9c0-1.657-1.343-3-3-3H38c-1.657,0-3,1.343-3,3v9H21.141  c-1.657,0-3,1.343-3,3s1.343,3,3,3H81c1.657,0,3-1.343,3-3S82.657,22,81,22z M59,22H41v-6h18V22z"/>
					</svg>
				</a>
		        <div class="catalog__img">
		            <a class="link-hidden" href="<?=$arElement["DETAIL_PAGE_URL"]?>">
		                <img src="<?=$arElement['DETAIL_PICTURE']["SRC"]?>" alt="<?=$arElement['DETAIL_PICTURE']['ALT']?>">
		            </a>
		        </div>
		        <div class="catalog__content">
		            <div class="catalog__content-top">
		                <div class="catalog__title">
		                    <a class="link-hidden" href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement['NAME']?></a>
		                </div>
                        <div class="catalog__social-rating">
                            <?include 'social_rating.php'?>
                        </div>
		                <div class="catalog__brand">
                            Бренд: <?=$arElement["PROPERTIES"]["BREND"]["VALUE"]?>

		                    <? if ($arElement["CATALOG_QUANTITY"]) {//CATALOG_QUANTITY?>
		                        <div class="catalog__avail">
		                        <span id="text-in-stock-product-id-<?= $arElement['ID'] ?>" class="text-success"
		                              >●</span> в наличии
		                        </div>
		                    <? } else { ?>
		                        <div class="catalog__avail">
		                        <span id="text-in-stock-product-id-<?= $arElement['ID'] ?>" class="text-warn"
		                              data-quantity-in-stock="0">●</span> нет наличии
		                        </div>
		                    <? } ?>
		                 </div>
		            </div>

		            <div class="catalog__content-bot">
		                <div class="product-price">
		                    <div class="product-price__current">
		                    <?php	if (isset($arElement['MIN_PRICE']) && is_array($arElement['MIN_PRICE']))
								{
									?><? echo $arElement['MIN_PRICE']['PRINT_DISCOUNT_VALUE']; ?><?
								}
								else
								{
									?>&nbsp;<?
								} ?>
		                    </div>
		                </div>
		                <!--<div class="catalog__buy">
		                    <div class="catalog__buy-count">
		                        <div class="input-group">
		                            <div class="input-group__item catalog__buy-input">

		                                <div class="product-card-wrapper">
		                                <span class="product-card-item-amount-btn-minus catalog_section_minus" data-entity="product-card-item-quantity-minus"><i class="fas fa-minus" style="font-size: 0.8em;"></i></span>
		                                    <input data-quntity="7" pattern="^[1-9]{1}[0-9]{3}" id="textbox-desired-quantity-product-id-7408" class="input-text input-text--sm text-center section_quantity_input" type="text" value="1">
		                                    <span class="product-card-item-amount-btn-plus catalog_section_plus" data-entity="product-card-item-quantity-plus"><i class="fas fa-plus" style="font-size: 0.8em;"></i></span>
		                                </div>

		                            </div>
		                            <div class="input-group__item catalog__buy-label">
		                                <div class="input-posfix">шт.</div>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="catalog__buy-btn">
		                        <button id="btn-buy-product-id-7408" class="btn btn--sm add_to_cart_section js-btn-to-basket" rel="/catalog/otoplenie/kotelnaya_komnata/dymokhody/?action=ADD2BASKET&amp;id=7408" type="button">
		                            <span>Купить</span>
		                        </button>
		                        <a href="/cart/" class="btn btn--inverse btn--sm btn-in-basket" style="display: none">
		                            <span>В корзине</span>
		                        </a>
		                    </div>
		                </div>-->
		            </div>
		        </div>
		    </div>
				</div>
			<?
			}
			}
			?>
			<div class="item add-new-item-button" style="border-right: 1px dashed #d8d8d8; ">
				<a class="catalog-transition-btn" href="<?=$cat_compare_url?>">
					<div class="icon-container"><span>+</span></div>
					<div class="text-container">Добавить еще товары из данной категории</div>
				</a>
			</div>
			<?
			unset($arElement);
		}
		?>
		</div>
		</div>
	</div>
	</div>
	<div class="data-table">
        <?
        foreach ($arResult['UNIQUE_IBLOCK'] as $iblockid){
            $property_params[$iblockid] = CIBlockSectionPropertyLink::GetArray($iblockid);
        }
        ?>
		<?
		if (!empty($arResult["SHOW_PROPERTIES"]))
		{
			foreach ($arResult["SHOW_PROPERTIES"] as $code => $arProperty)
			{
				$showRow = true;
				$arCompare = array();
				foreach($arResult["ITEMS"] as $arElement)
				{
					if( $cat_compare == $arElement["IBLOCK_SECTION_ID"] ){
						$arPropertyValue = $arElement["DISPLAY_PROPERTIES"][$code]["VALUE"];
						if (is_array($arPropertyValue))
						{
							sort($arPropertyValue);
							$arPropertyValue = implode(" / ", $arPropertyValue);
						}
						$arCompare[] = $arPropertyValue;
					}
				}
				unset($arElement);
				if ($arResult['DIFFERENT']) {
					$showRow = (count(array_unique($arCompare)) > 1);
				}else{
					$arCompare = array_diff($arCompare, array(''));
					$showRow = (count($arCompare) > 0);
				}

				if ($showRow)
				{
					?>
					<div class="param_title">
						<span>
							<span>?
								<div class="characteristic-popover">
									<?=$property_params[$arProperty['IBLOCK_ID']][$arProperty['ID']]['FILTER_HINT']?>
								</div>
							</span>
						</span>
						<?=$arProperty["NAME"]?>
						<div class="arrow"></div>
					</div>
					<div class="owl-carousel owl-theme draggable owl_carus_param">
						<?foreach($arResult["ITEMS"] as $arElement)
						{
							if( $cat_compare == $arElement["IBLOCK_SECTION_ID"] ){
							?>
							<div class="item">
								<?php
								if(!empty( $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]) ){
									if( is_array( $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"] ) ){
										echo implode("/ ", $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]);
									}else{
									 	echo $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"] ;
									}
								}else{
									echo "Нет данных";
								}
								?>
							</div>
						<?
						}
						}
						?><div class="item"></div><?
						unset($arElement);
						?>
					</div>
				<?
				}
			}
		} ?>

	</div>
</div>
<script type="text/javascript" src="<?php echo $this->GetFolder().'/owl.carousel.min.js'; ?>"></script>
<script type="text/javascript">
	$(document).ready(function () {
		var owl = $('.owl_carus_p');
		owl.owlCarousel({
		    loop:false,
		    margin:2,
		    responsiveClass:true,
			nav:true,
			dots:false,
			touchDrag:false,
			mouseDrag:false,
			pullDrag:false,
		    responsive:{
		        0:{
		            items:2,
		        },
		        767:{
		            items:3,
		        },
		        1000:{
		            items:5,
		        }
		    }
		});
		var owl_i = $('.owl_carus_param');
		owl_i.owlCarousel({
		    loop:false,
		    margin:2,
		    responsiveClass:true,
			nav:false,
			dots:false,
			touchDrag:false,
			mouseDrag:false,
			pullDrag:false,
		    responsive:{
		        0:{
		            items:2,
		        },
		        767:{
		            items:3,
		        },
		        1000:{
		            items:5,
		        }
		    }
		});
		$(".owl-next").click(function() {
	    	owl_i.trigger('next.owl.carousel');;
	  	});
	  	$(".owl-prev").click(function() {
	    	owl_i.trigger("prev.owl.carousel");
	  	});
	  	$('.param_title').click(function() {
	    	$(this).next().toggle();
	    	$(this).toggleClass("show");
	  	});
	});

	window.onscroll = function() {fix_prod_comp()};
	fix_prod_comp();
	function fix_prod_comp() {
	  var elem = document.getElementById("comp_prod_cont");
	  if(window.pageYOffset > 460){
	  	elem.classList.add("sticky_fix");
	  	elem.classList.add("sticky_up");
	  }else if (window.pageYOffset > 450 ) {
	    elem.classList.add("sticky_up");
	    elem.classList.remove("sticky_fix");
	  } else {
	    elem.classList.remove("sticky_fix");
	    elem.classList.remove("sticky_up");
	  }
	}
	<? if( isset($_GET['action']) && $_GET['action']=='DELETE_FROM_COMPARE_RESULT'){ ?>
		$(".skyCompareCounter").find(".nav__title").html(<?=count($arResult["ITEMS"])?>);
	<? } ?>
</script>
<?
if ($isAjax)
{
	die();
}
?>
<script type="text/javascript">
	var CatalogCompareObj = new BX.Iblock.Catalog.CompareClass("bx_catalog_compare_block", '<?=CUtil::JSEscape($arResult['~COMPARE_URL_TEMPLATE']); ?>');
</script>
</div>
