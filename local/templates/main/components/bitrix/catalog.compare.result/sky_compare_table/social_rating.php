<?

?>
<div class="stars"
     data-rating="<?=($arElement['AVERAGE_RATING'] ? $arElement['AVERAGE_RATING'] : 0)?>"
     itemtype="http://schema.org/AggregateRating" itemprop="aggregateRating"
     itemscope="itemscope"
>

    <meta itemprop="ratingValue" content="<?=($arElement['AVERAGE_RATING'] ? $arElement['AVERAGE_RATING'] : 0)?>">
    <meta itemprop="bestRating" content="5">
    <meta itemprop="ratingCount" content="<?=($arElement['REVIEW_COUNT'] ? $arElement['REVIEW_COUNT'] : 0)?>">
    <div class="stars__list">
        <i class="stars__star"></i>
        <i class="stars__star"></i>
        <i class="stars__star"></i>
        <i class="stars__star"></i>
        <i class="stars__star"></i>
    </div>
</div>


<div class="social-rating social-rating111111">
    <!--
    <div class="rating" data-rating="<?=($arElement['AVERAGE_RATING'] ? $arElement['AVERAGE_RATING'] : 0)?>">
        <div class="fill-rating"></div>
    </div>
    -->
    <a href="<?=$arElement["DETAIL_PAGE_URL"].'#reviews'?>"
       target="_blank"
       class="comments-ref" data-review-count="<?=($arElement['REVIEW_COUNT'] ? $arElement['REVIEW_COUNT'] : 0)?>"
    >
        <span class="comments-count"><?=($arElement['REVIEW_COUNT'] ? $arElement['REVIEW_COUNT'] : '')?></span>
    </a>
</div>


<style>
    .stars__list {
        margin-left: -17px;
        margin-top: -6px;
        transform: scale(0.7);
    }

    .social-rating,
    .stars
    {
        display: inline-block;
    }

    .social-rating {
        font-family: "PTSans","Helvetica","Arial",sans-serif;
        font-size: 13px;
        line-height: 1.4;
        color: #333;
        /*margin-left: 5px;*/
        /*margin-top: 1px;*/
        margin-left: -9px;
        margin-top: -5px;
    }

    .stars__star {
        margin-left: 2px;
    }

    .catalog-product .product-info .rating {
        margin-right: 0;
    }

    .rating {
        display: inline-block;
        width: 80px;
        margin: 0 12px 0 0;
    }

    .rating-single, .rating, .rating .fill-rating {
        height: 18px;
        background: url(<?=SITE_TEMPLATE_PATH?>/images/rating.png) repeat-x;
    }

    .rating[data-rating="1"] .fill-rating {
        width: 16px;
    }

    .rating[data-rating="2"] .fill-rating {
        width: 32px;
    }

    .rating[data-rating="3"] .fill-rating {
        width: 48px;
    }

    .rating[data-rating="4"] .fill-rating {
        width: 64px;
    }

    .rating[data-rating="5"] .fill-rating {
        width: 80px;
    }

    .rating .fill-rating {
        background: url(<?=SITE_TEMPLATE_PATH?>/images/rating-fill.png) repeat-x;
        width: 0;
    }

    .comments-ref {
        text-decoration: none;
        background: url(<?=SITE_TEMPLATE_PATH?>/images/comment-alt-regular.svg) no-repeat;
        background-size: 12px 12px;
        background-position: left center;
        padding-left: 16px;
    }

    .comments-ref[data-review-count="0"] {
        text-decoration: none;
        background: url(<?=SITE_TEMPLATE_PATH?>/images/comment-alt-regular-gray.svg) no-repeat;
        background-size: 12px 12px;
        background-position: left center;
        padding-left: 16px;
    }
</style>



