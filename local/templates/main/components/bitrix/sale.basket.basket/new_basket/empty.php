<style>
    .empty_basket{
        text-align: center;
    }
    .header_empty_basket h1{
        color: #ef6b00;
        font-weight: bold;
    }
    .text_empty_basket p{
        color: #ef6b00;
        font-weight: bold;
        font-size: 18px;
    }
</style>
<div class="empty_basket">
    <div class="header_empty_basket">
        <h1>Упс!</h1>
    </div>
    <div class="picture_empty_basket">
        <img src="/images/cart.png" alt="">
    </div>
    <div class="text_empty_basket">
        <p>Ваша корзина пуста</p>
    </div>
</div>