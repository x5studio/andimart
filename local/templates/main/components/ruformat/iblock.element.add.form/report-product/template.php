<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

?>
<div class="content-block content-block--report">
	<?php

	if (!empty($arResult["ERRORS"])):?>
		<?ShowError(implode("<br />", $arResult["ERRORS"]))?>
		<script>
			$(document).ready(function(){
				var destination = $("#ask-form").offset().top-130;
				jQuery("html:not(:animated),body:not(:animated)").animate({
					scrollTop: destination
				}, 800);
			});
		</script>
	<?endif;
	if (strlen($arResult["MESSAGE"]) > 0):?>
		<?ShowNote($arResult["MESSAGE"])?>
		<script>
			$(document).ready(function(){
				Popups.showMessage({
					title: "Запрос успешно отправлен.",
					text: "Спасибо. Ваш запрос отправлен на модерацию."
				});
			});
		</script>
	<?endif?>
<!-- js-ask-form-->
    <div class="report-form-content">
        <div class="popup">
        <form class="form " novalidate id="ask-form"  name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
            <?=bitrix_sessid_post()?>
            <?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>

            <input type="hidden" name="ITEM_ID" value="<?=$arParams['ITEM_ID']?>">
            <input type="hidden" name="PROPERTY[1130][]" value="<?=$arParams['ITEM_ID']?>">
            <!-- Почти готовый скрипт отправки формы src/scripts/app/forms/ask-form.js-->
            <div class="form__title" style="font-size: 20px "><b>Заказ товара</b></div>
            <div class="form-row form-error text-center js-form-error" style="display:none;"></div>
            <div class="row row--vcentr form-row">
                <div class="col-12 col-sm-2 col-md-3 text-sm-right">
                    <label class="input-label" for="ask_name">Имя:</label>
                </div>
                <div class="col-12 col-sm-8 col-md-6">
                    <input id="ask_name" class="input-text" data-field-name="Имя" name="PROPERTY[NAME][0]" type="text" required>
                </div>
            </div>
            <div class="row row--vcentr form-row">
                <div class="col-12 col-sm-2 col-md-3 text-sm-right">
                    <label class="input-label" for="ask_city">Телефон:</label>
                </div>
                <div class="col-12 col-sm-8 col-md-6">
                    <input id="ask_phone" class="input-text" data-field-name="Телефон" type="text" required name="PROPERTY[1128][0]" maxlength="17">
                </div>
            </div>
            <div class="row row--vcentr form-row">
                <div class="col-12 col-sm-2 col-md-3 text-sm-right">
                    <label class="input-label" for="ask_email">Email:</label>
                </div>
                <div class="col-12 col-sm-8 col-md-6">
                    <input id="ask_email" class="input-text" data-field-name="Email" type="email" required name="PROPERTY[1129][0]">
                </div>
            </div>
            <div class="row form-row">
                <div class="col-12 col-sm-2 col-md-3 text-sm-right">
                    <label class="input-label" for="ask_question">Комментарий:</label>
                </div>
                <div class="col-12 col-sm-8 col-md-6">
                    <textarea id="ask_question" class="input-text input-text--textarea" data-field-name="Комментарий" required name="PROPERTY[PREVIEW_TEXT][0]"></textarea>
                </div>
            </div>
            <div class="row row--vcentr form-row">
                <div class="col-12 col-sm-2 col-md-3 text-sm-right">

                </div>
                <div class="col-12 col-sm-8 col-md-6" style="padding-right: 0;padding-left: 0; text-align: center;">
                    <span>Мы уточним возможность заказа, цену товара и сроки его доставки,<br> и свяжемся с вами как можно скорее!</span>
                </div>
            </div>

            <div class="form-row text-center">
                <button class="btn" type="submit" name="iblock_submit2" value="send">
                    <span>Отправить</span>
                </button>
            </div>
            <input type="hidden" value="send" name="iblock_submit">
        </form>
        </div>
    </div>
    <script >
        $('#ask_phone').mask('+7 (999) 999-9999', {placeholder: "+7 (___) ___-____"});
    </script>
</div>
