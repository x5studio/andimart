<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 *
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 *
 * @global CUser $USER
 * @global CMain $APPLICATION
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?>



<? if ($USER->IsAuthorized()): ?>

    <p><?
        echo GetMessage("MAIN_REGISTER_AUTH") ?></p>

<? else: ?>
    <?
    if (count($arResult["ERRORS"]) > 0):
        foreach ($arResult["ERRORS"] as $key => $error)
            if (intval($key) == 0 && $key !== 0)
                $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;" . GetMessage("REGISTER_FIELD_" . $key) . "&quot;", $error);

        ShowError(implode("<br />", $arResult["ERRORS"]));

    elseif ($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):
        ?>
        <p><?
            echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT") ?></p>
    <?endif ?>
    <form class="js-form-validator" novalidate method="post" action="<?= POST_FORM_ACTION_URI ?>#reg-form-worker"
          name="regform" enctype="multipart/form-data">
        <input type="hidden" name="reg_group" value="master">
        <input type="hidden" name="reg_hash" value="222">
        <?
        if ($arResult["BACKURL"] <> ''):
            ?>
            <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
        <?
        endif;
        ?>

        <!-- У этой и других форм добавлена валидация (класс js-form-validator). Это демо для верстки! Пример интеграции валидатора с формой можно посмотреть у формы подписки (src/scripts/app/forms/subscribe-form.js)-->
        <div class="form-row">
            <div class="input-advice">Все поля обязательны для заполнения</div>
        </div>
        <div class="row row--vcentr form-row">
            <div class="col-12 col-sm-3 col-md-2">
                <div class="input-label">Фото:</div>
            </div>
            <div class="col-12 col-sm-9 col-md-10">
                <div class="input-file input-group js-input-file">
                    <div class="input-group__item input-group__item--grow">
                        <div class="input-text js-input-file__name"></div>
                    </div>
                    <label class="input-group__item">
                        <input name="REGISTER_FILES_PERSONAL_PHOTO" class="input-file__input" type="file"
                               onchange="$(this).closest('.js-input-file').find('.js-input-file__name').text(this.files[0] ? this.files[0].name : '');">
                        <span class="btn btn--fluid btn--inverse input-file__btn">
                                  <span>Обзор</span>
                                </span>
                    </label>
                </div>
            </div>
        </div>
        <div class="row row--vcentr form-row">
            <div class="col-12 col-sm-3 col-md-2">
                <label class="input-label" for="reg_worker_name">Имя:</label>
            </div>
            <div class="col-12 col-sm-9 col-md-10">
                <input id="reg_worker_name" class="input-text" type="text" required name="REGISTER[NAME]"
                       value="<?= $arResult["VALUES"]["NAME"] ?>">
            </div>
        </div>
        <div class="row row--vcentr form-row">
            <div class="col-12 col-sm-3 col-md-2">
                <label class="input-label" for="reg_worker_city">Город:</label>
            </div>
            <div class="col-12 col-sm-9 col-md-10">
                <?
                foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):
                    if ($FIELD_NAME != "UF_CITY")
                        continue;

                    $arUserField["SETTINGS"]["DEFAULT_VALUE"] = $_SESSION["PEK_CURRENT_CITY_ID"];
                    ?>

                    <?
                    $APPLICATION->IncludeComponent(
                        "bitrix:system.field.edit",
                        $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                        array(//"bVarsFromForm" => $arResult["bVarsFromForm"],
                            "new_name"     => 'UF_CITY_NEW',
                            "arUserField"  => $arUserField,
                            "~arUserField" => $arUserField,
//									    'VALUE' => array($_SESSION["PEK_CURRENT_CITY_ID"]),
                            "form_name"    => "regform"), null, array("HIDE_ICONS" => "Y")); ?>
                <?endforeach; ?>

            </div>
        </div>
        <div class="row row--vcentr form-row">
            <div class="col-12 col-sm-3 col-md-2">
                <label class="input-label" for="reg_worker_tel">Телефон:</label>
            </div>
            <div class="col-12 col-sm-9 col-md-10">
                <input id="reg_worker_tel" class="input-text js-mask-phone" type="tel" required
                       name="REGISTER[PERSONAL_PHONE]" value="<?= $arResult["VALUES"]["PERSONAL_PHONE"] ?>">
            </div>
        </div>
        <div class="row row--vcentr form-row">
            <div class="col-12 col-sm-3 col-md-2">
                <label class="input-label" for="reg_worker_email">Email:</label>
            </div>
            <div class="col-12 col-sm-9 col-md-10">
                <input id="reg_worker_email" class="input-text" type="email" data-doubleemail required
                       name="REGISTER[EMAIL]" value="<?= $arResult["VALUES"]["EMAIL"] ?>">
            </div>
            <div class="col-12 col-sm-9 offset-sm-3 col-md-10 offset-md-2 input-advice doubleemail-error">Так мы будем
                на связи
            </div>
        </div>
        <div class="row row--vcentr form-row">
            <div class="col-12 col-sm-3 col-md-2">
                <label class="input-label" for="reg_worker_password">Пароль:</label>
            </div>
            <div class="col-12 col-sm-9 col-md-10">
                <input id="reg_worker_password" class="input-text" data-minlength="6" type="password" required
                       name="REGISTER[PASSWORD]">
            </div>
            <div class="col-12 col-sm-9 offset-sm-3 col-md-10 offset-md-2 input-advice ">Пароль должен быть не менее 6
                символов длиной.
            </div>
        </div>
        <div class="row form-row">
            <div class="col-12 col-sm-4">
                <div class="input-label input-label--top">Вид деятельности:</div>
            </div>
            <div class="col-12 col-sm-8">
                <?
                foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):
                    if ($FIELD_NAME != "UF_SPECIALS")
                        continue;
                    //deb($arUserField["USER_TYPE"]["USER_TYPE_ID"], false);
                    ?>

                    <?
                    $APPLICATION->IncludeComponent(
                        "bitrix:system.field.edit",
                        $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                        array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "regform"), null, array("HIDE_ICONS" => "Y")); ?>
                <?endforeach; ?>
                <? //deb($arUserField["USER_TYPE"]["USER_TYPE_ID"], false)?>
                <?/*foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):
						if($FIELD_NAME!="UF_VID")
							continue;

						?>

						<?$APPLICATION->IncludeComponent(
						"bitrix:system.field.edit",
						$arUserField["USER_TYPE"]["USER_TYPE_ID"],
						array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "regform"), null, array("HIDE_ICONS"=>"Y"));?>
					<?endforeach;*/ ?>


            </div>
        </div>
        <div class="row row--vcentr form-row">
            <div class="col-12 col-sm-3 col-md-2">
                <label class="input-label" for="reg_worker_password_repeat">Пароль ещё&nbsp;раз:</label>
            </div>
            <div class="col-12 col-sm-9 col-md-10">
                <input id="reg_worker_password_repeat" class="input-text" data-minlength="6" type="password" required
                       name="REGISTER[CONFIRM_PASSWORD]">
            </div>
        </div>

        <?
        /* CAPTCHA */
        if ($arResult["USE_CAPTCHA"] == "Y") {
            ?>
            <div class="row row--vcentr form-row">
                <div class="col-12 col-sm-3 col-md-2">
                    <label class="input-label">Капча:</label>
                </div>
                <div class="col-12 col-sm-9 col-md-10">
                    <input type="hidden" name="captcha_sid" value="<?= $arResult["CAPTCHA_CODE"] ?>"/>
                    <img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>" width="180"
                         height="40" alt="CAPTCHA"/>
                    <input type="text" name="captcha_word" maxlength="50" value=""/>
                </div>
            </div>
            <?
        }
        /* !CAPTCHA */
        ?>

        <div class="row row--vcentr form-row form-row--push-top">
            <button class="btn btn--fluid" type="submit" name="register_submit_button" value="reg">
                <span>Зарегистрироваться</span>
            </button>
        </div>
    </form>
<?
endif;
?>
