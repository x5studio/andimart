<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
?>

<?if($USER->IsAuthorized()){ ?>

<p><?echo GetMessage("MAIN_REGISTER_AUTH")?></p>

<? }else{

if (count($arResult["ERRORS"]) > 0) {
    foreach ($arResult["ERRORS"] as $key => $error)
        if (intval($key) == 0 && $key !== 0)
            $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;" . GetMessage("REGISTER_FIELD_" . $key) . "&quot;", $error);

    ShowError(implode("<br />", $arResult["ERRORS"]));

}elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y"){
?>
<p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
<? } ?>
        <form class="js-form-validator" novalidate method="post" action="<?=POST_FORM_ACTION_URI?>#reg-form-user" name="regform" enctype="multipart/form-data">
            <input type="hidden" name="reg_group" value="default">
            <input type="hidden" name="reg_hash" value="111">
            <!-- У этой и других форм добавлена валидация (класс js-form-validator). Это демо для верстки! Пример интеграции валидатора с формой можно посмотреть у формы подписки (src/scripts/app/forms/subscribe-form.js)-->
            <?
            if($arResult["BACKURL"] <> ''):
                ?>
                <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
                <?
            endif;
            ?>
            <div class="form-row text-small">Вы можете зарегистрироваться и получить преимущества постоянного покупателя:
                <br>• следить за статусом заказа,
                <br>• получать информацию об акциях и скидках.</div>
            <div class="form-row">
                <div class="input-advice">Все поля обязательны для заполнения</div>
            </div>
            <div class="row row--vcentr form-row">
                <div class="col-12 col-sm-3 col-md-2">
                    <label class="input-label" for="reg_user_name" >Имя:</label>
                </div>
                <div class="col-12 col-sm-9 col-md-10">
                    <input id="reg_user_name" class="input-text" type="text" required name="REGISTER[NAME]" value="<?=$arResult["VALUES"]["NAME"]?>">
                </div>
            </div>
            <div class="row row--vcentr form-row">
                <div class="col-12 col-sm-3 col-md-2">
                    <label class="input-label" for="reg_user_tel">Телефон:</label>
                </div>
                <div class="col-12 col-sm-9 col-md-10">
                    <input id="reg_user_tel" class="input-text js-mask-phone" type="tel" required  name="REGISTER[PERSONAL_PHONE]" value="<?=$arResult["VALUES"]["PERSONAL_PHONE"]?>">
                </div>
                <div class="col-12 col-sm-9 offset-sm-3 col-md-10 offset-md-2 input-advice">По нему с вами свяжется курьер.</div>
            </div>
            <div class="row row--vcentr form-row">
                <div class="col-12 col-sm-3 col-md-2">
                    <label class="input-label" for="reg_user_email">Email:</label>
                </div>
                <div class="col-12 col-sm-9 col-md-10">
                    <input id="reg_user_email" class="input-text" type="email" data-doubleemail required name="REGISTER[EMAIL]" value="<?=$arResult["VALUES"]["EMAIL"]?>">
                </div>
                <div class="col-12 col-sm-9 offset-sm-3 col-md-10 offset-md-2 input-advice doubleemail-error">Так мы будем на связи</div>
            </div>
            <div class="row row--vcentr form-row">
                <div class="col-12 col-sm-3 col-md-2">
                    <label class="input-label" for="reg_user_password">Пароль:</label>
                </div>
                <div class="col-12 col-sm-9 col-md-10">
                    <input id="reg_user_password" class="input-text" type="password" required  name="REGISTER[PASSWORD]" data-minlength="6" >
                </div>
                <div class="col-12 col-sm-9 offset-sm-3 col-md-10 offset-md-2 input-advice">Пароль должен быть не менее 6 символов длиной.</div>
            </div>
            <div class="row row--vcentr form-row">
                <div class="col-12 col-sm-3 col-md-2">
                    <label class="input-label" for="reg_user_password_repeat">Пароль ещё&nbsp;раз:</label>
                </div>
                <div class="col-12 col-sm-9 col-md-10">
                    <input id="reg_user_password_repeat" class="input-text" type="password" required name="REGISTER[CONFIRM_PASSWORD]" data-minlength="6">
                </div>
            </div>
            
            <?
            /* CAPTCHA */
            if ($arResult["USE_CAPTCHA"] == "Y")
            {
                ?>
                <div class="row row--vcentr form-row">
	                <div class="col-12 col-sm-3 col-md-2">
	                    <label class="input-label">Капча:</label>
	                </div>
	                <div class="col-12 col-sm-9 col-md-10">
	                    <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                        <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                        <input type="text" name="captcha_word" maxlength="50" value="" />
	                </div>
	            </div>
                <?
            }
            /* !CAPTCHA */
            ?>
            
            <div class="row row--vcentr form-row form-row--push-top">
                <div class="col-12">
                    <button class="btn btn--fluid" type="submit" name="register_submit_button" value="reg">
                        <span>Зарегистрироваться</span>
                    </button>
                </div>
            </div>
        </form>
<?
}
    ?>
