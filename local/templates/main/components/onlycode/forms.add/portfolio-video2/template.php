<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div id="portfolio-add-video" class="radiotabs__content js-radiotabs__content">
    <form id="<?=$arParams['CODE']?>form-id" class="js-form-validator" 
	  data-is_ajax="y" 
	  data-is_json="y" 
	  data-is_onlycode="y" 
	  data-redirect_url="/lichniy_cabinet/#portfolio_block" data-is_redirect="y" 
	  novalidate name="<?=$arParams['CODE']?>" action="<?=POST_FORM_ACTION_URI?>#portfolio_block" method="post">
	<?=bitrix_sessid_post()?>
	<!-- У этой и других форм добавлена валидация (класс js-form-validator). Это демо для верстки! Пример интеграции валидатора с формой можно посмотреть у формы подписки (src/scripts/app/forms/subscribe-form.js)-->
	<div class="form-row text-small">Укажите ссылку на видео (доступные видео сервисы: www.youtube.com, vimeo.com)</div>
	<input name="<?=$arParams['CODE']?>[BRAND]" class="portfolio-section_inp" type="hidden" value="">
	<div class="form-row">
	    <input class="input-text" name="<?=$arParams['CODE']?>[VIDEO]" type="text" placeholder="пример: www.youtube.com/watch?v=SWt-Xg" required>
	</div>
	<div class="form-row form-row--push-top">
	    <button class="btn btn--fluid" type="submit">
		<span>Добавить</span>
	    </button>
	</div>
    </form>
</div>



<script>
//	$(document).ready(function(){
//		$("#applicationform-step3-id").click(function()
//		{
//			$("#applicationform-id").find('input.custom-form-field').val("");
//			$("#applicationform-id").find('select.custom-form-field').val("")
//			$("#applicationform-id").find('select.custom-form-field').change();   
//		});
//		$("#<?= $arParams['CODE'] ?>form-id").submit(function()
//		{
//			var url = $(this).attr('action');
//			var data = $(this).serialize();
//			
//			$.post(url, data, function(output)
//			{
//				if (output.RESULT === 'error')
//				{
//					$('.custom-form-field').removeClass('error');
//					$('.custom-form-select').removeClass('error');
//					
//					for (var i in output.ERRORS)
//					{
//						if ($("select.<?= $arParams['CODE'] ?>-"+output.ERRORS[i]+"-form-field").parents('.custom-form-select').length > 0)
//						{
//							$("select.<?= $arParams['CODE'] ?>-"+output.ERRORS[i]+"-form-field").parents('.custom-form-select').addClass('error');
//						}
//						else
//						{
//							$(".<?= $arParams['CODE'] ?>-"+output.ERRORS[i]+"-form-field").addClass('error');
//						}
//					}
//				}
//				else
//				{
//					$('.custom-form-field').removeClass('error');
//					$('.custom-form-select').removeClass('error');
//					
//					$('.filedapplication-form-step3').click();
//				}
//			}, 'json');
//			
//			return false;
//		});
//	});
</script>
<?
/*
  <form id="<?=$arParams['CODE']?>form-id" name="<?=$arParams['CODE']?>" action="<?=POST_FORM_ACTION_URI?>" method="post">
  <?=bitrix_sessid_post()?>
  <div>
  <? foreach ($arResult['FIELDS'] as $field) { ?>
  <? if (!in_array($field['CODE'], array('AGREE', 'SUBSCRIPTION'))) { ?>
  <? if ($field['PROPERTY_TYPE'] == 'L') { ?>
  <select name="<?=$arParams['CODE']?>[<?=$field['CODE']?>]" class="custom-form custom-form-field <?=$arParams['CODE']?>-<?=$field['CODE']?>-form-field" data-placeholder="<?=$field['NAME']?><? if ($field['IS_REQUIRED'] == 'Y') { ?>*<? } ?>">
  <option value=""></option>
  <? foreach ($field['VARIANTS'] as $variant) { ?>
  <option value="<?=$variant['ID']?>"><?=$variant['VALUE']?></option>
  <? } ?>
  </select>
  <? } else { ?>
  <input name="<?=$arParams['CODE']?>[<?=$field['CODE']?>]" placeholder="<?=$field['NAME']?><? if ($field['IS_REQUIRED'] === 'Y') { ?>*<? } ?>" type="text" name="" class="custom-form custom-form-field <?=$arParams['CODE']?>-<?=$field['CODE']?>-form-field"/>
  <? } ?>
  <? } ?>
  <? } ?>
  </div>
  <div class="action-form-step2">
  <input placeholder="Получить инвестиции" type="submit" name="" class="custom-form standart-speed" value="Получить инвестиции" style="float: right; margin-right: 0px;"/>
  <p>
  <? $variants = array_values($arResult['FIELDS']['AGREE']['VARIANTS']) ?>
  <input checked="checked" type="checkbox" name="<?=$arParams['CODE']?>[AGREE]" value="<?=$variants[0]['ID']?>" class="custom-form" id="iagreetotheproccess"/><label class="<?=$arParams['CODE']?>-AGREE-form-field custom-form-field" for="iagreetotheproccess">Я согласен на обработку персональных данных (<a href="/info.php" target="_blank">подробнее</a>)</label>
  </p>
  <p>
  <? $variants = array_values($arResult['FIELDS']['SUBSCRIPTION']['VARIANTS']) ?>
  <input checked="checked" type="checkbox" name="<?=$arParams['CODE']?>[SUBSCRIPTION]" value="<?=$variants[0]['ID']?>" class="custom-form" id="subscribeme"/><label class="<?=$arParams['CODE']?>-SUBSCRIPTION-form-field custom-form-field" for="subscribeme">подписаться на рассылку</label>
  </p>
  </div>
  </form>

 */?>