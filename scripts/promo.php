<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Генератор кода");
const IBLOCK_ID = 20;
$arPromoIDs = array();
$db_props = CIBlock::GetProperties(IBLOCK_ID, Array(), Array("CODE"=>"PROMO_%"));
while($ar_props = $db_props->Fetch()) {
    $arPromoIDs[] = $ar_props;
}
$output = '';
if (isset($_POST["submit"])) {

    $arSelect = Array("ID", "IBLOCK_ID", "NAME");
    $arFilter = Array("IBLOCK_ID"=>IBLOCK_ID, "ACTIVE"=>"Y", ">CATALOG_QUANTITY"=>0, "PROPERTY_". $_POST["promo_id"]."_VALUE" => "Y");
    $db_res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while($arFields = $db_res->Fetch())
    {
        $arIDs[] = $arFields["ID"];
    }
    $output = '<promo id="'.$_POST["id"].'" type="promo code">
 <description>'.$_POST["description"].'</description>
 <promo-code>'.$_POST["promo-code"].'</promo-code>
 <discount unit="percent">'.$_POST["percent"].'</discount>
 <purchase>'."\r\n";
    foreach ($arIDs as $id){
        $output .= '<product offer-id="'.$id.'"/>'."\r\n";
    }
    $output .= '</purchase>'."\r\n".'</promo>';
}
?>
<h1><?$APPLICATION->ShowTitle(false);?></h1>
<div class="script-form">
    <form action="promo.php" method="post">
        <label>Выберите промо-акцию
            <select name="promo_id" class="input-text">
                <?foreach ($arPromoIDs as $item):?>
                <option value="<?=$item["ID"]?>"><?=$item["NAME"]?></option>
                <?endforeach;?>
            </select>
        </label>
        <br />
        <label>Введите ID промо-акции:
            <input class="input-text" type="text" name="id">
        </label>
        <br />
        <label>Введите описание:
            <input class="input-text" type="text" name="description">
        </label>
        <br />
        <label>Введите промо-код:
            <input class="input-text" type="text" name="promo-code">
        </label>
        <br />
        <label>Укажите процент скидки:
            <input type="number" min="1" max="99" name="percent" class="input-text" style="width: 70px;">
        </label>
        <br />
        <button class="btn" type="submit" name="submit" value="" style="margin-bottom: 50px;">
            <span>Генерировать</span>
        </button>
        <br />
    </form>
</div>
<div class="output">
    <?if($output):?>
    <textarea style="width: 500px; height: 200px; border: none; padding: 10px; margin-bottom: 50px;" readonly><?=$output?></textarea>
    <?endif;?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
