<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->SetTitle("");

//$is_ajax = false;
//if (!empty($_REQUEST['ajax']) && $_REQUEST['ajax'] == 'restore')
//{
//	$is_ajax = true;
//}

$email = '';
$submit = false;
$error = false;
if (!empty($_POST))
{
	$email = htmlspecialchars($_POST['email']);
	if (!empty($email) && check_email($email))
	{
		$user = CUser::GetList(($by="id"), ($order="desc"), array('EMAIL' => $email));
		$user = $user->Fetch();
		
		if (!empty($user))
		{
			$USER->SendPassword($user['LOGIN'], $user['EMAIL']);
			$submit = true;
		}
		else
		{
			$error = true;
		}
	}
	else
	{
		$error = true;
	}
}
?><div id="popup-password-restore" class="popup">
	<div class="popup__title">Запрос пароля</div>
	<div class="popup__text">
		<!-- После успешной отправки в этом блоке вместо формы выводится текст: "На указанный e-mail выслано письмо с инструкцией о восстановлении пароля."-->
		<!-- Пример обработки и вывода ошибок формы смотри в src/scripts/app/forms/ask-form.js-->
		<form class="js-form-validator" name="restore" novalidate action="/ajax/restore-pass.php" data-is_ajax='y'>

		<? if ($submit === false) { ?>
			<div class="content-block">Укажите e-mail, который вы использовали при регистрации.
				<!-- <br>На него будут отправлены контрольная строка для смены пароля, а&nbsp;также&nbsp;ваши регистрационные данные.--> </div>
			<div class="content-block">
				<? if ($error) { ?>
					<div class="form-error content-block">E-mail не найден в нашей базе клиентов!</div>
				<? } ?>
				<!-- После отправки заменить форму на текст: На указанный e-mail выслано письмо с инструкцией о восстановлении пароля.-->
				<div class="form-row row row--vcentr">
					<div class="col-12 col-sm-3">
						<label class="input-label h5" for="pass_restore_email">Выслать пароль на данный e-mail:</label>
					</div>
					<div class="col-12 col-sm-8">
						<input id="pass_restore_email" class="input-text" name="email" type="email" required value="<?=$email?>">
					</div>
				</div>
				<div class="form-row row">
					<div class="col-12 col-sm-4">
						<button class="btn btn--fluid" type="submit">
							<span>Выслать</span>
						</button>
					</div>
				</div>
			</div>
		<? } else { ?>
			<div class="content-block">На указанный вами e-mail отправлена контрольная информация для смены пароля.</div>
		<? } ?>
		</form>
	</div>
</div>