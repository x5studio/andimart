<?php
/*
[
    "Москва",
    "Санкт-Петербург",
    "Воронеж",
    "Волгоград",
    "Еатеринбург",
    "Иваново",
    "Иркутск",
    "Казань",
    "Краснодар",
    "Красноярск",
    "Магнитогорск",
    "Нижнийновгород",
    "Новосибирск",
    "Омск",
    "Оренбург",
    "Пенза",
    "Ростов-на-Дону",
    "Самара",
    "Саратов",
    "Сочи",
    "Сургут",
    "Тольяти",
    "Томск",
    "Тюмень",
    "Уфа",
    "Челябинск",
    "Череповец"
]*/
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
//if($_GET["search"]){
//$arSelect = Array("ID", "NAME");



$arFilter = Array(
    "IBLOCK_ID"=>CITIES_IBLOCK_ID, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", 
    '!PROPERTY_LOCATION_ID' => false );
if(isset($_GET["q"]))
{
	$arFilter['NAME'] = ($_GET["q"])."%";
}
    /*,"NAME"=>$_GET["search"]."%"*/
$res = CIBlockElement::GetList(Array(), $arFilter, false, false);
$result = array();
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    $arProperties = $ob->GetProperties();

    $result[$arProperties['LOCATION_ID']['VALUE']] = $arFields["NAME"];
}
//}
echo json_encode($result);