<?
define("NO_KEEP_STATISTIC", true);
define("NO_AGENT_CHECK", true);
define('PUBLIC_AJAX_MODE', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$_SESSION["SESS_SHOW_INCLUDE_TIME_EXEC"]="N";
$APPLICATION->ShowIncludeStat = false;

CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");
CModule::IncludeModule("sale");

if ($_REQUEST['action'] == 'ADD2BASKET')
{
	
        $id = (int)$_REQUEST['id'];
        $quantity = (int)$_REQUEST['quantity'];
        
        if ($id > 0)
        {
	
		$bsk = array();
		$dbBasketItems = CSaleBasket::GetList(
			array(
				"NAME" => "ASC",
				"ID" => "ASC"
				),
			array(
				"FUSER_ID" => CSaleBasket::GetBasketUserID(),
				"LID" => SITE_ID,
				"ORDER_ID" => "NULL"
				),
			false,
			false,
			array()
			);
		while ($point = $dbBasketItems->Fetch())
		{
			$bsk[$point['PRODUCT_ID']] = $point;
		}
		
		if (!empty($bsk[$id]))
		{
//			CSaleBasket::Delete($bsk[$id]['ID']);
		}
                
                $offer = CIBlockElement::GetList(
			array(), 
			array('IBLOCK_ID' => mm_iblock_id_offer, 'ID' => $id), 
			false, 
			false, 
				array('ID', 'IBLOCK_ID', 'PROPERTY_CML2_LINK', 'PROPERTY_SIZE', 'CATALOG_QUANTITY')
			);
		$offer = $offer->Fetch();
//                deb($offer['CATALOG_QUANTITY']-$offer['CATALOG_QUANTITY_RESERVED']);
                $product = CIBlockElement::GetList(
                        array(), 
                        array('IBLOCK_ID' => mm_iblock_id_catalog, 'ID' => $offer['PROPERTY_CML2_LINK_VALUE']), 
                        false, 
                        false, 
                        array('ID', 'NAME', 'CODE', 'IBLOCK_ID', 'PROPERTY_ARTICLE', 'DETAIL_PAGE_URL')
                );
		$product = $product->GetNext();
                
                Add2BasketByProductID($id, $quantity,
                array(),array()
//                array(
//                        array("NAME" => "Артикул", "CODE" => "ARTICLE", "VALUE" => $product['PROPERTY_ARTICLE_VALUE']),
//                        array("NAME" => "Товар", "CODE" => "PRODUCT", "VALUE" => $product['ID']),
//                        array("NAME" => "Название товара", "CODE" => "NAME", "VALUE" => $product['NAME']),
//                        array("NAME" => "Ссылка на товар", "CODE" => "LINK", "VALUE" => $product['DETAIL_PAGE_URL']),
//                        array("NAME" => "Количество товара", "CODE" => "QUANTITY", "VALUE" => ($offer['CATALOG_QUANTITY']-$offer['CATALOG_QUANTITY_RESERVED'])),
//                        array("NAME" => "Размер", "CODE" => "SIZE", "VALUE" => $offer['PROPERTY_SIZE_VALUE'])
//                )
		);
                
        }
}

if ($_REQUEST['action'] == 'CHANGEPOSITION')
{
	
        $id = (int)$_REQUEST['id'];
        $quantity = (int)$_REQUEST['quantity'];
        
        if ($id > 0 && $quantity > 0)
        {
//		deb($id, false);
//		deb($quantity);
		$bsk = array();
		$dbBasketItems = CSaleBasket::GetList(
			array(
				"NAME" => "ASC",
				"ID" => "ASC"
				),
			array(
				"FUSER_ID" => CSaleBasket::GetBasketUserID(),
				"LID" => SITE_ID,
				"ORDER_ID" => "NULL"
				),
			false,
			false,
			array()
		);
		while ($point = $dbBasketItems->Fetch())
		{
			$bsk[$point['PRODUCT_ID']] = $point;
		}
		
		if (!empty($bsk[$id]))
		{
			CSaleBasket::Update($bsk[$id]['ID'], array(
			    'QUANTITY' => $quantity
			));
			/*CSaleBasket::Delete($bsk[$id]['ID']);*/
		}
                
                /*$offer = CIBlockElement::GetList(array(), array('IBLOCK_ID' => mm_iblock_id_offer, 'ID' => $id), false, false, array('ID', 'IBLOCK_ID', 'PROPERTY_CML2_LINK'));
		$offer = $offer->Fetch();
                
                $product = CIBlockElement::GetList(
                        array(), 
                        array('IBLOCK_ID' => mm_iblock_id_catalog, 'ID' => $offer['PROPERTY_CML2_LINK_VALUE']), 
                        false, 
                        false, 
                        array('ID', 'IBLOCK_ID', 'PROPERTY_ARTICLE')
                );
		$product = $product->Fetch();
                
                Add2BasketByProductID($id, 1,
                array(),
                array(
                        array("NAME" => "Артикул", "CODE" => "ARTICLE", "VALUE" => $product['PROPERTY_ARTICLE_VALUE']),
                        array("NAME" => "Товар", "CODE" => "PRODUCT", "VALUE" => $product['ID'])
                ));*/
                
        }
}


if ($_REQUEST['action'] == 'DELETEPOSITION') 
{
	$id = (int)$_REQUEST['id'];
        
        if ($id > 0)
        {
		$bsk = array();
		$dbBasketItems = CSaleBasket::GetList(
			array(
				"NAME" => "ASC",
				"ID" => "ASC"
				),
			array(
				"FUSER_ID" => CSaleBasket::GetBasketUserID(),
				"LID" => SITE_ID,
				"ORDER_ID" => "NULL"
				),
			false,
			false,
			array()
			);
		while ($point = $dbBasketItems->Fetch())
		{
			$bsk[$point['PRODUCT_ID']] = $point;
		}
		
		if (!empty($bsk[$id]))
		{
			CSaleBasket::Delete($bsk[$id]['ID']);
		}
	}
}
$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket.line",
	"header",
	Array(
		"HIDE_ON_BASKET_PAGES" => "Y",
		"PATH_TO_BASKET" => SITE_DIR."cart/",
		"PATH_TO_ORDER" => SITE_DIR."cart/order/",
		"POSITION_FIXED" => "N",
		"SHOW_AUTHOR" => "N",
		"SHOW_EMPTY_VALUES" => "Y",
		"SHOW_NUM_PRODUCTS" => "Y",
		"SHOW_PERSONAL_LINK" => "N",
		"SHOW_PRODUCTS" => "Y",
		"SHOW_TOTAL_PRICE" => "Y"
	)
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?> 