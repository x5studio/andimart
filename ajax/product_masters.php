<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


$specNum = array(
    'vodosnabzhenie'     => 1,
    'kanalizatsiya'      => 2,
    'konditsionirovanie' => 3,
    'santekhnika'        => 6,
    'otoplenie'          => 4,
    'banya_i_sauna'      => 5, //here was 'bani' key
);

$arResult['USERS_FIELDS'] = array();
$arResult['USERS_FIELDS']['UF_CITY_NEW'] = array();
$arResult['USERS_FIELDS']['UF_ADD_CITIES_NEW'] = array();
$rsEnum = CIBlockElement::GetList(array('name' => 'asc'), array('IBLOCK_ID' => const_IBLOCK_ID_cities, 'ACTIVE' => 'Y'), false, false, array('ID', 'NAME'));
while ($arEnum = $rsEnum->Fetch()) {
    $arResult['USERS_FIELDS']['UF_CITY_NEW'][$arEnum['ID']] = $arEnum;
    $arResult['USERS_FIELDS']['UF_ADD_CITIES_NEW'][$arEnum['ID']] = $arEnum;
}
$arResult['USERS_FIELDS']['UF_SPECIALS'] = array();
$rsEnum = CIBlockSection::GetList(array('name' => 'asc'), array('IBLOCK_ID' => const_IBLOCK_ID_catalog, 'ACTIVE' => 'Y', 'DEPTH_LEVEL' => 1), false, array('ID', 'NAME', 'CODE'));
while ($arEnum = $rsEnum->GetNext()) {
    $arResult['USERS_FIELDS']['UF_SPECIALS'][$arEnum['ID']] = $arEnum;
}

$arResult['USERS'] = array();

$specials = array();
$arResult['FILTER']['UF_SPECIALS'] = array();
$us = CUser::GetList(($by = "personal_country"), ($order = "desc"), array('ACTIVE' => 'Y', 'GROUPS_ID' => 8, '!UF_SPECIALS' => false), array('SELECT' => array('UF_SPECIALS')));
while ($u = $us->Fetch()) {
    $specials = array_merge($specials, $u['UF_SPECIALS']);
}
$specials = array_unique($specials);
if (!empty($specials)) {
    foreach ($specials as $special) {
        if (empty($arResult['USERS_FIELDS']['UF_SPECIALS'][$special])) continue;
        $arResult['FILTER']['UF_SPECIALS'][$arResult['USERS_FIELDS']['UF_SPECIALS'][$special]['ID']] = $arResult['USERS_FIELDS']['UF_SPECIALS'][$special]['NAME'];
    }
}

$cities = array();
$arResult['FILTER']['CITIES'] = array();
$us = CUser::GetList(($by = "personal_country"), ($order = "desc"), array('ACTIVE' => 'Y', 'GROUPS_ID' => 8, array('LOGIC' => 'OR', '!UF_ADD_CITIES_NEW' => false, '!UF_CITY_NEW' => false)), array('SELECT' => array('UF_ADD_CITIES_NEW', 'UF_CITY_NEW')));
while ($u = $us->Fetch()) {
    $cities = array_merge($cities, array($u['UF_CITY_NEW']), array($u['UF_ADD_CITIES_NEW']));
}
$cities = array_unique($cities);
sort($cities);
if (!empty($cities)) {
    foreach ($cities as $city) {
        if ($city)
            $arResult['FILTER']['CITIES'][$city] = $city;
    }
}

$arResult['FILTER_SELECT'] = array(
    'UF_SPECIALS' => false,
    'CITIES'      => false,
);
if (!empty($_GET)) {
    if (!empty($_GET['city'])) {
        $city = htmlspecialchars($_GET['city']);
        if (!empty($arResult['FILTER']['CITIES'][$city])) {
            $arResult['FILTER_SELECT']['CITIES'] = $city;
        }
    } else {
        $arResult['FILTER_SELECT']['CITIES'] = $_SESSION["PEK_CURRENT_CITY_NAME"];
    }
    if (!empty($_GET['special'])) {
        $brand = htmlspecialchars($_GET['special']);
        if (!empty($arResult['FILTER']['UF_SPECIALS'][$brand])) {
            $arResult['FILTER_SELECT']['UF_SPECIALS'] = $brand;
        }
    }
}
$filter = array('ACTIVE' => 'Y', 'GROUPS_ID' => 8);
foreach ($arResult['FILTER_SELECT'] as $filterCode => $filterVal) {
    if (empty($filterVal)) continue;
    if ($filterCode == 'CITIES') {
        $filter[] = array('LOGIC' => 'OR', 'UF_ADD_CITIES_NEW' => $filterVal, 'UF_CITY_NEW' => $filterVal);
    } else {
        $filter[$filterCode] = $filterVal;
    }
}

$us_all = CUser::GetList(($by = "personal_country"), ($order = "desc"), $filter, array('SELECT' => array('UF_*'), 'FIELDS' => array('*')));
while ($u = $us_all->Fetch()) {
    $u["REVIEWS"]["COUNT"] = 0;
    $u["REVIEWS"]["SIGN"] = 0;
    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "XML_ID", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_SIGN");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
    $arFilter = Array("PROPERTY_US" => $u["ID"], "IBLOCK_ID" => 5);
    $brands = array();
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        //p($arFields);
        $u["REVIEWS"]["COUNT"]++;
        $u["REVIEWS"]["SIGN"] += intval($arFields["PROPERTY_SIGN_VALUE"]);
    }
    if ($u["REVIEWS"]["COUNT"]) {
        $u["REVIEWS"]["SIGN_REAL"] = ceil($u["REVIEWS"]["SIGN"] / $u["REVIEWS"]["COUNT"]);
    }


    $arResult['USERS'][] = $u;
}
$arrSort = array_column($arResult['USERS'], 'UF_USER_SORT');
array_multisort($arrSort, SORT_DESC, $arResult['USERS'], SORT_NUMERIC);
$startFrom = 0;
if (isset($_GET["PAGEN_1"]) && $_GET["PAGEN_1"] > 1) {
    $startFrom = ($_GET["PAGEN_1"] - 1) * 8;
}
$arResult['USERS'] = array_slice($arResult['USERS'], $startFrom, 8, true);
unset($u);

$NAV_PARAMS = array('nPageSize' => 8); //
if (!isset($_GET['PAGEN_1'])) $NAV_PARAMS['iNumPage'] = 1;

$us3 = CUser::GetList(($by = "personal_country"), ($order = "desc"), $filter, array('SELECT' => array('UF_*'), 'FIELDS' => array('*'), 'NAV_PARAMS' => $NAV_PARAMS));

?>
<div class="content-block">
    <ul class="workers grid grid--xs-2 grid--sm-4">
        <? foreach ($arResult['USERS'] as $arUser) { //deb($arUser, false);
            ?>
            <li class="workers__item grid__item">
                <div class="workers__item-in">
                    <div class="workers__content">
                        <? if (!empty($arUser['PERSONAL_PHOTO'])) {
                            ?>
                            <div class="workers__photo">
                                <a class="link-hidden" href="/masters/<?= $arUser['ID'] ?>/">
                                    <img src="<?= CFile::GetPath($arUser['PERSONAL_PHOTO']) ?>" width="160" height="160"
                                         alt="<?= $arUser['NAME'] ?>">
                                </a>
                            </div>
                            <?
                        } else {
                            ?>
                            <div class="workers__photo">
                                <a class="link-hidden" href="/masters/<?= $arUser['ID'] ?>/">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/static/images/upload/master-photo-sm.jpg?v=d133494e"
                                         width="160" height="160" alt="<?= $arUser['NAME'] ?>">
                                </a>
                            </div>
                        <? } ?>
                        <div class="workers__name">
                            <a class="link-hidden" href="/masters/<?= $arUser['ID'] ?>/"><?= $arUser['NAME'] ?></a>
                        </div>
                        <div class="workers__city">г. <?= $arUser['UF_CITY_NEW'] ?></div>
                        <? if (!empty($arUser['UF_SPECIALS'])) {
                            ?>
                            <div class="workers__spec">
                                <div class="workers__spec-h">Специализация:</div>
                                <div class="workers__spec-list">
                                    <? $arResult['USERS_FIELDS']['UF_SPECIALS'] ?>
                                    <?
                                    foreach ($arUser['UF_SPECIALS'] as $specID) {

                                        if (!empty($arResult['USERS_FIELDS']['UF_SPECIALS'][$specID])) {
                                            $spec = $arResult['USERS_FIELDS']['UF_SPECIALS'][$specID];
                                            $spec_num = $specNum[$spec['CODE']];
                                            ?>
                                            <div class="workers__spec-item">
                                                <div class="workers__spec-label"><?= $spec['NAME'] ?></div>
                                                <img src="<?= SITE_TEMPLATE_PATH ?>/static/images/specialities/spec-<?= $spec_num ?>.<?= $spec_num > 2 ? 'png' : 'svg' ?>?v=4e2c5a32"
                                                     width="32" height="32" alt="<?= $spec['NAME'] ?>">
                                            </div>
                                        <? } ?>
                                    <? } ?>
                                </div>
                            </div>
                        <? } ?>
                    </div>
                    <div class="workers__footer">
                        <div class="workers__footer-item">
                            <div class="stars stars--sm" data-rating="<?= $arUser["REVIEWS"]["SIGN_REAL"] ?>">
                                <div class="stars__list"><i class="stars__star"></i><i class="stars__star"></i><i
                                            class="stars__star"></i><i class="stars__star"></i><i
                                            class="stars__star"></i></div>
                            </div>
                        </div>
                        <div class="workers__footer-item">
                            <a class="btn btn--sm btn--inverse" href="/masters/<?= $arUser['ID'] ?>/">Подробнее</a>
                        </div>
                    </div>
                </div>
            </li>
        <? } ?>
    </ul>
    <?= $us3->GetPageNavString(
        '',
        'forum',
        'N',
        false
    ); ?>
</div>
