<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

$arFilter = Array(
    "IBLOCK_ID"=>CITIES_IBLOCK_ID, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", 
    '!PROPERTY_LOCATION_ID' => false, '!PROPERTY_HAS_SAM' => false );

$res = CIBlockElement::GetList(Array('name' => 'asc'), $arFilter, false, false,array("ID","NAME"));
$result = array();
while($arFields = $res->Fetch())
{
    $result[] = array("id"=>$arFields['ID'],"name"=>$arFields["NAME"]);
}
//}
echo json_encode($result);