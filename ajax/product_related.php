<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$default = array(  
	"ELEMENT_SORT_FIELD" => "sort",
	"ELEMENT_SORT_FIELD2" => "id",
	"ELEMENT_SORT_ORDER" => "asc",
	"ELEMENT_SORT_ORDER2" => "desc",  
);

//		deb($_GET["type"]);
	$filter = array();
	//$_GET["ITEM_ID"]; 
	if($_GET["ITEM_ID"]){
		$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*");//IBLOCK_ID  ID    , .  arSelectFields 
		$arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID, "ACTIVE_DATE"=>"Y","ID"=>$_GET["ITEM_ID"]);
		
		$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
		while($ob = $res->GetNextElement()){ 
			 $arFields = $ob->GetFields();  
			 $arProps = $ob->GetProperties();
			 
			if($_GET["type"]=="accessories"){
				$filter = $arProps["AKSESSUARY"]["VALUE"];
				
				$default['ELEMENT_SORT_FIELD'] = "catalog_PRICE_14";
				$default['ELEMENT_SORT_ORDER'] = "desc";
			}else{
				$filter = $arProps["ZAPCHASTI"]["VALUE"];
				
				$default['ELEMENT_SORT_FIELD'] = "name";
				$default['ELEMENT_SORT_ORDER'] = "asc";
			}
		}
		
		if(is_array($filter) && !empty($filter)){
			global $tab_filter;
			$tab_filter["XML_ID"]=$filter;
			 $APPLICATION->IncludeComponent(
					"bitrix:catalog.section", 
					"product_tab", 
					array(
						"ACTION_VARIABLE" => "action",
						"ADD_PICT_PROP" => "MORE_PHOTO",
						"ADD_PROPERTIES_TO_BASKET" => "Y",
						"ADD_SECTIONS_CHAIN" => "N",
						"ADD_TO_BASKET_ACTION" => "ADD",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"AJAX_OPTION_HISTORY" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "N",
						"BACKGROUND_IMAGE" => "-",
						"BASKET_URL" => "/lichniy_cabinet/basket.php",
						"BROWSER_TITLE" => "-",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "Y",
						"CACHE_TIME" => "36000000",
						"CACHE_TYPE" => "A",
						"CONVERT_CURRENCY" => "N",
						"DETAIL_URL" => "",
						"DISABLE_INIT_JS_IN_COMPONENT" => "N",
						"DISPLAY_BOTTOM_PAGER" => "Y",
						"DISPLAY_TOP_PAGER" => "N",
						"ELEMENT_SORT_FIELD" => $default['ELEMENT_SORT_FIELD'],
						"ELEMENT_SORT_FIELD2" => "id",
						"ELEMENT_SORT_ORDER" => $default['ELEMENT_SORT_ORDER'],
						"ELEMENT_SORT_ORDER2" => "desc",
						"FILTER_NAME" => "tab_filter",
						"HIDE_NOT_AVAILABLE" => "Y",
						"IBLOCK_ID" => "20",
						"IBLOCK_TYPE" => "1c_catalog",
						"INCLUDE_SUBSECTIONS" => "Y",
						"LABEL_PROP" => "CML2_MANUFACTURER",
						"LINE_ELEMENT_COUNT" => "3",
						"MESSAGE_404" => "",
						"MESS_BTN_ADD_TO_BASKET" => " ",
						"MESS_BTN_BUY" => "",
						"MESS_BTN_DETAIL" => "",
						"MESS_BTN_SUBSCRIBE" => "",
						"MESS_NOT_AVAILABLE" => "  ",
						"META_DESCRIPTION" => "-",
						"META_KEYWORDS" => "-",
						"OFFERS_LIMIT" => "5",
						"PAGER_BASE_LINK_ENABLE" => "N",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "N",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_TEMPLATE" => "ajax",
						"PAGER_TITLE" => "",
						"PAGE_ELEMENT_COUNT" => "12",
						"PARTIAL_PRODUCT_PROPERTIES" => "N",
						"PRICE_CODE" => array(
			0 => "WEB-цена",
						),
						"PRICE_VAT_INCLUDE" => "Y",
						"PRODUCT_ID_VARIABLE" => "id",
						"PRODUCT_PROPERTIES" => array(
						),
						"PRODUCT_PROPS_VARIABLE" => "prop",
						"PRODUCT_QUANTITY_VARIABLE" => "",
						"PRODUCT_SUBSCRIPTION" => "N",
						"PROPERTY_CODE" => array(
							0 => "BREND",
							1 => "",
						),
						"SECTION_CODE" => "",
						"SECTION_ID" => "",
						"SECTION_ID_VARIABLE" => "SECTION_ID",
						"SECTION_URL" => "",
						"SECTION_USER_FIELDS" => array(
							0 => "",
							1 => "",
						),
						"SEF_MODE" => "N",
						"SET_BROWSER_TITLE" => "Y",
						"SET_LAST_MODIFIED" => "N",
						"SET_META_DESCRIPTION" => "Y",
						"SET_META_KEYWORDS" => "Y",
						"SET_STATUS_404" => "N",
						"SET_TITLE" => "Y",
						"SHOW_404" => "N",
						"SHOW_ALL_WO_SECTION" => "Y",
						"SHOW_CLOSE_POPUP" => "N",
						"SHOW_DISCOUNT_PERCENT" => "N",
						"SHOW_OLD_PRICE" => "N",
						"SHOW_PRICE_COUNT" => "1",
						"TEMPLATE_THEME" => "blue",
						"USE_MAIN_ELEMENT_SECTION" => "N",
						"USE_PRICE_COUNT" => "N",
						"USE_PRODUCT_QUANTITY" => "Y",
						"COMPONENT_TEMPLATE" => "product_slider",
						"NAME"=>" ",
					),
					false
				);
		}
		
}

?>