<?
define("NO_KEEP_STATISTIC", true);
define("NO_AGENT_CHECK", true);
define('PUBLIC_AJAX_MODE', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$_SESSION["SESS_SHOW_INCLUDE_TIME_EXEC"]="N";
$APPLICATION->ShowIncludeStat = false;

CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");

$found = false;
$email = htmlspecialchars($_POST['email']);
if (!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL))
{
	$us = CUser::GetList(($by="personal_country"), ($order="desc"), array('EMAIL' => $email));
	if ($us = $us->Fetch())
	{
		$found = true;
	}
}

echo json_encode(array('result' => $found));
exit;