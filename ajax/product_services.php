<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


$arResult['USERS_FIELDS'] = array();
$arResult['USERS_FIELDS']['UF_CITY_NEW'] = array();
$arResult['USERS_FIELDS']['UF_ADD_CITIES_NEW'] = array();
$rsEnum = CIBlockElement::GetList(array('name' => 'asc'), array('IBLOCK_ID' => const_IBLOCK_ID_cities, 'ACTIVE' => 'Y'), false, false, array('ID', 'NAME'));
while ($arEnum = $rsEnum->Fetch())
{
	$arResult['USERS_FIELDS']['UF_CITY_NEW'][$arEnum['ID']] = $arEnum;
	$arResult['USERS_FIELDS']['UF_ADD_CITIES_NEW'][$arEnum['ID']] = $arEnum;
}
$arResult['USERS_FIELDS']['UF_SPECIALS'] = array();
$rsEnum = CIBlockSection::GetList(array('name' => 'asc'), array('IBLOCK_ID' => const_IBLOCK_ID_catalog, 'ACTIVE' => 'Y', 'DEPTH_LEVEL' => 1), false, array('ID', 'NAME', 'CODE'));
while ($arEnum = $rsEnum->GetNext())
{
	$arResult['USERS_FIELDS']['UF_SPECIALS'][$arEnum['ID']] = $arEnum;
}
$arResult['USERS_FIELDS']['UF_BRAND'] = array();
$rsEnum = CIBlockElement::GetList(array('name' => 'asc'), array('IBLOCK_ID' => const_IBLOCK_ID_brands, 'ACTIVE' => 'Y'), false, false, array('ID', 'NAME', 'CODE'));
while ($arEnum = $rsEnum->GetNext())
{
	$arResult['USERS_FIELDS']['UF_BRAND'][$arEnum['ID']] = $arEnum;
}

$arResult['USERS'] = array();
$us = CUser::GetList(($by = "personal_country"), ($order = "desc"), 
	array('ACTIVE' => 'Y', 
	    'GROUPS_ID' => 9, 
	    'UF_BRAND' => (int)$_GET['brand'], 
	    'UF_SPECIALS' => (int)$_GET['special'], 
	    array('LOGIC' => 'OR', 'UF_ADD_CITIES_NEW' => $_SESSION["PEK_CURRENT_CITY_NAME"], 'UF_CITY_NEW' => $_SESSION["PEK_CURRENT_CITY_NAME"])), 
	array('SELECT' => array('UF_*'), 'FIELDS' => array('*'), 'NAV_PARAMS' => array('nPageSize' => 8)));
while ($u = $us->Fetch())
{
	$u["REVIEWS"]["COUNT"] = 0;
	$u["REVIEWS"]["SIGN"] = 0;
	$arSelect = Array("ID", "IBLOCK_ID", "NAME","XML_ID","PREVIEW_PICTURE","DETAIL_PAGE_URL","PROPERTY_SIGN");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
	$arFilter = Array("PROPERTY_US"=>$u["ID"],"IBLOCK_ID"=>5);
	$brands = array();
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($ob = $res->GetNextElement()){ 
		$arFields = $ob->GetFields();  
		//p($arFields);
		$u["REVIEWS"]["COUNT"]++;
		$u["REVIEWS"]["SIGN"]+=intval($arFields["PROPERTY_SIGN_VALUE"]);
	}
	if($u["REVIEWS"]["COUNT"]){
		$u["REVIEWS"]["SIGN_REAL"] = ceil($u["REVIEWS"]["SIGN"]/$u["REVIEWS"]["COUNT"]);
	}



	$arResult['USERS'][] = $u;
}
?>

<div class="content-block">
  <ul class="workers grid grid--xs-2 grid--sm-4">
    <? foreach ($arResult['USERS'] as $arUser)
			{
				?>
				<li class="workers__item grid__item">
				    <div class="workers__item-in">
					<div class="workers__content">
					    <? if (!empty($arUser['PERSONAL_PHOTO']))
					    {
						    ?>
						    <div class="workers__photo">
							<a class="link-hidden" href="/service-centers/<?= $arUser['ID'] ?>/">
							    <img src="<?= CFile::GetPath($arUser['PERSONAL_PHOTO']) ?>" width="160" height="160" alt>
							</a>
						    </div>
					    <?
					    }
					    else
					    {
						    ?>
						    <div class="workers__photo">
							<a class="link-hidden" href="/service-centers/<?= $arUser['ID'] ?>/">
							    <img src="<?= SITE_TEMPLATE_PATH ?>/static/images/upload/master-photo-sm.jpg?v=d133494e" width="160" height="160" alt>
							</a>
						    </div>
					    <? } ?>
					    <div class="workers__name">
						<a class="link-hidden" href="/service-centers/<?= $arUser['ID'] ?>/"><?= $arUser['NAME'] ?></a>
					    </div>
					    <div class="workers__city">г. <?= $arUser['UF_CITY_NEW'] ?></div>

					</div>
					<div class="workers__footer">
					    <div class="workers__footer-item">
						<div class="stars stars--sm" data-rating="<?=$arUser["REVIEWS"]["SIGN_REAL"]?>">
						    <div class="stars__list"><i class="stars__star"></i><i class="stars__star"></i><i class="stars__star"></i><i class="stars__star"></i><i class="stars__star"></i></div>
						</div>
					    </div>
					    <div class="workers__footer-item">
						<a class="btn btn--sm btn--inverse" href="/service-centers/<?= $arUser['ID'] ?>/">Подробнее</a>
					    </div>
					</div>
				    </div>
				</li>
		    <? } ?>
  </ul><?=$us->GetPageNavString(
				'',
				'forum',
				'N',
				false
			);?>
</div>
