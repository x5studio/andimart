<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

global $DB;
global $USER;

$id = (int)$_POST['id'];
if ($USER->IsAuthorized() && $id > 0)
{
	CModule::IncludeModule("iblock");
	$elem = CIBlockElement::GetList(Array(), array('IBLOCK_ID' => const_IBLOCK_ID_portfolio, 
	    'ACTIVE' => 'Y', 'PROPERTY_USER' => $USER->GetID(), 'ID' => $id), false, Array("nPageSize"=>1));
	$elem = $elem->Fetch();
//	deb($elem, false);
	if (!empty($elem))
	{
		$DB->StartTransaction();
		if(!CIBlockElement::Delete($elem['ID']))
		{
			$DB->Rollback();
		}
		else
			$DB->Commit();
	}
}