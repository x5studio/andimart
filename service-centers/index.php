<?php
define('PAGE_TYPE', 'masters');
define('NOT_SHOW_H1', 'Y');
define('NO_WYSIWYG', 'Y');
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Сервисные центры | интернет-магазин Эндимарт #VREGION_NAME#");
$APPLICATION->SetPageProperty("description", "Контактная информация ближайших к вам сервисных центров: телефоны, адреса, режим работы.");
$APPLICATION->SetTitle("Сервисные центры");

$APPLICATION->IncludeComponent(
	"bitrix:main.include", "", Array(
		"SEF_MODE" => "Y",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_TEMPLATE_PATH . "/include/service-centers/index.php"
	), false, array('HIDE_ICONS' => 'Y')
);
 require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>