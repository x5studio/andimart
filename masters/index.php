<?php
define('PAGE_TYPE', 'masters');
define('NOT_SHOW_H1', 'Y');
define('NO_WYSIWYG', 'Y');
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Мастера | интернет-магазин Эндимарт #VREGION_NAME#");
$APPLICATION->SetPageProperty("description", "Мы поможем вам в выборе квалифицированного специалиста для установки и настройки оборудования.");
$APPLICATION->SetTitle("Мастера");

$APPLICATION->IncludeComponent(
	"bitrix:main.include", "", Array(
		"SEF_MODE" => "Y",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_TEMPLATE_PATH . "/include/masters/index.php"
	), false, array('HIDE_ICONS' => 'Y')
);
 require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
