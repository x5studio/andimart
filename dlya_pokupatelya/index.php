<?php









require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Вы можете узнать о новых акциях и статьях, задать нам вопрос, а так же ознакомиться с пользовательским соглашением.");
$APPLICATION->SetPageProperty("title", "Информация для покупателей | интернет-магазин Эндимарт #VREGION_NAME#");
$APPLICATION->SetTitle("Информация для покупателей");
?><p align="JUSTIFY">
	 Мы верим, что нет более выгодных и оправданных инвестиций для бизнеса, чем вложения в сервис или, другими словами, в отношения с клиентом. Поэтому, мы всегда стараемся предоставить клиентам самый лучший сервис в отрасли.
</p>
<div class="content-block content-block--push-top">
 <nav class="box-nav"> <a class="box-nav__item" href="/dlya_pokupatelya/akcii_statyi/">Статьи, акции</a> <a class="box-nav__item" href="/dlya_pokupatelya/vopros-otvet/">Вопрос-ответ</a> <a class="box-nav__item" href="/dlya_pokupatelya/polzovatelskoe_soglashenie/">Соглашение</a> </nav>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>