<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Как сделать заказ");
?><?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"breadcrumbs",
	Array(
		"PATH" => "",
		"SITE_ID" => "s1",
		"START_FROM" => "0"
	)
);?>
<div class="content-block content-block--pull-top island ">
	<div class="wysiwyg">
		<h1 class="section-title title-line  js-title-line">Как сделать заказ</h1>
		<p>
 <b>Добро пожаловать в интернет-магазин Эндимарт!</b><br>
		</p>
		<p>
			 Перед тем, как совершать покупки, необходимо выбрать город вашей локации, в верхней части страницы. Это нужно, чтобы во время просмотра каталога Вам показывалась только актуальная информация о наличии товара и акциях.
		</p>
		<p align="center">
 <img src="/dlya_pokupatelya/kak_sdelat_zakaz/Kak-stelat-zakaz-1.jpg" width="733" height="425">
		</p>
		<p>
			 Когда Вы определились с выбором, поместите товар в корзину, нажав «Купить» на его карточке в каталоге, или на странице с его описанием
		</p>
		<p align="center">
 <img src="/dlya_pokupatelya/kak_sdelat_zakaz/Kak-stelat-zakaz-2.jpg" width="541" height="359">
		</p>
 <br>
		<p>
			 Вы можете сразу перейти в корзину, нажав на кнопку «В корзине».
		</p>
		<p align="center">
 <img src="/dlya_pokupatelya/kak_sdelat_zakaz/Kak-stelat-zakaz-3.jpg" width="545" height="355">
		</p>
		<p>
			 Когда Вы добавили все необходимые товары, можно перейти к оформлению заказа. Нажмите кнопку «Корзина» в верхней части страницы и отредактируйте ваш заказ. После этого можно нажать кнопку «Оформить заказ».
		</p>
		<p align="center">
 <img src="/dlya_pokupatelya/kak_sdelat_zakaz/Kak-stelat-zakaz-4.jpg" width="568" height="284">
		</p>
		<p>
			 На странице оформления заказа заполните регион доставки.
		</p>
		<p align="center">
 <img src="/dlya_pokupatelya/kak_sdelat_zakaz/Kak-stelat-zakaz-5.jpg" width="424" height="206">
		</p>
		<p>
			 Выберите способ доставки: <br>
			 - с помощью курьера
		</p>
		<p align="center">
 <img src="/dlya_pokupatelya/kak_sdelat_zakaz/Kak-stelat-zakaz-6.jpg" width="393" height="300">
		</p>
		<p>
			 - самовывоз. При выборе этого способа не забудьте указать <a href="/punkty_vidachi/">пункт выдачи</a>.
		</p>
		<p align="center">
 <img src="/dlya_pokupatelya/kak_sdelat_zakaz/Kak-stelat-zakaz-7.jpg" width="491" height="221">
		</p>
		<p>
			 После этого вам нужно указать желаемый способ оплаты: онлайн или при получении. Подробнее об оплате заказа можно узнать <a href="/oplata_i_dostavka/">здесь</a>.
		</p>
		<p align="center">
 <img src="/dlya_pokupatelya/kak_sdelat_zakaz/Kak-stelat-zakaz-8.jpg" width="451" height="302">
		</p>
		<p>
			 Далее заполните форму покупателя. Поля со звездочкой обязательны для заполнения.
		</p>
		<p align="center">
 <img src="/dlya_pokupatelya/kak_sdelat_zakaz/Kak-stelat-zakaz-9.jpg" width="420" height="593">
		</p>
		<p>
			 После заполнения данных нажмите на кнопку «Оформить заказ».
		</p>
		<p align="center">
 <img src="/dlya_pokupatelya/kak_sdelat_zakaz/Kak-stelat-zakaz-10.jpg" width="513" height="302">
		</p>
		<p>
			 Поздравляем, Ваш заказ принят! После выполнения всех шагов Вы увидите все указанные вами данные, сможете распечатать накладную заказа, или оставить отзыв о нашем магазине. Также Вы сможете воспользоваться нашими инструментами по подбору <a href="/masters/">мастеров-монтажников</a> и <a href="/service-centers/">сервисных центров</a>.
		</p>
		<p align="center">
 <img src="/dlya_pokupatelya/kak_sdelat_zakaz/Kak-stelat-zakaz-11.jpg" width="383" height="489">
		</p>
		<p>
			 После этого вам будут высланы письмо и смс с подтверждением о вашем заказе.
		</p>
	</div>
</div>
 <br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>