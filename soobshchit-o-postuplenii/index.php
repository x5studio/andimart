
        <div class="report-form-content">
            <!-- js-form-validator -->
            <form class="form " novalidate="" id="report-form" name="report_form" action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="sessid" id="sessid" value="<?=$_SESSION["fixed_session_id"]?>">
                <input type="hidden" name="ITEM_ID" value="<?=$id?>">
                <input type="hidden" name="PROPERTY[945][]" value="<?=$id?>">
                <!-- У этой и других форм добавлена валидация (класс js-form-validator). Это демо для верстки! Пример интеграции валидатора с формой можно посмотреть у формы подписки (src/scripts/app/forms/subscribe-form.js)-->
                <div class="form__title">Сообщить о поступлении</div>
                <div class="row row--vcentr form-row">
                    <div class="col-12 col-sm-2 col-md-3 text-sm-right">
                        <label class="input-label" for="report_name">Имя:</label>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6">
                        <input id="report_name" class="input-text" type="text" required="" name="user_name">
                    </div>
                </div>
                <div class="row row--vcentr form-row">
                    <div class="col-12 col-sm-2 col-md-3 text-sm-right">
                        <label class="input-label" for="report_phone">Телефон:</label>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6">
                        <input id="report_phone" class="input-text" type="text" name="user_phone" required="">
                    </div>
                </div>
                <div class="row row--vcentr form-row">
                    <div class="col-12 col-sm-2 col-md-3 text-sm-right">
                        <label class="input-label" for="report_email">Email:</label>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6">
                        <input id="report_email" class="input-text" type="email" name="user_email" required="">
                    </div>
                </div>
                <div class="row form-row">
                    <div class="col-12 col-sm-2 col-md-3 text-sm-right">
                        <label class="input-label" for="report_comment">Комментарий:</label>
                    </div>
                    <div class="col-12 col-sm-8 col-md-6">
                        <textarea id="report_comment" class="input-text input-text--textarea" required="" name="user_comment"></textarea>
                    </div>
                </div>
                <div class="form-row text-center">
                    <button class="btn" type="submit" name="submit" value="send">
                        <span>Отправить</span>
                    </button>
                </div>
                <input type="hidden" value="send" name="report_submit">
            </form>
        </div>
