<?php
//define("PAGE_TYPE","reviews");
define("NOT_SHOW_H1","Y");
define("NO_WYSIWYG","Y");
define("PERSONAL_ORDERS","Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Личный кабинет");
$APPLICATION->SetTitle("Личный кабинет");

$is_master = false;
$is_service = false;
if (in_array(8, $USER->GetUserGroupArray()))
{
	$is_master = true;
}
if (in_array(9, $USER->GetUserGroupArray()))
{
	$is_service = true;
}
//deb($USER->GetUserGroupArray(), false);
 if ($is_master) { $APPLICATION->IncludeComponent(
		"bitrix:main.profile",
		'master',
		Array(
			"CHECK_RIGHTS" => "N",
			"SEND_INFO" => "N",
			"SET_TITLE" => "N",
			"USER_PROPERTY" => array(	
				"UF_SPECIALS",
				"UF_CITY",
				"UF_ADD_CITIES_NEW",
				"UF_CITY_NEW",
			    'UF_IN_YT',
			    'UF_IN_IN',
			    'UF_IN_FB',
			    'UF_IN_OK',
			    'UF_IN_TW',
			    'UF_IN_VK',
			    'UF_ADD_CITIES',
			),
			"USER_PROPERTY_NAME" => ""
		)
	);
	global $arPortfolioFilter;
	$arPortfolioFilter = array(
		'PROPERTY_USER' => $USER->GetID()
	);
	$APPLICATION->IncludeComponent(
		"bitrix:news.list",
		"portfolio",
		Array(
			"ACTIVE_DATE_FORMAT" => "d.m.Y",
			"ADD_SECTIONS_CHAIN" => "N",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_ADDITIONAL" => "",
			"AJAX_OPTION_HISTORY" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "N",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "Y",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"CHECK_DATES" => "Y",
			"DETAIL_URL" => "",
			"DISPLAY_BOTTOM_PAGER" => "N",
			"DISPLAY_DATE" => "Y",
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "Y",
			"DISPLAY_PREVIEW_TEXT" => "Y",
			"DISPLAY_TOP_PAGER" => "N",
			"FIELD_CODE" => array("", ""),
			"FILTER_NAME" => "arPortfolioFilter",
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",
			"IBLOCK_ID" => "30",
			"IBLOCK_TYPE" => "content",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
			"INCLUDE_SUBSECTIONS" => "N",
			"MESSAGE_404" => "",
			"NEWS_COUNT" => "500",
			"PAGER_BASE_LINK_ENABLE" => "N",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "N",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_TEMPLATE" => ".default",
			"PAGER_TITLE" => "Новости",
			"PARENT_SECTION" => "",
			"PARENT_SECTION_CODE" => "",
			"PREVIEW_TRUNCATE_LEN" => "",
			"PROPERTY_CODE" => array("VIDEO", ""),
			"SET_BROWSER_TITLE" => "N",
			"SET_LAST_MODIFIED" => "N",
			"SET_META_DESCRIPTION" => "N",
			"SET_META_KEYWORDS" => "N",
			"SET_STATUS_404" => "N",
			"SET_TITLE" => "N",
			"SHOW_404" => "N",
			"SORT_BY1" => "ID",
			"SORT_BY2" => "SORT",
			"SORT_ORDER1" => "ASC",
			"SORT_ORDER2" => "ASC",
			"STRICT_SECTION_CHECK" => "N"
		)
	); } elseif ($is_service) { $APPLICATION->IncludeComponent(
		"bitrix:main.profile",
		'service',
		Array(
			"CHECK_RIGHTS" => "N",
			"SEND_INFO" => "N",
			"SET_TITLE" => "N",
			"USER_PROPERTY" => array(		    
				"UF_BRAND",
				"UF_SPECIALS",
				"UF_CITY",
				"UF_ADD_CITIES_NEW",
				"UF_CITY_NEW",
			    'UF_IN_YT',
			    'UF_IN_IN',
			    'UF_IN_FB',
			    'UF_IN_OK',
			    'UF_IN_TW',
			    'UF_IN_VK',
			    'UF_ADD_CITIES',
			),
			"USER_PROPERTY_NAME" => ""
		)
	);
	global $arPortfolioFilter;
	$arPortfolioFilter = array(
		'PROPERTY_USER' => $USER->GetID()
	);
	$APPLICATION->IncludeComponent(
		"bitrix:news.list",
		"portfolio2",
		Array(
			"ACTIVE_DATE_FORMAT" => "d.m.Y",
			"ADD_SECTIONS_CHAIN" => "N",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_ADDITIONAL" => "",
			"AJAX_OPTION_HISTORY" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "N",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "Y",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"CHECK_DATES" => "Y",
			"DETAIL_URL" => "",
			"DISPLAY_BOTTOM_PAGER" => "N",
			"DISPLAY_DATE" => "Y",
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "Y",
			"DISPLAY_PREVIEW_TEXT" => "Y",
			"DISPLAY_TOP_PAGER" => "N",
			"FIELD_CODE" => array("", ""),
			"FILTER_NAME" => "arPortfolioFilter",
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",
			"IBLOCK_ID" => "30",
			"IBLOCK_TYPE" => "content",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
			"INCLUDE_SUBSECTIONS" => "N",
			"MESSAGE_404" => "",
			"NEWS_COUNT" => "500",
			"PAGER_BASE_LINK_ENABLE" => "N",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "N",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_TEMPLATE" => ".default",
			"PAGER_TITLE" => "Новости",
			"PARENT_SECTION" => "",
			"PARENT_SECTION_CODE" => "",
			"PREVIEW_TRUNCATE_LEN" => "",
			"PROPERTY_CODE" => array("VIDEO", ""),
			"SET_BROWSER_TITLE" => "N",
			"SET_LAST_MODIFIED" => "N",
			"SET_META_DESCRIPTION" => "N",
			"SET_META_KEYWORDS" => "N",
			"SET_STATUS_404" => "N",
			"SET_TITLE" => "N",
			"SHOW_404" => "N",
			"SORT_BY1" => "ID",
			"SORT_BY2" => "SORT",
			"SORT_ORDER1" => "ASC",
			"SORT_ORDER2" => "ASC",
			"STRICT_SECTION_CHECK" => "N"
		)
	); } else { $APPLICATION->IncludeComponent(
		"bitrix:main.profile",
		'.default',
		Array(
			"CHECK_RIGHTS" => "N",
			"SEND_INFO" => "N",
			"SET_TITLE" => "N",
			"USER_PROPERTY" => array(	
			),
			"USER_PROPERTY_NAME" => ""
		)
	); } require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
