<?
define("PAGE_TYPE","reviews");
define("NOT_SHOW_H1","Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Восстановление пароля");
?>

	<div class="row b-list">
		<div class="col-12 col-sm-6 b-list__item">
			<div class="form form--box">
				<?$APPLICATION->IncludeComponent(
"bitrix:system.auth.changepasswd",
".default",
Array()
);?>
			</div>
		</div>
		<div class="col-12 col-sm-6 b-list__item">
			<div class="form form form--box">
				<h2 class="form__title text-left">Регистрация</h2>
				<div class="radiotabs js-radiotabs">
					<div class="radiotabs__btns">
						<div class="radiotabs__row">
							<a class="radiotabs__btn js-radiotabs__btn" href="#reg-form-user">Покупатель</a>
						</div>
						<div class="radiotabs__row">
							<a class="radiotabs__btn js-radiotabs__btn" href="#reg-form-worker">Мастер</a>
						</div>
						<div class="radiotabs__row">
							<a class="radiotabs__btn js-radiotabs__btn" href="#reg-form-company">Сервис-центр</a>
						</div>
					</div>
					
					<div id="reg-form-user" class="radiotabs__content js-radiotabs__content">
						<?$APPLICATION->IncludeComponent("ruformat:main.register", "register1", Array(
							"AUTH" => "Y",	// Автоматически авторизовать пользователей
							"REQUIRED_FIELDS" => "",	// Поля, обязательные для заполнения
							"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
							"SHOW_FIELDS" => array(	// Поля, которые показывать в форме
								0 => "EMAIL",
								1 => "NAME",
								2 => "PERSONAL_PHONE",
							),
							"SUCCESS_PAGE" => "/lichniy_cabinet/",	// Страница окончания регистрации
							"USER_PROPERTY" => "",	// Показывать доп. свойства
							"USER_PROPERTY_NAME" => "",	// Название блока пользовательских свойств
							"USE_BACKURL" => "Y",	// Отправлять пользователя по обратной ссылке, если она есть
							"reg_hash"=>"111",
						),
							false
						);?>
					</div>
					<?$APPLICATION->IncludeComponent(
						"ruformat:main.register",
						"register2",
						array(
							"AUTH" => "Y",
							"REQUIRED_FIELDS" => array(
							),
							"SET_TITLE" => "Y",
							"SHOW_FIELDS" => array(
								0 => "EMAIL",
								1 => "NAME",
								2 => "PERSONAL_PHOTO",
								3 => "PERSONAL_PHONE",
							),
							"SUCCESS_PAGE" => "/lichniy_cabinet/",
							"USER_PROPERTY" => array(
								0 => "UF_VID",
								1 => "UF_CITY",
							),
							"USER_PROPERTY_NAME" => "",
							"USE_BACKURL" => "Y",
							"COMPONENT_TEMPLATE" => "register2",
							"GROUPS" => array(USER_MASTER_GROUP_ID),
							"reg_hash"=>"222",
						),
						false
					);?>
					<?$APPLICATION->IncludeComponent(
						"ruformat:main.register",
						"register3",
						array(
							"AUTH" => "Y",
							"REQUIRED_FIELDS" => array(
							),
							"SET_TITLE" => "Y",
							"SHOW_FIELDS" => array(
								0 => "EMAIL",
								1 => "NAME",
								2 => "PERSONAL_PHOTO",
								3 => "PERSONAL_PHONE",
							),
							"SUCCESS_PAGE" => "/lichniy_cabinet/",
							"USER_PROPERTY" => array(
								0 => "UF_CITY",
								1 => "UF_BRAND",
							),
							"USER_PROPERTY_NAME" => "",
							"USE_BACKURL" => "Y",
							"COMPONENT_TEMPLATE" => "register2",
							"GROUPS" => array(USER_SERVICE_GROUP_ID),
							"reg_hash"=>"333",
						),
						false
					);?>
				</div>
				<div class="text-small">Нажимая кнопку «Зарегистрироваться», вы принимаете условия
					<a href="/dlya_pokupatelya/polzovatelskoe_soglashenie/" target="_blank">Пользовательского соглашения</a>.</div>
			</div>
		</div>
	</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>