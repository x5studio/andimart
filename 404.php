<head>
    <link href="/local/templates/404/template_styles.css" rel="stylesheet">
</head>
<title>Страница не найдена</title>
<?
header("HTTP/1.1 404 Not Found");
?>
<div class="page">
	<div class="page__header">
		<div id="header" class="header">
			<div class="container">
				<div class="header__mid">
					<div class="header__logo">
						<div class="logo">
 <a class="logo__link" href="/"><img src="/local/templates/404/logo.png" class="logo__image" alt="ЭНДИМАРТ" width="325" height="90"></a>
						</div>
					</div>
					<div class="header__elems">
						<div class="header__elem header__elem--tel">
 <a class="header__tel" href="/o_companii/contacti/"><img src="/local/templates/404/phone.png" alt="ТЕЛЕФОН" width="390" height="62"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="page__content">
		<div class="container">
			<div class="content-block content-block--pull-top island">
				<h1>404</h1>
				<h1>СТРАНИЦА НЕ НАЙДЕНА</h1>
				 Удостоверьтесь в правильности адреса или введите другой поисковый запрос
				<div id="header-search-form" class="header__search">
					<form class="search-form" novalidate="novalidate" action="/catalog/">
						<div class="input-group">
							<div class="input-group__item input-group__item--grow">
 <input class="input-text search-form__input" type="text" placeholder="Поиск" name="q" required="required" value=""><button class="search-form__submit" type="submit"></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
 <footer class="page__footer">
		<div class="footer">
			<div class="container">
				<div class="footer__bottom">
					<div class="row">
						<div class="col-12 col-md-6">
							<p style="text-align: justify;">
								 ©&nbsp;2022 Эндимарт
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
 </footer>
	</div>
	 <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter45162276 = new Ya.Metrika({ id:45162276, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script>
</div>