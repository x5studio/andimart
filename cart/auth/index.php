<?
define("PAGE_TYPE","order");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Эндимарт. Контактные данные");
?>
<div class="row b-list">
	<? 
	if (!$USER->IsAuthorized() || $_GET['login'] == 'yes') { ?>
                <div class="col-12 col-sm-6 b-list__item js-tabs">
                  <div class="island box js-focus-b focus-b is-active">
                    <div class="island-head-tabs">
                      <div class="island-head-tabs__item">
                        <a class="island-head-tabs__link js-tabs__btn" href="#checkout-reg">
                          <div class="island-head-tabs__title">Покупаете впервые?</div>
                        </a>
                      </div>
                      <div class="island-head-tabs__item">
                        <a class="island-head-tabs__link js-tabs__btn" href="#checkout-login">
                          <div class="island-head-tabs__title">Вход</div>
                        </a>
                      </div>
                    </div>
                    <div id="checkout-reg" class="js-tabs__content">
			 <?$APPLICATION->IncludeComponent("ruformat:main.register", "register1", Array(
				"AUTH" => "Y",	// Автоматически авторизовать пользователей
				"REQUIRED_FIELDS" => "",	// Поля, обязательные для заполнения
				"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
				"SHOW_FIELDS" => array(	// Поля, которые показывать в форме
					0 => "EMAIL",
					1 => "NAME",
					2 => "PERSONAL_PHONE",
				),
				"SUCCESS_PAGE" => "/cart/delivery/",	// Страница окончания регистрации
				"USER_PROPERTY" => "",	// Показывать доп. свойства
				"USER_PROPERTY_NAME" => "",	// Название блока пользовательских свойств
				"USE_BACKURL" => "Y",	// Отправлять пользователя по обратной ссылке, если она есть
				"reg_hash"=>"111",
			),
				false
			);?>
                      <div class="text-small form-row form-row--push-top">Нажимая кнопку «Зарегистрироваться» вы принимаете условия
                        <a href="/dlya_pokupatelya/polzovatelskoe_soglashenie/" target="_blank">Пользовательского соглашения</a>.</div>
                    </div>
                    <div id="checkout-login" class="js-tabs__content">
                        <!-- У этой и других форм добавлена валидация (класс js-form-validator). Это демо для верстки! Пример интеграции валидатора с формой можно посмотреть у формы подписки (src/scripts/app/forms/subscribe-form.js)-->
				<?$APPLICATION->IncludeComponent("bitrix:system.auth.form", "auth", Array(
						
						"FORGOT_PASSWORD_URL" => "/cart/auth/",	// Страница забытого пароля
						"PROFILE_URL" => "/cart/delivery/",	// Страница профиля
						"REGISTER_URL" => "/cart/auth/",	// Страница регистрации
						"SHOW_ERRORS" => "Y",	// Показывать ошибки
					),
					false
				);?>
                        <div class="hidden">
                          <?
				$APPLICATION->IncludeComponent(
					"bitrix:main.include", "", Array(
					"AREA_FILE_SHOW" => "file",
					"AREA_FILE_SUFFIX" => "inc",
					"EDIT_TEMPLATE" => "",
					"PATH" => "/ajax/restore-pass.php"
					)
				);
				?>
                        </div>
                    </div>
                  </div>
                </div>
	<? } ?>
                <div class="col-12 col-sm-6 b-list__item">
                  <div class="island box js-focus-b focus-b<? if ($USER->IsAuthorized()) { ?> is-active<? } ?>">
		      <? if (!$USER->IsAuthorized()) { ?>
				<div class="island-head-tabs">
				  <div class="island-head-tabs__item island-head-tabs__item--current">
				    <div class="island-head-tabs__title">Без регистрации</div>
				  </div>
				</div>
		      <form id="fastreg" class="js-form-validator" novalidate action="/cart/delivery/" method="post">
			  <div class="row row--vcentr form-row">
			    <div class="col-12 col-sm-3 col-md-2">
			      <label class="input-label" for="checkout_user_name">Имя:</label>
			    </div>
			    <div class="col-12 col-sm-9 col-md-10">
				<input id="checkout_user_name" name="name" class="input-text" type="text" required value="">
			    </div>
			  </div>
			  <div class="row row--vcentr form-row">
			    <div class="col-12 col-sm-3 col-md-2">
			      <label class="input-label" for="checkout_user_tel">Телефон:</label>
			    </div>
			    <div class="col-12 col-sm-9 col-md-10">
			      <input id="checkout_user_tel" name="phone" class="input-text js-mask-phone" type="tel" required>
			    </div>
			  </div>
			  <div class="row row--vcentr form-row">
			    <div class="col-12 col-sm-3 col-md-2">
			      <label class="input-label" for="checkout_user_email">Email:</label>
			    </div>
			    <div class="col-12 col-sm-9 col-md-10">
			      <input id="checkout_user_email" name="email" class="input-text" type="email" required value="<?=$USER->GetEmail()?>">
			    </div>
			  </div>
			  <div class="row row--vcentr form-row">
			    <div class="col-12 col-sm-3 col-md-2">
			      <label class="input-label" for="checkout_user_email">Капча:</label>
			    </div>
			    <div class="col-12 col-sm-9 col-md-10" id="capBorder">
			      	<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
					<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
			    	<input type="text" name="captcha_word" maxlength="50" value="" />
			    </div>
			  </div>
			  
			  
			  <div class="row row--vcentr form-row form-row--push-top">
			    <div class="col-12 col-sm-6">
			      <button class="btn btn--fluid" type="submit">
				<span>Далее</span>
			      </button>
			    </div>
			    
			    <div class="text-small form-row form-row--push-top">Нажимая кнопку «Далее» вы принимаете условия
                        <a href="/dlya_pokupatelya/polzovatelskoe_soglashenie/" target="_blank">Пользовательского соглашения</a>.</div>
			    
			  </div>
		      </form>
		      <? } else { ?>
		      <div class="island-head-tabs">
			<div class="island-head-tabs__item island-head-tabs__item--current">
			  <div class="island-head-tabs__title">Контактные данные</div>
			</div>
		      </div>
			<?$APPLICATION->IncludeComponent(
				"bitrix:main.profile",
				"cart",
				Array(
				"SUCCESS_PAGE" => "/cart/delivery/",	// Страница окончания регистрации
					"AJAX_MODE" => "N",
					"AJAX_OPTION_ADDITIONAL" => "",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "N",
					"CHECK_RIGHTS" => "N",
					"SEND_INFO" => "N",
					"SET_TITLE" => "N",
					"USER_PROPERTY" => array(),
					"USER_PROPERTY_NAME" => ""
				)
			);?>
		      <? } ?>
                    
                  </div>
                </div>
              </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>