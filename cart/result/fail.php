<?
define("STOP_STATISTICS", true);
define("NO_AGENT_CHECK", true);
define('NOT_CHECK_PERMISSIONS', true);
define("DisableEventsCheck", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

global $USER;
$successPath = COption::GetOptionString("sale", "sale_ps_success_path", "/");

$arFilter = Array(
   "USER_ID" => $USER->GetID()
);

$order = CSaleOrder::GetList(array("DATE_INSERT" => "DESC"), $arFilter);
$order = $order->Fetch();

LocalRedirect('/cart/delivery/?ORDER_ID='.$order['ID']);

require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_after.php");
?>