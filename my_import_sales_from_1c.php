<?

include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\CModule::IncludeModule('iblock');

$file_name = $_SERVER['DOCUMENT_ROOT'].'/sales___96af83d5-00c9-4eef-9cae-5db58e68380f.xml';


if(strpos(basename($file_name), 'sales') === 0) {
    $xml_object = simplexml_load_file($file_name);

    $xml_object_vars = get_object_vars($xml_object);

    $offer_package_vars = get_object_vars($xml_object_vars['ПакетПредложений']);

    $iblock_xml_id = $offer_package_vars['ИдКаталога'];

    $offers_vars = get_object_vars($offer_package_vars['Предложения']);

    $ar_offers = $offers_vars['Предложение'];

    $iblock_id = -1;

    $db_res = \CIBlock::GetList(array(), array('XML_ID' => $iblock_xml_id));
    while ($ar_res = $db_res->Fetch()) {
        $iblock_id = $ar_res['ID'];
        break;
    }

    $ar_purchase_count = [];
    $ar_offer_keys = [];

    foreach ($ar_offers as $offer) {
        $offer_vars = get_object_vars($offer);

        $key = $offer_vars['Ид'];
        $value = $offer_vars['КоличествоПродаж'];

        $ar_purchase_count[$key] = $value;
        $ar_offer_keys[] = $key;
    }

    $ar_new_values = [];

    $db_res = \CIBlockElement::GetList(array(), array('IBLOCK_ID' => $iblock_id, 'XML_ID' => $ar_offer_keys));
    while ($ar_res = $db_res->Fetch()) {
        $ar_new_values[$ar_res['ID']] = $ar_purchase_count[$ar_res['XML_ID']];
    }

    foreach ($ar_new_values as $key => $value) {
        \CIBlockElement::SetPropertyValuesEx($key, $iblock_id, array('PURCHASE_COUNT' => $value));
    }
}

