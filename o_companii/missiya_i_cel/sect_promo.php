<div class="aside-promo">
    <div id="js-aside-carousel" class="aside-promo__content">
        <div class="aside-promo__slide" style="background-color:#ee6b00;">
            <a class="aside-promo__link" href="#">
                <div class="aside-promo__title">Скидка 5%
                    <br>на фитинги vbccbz</div>
                <div class="aside-promo__text"></div>
                <div class="aside-promo__img">
                    <img src="<?=SITE_TEMPLATE_PATH?>/static/images/upload/aside/aside-img-1.jpg?v=b13fe72f" width="350" height="360" alt>
                </div>
            </a>
        </div>
        <div class="aside-promo__slide" style="background-color:#d9d9d9;">
            <a class="aside-promo__link" href="#">
                <div class="aside-promo__title">Заголовок миссия</div>
                <div class="aside-promo__text">Текст под заголовком</div>
                <div class="aside-promo__img">
                    <img src="<?=SITE_TEMPLATE_PATH?>/static/images/upload/aside/aside-img-2.jpg?v=74f4cc77" width="350" height="440" alt>
                </div>
            </a>
        </div>
    </div>
    <div id="js-aside-carousel-dots" class="aside-promo__dots"></div>
</div>