<?php








require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Мы хотим сделать принятие сложных потребительских решений простым и увлекательным занятием.");
$APPLICATION->SetPageProperty("title", "Наша миссия | интернет-магазин Эндимарт #VREGION_NAME#");
$APPLICATION->SetTitle("Наша миссия - делать жизнь людей лучше");
?><div class="wysiwyg">
	<div class="col-20 col-sm-10 b-list__item">
		<div class="text-small-expand">
			 Наша цель - сделать принятие сложных потребительских решений простым и увлекательным занятием.<br>
 <br>
			 Наши ценности:<br>
			<ul>
				<li>Трудолюбие</li>
				<li>Вера в себя и команду</li>
				<li>Открытость переменам</li>
			</ul>
 <br>
			 На этих принципах изо дня в день строится работа нашей команды!<br>
 <br>
 <br>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>