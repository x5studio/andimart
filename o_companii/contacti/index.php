<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Наши контакты | интернет-магазин Эндимарт #VREGION_NAME#");
$APPLICATION->SetPageProperty("description", "Контактная информация: Горячая линия: 8-800-550-33-95; Прием звонков: пн. - пт., 09:00 - 17:00; Отгрузка заказов: пн. - сб., 09:00 - 18:00");
$APPLICATION->SetTitle("Наши контакты");
$str = explode('.', $_SERVER["SERVER_NAME"]);
if (count($str)>2) {
    require_once("regional_block.php");
}
else {?><b>Федеральный номер: 8 (800) 550-33-95 (бесплатный)</b><br>
 Прием звонков: пн. – вс., 09:00 – 20:00<br>
<br>
 <b>Наша почта:</b>&nbsp;<br>
 Общие вопросы: <a href="mailto:ask@andimart.ru">ask@andimart.ru</a> <br>
 Отдел сервиса: <a href="mailto:srv@andimart.ru">srv@andimart.ru</a> <br>
 Отдел доставки: <a href="mailto:car@andimart.ru">car@andimart.ru</a> <br>
 Оптовый отдел: <a href="mailto:b2b@andimart.ru">b2b@andimart.ru</a> <br>
 Отдел закупок: <a href="mailto:tender@andimart.ru">tender@andimart.ru</a> <br>
 <br>
 <b>Чат в Telegram:</b> <a href="https://t.me/andimart_help_bot" rel="nofollow">@andimart_help_bot</a><br>
 <br>
 <b>Наши реквизиты:</b> <br>
 Адрес центрального офиса: 350080, Россия, Краснодар, улица Уральская, дом 212&nbsp;<br>
 Наименование: ИП Свидин Дмитрий Александрович ИНН: 263013460205 ОГРНИП: 311265127100211, серия 26 №003714717<br>
 <br>
 <iframe src="https://yandex.ru/map-widget/v1/-/CBe7zHR9tA" width="90%" height="400" frameborder="0"></iframe>
<?}?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>