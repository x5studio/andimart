<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

list($domain,) = explode(".", $_SERVER["SERVER_NAME"]);

$arTotalData = \XFive\GetContacts::getContactsByDomainFromCache($domain);?>
</div></div>
<style>
    .myflex {
        display: flex;
    }
    .button.mfp-arrow{
        top: 50%!important;
    }
    .shop-data {
        padding-bottom: 50px;
    }
    .shop-content {
        padding-bottom: 100px;
    }
    .carousel-content {
        border: 1px solid #ccc;
        padding: 5px;
    }
    .photo-gallery {
        margin-left: 5px;
    }
    .col-6 {
        width: 50%;
    }
    .slick-next {
        right: -15px !important;
    }
    .slick-prev {
        left: -25px!important;
    }
    @media screen and (max-width: 480px){
        .ymaps-map{
            width: 72%!important;
        }
        .myflex{
            display: block;
            padding: 10px 0px;
        }
        .carousel-content {
            margin-bottom: 10px;
        }
    }
    @media screen and (min-width: 481px) and (max-width: 1023px){
        .myflex{
            display: block;
            padding: 15px 10px;
        }
        .carousel-content {
            margin-bottom: 10px;
        }
    }
</style>
<?
//Output data
if ($arTotalData) {
    foreach ($arTotalData as $Items) {?>
        <div class="myflex content-block island">
            <div class="col-sm-12 col-md-6">
                <div class="row shop-data">
                    <div class="col-12">
                        <div class="point_name"><b>Пункт выдачи <?= $Items["NAME"] ?></b></div>
                        <div class="point_address"><?
                            echo $Items["ADDRESS"]["NAME"] . ": " . $Items["ADDRESS"]["VALUE"]; ?></div>
                        <div class="point_address"><?
                            echo $Items["PHONE"]["NAME"] . ": " . $Items["PHONE"]["VALUE"]; ?></div>
                        <div class="point_address"><?
                            echo $Items["WORKING_HOURS"]["NAME"] . ": " . $Items["WORKING_HOURS"]["VALUE"]; ?></div>
                    </div>
                </div>
                <?
                if ($Items["MORE_PHOTO"]["VALUE"]): ?>
                    <div class="row carousel-content">
                        <div class="col-12">
                            <div class="photo-gallery">
                                <?
                                foreach ($Items["MORE_PHOTO"]["VALUE"] as $id => $FILE) {
                                    $preview = CFile::ResizeImageGet($FILE, array('width' => 120, 'height' => '100'), BX_RESIZE_IMAGE_EXACT, true);

                                    $FILE = CFile::GetFileArray($FILE);
                                    if (is_array($FILE)) {
                                        ?>
                                        <div id="<?= $id ?>">
                                            <a class="photo_item" href="<?= $FILE["SRC"] ?>">
                                                <?
                                                echo "<img src='{$preview["src"]}' width='{$preview["width"]}' height='{$preview["height"]}' data-slides='{$id}'/>";
                                                ?>
                                            </a>
                                        </div>
                                        <?
                                    }
                                } ?>
                            </div>
                        </div>
                    </div>
                <?endif; ?>
            </div>
            <div class="col-sm-12 col-md-6">
             <? if ($Items["LOCATION"]["VALUE"]) {
                list($x,$y)=explode(",", $Items["LOCATION"]["VALUE"]);
                $mapData = serialize(array (
                    'yandex_lat' => $x,
                    'yandex_lon' => $y,
                    'yandex_scale' => 15,
                    'PLACEMARKS' =>
                        array (
                            0 =>
                                array (
                                    'LON' => $y,
                                    'LAT' => $x,
                                    'TEXT' => $Items["NAME"],
                                ),
                        ),
                ));
                 $APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view", 
	".default", 
	array(
		"CONTROLS" => array(
			0 => "ZOOM",
		),
		"INIT_MAP_TYPE" => "MAP",
                "MAP_DATA" => $mapData,
		"MAP_HEIGHT" => "250",
		"MAP_ID" => "",
		"MAP_WIDTH" => "450",
		"OPTIONS" => array(
			0 => "ENABLE_SCROLL_ZOOM",
			1 => "ENABLE_DBLCLICK_ZOOM",
			2 => "ENABLE_DRAGGING",
		),
		"COMPONENT_TEMPLATE" => ".default",
		"API_KEY" => "7cd000f0-6c2f-46d6-a3c4-a26922cfb3ab"
	),
	false
);
                }?>
            </div>
        </div><?
    }
}?>
<div class="content-block island">
    <b>Наша почта:</b>
    <br>
    Общие вопросы: <a href="mailto:ask@andimart.ru">ask@andimart.ru</a> <br>
    Отдел сервиса: <a href="mailto:srv@andimart.ru">srv@andimart.ru</a> <br>
    Отдел доставки: <a href="mailto:car@andimart.ru">car@andimart.ru</a> <br>
    Оптовый отдел: <a href="mailto:b2b@andimart.ru">b2b@andimart.ru</a> <br>
    Отдел закупок: <a href="mailto:tender@andimart.ru">tender@andimart.ru</a> <br>
    <br>
    <b>Чат в Telegram:</b> <a href="https://t.me/andimart_help_bot" rel="nofollow">@andimart_help_bot</a><br>
    <br>
    <b>Наши реквизиты:</b> <br>
    Адрес центрального офиса: 350080, Россия, Краснодар, улица Уральская, дом 212<br>
    Наименование: ИП Свидин Дмитрий Александрович ИНН: 263013460205 ОГРНИП: 311265127100211, серия 26 №003714717<br>
</div><div><div>
<script>
    $('.photo-gallery').slick({
        mobileFirst: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: true,
        responsive: [
            {
                breakpoint: 380,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $('.photo_item').magnificPopup({
        type: 'image',
        closeBtnInside: false
    });
</script>